using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Redshift.Model
{
    public class NAISpecimen : ConcreteEntityBase<int>
    {
        public NAISpecimen()
        {
        }
        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public string CSID { get; set; }
        public string CUID { get; set; }
        public string SubType { get; set; }
        public string PassageLevel { get; set; }
        public DateTime? DateHarvested { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public DateTime? DateCollected { get; set; }
        public string AliquotAsJson { get; set; }
	}
}