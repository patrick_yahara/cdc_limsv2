
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Redshift.Model;

namespace ContractLab.Repository.Redshift.Models.Mapping
{
    public class NAISpecimenMap : EntityTypeConfiguration<NAISpecimen>
    {
        public NAISpecimenMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.Action)
                .HasMaxLength(128);

            Property(t => t.Status)
                .HasMaxLength(50);

            Property(t => t.CSID)
                .HasMaxLength(128);

            Property(t => t.CUID)
                .HasMaxLength(128);

            Property(t => t.SubType)
                .HasMaxLength(128);

            Property(t => t.PassageLevel)
                .HasMaxLength(128);

            Property(t => t.State)
                .HasMaxLength(50);

            Property(t => t.Country)
                .HasMaxLength(50);

            Property(t => t.AliquotAsJson)
                .HasMaxLength(int.MaxValue);

            // Table & Column Mappings
            ToTable("NAISpecimen");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.Action).HasColumnName("Action");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.CSID).HasColumnName("CSID");
            Property(t => t.SubType).HasColumnName("SubType");
            Property(t => t.PassageLevel).HasColumnName("PassageLevel");
            Property(t => t.DateHarvested).HasColumnName("DateHarvested");
            Property(t => t.State).HasColumnName("State");
            Property(t => t.Country).HasColumnName("Country");
            Property(t => t.DateCollected).HasColumnName("DateCollected");
            Property(t => t.AliquotAsJson).HasColumnName("AliquotAsJson");

            // Relationships

        }
    }
}