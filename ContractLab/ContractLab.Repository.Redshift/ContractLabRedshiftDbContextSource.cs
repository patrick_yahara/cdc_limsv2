﻿using Yahara.Core.Repository;
using Yahara.Core.Repository.EF;

namespace ContractLab.Repository.Redshift
{
    public class ContractLabRedshiftDbContextSource : EntityFrameworkDbContextSource<ContractLabRedshiftDbContext, DefaultContextInitializer>
    {
        public ContractLabRedshiftDbContextSource(BasicDatabaseSettings databaseSettings)
            : base(databaseSettings)
        {
        }

        public override string GetConfigurationsNamespace()
        {
            return "ContractLab.Repository.Redshift.Models.Mapping";
        }
    }
}
