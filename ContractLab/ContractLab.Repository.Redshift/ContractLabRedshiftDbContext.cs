﻿using System.Data.Entity.Infrastructure;
using Yahara.Core.Repository.EF;

namespace ContractLab.Repository.Redshift
{
    public class ContractLabRedshiftDbContext : DomainSpecificContext
    {
        public ContractLabRedshiftDbContext(string nameOrConnectionString, DbCompiledModel compiledModel)
            : base(nameOrConnectionString, compiledModel)
        {
            Configuration.LazyLoadingEnabled = false;
        }
    }
}
