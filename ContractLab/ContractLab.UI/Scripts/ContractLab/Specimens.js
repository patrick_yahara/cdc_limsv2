﻿//function initialize() {
//    var TravelCounties = $('#TravelCountyId'); // cache the element
//    $('#TravelStateId').change(function () {
//        TravelCounties.empty();
//        var url = '/Specimens/GetCounties/' + $(this).val();;
//        $.getJSON(url, { id: $(this).val() }, function (response) {
//            // clear and add default (null) option
//            TravelCounties.empty().append($('<option></option>').val('').text(''));
//            $.each(response, function (index, item) {
//                TravelCounties.append($('<option></option>').val(item.Value).text(item.Text));
//            });
//        }).fail(function () {
//            console.log("error");
//        });
//    });
//    var SpecimenCounties = $('#CountyId'); // cache the element
//    $('#StateId').change(function () {
//        SpecimenCounties.empty();
//        var url = '/Specimens/GetCounties/' + $(this).val();;
//        $.getJSON(url, { id: $(this).val() }, function (response) {
//            // clear and add default (null) option
//            SpecimenCounties.empty().append($('<option></option>').val('').text(''));
//            $.each(response, function (index, item) {
//                SpecimenCounties.append($('<option></option>').val(item.Value).text(item.Text));
//            });
//        }).fail(function (jqXHR, textStatus, errorThrown) {
//            console.log("error: " + textStatus);
//            console.log("incoming: " + jqXHR.responseText);
//        });
//    });
//};
//initialize();
function keypressHandler(e) {
    if (e.which == 13) {
        e.preventDefault(); //stops default action: submitting form
        $(this).blur();
        $('#applybutton').focus().click();//give your submit an ID
    }
}

$(document).ready(function () {
    $('#MultiActionForm').keypress(keypressHandler);
});