﻿$(document).ready(function () {
    $("#checkboxes .btn").not("#checkboxes .dropdown-toggle").click(function () {
        $(this).button('loading').delay(500).queue(function () {
            $(this).button('reset');
            $(this).dequeue();
        });
    });
    /* Jira CLAW-1 */
    /* $('.datepicker').datepicker(); */
    //if (!Modernizr.inputtypes['date']) { $('.datepicker').datepicker({ format: 'yyyy-mm-dd', autoclose: true }); }
    $('.datepicker').datepicker({ format: 'yyyy-mm-dd', autoclose: true });
});

$(function () {


    function toggleChecked(status) {
        $("#checkboxes input").each(function () {
            // Set the checked status of each to match the 
            // checked status of the check all checkbox:
            $(this).prop("checked", status);
        });
    }

    // Grab a reference to the check all box:
    var checkAllBox = $("#checkall");
    if (checkAllBox.length) {
        //Set the default value of the global checkbox to true:
//        checkAllBox.prop('checked', true);

        // Attach the call to toggleChecked to the
        // click event of the global checkbox:
        checkAllBox.click(function () {
            var status = checkAllBox.prop('checked');
            toggleChecked(status);
        });
    }
});