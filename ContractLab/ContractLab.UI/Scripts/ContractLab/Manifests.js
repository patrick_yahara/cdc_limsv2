﻿function keypressHandler(e) {
    if (e.which == 13) {
        e.preventDefault(); //stops default action: submitting form
        $(this).blur();
        $('#addAliquot').focus().click();//give your submit an ID
    }
}

$(document).ready(function () {
    $('#EditForm').keypress(keypressHandler);
});