﻿using System.Web;
using System.Web.Optimization;

namespace ContractLab.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                                    "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-easyui").Include(
                                    "~/Scripts/jquery.easyui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datepicker.css",
                      "~/Content/themes/default/easyui.css",
                      "~/Content/animation.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/ContractLab/Specimens").Include(
                           "~/Scripts/ContractLab/Common.js",
                           "~/Scripts/modernizr-*",
                           "~/Scripts/ContractLab/Specimens.js"
                           ));
            bundles.Add(new ScriptBundle("~/bundles/ContractLab/Packages").Include(
                           "~/Scripts/ContractLab/Common.js",
                           "~/Scripts/modernizr-*",
                           "~/Scripts/ContractLab/Packages.js"
                           ));

            bundles.Add(new ScriptBundle("~/bundles/ContractLab/Manifests").Include(
                           "~/Scripts/ContractLab/Common.js",
                           "~/Scripts/modernizr-*",
                           "~/Scripts/ContractLab/Manifests.js"
                           ));

            bundles.Add(new ScriptBundle("~/bundles/ContractLab").Include(
                           "~/Scripts/ContractLab/Common.js",
                           "~/Scripts/modernizr-*"
                           ));
                           
        }
    }
}
