﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ContractLab.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            // /owin/security/keycloak/AIMS/callback
            routes.MapRoute(
                        name: "signin-keycloak",
                        url: "owin/security/keycloak/AIMS/callback",
                        defaults: new { controller = "Account", action = "ExternalLoginCallback" });
        }
    }
}
