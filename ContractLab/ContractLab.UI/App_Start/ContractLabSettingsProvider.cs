﻿using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ContractLab.UI.App_Start
{
    public class ContractLabSettingsProvider : ISettingsProvider
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ContractLabEFModel"].ConnectionString;
        }

        public string GetConnectionString_Redshift()
        {
            return ConfigurationManager.ConnectionStrings["ContractLabEFModel_Redshift"].ConnectionString;
        }

        public string GetAuthClientId()
        {
            return ConfigurationManager.AppSettings["AuthClientId"];
        }

        public string GetAuthClientSecret()
        {
            return ConfigurationManager.AppSettings["AuthClientSecret"];
        }

        public string GetAuthVirtualDirectory()
        {
            return ConfigurationManager.AppSettings["AuthVirtualDirectory"];
        }

        public string GetAuthRealm()
        {
            return ConfigurationManager.AppSettings["AuthRealm"];
        }

        public string GetAuthUrl()
        {
            return ConfigurationManager.AppSettings["AuthUrl"];
        }

        public string GetAuthType()
        {
            return ConfigurationManager.AppSettings["AuthType"];
        }

        public string GetAuthCaption()
        {
            return ConfigurationManager.AppSettings["AuthCaption"];
        }

        public string GetAuthForceHttps()
        {
            return ConfigurationManager.AppSettings["AuthForceHttps"];
        }

        public string GetTransmissionDropDirectory()
        {
            return ConfigurationManager.AppSettings["TransmissionDropDirectory"];
        }

        public string GetTransmissionErrorDirectory()
        {
            return ConfigurationManager.AppSettings["TransmissionErrorDirectory"];
        }

        public string GetTransmissionProcessedDirectory()
        {
            return ConfigurationManager.AppSettings["TransmissionProcessedDirectory"];
        }

        public string GetHangfirePollingInterval()
        {
            return ConfigurationManager.AppSettings["HangfirePollingInterval"];
        }

        public string GetErrorFileDeleteInterval()
        {
            return ConfigurationManager.AppSettings["ErrorFileDeleteInterval"];
        }

        public string GetProcessedFileDeleteInterval()
        {
            return ConfigurationManager.AppSettings["ProcessFileDeleteInterval"];
        }

        public string GetTransmissionMessageSize()
        {
            return ConfigurationManager.AppSettings["TransmissionMessageSize"];
        }

        public string GetQueueFileCreateInterval()
        {
            return ConfigurationManager.AppSettings["QueueFileCreateInterval"];
        }

        public string GetPasswordResetEmailAddress()
        {
            return ConfigurationManager.AppSettings["PasswordResetEmailAddress"];
        }

        public string GetPasswordResetEmailSubject()
        {
            return ConfigurationManager.AppSettings["PasswordResetEmailSubject"];
        }

        public string GetMaxCSID()
        {
            return ConfigurationManager.AppSettings["MaxCSID"];
        }

        public string GetMaxCUID()
        {
            return ConfigurationManager.AppSettings["MaxCUID"];
        }
    }
}
