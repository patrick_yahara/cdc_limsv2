﻿using ContractLab.UI.BackgroundJobs;
using ContractLab.DataService.Interfaces;
using Hangfire;
using Hangfire.SqlServer;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI
{
    public partial class Startup
    {

        public void ConfigureHangfire(IAppBuilder app)
        {
            // * some configuration also in ninject web common
            int pollinginterval = 0;
            int.TryParse(_settings.GetHangfirePollingInterval(), out pollinginterval);
            if (pollinginterval <= 0)
            {
                pollinginterval = 15;
            }
            GlobalConfiguration.Configuration.UseNinjectActivator(
                                                 new Ninject.Web.Common.Bootstrapper().Kernel);
            GlobalConfiguration.Configuration
                .UseSqlServerStorage(
                    _settings.GetConnectionString(),
                    new SqlServerStorageOptions { QueuePollInterval = TimeSpan.FromSeconds(pollinginterval) });
            app.UseHangfireDashboard("/Transmissions/Hangfire", new DashboardOptions
            {
                AppPath = VirtualPathUtility.ToAbsolute("~"),
                Authorization = new[] { new HangfireAuthFilter() },
            });
            app.UseHangfireServer();
            FileCleanup.CreateJobs(_settings);
            TransmissionDrop.CreateJobs(_settings);
        }
    }
}