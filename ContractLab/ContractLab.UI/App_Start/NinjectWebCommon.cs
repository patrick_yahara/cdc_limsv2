[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ContractLab.UI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ContractLab.UI.App_Start.NinjectWebCommon), "Stop")]

namespace ContractLab.UI.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using ContractLab.DataService.Modules;
    using ContractLab.DataService.Interfaces;
    using Yahara.Core.Logging;
    using Yahara.Core.Logging.Log4Net;
    using System.Security.Principal;
    using ContractLab.Utilities;
    using ContractLab.DataService.Redshift.Modules;
    using ContractLab.DataService.Redshift.Interfaces;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            LoggingFactory.InitializeLogFactory(new Log4NetAdapter());
            LoggingFactory.GetLogger().Log(typeof(NinjectWebCommon), LoggingLevel.Info, "Starting Ninject Kernel Binding.");
            kernel.Bind<ISettingsProvider>().To<ContractLabSettingsProvider>().InSingletonScope();
            kernel.Bind<IPrincipal>().ToMethod(ctx => HttpContext.Current.User).InRequestScope();
            kernel.Load(new UserContextModule());
            kernel.Load(new DataServiceRuntimeModule());
            //kernel.Load(new DataServiceRuntimeModule_Redshift());
        }        
    }
}
