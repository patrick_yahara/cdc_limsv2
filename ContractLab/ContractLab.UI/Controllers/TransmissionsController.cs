﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;
using ContractLab.DataService;
using System.IO;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class TransmissionsController : Controller
    {
        private readonly IContractLabDataService _dataService;
        private readonly ISettingsProvider _settings;
        public TransmissionsController(IContractLabDataService dataService, ISettingsProvider settings)
        {
            _dataService = dataService;
            _settings = settings;
        }

        // GET: Manifests
        public ActionResult Index(string SortOrder, int? PageSize, int? page)
        {
            string localSortOrder = SortOrder;
            int? localPageSize = PageSize;
            int? localpage = page;

            ViewBag.AllowC = _dataService.UserHasLab().Success;

            int? DefaultPageSize = 10;
            int currentPageIndex = localpage.HasValue ? localpage.Value - 1 : 0;
            if (localPageSize != null)
            {
                DefaultPageSize = localPageSize;
            }

            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));
            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;

            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("TransmittedOn", "TransmittedOn");
            sortColumns.Add("CreatedOn", "CreatedOn");
            sortColumns.Add("Specimen.CSID", "Specimen.CSID");
            sortColumns.Add("Isolate.IsolateType.DisplayName", "Isolate.IsolateType.DisplayName");
            sortColumns.Add("Aliquot.CUID", "Aliquot.CUID");
            sortColumns.Add("Attempts", "Attempts");
            sortColumns.Add("SupercedeId", "SupercedeId");
            sortColumns.Add("Id", "Id");

            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(localSortOrder) && localSortOrder.EndsWith(" DESC"))
            {
                var trimmed = localSortOrder.Remove(localSortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(localSortOrder) && sortColumns.ContainsValue(localSortOrder))
                {
                    validSort = true;
                    sortColumns[localSortOrder] = string.Concat(localSortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;

            var vm = new List<TransmissionVM>();
            VirtualPagedList<TransmissionVM> vpl;
            var requestdetail = new RequestDetail() { CurrentPage = localpage ?? 1, ItemsPerPage = DefaultPageSize.Value };

            // do the oerder by
            if (validSort)
            {
                requestdetail.OrderBy = localSortOrder;
                ViewBag.SortOrder = localSortOrder;
            }


            var dto = _dataService.TransmissionList(new MessageBase<RequestDetail>() { Data = requestdetail });
            if (dto.Success)
            {
                var transmissions = dto.Data as List<TransmissionDTO>;
                transmissions.ForEach(e => vm.Add(new TransmissionVM(e)));
            }
            vpl = new VirtualPagedList<TransmissionVM>(vm, dto.ItemsPerPage, dto.TotalCount, dto.TotalPages, dto.CurrentPage);
            return View(vpl);
        }

        // GET: Manifests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransmissionVM vm = null;
            var dto = _dataService.Transmission(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new TransmissionVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Manifests/Create
        public ActionResult Create()
        {

            PopulateCombos();

            var vm = new TransmissionVM();
            ViewData.Model = vm;
            return View();
        }

        private void PopulateCombos()
        {
            //var statusList = _dataService.StatusList().Data;
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name");
        }

        // POST: Manifests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SpecimenId, Payload")] TransmissionVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new TransmissionDTO { SpecimenId = vm.SpecimenId, Payload = vm.Payload };
                var Manifest = _dataService.TransmissionInsert(new MessageBase<TransmissionDTO>() { Data = dto });
                if (Manifest.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            PopulateCombos();
            return View(vm);
        }

        // GET: Manifests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TransmissionVM vm = null;
            var dto = _dataService.Transmission(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                PopulateCombos(dto.Data);
                vm = new TransmissionVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        private void PopulateCombos(TransmissionDTO dto)
        {
            //var statusList = _dataService.StatusList().Data;
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name", dto.StatusId);
        }

        // POST: Manifests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SpecimenId, Payload")] TransmissionVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new TransmissionDTO { Id = vm.Id, SpecimenId = vm.SpecimenId, Payload = vm.Payload };

                var TRS = _dataService.TransmissionEdit(new MessageBase<TransmissionDTO>() { Data = dto });
                if (TRS.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    var dtoE = _dataService.Transmission(new MessageBase<int>() { Data = vm.Id });
                    PopulateCombos(dtoE.Data);
                }
            }
            return View(vm);
        }

        // GET: Manifests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransmissionVM vm = null;
            var dto = _dataService.Transmission(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new TransmissionVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Manifests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.TransmissionDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult MultiAction(VirtualPagedList<ContractLab.UI.Models.ContractLab.TransmissionVM> vm)
        {
            var items = vm.Items.Where(s => s.Selected).Select(s => s.Id).ToList();
            if (items.Count > 0)
            {
                if (Request.Form["submitDLT"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateTransmissionFile(new MessageBase<List<int>>() { Data = items });
                    if (dto != null && dto.Success)
                    {
                        //var cd = new System.Net.Mime.ContentDisposition
                        //{
                        //    FileName = dto.Data.FileName,
                        //    Inline = false,
                        //};
                        //Response.AppendHeader("Content-Disposition", cd.ToString());
                        //return File(dto.Data.File, @"application/json"); //'application/json; charset=utf-8'
                        var dir = new DirectoryInfo(_settings.GetTransmissionDropDirectory());
                        if (dir.Exists)
                        {
                            var filepath = Path.Combine(dir.FullName, dto.Data.FileName);
                            System.IO.File.WriteAllBytes(filepath, dto.Data.File);              
                        }
                    }
                }

            }
            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Download")]
        public ActionResult Download(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Path.GetFileName(name) != name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dir = TempData["path"] as DirectoryInfo;
            if (dir == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fullpath = Path.Combine(dir.FullName, name);
            if (!System.IO.File.Exists(fullpath))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = name,
                Inline = false,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(fullpath, @"application/json");
        }

        [HttpGet, ActionName("DeleteFile")]
        public ActionResult DeleteFile(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Path.GetFileName(name) != name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dir = TempData["path"] as DirectoryInfo;
            if (dir == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fullpath = Path.Combine(dir.FullName, name);
            if (!System.IO.File.Exists(fullpath))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                System.IO.File.Delete(fullpath);
            }
            return RedirectToAction("TransmissionFiles");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult TransmissionFiles(string SortOrder, int? PageSize, int? page, string type)
        {
            string localtype = type;
            if (string.IsNullOrEmpty(localtype))
            {
                localtype = "queue";
            }
            string localSortOrder = SortOrder;
            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("LastAccessTimeUtc", "LastAccessTimeUtc");
            sortColumns.Add("LastWriteTimeUtc", "LastWriteTimeUtc");
            sortColumns.Add("Name", "Name");
            sortColumns.Add("Length", "Length");

            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(localSortOrder) && localSortOrder.EndsWith(" DESC"))
            {
                var trimmed = localSortOrder.Remove(localSortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(localSortOrder) && sortColumns.ContainsValue(localSortOrder))
                {
                    validSort = true;
                    sortColumns[localSortOrder] = string.Concat(localSortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;



            List<TransmissionFileVM> vm = new List<TransmissionFileVM>();
            DirectoryInfo dir;
            switch (localtype)
            {
                case "error":
                    dir = new DirectoryInfo(_settings.GetTransmissionErrorDirectory());
                    break;
                case "processed":
                    dir = new DirectoryInfo(_settings.GetTransmissionProcessedDirectory());
                    break;
                case "queue":
                default:
                    localtype = "queue";
                    dir = new DirectoryInfo(_settings.GetTransmissionDropDirectory());
                    break;
            }
            ViewBag.Type = localtype;
            
            TempData["path"] = dir;
            var files = dir.GetFiles().AsQueryable();

            int? localPageSize = PageSize;
            int? localpage = page;

            int? DefaultPageSize = 10;
            int currentPageIndex = localpage.HasValue ? localpage.Value : 1;
            if (localPageSize != null)
            {
                DefaultPageSize = localPageSize;
            }

            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));

            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;


            List<KeyValuePair<string, string>> types = new List<KeyValuePair<string, string>>();
            types.Add(new KeyValuePair<string, string>("queue", "queue"));
            types.Add(new KeyValuePair<string, string>("processed", "processed"));
            types.Add(new KeyValuePair<string, string>("error", "error"));

            ViewBag.Types = new SelectList(types, "Value", "Key", localtype);
            ViewBag.Type = localtype;

            int skip = DefaultPageSize.Value * (currentPageIndex - 1);
            int take = DefaultPageSize.Value;

            // do the oerder by
            if (validSort)
            {
                files = files.OrderBy(localSortOrder);
            }

            foreach (var file in files.Skip(skip).Take(take))
            {
                vm.Add(
                    new TransmissionFileVM() {
                        DirectoryName = dir.Name,
                        LastAccessTimeUtc = file.LastAccessTimeUtc,
                        LastWriteTimeUtc = file.LastWriteTimeUtc,
                        Name = file.Name,
                        Length = (int)(file.Length / 1024),
                    }
                    );
            }
            var totalPages = files.Count() % DefaultPageSize.Value == 0 ? files.Count() / DefaultPageSize.Value : files.Count() / DefaultPageSize.Value + 1;
            VirtualPagedList<TransmissionFileVM> vpl = new VirtualPagedList<TransmissionFileVM>(vm, DefaultPageSize.Value, files.Count(), totalPages, currentPageIndex);
            return View(vpl);
        }

    }
}