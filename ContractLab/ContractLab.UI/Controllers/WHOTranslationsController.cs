﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContractLab.UI.Models;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using ContractLab.DTO;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    public class WHOTranslationsController : Controller
    {

        private const string _fieldlist = @"Id, Heading, MatchExpression, ReplacementValue, CommentHeading, CommentValue";

        private readonly IContractLabDataService  _dataService;
        public WHOTranslationsController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: WHOTranslations
        public ActionResult Index()
        {
            var vm = new List<WHOTranslationVM>();
            var dto = _dataService.WHOTranslationList();
            if (dto.Success)
            {
                var translations = dto.Data as List<WHOTranslationDTO>;
                translations.ForEach(e => vm.Add(new WHOTranslationVM(e)));
            }
            return View(vm);
        }

        // GET: WHOTranslations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WHOTranslationVM vm = null;
            var dto = _dataService.WHOTranslation(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new WHOTranslationVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: WHOTranslations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WHOTranslations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] WHOTranslationVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new WHOTranslationDTO() 
                { 
                    CommentValue = vm.CommentValue,
                    CommentHeading = vm.CommentHeading,
                    Heading = vm.Heading,
                    MatchExpression = vm.MatchExpression,
                    ReplacementValue = vm.ReplacementValue,
                };
                var result = _dataService.WHOTranslationInsert(new MessageBase<WHOTranslationDTO>() { Data = dto });
                if (result.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: WHOTranslations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WHOTranslationVM vm = null;
            var dto = _dataService.WHOTranslation(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new WHOTranslationVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: WHOTranslations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] WHOTranslationVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new WHOTranslationDTO
                {
                    Id = vm.Id,
                    CommentValue = vm.CommentValue,
                    CommentHeading = vm.CommentHeading,
                    Heading = vm.Heading,
                    MatchExpression = vm.MatchExpression,
                    ReplacementValue = vm.ReplacementValue,
                };
                var Lab = _dataService.WHOTranslationEdit(new MessageBase<WHOTranslationDTO>() { Data = dto });
                if (Lab.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: WHOTranslations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WHOTranslationVM vm = null;
            var dto = _dataService.WHOTranslation(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new WHOTranslationVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: WHOTranslations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.WHOTranslationDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
