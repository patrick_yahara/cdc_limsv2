﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;
using ContractLab.DataService;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class ManifestsController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public ManifestsController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Manifests
        public ActionResult Index(string SortOrder, string IdFilter, string CUID, int? StatusFilterIndex, int? PageSize, int? page, bool newcookie = false)
        {
            string localSortOrder = SortOrder;
            string localIdFilter = IdFilter;
            int? localStatusFilterIndex = StatusFilterIndex;
            int? localPageSize = PageSize;
            int? localpage = page;
            string localCUID = CUID;

            if (!newcookie && this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParamsMF"))
            {
                var indexparams = this.ControllerContext.HttpContext.Request.Cookies["IndexParamsMF"];
                if (!newcookie)
                {
                    newcookie = false;
                    localSortOrder = indexparams["SortOrder"];
                    localIdFilter = indexparams["IdFilter"];
                    localCUID = indexparams["CUID"];
                    int tempInt;
                    if (!string.IsNullOrEmpty(indexparams["StatusFilterIndex"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["StatusFilterIndex"], out tempInt))
                        {
                            localStatusFilterIndex = tempInt;
                        }
                        else
                        {
                            localStatusFilterIndex = null;
                        }
                    }
                    else
                    {
                        localStatusFilterIndex = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["PageSize"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["PageSize"], out tempInt))
                        {
                            localPageSize = tempInt;
                        }
                        else
                        {
                            localPageSize = null;
                        }
                    }
                    else
                    {
                        localPageSize = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["page"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["page"], out tempInt))
                        {
                            localpage = tempInt;
                        }
                        else
                        {
                            localpage = null;
                        }
                    }
                    else
                    {
                        localpage = null;
                    }
                }
            }
            if (newcookie || !this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParamsMF"))
            {
                var cookie = new HttpCookie("IndexParamsMF");
                cookie.Values.Add("SortOrder", SortOrder);
                cookie.Values.Add("IdFilter", IdFilter);
                cookie.Values.Add("StatusFilterIndex", StatusFilterIndex.ToString());
                cookie.Values.Add("PageSize", PageSize.ToString());
                cookie.Values.Add("page", page.ToString());
                cookie.Values.Add("CUID", CUID);
                Response.Cookies.Add(cookie);
            }


            int currentStatusFilterIndex = localStatusFilterIndex.HasValue ? localStatusFilterIndex.Value : 0;
            ViewBag.AllowC = _dataService.UserHasLab().Success;

            int? DefaultPageSize = 10;
            int currentPageIndex = localpage.HasValue ? localpage.Value - 1 : 0;
            if (localPageSize != null)
            {
                DefaultPageSize = localPageSize;
            }
            ViewBag.IdFilter = localIdFilter;

            var statusList = _dataService.StatusList().Data;
            if (statusList != null)
            {
                if (currentStatusFilterIndex <= statusList.Count)
                {
                    ViewBag.StatusFilters = new SelectList(statusList, "Value", "Name", statusList[currentStatusFilterIndex].Value);
                }
                else
                {
                    ViewBag.StatusFilters = new SelectList(statusList, "Value", "Name");
                }
            }

            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));
            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;

            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("Name", "Name");
            sortColumns.Add("CreatedOn", "CreatedOn");
            sortColumns.Add("IsVisible", "IsVisible");
            sortColumns.Add("DateShipped", "DateShipped");
            sortColumns.Add("AliquotCount", "AliquotCount");
            sortColumns.Add("Status.DisplayName", "Status.DisplayName");

            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(localSortOrder) && localSortOrder.EndsWith(" DESC"))
            {
                var trimmed = localSortOrder.Remove(localSortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(localSortOrder) && sortColumns.ContainsValue(localSortOrder))
                {
                    validSort = true;
                    sortColumns[localSortOrder] = string.Concat(localSortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;
            ViewBag.CUID = localCUID;

            var vm = new List<ManifestVM>();
            VirtualPagedList<ManifestVM> vpl;
            var requestdetail = new RequestDetail() { CurrentPage = localpage ?? 1, ItemsPerPage = DefaultPageSize.Value, CUID = localCUID };
            if (!string.IsNullOrEmpty(localIdFilter))
            {
                requestdetail.IdFilter = localIdFilter;
            }

            if (currentStatusFilterIndex <= statusList.Count && !string.IsNullOrEmpty(statusList[currentStatusFilterIndex].Value))
            {
                    requestdetail.Where = "StatusId = " + statusList[currentStatusFilterIndex].Value;
            }

            // do the oerder by
            if (validSort)
            {
                requestdetail.OrderBy = localSortOrder;
                ViewBag.SortOrder = localSortOrder;
            }

            ViewBag.StatusFilterIndex = localStatusFilterIndex;

            var dto = _dataService.ManifestList(new MessageBase<RequestDetail>() { Data = requestdetail });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                var manifests = dto.Data as List<ManifestDTO>;
                
                var selectedList = (TempData["SelectedItems"] as List<int>);
                if (selectedList == null)
                {
                    manifests.ForEach(e => vm.Add(new ManifestVM(e, uro, uie)));
                }
                else
                {
                    manifests.ForEach(e => { var tempSVM = new ManifestVM(e, uro, uie); tempSVM.Selected = e.Id.HasValue && selectedList.Contains(e.Id.Value); vm.Add(tempSVM); });
                }
                if (TempData.ContainsKey("DialogTitle"))
                {
                    ViewBag.DialogTitle = TempData["DialogTitle"];
                }
                if (TempData.ContainsKey("DialogBody"))
                {
                    ViewBag.DialogBody = TempData["DialogBody"];
                }
                if (TempData.ContainsKey("DialogSuccess"))
                {
                    ViewBag.DialogSuccess = TempData["DialogSuccess"];
                }
            }
            vpl = new VirtualPagedList<ManifestVM>(vm, dto.ItemsPerPage, dto.TotalCount, dto.TotalPages, dto.CurrentPage);
            return View(vpl);
        }

        // GET: Manifests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManifestVM vm = null;
            var dto = _dataService.Manifest(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new ManifestVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Manifests/Create
        public ActionResult Create()
        {

            PopulateCombos();

            var vm = new ManifestVM();
            ViewData.Model = vm;
            return View();
        }

        private void PopulateCombos()
        {
            var statusList = _dataService.StatusList().Data;
            ViewBag.StatusId = new SelectList(statusList, "Value", "Name");
        }

        // POST: Manifests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, StatusId, IsVisible, Comments, DateShipped, TrackingNumber")] ManifestVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ManifestDTO { Name = vm.Name, StatusId = vm.StatusId, IsVisible = vm.IsVisible, Comments = vm.Comments, DateShipped = vm.DateShipped, TrackingNumber = vm.TrackingNumber };
                var Manifest = _dataService.ManifestInsert(new MessageBase<ManifestDTO>() { Data = dto });
                if (Manifest.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            PopulateCombos();
            return View(vm);
        }

        // GET: Manifests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ManifestVM vm = null;
            var dto = _dataService.Manifest(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                PopulateCombos(dto.Data);
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new ManifestVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        private void PopulateCombos(ManifestDTO dto)
        {
            var statusList = _dataService.StatusList().Data;
            ViewBag.StatusId = new SelectList(statusList, "Value", "Name", dto.StatusId);
        }

        // POST: Manifests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, StatusId, IsVisible, Comments, Aliquots, AliquotIds, DateShipped, TrackingNumber")] ManifestVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ManifestDTO { Id = vm.Id, Name = vm.Name, StatusId = vm.StatusId, IsVisible = vm.IsVisible, Comments = vm.Comments, DateShipped = vm.DateShipped, TrackingNumber = vm.TrackingNumber };
                if (vm.AliquotIds != null)
                {
                    foreach (var item in vm.AliquotIds)
                    {
                        dto.Aliquots.Add(new AliquotDTO() { Id = item });
                    }                    
                }

                var Manifest = _dataService.ManifestEdit(new MessageBase<ManifestDTO>() { Data = dto });
                if (Manifest.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    var dtoE = _dataService.Manifest(new MessageBase<int>() { Data = vm.Id.Value });
                    PopulateCombos(dtoE.Data);
                }
            }
            return View(vm);
        }

        // GET: Manifests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManifestVM vm = null;
            var dto = _dataService.Manifest(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new ManifestVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Manifests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.ManifestDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult MultiAction(VirtualPagedList<ContractLab.UI.Models.ContractLab.ManifestVM> vm)
        {
            var items = vm.Items.Where(s => s.Selected).Select(s => s.Id.Value).ToList();
            if (items.Count > 0)
            {
                if (Request.Form["submitDLS"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateSpecimenFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Manifests" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLA"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateAliquotFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Manifests" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLSTrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateSpecimenTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Manifests" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLATrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateAliquotTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Manifests" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult AliquotRow(string cuid)
        {
            AliquotVM vm = new AliquotVM();
            if (String.IsNullOrEmpty(cuid))
            {
                return new HttpNotFoundResult();
            }
            else
            {
                var aliquot = _dataService.GetAliquotByCUID(new MessageBase<string>() { Data = cuid }).Data;
                if (aliquot != null)
                {
                    var uro = _dataService.UserReadOnly().Success;
                    var uie = _dataService.UserEditor().Success;
                    vm = new AliquotVM(aliquot, uro, uie);
                }
                else 
                {
                    return new HttpNotFoundResult();
                }
            }
            return View("_Aliquot", vm);
        }

        [HttpGet]
        public ActionResult Print(int? id)
        {
            ManifestVM vm = null;
            if (id != null)
            {
                var dto = _dataService.Manifest(new MessageBase<int>() { Data = id.Value });
                if (dto.Success)
                {
                    vm = new ManifestVM(dto.Data, true, true);
                    vm.Lab = new LabVM(dto.Data.Lab);
                    ViewData.Model = vm;
                    return View("_Print", vm);

                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}