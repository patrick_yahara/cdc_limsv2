﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class IsolateTypesController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public IsolateTypesController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: IsolateTypes
        public ActionResult Index()
        {
            var vm = new List<IsolateTypeVM>();
            var dto = _dataService.IsolateTypeList();
            if (dto.Success)
            {
                var IsolateTypes = dto.Data as List<IsolateTypeDTO>;
                IsolateTypes.ForEach(e => vm.Add(new IsolateTypeVM(e)));
            }
            return View(vm);
        }

        // GET: IsolateTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IsolateTypeVM vm = null;
            var dto = _dataService.IsolateType(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new IsolateTypeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: IsolateTypes/Create
        public ActionResult Create()
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            return View();
        }

        // POST: IsolateTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, DisplayName, SysName, IsVisible")] IsolateTypeVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new IsolateTypeDTO {
                    DisplayName = vm.DisplayName,
                    SysName = vm.SysName,
                    IsVisible = vm.IsVisible,
                };
                var IsolateType = _dataService.IsolateTypeInsert(new MessageBase<IsolateTypeDTO>() { Data = dto });
                if (IsolateType.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: IsolateTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IsolateTypeVM vm = null;
            var dto = _dataService.IsolateType(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new IsolateTypeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: IsolateTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, DisplayName, SysName, IsVisible")] IsolateTypeVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new IsolateTypeDTO { 
                    Id = vm.Id,
                    DisplayName = vm.DisplayName,
                    SysName = vm.SysName,
                    IsVisible = vm.IsVisible,
                };
                var IsolateType = _dataService.IsolateTypeEdit(new MessageBase<IsolateTypeDTO>() { Data = dto });
                if (IsolateType.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: IsolateTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IsolateTypeVM vm = null;
            var dto = _dataService.IsolateType(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new IsolateTypeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: IsolateTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.IsolateTypeDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}