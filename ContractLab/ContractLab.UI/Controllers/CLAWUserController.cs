﻿using ContractLab.DTO;
using ContractLab.UI.Models;
using ContractLab.DataService.Interfaces;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CLAWUserController : Controller
    {
        // * this is NOT a ninject service module
        ApplicationDbContext context = new ApplicationDbContext();

        private const string _fieldlist = @"Id, Email, LabId, Roles, ExternalId, ExternalProvider, DateDisabled";
        
        private readonly IContractLabDataService _dataService;
        public CLAWUserController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        // GET: CLAWUser
        public ActionResult Index()
        {
            var vm = new List<CLAWUserVM>();
            var dto = _dataService.UserList();
            if (dto.Success)
            {
                var users = dto.Data as List<AspNetUserDTO>;
                foreach (var item in users.OrderBy(s => s.UserName))
	            {
                    var userLogins = UserManager.GetLoginsAsync(item.Id).Result.AsQueryable<UserLoginInfo>().Select(e => new AspNetUserLoginDTO() { LoginProvider = e.LoginProvider, ProviderKey = e.ProviderKey, UserId = item.Id }).ToList();
                    item.AspNetUserLogins = userLogins;
                    vm.Add(new CLAWUserVM(item));
	            }

            }
            return View(vm);
        }

        //public ActionResult Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CLAWUserVM vm = null;
        //    var dto = _dataService.User(new MessageBase<string>() { Data = id });
        //    if (dto.Success)
        //    {
        //        vm = new CLAWUserVM(dto.Data);
        //    }
        //    else
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vm);
        //}

        public ActionResult Create()
        {
            var labs = _dataService.GetLabs().Data;
            if (labs == null)
            {
                labs = new List<LabDTO>();
            }
            else
            {
                labs.Insert(0, new LabDTO());
            }
            ViewBag.LabId = new SelectList(labs, "Id", "Name");


            var rolesEF = context.Roles.Select(s => new { Name = s.Name }).ToList();
            var rolelist = new MultiSelectList(rolesEF, "Name", "Name");
            ViewBag.Roles = rolelist;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] CLAWUserVM vm)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = vm.Email, Email = vm.Email };
                var result = UserManager.Create(user);
                if (result.Succeeded)
                {
                    // * this is a workaround YCX0 PDC 
                    // * code first versus db first
                    if (vm.LabId.HasValue && vm.LabId.Value <= 0)
                    {
                        vm.LabId = null; 
                    }
                    var labupdate = new KeyValuePair<string, int?>(user.Id, vm.LabId);
                    _dataService.UserLabEdit(new MessageBase<KeyValuePair<string, int?>>() { Data = labupdate });


                    foreach (var role in vm.Roles)
                    {
                        UserManager.AddToRole(user.Id, role);    
                    }

                    if (!string.IsNullOrEmpty(vm.ExternalProvider) && !string.IsNullOrEmpty(vm.ExternalId))
                    {
                        var info = new UserLoginInfo(vm.ExternalProvider, vm.ExternalId);
                        UserManager.AddLogin(user.Id, info);
                    }

                    return RedirectToAction("Index", "CLAWUser");
                }
            }
            var labs = _dataService.GetLabs().Data;
            if (labs == null)
            {
                labs = new List<LabDTO>();
            }
            else
            {
                labs.Insert(0, new LabDTO());
            }
            ViewBag.LabId = new SelectList(labs, "Id", "Name");


            var rolesEF = context.Roles.ToList();
            var rolelist = new MultiSelectList(rolesEF, "Name", "Name");
            ViewBag.Roles = rolelist;
            return View(vm);
        }

        public ActionResult Edit(string Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vm = new CLAWUserVM();
            var dto = _dataService.User(new MessageBase<string>() { Data = Id });
            if (dto.Success)
            {
                var userLogins = UserManager.GetLoginsAsync(Id).Result.AsQueryable<UserLoginInfo>().Select(e => new AspNetUserLoginDTO() { LoginProvider = e.LoginProvider, ProviderKey = e.ProviderKey, UserId = Id }).ToList();
                dto.Data.AspNetUserLogins = userLogins;
                vm = new CLAWUserVM(dto.Data);
                var user = context.Users.Where(u => u.UserName.Equals(dto.Data.Email, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                var userroles = UserManager.GetRoles(user.Id);
                vm.Roles = userroles;
                vm.RolesDisplay = string.Join(",", userroles);
                var rolesEF = context.Roles.ToList();
                foreach (var item in rolesEF)
                {
                    (vm.AllRoles as List<SelectListItem>).Add(new SelectListItem() { Text = item.Name, Value = item.Name, Selected = userroles.Contains(item.Name) });
                }
                ViewBag.AllRoles = vm.AllRoles;
            }
            else
            {
                return HttpNotFound();
            }

            var labs = _dataService.GetLabs().Data;
            if (labs == null)
            {
                labs = new List<LabDTO>();
            }
            else
            {
                labs.Insert(0, new LabDTO());
            }
            var lablist = new SelectList(labs, "Id", "Name", dto.Data.LabId.HasValue ? dto.Data.LabId.Value : 0);
            ViewBag.LabId = lablist;

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] CLAWUserVM vm)
        {
            if (ModelState.IsValid)
            {
                //var user = new ApplicationUser { UserName = vm.Email, Email = vm.Email };
                var user = UserManager.FindById(vm.Id);
                user.Email = vm.Email;
                var result = UserManager.Update(user);

                // * this is a workaround YCX0 PDC 
                // * code first versus db first
                if (vm.LabId.HasValue && vm.LabId.Value <= 0)
                {
                    vm.LabId = null; 
                }
                var labupdate = new KeyValuePair<string, int?>(user.Id, vm.LabId);
                _dataService.UserLabEdit(new MessageBase<KeyValuePair<string, int?>>() { Data = labupdate });

                var disabledupdate = new KeyValuePair<string, DateTime?>(user.Id, vm.DateDisabled);
                _dataService.UserDateDisabledEdit(new MessageBase<KeyValuePair<string, DateTime?>>() { Data = disabledupdate });


                var roleclearresult = UserManager.RemoveFromRoles(vm.Id, UserManager.GetRoles(vm.Id).ToArray());
                var rolesetresult = UserManager.AddToRoles(user.Id, vm.Roles.ToArray());

                // * heavy handed
                var loginlist = UserManager.GetLogins(user.Id);
                foreach (var info in loginlist)
                {
                    var clearlogin = UserManager.RemoveLogin(user.Id, info);
                }
                if (!string.IsNullOrEmpty(vm.ExternalProvider) && !string.IsNullOrEmpty(vm.ExternalId))
                {
                    var info = new UserLoginInfo(vm.ExternalProvider, vm.ExternalId);
                    UserManager.AddLogin(user.Id, info);    
                }

                return RedirectToAction("Index", "CLAWUser");
            }
            var labs = _dataService.GetLabs().Data;
            if (labs == null)
            {
                labs = new List<LabDTO>();
            }
            else
            {
                labs.Insert(0, new LabDTO());
            }
            ViewBag.LabId = new SelectList(labs, "Id", "Name", vm.LabId.HasValue ? vm.LabId.Value : 0);


            var userroles = new List<string>();
            var tempuser = context.Users.Where(u => u.UserName.Equals(vm.Email, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            if (tempuser != null)
            {
                userroles = UserManager.GetRoles(tempuser.Id).ToList();
            }

            //var roles = new List<CLAWRoleVM>();

            var rolesEF = context.Roles.ToList();
            //foreach (var item in rolesEF)
            //{
            //    roles.Add(new CLAWRoleVM(item));
            //}
            ViewBag.Roles = new MultiSelectList(rolesEF, "Name", "Name", userroles);
            return View(vm);
        }

 

        // GET: /Manage/ChangePassword
        [Authorize(Roles = "Admin")]
        public ActionResult ChangeUserPassword(string Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var vm = new ChangeUserPasswordViewModel();
            if (!string.IsNullOrEmpty(Id))
            {
                using (var ctx = new ApplicationDbContext())
                {
                    var userE = ctx.Users.Where(s => s.Id.Equals(Id)).Select(s => new { Id = s.Id, Email = s.Email }).OrderBy(s => s.Email).FirstOrDefault();
                    if (userE != null)
                    {
                        vm.Email = userE.Email;
                        vm.Id = userE.Id;
                    }
                }                
            }
            return View(vm);
        }

        //
        // POST: /Manage/ChangeUserPassword
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeUserPassword(ChangeUserPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            IdentityResult passwordChangeResult = null;

            if (!string.IsNullOrEmpty(model.NewPassword))
            {
                string resetToken = UserManager.GeneratePasswordResetToken(model.Id);
                passwordChangeResult = UserManager.ResetPassword(model.Id, resetToken, model.NewPassword);
            }
            else
            {
                var user = UserManager.FindById(model.Id);
                user.PasswordHash = null;
                UserManager.UpdateSecurityStamp(model.Id);
                passwordChangeResult = UserManager.Update(user);
            }

            if (passwordChangeResult.Succeeded)
            {
                ViewBag.StatusMessage = "User password has been changed.";
                return View(model);
            }
            else
            {
                ViewBag.StatusMessage = passwordChangeResult.Errors.FirstOrDefault();
                return View(model);
            }
        }
    }
}