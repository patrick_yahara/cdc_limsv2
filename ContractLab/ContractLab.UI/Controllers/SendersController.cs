﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContractLab.DataService;
using ContractLab.DataService.Interfaces;
using ContractLab.UI.Models.ContractLab;
using Yahara.ServiceInfrastructure.Messages;
using ContractLab.DTO;
using System.Net;

namespace ContractLab.UI.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SendersController : Controller
    {
        private readonly IContractLabDataService _dataService;
        private const string _fieldlist = @"SenderId, CompanyName, Address, City, State, Country, RasClientId, 
                                            ContactId, FullName, ContactAddress, ContactCity, ContactState, ContactCountry,
                                            Phone, Fax, Email, RasContactId, FluSeason, ContactFluSeason";
     
        public SendersController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }        
        // GET: Senders
        public ActionResult Index(string SortOrder, string IdFilter, int? PageSize, int? page, int? FluSeason)
        {
            int? localFluSeason = FluSeason;
            ViewBag.FluSeason = localFluSeason;
            ViewBag.AllowC = true;

            int? DefaultPageSize = 10;
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            if (PageSize != null)
            {
                DefaultPageSize = PageSize;
            }
            ViewBag.IdFilter = IdFilter;

            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));
            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;

            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("Sender.RasClientId", "Sender.RasClientId");
            sortColumns.Add("Sender.CompanyName", "Sender.CompanyName");
            sortColumns.Add("Sender.State", "Sender.State");
            sortColumns.Add("Contact.FullName", "Contact.FullName");
            sortColumns.Add("Contact.Email", "Contact.Email");
            sortColumns.Add("Contact.Phone", "Contact.Phone");
            sortColumns.Add("Sender.FluSeason", "Sender.FluSeason");


            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(SortOrder) && SortOrder.EndsWith(" DESC"))
            {
                var trimmed = SortOrder.Remove(SortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(SortOrder) && sortColumns.ContainsValue(SortOrder))
                {
                    validSort = true;
                    sortColumns[SortOrder] = string.Concat(SortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;

            var vm = new List<SenderCompositeVM>();
            VirtualPagedList<SenderCompositeVM> vpl;
            var requestdetail = new RequestDetail() { CurrentPage = page ?? 1, ItemsPerPage = DefaultPageSize.Value, OrderBy = "Sender.Id ASC" };
            requestdetail.FluSeason = localFluSeason;
            if (!string.IsNullOrEmpty(IdFilter))
            {
                requestdetail.IdFilter = IdFilter;
            }

            // do the oerder by
            if (validSort)
            {
                requestdetail.OrderBy = SortOrder;
                ViewBag.SortOrder = SortOrder;
            }

            var dto = _dataService.SenderCompositeList(new MessageBase<RequestDetail>() { Data = requestdetail });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var senders = dto.Data as List<SenderCompositeDTO>;
                senders.ForEach(e => vm.Add(new SenderCompositeVM(e)));
            }
            vpl = new VirtualPagedList<SenderCompositeVM>(vm, dto.ItemsPerPage, dto.TotalCount, dto.TotalPages, dto.CurrentPage);
            return View(vpl);
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SenderCompositeVM vm = null;
            var dto = _dataService.SenderComposite(new MessageBase<string>() { Data = id });
            if (dto.Success)
            {
                vm = new SenderCompositeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SenderCompositeVM vm = null;
            var dto = _dataService.SenderComposite(new MessageBase<string>() { Data = id });
            if (dto.Success)
            {
                vm = new SenderCompositeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            _dataService.SenderCompositeDelete(new MessageBase<string>() { Data = id });
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SenderCompositeVM vm = null;
            var dto = _dataService.SenderComposite(new MessageBase<string>() { Data = id });
            if (dto.Success)
            {
                vm = new SenderCompositeVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] SenderCompositeVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new SenderCompositeDTO
                {
                    Sender = new SenderDTO() { 
                        Address = vm.Address,
                        City = vm.City,
                        CompanyName = vm.CompanyName,
                        Country = vm.Country,
                        Id = vm.SenderId,
                        RasClientId = vm.RasClientId,
                        State = vm.State,
                        FluSeason = vm.FluSeason,
                    },
                    Contact = new ContactDTO() { 
                        Address = vm.ContactAddress,
                        City = vm.ContactCity,
                        Country = vm.ContactCountry,
                        Email = vm.Email,
                        Fax = vm.Fax,
                        FullName = vm.FullName,
                        Phone = vm.Phone,
                        RasClientId = vm.RasClientId,
                        RasContactId = vm.RasContactId,
                        State = vm.ContactState,
                        FluSeason = vm.ContactFluSeason,
                    },
                };
                var Sender = _dataService.SenderCompositeEdit(new MessageBase<SenderCompositeDTO>() { Data = dto });
                if (Sender.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        public ActionResult Create()
        {
            SenderCompositeVM vm = null;
            vm = new SenderCompositeVM();
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] SenderCompositeVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new SenderCompositeDTO
                {
                    Sender = new SenderDTO()
                    {
                        Address = vm.Address,
                        City = vm.City,
                        CompanyName = vm.CompanyName,
                        Country = vm.Country,
                        Id = vm.SenderId,
                        RasClientId = vm.RasClientId,
                        State = vm.State,
                    },
                    Contact = new ContactDTO()
                    {
                        Address = vm.ContactAddress,
                        City = vm.ContactCity,
                        Country = vm.ContactCountry,
                        Email = vm.Email,
                        Fax = vm.Fax,
                        FullName = vm.FullName,
                        Phone = vm.Phone,
                        RasClientId = vm.RasClientId,
                        RasContactId = vm.RasContactId,
                        State = vm.ContactState,
                    },
                };
                var Sender = _dataService.SenderCompositeInsert(new MessageBase<SenderCompositeDTO>() { Data = dto });
                if (Sender.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

    }
}