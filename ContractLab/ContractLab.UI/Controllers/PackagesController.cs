﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;
using ContractLab.DataService;
using System.IO;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class PackagesController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public PackagesController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Packages
        public ActionResult Index(string SortOrder, string IdFilter, int? PageSize, int? page, bool newcookie = false)
        {

            string localSortOrder = SortOrder;
            string localIdFilter = IdFilter;
            int? localPageSize = PageSize;
            int? localpage = page;

            if (!newcookie && this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParamsP"))
            {
                var indexparams = this.ControllerContext.HttpContext.Request.Cookies["IndexParamsP"];
                if (!newcookie)
                {
                    newcookie = false;
                    localSortOrder = indexparams["SortOrder"];
                    localIdFilter = indexparams["IdFilter"];
                    int tempInt;
                    if (!string.IsNullOrEmpty(indexparams["PageSize"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["PageSize"], out tempInt))
                        {
                            localPageSize = tempInt;
                        }
                        else
                        {
                            localPageSize = null;
                        }
                    }
                    else
                    {
                        localPageSize = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["page"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["page"], out tempInt))
                        {
                            localpage = tempInt;
                        }
                        else
                        {
                            localpage = null;
                        }
                    }
                    else
                    {
                        localpage = null;
                    }
                }
            }
            if (newcookie || !this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParamsP"))
            {
                var cookie = new HttpCookie("IndexParamsP");
                cookie.Values.Add("SortOrder", SortOrder);
                cookie.Values.Add("IdFilter", IdFilter);
                cookie.Values.Add("PageSize", PageSize.ToString());
                cookie.Values.Add("page", page.ToString());
                Response.Cookies.Add(cookie);
            }

            ViewBag.AllowC = _dataService.UserHasLab().Success;

            int? DefaultPageSize = 10;
            int currentPageIndex = localpage.HasValue ? localpage.Value - 1 : 0;
            if (localPageSize != null)
            {
                DefaultPageSize = localPageSize;
            }
            ViewBag.IdFilter = localIdFilter;

            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));
            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;

            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("Name", "Name");
            sortColumns.Add("Sender.RasClientId", "Sender.RasClientId");
            sortColumns.Add("Sender.CompanyName", "Sender.CompanyName");
            sortColumns.Add("CreatedOn", "CreatedOn");
            sortColumns.Add("IsVisible", "IsVisible");
            sortColumns.Add("SpecimenCount", "SpecimenCount");

            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(localSortOrder) && localSortOrder.EndsWith(" DESC"))
            {
                var trimmed = localSortOrder.Remove(localSortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(localSortOrder) && sortColumns.ContainsValue(localSortOrder))
                {
                    validSort = true;
                    sortColumns[localSortOrder] = string.Concat(SortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;

            var vm = new List<PackageVM>();
            VirtualPagedList<PackageVM> vpl;
            var requestdetail = new RequestDetail() { CurrentPage = localpage ?? 1, ItemsPerPage = DefaultPageSize.Value };
            if (!string.IsNullOrEmpty(localIdFilter))
            {
                requestdetail.IdFilter = localIdFilter;
            }

            // do the oerder by
            if (validSort)
            {
                requestdetail.OrderBy = localSortOrder;
                ViewBag.SortOrder = localSortOrder;
            }

            var dto = _dataService.PackageList(new MessageBase<RequestDetail>() { Data = requestdetail });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                var packages = dto.Data as List<PackageDTO>;
                packages.ForEach(e => vm.Add(new PackageVM(e, uro, uie)));
            }
            vpl = new VirtualPagedList<PackageVM>(vm, dto.ItemsPerPage, dto.TotalCount, dto.TotalPages, dto.CurrentPage);
            return View(vpl);
        }

        // GET: Packages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PackageVM vm = null;
            var dto = _dataService.Package(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                if (TempData["SpecimenCount"] != null)
                {
                    int count = 0;
                    if (Int32.TryParse(TempData["SpecimenCount"].ToString(), out count))
                    {
                        ViewBag.DialogTitle = "Import Successful";
                        ViewBag.DialogBody = String.Format("You have successfully uploaded {0} samples to package {1}", count, dto.Data.Name);
                        ViewBag.DialogSuccess = "Close";
                    }
                }

                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new PackageVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Packages/Create
        public ActionResult Create()
        {

            PopulateCombos();

            var vm = new PackageVM();
            ViewData.Model = vm;
            return View();
        }

        private void PopulateCombos()
        {
            #region state
            var states = _dataService.GetStates().Data;
            if (states == null)
            {
                states = new List<StateDTO>();
                states.WithBlank();
            }
            ViewBag.State = new SelectList(states, "Abbreviation", "Abbreviation");
            #endregion

            var senders = new List<SenderDTO>();
            ViewBag.SenderId = new SelectList(senders, "Id", "RasClientId");

            #region SpecimenCondition
            var plSC = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SHIP_COND" }).Data;
            if (plSC != null)
            {
                ViewBag.SpecimenCondition = new SelectList(plSC, "Name", "Name");
            }
            else { ViewBag.SpecimenCondition = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion	
        }

        // POST: Packages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, SenderId, IsVisible, DateReceived, SpecimenCondition, Comments")] PackageVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new PackageDTO { 
                    Name = vm.Name, 
                    SenderId = vm.SenderId, 
                    IsVisible = vm.IsVisible, 
                    Comments = vm.Comments,
                    DateReceived = vm.DateReceived,
                    SpecimenCondition = vm.SpecimenCondition,
                };
                var Package = _dataService.PackageInsert(new MessageBase<PackageDTO>() { Data = dto });
                if (Package.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            PopulateCombos();
            return View(vm);
        }

        // GET: Packages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PackageVM vm = null;
            var dto = _dataService.Package(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                PopulateCombos(dto.Data);
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new PackageVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        private void PopulateCombos(PackageDTO dto)
        {
            #region state
            var states = _dataService.GetStates().Data;
            if (states == null)
            {
                states = new List<StateDTO>();
                states.WithBlank();
            }
            ViewBag.State = new SelectList(states, "Abbreviation", "Abbreviation", dto.Sender.State);
            #endregion

            var senders = _dataService.GetSenders(new MessageBase<SenderDTO>() { Data = dto.Sender }).Data;
            if (senders == null)
            {
                senders = new List<SenderDTO>();
                senders.Add(new SenderDTO());
            }
            var data = senders.Select(e => new
            {
                Id = e.Id,
                Address = e.Address,
                City = e.City,
                CompanyName = e.CompanyName,
                Country = e.Country,
                RasClientId = e.RasClientId,
                State = e.State,
                Desc = String.Format(@"{0}<br/>{1}<br/>{2}", e.Address, e.City, e.State),
            }).ToList();

            ViewBag.SenderId = new SelectList(data, "Id", "RasClientId", dto.Sender.Id);

            #region SpecimenCondition
            var plSC = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SHIP_COND" }).Data;
            if (plSC != null)
            {
                ViewBag.SpecimenCondition = new SelectList(plSC, "Name", "Name", dto.SpecimenCondition);
            }
            else { ViewBag.SpecimenCondition = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", dto.SpecimenCondition); }
            #endregion
        }

        // POST: Packages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, SenderId, IsVisible, DateReceived, SpecimenCondition, Comments")] PackageVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new PackageDTO { 
                    Id = vm.Id, 
                    Name = vm.Name, 
                    SenderId = vm.SenderId, 
                    IsVisible = vm.IsVisible,
                    Comments = vm.Comments,
                    DateReceived = vm.DateReceived,
                    SpecimenCondition = vm.SpecimenCondition,
                };
                var Package = _dataService.PackageEdit(new MessageBase<PackageDTO>() { Data = dto });
                if (Package.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    var dtoE = _dataService.Package(new MessageBase<int>() { Data = vm.Id.Value });
                    PopulateCombos(dtoE.Data); 
                }
            }
            return View(vm);
        }

        // GET: Packages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PackageVM vm = null;
            var dto = _dataService.Package(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new PackageVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Packages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.PackageDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetSenders(String abbrev)
        {
            #region senders
            var senders = _dataService.GetSenders(new MessageBase<string>() { Data = abbrev }).Data;
            if (senders == null)
            {
                senders = new List<SenderDTO>();
                senders.Add(new SenderDTO());
            }
            var data = senders.Select(e => new {
                Id = e.Id,
                Address = e.Address,
                City = e.City,
                CompanyName = e.CompanyName,
                Country = e.Country,
                RasClientId = e.RasClientId,
                State = e.State,
                Desc = String.Format(@"{0}<br/>{1}<br/>{2}", e.Address, e.City, e.State),
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
            #endregion
        }

        [HttpGet]
        public ActionResult SRV(int? id)
        {
            var invalids = System.IO.Path.GetInvalidFileNameChars();
            SRVMetadataVM vm = null;
            if (id != null)
            {
                var dto = _dataService.Package(new MessageBase<int>() { Data = id.Value });
                if (dto.Success)
                {
                    vm = new SRVMetadataVM(dto.Data);

                                        //{
                        ViewData.Model = vm;
                        return View("_SRV", vm);

                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}