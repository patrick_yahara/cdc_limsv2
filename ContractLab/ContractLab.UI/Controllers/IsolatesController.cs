﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class IsolatesController : Controller
    {
        private const string _fieldlist = @"Id, PassageLevel, DateHarvested, Titer, RedBloodCellType, SpecimenId, IsolateTypeId, CSID, Comments, StatusId";

        private readonly IContractLabDataService  _dataService;
        public IsolatesController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Isolates
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
         
            var uro = _dataService.UserReadOnly().Success;
            var uie = _dataService.UserEditor().Success;

            ViewBag.AllowC = _dataService.UserHasLab().Success;

            ViewBag.SpecimenId = id.Value;
            var vm = new List<IsolateVM>();
            var dto = _dataService.IsolateList(new MessageBase<int>() { Data = id.Value});
            if (dto.Success)
            {
                var Isolates = dto.Data as List<IsolateDTO>;
                var selectedList = (TempData["SelectedItems"] as List<int>);
                if (selectedList == null)
                {
                    Isolates.ForEach(e => vm.Add(new IsolateVM(e, uro, uie)));
                }
                else
                {
                    Isolates.ForEach(e => { var tempSVM = new IsolateVM(e, uro, uie); tempSVM.Selected = selectedList.Contains(e.Id); vm.Add(tempSVM); });
                }
                if (TempData.ContainsKey("DialogTitle"))
                {
                    ViewBag.DialogTitle = TempData["DialogTitle"];
                }
                if (TempData.ContainsKey("DialogBody"))
                {
                    ViewBag.DialogBody = TempData["DialogBody"];
                }
                if (TempData.ContainsKey("DialogSuccess"))
                {
                    ViewBag.DialogSuccess = TempData["DialogSuccess"];
                }               
            }
            return View(vm);
        }

        // GET: Isolates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IsolateVM vm = null;
            var dto = _dataService.Isolate(new MessageBase<int>(){ Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new IsolateVM(dto.Data, uro, uie);
            } else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Isolates/Create
        public ActionResult Create(int? id)
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            GetCreateRequiredData();

            var spec = _dataService.Specimen(new MessageBase<int>() { Data = id.Value });
            var csid = string.Empty;
            var subtype = string.Empty;
            if (spec.Success)
            {
                csid = spec.Data.CSID;
                subtype = spec.Data.SubType;
            }
            var vm = new IsolateVM() { SpecimenId = id.Value, CSID = csid, SubType = subtype };
            ViewData.Model = vm;
            return View();
        }

        private void GetCreateRequiredData()
        {
            var rbctList = new List<NameValueDTO>();
            rbctList.Add(new NameValueDTO());
            rbctList.Add(new NameValueDTO() { Name = "TRBC", Value = "TRBC" });
            rbctList.Add(new NameValueDTO() { Name = "GPRBC", Value = "GPRBC" });
            rbctList.Add(new NameValueDTO() { Name = "GPRBC-OSEL", Value = "GPRBC-OSEL" });

            ViewBag.RedBloodCellType = new SelectList(rbctList, "Value", "Value");

            var isolatetypes = _dataService.IsolateTypeList().Data;
            if (isolatetypes == null)
            {
                isolatetypes = new List<IsolateTypeDTO>();
            }
            else
            {
                isolatetypes = isolatetypes.Where(s => s.IsVisible).ToList();
            }
            ViewBag.IsolateTypeId = new SelectList(isolatetypes, "Id", "DisplayName", isolatetypes.Where(s => s.SysName.Equals("GROWN")).FirstOrDefault().Id);

            //var statusList = _dataService.StatusList().Data;
            //if (statusList == null)
            //{
            //    statusList = new List<NameValueDTO>();
            //}
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name");

            var statusChangeList = _dataService.StatusDTOList();
            if (statusChangeList.Success)
            {
                var tempList = statusChangeList.Data;
                if (!User.IsInRole("Admin"))
                {
                    var tempItem = tempList.FirstOrDefault(s => s.SysName == "SUBMITTED");
                    if (tempItem != null)
                    {
                        tempList.Remove(tempItem);
                    }
                }
                ViewBag.StatusId = new SelectList(statusChangeList.Data, "Id", "DisplayName", tempList[0].Id);
            }
            
        }

        // POST: Isolates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] IsolateVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new IsolateDTO {
                    PassageLevel = vm.PassageLevel,
                    DateHarvested = vm.DateHarvested,
                    Titer = vm.Titer,
                    RedBloodCellType = vm.RedBloodCellType,
                    SpecimenId = vm.SpecimenId, 
                    IsolateTypeId = vm.IsolateTypeId,
                    Comments = vm.Comments,
                    StatusId = vm.StatusId.HasValue && vm.StatusId > 0 ? vm.StatusId : null,
                };
                var Isolate = _dataService.IsolateInsert(new MessageBase<IsolateDTO>() { Data = dto });
                if (Isolate.Success)
                {
                    return RedirectToAction("Index", new { id = vm.SpecimenId });
                }
                else
                {
                    ModelState.AddModelError("", Isolate.GetDetailedErrorString());
                }
            }
            GetCreateRequiredData();
            var spec = _dataService.Specimen(new MessageBase<int>() { Data = vm.SpecimenId });
            var csid = string.Empty;
            if (spec.Success)
            {
                csid = spec.Data.CSID;
                vm.CSID = csid;
            }
            return View(vm);
        }

        // GET: Specimens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            IsolateVM vm = null;
            var dto = _dataService.Isolate(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {

                if (dto.Data.HasRCVAliquot)
                {
                    ViewBag.DialogTitle = "Edit Subsample";
                    ViewBag.DialogBody = String.Format("Warning: Do not edit an original subsample to add GROWN, ITT or VNR data. Add a new subsamples to add these data.");
                    ViewBag.DialogSuccess = "Close";
                }

                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new IsolateVM(dto.Data, uro, uie);
                GetCreateRequiredData(vm);

            }
            else
            {
                return HttpNotFound();
            }

            return View(vm);
        }

        private void GetCreateRequiredData(IsolateVM vm)
        {
            var rbctList = new List<NameValueDTO>();
            rbctList.Add(new NameValueDTO());
            rbctList.Add(new NameValueDTO() { Name = "TRBC", Value = "TRBC" });
            rbctList.Add(new NameValueDTO() { Name = "GPRBC", Value = "GPRBC" });
            rbctList.Add(new NameValueDTO() { Name = "GPRBC-OSEL", Value = "GPRBC-OSEL" });

            ViewBag.RedBloodCellType = new SelectList(rbctList, "Value", "Value", vm.RedBloodCellType);

            var isolatetypes = _dataService.IsolateTypeList().Data;
            if (isolatetypes == null)
            {
                isolatetypes = new List<IsolateTypeDTO>();
            }
            else
            {
                isolatetypes = isolatetypes.Where(s => s.IsVisible || s.Id == vm.IsolateTypeId).ToList();
            }
            ViewBag.IsolateTypeId = new SelectList(isolatetypes, "Id", "DisplayName", vm.IsolateTypeId);

            //var statusList = _dataService.StatusList().Data;
            //if (statusList == null)
            //{
            //    statusList = new List<NameValueDTO>();
            //}
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name", vm.StatusId);

            var statusChangeList = _dataService.StatusDTOList();
            if (statusChangeList.Success)
            {
                var tempList = statusChangeList.Data;
                if (!User.IsInRole("Admin"))
                {
                    var tempItem = tempList.FirstOrDefault(s => s.SysName == "SUBMITTED");
                    if (tempItem != null)
                    {
                        tempList.Remove(tempItem);
                    }
                }
                ViewBag.StatusId = new SelectList(statusChangeList.Data, "Id", "DisplayName", vm.StatusId);
            }
        }

        // POST: Specimens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] IsolateVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new IsolateDTO { 
                    Id = vm.Id,
                    PassageLevel = vm.PassageLevel,
                    DateHarvested = vm.DateHarvested,
                    Titer = vm.Titer,
                    RedBloodCellType = vm.RedBloodCellType,
                    SpecimenId = vm.SpecimenId, 
                    IsolateTypeId = vm.IsolateTypeId,
                    Comments = vm.Comments,
                    StatusId = vm.StatusId.HasValue && vm.StatusId > 0 ? vm.StatusId : null,
                };
                var Isolate = _dataService.IsolateEdit(new MessageBase<IsolateDTO>() { Data = dto });
                if (Isolate.Success)
                {
                    return RedirectToAction("Index", new { id = vm.SpecimenId });
                }
                else
                {
                    // * TODO: add error message here
                    ModelState.AddModelError("", Isolate.GetDetailedErrorString());
                    GetCreateRequiredData(vm);
                    var spec = _dataService.Specimen(new MessageBase<int>() { Data = vm.SpecimenId });
                    var csid = string.Empty;
                    if (spec.Success)
                    {
                        csid = spec.Data.CSID;
                        vm.CSID = csid;
                    }
                }
            }
            return View(vm);
        }

        // GET: Isolates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IsolateVM vm = null;
            var dto = _dataService.Isolate(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new IsolateVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Isolates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = _fieldlist)] IsolateVM vm)
        {
            var result = _dataService.IsolateDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index", new { id = vm.SpecimenId });
            }
            else
            {
                vm = null;
                var dto = _dataService.Isolate(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    var uro = _dataService.UserReadOnly().Success;
                    var uie = _dataService.UserEditor().Success;
                    vm = new IsolateVM(dto.Data, uro, uie);
                }
                else
                {
                    ModelState.AddModelError("", dto.GetDetailedErrorString());
                }
                // * TODO: Create error message and display
                return View(vm);
            }
            
        }

        [HttpPost]
        public ActionResult MultiAction(MultiActionVM vm)
        {
            var items = vm.Items.Where(s => s.Selected).Select(s => s.Id).ToList();
            if (items.Count > 0)
            {
                if (Request.Form["submitDLSTrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateSpecimenTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Isolates" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index", new { id = vm.ParentId });
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLATrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateAliquotTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Isolates" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index", new { id = vm.ParentId });
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitPL"] != null)
                {
                    var labels = _dataService.GenerateLabels(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "isolate" }, new MessageBase<int>() { Data = 1 });
                    if (labels.Success)
                    {
                        return Json(labels.Data);
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitPLP"] != null)
                {
                    var labels = _dataService.GenerateLabels_Page(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "isolate" }, new MessageBase<int>() { Data = 1 });
                    if (labels.Success)
                    {
                        return Content(labels.Data, "text/plain");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }
            }
            else
            {
                if (Request.Form["submitPLP"] != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            return RedirectToAction("Index", new { id = vm.ParentId });
        }

    }
}