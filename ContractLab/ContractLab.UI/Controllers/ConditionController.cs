﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    public class ConditionController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public ConditionController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Condition
        public ActionResult Index(int? id)
        {
            ViewBag.CategoryId = id;
            var vm = new List<ConditionVM>();
            var dto = _dataService.ConditionList(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                foreach (var item in dto.Data)
	            {
                    vm.Add(new ConditionVM(item));
	            }                
            }
            return View(vm);
        }

        // GET: Aliquots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConditionVM vm = null;
            var dto = _dataService.Condition(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new ConditionVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Aliquots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = "Id, CategoryId")] ConditionVM vm)
        {
            var result = _dataService.ConditionDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index", new { id = vm.CategoryId });
            }
            else
            {
                vm = null;
                var dto = _dataService.Condition(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    vm = new ConditionVM(dto.Data);
                }
                else
                {
                    return HttpNotFound();
                }
                return View(vm);
            }
        }

        // GET: Condition/Create
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vm = new ConditionVM() { CategoryId = id.Value };
            var category = _dataService.Category(new MessageBase<int>() { Data = id.Value });
            if (category.Success)
            {
                vm.CategoryName = category.Data.Name;
                vm.LitsQuestionId = category.Data.LitsQuestionId;
                vm.Status = "Active";
            }
            return View(vm);
        }

        // POST: Condition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, CategoryId, LitsQuestionId, Sorter, Status, CategoryName")] ConditionVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ConditionDTO { Name = vm.Name, CategoryId = vm.CategoryId, Description = vm.Name, LitsQuestionId = vm.LitsQuestionId, Sorter = vm.Sorter, Status = vm.Status };
                var newcondition = _dataService.ConditionInsert(new MessageBase<ConditionDTO>() { Data = dto });
                if (newcondition.Success)
                {
                    return RedirectToAction("Index", new { id = vm.CategoryId });
                }
                else
                {
                    ModelState.AddModelError("", newcondition.GetDetailedErrorString());
                }
            }
            return View(vm);
        }

        // GET: Condition/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConditionVM vm = null;
            var dto = _dataService.Condition(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new ConditionVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Condition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, CategoryId, LitsQuestionId, Sorter, Status, CategoryName")] ConditionVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ConditionDTO { Id = vm.Id, Name = vm.Name, CategoryId = vm.CategoryId, Description = vm.Name, LitsQuestionId = vm.LitsQuestionId, Sorter = vm.Sorter, Status = vm.Status };
                var newcondition = _dataService.ConditionEdit(new MessageBase<ConditionDTO>() { Data = dto });
                if (newcondition.Success)
                {
                    return RedirectToAction("Index", new { id = vm.CategoryId });
                }
                else
                {
                    ModelState.AddModelError("", newcondition.GetDetailedErrorString());
                }
            }
            return View(vm);
        }

    }
}