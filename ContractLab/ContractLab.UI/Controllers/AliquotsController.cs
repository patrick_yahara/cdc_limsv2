﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;
using System.Text.RegularExpressions;
using ContractLab.DataService.Interfaces;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class AliquotsController : Controller
    {
        private const string _fieldlist = @"CUID, Id, TestCode, IsolateId, ManifestId, IsStoredLocally, StatusId";

        private readonly IContractLabDataService _dataService;
        public AliquotsController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Aliquotsq
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.AllowC = _dataService.UserHasLab().Success;

            if (ViewBag.AllowC)
            {
                var manifestList = _dataService.ManifestList().Data;
                if (manifestList == null)
                {
                    manifestList = new List<ManifestDTO>();
                }
                // CLAW-91
                manifestList = manifestList.Where(s => s.IsVisible).OrderByDescending(s => s.Id).ToList();
                ViewBag.ManifestId = new SelectList(manifestList, "Id", "ComboDisplay");
            }

            ViewBag.IsolateId = id.Value;
            var vm = new List<AliquotVM>();
            var dto = _dataService.AliquotList(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                var Aliquots = dto.Data as List<AliquotDTO>;


                var selectedList = (TempData["SelectedItems"] as List<int>);
                if (selectedList == null)
                {
                    Aliquots.ForEach(e => vm.Add(new AliquotVM(e, uro, uie)));
                }
                else
                {
                    Aliquots.ForEach(e => { var tempSVM = new AliquotVM(e, uro, uie); tempSVM.Selected = selectedList.Contains(e.Id); vm.Add(tempSVM); });
                }
                if (TempData.ContainsKey("DialogTitle"))
                {
                    ViewBag.DialogTitle = TempData["DialogTitle"];
                }
                if (TempData.ContainsKey("DialogBody"))
                {
                    ViewBag.DialogBody = TempData["DialogBody"];
                }
                if (TempData.ContainsKey("DialogSuccess"))
                {
                    ViewBag.DialogSuccess = TempData["DialogSuccess"];
                }    
                
            }
            return View(vm);
        }

        // GET: Aliquots/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AliquotVM vm = null;
            var dto = _dataService.Aliquot(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new AliquotVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Aliquots/Create
        public ActionResult Create(int? id)
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PopulateLists();
            
            var iso = _dataService.Isolate(new MessageBase<int>() { Data = id.Value });
            var csid = string.Empty;
            var subtype = string.Empty;
            if (iso.Success)
            {
                csid = iso.Data.Specimen.CSID;
                subtype = iso.Data.Specimen.SubType;
            }

            bool sl = false;
            if (ViewBag.LocalTestCodes != null && ViewBag.TestCode != null)
            {
                string ltc = ViewBag.LocalTestCodes.ToString();
                var ltcA = ltc.Split(',');
                var tcSL = (ViewBag.TestCode as SelectList);
                if (tcSL != null && tcSL.Count() > 0)
                {
                    sl = ltcA.Contains(tcSL.FirstOrDefault().Value);
                }
            }
            var vm = new AliquotVM() { IsolateId = id.Value, CSID = csid, SubType = subtype, IsStoredLocally = sl };
            ViewData.Model = vm;
            return View();
        }

        private void PopulateLists()
        {
            var testcodes = _dataService.UserHasLab();
            if (testcodes.Data != null)
            {
                ViewBag.LocalTestCodes = testcodes.Data.StoredLocallyCodes != null ? string.Join("", testcodes.Data.StoredLocallyCodes.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)) : String.Empty;
            } 

            var testCodeList = _dataService.TestCodeList();
            if (testCodeList.Success)
            {
                ViewBag.TestCode = new SelectList(testCodeList.Data, "Value", "Value");
            }
            else
            {
                ViewBag.TestCode = new SelectList(new List<NameValueDTO>(), "Value", "Value");
            }

            var manifestList = _dataService.ManifestList().Data;
            if (manifestList == null)
            {
                manifestList = new List<ManifestDTO>();
            }
            // CLAW-91
            manifestList = manifestList.Where(s => s.IsVisible).OrderByDescending(s => s.Id).ToList();
            ViewBag.ManifestId = new SelectList(manifestList, "Id", "ComboDisplay");

            //var statusList = _dataService.StatusList().Data;
            //if (statusList == null)
            //{
            //    statusList = new List<NameValueDTO>();
            //}
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name");

            var statusChangeList = _dataService.StatusDTOList();
            if (statusChangeList.Success)
            {
                var tempList = statusChangeList.Data;
                if (!User.IsInRole("Admin"))
                {
                    var tempItem = tempList.FirstOrDefault(s => s.SysName == "SUBMITTED");
                    if (tempItem != null)
                    {
                        tempList.Remove(tempItem);
                    }
                }
                ViewBag.StatusId = new SelectList(statusChangeList.Data, "Id", "DisplayName", tempList[0].Id);
            }

        }

        // POST: Aliquots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] AliquotVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new AliquotDTO
                {
                    CUID = vm.CUID,
                    TestCode = vm.TestCode,
                    IsolateId = vm.IsolateId,
                    ManifestId = vm.ManifestId,
                    IsStoredLocally = vm.IsStoredLocally,
                    StatusId = vm.StatusId.HasValue && vm.StatusId > 0 ? vm.StatusId : null,
                };
                var Aliquot = _dataService.AliquotInsert(new MessageBase<AliquotDTO>() { Data = dto });
                if (Aliquot.Success)
                {
                    return RedirectToAction("Index", new { id = vm.IsolateId });
                }
                else
                {
                    ModelState.AddModelError("", Aliquot.GetDetailedErrorString());
                }
            }
            PopulateLists();

            var iso = _dataService.Isolate(new MessageBase<int>() { Data = vm.IsolateId });
            var csid = string.Empty;
            var subtype = string.Empty;
            if (iso.Success)
            {
                csid = iso.Data.Specimen.CSID;
                subtype = iso.Data.Specimen.SubType;
            }
            vm.CSID = csid;
            vm.SubType = subtype;

            return View(vm);
        }

        // GET: Specimens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AliquotVM vm = null;
            var dto = _dataService.Aliquot(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                ViewBag.AllowC = _dataService.UserHasLab().Success;
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new AliquotVM(dto.Data, uro, uie);
                PopulateLists(vm);
            }
            else
            {
                return HttpNotFound();
            }

            return View(vm);
        }

        private void PopulateLists(AliquotVM vm)
        {

            var testcodes = _dataService.UserHasLab();
            if (testcodes.Data != null)
            {
                ViewBag.LocalTestCodes = testcodes.Data.StoredLocallyCodes != null ? string.Join("", testcodes.Data.StoredLocallyCodes.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)) : String.Empty;
            } 

            var testCodeList = _dataService.TestCodeList();
            if (testCodeList.Success)
            {
                ViewBag.TestCode = new SelectList(testCodeList.Data, "Value", "Value", vm.TestCode);
            }
            else
            {
                ViewBag.TestCode = new SelectList(new List<NameValueDTO>(), "Value", "Value", vm.TestCode);
            }

            var manifestList = _dataService.ManifestList().Data;
            if (manifestList == null)
            {
                manifestList = new List<ManifestDTO>();
            }
            // CLAW-91
            manifestList = manifestList.Where(s => s.IsVisible || s.Id.Equals(vm.ManifestId)).OrderByDescending(s => s.Id).ToList();
            ViewBag.ManifestId = new SelectList(manifestList, "Id", "ComboDisplay", vm.ManifestId);

            //var statusList = _dataService.StatusList().Data;
            //if (statusList == null)
            //{
            //    statusList = new List<NameValueDTO>();
            //}
            //ViewBag.StatusId = new SelectList(statusList, "Value", "Name", vm.StatusId);

            var statusChangeList = _dataService.StatusDTOList();
            if (statusChangeList.Success)
            {
                var tempList = statusChangeList.Data;
                if (!User.IsInRole("Admin"))
                {
                    var tempItem = tempList.FirstOrDefault(s => s.SysName == "SUBMITTED");
                    if (tempItem != null)
                    {
                        tempList.Remove(tempItem);
                    }
                }
                ViewBag.StatusId = new SelectList(statusChangeList.Data, "Id", "DisplayName", vm.StatusId);
            }

        }

        // POST: Specimens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] AliquotVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new AliquotDTO
                {
                    Id = vm.Id,
                    CUID = vm.CUID,
                    TestCode = vm.TestCode,
                    IsolateId = vm.IsolateId,
                    ManifestId = vm.ManifestId,
                    IsStoredLocally = vm.IsStoredLocally,
                    StatusId = vm.StatusId.HasValue && vm.StatusId > 0 ? vm.StatusId : null,
                };
                var Aliquot = _dataService.AliquotEdit(new MessageBase<AliquotDTO>() { Data = dto });
                if (Aliquot.Success)
                {
                    return RedirectToAction("Index", new { id = vm.IsolateId });
                }
                else
                {
                    ModelState.AddModelError("", Aliquot.GetDetailedErrorString());
                }
            }
            PopulateLists(vm);
            var iso = _dataService.Isolate(new MessageBase<int>() { Data = vm.IsolateId });
            var csid = string.Empty;
            var subtype = string.Empty;
            if (iso.Success)
            {
                csid = iso.Data.Specimen.CSID;
                subtype = iso.Data.Specimen.SubType;
            }
            vm.CSID = csid;
            vm.SubType = subtype;
            ViewBag.AllowC = _dataService.UserHasLab().Success;
            return View(vm);
        }

        // GET: Aliquots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AliquotVM vm = null;
            var dto = _dataService.Aliquot(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new AliquotVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Aliquots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = "IsolateId")] AliquotVM vm)
        {
            var result = _dataService.AliquotDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index", new { id = vm.IsolateId });
            }
            else
            {
                vm = null;
                var dto = _dataService.Aliquot(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    var uro = _dataService.UserReadOnly().Success;
                    var uie = _dataService.UserEditor().Success;
                    vm = new AliquotVM(dto.Data, uro, uie);
                }
                else
                {
                    ModelState.AddModelError("", dto.GetDetailedErrorString());
                }
                return View(vm);
            }
        }

        [HttpPost]
        public ActionResult CreateMultiple([Bind(Include = "IsolateId, ManifestId")] AliquotVM vm)
        {
            if (vm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Request.Form["GROWN"] != null)
            { 
                var result = _dataService.AliquotInsertInitial(new MessageBase<Tuple<int, int?>>() { Data = new Tuple<int, int?>(vm.IsolateId, vm.ManifestId) });
                if (result.Success)
                {
                    return RedirectToAction("Index", new { id = vm.IsolateId });
                }
            }
            if (Request.Form["ITT"] != null)
            {
                var result = _dataService.AliquotInsertInitialITT(new MessageBase<Tuple<int, int?>>() { Data = new Tuple<int, int?>(vm.IsolateId, vm.ManifestId) });
                if (result.Success)
                {
                    return RedirectToAction("Index", new { id = vm.IsolateId });
                }
            }
            return RedirectToAction("Index", new { id = vm.IsolateId });
        }

        [HttpPost]
        public ActionResult MultiAction(MultiActionVM vm)
        {
            var items = vm.Items.Where(s => s.Selected).Select(s => s.Id).ToList();
            if (items.Count > 0)
            {
                if (Request.Form["submitDLSTrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateSpecimenTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Aliquots" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index", new { id = vm.ParentId });
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLATrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateAliquotTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Aliquots" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index", new { id = vm.ParentId });
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitPL"] != null)
                {
                    var labels = _dataService.GenerateLabels(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "aliquot" }, new MessageBase<int>() { Data = 1 });
                    if (labels.Success)
                    {
                        return Json(labels.Data);
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitPLP"] != null)
                {
                    var labels = _dataService.GenerateLabels_Page(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "aliquot" }, new MessageBase<int>() { Data = 1 });
                    if (labels.Success)
                    {
                        return Content(labels.Data, "text/plain");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }
            }
            else
            {
                if (Request.Form["submitPLP"] != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return RedirectToAction("Index", new { id = vm.ParentId });
        }
    }
}