﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class FiltersController : Controller
    {
        private const string _fieldlist = @"Display, Id, FilterText, BaseObject, IsLabAvailable";

        private readonly IContractLabDataService _dataService;
        public FiltersController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Filters
        public ActionResult Index()
        {
            var vm = new List<FilterVM>();
            var dto = _dataService.FilterList();
            if (dto.Success)
            {
                var Filters = dto.Data as List<FilterDTO>;
                Filters.ForEach(e => vm.Add(new FilterVM(e)));
            }
            return View(vm);
        }

        // GET: Filters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilterVM vm = null;
            var dto = _dataService.Filter(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new FilterVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Filters/Create
        public ActionResult Create()
        {
            var baseObjectList = new List<NameValueDTO>();
            baseObjectList.Add(new NameValueDTO());
            baseObjectList.Add(new NameValueDTO() { Name = "Specimen", Value = "Specimen" });
            baseObjectList.Add(new NameValueDTO() { Name = "Package", Value = "Package" });
            ViewBag.BaseObject = new SelectList(baseObjectList, "Value", "Value");
            var vm = new FilterVM() ;
            ViewData.Model = vm;
            return View();
        }

        // POST: Filters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] FilterVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new FilterDTO
                {
                    BaseObject = vm.BaseObject,
                    Display = vm.Display,
                    FilterText = vm.FilterText,
                    Id = vm.Id,
                    IsLabAvailable = vm.IsLabAvailable,
                };
                var Filter = _dataService.FilterInsert(new MessageBase<FilterDTO>() { Data = dto });
                if (Filter.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Specimens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            FilterVM vm = null;
            var dto = _dataService.Filter(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new FilterVM(dto.Data);
                var baseObjectList = new List<NameValueDTO>();
                baseObjectList.Add(new NameValueDTO());
                baseObjectList.Add(new NameValueDTO() { Name = "Specimen", Value = "Specimen" });
                baseObjectList.Add(new NameValueDTO() { Name = "Package", Value = "Package" });
                ViewBag.BaseObject = new SelectList(baseObjectList, "Value", "Value", vm.BaseObject);
            }
            else
            {
                return HttpNotFound();
            }

            return View(vm);
        }

        // POST: Specimens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] FilterVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new FilterDTO
                {
                    BaseObject = vm.BaseObject,
                    Display = vm.Display,
                    FilterText = vm.FilterText,
                    Id = vm.Id,
                    IsLabAvailable = vm.IsLabAvailable,
                };
                var Filter = _dataService.FilterEdit(new MessageBase<FilterDTO>() { Data = dto });
                if (Filter.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Filters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilterVM vm = null;
            var dto = _dataService.Filter(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new FilterVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Filters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = _fieldlist)] FilterVM vm)
        {
            _dataService.FilterDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

    }
}