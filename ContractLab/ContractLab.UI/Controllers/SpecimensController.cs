﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;
using ContractLab.DataService;
using MvcPaging;
using System.Globalization;

namespace ContractLab.UI.Controllers
{
    public class SpecimensController : Controller
    {
        private const string _fieldlist = @"Id, CSID, CUID, ProjectId, PackageId, CountryId, 
        StateId, CountyId, Activity, DateCollected, SpecimenId, PatientAge, PatientAgeUnits, PatientSex,
        PassageHistory, SubType, SpecimenSource, CompCSID1, CompCSID2,
        OtherLocation,SenderId,DrugResistant,Deceased,NursingHome,Vaccinated,Travel,ReasonForSubmission,Comments, 
        StatusId, SampleClass, DateInoculated, SpecialStudy,TravelCountryName,TravelStateName,TravelCountyName,TravelCity,
        HostCellLine,Substrate,PassageNumber,PCRInfA,PCRpdmInfA,
        PCRpdmH1,PCRH3,PCRInfB,PCRBVic,PCRBYam,City, TravelReturnDate, WHOSubmittedSubtype";

        private readonly IContractLabDataService  _dataService;
        public SpecimensController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Specimens
        [Authorize]
        public ActionResult Index(string SortOrder, string IdFilter, int? StatusFilterIndex, int? FilterIndex, int? PageSize, int? page, string From, string To, string CUID, int? FluSeason, bool newcookie = false)
        {
            string localSortOrder = SortOrder;
            string localIdFilter = IdFilter;
            int? localStatusFilterIndex = StatusFilterIndex;
            int? localFilterIndex = FilterIndex;
            int? localPageSize = PageSize;
            int? localpage = page;
            int? localFluSeason = FluSeason;
            string localFrom = From;
            string localTo = To;
            string localCUID = CUID;
            if (!newcookie && this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParams"))
            {
                var indexparams = this.ControllerContext.HttpContext.Request.Cookies["IndexParams"];
                if (!newcookie)
                {
                    newcookie = false;
                    localSortOrder = indexparams["SortOrder"];
                    localIdFilter = indexparams["IdFilter"];
                    localFrom = indexparams["From"];
                    localTo = indexparams["To"];
                    localCUID = indexparams["CUID"];
                    int tempInt;
                    if (!string.IsNullOrEmpty(indexparams["StatusFilterIndex"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["StatusFilterIndex"], out tempInt))
                        {
                            localStatusFilterIndex = tempInt;
                        }
                        else
                        {
                            localStatusFilterIndex = null;
                        }
                    }
                    else
                    {
                        localStatusFilterIndex = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["FilterIndex"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["FilterIndex"], out tempInt))
                        {
                            localFilterIndex = tempInt;
                        }
                        else
                        {
                            localFilterIndex = null;
                        }
                    }
                    else
                    {
                        localFilterIndex = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["PageSize"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["PageSize"], out tempInt))
                        {
                            localPageSize = tempInt;
                        }
                        else
                        {
                            localPageSize = null;
                        }
                    }
                    else
                    {
                        localPageSize = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["page"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["page"], out tempInt))
                        {
                            localpage = tempInt;
                        }
                        else
                        {
                            localpage = null;
                        }
                    }
                    else
                    {
                        localpage = null;
                    }
                    if (!string.IsNullOrEmpty(indexparams["FluSeason"]))
                    {
                        tempInt = 0;
                        if (int.TryParse(indexparams["FluSeason"], out tempInt))
                        {
                            localFluSeason = tempInt;
                        }
                        else
                        {
                            localFluSeason = null;
                        }
                    }
                    else
                    {
                        localFluSeason = null;
                    }
                }
            }
            if (newcookie || !this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("IndexParams"))
            {
                var cookie = new HttpCookie("IndexParams");
                cookie.Values.Add("SortOrder", SortOrder);
                cookie.Values.Add("IdFilter", IdFilter);
                cookie.Values.Add("StatusFilterIndex", StatusFilterIndex.ToString());
                cookie.Values.Add("FilterIndex", FilterIndex.ToString());
                cookie.Values.Add("PageSize", PageSize.ToString());
                cookie.Values.Add("page", page.ToString());
                cookie.Values.Add("From", From);
                cookie.Values.Add("To", To);
                cookie.Values.Add("CUID", CUID);
                cookie.Values.Add("FluSeason", FluSeason.ToString());
                Response.Cookies.Add(cookie);
            }

            int? DefaultPageSize = 10;
            int currentPageIndex = localpage.HasValue ? localpage.Value - 1 : 0;
            int currentFilterIndex = localFilterIndex.HasValue ? localFilterIndex.Value : 0;
            int currentStatusFilterIndex = localStatusFilterIndex.HasValue ? localStatusFilterIndex.Value : 0;
            if (localPageSize != null)
            {
                DefaultPageSize = localPageSize;
            }
            ViewBag.IdFilter = localIdFilter;
            ViewBag.StatusFilterIndex = localStatusFilterIndex;
            ViewBag.FilterIndex = localFilterIndex;
            ViewBag.FluSeason = localFluSeason;
            ViewBag.AllowC = _dataService.UserHasLab().Success;

            var statusList = _dataService.StatusList().Data;
            if (statusList != null)
            {
                if (currentStatusFilterIndex <= statusList.Count)
                {
                    ViewBag.StatusFilters = new SelectList(statusList, "Value", "Name", statusList[currentStatusFilterIndex].Value);
                }
                else 
                {
                    ViewBag.StatusFilters = new SelectList(statusList, "Value", "Name");
                }
            }

            var statusChangeList = _dataService.StatusDTOList();
            if (statusChangeList.Success)
            {
                var tempList = statusChangeList.Data;
                if (!User.IsInRole("Admin"))
                {
                    var tempItem = tempList.FirstOrDefault(s => s.SysName == "SUBMITTED");
                    if (tempItem != null)
                    {
                        tempList.Remove(tempItem);
                    }
                }
                ViewBag.ChangeStatus = new SelectList(statusChangeList.Data, "Id", "DisplayName", tempList[0].Id);
            }

            var filterList = _dataService.FilterList(new MessageBase<string>() { Data = "Specimen"}).Data;
            if (filterList != null)
            {
                if (currentFilterIndex <= filterList.Count)
                {
                    ViewBag.Filters = new SelectList(filterList, "Id", "Display", filterList[currentFilterIndex].Id);
                }
                else
                {
                    ViewBag.Filters = new SelectList(filterList, "Id", "Display");
                }
            }
            
            List<KeyValuePair<string, int>> sizes = new List<KeyValuePair<string, int>>();
            sizes.Add(new KeyValuePair<string, int>("10", 10));
            sizes.Add(new KeyValuePair<string, int>("20", 20));
            sizes.Add(new KeyValuePair<string, int>("40", 40));
            sizes.Add(new KeyValuePair<string, int>("80", 80));
            sizes.Add(new KeyValuePair<string, int>("All", Int16.MaxValue));
            ViewBag.PageSizes = new SelectList(sizes, "Value", "Key", DefaultPageSize);
            ViewBag.PageSize = DefaultPageSize;

            Dictionary<string, string> sortColumns = new Dictionary<string, string>();

            sortColumns.Add("SpecimenId", "SpecimenId");
            sortColumns.Add("Package.Name", "Package.Name");
            sortColumns.Add("Package.SenderId", "Package.SenderId");
            sortColumns.Add("CSID", "CSID");
            sortColumns.Add("Status", "Status");
            sortColumns.Add("FilterDate", "FilterDate");
            sortColumns.Add("IsolateCount", "IsolateCount");
            sortColumns.Add("SubType", "SubType");

            var validSort = false;
            //toggle
            if (!String.IsNullOrEmpty(localSortOrder) && localSortOrder.EndsWith(" DESC"))
            {
                var trimmed = localSortOrder.Remove(localSortOrder.Length - 5);
                if (sortColumns.ContainsValue(trimmed))
                {
                    validSort = true;
                    sortColumns[trimmed] = trimmed;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(localSortOrder) && sortColumns.ContainsValue(localSortOrder))
                {
                    validSort = true;
                    sortColumns[localSortOrder] = string.Concat(localSortOrder, " DESC");
                }
            }
            ViewBag.SortParm = sortColumns;
            ViewBag.CUID = localCUID;

            ViewBag.MultiPrintQty = 2;
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("PrintQty"))
            {
               var temp = this.ControllerContext.HttpContext.Request.Cookies["PrintQty"]["Qty"];
               if (string.IsNullOrEmpty(temp))
               {
                   ViewBag.MultiPrintQty = temp;    
               }
            }
            
            var vm = new List<SpecimenVM>();
            VirtualPagedList<SpecimenVM> vpl;
            var requestdetail = new RequestDetail() { CurrentPage = localpage ?? 1, ItemsPerPage = DefaultPageSize.Value, CUID = localCUID };
            DateTime tempDT;
            string[] formats = { "yyyy-MM-dd" };
            if (DateTime.TryParseExact(localFrom, formats, null, DateTimeStyles.None, out tempDT))
            {
                ViewBag.From = tempDT.ToString("yyyy-MM-dd"); 
                requestdetail.From = tempDT;
            }
            if (DateTime.TryParseExact(localTo, formats, null, DateTimeStyles.None, out tempDT))
            {
                ViewBag.To = tempDT.ToString("yyyy-MM-dd");
                requestdetail.To = tempDT;
            }
            if (!string.IsNullOrEmpty(localIdFilter))
            {
                requestdetail.IdFilter = localIdFilter;
            }

            if (currentFilterIndex <= filterList.Count && !string.IsNullOrEmpty(filterList[currentFilterIndex].FilterText))
            {
                requestdetail.Where = filterList[currentFilterIndex].FilterText;
            }

            if (currentStatusFilterIndex <= statusList.Count && !string.IsNullOrEmpty(statusList[currentStatusFilterIndex].Value))
            {
                if (!string.IsNullOrEmpty(requestdetail.Where))
                {
                    requestdetail.Where = requestdetail.Where + " AND " + "StatusId = " + statusList[currentStatusFilterIndex].Value;
                }
                else
                {
                    requestdetail.Where = "StatusId = " + statusList[currentStatusFilterIndex].Value;
                }
            }

            requestdetail.FluSeason = localFluSeason;

            // do the oerder by
            if (validSort)
            {
                requestdetail.OrderBy = localSortOrder;
                ViewBag.SortOrder = localSortOrder;
            }

            var dto = _dataService.SpecimenList(new MessageBase<RequestDetail>() { Data = requestdetail});
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                var specimens = dto.Data as List<SpecimenDTO>;
                var selectedList = (TempData["SelectedItems"] as List<int>);
                if (selectedList == null)
                {
                    specimens.ForEach(e => vm.Add(new SpecimenVM(e, uro, uie)));
                }
                else 
                {
                    specimens.ForEach(e => { var tempSVM = new SpecimenVM(e, uro, uie); tempSVM.Selected = selectedList.Contains(e.Id); vm.Add(tempSVM); });
                }
                if (TempData.ContainsKey("DialogTitle"))
                {
                    ViewBag.DialogTitle = TempData["DialogTitle"];
                }
                if (TempData.ContainsKey("DialogBody"))
                {
                    ViewBag.DialogBody = TempData["DialogBody"];
                }
                if (TempData.ContainsKey("DialogSuccess"))
                {
                    ViewBag.DialogSuccess = TempData["DialogSuccess"];
                }
            }
            vpl = new VirtualPagedList<SpecimenVM>(vm, dto.ItemsPerPage, dto.TotalCount, dto.TotalPages, dto.CurrentPage);
            return View(vpl);
        }

        // GET: Specimens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SpecimenVM vm = null;
            var dto = _dataService.Specimen(new MessageBase<int>(){ Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenVM(dto.Data, uro, uie);
            } else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Specimens/Create
        public ActionResult Create()
        {
            PopulateSelectLists();

            var vm = new SpecimenVM();
            ViewData.Model = vm;
            return View();
        }

        private void PopulateSelectLists()
        {

            #region package
            var packages = _dataService.PackageList().Data;
            if (packages == null)
            {
                packages = new List<PackageDTO>();
            }
            packages = packages.Where(s => s.IsVisible).OrderByDescending(s => s.Id).ToList();
            ViewBag.PackageId = new SelectList(packages, "Id", "ComboDisplay");
            #endregion

            #region country
            var countries = _dataService.GetCountries().Data;
            if (countries == null)
            {
                countries = new List<CountryDTO>();
                countries.WithBlank();
            }
            var usid = countries.Where(s => s.Name == "United States").FirstOrDefault();
            ViewBag.CountryId = new SelectList(countries, "Id", "LongCode", usid != null ? usid.Id : 0);
            #endregion

            #region travelcountry
            ViewBag.TravelCountryId = new SelectList(countries, "Id", "LongCode");
            #endregion

            #region state
            var states = _dataService.GetStates().Data;
            if (states == null)
            {
                states = new List<StateDTO>();
                states.WithBlank();
            }
            ViewBag.StateId = new SelectList(states, "Id", "Abbreviation");
            #endregion

            #region travelstate
            ViewBag.TravelStateId = new SelectList(states, "Id", "Abbreviation");
            #endregion

            #region travelcounty
            var counties = new List<CountyDTO>();
            counties.WithBlank();
            ViewBag.TravelCountyId = new SelectList(counties, "Id", "Name");
            #endregion

            counties = new List<CountyDTO>();
            counties.WithBlank();
            ViewBag.CountyId = new SelectList(counties, "Id", "Name");

            #region picklist_EXT_ACTV
            var plExtActv = _dataService.GetPickListItems(new MessageBase<string>() { Data = "EXT_ACTV" }).Data;
            if (plExtActv != null)
            {
                ViewBag.Activity = new SelectList(plExtActv, "Name", "Name");
            }
            else { ViewBag.Activity = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region picklist_AGE_TYPE
            var plAgeType = _dataService.GetPickListItems(new MessageBase<string>() { Data = "AGE_TYPE" }).Data;
            if (plAgeType != null)
            {
                ViewBag.PatientAgeUnits = new SelectList(plAgeType, "Name", "Name", "Years");
            }
            else { ViewBag.PatientAgeUnits = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region picklist_SPECIAL_STUDY
            var plSpecialStudy = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SPECIAL_STUDY" }).Data;
            if (plSpecialStudy != null)
            {
                ViewBag.SpecialStudy = new SelectList(plSpecialStudy, "Name", "Name");
            }
            else { ViewBag.SpecialStudy = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region picklist_SAMPLE_CLASS
            var plSCL = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SMP_CLASS" }).Data;
            if (plSCL != null)
            {
                ViewBag.SampleClass = new SelectList(plSCL, "Name", "Name");
            }
            else { ViewBag.SampleClass = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region PatientSex
            var plPSex = _dataService.GetPickListItems(new MessageBase<string>() { Data = "GNDR" }).Data;
            if (plPSex != null)
            {
                ViewBag.PatientSex = new SelectList(plPSex, "Name", "Name");
            }
            else { ViewBag.PatientSex = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region SpecimenSource
            var plSS = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SPEC_SOURCE" }).Data;
            if (plSS != null)
            {
                ViewBag.SpecimenSource = new SelectList(plSS, "Name", "Name");
            }
            else { ViewBag.SpecimenSource = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion

            #region CompCSID1
            var csid = _dataService.PackageCSIDList(new MessageBase<int?>() { Data = packages.FirstOrDefault().Id }).Data;
            if (csid != null)
            {
                ViewBag.CompCSID1 = new SelectList(csid, "Value", "Value");
            }
            else { ViewBag.CompCSID1 = new SelectList(new List<NameValueDTO>() { new NameValueDTO() }, "Value", "Name"); }
            #endregion

            #region CompCSID2
            var csid2 = _dataService.PackageCSIDList(new MessageBase<int?>() { Data = packages.FirstOrDefault().Id }).Data;
            if (csid2 != null)
            {
                ViewBag.CompCSID2 = new SelectList(csid2, "Value", "Value");
            }
            else { ViewBag.CompCSID2 = new SelectList(new List<NameValueDTO>() { new NameValueDTO() }, "Value", "Value"); }
            #endregion

            #region YNU
            var ynuList = new List<NameValueDTO>();
            ynuList.WithYNU();

            ViewBag.DrugResistant = new SelectList(ynuList, "Value", "Value");
            ViewBag.Deceased = new SelectList(ynuList, "Value", "Value");
            ViewBag.NursingHome = new SelectList(ynuList, "Value", "Value");
            ViewBag.Vaccinated = new SelectList(ynuList, "Value", "Value");
            #endregion

            #region ReasonForSubmission
            var plReasonForSubmission = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SUBM_REAS" }).Data;
            if (plReasonForSubmission != null)
            {
                ViewBag.ReasonForSubmission = new SelectList(plReasonForSubmission, "Name", "Name");
            }
            else { ViewBag.ReasonForSubmission = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name"); }
            #endregion
        }

        // POST: Specimens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] SpecimenVM vm)
        {
            if (!ModelState.IsValid)
            {
                PopulateSelectLists(vm);
                return View(vm);
            }    
            if (ModelState.IsValid)
            {
                var dto = new SpecimenDTO { 
                    //CSID = vm.CSID,
                    //CUID = vm.CUID, 
                    CSID = String.Empty,
                    //CUID = String.Empty, 
                    SampleClass = vm.SampleClass,
                    DateInoculated = vm.DateInoculated,
                    ProjectId = vm.ProjectId, PackageId = vm.PackageId, 
                    CountryId = vm.CountryId,
                    StateId = vm.StateId,
                    CountyId = vm.CountyId,
                    Activity = vm.Activity,
                    DateCollected = vm.DateCollected,
                    SpecimenID = vm.SpecimenId,
                    PatientAge = vm.PatientAge,
                    PatientAgeUnits = vm.PatientAgeUnits,
                    PatientSex = vm.PatientSex,
                    PassageHistory = vm.PassageHistory,
                    SubType = vm.SubType,
                    SpecimenSource = vm.SpecimenSource,
                    CompCSID1 = vm.CompCSID1,
                    CompCSID2 = vm.CompCSID2,
                    OtherLocation = vm.OtherLocation,
                    DrugResistant = vm.DrugResistant,
                    Deceased = vm.Deceased,
                    NursingHome = vm.NursingHome,
                    Vaccinated = vm.Vaccinated,
                    Travel = vm.Travel,
                    ReasonForSubmission = vm.ReasonForSubmission,
                    Comments = vm.Comments,
                    StatusId = vm.StatusId,
                    SpecialStudy = vm.SpecialStudy,

                    TravelCountry = vm.TravelCountryName,
                    TravelState = vm.TravelStateName,
                    TravelCounty = vm.TravelCountyName,
                    TravelCity = vm.TravelCity,
                    HostCellLine = vm.HostCellLine,
                    Substrate = vm.Substrate,
                    PassageNumber = vm.PassageNumber,
                    PCRInfA = vm.PCRInfA,
                    PCRpdmInfA = vm.PCRpdmInfA,
                    PCRpdmH1 = vm.PCRpdmH1,
                    PCRH3 = vm.PCRH3,
                    PCRInfB = vm.PCRInfB,
                    PCRBVic = vm.PCRBVic,
                    PCRBYam = vm.PCRBYam,
                    City = vm.City,
                    TravelReturnDate = vm.TravelReturnDate,
                    WHOSubmittedSubtype = vm.WHOSubmittedSubtype,

                };
                var specimen = _dataService.SpecimenInsert(new MessageBase<SpecimenDTO>() { Data = dto });
                if (specimen.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", specimen.Error);
                    PopulateSelectLists(vm);
                }
            }
            return View(vm);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SpecimenVM vm = null;
            var dto = _dataService.Specimen(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                ViewBag.AllowC = _dataService.UserHasLab().Success;
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenVM(dto.Data, uro, uie);

                PopulateSelectLists(vm);

            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        private void PopulateSelectLists(SpecimenVM vm)
        {
            #region package
            var items = _dataService.PackageList().Data;
            if (items == null)
            {
                items = new List<PackageDTO>();
            }
            items = items.Where(s => s.IsVisible || s.Id == vm.PackageId).OrderByDescending(s => s.Id).ToList();
            ViewBag.PackageId = new SelectList(items, "Id", "ComboDisplay", vm.PackageId);
            #endregion


            #region country
            var countries = _dataService.GetCountries().Data;
            if (countries == null)
            {
                countries = new List<CountryDTO>();
                countries.Add(new CountryDTO());
            }
            ViewBag.CountryId = new SelectList(countries, "Id", "LongCode", vm.CountryId);
            #endregion


            #region state
            var states = _dataService.GetStates().Data;
            if (states == null)
            {
                states = new List<StateDTO>();
                states.Add(new StateDTO());
            }
            ViewBag.StateId = new SelectList(states, "Id", "Abbreviation", vm.StateId);
            #endregion

         
            if (vm.StateId.HasValue)
            {
                var counties = _dataService.GetCounties(new MessageBase<int>() { Data = vm.StateId.Value }).Data;
                ViewBag.CountyId = new SelectList(counties, "Id", "Name", vm.CountyId);
            }
            else
            {
                var counties = new List<CountyDTO>();
                counties.WithBlank();
                ViewBag.CountyId = new SelectList(counties, "Id", "Name", vm.CountyId);
            }

            #region picklist_EXT_ACTV
            var plExtActv = _dataService.GetPickListItems(new MessageBase<string>() { Data = "EXT_ACTV" }).Data;
            if (plExtActv != null)
            {
                ViewBag.Activity = new SelectList(plExtActv, "Name", "Name", vm.Activity);
            }
            else { ViewBag.Activity = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.Activity); }
            #endregion

            #region picklist_AGE_TYPE
            var plAgeType = _dataService.GetPickListItems(new MessageBase<string>() { Data = "AGE_TYPE" }).Data;
            if (plAgeType != null)
            {
                ViewBag.PatientAgeUnits = new SelectList(plAgeType, "Name", "Name", vm.PatientAgeUnits);
            }
            else { ViewBag.PatientAgeUnits = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.PatientAgeUnits); }
            #endregion

            #region picklist_SPECIAL_STUDY
            var plSpecialStudy = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SPECIAL_STUDY" }).Data;
            if (plSpecialStudy != null)
            {
                if (plSpecialStudy.Where(s => s.Name == vm.SpecialStudy).Count() == 0)
                {
                    plSpecialStudy.Add(new SynonymDTO() { Name = vm.SpecialStudy });
                }
                ViewBag.SpecialStudy = new SelectList(plSpecialStudy, "Name", "Name", vm.SpecialStudy);
            }
            else { ViewBag.SpecialStudy = new SelectList(new List<SynonymDTO>() { new SynonymDTO() { Name = vm.SpecialStudy } }, "Name", "Name", vm.SpecialStudy); }
            #endregion

            #region picklist_SAMPLE_CLASS
            var plSCL = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SMP_CLASS" }).Data;
            if (plSCL != null)
            {
                ViewBag.SampleClass = new SelectList(plSCL, "Name", "Name", vm.SampleClass);
            }
            else { ViewBag.SampleClass = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.SampleClass); }
            #endregion

            #region PatientSex
            var plPSex = _dataService.GetPickListItems(new MessageBase<string>() { Data = "GNDR" }).Data;
            if (plPSex != null)
            {
                ViewBag.PatientSex = new SelectList(plPSex, "Name", "Name", vm.PatientSex);
            }
            else { ViewBag.PatientSex = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.PatientSex); }
            #endregion

            #region SpecimenSource
            var plSS = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SPEC_SOURCE" }).Data;
            if (plSS != null)
            {
                ViewBag.SpecimenSource = new SelectList(plSS, "Name", "Name", vm.SpecimenSource);
            }
            else { ViewBag.SpecimenSource = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.SpecimenSource); }
            #endregion

            #region CompCSID1
            var csid = _dataService.PackageCSIDList(new MessageBase<int?>() { Data = vm.PackageId }).Data;
            if (csid != null)
            {
                ViewBag.CompCSID1 = new SelectList(csid, "Value", "Value", vm.CompCSID1);
            }
            else { ViewBag.CompCSID1 = new SelectList(new List<NameValueDTO>() { new NameValueDTO() }, "Value", "Name", vm.CompCSID1); }
            #endregion

            #region CompCSID2
            var csid2 = _dataService.PackageCSIDList(new MessageBase<int?>() { Data = vm.PackageId }).Data;
            if (csid2 != null)
            {
                ViewBag.CompCSID2 = new SelectList(csid2, "Value", "Value", vm.CompCSID2);
            }
            else { ViewBag.CompCSID2 = new SelectList(new List<NameValueDTO>() { new NameValueDTO() }, "Value", "Value", vm.CompCSID2); }
            #endregion

            #region YNU
            var ynuList = new List<NameValueDTO>();
            ynuList.WithYNU();

            ViewBag.DrugResistant = new SelectList(ynuList, "Value", "Value", vm.DrugResistant);
            ViewBag.Deceased = new SelectList(ynuList, "Value", "Value", vm.Deceased);
            ViewBag.NursingHome = new SelectList(ynuList, "Value", "Value", vm.NursingHome);
            ViewBag.Vaccinated = new SelectList(ynuList, "Value", "Value", vm.Vaccinated);

            #endregion

            #region ReasonForSubmission
            var plReasonForSubmission = _dataService.GetPickListItems(new MessageBase<string>() { Data = "SUBM_REAS" }).Data;
            if (plReasonForSubmission != null)
            {
                ViewBag.ReasonForSubmission = new SelectList(plReasonForSubmission, "Name", "Name", vm.ReasonForSubmission);
            }
            else { ViewBag.ReasonForSubmission = new SelectList(new List<SynonymDTO>() { new SynonymDTO() }, "Name", "Name", vm.ReasonForSubmission); }
            #endregion
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] SpecimenVM vm)
        {
            if (!ModelState.IsValid)
            {
                PopulateSelectLists(vm);
                return View(vm);
            }

            if (ModelState.IsValid)
            {
                var dto = new SpecimenDTO { 
                    Id = vm.Id, 
                    CSID = vm.CSID, 
                    //CUID = vm.CUID, 
                    SampleClass = vm.SampleClass,
                    DateInoculated = vm.DateInoculated,
                    ProjectId = vm.ProjectId, 
                    PackageId = vm.PackageId,
                    CountryId = vm.CountryId,
                    StateId = vm.StateId,
                    CountyId = vm.CountyId,
                    Activity = vm.Activity,
                    DateCollected = vm.DateCollected,
                    SpecimenID = vm.SpecimenId,
                    PatientAge = vm.PatientAge,
                    PatientAgeUnits = vm.PatientAgeUnits,
                    PatientSex = vm.PatientSex,
                    PassageHistory = vm.PassageHistory,
                    SubType = vm.SubType,
                    SpecimenSource = vm.SpecimenSource,
                    CompCSID1 = vm.CompCSID1,
                    CompCSID2 = vm.CompCSID2,
                    OtherLocation = vm.OtherLocation,
                    DrugResistant = vm.DrugResistant,
                    Deceased = vm.Deceased,
                    NursingHome = vm.NursingHome,
                    Vaccinated = vm.Vaccinated,
                    Travel = vm.Travel,
                    ReasonForSubmission = vm.ReasonForSubmission,
                    Comments = vm.Comments,
                    StatusId = vm.StatusId,
                    SpecialStudy = vm.SpecialStudy,

                    TravelCountry = vm.TravelCountryName,
                    TravelState = vm.TravelStateName,
                    TravelCounty = vm.TravelCountyName,
                    TravelCity = vm.TravelCity,
                    HostCellLine = vm.HostCellLine,
                    Substrate = vm.Substrate,
                    PassageNumber = vm.PassageNumber,
                    PCRInfA = vm.PCRInfA,
                    PCRpdmInfA = vm.PCRpdmInfA,
                    PCRpdmH1 = vm.PCRpdmH1,
                    PCRH3 = vm.PCRH3,
                    PCRInfB = vm.PCRInfB,
                    PCRBVic = vm.PCRBVic,
                    PCRBYam = vm.PCRBYam,
                    City = vm.City,
                    TravelReturnDate = vm.TravelReturnDate,
                    WHOSubmittedSubtype = vm.WHOSubmittedSubtype,

                };
                var specimen = _dataService.SpecimenEdit(new MessageBase<SpecimenDTO>() { Data = dto });
                if (specimen.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    // * TODO: Create error message and display
                    ModelState.AddModelError("", specimen.Error);
                    PopulateSelectLists(vm);
                }
            }
            ViewBag.AllowC = _dataService.UserHasLab().Success;
            return View(vm);
        }

        // GET: Specimens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecimenVM vm = null;
            var dto = _dataService.Specimen(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Specimens/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var result = _dataService.SpecimenDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index");
            }
            // * TODO: Create error message and display
            SpecimenVM vm = null;
            var dto = _dataService.Specimen(new MessageBase<int>() { Data = id });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        [HttpGet]
        public ActionResult GetCounties(int id)
        {
            #region county
            var counties = _dataService.GetCounties(new MessageBase<int>() { Data = id }).Data;
            if (counties == null)
            {
                counties = new List<CountyDTO>();
                counties.Add(new CountyDTO());
            }
            var data = counties.Select(d => new { Text = d.Name, Value = d.Id }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
            #endregion
        }

        // POST: Specimens/MultiAction
        [Authorize]
        [HttpPost]
        public ActionResult MultiAction(VirtualPagedList<ContractLab.UI.Models.ContractLab.SpecimenVM> vm)
        {
            var items = vm.Items.Where(s => s.Selected).Select(s => s.Id).ToList();
            if (items.Count > 0)
	        {
                TempData["SelectedItems"] = items;
                if (Request.Form["submitDLS"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateSpecimenFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }

                }
                else if (Request.Form["submitDLSTrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateSpecimenTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Specimen Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLA"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateAliquotFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLATrs"] != null)
                {
                    ResponseBase dto = null;
                    dto = _dataService.GenerateAliquotTransmissionRows(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Aliquot Transmission";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLW"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateWeeklyFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Weekly File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLM"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateMonthlyFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Monthly File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLI"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateISRFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "ISR File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLN"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateNAIFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "NAI File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitDLINOC"] != null)
                {
                    ResponseBase<TemplateDTO> dto = null;
                    dto = _dataService.GenerateINOCFile(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "Specimens" });
                    if (dto != null && dto.Success)
                    {
                        var cd = new System.Net.Mime.ContentDisposition
                        {
                            FileName = dto.Data.FileName,
                            Inline = false,
                        };
                        Response.AppendHeader("Content-Disposition", cd.ToString());
                        return File(dto.Data.File, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "INOC File";
                        TempData["DialogBody"] = dto.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitCS"] != null)
                {
                    if (vm.ChangeStatus.HasValue)
                    {
                        var updated = _dataService.UpdateStatus(new MessageBase<List<int>>() { Data = items }, new MessageBase<int>() { Data = vm.ChangeStatus.Value });
                        if (!updated.Success)
                        {
                            TempData["DialogTitle"] = "Update Status";
                            TempData["DialogBody"] = updated.Error;
                            TempData["DialogSuccess"] = "Close";
                        }
                    }
                }
                else if (Request.Form["submitpl"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        if (int.TryParse(Request.Form["print_qty"], out qty))
                        {
                                var cookie = new HttpCookie("PrintQty");
                                cookie.Values.Add("Qty", qty.ToString());
                                Response.Cookies.Add(cookie);
                        }
                    }
                    var labels = _dataService.GenerateLabels(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Json(labels.Data);
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitplp"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        if (int.TryParse(Request.Form["print_qty"], out qty))
                        {
                            var cookie = new HttpCookie("PrintQty");
                            cookie.Values.Add("Qty", qty.ToString());
                            Response.Cookies.Add(cookie);
                        }
                    }
                    var labels = _dataService.GenerateLabels_Page(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Content(labels.Data, "text/plain");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitpl_set"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        int.TryParse(Request.Form["print_qty"], out qty);
                    }
                    var labels = _dataService.GenerateLabels(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen_set" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Json(labels.Data);
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitplp_set"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        int.TryParse(Request.Form["print_qty"], out qty);
                    }
                    var labels = _dataService.GenerateLabels_Page(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen_set" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Content(labels.Data, "text/plain");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }
                else if (Request.Form["submitpl_who"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        int.TryParse(Request.Form["print_qty"], out qty);
                    }
                    var labels = _dataService.GenerateLabels(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen_who" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Json(labels.Data);
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                    }
                }
                else if (Request.Form["submitplp_who"] != null)
                {
                    int qty = 1;
                    if (Request.Form["print_qty"] != null)
                    {
                        int.TryParse(Request.Form["print_qty"], out qty);
                    }
                    var labels = _dataService.GenerateLabels_Page(new MessageBase<List<int>>() { Data = items }, new MessageBase<string>() { Data = "specimen_who" }, new MessageBase<int>() { Data = qty });
                    if (labels.Success)
                    {
                        return Content(labels.Data, "text/plain");
                    }
                    else
                    {
                        TempData["DialogTitle"] = "Print Labels";
                        TempData["DialogBody"] = labels.Error;
                        TempData["DialogSuccess"] = "Close";
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }


	        }
            else
            {
                if (Request.Form["submitplp"] != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                if (Request.Form["submitplp_set"] != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                if (Request.Form["submitplp_who"] != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult PackageCSIDList(int packageId)
        {
            var csids = _dataService.PackageCSIDList(new MessageBase<int?> { Data = packageId });
            if (csids.Success)
            {
                return Json(csids.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new List<NameValueDTO> { }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}