﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private readonly IContractLabDataService  _dataService;
        public ProjectsController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Projects
        public ActionResult Index()
        {
            var vm = new List<ProjectVM>();
            var dto = _dataService.ProjectList();
            if (dto.Success)
            {
                var projects = dto.Data as List<ProjectDTO>;
                projects.ForEach(e => vm.Add(new ProjectVM(e)));
            }
            return View(vm);
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectVM vm = null;
            var dto = _dataService.Project(new MessageBase<int>(){ Data = id.Value });
            if (dto.Success)
            {
                vm = new ProjectVM(dto.Data);
            } else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Projects/Create
        public ActionResult Create()
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            return View();
        }

        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] ProjectVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ProjectDTO { Name = vm.Name};
                var project = _dataService.ProjectInsert(new MessageBase<ProjectDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectVM vm = null;
            var dto = _dataService.Project(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new ProjectVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name")] ProjectVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new ProjectDTO { Id = vm.Id, Name = vm.Name};
                var project = _dataService.ProjectEdit(new MessageBase<ProjectDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectVM vm = null;
            var dto = _dataService.Project(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new ProjectVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.ProjectDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
