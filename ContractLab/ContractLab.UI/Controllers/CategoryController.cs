﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public CategoryController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Category
        public ActionResult Index()
        {
            int? localpage = null;
            int? DefaultPageSize = int.MaxValue;
            var vm = new List<CategoryVM>();
            var requestdetail = new RequestDetail() { CurrentPage = localpage ?? 1, ItemsPerPage = DefaultPageSize.Value };
            var dto = _dataService.CategoryList(new MessageBase<RequestDetail>() { Data = requestdetail});
            if (dto.Success)
            {
                foreach (var item in dto.Data)
	            {
                    vm.Add(new CategoryVM(item));
	            }                
            }
            return View(vm);
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryVM vm = null;
            var dto = _dataService.Category(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new CategoryVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = "Id")] CategoryVM vm)
        {
            var result = _dataService.CategoryDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index");
            }
            else
            {
                vm = null;
                var dto = _dataService.Category(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    vm = new CategoryVM(dto.Data);
                }
                else
                {
                    return HttpNotFound();
                }
                return View(vm);
            }
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, AllowBlank, LitsQuestionId")] CategoryVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new CategoryDTO { Name = vm.Name,  AllowBlank = vm.AllowBlank, Description = vm.Name, LitsQuestionId = vm.LitsQuestionId };
                var project = _dataService.CategoryInsert(new MessageBase<CategoryDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryVM vm = null;
            var dto = _dataService.Category(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new CategoryVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, AllowBlank, LitsQuestionId")] CategoryVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new CategoryDTO { Id = vm.Id, Name = vm.Name, AllowBlank = vm.AllowBlank, Description = vm.Name, LitsQuestionId = vm.LitsQuestionId };
                var project = _dataService.CategoryEdit(new MessageBase<CategoryDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

    }
}