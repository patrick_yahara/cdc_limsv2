﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContractLab.UI.Models;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DTO;
using System.Data.Entity.Infrastructure;
using ContractLab.DataService.Interfaces;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class DEIFilesController : Controller
    {

        private readonly IContractLabDataService  _dataService;
        public DEIFilesController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: DEIFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DEIFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id")] DEIFile dEIFile, HttpPostedFileBase upload, HttpPostedFileBase uploadclients, HttpPostedFileBase uploadcontacts)
        {
            try 
            {
                if (ModelState.IsValid)
                {
                    #region piicklist
                    if (upload != null && upload.ContentLength > 0)
                    {
                        var dto = new DEIFileDTO
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            ContentType = upload.ContentType,
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            dto.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        _dataService.DEIFileMerge(new MessageBase<DEIFileDTO>() { Data = dto});
                    }
                    #endregion

                    #region clients
                    if (uploadclients != null && uploadclients.ContentLength > 0)
                    {
                        var dto = new DEIFileDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadclients.FileName),
                            ContentType = uploadclients.ContentType,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadclients.InputStream))
                        {
                            dto.Content = reader.ReadBytes(uploadclients.ContentLength);
                        }
                        _dataService.DEIFileMergeClients(new MessageBase<DEIFileDTO>() { Data = dto });
                    }
                    #endregion

                    #region contacts
                    if (uploadcontacts != null && uploadcontacts.ContentLength > 0)
                    {
                        var dto = new DEIFileDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadcontacts.FileName),
                            ContentType = uploadcontacts.ContentType,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadcontacts.InputStream))
                        {
                            dto.Content = reader.ReadBytes(uploadcontacts.ContentLength);
                        }
                        var result = _dataService.DEIFileMergeContacts(new MessageBase<DEIFileDTO>() { Data = dto });
                        if (!result.Success)
                        {
                            ViewBag.Error = result.GetDetailedErrorString(); 
                        }
                    }
                    #endregion
                    return RedirectToAction("Create");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}
