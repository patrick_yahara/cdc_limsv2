﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContractLab.UI.Models;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DTO;
using System.Data.Entity.Infrastructure;
using ContractLab.DataService.Interfaces;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class TemplateFilesController : Controller
    {

        private readonly IContractLabDataService  _dataService;
        public TemplateFilesController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: DEIFiles/Create
        public ActionResult Create()
        {
            var vm = new List<TemplateVM>();

            var labs = _dataService.GetLabs().Data;
            if (labs == null)
            {
                labs = new List<LabDTO>();
            }
            labs.WithBlank();
            ViewBag.LabId = new SelectList(labs, "Id", "Name");
           
            
            var dto = _dataService.TemplateList();
            if (dto.Success)
            {
                var Templates = dto.Data as List<TemplateDTO>;
                Templates.ForEach(e => vm.Add(new TemplateVM(e)));
            }


            return View(vm);
        }

        // POST: Filters/Delete/5
        public ActionResult Delete(int id)
        {
            _dataService.TemplateDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Create");
        }

        // POST: DEIFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase uploadS, 
            HttpPostedFileBase uploadA, 
            HttpPostedFileBase uploadW, 
            HttpPostedFileBase uploadM,
            HttpPostedFileBase uploadI,
            HttpPostedFileBase uploadN,
            HttpPostedFileBase uploadINOC
            )
        {
            try 
            {
                if (ModelState.IsValid)
                {
                    int? labId = null;
                    int tempI;
                    if (int.TryParse(Request.Form["LabId"], out tempI))
                    {
                        labId = tempI;
                    }
                    #region specimens
                    if (uploadS != null && uploadS.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadS.FileName),
                            TemplateType = "Specimen",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadS.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadS.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                        if (!result.Success)
                        {
                            
                        }
                    }
                    #endregion

                    #region aliquots
                    if (uploadA != null && uploadA.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadA.FileName),
                            TemplateType = "Aliquot",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadA.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadA.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                        if (!result.Success)
                        {

                        }
                    }
                    #endregion

                    #region weekly
                    if (uploadW != null && uploadW.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadW.FileName),
                            TemplateType = "Weekly",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadW.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadW.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                        if (!result.Success)
                        {

                        }
                    }
                    #endregion


                    #region monthly
                    if (uploadM != null && uploadM.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadM.FileName),
                            TemplateType = "Monthly",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadM.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadM.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                    }
                    #endregion

                    #region ISR
                    if (uploadI != null && uploadI.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadI.FileName),
                            TemplateType = "ISR",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadI.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadI.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                    }
                    #endregion

                    #region NAI
                    if (uploadN != null && uploadN.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadN.FileName),
                            TemplateType = "NAI",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadN.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadN.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                    }
                    #endregion

                    #region INOC
                    if (uploadINOC != null && uploadINOC.ContentLength > 0)
                    {
                        var dto = new TemplateDTO
                        {
                            FileName = System.IO.Path.GetFileName(uploadINOC.FileName),
                            TemplateType = "INOC",
                            LabId = labId,
                        };
                        using (var reader = new System.IO.BinaryReader(uploadINOC.InputStream))
                        {
                            dto.File = reader.ReadBytes(uploadINOC.ContentLength);
                        }
                        var result = _dataService.TemplateUpload(new MessageBase<TemplateDTO>() { Data = dto });
                    }
                    #endregion


                    return RedirectToAction("Create");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}
