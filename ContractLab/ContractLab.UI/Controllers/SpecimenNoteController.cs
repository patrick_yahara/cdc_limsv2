﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;
using Microsoft.AspNet.Identity;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class SpecimenNoteController : Controller
    {
        private const string _fieldlist = @"Id, Note, FileName, NoteCategoryId, SpecimenId";

        private readonly IContractLabDataService _dataService;
        public SpecimenNoteController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: SpecimenNotes
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var uro = _dataService.UserReadOnly().Success;
            var uie = _dataService.UserEditor().Success;

            ViewBag.AllowC = _dataService.UserHasLab().Success;

            ViewBag.SpecimenId = id.Value;
            var vm = new List<SpecimenNoteVM>();
            var dto = _dataService.SpecimenNoteList(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var Notes = dto.Data as List<SpecimenNoteDTO>;
                //var selectedList = (TempData["SelectedItems"] as List<int>);
                //if (selectedList == null)
                //{
                    Notes.ForEach(e => vm.Add(new SpecimenNoteVM(e, uro, uie)));
                //}
                //else
                //{
                //    Notes.ForEach(e => { var tempSVM = new SpecimenNoteVM(e, uro, uie); tempSVM.Selected = selectedList.Contains(e.Id); vm.Add(tempSVM); });
                //}
                //if (TempData.ContainsKey("DialogTitle"))
                //{
                //    ViewBag.DialogTitle = TempData["DialogTitle"];
                //}
                //if (TempData.ContainsKey("DialogBody"))
                //{
                //    ViewBag.DialogBody = TempData["DialogBody"];
                //}
                //if (TempData.ContainsKey("DialogSuccess"))
                //{
                //    ViewBag.DialogSuccess = TempData["DialogSuccess"];
                //}
            }
            return View(vm);
        }

        // GET: SpecimenNotes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecimenNoteVM vm = null;
            var dto = _dataService.SpecimenNote(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenNoteVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: SpecimenNotes/Create
        public ActionResult Create(int? id)
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            GetCreateRequiredData();

            var spec = _dataService.Specimen(new MessageBase<int>() { Data = id.Value });
            var csid = string.Empty;
            if (spec.Success)
            {
                csid = spec.Data.CSID;
            }
            var vm = new SpecimenNoteVM() { SpecimenId = id.Value, CSID = csid };
            ViewData.Model = vm;
            return View();
        }

        private void GetCreateRequiredData()
        {
            var NoteCategories = _dataService.NoteCategoryList().Data;
            if (NoteCategories == null)
            {
                NoteCategories = new List<NoteCategoryDTO>();
            }
            var remove = NoteCategories.Where(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
            if (remove != null)
            {
                NoteCategories.Remove(remove);
            }
            ViewBag.NoteCategoryId = new SelectList(NoteCategories, "Id", "DisplayName");

        }

        // POST: SpecimenNotes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] SpecimenNoteVM vm, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                var dto = new SpecimenNoteDTO
                {
                    NoteCategoryId = vm.NoteCategoryId,
                    Note = vm.Note,
                    SpecimenId = vm.SpecimenId,
                };
                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        dto.File = reader.ReadBytes(upload.ContentLength);
                        dto.FileName = upload.FileName;
                    }
                }
                var SpecimenNote = _dataService.SpecimenNoteInsert(new MessageBase<SpecimenNoteDTO>() { Data = dto });
                if (SpecimenNote.Success)
                {
                    return RedirectToAction("Index", new { id = vm.SpecimenId });
                }
                else
                {
                    // * TODO: add error message
                }
            }
            GetCreateRequiredData();
            var spec = _dataService.Specimen(new MessageBase<int>() { Data = vm.SpecimenId });
            var csid = string.Empty;
            if (spec.Success)
            {
                csid = spec.Data.CSID;
                vm.CSID = csid;
            }
            return View(vm);
        }

        // GET: Specimens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SpecimenNoteVM vm = null;
            var dto = _dataService.SpecimenNote(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {

                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenNoteVM(dto.Data, uro, uie);
                GetCreateRequiredData(vm);

            }
            else
            {
                return HttpNotFound();
            }

            return View(vm);
        }

        private void GetCreateRequiredData(SpecimenNoteVM vm)
        {

            var NoteCategories = _dataService.NoteCategoryList().Data;
            if (NoteCategories == null)
            {
                NoteCategories = new List<NoteCategoryDTO>();
            }

            var remove = NoteCategories.Where(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
            if (remove != null && remove.Id != vm.NoteCategoryId)
            {
                NoteCategories.Remove(remove);
            }

            ViewBag.NoteCategoryId = new SelectList(NoteCategories, "Id", "DisplayName", vm.NoteCategoryId);

        }

        // POST: Specimens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] SpecimenNoteVM vm, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (Request.Form["removeFile"] != null)
                {
                    var remove = _dataService.SpecimenNoteFile_Remove(new MessageBase<int>() { Data = vm.Id });
                    if (remove.Success)
                    {
                        return RedirectToAction("Edit", new { id = vm.Id });
                    }
                    else
                    {
                        ModelState.AddModelError("", remove.GetDetailedErrorString());
                        GetCreateRequiredData(vm);
                        var spec = _dataService.Specimen(new MessageBase<int>() { Data = vm.SpecimenId });
                        var csid = string.Empty;
                        if (spec.Success)
                        {
                            csid = spec.Data.CSID;
                            vm.CSID = csid;
                        }
                        return View(vm);
                    }
                }


                var dto = new SpecimenNoteDTO
                {
                    Id = vm.Id,
                    NoteCategoryId = vm.NoteCategoryId,
                    Note = vm.Note,
                    SpecimenId = vm.SpecimenId,
                };
                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        dto.File = reader.ReadBytes(upload.ContentLength);
                        dto.FileName = upload.FileName;
                    }
                }

                var SpecimenNote = _dataService.SpecimenNoteEdit(new MessageBase<SpecimenNoteDTO>() { Data = dto });
                if (SpecimenNote.Success)
                {
                    return RedirectToAction("Index", new { id = vm.SpecimenId });
                }
                else
                {
                    ModelState.AddModelError("", SpecimenNote.GetDetailedErrorString());
                    GetCreateRequiredData(vm);
                    var spec = _dataService.Specimen(new MessageBase<int>() { Data = vm.SpecimenId });
                    var csid = string.Empty;
                    if (spec.Success)
                    {
                        csid = spec.Data.CSID;
                        vm.CSID = csid;
                    }
                }
            }
            return View(vm);
        }

        // GET: SpecimenNotes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecimenNoteVM vm = null;
            var dto = _dataService.SpecimenNote(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                var uro = _dataService.UserReadOnly().Success;
                var uie = _dataService.UserEditor().Success;
                vm = new SpecimenNoteVM(dto.Data, uro, uie);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: SpecimenNotes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = _fieldlist)] SpecimenNoteVM vm)
        {
            var result = _dataService.SpecimenNoteDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index", new { id = vm.SpecimenId });
            }
            else
            {
                ModelState.AddModelError("", result.GetDetailedErrorString());
                vm = null;
                var dto = _dataService.SpecimenNote(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    var uro = _dataService.UserReadOnly().Success;
                    var uie = _dataService.UserEditor().Success;
                    vm = new SpecimenNoteVM(dto.Data, uro, uie);
                }
                else
                {
                    return HttpNotFound();
                }
                // * TODO: Create error message and display
                return View(vm);
            }

        }

        [HttpGet, ActionName("Download")]
        public ActionResult Download(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var dto = _dataService.SpecimenNoteFile(new MessageBase<int> { Data = id.Value });
            if (dto.Success && dto.Data.File != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = dto.Data.FileName,
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                //return File(dto.Data.File, @"application/octet-stream");
                return File(dto.Data.File, @"attachment");
            }
            return RedirectToAction("Index", new { id = dto.Data.SpecimenId });
        }
    }
}