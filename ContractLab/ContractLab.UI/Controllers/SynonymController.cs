﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    public class SynonymController : Controller
    {
        private readonly IContractLabDataService _dataService;
        public SynonymController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Condition
        public ActionResult Index(int? id)
        {
            var condition = _dataService.Condition(new MessageBase<int>() { Data = id.Value });
            if (condition.Success)
            {
                ViewBag.ConditionId = id;
                ViewBag.CategoryId = condition.Data.CategoryId;
            }
            var vm = new List<SynonymVM>();
            var dto = _dataService.SynonymList(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                foreach (var item in dto.Data)
	            {
                    vm.Add(new SynonymVM(item));
	            }                
            }
            return View(vm);
        }

        // GET: Aliquots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.CategoryId = id;
            SynonymVM vm = null;
            var dto = _dataService.Synonym(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new SynonymVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Aliquots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, [Bind(Include = "Id, CategoryId, ConditionId")] SynonymVM vm)
        {
            var result = _dataService.SynonymDelete(new MessageBase<int>() { Data = id });
            if (result.Success)
            {
                return RedirectToAction("Index", new { id = vm.ConditionId });
            }
            else
            {
                vm = null;
                var dto = _dataService.Synonym(new MessageBase<int>() { Data = id });
                if (dto.Success)
                {
                    vm = new SynonymVM(dto.Data);
                }
                else
                {
                    return HttpNotFound();
                }
                
                return View(vm);
            }
        }

        // GET: Synonym/Create
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vm = new SynonymVM() { ConditionId = id.Value };
            var condition = _dataService.Condition(new MessageBase<int>() { Data = id.Value });
            if (condition.Success)
            {
                vm.CategoryId = condition.Data.CategoryId;
                vm.ConditionName = condition.Data.Name;
                vm.ConditionId = condition.Data.Id;
                vm.LitsQuestionId = condition.Data.LitsQuestionId;
                vm.Status = condition.Data.Status;
            }
            return View(vm);
        }

        // POST: Synonym/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, CategoryId, ConditionId, LitsQuestionId, Sorter, Status, ConditionName")] SynonymVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new SynonymDTO { Name = vm.Name, CategoryId = vm.CategoryId, ConditionId = vm.ConditionId, LitsQuestionId = vm.LitsQuestionId, Status = vm.Status, ConditionName = vm.ConditionName };
                var project = _dataService.SynonymInsert(new MessageBase<SynonymDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index", new { id = vm.ConditionId });
                }
                else
                {
                    ModelState.AddModelError("", project.GetDetailedErrorString());
                }
            }
            return View(vm);
        }

        // GET: Synonym/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SynonymVM vm = null;
            var dto = _dataService.Synonym(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new SynonymVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Synonym/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, CategoryId, ConditionId, LitsQuestionId, Sorter, Status")] SynonymVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new SynonymDTO { Id = vm.Id, Name = vm.Name, CategoryId = vm.CategoryId, ConditionId = vm.ConditionId, LitsQuestionId = vm.LitsQuestionId, Status = vm.Status };
                var project = _dataService.SynonymEdit(new MessageBase<SynonymDTO>() { Data = dto });
                if (project.Success)
                {
                    return RedirectToAction("Index", new { id = vm.ConditionId });
                }
                else
                {
                    ModelState.AddModelError("", project.GetDetailedErrorString());
                }
            }
            return View(vm);
        }

    }
}