﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class WHOImportController : Controller
    {
        private readonly IContractLabDataService  _dataService;
        public WHOImportController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: WHOImport
        public ActionResult Import()
        {
            var vm = new WHOImportVM();
            var lab = _dataService.UserHasLab();
            if (lab.Success && !String.IsNullOrEmpty(lab.Data.ImportPropertyOrder))
            {
                vm.PropertyOrder = lab.Data.ImportPropertyOrder;
            }
            else
            {
                vm.PropertyOrder = @"RowNumber
SpecimenID
DateCollected
SpecimenSource
ReasonForSubmission
PatientAge
PatientAgeUnits
PatientSex
Country
State
County
City
PCRInfA
PCRpdmInfA
PCRpdmH1
PCRH3
PCRInfB
PCRBVic
PCRBYam
SubType
PassageHistory
Comments
Deceased
NursingHome
Vaccinated
TravelCountry
TravelState
TravelCounty
TravelCity
TravelReturnDate
";
            }
            vm.ColumnStart = 1;
            vm.RowStart = 2;

            #region package
            var packages = _dataService.PackageList().Data;
            if (packages == null)
            {
                packages = new List<PackageDTO>();
            }
            packages = packages.Where(s => s.IsVisible).OrderByDescending(s => s.Id).ToList();
            ViewBag.PackageId = new SelectList(packages, "Id", "ComboDisplay");
            #endregion

            ViewBag.SubmitLabel = "Validate";

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Import([Bind(Include = "PackageId, PackageName, ColumnStart, ColumnEnd, RowStart, RowEnd, PropertyOrder, FileName, IsError, WHOSpecimens")] WHOImportVM vm, HttpPostedFileBase upload)
        {
            #region package
            var items = _dataService.PackageList().Data;
            if (items == null)
            {
                items = new List<PackageDTO>();
            }
            items = items.Where(s => s.IsVisible || s.Id == vm.PackageId).OrderByDescending(s => s.Id).ToList();
            ViewBag.PackageId = new SelectList(items, "Id", "ComboDisplay", vm.PackageId);
            #endregion  
            ViewBag.SubmitLabel = "Validate";
            if (Request.Form["Validate"] != null)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var dto = new WHOImportDTO();
                    dto.ColumnEnd = vm.ColumnEnd;
                    dto.ColumnStart = vm.ColumnStart;
                    dto.IsError = vm.IsError;
                    dto.PackageId = vm.PackageId;
                    dto.PackageName = vm.PackageName;
                    dto.PropertyOrder = vm.PropertyOrder;
                    dto.RowEnd = vm.RowEnd;
                    dto.RowStart = vm.RowStart;
                    dto.FileName = System.IO.Path.GetFileName(upload.FileName);
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        dto.File = reader.ReadBytes(upload.ContentLength);
                    }
                    var result = _dataService.ValidateWHO(new MessageBase<WHOImportDTO>() { Data = dto });



                    if (result.Success)
                    {
                        var newvm = new WHOImportVM(result.Data);

                        if (result.Data.IsError)
                        {
                            ViewBag.SubmitLabel = "Validate";
                        }
                        else
                        {
                            ViewBag.SubmitLabel = "Import";
                        }
                        return View(newvm);
                    }
                    else
                    {
                        ViewBag.SubmitLabel = "Validate";
                    }
                }
            }
            else if (Request.Form["Import"] != null)
            {
                var dto = new WHOImportDTO();
                dto.ColumnEnd = vm.ColumnEnd;
                dto.ColumnStart = vm.ColumnStart;
                dto.IsError = vm.IsError;
                dto.PackageId = vm.PackageId;
                dto.PackageName = vm.PackageName;
                dto.PropertyOrder = vm.PropertyOrder;
                dto.RowEnd = vm.RowEnd;
                dto.RowStart = vm.RowStart;
                dto.WHOSpecimens = new List<WHOSpecimenDTO>();
                foreach (var item in vm.WHOSpecimens)
                {
                    dto.WHOSpecimens.Add(new WHOSpecimenDTO()
                    {
                        SpecimenID = item.SpecimenID,
                        SpecimenID_Error = item.SpecimenID_Error,
                        DateCollected = item.DateCollected,
                        DateCollected_Error = item.DateCollected_Error,
                        PatientAge = item.PatientAge,
                        PatientAgeUnits_Error = item.PatientAgeUnits_Error,
                        PatientAgeUnits = item.PatientAgeUnits,
                        PatientAge_Error = item.PatientAge_Error,
                        PatientSex = item.PatientSex,
                        PatientSex_Error = item.PatientSex_Error,
                        County = item.County,
                        County_Error = item.County_Error,
                        State = item.State,
                        State_Error = item.State_Error,
                        SpecimenSource = item.SpecimenSource,
                        SpecimenSource_Error = item.SpecimenSource_Error,
                        SubType = item.SubType,
                        SubType_Error = item.SubType_Error,
                        ReasonForSubmission = item.ReasonForSubmission,
                        ReasonForSubmission_Error = item.ReasonForSubmission_Error,
                        Deceased = item.Deceased,
                        Deceased_Error = item.Deceased_Error,
                        NursingHome = item.NursingHome,
                        NursingHome_Error = item.NursingHome_Error,
                        Vaccinated = item.Vaccinated,
                        Vaccinated_Error = item.Vaccinated_Error,
                        Comments = item.Comments,
                        Comments_Error = item.Comments_Error,
                        IsError = item.IsError,
                        PackageId = vm.PackageId,
                        PackageName = vm.PackageName,
                        RowError = item.RowError,
                        RowNumber = item.RowNumber,

                        TravelCountry = item.TravelCountry,
                        TravelCountry_Error = item.TravelCountry_Error,
                        TravelState = item.TravelState,
                        TravelState_Error = item.TravelState_Error,
                        TravelCounty = item.TravelCounty,
                        TravelCounty_Error = item.TravelCounty_Error,
                        TravelCity = item.TravelCity,
                        TravelCity_Error = item.TravelCity_Error,
                        HostCellLine = item.HostCellLine,
                        HostCellLine_Error = item.HostCellLine_Error,
                        Substrate = item.Substrate,
                        Substrate_Error = item.Substrate_Error,
                        PassageNumber = item.PassageNumber,
                        PassageNumber_Error = item.PassageNumber_Error,
                        PCRInfA = item.PCRInfA,
                        PCRInfA_Error = item.PCRInfA_Error,
                        PCRpdmInfA = item.PCRpdmInfA,
                        PCRpdmInfA_Error = item.PCRpdmInfA_Error,
                        PCRpdmH1 = item.PCRpdmH1,
                        PCRpdmH1_Error = item.PCRpdmH1_Error,
                        PCRH3 = item.PCRH3,
                        PCRH3_Error = item.PCRH3_Error,
                        PCRInfB = item.PCRInfB,
                        PCRInfB_Error = item.PCRInfB_Error,
                        PCRBVic = item.PCRBVic,
                        PCRBVic_Error = item.PCRBVic_Error,
                        PCRBYam = item.PCRBYam,
                        PCRBYam_Error = item.PCRBYam_Error,
                        City = item.City,
                        City_Error = item.City_Error,
                        TravelReturnDate = item.TravelReturnDate,
                        TravelReturnDate_Error = item.TravelReturnDate_Error,
                        Country = item.Country,
                        Country_Error = item.Country_Error,

                        PassageHistory = item.PassageHistory,
                        PassageHistory_Error = item.PassageHistory_Error,

                        WhoSubmittedSubType = item.WHOSubmittedSubtype,
                        WhoSubmittedSubType_Error = item.WHOSubmittedSubtype_Error,

                    });
                }
                var result = _dataService.ImportWHO(new MessageBase<WHOImportDTO>() { Data = dto });
                if (result.Success)
                {
                    TempData["SpecimenCount"] = vm.WHOSpecimens.Count;
                    return RedirectToAction("Details", "Packages", new { id = vm.PackageId });
                }
                else
                {
                    ViewBag.SubmitLabel = "Import";
                    return View(vm);
                }
            }
            return View(vm);
        }
    }
}