﻿using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.Controllers
{
    
    public class HomeController : Controller
    {

        private readonly IContractLabDataService  _dataService;
        private readonly ISettingsProvider _settings;
        public HomeController(IContractLabDataService dataService, ISettingsProvider settings)
        {
            _dataService = dataService;
            _settings = settings;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var version = _dataService.VersionInfo();
            if (version.Success)
            {
                ViewBag.Version = version.Data.Version.ToString();
                ViewBag.AppliedOn = version.Data.AppliedOn.HasValue ? version.Data.AppliedOn.ToString() : "";
            }
            else
            {
                ViewBag.Version = "Error";
                ViewBag.AppliedOn = "";
            }
            long csid = 0;
            string cuid = string.Empty;
            var tempcsid = _settings.GetMaxCSID();
            long.TryParse(tempcsid, out csid);
            cuid = _settings.GetMaxCUID();
            var counts = _dataService.GetRemainingCounts(new MessageBase<long>() { Data = csid }, new MessageBase<string>() { Data = cuid });
            if (counts.Data != null)
            {
                ViewBag.CSIDCount = counts.Data.Where(s => s.Name == "CSID").FirstOrDefault().Value;
                ViewBag.CUIDCount = counts.Data.Where(s => s.Name == "CUID").FirstOrDefault().Value;
            }

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}