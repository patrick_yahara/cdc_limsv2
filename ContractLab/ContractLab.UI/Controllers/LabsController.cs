﻿using ContractLab.DTO;
using ContractLab.UI.Models.ContractLab;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Yahara.ServiceInfrastructure.Messages;
using System.Text;

namespace ContractLab.UI.Controllers
{
    [Authorize]
    public class LabsController : Controller
    {
        private const string _fieldlist = @"Name, Id, PrinterURI, LabelPreamble, LabelPostamble, LabelFormatString, WHOLabelFormatString, StoredLocallyCodes, AliquotVersionCode, LabelTestCodeOrder, ImportPropertyOrder, GrownTestCodeList, IttTestCodeList";

        private readonly IContractLabDataService _dataService;
        public LabsController(IContractLabDataService dataService)
        {
            _dataService = dataService;
        }

        // GET: Labs
        public ActionResult Index()
        {
            var vm = new List<LabVM>();
            var dto = _dataService.LabList();
            if (dto.Success)
            {
                var Labs = dto.Data as List<LabDTO>;
                Labs.ForEach(e => vm.Add(new LabVM(e)));
            }
            return View(vm);
        }

        // GET: Labs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LabVM vm = null;
            var dto = _dataService.Lab(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new LabVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // GET: Labs/Create
        public ActionResult Create()
        {
            //ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "SysName");
            return View();
        }

        // POST: Labs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = _fieldlist)] LabVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new LabDTO { 
                    Name = vm.Name,
                    PrinterURI = vm.PrinterURI, 
                    LabelPreamble = vm.LabelPreamble, 
                    LabelPostamble = vm.LabelPostamble,
                    LabelFormatString = vm.LabelFormatString,
                    WHOLabelFormatString = vm.WHOLabelFormatString,
                    StoredLocallyCodes = vm.StoredLocallyCodes,
                    AliquotVersionCode = vm.AliquotVersionCode,
                    LabelTestCodeOrder = vm.LabelTestCodeOrder,
                    ImportPropertyOrder = vm.ImportPropertyOrder,
                    GrownTestCodeList = vm.GrownTestCodeList,
                    IttTestCodeList = vm.IttTestCodeList,
                };
                var Lab = _dataService.LabInsert(new MessageBase<LabDTO>() { Data = dto });
                if (Lab.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Labs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LabVM vm = null;
            var dto = _dataService.Lab(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new LabVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Labs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = _fieldlist)] LabVM vm)
        {
            if (ModelState.IsValid)
            {
                var dto = new LabDTO { 
                    Id = vm.Id,
                    Name = vm.Name,
                    PrinterURI = vm.PrinterURI,
                    LabelPreamble = vm.LabelPreamble,
                    LabelPostamble = vm.LabelPostamble,
                    LabelFormatString = vm.LabelFormatString,
                    WHOLabelFormatString = vm.WHOLabelFormatString,
                    StoredLocallyCodes = vm.StoredLocallyCodes,
                    AliquotVersionCode = vm.AliquotVersionCode,
                    LabelTestCodeOrder = vm.LabelTestCodeOrder,
                    ImportPropertyOrder = vm.ImportPropertyOrder,
                    GrownTestCodeList = vm.GrownTestCodeList,
                    IttTestCodeList = vm.IttTestCodeList,
                };
                var Lab = _dataService.LabEdit(new MessageBase<LabDTO>() { Data = dto });
                if (Lab.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(vm);
        }

        // GET: Labs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LabVM vm = null;
            var dto = _dataService.Lab(new MessageBase<int>() { Data = id.Value });
            if (dto.Success)
            {
                vm = new LabVM(dto.Data);
            }
            else
            {
                return HttpNotFound();
            }
            return View(vm);
        }

        // POST: Labs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _dataService.LabDelete(new MessageBase<int>() { Data = id });
            return RedirectToAction("Index");
        }

        public FileResult LabConsumption()
        {
            var lstData = _dataService.GenerateLabConsumptionFile().Data;
            var sb = new StringBuilder();
            foreach (var data in lstData)
            {
                sb.AppendLine(string.Format(@"{0},{1},{2},{3},{4}", data.Month, data.Year, data.Lab, data.CSID, data.CUID));
            }
            return File(new UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "LabConsumption.csv");
}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
