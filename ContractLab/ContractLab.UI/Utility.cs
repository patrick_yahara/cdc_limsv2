﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ContractLab.DTO;

namespace ContractLab.UI
{
    public static class Utility
    {
        public static void WithBlank<T>(this IList<T> list)
        {
            list.Insert(0, Activator.CreateInstance<T>());
        }

        public static void WithYN(this IList<NameValueDTO> list)
        {
            list.Add(new NameValueDTO() { Name = "Y", Value = "Y" });
            list.Add(new NameValueDTO() { Name = "N", Value = "N" });
        }

        public static void WithYNU(this IList<NameValueDTO> list)
        {
            list.WithYN();
            list.Insert(0, new NameValueDTO() { Name = "U", Value = "U" });
        }


    }
}