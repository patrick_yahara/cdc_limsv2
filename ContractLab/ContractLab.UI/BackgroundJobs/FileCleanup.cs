﻿using ContractLab.DataService.Interfaces;
using Hangfire;
using Hangfire.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ContractLab.UI.BackgroundJobs
{
    public static class FileCleanup
    {
        public static void CreateJobs(ISettingsProvider settings)
        {
            var manager = new RecurringJobManager();

            int deleteinterval = 0;
            // * hours
            int.TryParse(settings.GetErrorFileDeleteInterval(), out deleteinterval);
            if (deleteinterval <= 0)
            {
                deleteinterval = 72;
            }
            manager.AddOrUpdate("transmission-error-file-delete", Job.FromExpression(() => DeleteFiles(settings.GetTransmissionErrorDirectory(), deleteinterval)), Cron.Hourly());

            int.TryParse(settings.GetProcessedFileDeleteInterval(), out deleteinterval);
            // * hours
            if (deleteinterval <= 0)
            {
                deleteinterval = 72;
            }
            manager.AddOrUpdate("transmission-processed-file-delete", Job.FromExpression(() => DeleteFiles(settings.GetTransmissionProcessedDirectory(), deleteinterval)), Cron.Hourly());
        }

        public static void DeleteFiles(string path, int age)
        {
            if (!Directory.Exists(path))
            {
                return; 
            }
            var files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastWriteTimeUtc < DateTime.Now.AddHours(-age))
                    fi.Delete();
            }
        }
    }
}