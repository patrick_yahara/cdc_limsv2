﻿using ContractLab.DataService.Interfaces;
using Hangfire;
using Hangfire.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI.BackgroundJobs
{
    public static class TransmissionDrop
    {
        public static void CreateJobs(ISettingsProvider settings)
        {
            var manager = new RecurringJobManager();

            int messagesize = 10;
            // * objects
            int.TryParse(settings.GetTransmissionMessageSize(), out messagesize);
            if (messagesize <= 0)
            {
                messagesize = 10;
            }
            int queueinterval = 4;
            // * hours
            int.TryParse(settings.GetQueueFileCreateInterval(), out queueinterval);
            if (queueinterval <= 0)
            {
                queueinterval = 4;
            }
            manager.AddOrUpdate("transmission-queue-file-create", Job.FromExpression<IContractLabDataService_HttpContextFree>(s => s.GenerateTransmissionFile_Background(new MessageBase<int>() { Data = messagesize }, new MessageBase<string>() { Data = settings.GetTransmissionDropDirectory() })), Cron.HourInterval(queueinterval));
        }
    }
}