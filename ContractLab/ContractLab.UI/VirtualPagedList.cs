﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.UI
{
    public class VirtualPagedList<T> : IPagedList<T>
    {
        public List<T> Items { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int? ChangeStatus { get; set; }

        public VirtualPagedList() { }

        public VirtualPagedList(List<T> items, int itemsperpage, int totalcount, int totalpages, int currentpage)
        {
            this.Items = items;
            this.ItemsPerPage = itemsperpage;
            this.TotalCount = totalcount;
            this.TotalPages = totalpages;
            this.CurrentPage = currentpage;
        }

        public bool HasNextPage
        {
            get { return CurrentPage < TotalPages; }
        }

        public bool HasPreviousPage
        {
            get { return CurrentPage > 1; }
        }

        public bool IsFirstPage
        {
            get { return CurrentPage == 1; }
        }

        public bool IsLastPage
        {
            get { return CurrentPage == TotalPages; }
        }

        public int ItemEnd
        {
            get 
            {
                var value = CurrentPage * ItemsPerPage;
                if (TotalItemCount <  value)
                {
                    return TotalItemCount;
                }
                return value;
            }
        }

        public int ItemStart
        {
            get {
                    if (CurrentPage == 1)
                    {
                        return 1;
                    } 
                    else
                    {
                        if ((CurrentPage * ItemsPerPage - 1) > TotalItemCount)
                        {
                            return TotalItemCount;
                        }
                        return CurrentPage * ItemsPerPage - 1; 
                    }
                }
        }

        public int PageCount
        {
            get { return TotalPages; }
        }

        public int PageIndex
        {
            get { return CurrentPage; }
        }

        public int PageNumber
        {
            get { return CurrentPage; }
        }

        public int PageSize
        {
            get { return ItemsPerPage; }
        }

        public int TotalItemCount
        {
            get { return TotalCount; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }
}