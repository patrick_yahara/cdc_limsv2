﻿using ContractLab.DataService.Interfaces;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ContractLab.UI.Startup))]
namespace ContractLab.UI
{
    public partial class Startup
    {

        private readonly ISettingsProvider _settings;

        public Startup()
        {
            _settings = (ISettingsProvider)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ISettingsProvider));
        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureHangfire(app);
        }
    }
}
