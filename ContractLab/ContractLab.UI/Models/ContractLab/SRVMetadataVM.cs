﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    public class SRVMetadataVM
    {
        public SRVMetadataVM(PackageDTO dto)
        {
            this.FullName = dto.Contact.FullName;
            this.Address = dto.Contact.Address;
            this.City = dto.Contact.City;
            this.State = dto.Contact.State;
            this.Country = dto.Contact.Country;
            this.Phone = dto.Contact.Phone;
            this.Fax = dto.Contact.Fax;
            this.Email = dto.Contact.Email;
            Specimens = new List<SRVSpecimenVM>();
            foreach (var item in dto.Specimens)
            {
                Specimens.Add(new SRVSpecimenVM(item));
            }
            this.SpecimenCondition = dto.SpecimenCondition;
            this.DateReceived = dto.DateReceived;
        }

        [Display(Name = "Date Received")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateReceived { get; set; }

        [Display(Name = "Condition")]
        public String SpecimenCondition { get; set; }

        public String FullName { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Country { get; set; }

        [Display(Name = "Phone")]
        public String Phone { get; set; }
        [Display(Name = "Fax")]
        public String Fax { get; set; }
        [Display(Name = "Email")]
        public String Email { get; set; }
        
        public List<SRVSpecimenVM> Specimens { get; set; }
        
    }
}
