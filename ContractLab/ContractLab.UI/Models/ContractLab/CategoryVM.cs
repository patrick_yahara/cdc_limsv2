﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Category")]
    public class CategoryVM
    {
        public CategoryVM() { }
        public CategoryVM(CategoryDTO dto)
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.SynonymsCount = dto.SynonymsCount;
            this.LitsQuestionId = dto.LitsQuestionId;
            this.ConditionsCount = dto.ConditionsCount;
            this.AllowBlank = dto.AllowBlank;
        }
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "LITS")]
        public string LitsQuestionId { get; set; }
        [Display(Name = "Conditions")]
        public int ConditionsCount { get; set; }
        [Display(Name = "Synonyms")]
        public int SynonymsCount { get; set; }
        [Display(Name = "Allow Blank")]
        public bool AllowBlank { get; set; }
    }
}