﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Package")]
    public class PackageVM
    {
        public PackageVM() { IsVisible = true; AllowRUD = true; }
        public PackageVM(PackageDTO dto, bool IsUserReadOnly, bool IsUserEditor)
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.SenderId = dto.SenderId;
            this.Sender = dto.Sender.RasClientId;
            this.SpecimenCount = dto.SpecimenCount;
            this.CompanyName = dto.Sender.CompanyName;
            this.IsVisible = dto.IsVisible;
            this.CreatedOn = dto.CreatedOn;
            this.AllowRUD = !IsUserReadOnly;
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;                
            }
            this.Comments = dto.Comments;
            this.DateReceived = dto.DateReceived;
            this.SpecimenCondition = dto.SpecimenCondition;
        }

        [HiddenInput(DisplayValue = false)]
        public int? Id { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Sender Id")]
        public int SenderId { get; set; }

        [StringLength(30)]
        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "State")]
        public String State { get; set; }

        [Display(Name = "Sender Id")]
        public String Sender { get; set; }

        [Display(Name = "Company Name")]
        public String CompanyName { get; set; }

        [Display(Name = "Is Visible")]
        public bool IsVisible { get; set; }

        [Display(Name = "Comments")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [ReadOnly(true)]
        [DataType(DataType.Date)]
        [Display(Name = "Created On")]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Date Received")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateReceived { get; set; }

        [Display(Name = "Spec Condition")]
        public String SpecimenCondition { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Specimens")]
        public int SpecimenCount { get; set; }

        public bool AllowRUD { get; set; }

    }
}