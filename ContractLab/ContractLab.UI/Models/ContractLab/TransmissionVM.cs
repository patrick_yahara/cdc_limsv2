﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Transmission")]
    public class TransmissionVM
    {
        public TransmissionVM() { }
        public TransmissionVM(TransmissionDTO dto)
        {
            this.Id = dto.Id;
            this.CreatedOn = dto.CreatedOn;
            this.CreatedBy = dto.CreatedBy;
            this.CSID = dto.Specimen.CSID;
            if (dto.Isolate != null && dto.Isolate.IsolateType != null)
            {
                this.IsolateType = dto.Isolate.IsolateType.DisplayName;
            }
            if (dto.Aliquot != null)
            {
                this.CUID = dto.Aliquot.CUID;
            }
            this.SupercedeId = dto.SupercedeId;
            this.Payload = dto.Payload;
            this.Response = dto.Response;
            this.Attempts = dto.Attempts;
            this.TransmittedOn = dto.TransmittedOn;
        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "CSID")]
        public string CSID { get; set; }
        public int SpecimenId { get; set; }
        [Display(Name = "Isolate Type")]
        public string IsolateType { get; set; }
        [Display(Name = "CUID")]
        public string CUID { get; set; }
        [Display(Name = "Payload")]
        public string Payload { get; set; }
        [Display(Name = "Response")]
        public string Response { get; set; }
        [Display(Name = "Attempts")]
        public int? Attempts { get; set; }
        [Display(Name = "Transmitted On")]
        public DateTime? TransmittedOn { get; set; }

        [Display(Name = "Id")]
        public int IdDisplay { get { return Id;} }

        [Display(Name = "Supercede Id")]
        public int? SupercedeId { get; set; }

        public bool Selected { get; set; }
        
    }
}