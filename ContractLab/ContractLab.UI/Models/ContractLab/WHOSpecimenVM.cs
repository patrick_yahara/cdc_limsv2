﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ContractLab.DTO;
using System.ComponentModel.DataAnnotations;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("WHOSpecimen")]
    public class WHOSpecimenVM
    {
        public WHOSpecimenVM()
        { 
        }

        public WHOSpecimenVM(WHOSpecimenDTO dto)
        {
            this.SpecimenID = dto.SpecimenID;
            this.SpecimenID_Error = dto.SpecimenID_Error;
            this.DateCollected = dto.DateCollected;
            this.DateCollected_Error = dto.DateCollected_Error;
            this.DateCollectedDisplay = dto.DateCollected.HasValue ? dto.DateCollected.Value.ToShortDateString() : string.Empty;
            this.PatientAge = dto.PatientAge;
            this.PatientAge_Error = dto.PatientAge_Error;
            this.PatientAgeUnits = dto.PatientAgeUnits;
            this.PatientAgeUnits_Error = dto.PatientAgeUnits_Error;
            this.PatientSex = dto.PatientSex;
            this.PatientSex_Error = dto.PatientSex_Error;
            this.County = dto.County;
            this.County_Error = dto.County_Error;
            this.State = dto.State;
            this.State_Error = dto.State_Error;
            this.SpecimenSource = dto.SpecimenSource;
            this.SpecimenSource_Error = dto.SpecimenSource_Error;
            this.SubType = dto.SubType;
            this.SubType_Error = dto.SubType_Error;
            this.ReasonForSubmission = dto.ReasonForSubmission;
            this.ReasonForSubmission_Error = dto.ReasonForSubmission_Error;
            this.Deceased = dto.Deceased;
            this.Deceased_Error = dto.Deceased_Error;
            this.NursingHome = dto.NursingHome;
            this.NursingHome_Error = dto.NursingHome_Error;
            this.Vaccinated = dto.Vaccinated;
            this.Vaccinated_Error = dto.Vaccinated_Error;
            this.Comments = dto.Comments;
            this.Comments_Error = dto.Comments_Error;
            this.RowError = dto.RowError;
            this.RowNumber = dto.RowNumber;
            this.IsError = dto.IsError;

            this.TravelCountry = dto.TravelCountry;
            this.TravelCountry_Error = dto.TravelCountry_Error;
            this.TravelState = dto.TravelState;
            this.TravelState_Error = dto.TravelState_Error;
            this.TravelCounty = dto.TravelCounty;
            this.TravelCounty_Error = dto.TravelCounty_Error;
            this.TravelCity = dto.TravelCity;
            this.TravelCity_Error = dto.TravelCity_Error;
            this.HostCellLine = dto.HostCellLine;
            this.HostCellLine_Error = dto.HostCellLine_Error;
            this.Substrate = dto.Substrate;
            this.Substrate_Error = dto.Substrate_Error;
            this.PassageNumber = dto.PassageNumber;
            this.PassageNumber_Error = dto.PassageNumber_Error;
            this.PCRInfA = dto.PCRInfA;
            this.PCRInfA_Error = dto.PCRInfA_Error;
            this.PCRpdmInfA = dto.PCRpdmInfA;
            this.PCRpdmInfA_Error = dto.PCRpdmInfA_Error;
            this.PCRpdmH1 = dto.PCRpdmH1;
            this.PCRpdmH1_Error = dto.PCRpdmH1_Error;
            this.PCRH3 = dto.PCRH3;
            this.PCRH3_Error = dto.PCRH3_Error;
            this.PCRInfB = dto.PCRInfB;
            this.PCRInfB_Error = dto.PCRInfB_Error;
            this.PCRBVic = dto.PCRBVic;
            this.PCRBVic_Error = dto.PCRBVic_Error;
            this.PCRBYam = dto.PCRBYam;
            this.PCRBYam_Error = dto.PCRBYam_Error;
            this.City = dto.City;
            this.City_Error = dto.City_Error;
            this.TravelReturnDate = dto.TravelReturnDate;
            this.TravelReturnDate_Error = dto.TravelReturnDate_Error;
            this.TravelReturnDateDisplay = dto.TravelReturnDate.HasValue ? dto.TravelReturnDate.Value.ToShortDateString() : string.Empty;
            this.Country = dto.Country;
            this.Country_Error = dto.Country_Error;
            this.PassageHistory = dto.PassageHistory;
            this.PassageHistory_Error = dto.PassageHistory_Error;

            this.WHOSubmittedSubtype = dto.WhoSubmittedSubType;
            this.WHOSubmittedSubtype_Error = dto.WhoSubmittedSubType_Error;

        }

        public int RowNumber { get; set; }

        [Display(Name = "Specimen ID")]
        public string SpecimenID { get; set; }
        [Required]
        [Display(Name = "Date of Specimen Collection")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateCollected { get; set; }
        public string DateCollectedDisplay { get; set; }
        [Display(Name = "Age")]
        public string PatientAge { get; set; }
        [Display(Name = "Sex")]
        public string PatientSex { get; set; }
        [Display(Name = "Specimen Collection County")]
        public string County { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Display(Name = "Specimen Type")]
        public string SpecimenSource { get; set; }
        [Display(Name = "Type/Subtype")]
        public string SubType { get; set; }
        [Display(Name = "Reason for Submission")]
        public string ReasonForSubmission { get; set; }
        [Display(Name = "Fatal?")]
        public string Deceased { get; set; }
        [Display(Name = "Nursing Home?")]
        public string NursingHome { get; set; }
        [Display(Name = "Vaccinated?")]
        public string Vaccinated { get; set; }
        [Display(Name = "Comments")]
        public string Comments { get; set; }

        public string SpecimenID_Error { get; set; }
        public string DateCollected_Error { get; set; }
        public string PatientAge_Error { get; set; }
        public string PatientSex_Error { get; set; }
        public string County_Error { get; set; }
        public string State_Error { get; set; }
        public string SpecimenSource_Error { get; set; }
        public string SubType_Error { get; set; }
        public string ReasonForSubmission_Error { get; set; }
        public string Deceased_Error { get; set; }
        public string NursingHome_Error { get; set; }
        public string Vaccinated_Error { get; set; }
        public string Comments_Error { get; set; }

        [Display(Name = "TravelCountry")]
        public string TravelCountry { get; set; }
        public string TravelCountry_Error { get; set; }

        [Display(Name = "TravelState")]
        public string TravelState { get; set; }
        public string TravelState_Error { get; set; }

        [Display(Name = "TravelCounty")]
        public string TravelCounty { get; set; }
        public string TravelCounty_Error { get; set; }

        [Display(Name = "TravelCity")]
        public string TravelCity { get; set; }
        public string TravelCity_Error { get; set; }

        [Display(Name = "HostCellLine")]
        public string HostCellLine { get; set; }
        public string HostCellLine_Error { get; set; }

        [Display(Name = "Substrate")]
        public string Substrate { get; set; }
        public string Substrate_Error { get; set; }

        [Display(Name = "PassageNumber")]
        public string PassageNumber { get; set; }
        public string PassageNumber_Error { get; set; }

        [Display(Name = "PassageHistory")]
        public string PassageHistory { get; set; }
        public string PassageHistory_Error { get; set; }

        [Display(Name = "PCRInfA")]
        public string PCRInfA { get; set; }
        public string PCRInfA_Error { get; set; }

        [Display(Name = "PCRpdmInfA")]
        public string PCRpdmInfA { get; set; }
        public string PCRpdmInfA_Error { get; set; }

        [Display(Name = "PCRpdmH1")]
        public string PCRpdmH1 { get; set; }
        public string PCRpdmH1_Error { get; set; }

        [Display(Name = "PCRH3")]
        public string PCRH3 { get; set; }
        public string PCRH3_Error { get; set; }

        [Display(Name = "PCRInfB")]
        public string PCRInfB { get; set; }
        public string PCRInfB_Error { get; set; }

        [Display(Name = "PCRBVic")]
        public string PCRBVic { get; set; }
        public string PCRBVic_Error { get; set; }

        [Display(Name = "PCRBYam")]
        public string PCRBYam { get; set; }
        public string PCRBYam_Error { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }
        public string City_Error { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? TravelReturnDate { get; set; }
        public string TravelReturnDate_Error { get; set; }

        public string TravelReturnDateDisplay { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }
        public string Country_Error { get; set; }

        [Display(Name = "Age Units")]
        public string PatientAgeUnits { get; set; }
        public string PatientAgeUnits_Error { get; set; }

        [Display(Name = "WHO Submitted Subtype")]
        public string WHOSubmittedSubtype { get; set; }
        public string WHOSubmittedSubtype_Error { get; set; }


        [Display(Name = "Error")]
        private bool _error;
        public bool IsError { get {
            if (!_error)
            {
                return _error;
            }
            bool localE = false;
            if (!string.IsNullOrEmpty(SpecimenID_Error))
            {
                localE = true; 
            }
            if (!string.IsNullOrEmpty(DateCollected_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PatientAge_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PatientSex_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(County_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(State_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(SpecimenSource_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(SubType_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(ReasonForSubmission_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(Deceased_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(NursingHome_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(Vaccinated_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(Comments_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(TravelCountry_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(TravelState_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(TravelCounty_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(TravelCity_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(HostCellLine_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(Substrate_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PassageNumber_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRInfA_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRpdmInfA_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRpdmH1_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRH3_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRInfB_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRBVic_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PCRBYam_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(City_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(TravelReturnDate_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(Country_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PatientAgeUnits_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(PassageHistory_Error))
            {
                localE = true;
            }
            if (!string.IsNullOrEmpty(WHOSubmittedSubtype_Error))
            {
                localE = true;
            }
            _error = localE;
            return _error; 
        } set { _error = value; } }

        public string RowError { get; set; }
    }
}