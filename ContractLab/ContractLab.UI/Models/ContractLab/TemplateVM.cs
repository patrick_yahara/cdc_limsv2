﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Template")]
    public class TemplateVM
    {
        public TemplateVM()
        { }
        public TemplateVM(TemplateDTO dto)
        {
            this.Id = dto.Id;
            this.FileName = dto.FileName;
            this.TemplateType = dto.TemplateType;
            this.File = dto.File;
            if (dto.Lab != null)
            {
                Lab = dto.Lab.Name;
                LabId = dto.LabId;
            }
        }
        public int Id { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "Type")]
        public string TemplateType { get; set; }
        public byte[] File { get; set; }
        [Display(Name = "Lab")]
        public int? LabId { get; set; }
        public string Lab { get; set; }
    }
}