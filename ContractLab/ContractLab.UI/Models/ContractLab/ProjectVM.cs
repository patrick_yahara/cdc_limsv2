﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Project")]
    public class ProjectVM
    {
        public ProjectVM()
        {

        }
        public ProjectVM(ProjectDTO dto)  : this()
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.CreatedOn = dto.CreatedOn;
            this.SpecimenCount = dto.SpecimenCount;
        }

        [HiddenInput(DisplayValue=false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Created")]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOn { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Specimens")]
        public int SpecimenCount { get; set; }

    }
}