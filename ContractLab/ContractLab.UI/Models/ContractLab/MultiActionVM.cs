﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    public class MultiActionVM
    {
        public MultiActionVM()
        { 
        }
        public int ParentId { get; set; }
        public List<MultiActionItemVM> Items { get; set; }   
    }
}