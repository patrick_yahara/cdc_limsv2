﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Manifest")]
    public class ManifestVM
    {
        public ManifestVM() { IsVisible = true; AllowRUD = true; Aliquots = new List<AliquotVM>();  }
        public ManifestVM(ManifestDTO dto, bool IsUserReadOnly, bool IsUserEditor)
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.AliquotCount = dto.AliquotCount;
            this.IsVisible = dto.IsVisible;
            this.CreatedOn = dto.CreatedOn;
            this.AllowRUD = !IsUserReadOnly;
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;
            }
            this.StatusId = dto.StatusId;
            this.Status = dto.Status.DisplayName;
            this.Comments = dto.Comments;
            this.DateShipped = dto.DateShipped;
            this.TrackingNumber = dto.TrackingNumber;
            Aliquots = new List<AliquotVM>();
            foreach (var item in dto.Aliquots)
            {
                Aliquots.Add(new AliquotVM(item, IsUserReadOnly, IsUserEditor));
            }
        }

        [HiddenInput(DisplayValue = false)]
        public int? Id { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Is Visible")]
        public bool IsVisible { get; set; }

        [ReadOnly(true)]
        [DataType(DataType.Date)]
        [Display(Name = "Created On")]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOn { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Number of Aliquots")]
        public int AliquotCount { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Status")]
        public String Status { get; set; }

        [Display(Name = "Comments")]
        [DataType(DataType.MultilineText)]
        public String Comments { get; set; }

        [Display(Name = "Aliquots")]
        public List<AliquotVM> Aliquots { get; set; }
        public List<int> AliquotIds { get; set; }

        public bool Selected { get; set; }
        public bool AllowRUD { get; set; }

        public LabVM Lab { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Shipped")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateShipped { get; set; }

        [Display(Name = "Tracking")]
        public string TrackingNumber { get; set; }

    }
}