﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Aliquot")]
    public class AliquotVM
    {
        public AliquotVM() { AllowRUD = false; }
        public AliquotVM(AliquotDTO dto, bool IsUserReadOnly, bool IsUserEditor)
        {
            this.Id = dto.Id;
            this.IsolateId = dto.IsolateId;
            this.CUID = dto.CUID;
            this.TestCode = dto.TestCode;
            this.AllowRUD = dto.Isolate.Specimen.Status.SysName.Equals("INPROGRESS") && !IsUserReadOnly;
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;    
            }
            this.AllowC = !IsUserReadOnly;
            if (!this.AllowC)
            {
                this.AllowC = IsUserEditor;                
            }
            this.CSID = dto.Isolate.Specimen.CSID;
            this.SubType = dto.Isolate.Specimen.SubType;
            this.ManifestId = dto.ManifestId;
            this.Manifest = dto.Manifest != null ? dto.Manifest.ComboDisplay : String.Empty;

            this.SpecimenId = dto.Isolate.Specimen.SpecimenID;
            this.DateCollected = dto.Isolate.Specimen.DateCollected;
            
            this.DateHarvested = dto.Isolate.DateHarvested;

            this.IsStoredLocally = dto.IsStoredLocally;
            this.IsStoredLocallyDisplay = dto.IsStoredLocally.ToString();

            this.TubeCount = dto.TubeCount;

            this.StatusId = dto.StatusId;
            this.Status = dto.Status != null ? dto.Status.DisplayName : string.Empty;

        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Isolate Id")]
        public int IsolateId { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "CUID")]
        public String CUID { get; set; }

        [Display(Name = "Test Code")]
        public String TestCode { get; set; }

        public bool Selected { get; set; }
        public bool AllowRUD { get; set; }
        public bool AllowC { get; set; }

        [ReadOnly(true)]
        [Display(Name = "CSID")]
        public string CSID { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Specimen Id")]
        public string SpecimenId { get; set; }

        [ReadOnly(true)]
        [Display(Name = "(Sub)Type")]
        public String SubType { get; set; }

        [Display(Name = "Date Harvested")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public DateTime? DateHarvested { get; set; }

        [Display(Name = "Date Collected")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateCollected { get; set; }

        [Display(Name = "Manifest")]
        public int? ManifestId { get; set; }

        [Display(Name = "Manifest")]
        public String Manifest { get; set; }

        [Display(Name = "Stored Locally")]
        public bool IsStoredLocally { get; set; }

        [Display(Name = "Stored Locally")]
        public string IsStoredLocallyDisplay { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Tube Count")]
        public int TubeCount { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Status")]
        public String Status { get; set; }

    }
}