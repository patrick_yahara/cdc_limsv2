﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Lab")]
    public class LabVM
    {
        public LabVM() { }
        public LabVM(LabDTO dto)
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.PrinterURI = dto.PrinterURI;
            this.LabelPreamble = dto.LabelPreamble;
            this.LabelPostamble = dto.LabelPostamble;
            this.LabelFormatString = dto.LabelFormatString;
            this.WHOLabelFormatString = dto.WHOLabelFormatString;
            this.StoredLocallyCodes = dto.StoredLocallyCodes;
            this.AliquotVersionCode = dto.AliquotVersionCode;
            this.LabelTestCodeOrder = dto.LabelTestCodeOrder;
            this.ImportPropertyOrder = dto.ImportPropertyOrder;
            this.GrownTestCodeList = dto.GrownTestCodeList;
            this.IttTestCodeList = dto.IttTestCodeList;
        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Printer URI")]
        public String PrinterURI { get; set; }

        [Display(Name = "Label Preamble")]
        [DataType(DataType.MultilineText)]
        public String LabelPreamble { get; set; }

        [Display(Name = "Label Postamble")]
        [DataType(DataType.MultilineText)]
        public String LabelPostamble { get; set; }

        [Display(Name = "Label Format String")]
        [DataType(DataType.MultilineText)]
        public String LabelFormatString { get; set; }

        [Display(Name = "WHO Label Format String")]
        [DataType(DataType.MultilineText)]
        public String WHOLabelFormatString { get; set; }

        [Display(Name = "Stored Locally Codes")]
        [DataType(DataType.MultilineText)]
        public string StoredLocallyCodes { get; set; }

        [Display(Name = "Aliquot Version Code")]
        public String AliquotVersionCode { get; set; }

        [Display(Name = "Label Order")]
        public String LabelTestCodeOrder { get; set; }

        [Display(Name = "Import Order")]
        [DataType(DataType.MultilineText)]
        public string ImportPropertyOrder { get; set; }

        [Display(Name = "Grown Test Codes")]
        public String GrownTestCodeList { get; set; }

        [Display(Name = "ITT Test Codes")]
        public String IttTestCodeList { get; set; }
    }
}