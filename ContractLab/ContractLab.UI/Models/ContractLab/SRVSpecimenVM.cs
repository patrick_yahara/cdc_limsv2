﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    public class SRVSpecimenVM
    {
        public SRVSpecimenVM(SpecimenDTO dto)
        {
            this.CSID = dto.CSID;
            this.SampleClass = dto.SampleClass;
            this.SpecimenId = dto.SpecimenID;
            this.DateCollected = dto.DateCollected;
            this.SubType = dto.SubType;
        }

        [ReadOnly(true)]
        [Display(Name = "CSID")]
        public String CSID { get; set; }

        [Display(Name = "Sample Class")]
        public string SampleClass { get; set; }

        [Display(Name = "Specimen ID")]
        public String SpecimenId { get; set; }

        [Display(Name = "Date Collected")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateCollected { get; set; }

        [Display(Name = "(Sub)Type")]
        public String SubType { get; set; }
    }
}