﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Sender")]
    public class SenderCompositeVM
    {
        public SenderCompositeVM()
        { }

        public SenderCompositeVM(SenderCompositeDTO dto)
        {
            this.SenderId = dto.Sender.Id;
            this.SenderVersionColumn = dto.Sender.VersionColumn;
            this.CompanyName = dto.Sender.CompanyName;
            this.Address = dto.Sender.Address;
            this.City = dto.Sender.City;
            this.State = dto.Sender.State;
            this.Country = dto.Sender.Country;
            this.RasClientId = dto.Sender.RasClientId;
            this.FluSeason = dto.Sender.FluSeason;
            if (dto.Contact != null)
            {
                this.ContactId = dto.Contact.Id;
                this.ContactVersionColumn = dto.Contact.VersionColumn;
                this.FullName = dto.Contact.FullName;
                this.ContactAddress = dto.Contact.Address;
                this.ContactCity = dto.Contact.City;
                this.ContactState = dto.Contact.State;
                this.ContactCountry = dto.Contact.Country;
                this.Phone = dto.Contact.Phone;
                this.Fax = dto.Contact.Fax;
                this.Email = dto.Contact.Email;
                this.RasContactId = dto.Contact.RasContactId;
                this.ContactFluSeason = dto.Contact.FluSeason;
            }

        }
        [HiddenInput(DisplayValue = false)]
        public int SenderId { get; set; }
        public byte[] SenderVersionColumn { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Address")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "RasClientId")]
        public string RasClientId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int ContactId { get; set; }
        public byte[] ContactVersionColumn { get; set; }
        [Display(Name = "Contact Name")]
        public string FullName { get; set; }
        [Display(Name = "Contact Address")]
        [DataType(DataType.MultilineText)]
        public string ContactAddress { get; set; }
        [Display(Name = "Contact City")]
        public string ContactCity { get; set; }
        [Display(Name = "Contact State")]
        public string ContactState { get; set; }
        [Display(Name = "Contact Country")]
        public string ContactCountry { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "Fax")]
        public string Fax { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "RasContactId")]
        public string RasContactId { get; set; }
        [Display(Name = "Flu Season")]
        public int? FluSeason { get; set; }
        [Display(Name = "Contact Flu Season")]
        public int? ContactFluSeason { get; set; }
    }
}