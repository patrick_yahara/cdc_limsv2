﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("SpecimenNote")]
    public class SpecimenNoteVM
    {
        public SpecimenNoteVM()
        { }
        public SpecimenNoteVM(SpecimenNoteDTO dto, bool IsUserReadOnly, bool IsUserEditor)
        {
            this.Id = dto.Id;
            this.Note = dto.Note;
            this.FileName = dto.FileName;
            this.NoteCategory = dto.NoteCategory.DisplayName;
            this.NoteCategoryId = dto.NoteCategoryId;
            this.File = dto.File;
            this.AllowRUD = dto.Specimen.Status.SysName.Equals("INPROGRESS") && !IsUserReadOnly;
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;    
            }
            this.AllowC = !IsUserReadOnly;
            if (!AllowC)
            {
                this.AllowC = IsUserEditor;
            }
            this.SpecimenId = dto.SpecimenId;
            this.CSID = dto.Specimen.CSID;
            this.ModifiedOn = dto.ModifiedOn;
        }
        public int Id { get; set; }
        [Display(Name = "Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
        [Display(Name = "Category")]
        public string NoteCategory { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public bool AllowRUD { get; set; }
        public bool AllowC { get; set; }
        public int SpecimenId { get; set; }
        public int NoteCategoryId { get; set; }
        [Display(Name = "CSID")]
        public string CSID { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [ReadOnly(true)]
        [Display(Name = "Date")]
        public DateTime ModifiedOn { get; set; }
    }
}