﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    public class MultiActionItemVM
    {
        public MultiActionItemVM()
        { 
        }
        public int Id { get; set; }
        public bool Selected { get; set; }
    }
}