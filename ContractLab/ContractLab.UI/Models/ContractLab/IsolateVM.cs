﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Isolate")]
    public class IsolateVM
    {
        public IsolateVM() { AllowRUD = false; }
        public IsolateVM(IsolateDTO dto, bool IsUserReadOnly, bool IsUserEditor)
        {
            this.Id = dto.Id;
            this.SpecimenId = dto.SpecimenId;
            this.PassageLevel = dto.PassageLevel;
            this.DateHarvested = dto.DateHarvested;
            this.Titer = dto.Titer;
            this.RedBloodCellType = dto.RedBloodCellType;
            this.AliquotCount = dto.AliquotCount;
            this.IsolateTypeId = dto.IsolateType.Id;
            this.IsolateTypeName = dto.IsolateType.DisplayName;
            this.CSID = dto.Specimen.CSID;
            this.AllowRUD = dto.Specimen.Status.SysName.Equals("INPROGRESS") && !IsUserReadOnly;
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;                
            }
            this.AllowC = !IsUserReadOnly;
            if (!AllowC)
            {
                this.AllowC = IsUserEditor;                
            }
            this.SubType = dto.Specimen.SubType;
            this.Comments = dto.Comments;
            this.StatusId = dto.StatusId;
            this.Status =  dto.Status != null ? dto.Status.DisplayName : string.Empty;

        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Specimen Id")]
        public int SpecimenId { get; set; }

        [Display(Name = "Passage Level")]
        public String PassageLevel { get; set; }
        [Display(Name = "Date Harvested")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateHarvested { get; set; }
        [Display(Name = "Titer")]
        public int? Titer { get; set; }
        [Display(Name = "Red Blood Cell Type")]
        public String RedBloodCellType { get; set; }

        [Display(Name = "Type")]
        public int IsolateTypeId { get; set; }

        [Display(Name = "Type")]
        public String IsolateTypeName { get; set; }

        [ReadOnly(true)]
        [Display(Name = "CSID")]
        public string CSID { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Aliquots")]
        public int AliquotCount { get; set; }

        public bool Selected { get; set; }
        public bool AllowRUD { get; set; }
        public bool AllowC { get; set; }

        [ReadOnly(true)]
        [Display(Name = "(Sub)Type")]
        public String SubType { get; set; }

        [Display(Name = "Comments")]
        [DataType(DataType.MultilineText)]
        public String Comments { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Status")]
        public String Status { get; set; }


    }
}