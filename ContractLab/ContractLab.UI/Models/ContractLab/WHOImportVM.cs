﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ContractLab.DTO;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("WHOImport")]
    public class WHOImportVM
    {
        public WHOImportVM()
        {
            this.WHOSpecimens = new List<WHOSpecimenVM>();
        }

        public WHOImportVM(WHOImportDTO dto) : this()
        {
            this.ColumnEnd = dto.ColumnEnd;
            this.ColumnStart = dto.ColumnStart;
            this.RowEnd = dto.RowEnd;
            this.RowStart = dto.RowStart;
            this.FileName = dto.FileName;
            this.IsError = dto.IsError;
            this.PackageId = dto.PackageId;
            this.PackageName = dto.PackageName;
            this.PropertyOrder = dto.PropertyOrder;
     
            foreach (var item in dto.WHOSpecimens)
            {
                this.WHOSpecimens.Add(new WHOSpecimenVM(item));
            }
        }

        public List<WHOSpecimenVM> WHOSpecimens { get; set; }

        [Required(ErrorMessage = "A Package is required")]
        [Display(Name = "Package")]
        public int PackageId { get; set; }

        public string PackageName { get; set; }

        [Required(ErrorMessage = "A Start Column is required")]
        [Display(Name = "Start Column")]
        public int ColumnStart { get; set; }

        [Display(Name = "End Column")]
        public int? ColumnEnd { get; set; }

        [Required(ErrorMessage = "A Start Row is required")]
        [Display(Name = "Start Row")]
        public int RowStart { get; set; }
        
        [Display(Name = "End Row")]
        public int? RowEnd { get; set; }

        [DataType(DataType.MultilineText)]
        public string PropertyOrder { get; set; }

        [Display(Name = "File Name")]
        public string FileName { get; set; }
        public byte[] File { get; set; }

        public bool IsError { get; set; }
    }
}