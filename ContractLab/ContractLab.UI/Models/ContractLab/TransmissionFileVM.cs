﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    public class TransmissionFileVM
    {
        [Display(Name = "Directory")]
        public String DirectoryName { get; set; }
        [Display(Name = "Last Access")]
        public DateTime LastAccessTimeUtc { get; set; }
        [Display(Name = "Last Write")]
        public DateTime LastWriteTimeUtc { get; set; }
        [Display(Name = "Size (kb)")]
        public int Length { get; set; }
        [Display(Name = "Name")]
        public String Name { get; set; }
        public bool Selected { get; set; }
    }
}