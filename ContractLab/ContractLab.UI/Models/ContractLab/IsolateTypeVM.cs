﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("IsolateType")]
    public class IsolateTypeVM
    {
        public IsolateTypeVM(){}
        public IsolateTypeVM(IsolateTypeDTO dto)
        {
            this.Id = dto.Id;
            this.DisplayName = dto.DisplayName;
            this.SysName = dto.SysName;
            this.IsVisible = dto.IsVisible;
        }
        public int Id { get; set; }
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }
        [Display(Name = "System Name")]
        public string SysName { get; set; }
        [Display(Name = "Is Visible")]
        public bool IsVisible { get; set; }
    }
}