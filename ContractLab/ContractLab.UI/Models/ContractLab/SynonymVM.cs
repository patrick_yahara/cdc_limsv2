﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Synonym")]
    public class SynonymVM
    {
        public SynonymVM() { }
        public SynonymVM(SynonymDTO dto)
        {
            this.Id = dto.Id;
            this.LitsQuestionId = dto.LitsQuestionId;
            this.Name = dto.Name;
            this.Status = dto.Status;
            this.CategoryId = dto.CategoryId;
            this.ConditionId = dto.ConditionId;
            this.ConditionName = dto.ConditionName;
        }
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "LITS")]
        public string LitsQuestionId { get; set; }
        [Display(Name = "Category Id")]
        public int CategoryId { get; set; }
        [Display(Name = "Condition")]
        public string ConditionName { get; set; }
        [Display(Name = "Condition Id")]
        public int ConditionId { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}