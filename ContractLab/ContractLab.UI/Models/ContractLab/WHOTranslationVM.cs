﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("WHOTranslation")]
    public class WHOTranslationVM
    {
        public WHOTranslationVM() { }
        public WHOTranslationVM(WHOTranslationDTO dto) 
        {
            this.Id = dto.Id;
            this.Heading = dto.Heading;
            this.MatchExpression = dto.MatchExpression;
            this.ReplacementValue = dto.ReplacementValue;
            this.CommentHeading = dto.CommentHeading;
            this.CommentValue = dto.CommentValue;
        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Heading")]
        public string Heading { get; set; }
        [Display(Name = "Match")]
        [DataType(DataType.MultilineText)]
        public string MatchExpression { get; set; }
        [Display(Name = "Replacement")]
        [DataType(DataType.MultilineText)]
        public string ReplacementValue { get; set; }
        [Display(Name = "Comment Heading")]
        public string CommentHeading { get; set; }
        [Display(Name = "Comment")]
        [DataType(DataType.MultilineText)]
        public string CommentValue { get; set; }
    }
}