﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Condition")]
    public class ConditionVM
    {
        public ConditionVM() { }
        public ConditionVM(ConditionDTO dto)
        {
            this.Id = dto.Id;
            this.LitsQuestionId = dto.LitsQuestionId;
            this.Name = dto.Name;
            this.CategoryId = dto.CategoryId;
            this.CategoryName = dto.Category != null ? dto.Category.Name : string.Empty;
            this.Status = dto.Status;
            this.SynonymsCount = dto.SynonymsCount;
            this.Sorter = dto.Sorter;
        }

        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "LITS")]
        public string LitsQuestionId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Category Id")]
        public int CategoryId { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Display(Name = "Synonyms")]
        public int SynonymsCount { get; set; }
        [Display(Name = "Order")]
        public int Sorter { get; set; }

    }
}