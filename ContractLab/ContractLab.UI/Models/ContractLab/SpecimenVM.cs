﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models.ContractLab
{
    [Table("Specimen")]
    public class SpecimenVM
    {
        public SpecimenVM() { AllowRUD = false; }
        public SpecimenVM(SpecimenDTO dto, bool IsUserReadOnly, bool IsUserEditor) 
        {
            this.Id = dto.Id;
            this.CSID = dto.CSID;
            //this.CUID = dto.CUID;
            this.ProjectId = dto.ProjectId;
            this.IsolateCount = dto.IsolateCount;
            this.PackageId = dto.PackageId;
            this.PackageName = dto.Package.Name;
            this.SenderId = dto.Package.Sender.RasClientId;
            this.CountryId = dto.CountryId;
            this.CountryName = dto.Country.LongCode;
            this.StateId = dto.StateId;
            this.StateName = dto.State.Abbreviation;
            this.CountyId = dto.CountyId;
            this.CountyName = dto.County.Name;
            this.Activity = dto.Activity;
            this.DateCollected = dto.DateCollected;
            this.SpecimenId = dto.SpecimenID;
            this.PatientAge = dto.PatientAge;
            this.PatientAgeUnits = dto.PatientAgeUnits;
            this.PatientSex = dto.PatientSex;
            this.PassageHistory = dto.PassageHistory;
            this.SubType = dto.SubType;
            this.WHOSubmittedSubtype = dto.WHOSubmittedSubtype;
            this.SpecimenSource = dto.SpecimenSource;
            this.CompCSID1 = dto.CompCSID1;
            this.CompCSID2 = dto.CompCSID2;
            this.OtherLocation = dto.OtherLocation;
            this.DrugResistant = dto.DrugResistant;
            this.Deceased = dto.Deceased;
            this.NursingHome = dto.NursingHome;
            this.Vaccinated = dto.Vaccinated;
            this.Travel = dto.Travel;
            this.ReasonForSubmission = dto.ReasonForSubmission;
            this.Comments = dto.Comments;
            this.StatusId = dto.StatusId;
            this.Status = dto.Status.DisplayName;
            this.SampleClass = dto.SampleClass;
            this.Sender = dto.Package.Sender.CompanyName;
            this.FilterDate = dto.FilterDate;
            this.AllowRUD = dto.Status.SysName.Equals("INPROGRESS") && !IsUserReadOnly;
            // just fall through to override
            if (!AllowRUD)
            {
                this.AllowRUD = IsUserEditor;
            }
            this.DateInoculated = dto.DateInoculated;
            this.SpecialStudy = dto.SpecialStudy;

            this.TravelCountryName = dto.TravelCountry;
            this.TravelCountyName = dto.TravelCounty;
            this.TravelStateName = dto.TravelState;
            this.TravelCity = dto.TravelCity;
            this.HostCellLine = dto.HostCellLine;
            this.Substrate = dto.Substrate;
            this.PassageNumber = dto.PassageNumber;
            this.PCRInfA = dto.PCRInfA;
            this.PCRpdmInfA = dto.PCRpdmInfA;
            this.PCRpdmH1 = dto.PCRpdmH1;
            this.PCRH3 = dto.PCRH3;
            this.PCRInfB = dto.PCRInfB;
            this.PCRBVic = dto.PCRBVic;
            this.PCRBYam = dto.PCRBYam;
            this.City = dto.City;
            this.TravelReturnDate = dto.TravelReturnDate;

        }

        [Display(Name = "Travel Return")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? TravelReturnDate { get; set; }

        [HiddenInput(DisplayValue=false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Project Id")]
        public int? ProjectId { get; set; }

        [HiddenInput(DisplayValue = true)]
        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Status")]
        public String Status { get; set; }

        [ReadOnly(true)]
        [Display(Name = "CSID")]
        public String CSID { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Sub Samples")]
        public int IsolateCount { get; set; }

        [Required(ErrorMessage = "A Package is required")]
        [Display(Name = "Package")]
        public int PackageId { get; set; }

        [Display(Name = "Package")]
        public String PackageName { get; set; }

        [Display(Name = "Special Study")]
        public String SpecialStudy { get; set; }

        [Display(Name = "Sender Id")]
        public String SenderId { get; set; }

        [Display(Name = "Sender")]
        public String Sender{ get; set; }

        [Required]
        [Display(Name = "Country")]
        public int? CountryId { get; set; }

        [Display(Name = "Country")]
        public String CountryName { get; set; }

        [Display(Name = "State")]
        public int? StateId { get; set; }

        [Display(Name = "State")]
        public String StateName { get; set; }

        [Display(Name = "County")]
        public int? CountyId { get; set; }

        [Display(Name = "County")]
        public String CountyName { get; set; }

        [Display(Name = "Extent Activity")]
        public String Activity { get; set; }

        [Display(Name = "Date Collected")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateCollected { get; set; }

        [Display(Name = "Specimen ID")]
        public String SpecimenId { get; set; }

        [Display(Name = "Patient Age")]
        [DisplayFormat(DataFormatString = "{0:###}", ApplyFormatInEditMode=true)]
        public String PatientAge { get; set; }

        [Display(Name = "Patient Age Units")]
        public String PatientAgeUnits { get; set; }

        [Display(Name = "Patient Sex")]
        public String PatientSex { get; set; }

        [Display(Name = "Passage History")]
        public String PassageHistory { get; set; }

        [Display(Name = "(Sub)Type")]
        public String SubType { get; set; }

        [Display(Name = "WHO Submitted (Sub)Type")]
        public string WHOSubmittedSubtype { get; set; }

        [Display(Name = "Spec Source")]
        public String SpecimenSource { get; set; }

        [Display(Name = "Comp CSID 1")]
        public String CompCSID1 { get; set; }

        [Display(Name = "Comp CSID 2")]
        public String CompCSID2 { get; set; }

        [Display(Name = "Patient Location")]
        public string OtherLocation { get; set; }
        [Display(Name = "Drug Resistant")]
        public string DrugResistant { get; set; }
        [Display(Name = "Deceased")]
        public string Deceased { get; set; }
        [Display(Name = "Nursing Home")]
        public string NursingHome { get; set; }
        [Display(Name = "Vaccinated")]
        public string Vaccinated { get; set; }
        [Display(Name = "Travel")]
        public string Travel { get; set; }
        [Display(Name = "Reason For Submission")]
        public string ReasonForSubmission { get; set; }

        [Display(Name = "Sample Class")]
        public string SampleClass { get; set; }

        [Display(Name = "Comments")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        public bool Selected { get; set; }
        public bool AllowRUD { get; set; }

        [Display(Name = "Received/Created")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FilterDate { get; set; }

        [Display(Name = "Date Inoculated at Contract Lab")]
        [DataType(DataType.Date)]
        /* Jira CLAW-1 */
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateInoculated { get; set; }

        [Display(Name = "Travel Country")]
        public string TravelCountryName { get; set; }
        [Display(Name = "Travel State")]
        public string TravelStateName { get; set; }
        [Display(Name = "Travel County")]
        public string TravelCountyName { get; set; }
        [Display(Name = "Travel City")]
        public string TravelCity { get; set; }
        [Display(Name = "Host Cell Line")]
        public string HostCellLine { get; set; }
        [Display(Name = "Substrate")]
        public string Substrate { get; set; }
        [Display(Name = "Passage Number")]
        public string PassageNumber { get; set; }
        [Display(Name = "PCR InfA")]
        public string PCRInfA { get; set; }
        [Display(Name = "PCR InfA pdm")]
        public string PCRpdmInfA { get; set; }
        [Display(Name = "PCR H1 pdm")]
        public string PCRpdmH1 { get; set; }
        [Display(Name = "PCRH3")]
        public string PCRH3 { get; set; }
        [Display(Name = "PCR InfB")]
        public string PCRInfB { get; set; }
        [Display(Name = "PCR BVic")]
        public string PCRBVic { get; set; }
        [Display(Name = "PCR BYam")]
        public string PCRBYam { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
    }
}