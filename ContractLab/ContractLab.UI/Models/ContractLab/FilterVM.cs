﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models.ContractLab
{
    public class FilterVM
    {
        public FilterVM()
        { }
        public FilterVM(FilterDTO dto)
        {
            this.Id = dto.Id;
            this.Display = dto.Display;
            this.FilterText = dto.FilterText;
            this.BaseObject = dto.BaseObject;
            this.IsLabAvailable = dto.IsLabAvailable;
        }
        public int Id { get; set; }
        public string Display { get; set; }
        public string FilterText { get; set; }
        public string BaseObject { get; set; }
        public bool IsLabAvailable { get; set; }
    }
}