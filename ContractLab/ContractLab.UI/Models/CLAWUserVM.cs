﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContractLab.UI.Models
{
    public class CLAWUserVM
    {
        public CLAWUserVM() 
        {
            this.Roles = new List<string>();
            this.AllRoles = new List<SelectListItem>();        
        }

        public CLAWUserVM(AspNetUserDTO dto)
        {
            this.Id = dto.Id;
            this.Email = dto.Email;
            this.LabId = dto.LabId;
            this.LabName = dto.Lab != null ? dto.Lab.Name : string.Empty;
            this.ExternalId = String.Join(",", dto.AspNetUserLogins.Select(s => s.ProviderKey).ToArray());
            this.ExternalProvider = String.Join(",", dto.AspNetUserLogins.Select(s => s.LoginProvider).ToArray());
            this.HasPassword = !string.IsNullOrEmpty(dto.PasswordHash);
            this.RolesDisplay = String.Join(",", dto.AspNetRoles.Select(s => s.Name).ToArray());
            this.Roles = new List<string>();
            this.DateDisabled = dto.DateDisabled;
            foreach (var item in dto.AspNetRoles)
            {
                (this.Roles as List<string>).Add(item.Name);
            }
            this.AllRoles = new List<SelectListItem>();
        }

        [Display(Name = "Id")]
        public string Id { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Lab")]
        public int? LabId { get; set; }

        [Display(Name = "Lab")]
        public string LabName { get; set; }

        [Display(Name = "External Id")]
        public string ExternalId { get; set; }

        [Display(Name = "External Provider")]
        public string ExternalProvider { get; set; }

        [Display(Name = "Role List")]
        public string RolesDisplay { get; set; }

        [Display(Name = "Has Password")]
        public bool HasPassword { get; set; }

        [Display(Name = "Roles")]
        public IEnumerable<string> Roles { get; set; }

        public IEnumerable<SelectListItem> AllRoles { get; set; }

        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        [Display(Name = "Disabled")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateDisabled { get; set; }


    }
}