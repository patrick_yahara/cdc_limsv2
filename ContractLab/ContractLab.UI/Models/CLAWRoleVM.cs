﻿using ContractLab.DTO;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models
{
    public class CLAWRoleVM
    {

        public CLAWRoleVM(AspNetRoleDTO dto) : this()
        {
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.UserCount = dto.AspNetUsers.Count;
        }

        public CLAWRoleVM(IdentityRole role)
            : this()
        {
            this.Id = role.Id;
            this.Name = role.Name;
        }

        public CLAWRoleVM()
        {
            AspNetUsers = new List<CLAWUserVM>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public int UserCount { get; set; }
        public virtual ICollection<CLAWUserVM> AspNetUsers { get; set; }
    }
}