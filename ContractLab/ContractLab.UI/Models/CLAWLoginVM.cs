﻿using ContractLab.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractLab.UI.Models
{
    public class CLAWLoginVM
    {
        public CLAWLoginVM(){}
        public CLAWLoginVM(AspNetUserLoginDTO dto)
        {
            this.LoginProvider = dto.LoginProvider;
            this.ProviderKey = dto.ProviderKey;
            this.UserId = dto.UserId;
        }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string UserId { get; set; }
    }
}