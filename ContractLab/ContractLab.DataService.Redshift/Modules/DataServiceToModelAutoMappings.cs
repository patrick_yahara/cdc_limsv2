﻿using AutoMapper;

namespace ContractLab.DataService.Redshift.Modules
{
    /// <summary>
    /// A set of mappings from types in the FoundationAssembly.Model namespace to types 
    /// in the current namespace.
    /// </summary>
    public static class DataServiceToModelAutoMappings
    {
        public static void CreateMaps()
        {
            //Mapper.CreateMap<SourceType, DestinationType>();
        }
    }
}
