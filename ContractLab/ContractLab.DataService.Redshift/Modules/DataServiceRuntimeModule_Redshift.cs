﻿using Ninject;
using Ninject.Modules;
using ContractLab.DataService.Redshift.Interfaces;
using ContractLab.DataService.Interfaces;
using Yahara.Core.Repository;
using System.Security.Principal;
using ContractLab.Repository.Redshift;

namespace ContractLab.DataService.Redshift.Modules
{
    public class DataServiceRuntimeModule_Redshift : NinjectModule
    {
        public override void Load()
        {
            var settingsProvider = Kernel.Get<ISettingsProvider>();

            Bind<ContractLabRedshiftDbContextSource>().To<ContractLabRedshiftDbContextSource>()
                                    .InSingletonScope()
                                    .WithConstructorArgument("databaseSettings", new BasicDatabaseSettings { ConnectionString = settingsProvider.GetConnectionString_Redshift() });

            Bind<IContractLabDataServiceRedshift>().To<ContractLabDataServiceRedshift>();
        }
    }
}
