﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Web.Common;
using ContractLab.DataService.Redshift.Interfaces;

namespace ContractLab.DataService.Redshift.Modules
{
    public class UserContextModule_Redshift : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserContext_Redshift>().To<AspNetUserContext>().InRequestScope();
        }
    }
}
