﻿using ContractLab.DTO.Redshift;
using ContractLab.Repository.Redshift.Models;
using System;
using System.Collections.Generic;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.DataService.Redshift.Interfaces
{
    public interface IContractLabDataServiceRedshift
    {
        ResponseBase NAISpecimenInsert(MessageBase<NAISpecimenDTO> dto);
    }
}
