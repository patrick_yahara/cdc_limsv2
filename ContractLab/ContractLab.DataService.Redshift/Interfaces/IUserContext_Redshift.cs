﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ContractLab.DataService.Redshift.Interfaces
{
    public interface IUserContext_Redshift
    {
        string UserId { get; }
        IPrincipal User { get; }
        IIdentity Identity { get; }
    }
}