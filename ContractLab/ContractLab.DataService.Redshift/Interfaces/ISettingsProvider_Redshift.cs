﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DataService.Redshift.Interfaces
{
    public interface ISettingsProvider_Redshift
    {
        string GetConnectionString_Redshift();
    }
}
