﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class IsolateDTO
    {
        public IsolateDTO()
        {
            Aliquots = new List<AliquotDTO>();
        }

        [Heading("Isolate ID")]
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int SpecimenId { get; set; }
        public int IsolateTypeId { get; set; }
        [Heading("Passage")]
        public string PassageLevel { get; set; }
        public DateTime? DateHarvested { get; set; }
        [Heading("Date harvested")]
        public String DateHarvestedDisplay
        {
            get
            {
                if (DateHarvested.HasValue)
                {
                    return DateHarvested.Value.ToShortDateString();
                }
                else return null;
            }
        }

        [Heading("titer")]
        public int? Titer { get; set; }
        [Heading("RBC type")]
        public string RedBloodCellType { get; set; }
        public DateTime? DateInoculated { get; set; }
        public virtual ICollection<AliquotDTO> Aliquots { get; set; }
        public virtual SpecimenDTO Specimen { get; set; }
        public virtual IsolateTypeDTO IsolateType { get; set; }
        public int AliquotCount { get; set; }
        [Heading("Not Harvested Reason")]
        public string Comments { get; set; }
        public bool HasRCVAliquot { get; set; }
        public int? StatusId { get; set; }
        public virtual StatusDTO Status { get; set; }

    }
}
