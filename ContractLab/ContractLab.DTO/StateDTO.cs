﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class StateDTO
    {
        public int? Id { get; set; }
        public String Name { get; set; }
        [Heading("Specimen_Collection_State")]
        public string Abbreviation { get; set; }
    }
}
