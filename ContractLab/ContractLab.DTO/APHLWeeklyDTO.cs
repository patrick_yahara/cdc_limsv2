﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class APHLWeeklyDTO
    {
        //CDC ID #	(Sub)type	State/Country	Sender ID	Date Received	Special Study	Date Coll't
        [Heading("Lab")]
        public string Lab { get; set; }
        [Heading("SpecimenID")]
        public string SpecimenID { get; set; }
        [Heading("CDC ID #")]
        public string CSID { get; set; }
        [Heading("CUID")]
        public string CUID { get; set; }
        [Heading("(Sub)type")]
        public string SubType { get; set; }
        [Heading("State/Country")]
        public string State { get; set; }
        [Heading("Sender ID")]
        public string SenderId { get; set; }
        public DateTime? DateReceived { get; set; }
        [Heading("Date Received")]
        public string DateReceivedDisplay { get { return this.DateReceived.HasValue ? this.DateReceived.Value.ToShortDateString() : String.Empty; } }
        [Heading("Special Study")]
        public string SpecialStudy { get; set; }
        public DateTime? DateCollected { get; set; }
        [Heading("Date Coll't")]
        public string DateCollectedDisplay { get { return this.DateCollected.HasValue ? this.DateCollected.Value.ToShortDateString() : String.Empty; } }
        public string CreatedBy { get; set; }
        public int Id { get; set; }
    }
}
