﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class WHOImportDTO
    {
        public List<WHOSpecimenDTO> WHOSpecimens { get; set; }
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public int ColumnStart { get; set; }
        public int? ColumnEnd { get; set; }
        public int RowStart { get; set; }
        public int? RowEnd { get; set; }
        public string PropertyOrder { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public bool IsError { get; set; }
    }
}
