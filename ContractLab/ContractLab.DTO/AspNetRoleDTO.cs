﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class AspNetRoleDTO
    {
        public AspNetRoleDTO()
        {
            AspNetUsers = new List<AspNetUserDTO>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AspNetUserDTO> AspNetUsers { get; set; }
    }
}
