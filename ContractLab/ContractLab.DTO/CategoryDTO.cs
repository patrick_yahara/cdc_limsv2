﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class CategoryDTO
    {
        public CategoryDTO()
        {
            Conditions = new List<ConditionDTO>();
            Synonyms = new List<SynonymDTO>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LitsQuestionId { get; set; }
        public bool AllowBlank { get; set; }
        public int ConditionsCount { get; set; }
        public int SynonymsCount { get; set; }
        public virtual ICollection<ConditionDTO> Conditions { get; set; }
        public virtual ICollection<SynonymDTO> Synonyms { get; set; }
    }
}
