﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class APHLMonthlyDTO
    {
        [Heading("SubType")]
        public string SubType { get; set; }
        [Heading("Received")]
        public int Received { get; set; }
        [Heading("Inocolated")]
        public int Inocolated { get; set; }
        [Heading("Recovered")]
        public int Recovered { get; set; }
        [Heading("InCulture")]
        public int InCulture { get; set; }
        [Heading("HAUnder4")]
        public int HAUnder4 { get; set; }
        [Heading("VNR")]
        public int VNR { get; set; }
        [Heading("Recovered_Pct")]
        public double Recovered_Pct { get; set; }
        [Heading("HAUnder4_Pct")]
        public double HAUnder4_Pct { get; set; }
        [Heading("VNR_Pct")]
        public double VNR_Pct { get; set; }
        [Heading("PendingInCulture_Pct")]
        public double PendingInCulture_Pct { get; set; }
        [Heading("TotalSent")]
        public int TotalSent { get; set; }
        [Heading("TAT_Avg")]
        public double? TAT_Avg { get; set; }
    }
}
