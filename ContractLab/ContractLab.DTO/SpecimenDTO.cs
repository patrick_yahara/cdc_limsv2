﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class SpecimenDTO
    {
        public SpecimenDTO()
        {
            Isolates = new List<IsolateDTO>();
        }
        public int Id { get; set; }
        [Heading("Date_Logged")]
        public string CreatedOnDisplay { get { return CreatedOn.ToShortDateString(); } }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        [Heading("CSID")]
        public string CSID { get; set; }
        [Heading("PARENT_CUID")]
        public string CUID { get; set; }
        public int? ProjectId { get; set; }
        public int PackageId { get; set; }
        public virtual ICollection<IsolateDTO> Isolates { get; set; }
        public virtual PackageDTO Package { get; set; }
        public virtual ProjectDTO Project { get; set; }
        [Heading("Specimen ID")]
        public string SpecimenID { get; set; }
        [Heading("Date_Collected")]
        public DateTime? DateCollected { get; set; }
        [Heading("Patient_Age")]
        public string PatientAge { get; set; }
        [Heading("Patient_Age_Type")]
        public string PatientAgeUnits { get; set; }
        [Heading("Patient_Sex")]
        public string PatientSex { get; set; }
        [Heading("Extent_of_Activity")]
        public string Activity { get; set; }
        [Heading("Passage History")]
        public string PassageHistory { get; set; }
        [Heading("(Sub) type")]
        public string SubType { get; set; }
        [Heading("Specimen_Type")]
        public string SpecimenSource { get; set; }
        [Heading("SAMP_LOC_BLDG")]
        public string InitialStorage { get; set; }
        [Heading("Comp CDC ID #1")]
        public string CompCSID1 { get; set; }
        [Heading("Comp CDC ID #2")]
        public string CompCSID2 { get; set; }
        [Heading("Specimen_Collection_Other")]
        public string OtherLocation { get; set; }
        [Heading("Drug Resistant")]
        public string DrugResistant { get; set; }
        [Heading("FATAL")]
        public string Deceased { get; set; }
        [Heading("Study_Name")]
        public string SpecialStudy { get; set; }
        [Heading("Nursing Home")]
        public string NursingHome { get; set; }
        [Heading("Flu_Vaccination")]
        public string Vaccinated { get; set; }
        [Heading("Travel_Place")]
        public string Travel { get; set; }
        [Heading("Reason for submission")]
        public string ReasonForSubmission { get; set; }
        [Heading("Comments")]
        public string Comments { get; set; }
        [Heading("Sample_Class")]
        public string SampleClass { get; set; }
        [Heading("Contract_Lab_From")]
        public string ContractLabFrom { get; set; }
        public int StatusId { get; set; }
        public virtual StatusDTO Status { get; set; }

        [Heading("Travel_City")]
        public string TravelCity { get; set; }
        [Heading("Host_Cell_Line")]
        public string HostCellLine { get; set; }
        [Heading("Substrate")]
        public string Substrate { get; set; }
        [Heading("Passage_Number")]
        public string PassageNumber { get; set; }
        [Heading("PCRInfA")]
        public string PCRInfA { get; set; }
        [Heading("PCRpdmInfA")]
        public string PCRpdmInfA { get; set; }
        [Heading("PCRpdmH1")]
        public string PCRpdmH1 { get; set; }
        [Heading("PCRH3")]
        public string PCRH3 { get; set; }
        [Heading("PCRInfB")]
        public string PCRInfB { get; set; }
        [Heading("PCRBVic")]
        public string PCRBVic { get; set; }
        [Heading("PCRBYam")]
        public string PCRBYam { get; set; }
        [Heading("Specimen_Collection_City")]
        public string City { get; set; }

        [Heading("Travel_Return_Date")]
        public DateTime? TravelReturnDate { get; set; }

        [Heading("Travel_Country")]
        public string TravelCountry { get; set; }
        [Heading("Travel_State")]
        public string TravelState { get; set; }
        [Heading("Travel_County")]
        public string TravelCounty { get; set; }
        [Heading("WHO_Submitted_Subtype")]
        public string WHOSubmittedSubtype { get; set; }


        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CountyId { get; set; }

        public virtual StateDTO State { get; set; }
        public virtual CountryDTO Country { get; set; }
        public virtual CountyDTO County { get; set; }

        public int? LabId { get; set; }
        public virtual LabDTO Lab { get; set; }

        public DateTime FilterDate { get; set; }

        public int IsolateCount { get; set; }

        public DateTime? DateInoculated { get; set; }
        [Heading("Date inoculated")]
        public String DateInoculatedDisplay
        {
            get
            {
                if (DateInoculated.HasValue)
                {
                    return DateInoculated.Value.ToShortDateString();
                }
                else return null;
            }
        }
        public int? FluSeason { get; set; }
    }
}

