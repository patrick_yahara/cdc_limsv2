﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class IsolateTypeDTO
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        [Heading("Subsample Type")]
        public string SysName { get; set; }
        public virtual ICollection<IsolateDTO> Isolates { get; set; }
        public bool IsVisible { get; set; }
    }
}
