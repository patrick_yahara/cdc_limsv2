﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class PackageDTO
    {
        public PackageDTO()
        {
            Specimens = new List<SpecimenDTO>();
        }

        public int? Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        [Heading("PACKAGE_ID")]
        public string Name { get; set; }
        public int SenderId { get; set; }
        public int? ContactId { get; set; }
        public string Comments { get; set; }
        public bool IsVisible { get; set; }

        [Heading("Date_Received")]
        public DateTime? DateReceived { get; set; }
        [Heading("Serum_Condition")]
        public string SpecimenCondition { get; set; }

        public string ComboDisplay { get { return String.Format(@"{0} {1}", Name, CreatedOn.ToShortDateString()); } }
        public virtual ContactDTO Contact { get; set; }
        public virtual SenderDTO Sender { get; set; }
        public virtual ICollection<SpecimenDTO> Specimens { get; set; }

        public int SpecimenCount { get; set; }
    }
}
