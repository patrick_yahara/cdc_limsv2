﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class APHLMonthlySpecimenDTO
    {
        public int Id { get; set; }
        public string SubType { get; set; }
        public bool? Inoculated { get; set; }
        public bool? Recovered { get; set; }
        public bool? InCulture { get; set; }
        public bool? TiterLT4 { get; set; }
        public bool? VNR { get; set; }
        public bool? Shipped { get; set; }
        public int? TAT { get; set; }
        public string Lab { get; set; }
        public string CreatedBy { get; set; }
    }
}
