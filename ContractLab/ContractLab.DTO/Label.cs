﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ContractLab.DTO
{
    public class Labels
    {
        public string url { get; set; }
        public string preamble { get; set; }
        public string format { get; set; }
        public string postamble { get; set; }
        public List<Label> data { get; set; }
    }    

    public class Label
    {
        public Label() { }
        public string HA { get; set; } // 6
        public string FT { get; set; } // 3
        public string PH { get; set; } // 6
        public string DH
        {
            get
            {
                if (DaveHarvested.HasValue)
                {
                    return String.Format(@"{0}({1}){2}", PH, DaveHarvested.Value.ToShortDateString(), Lab).ToUpper();
                }
                else
                {
                    if (String.IsNullOrEmpty(PH))
                    {
                        return String.Format(@"{0}", Lab).ToUpper();
                    }
                    else
                    {
                        return String.Format(@"{0} {1}", PH, Lab).ToUpper();
                    }
                };
            }
        } // 10
        [ScriptIgnore]
        private string _ST;
        public string ST { get { return string.Format("{0} {1}", SpecimenSource, _ST);} set { _ST = value; } }
        public string CUIDHR { get {
            if (DateCollected.HasValue)
            {
                if (DateCollected.Value.Month > 9)
                {
                    return (DateCollected.Value.Year + 1).ToString();
                }
                else
                {
                    return DateCollected.Value.Year.ToString();
                }
                
            }
            else
            {
                if (DateTime.UtcNow.Month > 9)
                {
                    return (DateTime.UtcNow.Year + 1).ToString();
                }
                else
                {
                    return DateTime.UtcNow.Year.ToString();
                }
            }
            ;} } // NA
        public string CUIDBC { get; set; } // NA
        public string CSID { get; set; } // 10
        public string SUB { get; set; } // 12
        [ScriptIgnore]
        public DateTime? DaveHarvested { get; set; }
        //public DateTime? DaveHarvested { set { DH = value.HasValue ? value.Value.ToShortDateString() : ""; } }
        public string SID { get; set; }// 12
        [ScriptIgnore]
        public int Id { get; set; }
        [ScriptIgnore]
        public string Lab { get; set; }
        [ScriptIgnore]
        public DateTime? DateCollected { get; set; }
        [ScriptIgnore]
        public string SpecimenSource { get; set; }
        [ScriptIgnore]
        public int OrderValue { get; set; }
    }
}
