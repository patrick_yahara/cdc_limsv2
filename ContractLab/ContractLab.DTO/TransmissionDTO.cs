﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class TransmissionDTO : IEquatable<TransmissionDTO>, IComparable<TransmissionDTO>
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int SpecimenId { get; set; }
        public int? IsolateId { get; set; }
        public int? AliquotId { get; set; }
        public string Payload { get; set; }
        public string Response { get; set; }
        public int? Attempts { get; set; }
        public DateTime? TransmittedOn { get; set; }
        public byte[] SpecimenRowVersion { get; set; }
        public byte[] IsolateRowVersion { get; set; }
        public byte[] AliquotRowVersion { get; set; }
        public byte[] Signature { get; set; }
        public int? SupercedeId { get; set; }
        public virtual AliquotDTO Aliquot { get; set; }
        public virtual IsolateDTO Isolate { get; set; }
        public virtual SpecimenDTO Specimen { get; set; }

        //https://msdn.microsoft.com/en-us/library/4d7sx9hd(v=vs.110).aspx
        public int CompareTo(TransmissionDTO other)
        {
            if (other == null) return 1;

            var temp = this.SpecimenId.CompareTo(other.SpecimenId);
            if (temp == 0)
	        {
		        temp = (this.IsolateId.HasValue ? this.IsolateId.Value : 0).CompareTo(other.IsolateId.HasValue ? other.IsolateId.Value : 0);
	        }
            if (temp == 0)
	        {
		        temp = (this.AliquotId.HasValue ? this.AliquotId.Value : 0).CompareTo(other.AliquotId.HasValue ? other.AliquotId.Value : 0);
	        }

            if (temp == 0)
	        {
                temp = StructuralComparisons.StructuralComparer.Compare(this.SpecimenRowVersion, other.SpecimenRowVersion); ;
	        }

            if (temp == 0)
            {
                temp = StructuralComparisons.StructuralComparer.Compare(this.IsolateRowVersion, other.IsolateRowVersion); ;
            }

            if (temp == 0)
            {
                temp = StructuralComparisons.StructuralComparer.Compare(this.AliquotRowVersion, other.AliquotRowVersion); ;
            }


            return temp;
          }

        //https://msdn.microsoft.com/en-us/library/ms131187(v=vs.110).aspx
        public bool Equals(TransmissionDTO other)
        {
            if (other == null)
                return false;

            if (this.SpecimenId == other.SpecimenId
                && this.IsolateId == other.IsolateId
                && this.AliquotId == other.AliquotId
                //&& StructuralComparisons.StructuralEqualityComparer.Equals(this.SpecimenRowVersion, other.SpecimenRowVersion)
                //&& StructuralComparisons.StructuralEqualityComparer.Equals(this.IsolateRowVersion, other.IsolateRowVersion)
                //&& StructuralComparisons.StructuralEqualityComparer.Equals(this.AliquotRowVersion, other.AliquotRowVersion)
                )
                return true;
            else
                return false;
        }
    }
}
