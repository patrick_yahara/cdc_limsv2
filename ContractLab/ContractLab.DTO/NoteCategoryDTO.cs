using System;
using System.Collections.Generic;

namespace ContractLab.DTO
{
    public class NoteCategoryDTO
    {
        public NoteCategoryDTO()
        {
            SpecimenNotes = new List<SpecimenNoteDTO>();
        }

        public int Id { get; set; }
        public string SysName { get; set; }
        public string DisplayName { get; set; }
        public virtual ICollection<SpecimenNoteDTO> SpecimenNotes { get; set; }

	}
}
