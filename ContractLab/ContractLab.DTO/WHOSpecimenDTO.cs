﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class WHOSpecimenDTO
    {
        public int RowNumber { get; set; }
        [Heading("SpecimenID")]
        public string SpecimenID { get; set; }
        [Heading("DateCollected")]
        public DateTime? DateCollected { get; set; }
        [Heading("PatientAge")]
        public string PatientAge { get; set; }
        [Heading("PatientSex")]
        public string PatientSex { get; set; }
        [Heading("State")]
        public string State { get; set; }
        [Heading("SpecimenType")]
        public string SpecimenSource { get; set; }
        [Heading("SubType")]
        public string SubType { get; set; }
        [Heading("ReasonForSubmission")]
        public string ReasonForSubmission { get; set; }
        [Heading("Deceased")]
        public string Deceased { get; set; }
        [Heading("NursingHome")]
        public string NursingHome { get; set; }
        [Heading("Vaccinated")]
        public string Vaccinated { get; set; }
        [Heading("Comments")]
        public string Comments { get; set; }

        [Heading("Passage History")]
        public string PassageHistory { get; set; }
        public string PassageHistory_Error { get; set; }

        public string SpecimenID_Error { get; set; }
        public string DateCollected_Error { get; set; }
        public string PatientAge_Error { get; set; }
        public string PatientSex_Error { get; set; }
        public string State_Error { get; set; }
        public string SpecimenSource_Error { get; set; }
        public string SubType_Error { get; set; }
        public string ReasonForSubmission_Error { get; set; }
        public string Deceased_Error { get; set; }
        public string NursingHome_Error { get; set; }
        public string Vaccinated_Error { get; set; }
        public string Travel_Error { get; set; }
        public string Comments_Error { get; set; }

        [Heading("TravelCountry")]
        public string TravelCountry { get; set; }
        [Heading("TravelState")]
        public string TravelState { get; set; }
        [Heading("TravelCounty")]
        public string TravelCounty { get; set; }
        [Heading("TravelCity")]
        public string TravelCity { get; set; }
        [Heading("HostCellLine")]
        public string HostCellLine { get; set; }
        [Heading("Substrate")]
        public string Substrate { get; set; }
        [Heading("PassageNumber")]
        public string PassageNumber { get; set; }
        [Heading("PCRInfA")]
        public string PCRInfA { get; set; }
        [Heading("PCRpdmInfA")]
        public string PCRpdmInfA { get; set; }
        [Heading("PCRpdmH1")]
        public string PCRpdmH1 { get; set; }
        [Heading("PCRH3")]
        public string PCRH3 { get; set; }
        [Heading("PCRInfB")]
        public string PCRInfB { get; set; }
        [Heading("PCRBVic")]
        public string PCRBVic { get; set; }
        [Heading("PCRBYam")]
        public string PCRBYam { get; set; }
        [Heading("City")]
        public string City { get; set; }
        [Heading("Travel_Return_Date")]
        public DateTime? TravelReturnDate { get; set; }

        [Heading("Country")]
        public string Country { get; set; }
        public string Country_Error { get; set; }

        [Heading("County")]
        public string County { get; set; }
        public string County_Error { get; set; }

        [Heading("PatientAgeUnits")]
        public string PatientAgeUnits { get; set; }
        public string PatientAgeUnits_Error { get; set; }

        [Heading("Who_Submitted_SubType")]
        public string WhoSubmittedSubType { get; set; }
        public string WhoSubmittedSubType_Error { get; set; }

        public string TravelCountry_Error { get; set; }
        public string TravelState_Error { get; set; }
        public string TravelCounty_Error { get; set; }
        public string TravelCity_Error { get; set; }
        public string HostCellLine_Error { get; set; }
        public string Substrate_Error { get; set; }
        public string PassageNumber_Error { get; set; }
        public string PCRInfA_Error { get; set; }
        public string PCRpdmInfA_Error { get; set; }
        public string PCRpdmH1_Error { get; set; }
        public string PCRH3_Error { get; set; }
        public string PCRInfB_Error { get; set; }
        public string PCRBVic_Error { get; set; }
        public string PCRBYam_Error { get; set; }
        public string City_Error { get; set; }
        public string TravelReturnDate_Error { get; set; }

        public string RowError { get; set; }

        public bool IsError { get; set; }

        public string PackageName { get; set; }
        public int PackageId { get; set; }
    }
}
