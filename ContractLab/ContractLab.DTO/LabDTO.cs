﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class LabDTO
    {
        public int Id { get; set; }
        [Heading("Lab")]
        public String Name { get; set; }
        public string PrinterURI { get; set; }
        public string LabelPreamble { get; set; }
        public string LabelPostamble { get; set; }
        public string LabelFormatString { get; set; }
        public string WHOLabelFormatString { get; set; }
        public string StoredLocallyCodes { get; set; }
        public string AliquotVersionCode { get; set; }
        public string LabelTestCodeOrder { get; set; }
        public string ImportPropertyOrder { get; set; }
        public string GrownTestCodeList { get; set; }
        public string IttTestCodeList { get; set; }
    }
}
