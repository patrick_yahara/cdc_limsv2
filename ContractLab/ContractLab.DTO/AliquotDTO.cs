﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class AliquotDTO
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        [Heading("Isolate ID")]
        public int IsolateId { get; set; }
        [Heading("CUID")]
        public string CUID { get; set; }
        [Heading("CSID_CUID")]
        public string CSID_CUID { get { return String.Format(@"{0}_{1}{2}", this.Isolate.Specimen.CSID, this.CUID, this.VersionCode); } }
        [Heading("Fraction Type")]
        public string TestCode { get; set; }
        public virtual IsolateDTO Isolate { get; set; }
        public virtual ManifestDTO Manifest { get; set; }
        public int? ManifestId { get; set; }
        [Heading("Stored_Locally")]
        public bool IsStoredLocally { get; set; }
        public string VersionCode { get; set; }
        [Heading("Tube Count")]
        public int TubeCount { get; set; }
        public int? StatusId { get; set; }
        public virtual StatusDTO Status { get; set; }
        [Heading("Redshift_Tracking")]
        public bool RedshiftTracking { get; set; }

    }
}
