using System;
using System.Collections.Generic;

namespace ContractLab.DTO
{
    public class SpecimenNoteDTO
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int SpecimenId { get; set; }
        public int NoteCategoryId { get; set; }
        public string Note { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public virtual NoteCategoryDTO NoteCategory { get; set; }
        public virtual SpecimenDTO Specimen { get; set; }
	}
}
