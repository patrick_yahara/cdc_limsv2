﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class AspNetUserDTO
    {
        public AspNetUserDTO()
        {
            AspNetUserClaims = new List<AspNetUserClaimDTO>();
            AspNetUserLogins = new List<AspNetUserLoginDTO>();
            AspNetRoles = new List<AspNetRoleDTO>();
        }

        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public DateTime? DateDisabled { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public int? LabId { get; set; }
        public virtual ICollection<AspNetUserClaimDTO> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLoginDTO> AspNetUserLogins { get; set; }
        public virtual LabDTO Lab { get; set; }
        public virtual ICollection<AspNetRoleDTO> AspNetRoles { get; set; }

    }
}
