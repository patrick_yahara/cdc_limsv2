﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class ManifestDTO
    {
        public ManifestDTO()
        {
            Aliquots = new List<AliquotDTO>();
        }
        [Heading("Manifest_Id")]
        public int? Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        [Heading("Manifest_Name")]
        public string Name { get; set; }
        public int StatusId { get; set; }
        public bool IsVisible { get; set; }
        [Heading("Manifest_Combo")]
        public string ComboDisplay { get { return Id > 0 ? String.Format(@"{0} {1}", Name, CreatedOn.ToShortDateString()) : ""; } }
        public virtual ICollection<AliquotDTO> Aliquots { get; set; }
        public virtual StatusDTO Status { get; set; }
        public int AliquotCount { get; set; }
        public string Comments { get; set; }
        public virtual LabDTO Lab { get; set; }
        public DateTime? DateShipped { get; set; }
        public string TrackingNumber { get; set; }
    }
}
