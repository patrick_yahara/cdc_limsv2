﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class FilterDTO
    {
        public int Id { get; set; }
        public string Display { get; set; }
        public string BaseObject { get; set; }
        public string FilterText { get; set; }
        public bool IsLabAvailable { get; set; }
    }
}
