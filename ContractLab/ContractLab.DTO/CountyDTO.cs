﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class CountyDTO
    {
        public int? Id { get; set; }
        [Heading("Specimen_Collection_County")]
        public String Name { get; set; }
    }
}
