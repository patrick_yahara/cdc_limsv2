﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
    
{
    public class SenderCompositeDTO
    {
        public int Id { get { return Sender.Id; } }
        public SenderDTO Sender { get; set; }
        public ContactDTO Contact { get; set; }
    }
}
