﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class CountryDTO
    {
        public int? Id { get; set; }
        [Heading("Specimen_Collection_Country")]
        public string ShortCode { get; set; }
        public string LongCode { get; set; }
        public String Name { get; set; }
    }
}
