﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class ConditionDTO
    {
        public ConditionDTO()
        {
            Synonyms = new List<SynonymDTO>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LitsQuestionId { get; set; }
        public int CategoryId { get; set; }
        public string Status { get; set; }
        public int Sorter { get; set; }
        public virtual CategoryDTO Category { get; set; }
        public virtual IList<SynonymDTO> Synonyms { get; set; }
        public int SynonymsCount { get; set; }
    }
}
