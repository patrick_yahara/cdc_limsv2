﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class ProjectDTO
    {
        public ProjectDTO()
        {
            Specimens = new List<SpecimenDTO>();
        }

        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SpecimenDTO> Specimens { get; set; }

        public int SpecimenCount { get; set; }
    }
}
