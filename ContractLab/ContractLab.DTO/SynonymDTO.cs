﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class SynonymDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LitsQuestionId { get; set; }
        public int CategoryId { get; set; }
        public string ConditionName { get; set; }
        public int ConditionId { get; set; }
        public string Status { get; set; }
        public int Sorter { get; set; }
        public virtual CategoryDTO Category { get; set; }
        public virtual ConditionDTO Condition { get; set; }
    }
}
