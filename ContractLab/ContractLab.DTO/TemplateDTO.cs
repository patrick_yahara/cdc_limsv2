﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DTO
{
    public class TemplateDTO
    {
        public int Id { get; set; }
        public string TemplateType { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public int? LabId { get; set; }
        public virtual LabDTO Lab { get; set; }
    }
}
