﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database.Redshift
{
    [Migration(201906260000)]
    public class Mig001_NAISpecimen : Migration
    {
        public override void Up()
        {
            Create.Table("NAISpecimen")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Action").AsString(128).NotNullable()
            .WithColumn("Status").AsString(50)
            .WithColumn("CSID").AsString(128).NotNullable()
            .WithColumn("CUID").AsString(128).NotNullable()
            .WithColumn("SubType").AsString(128).Nullable()
            .WithColumn("PassageLevel").AsString(128).Nullable()
            .WithColumn("DateHarvested").AsDateTime().Nullable()
            .WithColumn("State").AsString(50).Nullable()
            .WithColumn("Country").AsString(50).Nullable()
            .WithColumn("DateCollected").AsDateTime().Nullable()
            .WithColumn("AliquotAsJson").AsString(int.MaxValue).Nullable()
            ;
        }

        public override void Down()
        {
            Delete.Table("NAISpecimen");
        }

    }
}
