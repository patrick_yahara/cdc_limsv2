﻿using FluentMigrator;

namespace ContractLab.Database.Redshift
{
    /// <summary>
    /// Initial NOOP migration to mark the database in an initial empty state.
    /// </summary>
    [Migration(201906260000)]
    public class Mig000Initial : Migration
    {
        public override void Up()
        {

        }

        public override void Down()
        {
        }
    }
}