﻿using ContractLab.DTO;
using ContractLab.Repository.Models;
using System;
using System.Collections.Generic;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.DataService.Interfaces
{
    public interface IContractLabDataService_HttpContextFree
    {
        ResponseBase GenerateTransmissionFile_Background(MessageBase<int> size, MessageBase<string> path);
    }
}
