﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ContractLab.Utilities
{
    public interface IUserContext
    {
        string UserId { get; }
        IPrincipal User { get; }
        IIdentity Identity { get; }
    }
}