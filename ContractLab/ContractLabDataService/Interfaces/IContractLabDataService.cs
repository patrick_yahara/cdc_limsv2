﻿using ContractLab.DTO;
using ContractLab.Repository.Models;
using System;
using System.Collections.Generic;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.DataService.Interfaces
{
    public interface IContractLabDataService
    {
        ResponseBase<int> DoTestCall(MessageBase<int> id);
        ResponseBase<IList<ProjectDTO>> ProjectList();
        ResponseBase ProjectInsert(MessageBase<ProjectDTO> dto);
        ResponseBase<ProjectDTO> Project(MessageBase<int> id);
        ResponseBase ProjectDelete(MessageBase<int> id);
        ResponseBase ProjectEdit(MessageBase<ProjectDTO> dto);
        PagedResponseBase<IList<SpecimenDTO>> SpecimenList(MessageBase<RequestDetail> request);
        ResponseBase SpecimenInsert(MessageBase<SpecimenDTO> dto);
        ResponseBase SpecimenDelete(MessageBase<int> id);
        ResponseBase<SpecimenDTO> Specimen(MessageBase<int> id);
        ResponseBase SpecimenEdit(MessageBase<SpecimenDTO> dto);
        ResponseBase<IList<PackageDTO>> PackageList();
        PagedResponseBase<IList<PackageDTO>> PackageList(MessageBase<RequestDetail> request);
        ResponseBase PackageInsert(MessageBase<PackageDTO> dto);
        ResponseBase PackageDelete(MessageBase<int> id);
        ResponseBase PackageEdit(MessageBase<PackageDTO> dto);
        ResponseBase<PackageDTO> Package(MessageBase<int> id);
        ResponseBase<IList<IsolateDTO>> IsolateList(MessageBase<int> id);
        ResponseBase IsolateInsert(MessageBase<IsolateDTO> dto);
        ResponseBase IsolateDelete(MessageBase<int> id);
        ResponseBase IsolateEdit(MessageBase<IsolateDTO> dto);
        ResponseBase<IsolateDTO> Isolate(MessageBase<int> id);

        ResponseBase<IList<SpecimenNoteDTO>> SpecimenNoteList(MessageBase<int> id);
        ResponseBase SpecimenNoteInsert(MessageBase<SpecimenNoteDTO> dto);
        ResponseBase SpecimenNoteDelete(MessageBase<int> id);
        ResponseBase SpecimenNoteEdit(MessageBase<SpecimenNoteDTO> dto);
        ResponseBase<SpecimenNoteDTO> SpecimenNote(MessageBase<int> id);
        ResponseBase<SpecimenNoteDTO> SpecimenNoteFile(MessageBase<int> id);
        ResponseBase SpecimenNoteFile_Remove(MessageBase<int> id);
        ResponseBase<IList<NoteCategoryDTO>> NoteCategoryList();
        

        ResponseBase<IList<IsolateTypeDTO>> IsolateTypeList();
        ResponseBase IsolateTypeInsert(MessageBase<IsolateTypeDTO> dto);
        ResponseBase<IsolateTypeDTO> IsolateType(MessageBase<int> id);
        ResponseBase IsolateTypeDelete(MessageBase<int> id);
        ResponseBase IsolateTypeEdit(MessageBase<IsolateTypeDTO> dto);

        ResponseBase DEIFileMerge(MessageBase<DEIFileDTO> dto);
        ResponseBase<List<SynonymDTO>> GetPickListItems(MessageBase<string> lits);
        ResponseBase<IList<CountryDTO>> GetCountries();
        ResponseBase<IList<StateDTO>> GetStates();
        ResponseBase<IList<CountyDTO>> GetCounties(MessageBase<int> stateId);
        ResponseBase<IList<NameValueDTO>> PackageCSIDList(MessageBase<int?> id);
        ResponseBase<IList<AliquotDTO>> AliquotList(MessageBase<int> id);
        ResponseBase AliquotInsert(MessageBase<AliquotDTO> dto);
        ResponseBase AliquotDelete(MessageBase<int> id);
        ResponseBase AliquotEdit(MessageBase<AliquotDTO> dto);
        ResponseBase<AliquotDTO> Aliquot(MessageBase<int> id);
        ResponseBase DEIFileMergeClients(MessageBase<DEIFileDTO> dto);
        ResponseBase DEIFileMergeContacts(MessageBase<DEIFileDTO> dto);
        ResponseBase<IList<SenderDTO>> GetSenders(MessageBase<string> stateAbbrev);
        ResponseBase<IList<SenderDTO>> GetSenders(MessageBase<SenderDTO> dto);

        ResponseBase<IList<LabDTO>> GetLabs();
        ResponseBase UserLabEdit(MessageBase<KeyValuePair<string, int?>> dto);
        ResponseBase UserDateDisabledEdit(MessageBase<KeyValuePair<string, DateTime?>> dto);

        ResponseBase AliquotInsertInitial(MessageBase<Tuple<int, int?>> isolateId);
        ResponseBase AliquotInsertInitialITT(MessageBase<Tuple<int, int?>> isolateId);
        ResponseBase<IList<NameValueDTO>> TestCodeList();

        ResponseBase<IList<FilterDTO>> FilterList();
        ResponseBase<IList<FilterDTO>> FilterList(MessageBase<string> baseobject);
        ResponseBase FilterInsert(MessageBase<FilterDTO> dto);
        ResponseBase<FilterDTO> Filter(MessageBase<int> id);
        ResponseBase FilterDelete(MessageBase<int> id);
        ResponseBase FilterEdit(MessageBase<FilterDTO> dto);
        ResponseBase<IList<NameValueDTO>> StatusList();
        ResponseBase<IList<StatusDTO>> StatusDTOList();

        ResponseBase TemplateUpload(MessageBase<TemplateDTO> dto);
        ResponseBase TemplateDelete(MessageBase<int> id);
        ResponseBase<IList<TemplateDTO>> TemplateList();
        ResponseBase<TemplateDTO> GenerateSpecimenFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase GenerateSpecimenTransmissionRows(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateAliquotFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase GenerateAliquotTransmissionRows(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateTransmissionFile(MessageBase<List<int>> dto);
        ResponseBase<TemplateDTO> GenerateWeeklyFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateMonthlyFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateISRFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateNAIFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase<TemplateDTO> GenerateINOCFile(MessageBase<List<int>> dto, MessageBase<string> view);
        ResponseBase UpdateStatus(MessageBase<List<int>> specimens, MessageBase<int> status);

        ResponseBase<Labels> GenerateLabels(MessageBase<List<int>> dto, MessageBase<string> baseobject, MessageBase<int> qty);
        ResponseBase<string> GenerateLabels_Page(MessageBase<List<int>> dto, MessageBase<string> baseobject, MessageBase<int> qty);

        ResponseBase<IList<LabDTO>> LabList();
        ResponseBase LabInsert(MessageBase<LabDTO> dto);
        ResponseBase<LabDTO> Lab(MessageBase<int> id);
        ResponseBase LabDelete(MessageBase<int> id);
        ResponseBase LabEdit(MessageBase<LabDTO> dto);
        ResponseBase<LabDTO> UserHasLab();
        ResponseBase UserReadOnly();
        ResponseBase UserEditor();

        PagedResponseBase<IList<ManifestDTO>> ManifestList(MessageBase<RequestDetail> request);
        ResponseBase ManifestInsert(MessageBase<ManifestDTO> dto);
        ResponseBase<ManifestDTO> Manifest(MessageBase<int> id);
        ResponseBase ManifestDelete(MessageBase<int> id);
        ResponseBase ManifestEdit(MessageBase<ManifestDTO> dto);
        ResponseBase<AliquotDTO> GetAliquotByCUID(MessageBase<string> cuid);
        ResponseBase<IList<ManifestDTO>> ManifestList();

        PagedResponseBase<IList<SenderCompositeDTO>> SenderCompositeList(MessageBase<RequestDetail> request);
        ResponseBase SenderCompositeInsert(MessageBase<SenderCompositeDTO> dto);
        ResponseBase<SenderCompositeDTO> SenderComposite(MessageBase<string> id);
        ResponseBase SenderCompositeDelete(MessageBase<string> id);
        ResponseBase SenderCompositeEdit(MessageBase<SenderCompositeDTO> dto);

        ResponseBase<WHOImportDTO> ValidateWHO(MessageBase<WHOImportDTO> dto);
        ResponseBase ImportWHO(MessageBase<WHOImportDTO> dto);

        ResponseBase<IList<WHOTranslationDTO>> WHOTranslationList();
        ResponseBase WHOTranslationInsert(MessageBase<WHOTranslationDTO> dto);
        ResponseBase<WHOTranslationDTO> WHOTranslation(MessageBase<int> id);
        ResponseBase WHOTranslationDelete(MessageBase<int> id);
        ResponseBase WHOTranslationEdit(MessageBase<WHOTranslationDTO> dto);

        ResponseBase<VersionInfoDTO> VersionInfo();

        PagedResponseBase<IList<CategoryDTO>> CategoryList(MessageBase<RequestDetail> request);
        ResponseBase CategoryDelete(MessageBase<int> id);
        ResponseBase<CategoryDTO> Category(MessageBase<int> id);
        ResponseBase CategoryInsert(MessageBase<CategoryDTO> dto);
        ResponseBase CategoryEdit(MessageBase<CategoryDTO> dto);
        ResponseBase<IList<ConditionDTO>> ConditionList(MessageBase<int> id);
        ResponseBase ConditionDelete(MessageBase<int> id);
        ResponseBase<ConditionDTO> Condition(MessageBase<int> id);
        ResponseBase ConditionInsert(MessageBase<ConditionDTO> dto);
        ResponseBase ConditionEdit(MessageBase<ConditionDTO> dto);
        ResponseBase<IList<SynonymDTO>> SynonymList(MessageBase<int> id);
        ResponseBase SynonymDelete(MessageBase<int> id);
        ResponseBase<SynonymDTO> Synonym(MessageBase<int> id);
        ResponseBase SynonymInsert(MessageBase<SynonymDTO> dto);
        ResponseBase SynonymEdit(MessageBase<SynonymDTO> dto);

        ResponseBase<IList<AspNetUserDTO>> UserList();
        ResponseBase UserInsert(MessageBase<AspNetUserDTO> dto);
        ResponseBase<AspNetUserDTO> User(MessageBase<string> id);
        ResponseBase UserDelete(MessageBase<string> id);
        ResponseBase UserEdit(MessageBase<AspNetUserDTO> dto);

        ResponseBase<IList<AspNetRoleDTO>> RoleList();
        ResponseBase RoleInsert(MessageBase<AspNetRoleDTO> dto);
        ResponseBase<AspNetRoleDTO> Role(MessageBase<string> id);
        ResponseBase RoleDelete(MessageBase<string> id);
        ResponseBase RoleEdit(MessageBase<AspNetRoleDTO> dto);

        PagedResponseBase<IList<TransmissionDTO>> TransmissionList(MessageBase<RequestDetail> request);
        ResponseBase TransmissionInsert(MessageBase<TransmissionDTO> dto);
        ResponseBase<TransmissionDTO> Transmission(MessageBase<int> id);
        ResponseBase TransmissionDelete(MessageBase<int> id);
        ResponseBase TransmissionEdit(MessageBase<TransmissionDTO> dto);

        ResponseBase GenerateTransmissionFile_Background(MessageBase<int> size, MessageBase<string> path);

        ResponseBase<IList<NameValueDTO>> GetRemainingCounts(MessageBase<long> csid, MessageBase<string> cuid);

        ResponseBase<IList<LabConsumptionDTO>> GenerateLabConsumptionFile();
    }
}
