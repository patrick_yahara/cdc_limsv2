﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DataService.Interfaces
{
    public interface ISettingsProvider
    {
        string GetConnectionString();
        string GetConnectionString_Redshift();
        string GetAuthClientId();
        string GetAuthClientSecret();
        string GetAuthVirtualDirectory();
        string GetAuthRealm();
        string GetAuthUrl();
        string GetAuthType();
        string GetAuthCaption();
        string GetAuthForceHttps();
        string GetTransmissionDropDirectory();
        string GetTransmissionErrorDirectory();
        string GetTransmissionProcessedDirectory();
        string GetHangfirePollingInterval();
        string GetErrorFileDeleteInterval();
        string GetProcessedFileDeleteInterval();
        string GetTransmissionMessageSize();
        string GetQueueFileCreateInterval();
        string GetPasswordResetEmailAddress();
        string GetPasswordResetEmailSubject();
        string GetMaxCSID();
        string GetMaxCUID();
    }
}
