﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace ContractLab.Utilities
{
    public class AspNetUserContext : IUserContext
    {
        private readonly IPrincipal _user;
        public AspNetUserContext(IPrincipal principal)
        {
            _user = principal;
        }
        public string UserId
        {
            get 
            {
                if (_user != null) { return _user.Identity.GetUserId(); } return null; 
            }
        }

        public IPrincipal User
        {
            get
            {
                if (_user != null) { return _user; } return null;
            }
        }

        public IIdentity Identity
        {
            get
            {
                if (_user != null) { return _user.Identity; } return null;
            }
        }
    }
}