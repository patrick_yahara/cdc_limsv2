﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.DataService
{
    /// <summary>
    /// A set of utility functions for mapping results of one type to another.  
    /// In particular, going from vector to scalar types and using AutoMapper to map from types T to TR
    /// </summary>
    public static class ServiceUtility
    {
        internal static ResponseBase<TR> GetMappedScalarResult<T, TR>(IEnumerable<T> results)
        {
            var queryResult = GetScalarResult(results);
            if (!queryResult.Success)
                return ResponseBase<TR>.CreateErrorResponse(queryResult.Error);

            return ResponseBase<TR>.CreateSuccessResponse(AutoMapper.Mapper.Map<TR>(queryResult.Data));
        }

        internal static ResponseBase<T> GetScalarResult<T>(IEnumerable<T> results)
        {
            var count = results.Count();
            if (count != 1)
                return ResponseBase<T>.CreateErrorResponse(new Exception(string.Format("Expected one result but returned {0}", count)));

            return ResponseBase<T>.CreateSuccessResponse(results.First());
        }


        internal static ResponseBase<TR> GetMappedCollectionResult<T, TR>(IEnumerable<T> results)
        {
            var queryResult = GetCollectionResult(results);
            if (!queryResult.Success)
                return ResponseBase<TR>.CreateErrorResponse(queryResult.Error);

            return ResponseBase<TR>.CreateSuccessResponse(AutoMapper.Mapper.Map<TR>(queryResult.Data));
        }

        internal static ResponseBase<IEnumerable<T>> GetCollectionResult<T>(IEnumerable<T> results)
        {
            if (results == null)
                return ResponseBase<IEnumerable<T>>.CreateErrorResponse(new Exception("Results returned null"));
            else
                return ResponseBase<IEnumerable<T>>.CreateSuccessResponse(results);
        }

    }
}
