﻿using ContractLab.DTO;
using ContractLab.Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DataService
{
    public static class Utility
    {
        public static PropertyInfo ErrorPI(this PropertyInfo info)
        {
            var error = info.DeclaringType.GetProperty(string.Format(@"{0}_Error", info.Name));
            return error;
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static ICollection<AspNetUserDTO> ToDTOList(this ICollection<AspNetUser> items)
        {
            var result = new List<AspNetUserDTO>();
            foreach (var e in items)
            {
                result.Add(new AspNetUserDTO()
                {
                    Email = e.Email,
                    Id = e.Id,   
                });
            }
            return result;
        }

        public static ICollection<AspNetRoleDTO> ToDTOList(this ICollection<AspNetRole> items)
        {
            var result = new List<AspNetRoleDTO>();
            foreach (var e in items)
            {
                result.Add(new AspNetRoleDTO()
                {
                    Id = e.Id,
                    Name = e.Name,
                });
            }
            return result;
        }

        public static LabDTO ToDTO(this Lab e)
        {
            if (e == null)
	        {
		        return null;
	        }
            var result = 
                new LabDTO()
                {
                    Id = e.Id,
                    Name = e.Name,
                };
            return result;
        }

        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }
    
        private const string CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static long Decode36(this string value)
        {
            var database = new List<char>(CHARACTERS);
            var tmp = new List<char>(value.ToUpper().TrimStart(new char[] { '0' }).ToCharArray());
            tmp.Reverse();

            long number = 0;
            int index = 0;
            foreach (char character in tmp)
            {
            number += database.IndexOf(character) * (long)Math.Pow(36, index);
            index++;
            }

            return number;
        }

        public static string Encode36(this long number)
        {
            var database = new List<char>(CHARACTERS);
            var value = new List<char>();
            long tmp = number;

            while (tmp != 0)
            {
            value.Add(database[Convert.ToInt32(tmp % 36)]);
            tmp /= 36;
            }

            value.Reverse();
            return new string(value.ToArray());
        }    
    
    }

}
