using System;
using System.Linq;
using ContractLab.DataService.Interfaces;
using Yahara.Core.Repository.EF;
using Yahara.ServiceInfrastructure.EF;
using Yahara.ServiceInfrastructure.Messages;
using ContractLab.Repository;
using ContractLab.Repository.Model;
using System.Collections.Generic;
using ContractLab.DTO;
using Ninject;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Security.Principal;
using ContractLab.Utilities;
using Microsoft.AspNet.Identity;
using System.Reflection;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace ContractLab.DataService
{
    /// <summary>
    /// Services should represent classes of chart types... no monolithic services
    /// </summary>
    public class ContractLabDataService : EfDataServiceBase, IContractLabDataService
    {
        private readonly int? _currentFluSeason;
        // alternatively you can loosely type this to IDbContextSource
        private readonly ContractLabDbContextSource _contextSource;
        private readonly IUserContext _user;
        private readonly Lab _lab;
        private List<string> _validIds;

        private bool? _isuserreadonly;
        public bool IsUserReadOnly
        {
            get 
            {
                if (!_isuserreadonly.HasValue)
                {
                    _isuserreadonly = _user.User.IsInRole("CDCEPI") || _user.User.IsInRole("APHL");
                }
                return _isuserreadonly.Value; 
            }
        }

        private string _userFilter;
        public string userFilter
        {
            get
            {
                if (_userFilter == null)
                {
                    if (_user.User.IsInRole("Admin") || _user.User.IsInRole("CDCEPI") || _user.User.IsInRole("CDCLAB") || _user.User.IsInRole("APHL"))
                    {
                        _userFilter = @"Id > 0";
                    }
                    else
                    {
                        using (var session = new SingleDisposableRepositorySession(_contextSource))
                        {
                            if (_lab != null && _lab.Id > 0)
                            {
                                var labUsers = session.Repository.Query<AspNetUser>().Where(s => s.LabId == _lab.Id).Select(s => s.Id).ToList();
                                if (labUsers.Count() > 0)
                                {
                                    _validIds = labUsers;
                                    _userFilter = "@0.Contains(outerIt.CreatedBy)";
                                }
                            }
                        }
                    }
                }
                if (_userFilter == null)
                {
                    _userFilter = string.Format(@"CreatedBy.Equals(""{0}"")", _user.UserId);
                }
                return _userFilter;
            }
        }

        public ContractLabDataService(ContractLabDbContextSource contextSource, IUserContext user)
        {
            var tempcurrent = DateTime.UtcNow;
            _currentFluSeason = tempcurrent.Month > 7 ? tempcurrent.Year + 1 : tempcurrent.Year;
            _contextSource = contextSource;
            _user = user;
            if (_user != null && _contextSource != null)
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var tempL = session.Repository.Query<AspNetUser>(s => s.Id == _user.UserId, new List<string>() { "Lab" }).FirstOrDefault();
                    if (tempL != null)
                    {
                        _lab = tempL.Lab;
                    }
                }
            }
        }

        public ResponseBase<int> DoTestCall(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction<int>(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var query = session.Repository.Query<Specimen>().ToList();

                    return ResponseBase<int>.CreateErrorResponse(new NotImplementedException());
                }
            });
        }

        public ResponseBase<IList<ProjectDTO>> ProjectList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Project>()
                .Select(e => new ProjectDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenCount = e.Specimens.Count,
                }
                )
                .Where(userFilter,  _validIds)
                .ToList();
                return ResponseBase<IList<ProjectDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase ProjectInsert(MessageBase<ProjectDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (_lab == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Projects without a lab assigned."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new Project() { 
                        Name = dto.Data.Name, 
                        CreatedOn = DateTime.UtcNow, 
                        ModifiedOn = DateTime.UtcNow, 
                        CreatedBy = _user.Identity.GetUserId(), 
                        ModifiedBy = _user.Identity.GetUserId() 
                    };
                    session.Repository.Insert<Project>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<ProjectDTO> Project(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Project>().Where(s => s.Id == id.Data)
                .Where(userFilter,  _validIds)
                .Select(e => new ProjectDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenCount = e.Specimens.Count,
                }
                )
                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<ProjectDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<ProjectDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase ProjectEdit(MessageBase<ProjectDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Projects."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Project>().Where(s => s.Id == dto.Data.Id)
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Project>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase ProjectDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Projects."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Project>().Where(s => s.Id == id.Data)
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Project>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public PagedResponseBase<IList<SpecimenDTO>> SpecimenList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";
            string fluseasonwhere = "Id > 0";
            if (request.Data.FluSeason.HasValue)
            {
                fluseasonwhere = "FluSeason = " + request.Data.FluSeason.ToString();
            }
            if (!String.IsNullOrEmpty(request.Data.IdFilter))
            {
                idwhere = String.Format(@"SpecimenID.Contains(""{0}"")
                 OR CSID.Contains(""{0}"")
                 OR Package.Sender.RasClientId.Contains(""{0}"")
                 OR Package.Sender.CompanyName.Contains(""{0}"")
                 OR Package.Name.Contains(""{0}"")
                ", request.Data.IdFilter);
            }

            string fromWhere = "Id > 0";
            if (request.Data.From.HasValue)
            {
                fromWhere = "FilterDate >= @0";
            }
            string toWhere = "Id > 0";
            if (request.Data.To.HasValue)
            {
                toWhere = "FilterDate <= @0";
            }

            using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    // * I am going to cheat here
                    if (!String.IsNullOrEmpty(request.Data.CUID))
                    {
                        var aliquot = session.Repository.Query<Aliquot>(s => s.CUID == request.Data.CUID, new List<string>() { "Isolate", "Isolate.Specimen" }).FirstOrDefault();
                        if (aliquot != null)
                        {
                            idwhere = String.Format("Id = {0}", aliquot.Isolate.Specimen.Id);
                        }
                        else
                        {
                            idwhere = "Id = 0";
                        }
                    }

                    IQueryable<SpecimenDTO> query = session.Repository.Query<Specimen>()
                    .Where(userFilter, _validIds)
                    .Where(request.Data.Where)
                    .Where(idwhere)
                    .Where(fluseasonwhere)
                    .Select(e => new SpecimenDTO
                    {
                        FluSeason = e.FluSeason,
                        Id = e.Id,
                        DateInoculated = e.DateInoculated,
                        SampleClass = e.SampleClass,
                        CSID = e.CSID,
                        //CUID = e.CUID,
                        CreatedOn = e.CreatedOn,
                        CreatedBy = e.CreatedBy,
                        ModifiedBy = e.ModifiedBy,
                        ModifiedOn = e.ModifiedOn,
                        ProjectId = e.ProjectId,
                        IsolateCount = e.Isolates.Count,
                        Package = new PackageDTO()
                        {
                            Id = e.Package.Id,
                            Name = e.Package.Name,
                            SenderId = e.Package.SenderId,
                            DateReceived = e.Package.DateReceived,
                            SpecimenCondition = e.Package.SpecimenCondition,
                            Sender = new SenderDTO()
                            {
                                Id = e.Package.Sender.Id,
                                RasClientId = e.Package.Sender.RasClientId,
                                CompanyName = e.Package.Sender.CompanyName,
                                Address = e.Package.Sender.Address,
                                City = e.Package.Sender.City,
                                State = e.Package.Sender.State,
                                FluSeason = e.Package.Sender.FluSeason,
                            }
                        },
                        PackageId = e.PackageId,
                        Country = new CountryDTO() { Id = e.Country.Id, Name = e.Country.Name },
                        CountryId = e.CountryId,
                        State = new StateDTO() { Id = e.State.Id, Name = e.State.Name, Abbreviation = e.State.Abbreviation },
                        StateId = e.StateId,
                        County = new CountyDTO() { Id = e.County.Id, Name = e.County.Name },
                        CountyId = e.CountyId,
                        Activity = e.Activity,
                        DateCollected = e.DateCollected,
                        SpecimenID = e.SpecimenID,
                        PatientAge = e.PatientAge,
                        PatientAgeUnits = e.PatientAgeUnits,
                        PatientSex = e.PatientSex,
                        PassageHistory = e.PassageHistory,
                        SubType = e.SubType,
                        SpecimenSource = e.SpecimenSource,
                        InitialStorage = e.InitialStorage,
                        CompCSID1 = e.CompCSID1,
                        CompCSID2 = e.CompCSID2,
                        OtherLocation = e.OtherLocation,
                        DrugResistant = e.DrugResistant,
                        Deceased = e.Deceased,
                        SpecialStudy = e.SpecialStudy,
                        NursingHome = e.NursingHome,
                        Vaccinated = e.Vaccinated,
                        Travel = e.Travel,
                        ReasonForSubmission = e.ReasonForSubmission,
                        Comments = e.Comments,
                        StatusId = e.StatusId,
                        Status = new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName },
                        FilterDate = e.Package != null && e.Package.DateReceived.HasValue ? e.Package.DateReceived.Value : e.CreatedOn,
                        TravelCountry = e.TravelCountry,
                        TravelState = e.TravelState,
                        TravelCounty = e.TravelCounty,
                        TravelCity = e.TravelCity,
                        TravelReturnDate = e.TravelReturnDate,
                        HostCellLine = e.HostCellLine,
                        Substrate = e.Substrate,
                        PassageNumber = e.PassageNumber,
                        PCRInfA = e.PCRInfA,
                        PCRpdmInfA = e.PCRpdmInfA,
                        PCRpdmH1 = e.PCRpdmH1,
                        PCRH3 = e.PCRH3,
                        PCRInfB = e.PCRInfB,
                        PCRBVic = e.PCRBVic,
                        PCRBYam = e.PCRBYam,
                        City = e.City,
                        WHOSubmittedSubtype = e.WHOSubmittedSubtype,
                    }
                    )
                    .Where(fromWhere, request.Data.From)
                    .Where(toWhere, request.Data.To)
                    .OrderBy(request.Data.OrderBy);
                    int count = query.Count();
                    List<SpecimenDTO> entity = new List<SpecimenDTO>(query.Skip<SpecimenDTO>(skip).Take<SpecimenDTO>(take));
                    return PagedResponseBase<IList<SpecimenDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
                }        
        }

        public ResponseBase SpecimenInsert(MessageBase<SpecimenDTO> dto)
        {
            {
                return ExecuteValidatedDataAction(() =>
                {
                    if (_lab == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("Cannot insert without a lab assigned."));
                    }

                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {
                        var status = session.Repository.FindBy<Status>(s => s.SysName.Equals("INPROGRESS"), null);
                        if (status == null)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "INPROGRESS")));
                        }

                        var isolateType = session.Repository.FindBy<IsolateType>(s => s.SysName.Equals("ORIGINAL"), null);
                        if (isolateType == null)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"IsolateType {0} not found.", "ORIGINAL")));
                        }

                        SpecimenInsert_Internal(dto, session, status, isolateType);

                        return ResponseBase.CreateSuccessResponse();
                    }
                });
            }
        }

        private void SpecimenInsert_Internal(MessageBase<SpecimenDTO> dto, SingleDisposableRepositorySession session, Status status, ContractLab.Repository.Model.IsolateType isolateType)
        {
            var clabEntity = session.Repository.Query<AspNetUser>(s => s.Id.Equals(_user.UserId), new List<string>() { "Lab" }).Select(s => s.Lab).FirstOrDefault();

            var newentity = new Specimen()
            {
                DateInoculated = dto.Data.DateInoculated,
                SampleClass = dto.Data.SampleClass,
                CSID = dto.Data.CSID,
                //CUID = dto.Data.CUID,
                ProjectId = dto.Data.ProjectId,
                PackageId = dto.Data.PackageId,
                CountryId = dto.Data.CountryId,
                StateId = dto.Data.StateId,
                CountyId = dto.Data.CountyId,
                Activity = dto.Data.Activity,
                DateCollected = dto.Data.DateCollected,
                SpecimenID = dto.Data.SpecimenID,
                PatientAge = dto.Data.PatientAge,
                PatientAgeUnits = dto.Data.PatientAgeUnits,
                PatientSex = dto.Data.PatientSex,
                PassageHistory = dto.Data.PassageHistory,
                SubType = dto.Data.SubType,
                SpecimenSource = dto.Data.SpecimenSource,
                InitialStorage = dto.Data.InitialStorage,
                CompCSID1 = dto.Data.CompCSID1,
                CompCSID2 = dto.Data.CompCSID2,
                OtherLocation = dto.Data.OtherLocation,
                DrugResistant = dto.Data.DrugResistant,
                Deceased = dto.Data.Deceased,
                SpecialStudy = dto.Data.SpecialStudy,
                NursingHome = dto.Data.NursingHome,
                Vaccinated = dto.Data.Vaccinated,
                Travel = dto.Data.Travel,
                ReasonForSubmission = dto.Data.ReasonForSubmission,
                Comments = dto.Data.Comments,
                StatusId = status.Id,
                ContractLabFrom = clabEntity != null ? clabEntity.Name : null,
                LabId = _lab.Id,
                CreatedOn = DateTime.UtcNow,
                ModifiedOn = DateTime.UtcNow,
                CreatedBy = _user.Identity.GetUserId(),
                ModifiedBy = _user.Identity.GetUserId(),

                TravelCountry = dto.Data.TravelCountry,
                TravelState = dto.Data.TravelState,
                TravelCounty = dto.Data.TravelCounty,
                TravelCity = dto.Data.TravelCity,
                TravelReturnDate = dto.Data.TravelReturnDate,
                HostCellLine = dto.Data.HostCellLine,
                Substrate = dto.Data.Substrate,
                PassageNumber = dto.Data.PassageNumber,
                PCRInfA = dto.Data.PCRInfA,
                PCRpdmInfA = dto.Data.PCRpdmInfA,
                PCRpdmH1 = dto.Data.PCRpdmH1,
                PCRH3 = dto.Data.PCRH3,
                PCRInfB = dto.Data.PCRInfB,
                PCRBVic = dto.Data.PCRBVic,
                PCRBYam = dto.Data.PCRBYam,
                City = dto.Data.City,
                WHOSubmittedSubtype = dto.Data.WHOSubmittedSubtype,
            };

            session.Repository.Insert<Specimen>(newentity);

            var newisolate = new Isolate()
            {
                SpecimenId = newentity.Id,
                IsolateTypeId = isolateType.Id,
                CreatedOn = DateTime.UtcNow,
                ModifiedOn = DateTime.UtcNow,
                CreatedBy = _user.Identity.GetUserId(),
                ModifiedBy = _user.Identity.GetUserId()
            };

            session.Repository.Insert<Isolate>(newisolate);

            //CLAW-41
            var storedLocally = new string[] { };
            if (_lab != null)
            {
                if (!String.IsNullOrEmpty(_lab.StoredLocallyCodes))
                {

                    var codes = string.Join("", _lab.StoredLocallyCodes.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                    storedLocally = codes.Split(',');
                }
            }

            var newaliquot = new Aliquot()
            {
                CUID = "",
                TestCode = "RCV",
                //CLAW-41
                IsStoredLocally = storedLocally.Contains("RCV"),
                IsolateId = newisolate.Id,
                CreatedOn = DateTime.UtcNow,
                ModifiedOn = DateTime.UtcNow,
                CreatedBy = _user.Identity.GetUserId(),
                ModifiedBy = _user.Identity.GetUserId()
            };
            session.Repository.Insert<Aliquot>(newaliquot);

            if (_lab != null)
            {
                var labWhere = "Id > 0";
                labWhere = String.Format(@"LabId = {0}", _lab.Id);
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "ISR")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity != null)
                {
                    var newaliquotISR = new Aliquot()
                    {
                        CUID = "",
                        TestCode = "ISR",
                        //CLAW-41
                        IsStoredLocally = storedLocally.Contains("ISR"),
                        IsolateId = newisolate.Id,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId()
                    };
                    session.Repository.Insert<Aliquot>(newaliquotISR);
                }
            }
        }


        public ResponseBase SpecimenDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimens."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Specimen>(s => s.Id == id.Data, new List<string>() {"Status", "Isolates", "Isolates.Aliquots", "SpecimenNotes" })
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        if (!entity.Status.SysName.Equals("INPROGRESS"))
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot delete a specimen with the status {0}", entity.Status.DisplayName)));
                        }
                        var arrayIsolates = entity.Isolates.ToArray<Isolate>();
                        for (int i = arrayIsolates.Length - 1; i >= 0; i--)
                        {
                            var isolate = arrayIsolates[i];
                            var arrayAliquots = isolate.Aliquots.ToArray<Aliquot>();
                            for (int j = arrayAliquots.Length - 1; j >= 0; j--)
                            {
                                var aliquot = arrayAliquots[j];
                                session.Repository.Delete<Aliquot>(aliquot);
                            }
                            session.Repository.Delete<Isolate>(isolate);
                        }
                        var arrayNotes = entity.SpecimenNotes.ToArray<SpecimenNote>();
                        for (int i = arrayNotes.Length -1; i >= 0; i--)
                        {
                             var note = arrayNotes[i];
                             session.Repository.Delete<SpecimenNote>(note);
                        }
                        session.Repository.Delete<Specimen>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        ResponseBase<SpecimenDTO> IContractLabDataService.Specimen(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Specimen>().Where(s => s.Id == id.Data)
                .Select(e => new SpecimenDTO
                {
                    
                    FluSeason = e.FluSeason,
                    Id = e.Id,
                    DateInoculated = e.DateInoculated,
                    SampleClass = e.SampleClass,
                    CSID = e.CSID,
                    //CUID = e.CUID,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    ProjectId = e.ProjectId,
                    IsolateCount = e.Isolates.Count,
                    Package = new PackageDTO()
                    {
                        Id = e.Package.Id,
                        Name = e.Package.Name,
                        SenderId = e.Package.SenderId,
                        DateReceived = e.Package.DateReceived,
                        SpecimenCondition = e.Package.SpecimenCondition,
                        Sender = new SenderDTO()
                        {
                            Id = e.Package.Sender.Id,
                            RasClientId = e.Package.Sender.RasClientId,
                            CompanyName = e.Package.Sender.CompanyName,
                            Address = e.Package.Sender.Address,
                            City = e.Package.Sender.City,
                            State = e.Package.Sender.State,
                            FluSeason = e.Package.Sender.FluSeason,
                        }
                    },
                    PackageId = e.PackageId,
                    Country = new CountryDTO() { Id = e.Country.Id, Name = e.Country.Name, ShortCode = e.Country.ShortCode, LongCode = e.Country.LongCode },
                    CountryId = e.CountryId,
                    State = new StateDTO() { Id = e.State.Id, Name = e.State.Name, Abbreviation = e.State.Abbreviation },
                    StateId = e.StateId,
                    County = new CountyDTO() { Id = e.County.Id, Name = e.County.Name },
                    CountyId = e.CountyId,
                    Activity = e.Activity,
                    DateCollected = e.DateCollected,
                    SpecimenID = e.SpecimenID,
                    PatientAge = e.PatientAge,
                    PatientAgeUnits = e.PatientAgeUnits,
                    PatientSex = e.PatientSex,
                    PassageHistory = e.PassageHistory,
                    SubType = e.SubType,
                    SpecimenSource = e.SpecimenSource,
                    InitialStorage = e.InitialStorage,
                    CompCSID1 = e.CompCSID1,
                    CompCSID2 = e.CompCSID2,
                    OtherLocation = e.OtherLocation,
                    DrugResistant = e.DrugResistant,
                    Deceased = e.Deceased,
                    SpecialStudy = e.SpecialStudy,
                    NursingHome = e.NursingHome,
                    Vaccinated = e.Vaccinated,
                    Travel = e.Travel,
                    ReasonForSubmission = e.ReasonForSubmission,
                    Comments = e.Comments,
                    StatusId = e.StatusId,
                    Status = new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName },

                    TravelCountry = e.TravelCountry,
                    TravelState = e.TravelState,
                    TravelCounty = e.TravelCounty,
                    TravelCity = e.TravelCity,
                    TravelReturnDate = e.TravelReturnDate,
                    HostCellLine = e.HostCellLine,
                    Substrate = e.Substrate,
                    PassageNumber = e.PassageNumber,
                    PCRInfA = e.PCRInfA,
                    PCRpdmInfA = e.PCRpdmInfA,
                    PCRpdmH1 = e.PCRpdmH1,
                    PCRH3 = e.PCRH3,
                    PCRInfB = e.PCRInfB,
                    PCRBVic = e.PCRBVic,
                    PCRBYam = e.PCRBYam,
                    City = e.City,
                    WHOSubmittedSubtype = e.WHOSubmittedSubtype,
                }
                )
                .Where(userFilter,  _validIds)
                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<SpecimenDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<SpecimenDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase SpecimenEdit(MessageBase<SpecimenDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimens."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Specimen>(s => s.Id == dto.Data.Id, new List<string>() {"Status"})
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        //if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                        if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            {
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot edit a specimen with the status {0}", entity.Status.DisplayName)));
                            }
                            else
                            { 
                                var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                                if (notecategory != null)
                                {
                                    var note = new SpecimenNote()
                                    {
                                        CreatedBy = _user.Identity.GetUserId(),
                                        CreatedOn = DateTime.UtcNow,
                                        ModifiedBy = _user.Identity.GetUserId(),
                                        ModifiedOn = DateTime.UtcNow,
                                        NoteCategoryId = notecategory.Id,
                                        Note = string.Format(@"Specimen Edited With a Status of {0}", entity.Status.DisplayName),
                                        SpecimenId = entity.Id,
                                    };
                                    session.Repository.Insert<SpecimenNote>(note);
                                }
                                else
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimen (missing note category)"));
                                }
                            }
                        }
                        entity.DateInoculated = dto.Data.DateInoculated;
                        entity.SampleClass = dto.Data.SampleClass;
                        if (_lab != null)
                        {
                            entity.PackageId = dto.Data.PackageId;    
                        }
                        entity.CountryId = dto.Data.CountryId;
                        entity.StateId = dto.Data.StateId;
                        entity.CountyId = dto.Data.CountyId;
                        entity.Activity = dto.Data.Activity;
                        entity.DateCollected = dto.Data.DateCollected;
                        entity.SpecimenID = dto.Data.SpecimenID;
                        entity.PatientAge = dto.Data.PatientAge;
                        entity.PatientAgeUnits = dto.Data.PatientAgeUnits;
                        entity.PatientSex = dto.Data.PatientSex;
                        entity.PassageHistory = dto.Data.PassageHistory;
                        entity.SubType = dto.Data.SubType;
                        entity.SpecimenSource = dto.Data.SpecimenSource;
                        entity.InitialStorage = dto.Data.InitialStorage;
                        entity.CompCSID1 = dto.Data.CompCSID1;
                        entity.CompCSID2 = dto.Data.CompCSID2;
                        entity.OtherLocation = dto.Data.OtherLocation;
                        entity.DrugResistant = dto.Data.DrugResistant;
                        entity.Deceased = dto.Data.Deceased;
                        entity.SpecialStudy = dto.Data.SpecialStudy;
                        entity.NursingHome = dto.Data.NursingHome;
                        entity.Vaccinated = dto.Data.Vaccinated;
                        entity.Travel = dto.Data.Travel;
                        entity.ReasonForSubmission = dto.Data.ReasonForSubmission;
                        entity.Comments = dto.Data.Comments;
                        entity.StatusId = dto.Data.StatusId;

                        entity.TravelCountry = dto.Data.TravelCountry;
                        entity.TravelState = dto.Data.TravelState;
                        entity.TravelCounty = dto.Data.TravelCounty;
                        entity.TravelCity = dto.Data.TravelCity;
                        entity.TravelReturnDate = dto.Data.TravelReturnDate;
                        entity.HostCellLine = dto.Data.HostCellLine;
                        entity.Substrate = dto.Data.Substrate;
                        entity.PassageNumber = dto.Data.PassageNumber;
                        entity.PCRInfA = dto.Data.PCRInfA;
                        entity.PCRpdmInfA = dto.Data.PCRpdmInfA;
                        entity.PCRpdmH1 = dto.Data.PCRpdmH1;
                        entity.PCRH3 = dto.Data.PCRH3;
                        entity.PCRInfB = dto.Data.PCRInfB;
                        entity.PCRBVic = dto.Data.PCRBVic;
                        entity.PCRBYam = dto.Data.PCRBYam;
                        entity.City = dto.Data.City;
                        entity.WHOSubmittedSubtype = dto.Data.WHOSubmittedSubtype;

                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Specimen>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<PackageDTO>> PackageList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Package>()
                .Where(userFilter,  _validIds)
                .Select(e => new PackageDTO
                {
                    
                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenCount = e.Specimens.Count,
                    SenderId = e.SenderId,
                    IsVisible = e.IsVisible,
                    Comments = e.Comments,
                    DateReceived = e.DateReceived,
                    SpecimenCondition = e.SpecimenCondition,
                    Sender = new SenderDTO()
                    {
                        Id = e.Sender.Id,
                        RasClientId = e.Sender.RasClientId,
                        CompanyName = e.Sender.CompanyName,
                        Address = e.Sender.Address,
                        City = e.Sender.City,
                        State = e.Sender.State,
                        FluSeason = e.Sender.FluSeason,
                    },
                }
                ).ToList();
                return ResponseBase<IList<PackageDTO>>.CreateSuccessResponse(entity);
            }
        }

        public PagedResponseBase<IList<PackageDTO>> PackageList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";

            if (!String.IsNullOrEmpty(request.Data.IdFilter))
            {
                idwhere = String.Format(@"Sender.RasClientId.Contains(""{0}"")
                 OR Sender.CompanyName.Contains(""{0}"")
                 OR Name.Contains(""{0}"")
                ", request.Data.IdFilter);
            }

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                IQueryable<PackageDTO> query = session.Repository.Query<Package>()
                .Where(userFilter, _validIds)
                .Where(idwhere)
                .Select(e => new PackageDTO
                {
                    
                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenCount = e.Specimens.Count,
                    SenderId = e.SenderId,
                    IsVisible = e.IsVisible,
                    Comments = e.Comments,
                    DateReceived = e.DateReceived,
                    SpecimenCondition = e.SpecimenCondition,
                    Sender = new SenderDTO()
                    {
                        Id = e.Sender.Id,
                        RasClientId = e.Sender.RasClientId,
                        CompanyName = e.Sender.CompanyName,
                        Address = e.Sender.Address,
                        City = e.Sender.City,
                        State = e.Sender.State,
                        FluSeason = e.Sender.FluSeason,
                    },
                }
                ).OrderBy(request.Data.OrderBy);
                int count = query.Count();
                List<PackageDTO> entity = new List<PackageDTO>(query.Skip<PackageDTO>(skip).Take<PackageDTO>(take));
                return PagedResponseBase<IList<PackageDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
            }
        }

        public ResponseBase PackageInsert(MessageBase<PackageDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (_lab == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Packages without a lab assigned."));
                }

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var newentity = new Package() { 
                        Name = dto.Data.Name,
                        SenderId = dto.Data.SenderId,
                        IsVisible = dto.Data.IsVisible,
                        Comments = dto.Data.Comments,
                        DateReceived = dto.Data.DateReceived,
                        SpecimenCondition = dto.Data.SpecimenCondition,
                        CreatedOn = DateTime.UtcNow, 
                        ModifiedOn = DateTime.UtcNow, 
                        CreatedBy = _user.Identity.GetUserId(), 
                        ModifiedBy = _user.Identity.GetUserId(),
                    };
                    session.Repository.Insert<Package>(newentity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase PackageDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Packages."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Package>().Where(s => s.Id == id.Data)
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Package>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<PackageDTO> Package(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Package>(s => s.Id == id.Data, new List<String>() { "Sender", "Contact" })
                .Where(userFilter, _validIds)
                .Select(e => new PackageDTO
                {

                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenCount = e.Specimens.Count,
                    SenderId = e.SenderId,
                    IsVisible = e.IsVisible,
                    Comments = e.Comments,
                    DateReceived = e.DateReceived,
                    SpecimenCondition = e.SpecimenCondition,
                    Sender = new SenderDTO()
                    {
                        Id = e.Sender.Id,
                        RasClientId = e.Sender.RasClientId,
                        CompanyName = e.Sender.CompanyName,
                        Address = e.Sender.Address,
                        City = e.Sender.City,
                        State = e.Sender.State,
                        FluSeason = e.Sender.FluSeason,
                    },
                    Contact = new ContactDTO() 
                    { 
                        Id = e.Contact != null ? e.Contact.Id : 0,
                        FullName = e.Contact.FullName,
                        Address = e.Contact.Address,
                        City = e.Contact.City,
                        State = e.Contact.State,
                        Country = e.Contact.Country,
                        Phone = e.Contact.Phone,
                        Fax = e.Contact.Fax,
                        Email = e.Contact.Email,
                        FluSeason = e.Contact.FluSeason,
                    }
                }
                ).FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase<PackageDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                var specimens = session.Repository.Query<Specimen>(s => s.PackageId == entity.Id).ToList();
                foreach (var item in specimens)
                {
                    entity.Specimens.Add(new SpecimenDTO()
                    {
                        Id = item.Id,
                        CSID = item.CSID,
                        SampleClass = item.SampleClass,
                        SpecimenID = item.SpecimenID,
                        DateCollected = item.DateCollected,
                        SubType = item.SubType,
                    });
                }
                if (entity.Sender != null && (entity.Contact == null || entity.Contact.Id == 0))
                {
                    var e = session.Repository.Query<Contact>(s => s.RasClientId == entity.Sender.RasClientId).FirstOrDefault();
                    if (e != null)
                    {
                        entity.Contact = new ContactDTO() 
                        {
                            Id = e.Id,
                            FullName = e.FullName,
                            Address = e.Address,
                            City = e.City,
                            State = e.State,
                            Country = e.Country,
                            Phone = e.Phone,
                            Fax = e.Fax,
                            Email = e.Email,
                            FluSeason = e.FluSeason,
                        };
                    }
                }
                return ResponseBase<PackageDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase PackageEdit(MessageBase<PackageDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Packages."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Package>().Where(s => s.Id == dto.Data.Id)
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.SenderId = dto.Data.SenderId;
                        entity.IsVisible = dto.Data.IsVisible;
                        entity.Comments = dto.Data.Comments;
                        entity.DateReceived = dto.Data.DateReceived;
                        entity.SpecimenCondition = dto.Data.SpecimenCondition;
                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Package>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<IsolateDTO>> IsolateList(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Isolate>(s => s.SpecimenId == id.Data, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new IsolateDTO
                {
                    
                    Id = e.Id,
                    PassageLevel = e.PassageLevel,
                    DateHarvested = e.DateHarvested,
                    Titer = e.Titer,
                    RedBloodCellType = e.RedBloodCellType,
                    DateInoculated = e.DateInoculated,
                    Comments = e.Comments,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenId = e.SpecimenId,
                    AliquotCount = e.Aliquots.Count,
                    IsolateTypeId = e.IsolateType.Id,
                    IsolateType = new IsolateTypeDTO() { Id = e.IsolateType.Id, DisplayName = e.IsolateType.DisplayName, SysName = e.IsolateType.SysName },
                    Specimen = new SpecimenDTO() { Id = e.Specimen.Id, SubType = e.Specimen.SubType, CSID = e.Specimen.CSID, Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName } },
                    StatusId = e.StatusId,
                    Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,

                }
                ).ToList();
                return ResponseBase<IList<IsolateDTO>>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase IsolateInsert(MessageBase<IsolateDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (_lab == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Sub Samples without a lab assigned."));
                }

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Specimen>(s => s.Id == dto.Data.SpecimenId, new List<string>() { "Status" })
                    .Where(userFilter, _validIds).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Specimen Id {0} not found.", dto.Data.SpecimenId)));
                    }
                    if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                    //if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS"))
                    {
                        if (!_user.User.IsInRole("Lab Editor"))
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot add isolates to a specimen with the status {0}", entity.Status.DisplayName)));
                        }
                        else
                        {
                            var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                            if (notecategory != null)
                            {
                                var note = new SpecimenNote()
                                {
                                    CreatedBy = _user.Identity.GetUserId(),
                                    CreatedOn = DateTime.UtcNow,
                                    ModifiedBy = _user.Identity.GetUserId(),
                                    ModifiedOn = DateTime.UtcNow,
                                    NoteCategoryId = notecategory.Id,
                                    Note = string.Format(@"Specimen Edited With a Status of {0} (Isolate Added)", entity.Status.DisplayName),
                                    SpecimenId = entity.Id,
                                };
                                session.Repository.Insert<SpecimenNote>(note);
                            }
                            else
                            {
                                return ResponseBase.CreateErrorResponse(new Exception("Cannot add isolate (missing note category)"));
                            }
                        }
                    }
                    var newentity = new Isolate() {
                        PassageLevel = dto.Data.PassageLevel,
                        DateHarvested = dto.Data.DateHarvested,
                        Titer = dto.Data.Titer,
                        RedBloodCellType = dto.Data.RedBloodCellType,
                        DateInoculated = dto.Data.DateInoculated,
                        SpecimenId = dto.Data.SpecimenId,
                        IsolateTypeId = dto.Data.IsolateTypeId,
                        Comments = dto.Data.Comments,
                        StatusId = dto.Data.StatusId,
                        CreatedOn = DateTime.UtcNow, ModifiedOn = DateTime.UtcNow, CreatedBy = _user.Identity.GetUserId(), ModifiedBy = _user.Identity.GetUserId() };
                    session.Repository.Insert<Isolate>(newentity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase IsolateDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Sub Samples."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Isolate>(s => s.Id == id.Data, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        if (!entity.Specimen.Status.SysName.Equals("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            {
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot delete isolates from a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                            }
                        }
                        var arrayAliquots = entity.Aliquots.ToArray<Aliquot>();
                        for (int j = arrayAliquots.Length - 1; j >= 0; j--)
                        {
                            var aliquot = arrayAliquots[j];
                            session.Repository.Delete<Aliquot>(aliquot);
                        }
                        session.Repository.Delete<Isolate>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IsolateDTO> Isolate(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Isolate>(s => s.Id == id.Data, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new IsolateDTO
                {
                    
                    Id = e.Id,
                    PassageLevel = e.PassageLevel,
                    DateHarvested = e.DateHarvested,
                    Titer = e.Titer,
                    RedBloodCellType = e.RedBloodCellType,
                    DateInoculated = e.DateInoculated,
                    Comments = e.Comments,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenId = e.SpecimenId,
                    AliquotCount = e.Aliquots.Count,
                    IsolateTypeId = e.IsolateType.Id,
                    IsolateType = new IsolateTypeDTO() { Id = e.IsolateType.Id, DisplayName = e.IsolateType.DisplayName, SysName = e.IsolateType.SysName },
                    Specimen = new SpecimenDTO() { Id = e.Specimen.Id, SubType = e.Specimen.SubType, CSID = e.Specimen.CSID, Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName } },
                    HasRCVAliquot = e.Aliquots.Any(s => s.TestCode.Equals("RCV")),
                    StatusId = e.StatusId,
                    Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<IsolateDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<IsolateDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase IsolateEdit(MessageBase<IsolateDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Sub Samples."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Isolate>(s => s.Id == dto.Data.Id, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        //if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                        if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            {
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot edit isolates from a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                            }
                            else
                            {
                                var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                                if (notecategory != null)
                                {
                                    var note = new SpecimenNote()
                                    {
                                        CreatedBy = _user.Identity.GetUserId(),
                                        CreatedOn = DateTime.UtcNow,
                                        ModifiedBy = _user.Identity.GetUserId(),
                                        ModifiedOn = DateTime.UtcNow,
                                        NoteCategoryId = notecategory.Id,
                                        Note = string.Format(@"Specimen Edited With a Status of {0} (Isolate Edited)", entity.Status.DisplayName),
                                        SpecimenId = entity.SpecimenId,
                                    };
                                    session.Repository.Insert<SpecimenNote>(note);
                                }
                                else
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit isolate (missing note category)"));
                                }
                            }
                        }
                        entity.PassageLevel = dto.Data.PassageLevel;
                        entity.DateHarvested = dto.Data.DateHarvested;
                        entity.Titer = dto.Data.Titer;
                        entity.RedBloodCellType = dto.Data.RedBloodCellType;
                        entity.DateInoculated = dto.Data.DateInoculated;
                        entity.IsolateTypeId = dto.Data.IsolateTypeId;
                        entity.Comments = dto.Data.Comments;
                        entity.StatusId = dto.Data.StatusId;
                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Isolate>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase DEIFileMerge(MessageBase<DEIFileDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    using (var file = new MemoryStream(dto.Data.Content))
                    {
                        using (var zip = new ZipArchive(file, ZipArchiveMode.Read))
                        {
                            #region CATEGORY
                            var entryCAT = zip.Entries.Where(s => s.FullName.StartsWith("PICK_LIST_MGR_CATEGORY") && Path.GetExtension(s.FullName).Equals(".txt")).FirstOrDefault();
                            {
                                if (entryCAT == null)
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("PICK_LIST_MGR_CATEGORY not found"));
                                }
                                using (var stream = entryCAT.Open())
                                {
                                    var ds = new DataSet();
                                    ds.ReadXml(stream);
                                    Console.WriteLine(ds.Tables[0].TableName);
                                    var category = ds.Tables[0].Columns.IndexOf("CATEGORY");
                                    var lits = ds.Tables[0].Columns.IndexOf("LITS_QUESTION_ID");
                                    var blank = ds.Tables[0].Columns.IndexOf("ALLOWBLANK");
                                    var description = ds.Tables[0].Columns.IndexOf("CATEGORY_DESCRIPTION");
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        var row = ds.Tables[0].Rows[i].ItemArray;
                                        var sTemp = row[lits].ToString();
                                        var item = session.Repository.Query<Category>().Where(s => s.LitsQuestionId == sTemp).FirstOrDefault();
                                        if (item != null)
                                        {
                                            item.Name = row[category].ToString();
                                            item.Description = row[description].ToString();
                                            item.AllowBlank = row[blank].ToString().ToLower().Equals("y");
                                            item.ModifiedBy = _user.Identity.GetUserId();
                                            item.ModifiedOn = DateTime.UtcNow;
                                            session.Repository.Update<Category>(item);
                                        }
                                        else
                                        {
                                            var newitem = new Category()
                                            {
                                                Name = row[category].ToString(),
                                                Description = row[description].ToString(),
                                                LitsQuestionId = row[lits].ToString(),
                                                AllowBlank = row[blank].ToString().ToLower().Equals("y"),
                                                CreatedBy = _user.Identity.GetUserId(),
                                                CreatedOn = DateTime.UtcNow,
                                                ModifiedBy = _user.Identity.GetUserId(),
                                                ModifiedOn = DateTime.UtcNow,
                                            };
                                            session.Repository.Insert<Category>(newitem);
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region CONDITIONS
                            var entryCOND = zip.Entries.Where(s => s.FullName.StartsWith("PICK_LIST_MGR_CONDITIONS") && Path.GetExtension(s.FullName).Equals(".txt")).FirstOrDefault();
                            {
                                if (entryCOND == null)
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("PICK_LIST_MGR_CONDITIONS not found"));
                                }

                                using (var stream = entryCOND.Open())
                                {
                                    var ds = new DataSet();
                                    ds.ReadXml(stream);
                                    Console.WriteLine(ds.Tables[0].TableName);
                                    var condition = ds.Tables[0].Columns.IndexOf("CONDITION");
                                    var lits = ds.Tables[0].Columns.IndexOf("LITS_QUESTION_ID");
                                    var sorter = ds.Tables[0].Columns.IndexOf("SORTER");
                                    var description = ds.Tables[0].Columns.IndexOf("CONDITION_DESCRIPTION");
                                    var status = ds.Tables[0].Columns.IndexOf("STATUS");
                                    var dept = ds.Tables[0].Columns.IndexOf("DEPT");
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        var row = ds.Tables[0].Rows[i].ItemArray;
                                        var bImport = row[dept].ToString().ToLower().Contains("influenza");
                                        if (!bImport)
                                        {
                                            continue;
                                        }
                                        var sTemp = row[lits].ToString();
                                        var sTemp2 = row[condition].ToString();
                                        int tempInt;
                                        int.TryParse(row[sorter].ToString(), out tempInt);
                                        var item = session.Repository.Query<Condition>().Where(s => s.LitsQuestionId == sTemp
                                            && s.Name.Equals(sTemp2)
                                            ).FirstOrDefault();
                                        if (item != null)
                                        {
                                            item.Description = row[description].ToString();
                                            item.Sorter = tempInt;
                                            item.Status = row[status].ToString();
                                            item.ModifiedBy = _user.Identity.GetUserId();
                                            item.ModifiedOn = DateTime.UtcNow;
                                            session.Repository.Update<Condition>(item);
                                        }
                                        else
                                        {
                                            var cat = session.Repository.Query<Category>().Where(s => s.LitsQuestionId == sTemp).FirstOrDefault();
                                            if (cat != null && cat.Id > 0)
                                            {
                                                var newitem = new Condition()
                                                {
                                                    Name = row[condition].ToString(),
                                                    Description = row[description].ToString(),
                                                    LitsQuestionId = row[lits].ToString(),
                                                    Sorter = tempInt,
                                                    Status = row[status].ToString(),
                                                    CategoryId = cat.Id,
                                                    CreatedBy = _user.Identity.GetUserId(),
                                                    CreatedOn = DateTime.UtcNow,
                                                    ModifiedBy = _user.Identity.GetUserId(),
                                                    ModifiedOn = DateTime.UtcNow,
                                                };
                                                session.Repository.Insert<Condition>(newitem);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Synonym
                            var entrySYN = zip.Entries.Where(s => s.FullName.StartsWith("PICK_LIST_MGR_COND_SYN") && Path.GetExtension(s.FullName).Equals(".txt")).FirstOrDefault();
                            {
                                if (entrySYN == null)
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("PICK_LIST_MGR_COND_SYN not found"));
                                }

                                using (var stream = entrySYN.Open())
                                {
                                    var ds = new DataSet();
                                    ds.ReadXml(stream);
                                    Console.WriteLine(ds.Tables[0].TableName);
                                    var synonim = ds.Tables[0].Columns.IndexOf("SYNONIM");
                                    var lits = ds.Tables[0].Columns.IndexOf("LITS_QUESTION_ID");
                                    var condition = ds.Tables[0].Columns.IndexOf("CONDITION");
                                    var status = ds.Tables[0].Columns.IndexOf("SYNONYM_STATUS");
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        var row = ds.Tables[0].Rows[i].ItemArray;
                                        var sTemp = row[lits].ToString();
                                        var sTemp2 = row[condition].ToString();
                                        var item = session.Repository.Query<Synonym>().Where(
                                            s => s.LitsQuestionId == sTemp
                                            && s.ConditionName == sTemp2).FirstOrDefault();
                                        if (item != null)
                                        {
                                            item.Name = row[synonim].ToString();
                                            item.Status = row[status].ToString();
                                            item.ModifiedBy = _user.Identity.GetUserId();
                                            item.ModifiedOn = DateTime.UtcNow;
                                            session.Repository.Update<Synonym>(item);
                                        }
                                        else
                                        {
                                            var cond = session.Repository.Query<Condition>().Where(
                                                s => s.LitsQuestionId == sTemp
                                                && s.Name.Equals(sTemp2)).FirstOrDefault();
                                            var cat = session.Repository.Query<Category>().Where(
                                                s => s.LitsQuestionId == sTemp
                                                ).FirstOrDefault();
                                            if (cond != null && cond.Id > 0 && cat != null && cat.Id > 0)
                                            {
                                                var newitem = new Synonym()
                                                {
                                                    Name = row[condition].ToString(),
                                                    Status = row[status].ToString(),
                                                    LitsQuestionId = row[lits].ToString(),
                                                    ConditionName = row[condition].ToString(),
                                                    ConditionId = cond.Id,
                                                    CategoryId = cat.Id,
                                                    CreatedBy = _user.Identity.GetUserId(),
                                                    CreatedOn = DateTime.UtcNow,
                                                    ModifiedBy = _user.Identity.GetUserId(),
                                                    ModifiedOn = DateTime.UtcNow,
                                                };
                                                session.Repository.Insert<Synonym>(newitem);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            });
        }


        public ResponseBase<List<SynonymDTO>> GetPickListItems(MessageBase<string> lits)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var categoryE = session.Repository.Query<Category>().Where(s => s.LitsQuestionId == lits.Data).FirstOrDefault();
                if (categoryE == null)
                {
                    return ResponseBase<List<SynonymDTO>>.CreateErrorResponse(new Exception("Category not found. " + lits.Data));
                }

                var entity = session.Repository.Query<Synonym>().Where(s => s.Status.Equals("Active") && s.CategoryId == categoryE.Id).OrderBy(s => s.Condition.Sorter)
                .Select(e => new SynonymDTO
                {
                    Id = e.Id,
                    Category = new CategoryDTO() { Id = e.Category.Id, AllowBlank = e.Category.AllowBlank, LitsQuestionId = e.Category.LitsQuestionId, Name = e.Category.Name },
                    CategoryId = e.Category.Id,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    Sorter = e.Condition.Sorter
                }
                ).ToList();
                if (categoryE.AllowBlank)
                {
                    entity.Insert(0, new SynonymDTO() { Id = 0, Sorter = 0, Name = "", LitsQuestionId = categoryE.LitsQuestionId, Category = new CategoryDTO() { Id = categoryE.Id, AllowBlank = categoryE.AllowBlank, LitsQuestionId = categoryE.LitsQuestionId, Name = categoryE.Name }, CategoryId = categoryE.Id });
                }
                return ResponseBase<List<SynonymDTO>>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase<IList<CountryDTO>> GetCountries()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var dto = session.Repository.Query<Country>().Select(e => new CountryDTO() { 
                    Id = e.Id,
                    Name = e.Name,
                    ShortCode = e.ShortCode,
                    LongCode = e.LongCode,
                }).ToList();
                // put US first
                var item = dto.Find(s => s.Name.Equals("United States"));
                if (item != null)
                {
                    dto.Remove(item);
                    dto.Insert(0, item);
                }
                // it is nullable
                dto.Insert(0, new CountryDTO());
                return ResponseBase<IList<CountryDTO>>.CreateSuccessResponse(dto);
            }
        }

        public ResponseBase<IList<StateDTO>> GetStates()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var dto = session.Repository.Query<State>().Select(e => new StateDTO()
                {
                    Id = e.Id,
                    Name = e.Name,
                    Abbreviation = e.Abbreviation,
                }).ToList();
                // it is nullable
                dto.Insert(0, new StateDTO());
                return ResponseBase<IList<StateDTO>>.CreateSuccessResponse(dto);
            }
        }

        public ResponseBase<IList<CountyDTO>> GetCounties(MessageBase<int> stateId)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var dto = session.Repository.Query<County>().Where(s => s.StateId == stateId.Data).Select(e => new CountyDTO()
                {
                    Id = e.Id,
                    Name = e.Name,
                }).ToList();
                // it is nullable
                dto.Insert(0, new CountyDTO() { Name = String.Empty});
                return ResponseBase<IList<CountyDTO>>.CreateSuccessResponse(dto);
            }
        }


        public ResponseBase<IList<NameValueDTO>> PackageCSIDList(MessageBase<int?> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Specimen>().Where(s => s.PackageId == id.Data)
                .Where(userFilter,  _validIds)
                .Select(e => new NameValueDTO() { Name = e.CSID, Value = e.CSID }
                ).ToList();
                entity.Insert(0, new NameValueDTO());
                return ResponseBase<IList<NameValueDTO>>.CreateSuccessResponse(entity);
            }    
        }

        public ResponseBase<IList<AliquotDTO>> AliquotList(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Aliquot>(s => s.IsolateId == id.Data, new List<string>() { "Isolate", "Isolate.Status", "Isolate.Specimen", "Isolate.Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new AliquotDTO
                {
                    
                    Id = e.Id,
                    IsolateId = e.IsolateId,
                    CUID = e.CUID,
                    TestCode = e.TestCode,
                    Isolate = new IsolateDTO() { 
                        StatusId = e.Isolate.StatusId,
                        Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                        Specimen = new SpecimenDTO() { SubType = e.Isolate.Specimen.SubType, CSID = e.Isolate.Specimen.CSID, Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName } }
                    },
                    ManifestId = e.ManifestId,
                    Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                    IsStoredLocally = e.IsStoredLocally,
                    TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                    StatusId = e.StatusId,
                    Status = e.StatusId.HasValue ?  new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                }
                ).ToList();
                return ResponseBase<IList<AliquotDTO>>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase AliquotInsert(MessageBase<AliquotDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (_lab == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Aliquots without a lab assigned."));
                }

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Isolate>(s => s.Id == dto.Data.IsolateId, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Isolate Id {0} not found.", dto.Data.IsolateId)));
                    }
                    else
                    {
                        if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                        //if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            {
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot add aliquots to a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                            }
                            else
                            {
                                var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                                if (notecategory != null)
                                {
                                    var note = new SpecimenNote()
                                    {
                                        CreatedBy = _user.Identity.GetUserId(),
                                        CreatedOn = DateTime.UtcNow,
                                        ModifiedBy = _user.Identity.GetUserId(),
                                        ModifiedOn = DateTime.UtcNow,
                                        NoteCategoryId = notecategory.Id,
                                        Note = string.Format(@"Specimen Edited With a Status of {0} (Aliquot Added)", entity.Status.DisplayName),
                                        SpecimenId = entity.SpecimenId,
                                    };
                                    session.Repository.Insert<SpecimenNote>(note);
                                }
                                else
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("Cannot add aliquot (missing note category)"));
                                }
                            }
                        }
                    }

                    if (dto.Data.IsStoredLocally && dto.Data.ManifestId.HasValue)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(@"Cannot assign an aliquot to a manifest if it is stored locally"));
                    }

                    var newentity = new Aliquot()
                    {
                        CUID = dto.Data.CUID,
                        TestCode = dto.Data.TestCode,
                        IsolateId = dto.Data.IsolateId,
                        ManifestId = dto.Data.ManifestId,
                        IsStoredLocally = dto.Data.IsStoredLocally,
                        StatusId = dto.Data.StatusId,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId()
                    };
                    session.Repository.Insert<Aliquot>(newentity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase AliquotDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Aliquots."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Aliquot>(s => s.Id == id.Data, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.Status" })
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        if (!entity.Isolate.Specimen.Status.SysName.Equals("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            { 
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot delete aliquots from a specimen with the status {0}", entity.Isolate.Specimen.Status.DisplayName)));
                            }
                        }
                        session.Repository.Delete<Aliquot>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<AliquotDTO> Aliquot(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Aliquot>(s => s.Id == id.Data, new List<string>() { "Isolate", "Isolate.Status", "Isolate.Specimen", "Isolate.Specimen.Status" })
                .Where(userFilter,  _validIds)
                .Select(e => new AliquotDTO
                {
                    
                    Id = e.Id,
                    CUID = e.CUID,
                    TestCode = e.TestCode,
                    IsolateId = e.IsolateId,
                    Isolate = new IsolateDTO() {
                        StatusId = e.Isolate.StatusId,
                        Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                        DateHarvested = e.Isolate.DateHarvested,
                        Specimen = new SpecimenDTO() {
                            SubType = e.Isolate.Specimen.SubType,
                            CSID = e.Isolate.Specimen.CSID,
                            Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },
                        SpecimenID = e.Isolate.Specimen.SpecimenID,
                        DateCollected = e.Isolate.Specimen.DateCollected,
                    } },

                    ManifestId = e.ManifestId,
                    Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                    IsStoredLocally = e.IsStoredLocally,
                    TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                    StatusId = e.StatusId,
                    Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,

                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<AliquotDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<AliquotDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase AliquotEdit(MessageBase<AliquotDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Aliquots."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Aliquot>(s => s.Id == dto.Data.Id, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.Status" })
                    .Where(userFilter,  _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                        if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS"))
                        {
                            if (!_user.User.IsInRole("Lab Editor"))
                            {
                                return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot edit aliquots from a specimen with the status {0}", entity.Isolate.Specimen.Status.DisplayName)));
                            }
                            else
                            {
                                var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                                if (notecategory != null)
                                {
                                    var note = new SpecimenNote()
                                    {
                                        CreatedBy = _user.Identity.GetUserId(),
                                        CreatedOn = DateTime.UtcNow,
                                        ModifiedBy = _user.Identity.GetUserId(),
                                        ModifiedOn = DateTime.UtcNow,
                                        NoteCategoryId = notecategory.Id,
                                        Note = string.Format(@"Specimen Edited With a Status of {0} (Aliquot Edited)", entity.Status.DisplayName),
                                        SpecimenId = entity.Isolate.SpecimenId,
                                    };
                                    session.Repository.Insert<SpecimenNote>(note);
                                }
                                else
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit aliquot (missing note category)"));
                                }
                            }
                        }

                        if (dto.Data.IsStoredLocally && dto.Data.ManifestId.HasValue)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(@"Cannot assign an aliquot to a manifest if it is stored locally"));
                        }

                        entity.CUID = dto.Data.CUID;
                        entity.TestCode = dto.Data.TestCode;

                        if (_lab != null)
                        {
                            entity.ManifestId = dto.Data.ManifestId;
                        }

                        entity.IsStoredLocally = dto.Data.IsStoredLocally;
                        entity.StatusId = dto.Data.StatusId;
                        
                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Aliquot>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }



        public ResponseBase DEIFileMergeClients(MessageBase<DEIFileDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    using (var file = new MemoryStream(dto.Data.Content))
                    {
                        using (var zip = new ZipArchive(file, ZipArchiveMode.Read))
                        {
                            #region RASCLIENTS
                            var entryClient = zip.Entries.Where(s => s.FullName.StartsWith("RASCLIENTS") && Path.GetExtension(s.FullName).Equals(".txt")).FirstOrDefault();
                            {
                                if (entryClient == null)
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("RASCLIENTS not found"));
                                }
                                using (var stream = entryClient.Open())
                                {
                                    var ds = new DataSet();
                                    ds.ReadXml(stream);
                                    Console.WriteLine(ds.Tables[0].TableName);
                                    var rasclientid = ds.Tables[0].Columns.IndexOf("RASCLIENTID");
                                    var address = ds.Tables[0].Columns.IndexOf("ADRESS");
                                    var city = ds.Tables[0].Columns.IndexOf("CITY");
                                    var companyname = ds.Tables[0].Columns.IndexOf("COMPNAME");
                                    var country = ds.Tables[0].Columns.IndexOf("COUNTRY");
                                    var state = ds.Tables[0].Columns.IndexOf("STATE");
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        var row = ds.Tables[0].Rows[i].ItemArray;
                                        var sTemp = row[rasclientid].ToString();
                                        var item = session.Repository.Query<Sender>().Where(s => s.RasClientId == sTemp).FirstOrDefault();
                                        if (item != null)
                                        {
                                            item.Address = row[address].ToString();
                                            item.City = row[city].ToString();
                                            item.CompanyName = row[companyname].ToString();
                                            item.Country = row[country].ToString();
                                            item.State = row[state].ToString();
                                            item.ModifiedBy = _user.Identity.GetUserId();
                                            item.ModifiedOn = DateTime.UtcNow;
                                            item.FluSeason = _currentFluSeason;
                                            session.Repository.Update<Sender>(item);
                                        }
                                        else
                                        {
                                            var newitem = new Sender()
                                            {
                                                RasClientId = row[rasclientid].ToString(),
                                                Address = row[address].ToString(),
                                                City = row[city].ToString(),
                                                CompanyName = row[companyname].ToString(),
                                                Country = row[country].ToString(),
                                                State = row[state].ToString(),
                                                CreatedBy = _user.Identity.GetUserId(),
                                                CreatedOn = DateTime.UtcNow,
                                                ModifiedBy = _user.Identity.GetUserId(),
                                                ModifiedOn = DateTime.UtcNow,
                                                FluSeason = _currentFluSeason,
                                            };
                                            session.Repository.Insert<Sender>(newitem);
                                        }
                                    }
                                }
                            }
                            #endregion
                            
                        }
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            });
        }

        public ResponseBase DEIFileMergeContacts(MessageBase<DEIFileDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    using (var file = new MemoryStream(dto.Data.Content))
                    {
                        using (var zip = new ZipArchive(file, ZipArchiveMode.Read))
                        {
                            #region RASCLIENTCONTACTSEXT
                            var entryClient = zip.Entries.Where(s => s.FullName.StartsWith("RASCLIENTCONTACTSEXT") && Path.GetExtension(s.FullName).Equals(".txt")).FirstOrDefault();
                            {
                                if (entryClient == null)
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("RASCLIENTCONTACTSEXT not found"));
                                }
                                using (var stream = entryClient.Open())
                                {
                                    var ds = new DataSet();
                                    ds.ReadXml(stream);
                                    Console.WriteLine(ds.Tables[0].TableName);
                                    var rasclientid = ds.Tables[0].Columns.IndexOf("RASCLIENTID");
                                    var rascontactid = ds.Tables[0].Columns.IndexOf("CONTACTID");
                                    var address = ds.Tables[0].Columns.IndexOf("ADRESS");
                                    var city = ds.Tables[0].Columns.IndexOf("CITY");
                                    var country = ds.Tables[0].Columns.IndexOf("COUNTRY");
                                    var state = ds.Tables[0].Columns.IndexOf("STATE");
                                    var phone = ds.Tables[0].Columns.IndexOf("CONTACTPHONE");
                                    var fax = ds.Tables[0].Columns.IndexOf("FAX");
                                    var email = ds.Tables[0].Columns.IndexOf("EMAIL");
                                    var defaultcontact = ds.Tables[0].Columns.IndexOf("DEFAULTCONTACT");
                                    var fullname = ds.Tables[0].Columns.IndexOf("FULLNAME");
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        var row = ds.Tables[0].Rows[i].ItemArray;
                                        var sTemp = row[rascontactid].ToString();
                                        if (row[defaultcontact].ToString().ToLower().Equals('y'))
                                        {
                                            continue;
                                        } 
                                        var item = session.Repository.Query<Contact>().Where(s => s.RasContactId == sTemp).FirstOrDefault();
                                        if (item != null)
                                        {
                                            item.RasClientId = row[rasclientid].ToString();
                                            item.RasContactId = row[rascontactid].ToString();
                                            item.Address = row[address].ToString();
                                            item.City = row[city].ToString();
                                            item.Country = row[country].ToString();
                                            item.State = row[state].ToString();
                                            item.Phone = row[phone].ToString();
                                            item.Fax = row[fax].ToString();
                                            item.Email = row[email].ToString();
                                            item.FullName = row[fullname].ToString();

                                            item.ModifiedBy = _user.Identity.GetUserId();
                                            item.ModifiedOn = DateTime.UtcNow;
                                            item.FluSeason = _currentFluSeason;
                                            session.Repository.Update<Contact>(item);
                                        }
                                        else
                                        {
                                            var newitem = new Contact()
                                            {
                                                RasContactId = row[rascontactid].ToString(),
                                                RasClientId = row[rasclientid].ToString(),
                                                Address = row[address].ToString(),
                                                City = row[city].ToString(),
                                                Country = row[country].ToString(),
                                                State = row[state].ToString(),
                                                Phone = row[phone].ToString(),
                                                Fax = row[fax].ToString(),
                                                Email = row[email].ToString(),
                                                FullName = row[fullname].ToString(),
                                                CreatedBy = _user.Identity.GetUserId(),
                                                CreatedOn = DateTime.UtcNow,
                                                ModifiedBy = _user.Identity.GetUserId(),
                                                ModifiedOn = DateTime.UtcNow,
                                                FluSeason = _currentFluSeason,
                                            };
                                            session.Repository.Insert<Contact>(newitem);
                                        }
                                    }
                                }
                            }
                            #endregion

                        }
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            });
        }


        public ResponseBase<IList<SenderDTO>> GetSenders(MessageBase<string> stateAbbrev)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var dto = new List<SenderDTO>();
                dto = session.Repository.Query<Sender>().Where(s => s.State.Equals(stateAbbrev.Data) && s.FluSeason == _currentFluSeason).Select(e => new SenderDTO()
                {
                    Id = e.Id,
                    Address = e.Address,
                    City = e.City,
                    CompanyName = e.CompanyName,
                    Country = e.Country,
                    RasClientId = e.RasClientId,
                    State = e.State,
                }).ToList();
                return ResponseBase<IList<SenderDTO>>.CreateSuccessResponse(dto);
            }           
        }



        public ResponseBase<IList<LabDTO>> GetLabs()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var dto = new List<LabDTO>();
                dto = session.Repository.Query<Lab>().Select(e => new LabDTO()
                {
                    Id = e.Id,
                    Name = e.Name,
                }).ToList();
                return ResponseBase<IList<LabDTO>>.CreateSuccessResponse(dto);
            }
        }


        public ResponseBase UserLabEdit(MessageBase<KeyValuePair<string, int?>> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    KeyValuePair<string, int?> kvp = dto.Data;
                    var entity = session.Repository.Query<AspNetUser>().Where(s => s.Id == kvp.Key).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.LabId = kvp.Value;
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            });
        }


        public ResponseBase UserDateDisabledEdit(MessageBase<KeyValuePair<string, DateTime?>> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    KeyValuePair<string, DateTime?> kvp = dto.Data;
                    var entity = session.Repository.Query<AspNetUser>().Where(s => s.Id == kvp.Key).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.DateDisabled = kvp.Value;
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            });
        }

        public ResponseBase<IList<FilterDTO>> FilterList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Filter>()
                .Select(e => new FilterDTO
                {
                    Id = e.Id,
                    Display = e.Display,
                    FilterText = e.FilterText,
                    BaseObject = e.BaseObject,
                    IsLabAvailable = e.IsLabAvailable,
                }
                )
                .ToList();
                return ResponseBase<IList<FilterDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase FilterInsert(MessageBase<FilterDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var newentity = new Filter()
                    {
                        Display = dto.Data.Display,
                        FilterText = dto.Data.FilterText,
                        BaseObject = dto.Data.BaseObject,
                        IsLabAvailable = dto.Data.IsLabAvailable,
                    };
                    session.Repository.Insert<Filter>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<FilterDTO> Filter(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Filter>().Where(s => s.Id == id.Data)
                .Select(e => new FilterDTO
                {
                    Id = e.Id,
                    Display = e.Display,
                    FilterText = e.FilterText,
                    BaseObject = e.BaseObject,
                    IsLabAvailable = e.IsLabAvailable,
                }
                )
                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<FilterDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<FilterDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase FilterEdit(MessageBase<FilterDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Filter>().Where(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity. Display = dto.Data.Display;
                        entity.FilterText = dto.Data.FilterText;
                        entity.BaseObject = dto.Data.BaseObject;
                        entity.IsLabAvailable = dto.Data.IsLabAvailable;
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase FilterDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Filter>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Filter>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        
        public ResponseBase<IList<FilterDTO>> FilterList(MessageBase<string> baseobject)
        {
            var labuser = _user.User.IsInRole("Lab");
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Filter>()
                .Where(s => s.BaseObject == baseobject.Data)
                .Where(s => s.IsLabAvailable || !labuser)
                .Select(e => new FilterDTO
                {
                    Id = e.Id,
                    Display = e.Display,
                    FilterText = e.FilterText,
                    BaseObject = e.BaseObject,
                    IsLabAvailable = e.IsLabAvailable,
                }
                )
                .ToList();
                entity.Insert(0, new FilterDTO());
                return ResponseBase<IList<FilterDTO>>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase<IList<NameValueDTO>> StatusList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Status>().OrderBy(s => s.Id)
                .Select(e => new NameValueDTO() { Name = e.DisplayName, Value = e.Id.ToString() }
                ).ToList();
                entity.Insert(0, new NameValueDTO());
                return ResponseBase<IList<NameValueDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase TemplateDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot delete Templates."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Template>(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Template>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase TemplateUpload(MessageBase<TemplateDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == dto.Data.TemplateType && s.LabId == dto.Data.LabId)
                    .FirstOrDefault();
                    if (entity != null)
                    {
                        session.Repository.Delete<Template>(entity);
                    }

                    var newentity = new Template()
                    {
                        FileName = dto.Data.FileName,
                        TemplateType = dto.Data.TemplateType,
                        File = dto.Data.File,
                        LabId = dto.Data.LabId > 0 ? dto.Data.LabId : null,
                    };
                    session.Repository.Insert<Template>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<TemplateDTO>> TemplateList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Template>().OrderBy(s => s.Id)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, LabId = e.LabId, Lab =  e.LabId != null ? new LabDTO() { Id = e.Lab.Id, Name = e.Lab.Name } : null }
                ).ToList();
                return ResponseBase<IList<TemplateDTO>>.CreateSuccessResponse(entity);
            }
        }


        private List<AliquotDTO> GenerateSpecimenFile_Internal(List<int> dto, string view)
        {
            // * CLAW 80
            var pagesize = 80;
            var pages = dto.Count / pagesize;
            if (dto.Count % pagesize > 0 || pages == 0)
            {
                pages++;
            }
            var localAliquotList = new List<AliquotDTO>();

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {

                var viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                if (view.Equals("Manifests"))
                {
                    viewwhere = "@0.Contains(outerIt.ManifestId.Value)";
                }
                else if (view.Equals("Isolates"))
                {
                    viewwhere = "@0.Contains(outerIt.Isolate.Id)";
                }
                else if (view.Equals("Aliquots"))
                {
                    viewwhere = "@0.Contains(outerIt.Id)";
                }

                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Aliquot>()
                        .Where(s => s.TestCode == "RCV")
                        .Select(e => new AliquotDTO()
                        {
                            VersionColumn = e.VersionColumn,
                            StatusId = e.StatusId,
                            Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                            Id = e.Id,
                            IsolateId = e.IsolateId,
                            CUID = e.CUID,
                            TestCode = e.TestCode,
                            CreatedBy = e.CreatedBy,
                            ManifestId = e.ManifestId,
                            Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                            IsStoredLocally = e.IsStoredLocally,
                            TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                            Isolate = new IsolateDTO()
                            {
                                VersionColumn = e.Isolate.VersionColumn,
                                StatusId = e.Isolate.StatusId,
                                Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                                Id = e.Isolate.Id,
                                PassageLevel = e.Isolate.PassageLevel,
                                DateHarvested = e.Isolate.DateHarvested,
                                Titer = e.Isolate.Titer,
                                RedBloodCellType = e.Isolate.RedBloodCellType,
                                DateInoculated = e.Isolate.DateInoculated,
                                CreatedOn = e.Isolate.CreatedOn,
                                CreatedBy = e.Isolate.CreatedBy,
                                ModifiedBy = e.Isolate.ModifiedBy,
                                ModifiedOn = e.Isolate.ModifiedOn,
                                SpecimenId = e.Isolate.SpecimenId,
                                Comments = e.Isolate.Comments,
                                IsolateType = new IsolateTypeDTO()
                                {
                                    Id = e.Isolate.IsolateType.Id,
                                    DisplayName = e.Isolate.IsolateType.DisplayName,
                                    SysName = e.Isolate.IsolateType.SysName,
                                    IsVisible = e.Isolate.IsolateType.IsVisible,
                                },
                                Specimen = new SpecimenDTO()
                                {
                                    VersionColumn = e.Isolate.Specimen.VersionColumn,
                                    FluSeason = e.Isolate.Specimen.FluSeason,
                                    Id = e.Isolate.Specimen.Id,
                                    DateInoculated = e.Isolate.Specimen.DateInoculated,
                                    SampleClass = e.Isolate.Specimen.SampleClass,
                                    CSID = e.Isolate.Specimen.CSID,
                                    //CUID = e.Isolate.Specimen.CUID,
                                    CreatedOn = e.Isolate.Specimen.CreatedOn,
                                    CreatedBy = e.Isolate.Specimen.CreatedBy,
                                    ModifiedBy = e.Isolate.Specimen.ModifiedBy,
                                    ModifiedOn = e.Isolate.Specimen.ModifiedOn,
                                    ProjectId = e.Isolate.Specimen.ProjectId,
                                    Package = new PackageDTO()
                                    {
                                        Id = e.Isolate.Specimen.Package.Id,
                                        Name = e.Isolate.Specimen.Package.Name,
                                        SenderId = e.Isolate.Specimen.Package.SenderId,
                                        DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                        SpecimenCondition = e.Isolate.Specimen.Package.SpecimenCondition,
                                        Sender = new SenderDTO()
                                        {
                                            Id = e.Isolate.Specimen.Package.Sender.Id,
                                            RasClientId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                            CompanyName = e.Isolate.Specimen.Package.Sender.CompanyName,
                                            Address = e.Isolate.Specimen.Package.Sender.Address,
                                            City = e.Isolate.Specimen.Package.Sender.City,
                                            State = e.Isolate.Specimen.Package.Sender.State,
                                        }
                                    },
                                    PackageId = e.Isolate.Specimen.PackageId,
                                    Country = new CountryDTO() { Id = e.Isolate.Specimen.Country.Id, Name = e.Isolate.Specimen.Country.Name, ShortCode = e.Isolate.Specimen.Country.ShortCode, LongCode = e.Isolate.Specimen.Country.LongCode },
                                    CountryId = e.Isolate.Specimen.CountryId,
                                    State = new StateDTO() { Id = e.Isolate.Specimen.State.Id, Name = e.Isolate.Specimen.State.Name, Abbreviation = e.Isolate.Specimen.State.Abbreviation },
                                    StateId = e.Isolate.Specimen.StateId,
                                    County = new CountyDTO() { Id = e.Isolate.Specimen.County.Id, Name = e.Isolate.Specimen.County.Name },
                                    CountyId = e.Isolate.Specimen.CountyId,
                                    Activity = e.Isolate.Specimen.Activity,
                                    DateCollected = e.Isolate.Specimen.DateCollected,
                                    SpecimenID = e.Isolate.Specimen.SpecimenID,
                                    PatientAge = e.Isolate.Specimen.PatientAge,
                                    PatientAgeUnits = e.Isolate.Specimen.PatientAgeUnits,
                                    PatientSex = e.Isolate.Specimen.PatientSex,
                                    PassageHistory = e.Isolate.Specimen.PassageHistory,
                                    SubType = e.Isolate.Specimen.SubType,
                                    SpecimenSource = e.Isolate.Specimen.SpecimenSource,
                                    InitialStorage = e.Isolate.Specimen.InitialStorage,
                                    CompCSID1 = e.Isolate.Specimen.CompCSID1,
                                    CompCSID2 = e.Isolate.Specimen.CompCSID2,
                                    OtherLocation = e.Isolate.Specimen.OtherLocation,
                                    DrugResistant = e.Isolate.Specimen.DrugResistant,
                                    Deceased = e.Isolate.Specimen.Deceased,
                                    SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                    NursingHome = e.Isolate.Specimen.NursingHome,
                                    Vaccinated = e.Isolate.Specimen.Vaccinated,
                                    Travel = e.Isolate.Specimen.Travel,
                                    ReasonForSubmission = e.Isolate.Specimen.ReasonForSubmission,
                                    Comments = e.Isolate.Specimen.Comments,
                                    StatusId = e.Isolate.Specimen.StatusId,
                                    ContractLabFrom = e.Isolate.Specimen.ContractLabFrom,

                                    TravelCountry = e.Isolate.Specimen.TravelCountry,
                                    TravelState = e.Isolate.Specimen.TravelState,
                                    TravelCounty = e.Isolate.Specimen.TravelCounty,
                                    TravelCity = e.Isolate.Specimen.TravelCity,
                                    TravelReturnDate = e.Isolate.Specimen.TravelReturnDate,
                                    HostCellLine = e.Isolate.Specimen.HostCellLine,
                                    Substrate = e.Isolate.Specimen.Substrate,
                                    PassageNumber = e.Isolate.Specimen.PassageNumber,
                                    PCRInfA = e.Isolate.Specimen.PCRInfA,
                                    PCRpdmInfA = e.Isolate.Specimen.PCRpdmInfA,
                                    PCRpdmH1 = e.Isolate.Specimen.PCRpdmH1,
                                    PCRH3 = e.Isolate.Specimen.PCRH3,
                                    PCRInfB = e.Isolate.Specimen.PCRInfB,
                                    PCRBVic = e.Isolate.Specimen.PCRBVic,
                                    PCRBYam = e.Isolate.Specimen.PCRBYam,
                                    City = e.Isolate.Specimen.City,
                                    WHOSubmittedSubtype = e.Isolate.Specimen.WHOSubmittedSubtype,
                                    Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },
                                }
                            }
                        })
                        .Where(userFilter, _validIds)
                        //.Where(viewwhere, dto.Data);
                        .Where(viewwhere, localIdList);
                    localAliquotList.AddRange(query.ToList());
                }
                //var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, query.ToList(), 6);
                return localAliquotList;
            } 
        }

        public ResponseBase<TemplateDTO> GenerateSpecimenFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            TemplateDTO entity = null;
            List<AliquotDTO> localAliquotList = null;
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Specimen")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Specimen" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        //return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Specimen template not found.")));
                        new Exception(string.Format(@"Specimen template not found."));
                    }
                }
            }
            try
            {
                localAliquotList = GenerateSpecimenFile_Internal(dto.Data, view.Data);
            }
            catch (Exception ex)
            {
                return ResponseBase<TemplateDTO>.CreateErrorResponse(ex);
            }

            // * CLAW - 53
            if (localAliquotList.Count == 0)
            {
                return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to export"));
            }

            var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, localAliquotList, 6);
            return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
        }

        public ResponseBase GenerateSpecimenTransmissionRows(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            TemplateDTO entity = null;
            List<AliquotDTO> localAliquotList = null;
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var submitted_status = session.Repository.FindBy<Status>(s => s.SysName.Equals("SUBMITTED"), null);
                if (submitted_status == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "SUBMITTED")));
                }

                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Specimen")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Specimen" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        //return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Specimen template not found.")));
                        new Exception(string.Format(@"Specimen template not found."));
                    }
                }

                try
                {
                    localAliquotList = GenerateSpecimenFile_Internal(dto.Data, view.Data);

                    // * CLAW - 53
                    if (localAliquotList.Count == 0)
                    {
                        return ResponseBase<Labels>.CreateErrorResponse(new Exception("No samples to transmit"));
                    }

                }
                catch (Exception ex)
                {
                    return ResponseBase.CreateErrorResponse(ex);
                }
                //var outputfile = ExcelFileFactory.GenerateJsonFile<AliquotDTO>(entity, localAliquotList);
                //var RSA = new RSACryptoServiceProvider();
                //RSA.FromXmlString(<Get key from certificate>);
                //var RSAFormatter = new RSAPKCS1SignatureFormatter(RSA);
                //RSAFormatter.SetHashAlgorithm("SHA1");
                //var SHhash = new SHA1Managed();
                var isolateids = new List<int>();
                var specimenids = new List<int>();
                var serializer = new JsonFx.Json.JsonWriter();
                foreach (var item in localAliquotList)
                {

                    if (item.Id.Equals(int.MaxValue))
                    {
                        item.Status = null;
                        item.StatusId = null;
                        item.VersionColumn = null;
                    }

                    var rv2 = item.Isolate.VersionColumn;
                    var rv3 = Encoding.ASCII.GetBytes(new string('0', 8));
                    if (item.VersionColumn != null)
                    {
                        rv3 = item.VersionColumn;
                    }
                    var keyrv = item.Isolate.Specimen.VersionColumn.Concat(rv2).Concat(rv3).ToArray();

                    var expitem = ExcelFileFactory.GenerateObject<AliquotDTO>(entity, item);
                    (expitem as IDictionary<string, Object>).Add("Key", string.Format(@"{0},{1},{2}", item.Isolate.Specimen.CSID, item.Isolate.IsolateType.SysName, item.CUID));
                    (expitem as IDictionary<string, Object>).Add("KeyRowVersion", Encoding.UTF8.GetString(keyrv));

                    var payload = serializer.Write(expitem);

                    //byte[] SignedHashValue = RSAFormatter.CreateSignature(SHhash.ComputeHash(new UnicodeEncoding().GetBytes(payload)));
                    var newentity = new Transmission()
                    {
                        AliquotId = item.Id,
                        IsolateId = item.IsolateId,
                        SpecimenId = item.Isolate.SpecimenId,
                        AliquotRowVersion = item.VersionColumn,
                        IsolateRowVersion = item.Isolate.VersionColumn,
                        SpecimenRowVersion = item.Isolate.Specimen.VersionColumn,
                        Payload = payload,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId()
                    };
                    session.Repository.Insert<Transmission>(newentity);
                    if (newentity.Id > 0)
                    {
                        if (newentity.AliquotId.HasValue)
                        {
                            var temp = session.Repository.Find<Aliquot>(newentity.AliquotId.Value);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Aliquot>(temp);
                            }
                        }
                        if (newentity.IsolateId.HasValue && !isolateids.Contains(newentity.IsolateId.Value))
                        {
                            isolateids.Add(newentity.IsolateId.Value);
                            var temp = session.Repository.Find<Isolate>(newentity.IsolateId.Value);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Isolate>(temp);
                            }
                        }
                        if (!specimenids.Contains(newentity.SpecimenId))
                        {
                            specimenids.Add(newentity.SpecimenId);
                            var temp = session.Repository.Find<Specimen>(newentity.SpecimenId);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Specimen>(temp);
                            }
                        }
                    }

                }
                return ResponseBase.CreateSuccessResponse();
            }


        }

        private List<AliquotDTO> GenerateAliquotFile_Internal(List<int> dto, string view)
        {
            // * CLAW 80
            var pagesize = 80;
            var pages = dto.Count / pagesize;
            if (dto.Count % pagesize > 0 || pages == 0)
            {
                pages++;
            }
            var localAliquotList = new List<AliquotDTO>();

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                if (view.Equals("Manifests"))
                {
                    viewwhere = "@0.Contains(outerIt.ManifestId.Value)";
                }
                else if (view.Equals("Isolates"))
                {
                    viewwhere = "@0.Contains(outerIt.Isolate.Id)";
                }
                else if (view.Equals("Aliquots"))
                {
                    viewwhere = "@0.Contains(outerIt.Id)";
                }

                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Aliquot>()
                                    .Where(s => s.TestCode != "RCV")
                                    .Select(e => new AliquotDTO()
                                    {
                                        VersionColumn = e.VersionColumn,
                                        StatusId = e.StatusId,
                                        Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                                        Id = e.Id,
                                        IsolateId = e.IsolateId,
                                        CUID = e.CUID,
                                        TestCode = e.TestCode,
                                        CreatedBy = e.CreatedBy,
                                        ManifestId = e.ManifestId,
                                        Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                                        IsStoredLocally = e.IsStoredLocally,
                                        TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                                        Isolate = new IsolateDTO()
                                        {
                                            VersionColumn = e.Isolate.VersionColumn,
                                            StatusId = e.Isolate.StatusId,
                                            Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                                            Id = e.Isolate.Id,
                                            PassageLevel = e.Isolate.PassageLevel,
                                            DateHarvested = e.Isolate.DateHarvested,
                                            Titer = e.Isolate.Titer,
                                            RedBloodCellType = e.Isolate.RedBloodCellType,
                                            DateInoculated = e.Isolate.DateInoculated,
                                            CreatedOn = e.Isolate.CreatedOn,
                                            CreatedBy = e.Isolate.CreatedBy,
                                            ModifiedBy = e.Isolate.ModifiedBy,
                                            ModifiedOn = e.Isolate.ModifiedOn,
                                            SpecimenId = e.Isolate.SpecimenId,
                                            Comments = e.Isolate.Comments,
                                            IsolateType = new IsolateTypeDTO()
                                            {
                                                Id = e.Isolate.IsolateType.Id,
                                                DisplayName = e.Isolate.IsolateType.DisplayName,
                                                SysName = e.Isolate.IsolateType.SysName,
                                                IsVisible = e.Isolate.IsolateType.IsVisible,
                                            },
                                            Specimen = new SpecimenDTO()
                                            {
                                                VersionColumn = e.Isolate.Specimen.VersionColumn,
                                                FluSeason = e.Isolate.Specimen.FluSeason,
                                                Id = e.Isolate.Specimen.Id,
                                                DateInoculated = e.Isolate.Specimen.DateInoculated,
                                                SampleClass = e.Isolate.Specimen.SampleClass,
                                                CSID = e.Isolate.Specimen.CSID,
                                                CUID = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Where(s => s.TestCode.Equals("RCV")).OrderBy(s => s.Id).FirstOrDefault().CUID,
                                                CreatedOn = e.Isolate.Specimen.CreatedOn,
                                                CreatedBy = e.Isolate.Specimen.CreatedBy,
                                                ModifiedBy = e.Isolate.Specimen.ModifiedBy,
                                                ModifiedOn = e.Isolate.Specimen.ModifiedOn,
                                                ProjectId = e.Isolate.Specimen.ProjectId,
                                                Package = new PackageDTO()
                                                {
                                                    Id = e.Isolate.Specimen.Package.Id,
                                                    Name = e.Isolate.Specimen.Package.Name,
                                                    SenderId = e.Isolate.Specimen.Package.SenderId,
                                                    DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                                    SpecimenCondition = e.Isolate.Specimen.Package.SpecimenCondition,
                                                    Sender = new SenderDTO()
                                                    {
                                                        Id = e.Isolate.Specimen.Package.Sender.Id,
                                                        RasClientId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                                        CompanyName = e.Isolate.Specimen.Package.Sender.CompanyName,
                                                        Address = e.Isolate.Specimen.Package.Sender.Address,
                                                        City = e.Isolate.Specimen.Package.Sender.City,
                                                        State = e.Isolate.Specimen.Package.Sender.State,
                                                    }
                                                },
                                                PackageId = e.Isolate.Specimen.PackageId,
                                                Country = new CountryDTO() { Id = e.Isolate.Specimen.Country.Id, Name = e.Isolate.Specimen.Country.Name, ShortCode = e.Isolate.Specimen.Country.ShortCode, LongCode = e.Isolate.Specimen.Country.LongCode },
                                                CountryId = e.Isolate.Specimen.CountryId,
                                                State = new StateDTO() { Id = e.Isolate.Specimen.State.Id, Name = e.Isolate.Specimen.State.Name, Abbreviation = e.Isolate.Specimen.State.Abbreviation },
                                                StateId = e.Isolate.Specimen.StateId,
                                                County = new CountyDTO() { Id = e.Isolate.Specimen.County.Id, Name = e.Isolate.Specimen.County.Name },
                                                CountyId = e.Isolate.Specimen.CountyId,
                                                Activity = e.Isolate.Specimen.Activity,
                                                DateCollected = e.Isolate.Specimen.DateCollected,
                                                SpecimenID = e.Isolate.Specimen.SpecimenID,
                                                PatientAge = e.Isolate.Specimen.PatientAge,
                                                PatientAgeUnits = e.Isolate.Specimen.PatientAgeUnits,
                                                PatientSex = e.Isolate.Specimen.PatientSex,
                                                PassageHistory = e.Isolate.Specimen.PassageHistory,
                                                SubType = e.Isolate.Specimen.SubType,
                                                SpecimenSource = e.Isolate.Specimen.SpecimenSource,
                                                InitialStorage = e.Isolate.Specimen.InitialStorage,
                                                CompCSID1 = e.Isolate.Specimen.CompCSID1,
                                                CompCSID2 = e.Isolate.Specimen.CompCSID2,
                                                OtherLocation = e.Isolate.Specimen.OtherLocation,
                                                DrugResistant = e.Isolate.Specimen.DrugResistant,
                                                Deceased = e.Isolate.Specimen.Deceased,
                                                SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                                NursingHome = e.Isolate.Specimen.NursingHome,
                                                Vaccinated = e.Isolate.Specimen.Vaccinated,
                                                Travel = e.Isolate.Specimen.Travel,
                                                ReasonForSubmission = e.Isolate.Specimen.ReasonForSubmission,
                                                Comments = e.Isolate.Specimen.Comments,
                                                StatusId = e.Isolate.Specimen.StatusId,
                                                Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },

                                                TravelCountry = e.Isolate.Specimen.TravelCountry,
                                                TravelState = e.Isolate.Specimen.TravelState,
                                                TravelCounty = e.Isolate.Specimen.TravelCounty,
                                                TravelCity = e.Isolate.Specimen.TravelCity,
                                                TravelReturnDate = e.Isolate.Specimen.TravelReturnDate,
                                                HostCellLine = e.Isolate.Specimen.HostCellLine,
                                                Substrate = e.Isolate.Specimen.Substrate,
                                                PassageNumber = e.Isolate.Specimen.PassageNumber,
                                                PCRInfA = e.Isolate.Specimen.PCRInfA,
                                                PCRpdmInfA = e.Isolate.Specimen.PCRpdmInfA,
                                                PCRpdmH1 = e.Isolate.Specimen.PCRpdmH1,
                                                PCRH3 = e.Isolate.Specimen.PCRH3,
                                                PCRInfB = e.Isolate.Specimen.PCRInfB,
                                                PCRBVic = e.Isolate.Specimen.PCRBVic,
                                                PCRBYam = e.Isolate.Specimen.PCRBYam,
                                                City = e.Isolate.Specimen.City,
                                                WHOSubmittedSubtype = e.Isolate.Specimen.WHOSubmittedSubtype,
                                            }
                                        }
                                    })
                                    .Where(userFilter, _validIds)
                        //.Where(viewwhere, dto.Data);   
                                    .Where(viewwhere, localIdList);
                    // * VNR
                    if (view.Equals("Isolates"))
                    {
                        viewwhere = "@0.Contains(outerIt.Isolate.Id)";
                    }
                    else
                    {
                        viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                    }
                    if (view.Equals("Manifests"))
                    {
                        // not possible for VNR
                        viewwhere = @"Id < 0";
                    }
                    var vnrQuery = session.Repository.Query<Isolate>()
                                        .Where(s => s.IsolateType.SysName == "VNR" || s.IsolateType.SysName == "ITT")
                                        .Select(e => new AliquotDTO()
                                        {
                                            // * ugh
                                            VersionColumn = null,
                                            StatusId = null,
                                            Status = new StatusDTO() { Id = int.MaxValue, DisplayName = string.Empty, SysName = string.Empty },
                                            Id = int.MaxValue,
                                            IsolateId = e.Id,
                                            CUID = "",
                                            TestCode = "",
                                            CreatedBy = e.CreatedBy,
                                            ManifestId = null,
                                            Manifest = new ManifestDTO() { Id = 0, Name = "", CreatedOn = e.CreatedOn },
                                            // IsStoredLocally use default
                                            IsStoredLocally = false,
                                            TubeCount = e.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                                            Isolate = new IsolateDTO()
                                            {
                                                VersionColumn = e.VersionColumn,
                                                StatusId = e.StatusId,
                                                Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                                                Id = e.Id,
                                                PassageLevel = e.PassageLevel,
                                                DateHarvested = e.DateHarvested,
                                                Titer = e.Titer,
                                                RedBloodCellType = e.RedBloodCellType,
                                                DateInoculated = e.DateInoculated,
                                                CreatedOn = e.CreatedOn,
                                                CreatedBy = e.CreatedBy,
                                                ModifiedBy = e.ModifiedBy,
                                                ModifiedOn = e.ModifiedOn,
                                                SpecimenId = e.SpecimenId,
                                                Comments = e.Comments,
                                                IsolateType = new IsolateTypeDTO()
                                                {
                                                    Id = e.IsolateType.Id,
                                                    DisplayName = e.IsolateType.DisplayName,
                                                    SysName = e.IsolateType.SysName,
                                                    IsVisible = e.IsolateType.IsVisible,
                                                },
                                                Specimen = new SpecimenDTO()
                                                {
                                                    VersionColumn = e.Specimen.VersionColumn,
                                                    FluSeason = e.Specimen.FluSeason,
                                                    Id = e.Specimen.Id,
                                                    DateInoculated = e.Specimen.DateInoculated,
                                                    SampleClass = e.Specimen.SampleClass,
                                                    CSID = e.Specimen.CSID,
                                                    CUID = e.Specimen.Isolates.SelectMany(s => s.Aliquots).Where(s => s.TestCode.Equals("RCV")).OrderBy(s => s.Id).FirstOrDefault().CUID,
                                                    CreatedOn = e.Specimen.CreatedOn,
                                                    CreatedBy = e.Specimen.CreatedBy,
                                                    ModifiedBy = e.Specimen.ModifiedBy,
                                                    ModifiedOn = e.Specimen.ModifiedOn,
                                                    ProjectId = e.Specimen.ProjectId,
                                                    Package = new PackageDTO()
                                                    {
                                                        Id = e.Specimen.Package.Id,
                                                        Name = e.Specimen.Package.Name,
                                                        SenderId = e.Specimen.Package.SenderId,
                                                        DateReceived = e.Specimen.Package.DateReceived,
                                                        SpecimenCondition = e.Specimen.Package.SpecimenCondition,
                                                        Sender = new SenderDTO()
                                                        {
                                                            Id = e.Specimen.Package.Sender.Id,
                                                            RasClientId = e.Specimen.Package.Sender.RasClientId,
                                                            CompanyName = e.Specimen.Package.Sender.CompanyName,
                                                            Address = e.Specimen.Package.Sender.Address,
                                                            City = e.Specimen.Package.Sender.City,
                                                            State = e.Specimen.Package.Sender.State,
                                                        }
                                                    },
                                                    PackageId = e.Specimen.PackageId,
                                                    Country = new CountryDTO() { Id = e.Specimen.Country.Id, Name = e.Specimen.Country.Name, ShortCode = e.Specimen.Country.ShortCode, LongCode = e.Specimen.Country.LongCode },
                                                    CountryId = e.Specimen.CountryId,
                                                    State = new StateDTO() { Id = e.Specimen.State.Id, Name = e.Specimen.State.Name, Abbreviation = e.Specimen.State.Abbreviation },
                                                    StateId = e.Specimen.StateId,
                                                    County = new CountyDTO() { Id = e.Specimen.County.Id, Name = e.Specimen.County.Name },
                                                    CountyId = e.Specimen.CountyId,
                                                    Activity = e.Specimen.Activity,
                                                    DateCollected = e.Specimen.DateCollected,
                                                    SpecimenID = e.Specimen.SpecimenID,
                                                    PatientAge = e.Specimen.PatientAge,
                                                    PatientAgeUnits = e.Specimen.PatientAgeUnits,
                                                    PatientSex = e.Specimen.PatientSex,
                                                    PassageHistory = e.Specimen.PassageHistory,
                                                    SubType = e.Specimen.SubType,
                                                    SpecimenSource = e.Specimen.SpecimenSource,
                                                    InitialStorage = e.Specimen.InitialStorage,
                                                    CompCSID1 = e.Specimen.CompCSID1,
                                                    CompCSID2 = e.Specimen.CompCSID2,
                                                    OtherLocation = e.Specimen.OtherLocation,
                                                    DrugResistant = e.Specimen.DrugResistant,
                                                    Deceased = e.Specimen.Deceased,
                                                    SpecialStudy = e.Specimen.SpecialStudy,
                                                    NursingHome = e.Specimen.NursingHome,
                                                    Vaccinated = e.Specimen.Vaccinated,
                                                    Travel = e.Specimen.Travel,
                                                    ReasonForSubmission = e.Specimen.ReasonForSubmission,
                                                    Comments = e.Specimen.Comments,
                                                    StatusId = e.Specimen.StatusId,
                                                    Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName },

                                                    TravelCountry = e.Specimen.TravelCountry,
                                                    TravelState = e.Specimen.TravelState,
                                                    TravelCounty = e.Specimen.TravelCounty,
                                                    TravelCity = e.Specimen.TravelCity,
                                                    TravelReturnDate = e.Specimen.TravelReturnDate,
                                                    HostCellLine = e.Specimen.HostCellLine,
                                                    Substrate = e.Specimen.Substrate,
                                                    PassageNumber = e.Specimen.PassageNumber,
                                                    PCRInfA = e.Specimen.PCRInfA,
                                                    PCRpdmInfA = e.Specimen.PCRpdmInfA,
                                                    PCRpdmH1 = e.Specimen.PCRpdmH1,
                                                    PCRH3 = e.Specimen.PCRH3,
                                                    PCRInfB = e.Specimen.PCRInfB,
                                                    PCRBVic = e.Specimen.PCRBVic,
                                                    PCRBYam = e.Specimen.PCRBYam,
                                                    City = e.Specimen.City,
                                                    WHOSubmittedSubtype = e.Specimen.WHOSubmittedSubtype,
                                                }
                                            }
                                        })
                                        .Where(userFilter, _validIds)
                        //.Where(viewwhere, dto.Data);
                                        .Where(viewwhere, localIdList);
                    localAliquotList.AddRange(query.Union(vnrQuery).ToList());
                }
                return localAliquotList;
            }
        }

        public ResponseBase<TemplateDTO> GenerateAliquotFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {


                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Aliquot")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Aliquot" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Aliquot template not found.")));
                    }
                }


                var localAliquotList = GenerateAliquotFile_Internal(dto.Data, view.Data);

                // * CLAW - 53
                if (localAliquotList.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to export"));
                }

                //var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, query.Union(vnrQuery).ToList(), 6);
                var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, localAliquotList, 6);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }

        public ResponseBase GenerateAliquotTransmissionRows(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var submitted_status = session.Repository.FindBy<Status>(s => s.SysName.Equals("SUBMITTED"), null);
                if (submitted_status == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "SUBMITTED")));
                }

                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<AliquotDTO>();

                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Aliquot")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Aliquot" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Aliquot template not found.")));
                    }
                }

                try
                {
                    localAliquotList = GenerateAliquotFile_Internal(dto.Data, view.Data);

                    // * CLAW - 53
                    if (localAliquotList.Count == 0)
                    {
                        return ResponseBase<Labels>.CreateErrorResponse(new Exception("No samples to transmit"));
                    }
                    
                }
                catch (Exception ex)
                {
                    return ResponseBase.CreateErrorResponse(ex);
                }

                //var outputfile = ExcelFileFactory.GenerateJsonFile<AliquotDTO>(entity, localAliquotList);
                //var RSA = new RSACryptoServiceProvider();
                //RSA.FromXmlString(<Get key from certificate>);
                //var RSAFormatter = new RSAPKCS1SignatureFormatter(RSA);
                //RSAFormatter.SetHashAlgorithm("SHA1");
                //var SHhash = new SHA1Managed();
                var isolateids = new List<int>();
                var specimenids = new List<int>();
                var serializer = new JsonFx.Json.JsonWriter();
                foreach (var item in localAliquotList)
                {
                    if (item.Id.Equals(int.MaxValue))
                    {
                        item.Status = null;
                        item.StatusId = null;
                        item.VersionColumn = null;
                    }

                    var rv2 = item.Isolate.VersionColumn;
                    var rv3 = Encoding.ASCII.GetBytes(new string('0', 8));
                    if (item.VersionColumn != null)
                    {
                        rv3 = item.VersionColumn;
                    }
                    var keyrv = item.Isolate.Specimen.VersionColumn.Concat(rv2).Concat(rv3).ToArray();

                    var expitem = ExcelFileFactory.GenerateObject<AliquotDTO>(entity, item);
                    (expitem as IDictionary<string, Object>).Add("Key", string.Format(@"{0},{1},{2}", item.Isolate.Specimen.CSID, item.Isolate.IsolateType.SysName, item.CUID));
                    (expitem as IDictionary<string, Object>).Add("KeyRowVersion", Encoding.UTF8.GetString(keyrv));

                    var payload = serializer.Write(expitem);
                    //byte[] SignedHashValue = RSAFormatter.CreateSignature(SHhash.ComputeHash(new UnicodeEncoding().GetBytes(payload)));
                    var newentity = new Transmission()
                    {
                        IsolateId = item.IsolateId,
                        SpecimenId = item.Isolate.SpecimenId,
                        AliquotRowVersion = item.VersionColumn,
                        IsolateRowVersion = item.Isolate.VersionColumn,
                        SpecimenRowVersion = item.Isolate.Specimen.VersionColumn,
                        Payload = payload,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId()
                    };

                    if (item.Id < int.MaxValue)
	                {
		                newentity.AliquotId = item.Id;
	                }

                    session.Repository.Insert<Transmission>(newentity);

                    if (newentity.Id > 0)
                    {
                        if (newentity.AliquotId.HasValue)
                        {
                            var temp = session.Repository.Find<Aliquot>(newentity.AliquotId.Value);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Aliquot>(temp);
                            }
                        }
                        if (newentity.IsolateId.HasValue && !isolateids.Contains(newentity.IsolateId.Value))
                        {
                            isolateids.Add(newentity.IsolateId.Value);
                            var temp = session.Repository.Find<Isolate>(newentity.IsolateId.Value);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Isolate>(temp);
                            }
                        }
                        if (!specimenids.Contains(newentity.SpecimenId))
                        {
                            specimenids.Add(newentity.SpecimenId);
                            var temp = session.Repository.Find<Specimen>(newentity.SpecimenId);
                            if (temp != null)
                            {
                                temp.StatusId = submitted_status.Id;
                                session.Repository.Update<Specimen>(temp);
                            }
                        }
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            }
        }

        public ResponseBase<string> GenerateLabels_Page(MessageBase<List<int>> dto, MessageBase<string> baseobject, MessageBase<int> qty)
        {
            var temp = GenerateLabels(dto, baseobject, qty);
            if (!temp.Success)
            {
                return ResponseBase<string>.CreateErrorResponse(temp.Error);
            }
            var sb = new StringBuilder();
            if (temp.Data != null)
            {
                var labels = temp.Data;
                
                if (labels.data.Count > 0)
                {
                    sb.Append(labels.preamble);
                    foreach (var item in labels.data)
                    {
                        sb.Append(String.Format(labels.format
                            , item.CUIDBC
                            , item.SUB
                            , item.SID
                            , item.CSID
                            , item.DH
                            , item.PH
                            , item.FT
                            , item.HA
                            , item.ST
                            , item.CUIDHR
                            ));
                    }
                    sb.Append(labels.postamble);
                }
            }
            return ResponseBase<string>.CreateSuccessResponse(sb.ToString());
        }

        public ResponseBase<Labels> GenerateLabels(MessageBase<List<int>> dto, MessageBase<string> baseobject, MessageBase<int> qty)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                IQueryable<Aliquot> query;
                switch (baseobject.Data)
                {
                    case "specimen_set":
                        query = session.Repository.Query<Aliquot>(s => s.TestCode != "RCV", new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.State" })
                        .Where(userFilter, _validIds)
                        .Where("@0.Contains(outerIt.Isolate.Specimen.Id)", dto.Data);
                        break;
                    case "specimen":
                    case "specimen_who":
                        query = session.Repository.Query<Aliquot>(s => s.TestCode == "RCV", new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.State" })
                        .Where(userFilter, _validIds)
                        .Where("@0.Contains(outerIt.Isolate.Specimen.Id)", dto.Data);
                        break;
                    case "isolate":
                        query = session.Repository.Query<Aliquot>(s => s.Id > 0, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.State" })
                        .Where(userFilter, _validIds)
                        .Where("@0.Contains(outerIt.Isolate.Id)", dto.Data);
                        break;
                    case "aliquot":
                        query = session.Repository.Query<Aliquot>(s => s.Id > 0, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.State" })
                        .Where(userFilter, _validIds)
                        .Where("@0.Contains(outerIt.Id)", dto.Data);
                        break;
                    default:
                        // * nothing
                        query = session.Repository.Query<Aliquot>(s => s.Id == 0, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.State" });
                        break;
                }
                try
                {
                    var codes = new string[] { "ISR", "RCV" };
                    var gplist = new string[] { "GPRBC-OSEL", "GPRBC" };
                    var items = query.Select(e => new Label()
                    {
                        Id = e.Isolate.Specimen.Id,
                        CSID = e.Isolate.Specimen.CSID,
                        // * Hijacking //CUIDHR for Flu Season
                        //CUIDHR
                        DateCollected = e.Isolate.Specimen.DateCollected,
                        CUIDBC = e.CUID,
                        DaveHarvested = e.Isolate.DateHarvested,
                        FT = e.TestCode.ToUpper(),
                        HA = e.Isolate.Titer.HasValue ?
                         gplist.Contains(e.Isolate.RedBloodCellType) ? "HA: " + e.Isolate.Titer.Value.ToString() + " GP" : "HA: " + e.Isolate.Titer.Value.ToString()
                         : "HA:",
                        PH = codes.Contains(e.TestCode) ? e.Isolate.Specimen.PassageHistory : e.Isolate.PassageLevel.ToUpper(),
                        SID = e.Isolate.Specimen.SpecimenID.ToUpper().Trim(),
                        ST = e.Isolate.Specimen.State.Abbreviation.ToUpper(),
                        SUB = e.Isolate.Specimen.SubType.ToUpper(),
                        SpecimenSource = e.Isolate.Specimen.SpecimenSource.ToUpper(),

                    }).ToList();
                    List<string> laborder = null;
                    if (_lab != null && !String.IsNullOrEmpty(_lab.LabelTestCodeOrder))
                    {
                        var list = TestCodeList().Data.Select(s => s.Value);
                        laborder = _lab.LabelTestCodeOrder.Replace(" ", "").Trim().Split(',').ToList();
                    }
                    var labName = string.Empty;
                    foreach (var item in items)
                    {
                        if (String.IsNullOrEmpty(labName))
                        {
                            var tempSpecimen = session.Repository.Query<Specimen>(s => s.Id == item.Id).FirstOrDefault();
                            if (tempSpecimen != null)
                            {
                                var tempUser = session.Repository.Query<AspNetUser>(s => s.Id == tempSpecimen.CreatedBy, new List<string>(){"Lab"}).FirstOrDefault();
                                if (tempUser != null && tempUser.Lab != null)
                                {
                                    labName = tempUser.Lab.Name;
                                }
                            }
                        }
                        item.Lab = labName;
                        item.SID = item.SID.Truncate(16);
                        if (laborder != null)
                        {
                            if (laborder.Contains(item.FT))
                            {
                                item.OrderValue = laborder.IndexOf(item.FT);
                            }
                        }
                    }
                    items = items.OrderBy(s => s.OrderValue).ToList();
                    if (qty.Data > 1)
                    {
                        var n = qty.Data;
                        var newlist = new List<Label>();
                        items.ForEach(delegate(Label item)
                        {
                            for (int i = 0; i < n; i++)
                            {
                                newlist.Add(item);    
                            };
                        });
                        items = newlist;
                    }
                    var outputlist = new Labels() { data = items, format = baseobject.Data.Equals("specimen_who") ? _lab.WHOLabelFormatString : _lab.LabelFormatString,
                                                    postamble = _lab.LabelPostamble,
                                                    preamble = _lab.LabelPreamble,
                                                    url = _lab.PrinterURI
                    };

                    // * CLAW - 53
                    if (items.Count == 0)
                    {
                        return ResponseBase<Labels>.CreateErrorResponse(new Exception("No samples to print"));
                    }

                    return ResponseBase<Labels>.CreateSuccessResponse(outputlist);
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }

            }
        }

        public ResponseBase<IList<IsolateTypeDTO>> IsolateTypeList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<IsolateType>().OrderBy(s => s.Id)
                .Select(e => new IsolateTypeDTO() { DisplayName = e.DisplayName, Id = e.Id, SysName = e.SysName, IsVisible = e.IsVisible }
                ).ToList();
                return ResponseBase<IList<IsolateTypeDTO>>.CreateSuccessResponse(entity);
            }
        }

        // * Grown Set
        public ResponseBase AliquotInsertInitial(MessageBase<Tuple<int, int?>> isolateId)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Isolate>(s => s.Id == isolateId.Data.Item1, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Isolate Id {0} not found.", isolateId.Data.Item1)));
                }
                else
                {
                    if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot add aliquots to a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                    }
                }

                //CLAW-41
                var storedLocally = new string[]{};
                if (_lab != null)
                {
                    if (!String.IsNullOrEmpty(_lab.StoredLocallyCodes))
                    {
                        var codes = string.Join("", _lab.StoredLocallyCodes.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                        storedLocally = codes.Split(',');
                    }
                }

                //CLAW-128
                var customGrownSet = new string[] { };
                if (_lab != null)
                {
                    if (!String.IsNullOrEmpty(_lab.GrownTestCodeList))
                    {
                        var codes = string.Join("", _lab.GrownTestCodeList.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                        customGrownSet = codes.Split(',');
                    }
                }


                //CLAW-128
                //var target = this.TestCodeList();
                var target = customGrownSet.Length > 0 ? customGrownSet.ToList() : this.TestCodeList().Data.Select(s => s.Value);
                //CLAW-128
                //if (entity != null & target.Success)
                if (entity != null & target.Count() > 0)
                {
                    var existing = entity.Aliquots.Select(s => s.TestCode).Distinct();
                    //CLAW-128
                    //foreach (var item in target.Data.Select(s => s.Value).Except(existing).Except( new List<string>() { "RCV", "ISR" }))
                    foreach (var item in target.Except(existing).Except(new List<string>() { "RCV", "ISR" }))
                    {
                        var aliquot = new Aliquot()
                        {
                            CUID = "",
                            TestCode = item,
                            IsolateId = isolateId.Data.Item1,
                            //CLAW-41
                            IsStoredLocally = storedLocally.Contains(item),
                            CreatedOn = DateTime.UtcNow,
                            ModifiedOn = DateTime.UtcNow,
                            CreatedBy = _user.Identity.GetUserId(),
                            ModifiedBy = _user.Identity.GetUserId(),
                            ManifestId = !storedLocally.Contains(item) ? isolateId.Data.Item2 : null,
                        };
                        session.Repository.Insert<Aliquot>(aliquot);
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            }
        }

        // * ITT Set
        public ResponseBase AliquotInsertInitialITT(MessageBase<Tuple<int, int?>> isolateId)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Isolate>(s => s.Id == isolateId.Data.Item1, new List<string>() { "Aliquots", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Isolate Id {0} not found.", isolateId.Data.Item1)));
                }
                else
                {
                    if (entity.Status != null && !entity.Status.SysName.In("INPROGRESS", "SUBMITTED"))
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot add aliquots to a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                    }
                }

                //CLAW-41
                var storedLocally = new string[] { };
                if (_lab != null)
                {
                    if (!String.IsNullOrEmpty(_lab.StoredLocallyCodes))
                    {
                        var codes = string.Join("", _lab.StoredLocallyCodes.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                        storedLocally = codes.Split(',');
                    }
                }

                //CLAW-128
                var customITTSet = new string[] { };
                if (_lab != null)
                {
                    if (!String.IsNullOrEmpty(_lab.IttTestCodeList))
                    {
                        var codes = string.Join("", _lab.IttTestCodeList.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                        customITTSet = codes.Split(',');
                    }
                }


                //CLAW-128
                //var target = this.TestCodeListITT();
                var target = customITTSet.Length > 0 ? customITTSet.ToList() : this.TestCodeListITT().Data.Select(s => s.Value);
                //CLAW-128
                //if (entity != null & target.Success)
                if (entity != null & target.Count() > 0)
                {
                    var existing = entity.Aliquots.Select(s => s.TestCode).Distinct();
                    //CLAW-128
                    //foreach (var item in target.Data.Select(s => s.Value).Except(existing).Except(new List<string>() { "RCV", "ISR" }))
                    foreach (var item in target.Except(existing).Except(new List<string>() { "RCV", "ISR" }))
                    {
                        var aliquot = new Aliquot()
                        {
                            CUID = "",
                            TestCode = item,
                            IsolateId = isolateId.Data.Item1,
                            //CLAW-41
                            IsStoredLocally = storedLocally.Contains(item),
                            CreatedOn = DateTime.UtcNow,
                            ModifiedOn = DateTime.UtcNow,
                            CreatedBy = _user.Identity.GetUserId(),
                            ModifiedBy = _user.Identity.GetUserId(),
                            ManifestId = !storedLocally.Contains(item) ? isolateId.Data.Item2 : null,
                        };
                        session.Repository.Insert<Aliquot>(aliquot);
                    }
                }
                return ResponseBase.CreateSuccessResponse();
            }
        }


        public ResponseBase<IList<NameValueDTO>> TestCodeList()
        {
            var testCodeList = new List<NameValueDTO>();
            testCodeList.Add(new NameValueDTO() { Name = "REF", Value = "REF" });
            testCodeList.Add(new NameValueDTO() { Name = "RHI", Value = "RHI" });
            testCodeList.Add(new NameValueDTO() { Name = "RPS", Value = "RPS" });
            testCodeList.Add(new NameValueDTO() { Name = "MET", Value = "MET" });
            testCodeList.Add(new NameValueDTO() { Name = "ISA", Value = "ISA" });
            testCodeList.Add(new NameValueDTO() { Name = "ISR", Value = "ISR" });
            testCodeList.Add(new NameValueDTO() { Name = "NAI", Value = "NAI" });
            testCodeList.Add(new NameValueDTO() { Name = "RCV", Value = "RCV" });
            return ResponseBase<IList<NameValueDTO>>.CreateSuccessResponse(testCodeList);
        }

        public ResponseBase<IList<NameValueDTO>> TestCodeListITT()
        {
            var testCodeList = new List<NameValueDTO>();
            testCodeList.Add(new NameValueDTO() { Name = "REF", Value = "REF" });
            testCodeList.Add(new NameValueDTO() { Name = "MET", Value = "MET" });
            testCodeList.Add(new NameValueDTO() { Name = "ISA", Value = "ISA" });
            testCodeList.Add(new NameValueDTO() { Name = "NAI", Value = "NAI" });
            return ResponseBase<IList<NameValueDTO>>.CreateSuccessResponse(testCodeList);
        }

        public ResponseBase<IList<StatusDTO>> StatusDTOList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Status>().OrderBy(s => s.Id)
                .Select(e => new StatusDTO() { DisplayName = e.DisplayName, Id = e.Id, SysName = e.SysName }
                ).ToList();
                entity.Insert(0, new StatusDTO());
                return ResponseBase<IList<StatusDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase UpdateStatus(MessageBase<List<int>> specimens, MessageBase<int> status)
        {
            var bupdated = false;
            var bsubmitted = false;
            // * CLAW 80
            var pagesize = 80;
            var pages = specimens.Data.Count / pagesize;
            if (specimens.Data.Count % pagesize > 0 || pages == 0)
            {
                pages++;
            }

            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var submitted_status = session.Repository.FindBy<Status>(s => s.SysName.Equals("SUBMITTED"), null);
                    if (submitted_status == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "SUBMITTED")));
                    }

                    for (int i = 0; i < pages; i++)
                    {
                        var localIdList = specimens.Data.Skip(i * pagesize).Take(pagesize);

                        var statusEntity = session.Repository.FindBy<Status>(s => s.Id == status.Data, null);
                        if (statusEntity == null)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status not found.")));
                        }
                        var entities = session.Repository.Query<Specimen>(s => s.Id > 0, new List<string>() { "Status", "Isolates", "Isolates.Aliquots", "Isolates.Status", "Isolates.Aliquots.Status" })
                            .Where(userFilter, _validIds)
                            //.Where("@0.Contains(outerIt.Id)", specimens.Data);
                            .Where("@0.Contains(outerIt.Id)", localIdList);
                        foreach (var item in entities.ToList())
                        {
                            if (item.StatusId.Equals(submitted_status.Id))
                            {
                                //do nothing;
                                bsubmitted = true;
                                if (_user.User.IsInRole("Lab Editor"))
                                {
                                    item.StatusId = statusEntity.Id;
                                    session.Repository.Update<Specimen>(item);
                                    bupdated = true;
                                }
                            }
                            else
                            {
                                item.StatusId = statusEntity.Id;
                                session.Repository.Update<Specimen>(item);
                                bupdated = true;
                            }
                            foreach (var isolateitem in item.Isolates)
                            {
                                if (isolateitem.StatusId != null && isolateitem.StatusId.Equals(submitted_status.Id))
                                {
                                    //do nothing;
                                    bsubmitted = true;
                                    if (_user.User.IsInRole("Lab Editor"))
                                    {
                                        isolateitem.StatusId = statusEntity.Id;
                                        session.Repository.Update<Isolate>(isolateitem);
                                        bupdated = true;
                                    }

                                }
                                else
                                {
                                    isolateitem.StatusId = statusEntity.Id;
                                    session.Repository.Update<Isolate>(isolateitem);
                                    bupdated = true;
                                }
                                foreach (var aliquotitem in isolateitem.Aliquots)
                                {
                                    if (aliquotitem.StatusId != null && aliquotitem.StatusId.Equals(submitted_status.Id))
                                    {
                                        //do nothing;
                                        bsubmitted = true;
                                        if (_user.User.IsInRole("Lab Editor"))
                                        {
                                            aliquotitem.StatusId = statusEntity.Id;
                                            session.Repository.Update<Aliquot>(aliquotitem);
                                            bupdated = true;
                                        }
                                    }
                                    else
                                    {
                                        aliquotitem.StatusId = statusEntity.Id;
                                        session.Repository.Update<Aliquot>(aliquotitem);
                                        bupdated = true;
                                    }
                                }
                            }
                            if (bupdated && bsubmitted)
                            {
                                var notecategory = session.Repository.Query<NoteCategory>(s => s.SysName == "TRANSMISSION_EDIT").FirstOrDefault();
                                if (notecategory != null)
                                {
                                    var note = new SpecimenNote()
                                    {
                                        CreatedBy = _user.Identity.GetUserId(),
                                        CreatedOn = DateTime.UtcNow,
                                        ModifiedBy = _user.Identity.GetUserId(),
                                        ModifiedOn = DateTime.UtcNow,
                                        NoteCategoryId = notecategory.Id,
                                        Note = string.Format(@"Specimen Edited With a Status of {0} (Status changed)", item.Status.DisplayName),
                                        SpecimenId = item.Id,
                                    };
                                    session.Repository.Insert<SpecimenNote>(note);
                                }
                                else
                                {
                                    return ResponseBase.CreateErrorResponse(new Exception("Cannot add isolate (missing note category)"));
                                }
                            }
                        }
                    }
                    if (!bupdated)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("No status updated"));
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase<IList<LabDTO>> LabList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var idwhere = "Id > 0";
                if (_lab != null)
                {
                    idwhere = String.Format(@"Id == {0}", _lab.Id);
                }
                var entity = session.Repository.Query<Lab>()
                .Select(e => new LabDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    PrinterURI = e.PrinterURI,
                    LabelPreamble = e.LabelPreamble,
                    LabelPostamble = e.LabelPostamble,
                    LabelFormatString = e.LabelFormatString,
                    WHOLabelFormatString = e.WHOLabelFormatString,
                    StoredLocallyCodes = e.StoredLocallyCodes,
                    AliquotVersionCode = e.AliquotVersionCode,
                    LabelTestCodeOrder = e.LabelTestCodeOrder,
                    ImportPropertyOrder = e.ImportPropertyOrder,
                    GrownTestCodeList = e.GrownTestCodeList,
                    IttTestCodeList = e.IttTestCodeList,
                }
                )
                .Where(idwhere)
                .ToList();
                return ResponseBase<IList<LabDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase LabInsert(MessageBase<LabDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new Lab()
                    {
                        Name = dto.Data.Name,
                        PrinterURI = dto.Data.PrinterURI,
                        LabelPreamble = dto.Data.LabelPreamble,
                        LabelPostamble = dto.Data.LabelPostamble,
                        LabelFormatString = dto.Data.LabelFormatString,
                        WHOLabelFormatString = dto.Data.WHOLabelFormatString,
                        StoredLocallyCodes = dto.Data.StoredLocallyCodes,
                        AliquotVersionCode = dto.Data.AliquotVersionCode,
                        LabelTestCodeOrder = dto.Data.LabelTestCodeOrder,
                        ImportPropertyOrder = dto.Data.ImportPropertyOrder,
                        GrownTestCodeList = dto.Data.GrownTestCodeList,
                        IttTestCodeList = dto.Data.IttTestCodeList,
                    };
                    session.Repository.Insert<Lab>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<LabDTO> Lab(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Lab>().Where(s => s.Id == id.Data)
                .Select(e => new LabDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    PrinterURI = e.PrinterURI,
                    LabelPreamble = e.LabelPreamble,
                    LabelPostamble = e.LabelPostamble,
                    LabelFormatString = e.LabelFormatString,
                    WHOLabelFormatString = e.WHOLabelFormatString,
                    StoredLocallyCodes = e.StoredLocallyCodes,
                    AliquotVersionCode = e.AliquotVersionCode,
                    LabelTestCodeOrder = e.LabelTestCodeOrder,
                    ImportPropertyOrder = e.ImportPropertyOrder,
                    GrownTestCodeList = e.GrownTestCodeList,
                    IttTestCodeList = e.IttTestCodeList,
                }
                )
                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<LabDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<LabDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase LabEdit(MessageBase<LabDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Lab>().Where(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.PrinterURI = dto.Data.PrinterURI;
                        entity.LabelPreamble = dto.Data.LabelPreamble;
                        entity.LabelPostamble = dto.Data.LabelPostamble;
                        entity.LabelFormatString = dto.Data.LabelFormatString;
                        entity.WHOLabelFormatString = dto.Data.WHOLabelFormatString;
                        entity.StoredLocallyCodes = dto.Data.StoredLocallyCodes;
                        entity.AliquotVersionCode = dto.Data.AliquotVersionCode;
                        entity.LabelTestCodeOrder = dto.Data.LabelTestCodeOrder;
                        entity.ImportPropertyOrder = dto.Data.ImportPropertyOrder;
                        entity.GrownTestCodeList = dto.Data.GrownTestCodeList;
                        entity.IttTestCodeList = dto.Data.IttTestCodeList;
                        session.Repository.Update<Lab>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase LabDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Lab>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Lab>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<LabDTO> UserHasLab()
        {
            return _lab != null ? ResponseBase<LabDTO>.CreateSuccessResponse(new LabDTO() { Id = _lab.Id, Name = _lab.Name, StoredLocallyCodes = _lab.StoredLocallyCodes, ImportPropertyOrder = _lab.ImportPropertyOrder }) : ResponseBase<LabDTO>.CreateErrorResponse(new Exception("No lab assigned."));
        }


        public ResponseBase UserReadOnly()
        {
            var result = IsUserReadOnly;
            if (_user.User.IsInRole("Lab Editor"))
            {
                result = false;
            }
            return result ? ResponseBase.CreateSuccessResponse() : ResponseBase.CreateErrorResponse(new Exception("Read only user."));
        }
        public ResponseBase UserEditor()
        {
            var result = false;
            if (_user.User.IsInRole("Lab Editor"))
            {
                result = true;
            }
            return result ? ResponseBase.CreateSuccessResponse() : ResponseBase.CreateErrorResponse(new Exception("Not an editor."));
        }


        public PagedResponseBase<IList<ManifestDTO>> ManifestList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";

            if (!String.IsNullOrEmpty(request.Data.IdFilter))
            {
                idwhere = String.Format(@"Name.Contains(""{0}"")", request.Data.IdFilter);
            }

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                IQueryable<ManifestDTO> query;
                if (!String.IsNullOrEmpty(request.Data.CUID))
                {
                    // * non-elegant
                    query = session.Repository.Query<Manifest>(s => s.Id == s.Id, new List<string>() { "Status", "Aliquots" })
                    .Where(userFilter, _validIds)
                    .Where(s => s.Aliquots.Any<Aliquot>(a => a.CUID.Equals(request.Data.CUID)))
                    .Select(e => new ManifestDTO
                    {
                        Id = e.Id,
                        Name = e.Name,
                        CreatedOn = e.CreatedOn,
                        CreatedBy = e.CreatedBy,
                        ModifiedBy = e.ModifiedBy,
                        ModifiedOn = e.ModifiedOn,
                        AliquotCount = e.Aliquots.Count,
                        IsVisible = e.IsVisible,
                        Comments = e.Comments,
                        DateShipped = e.DateShipped,
                        TrackingNumber = e.TrackingNumber,
                        Status = new StatusDTO() { DisplayName = e.Status.DisplayName, Id = e.StatusId, SysName = e.Status.SysName },
                    }
                    ).OrderBy(request.Data.OrderBy);
                }
                else 
                {
                    query = session.Repository.Query<Manifest>(s => s.Id == s.Id, new List<string>() { "Status", "Aliquots" })
                    .Where(userFilter, _validIds)
                    .Where(idwhere)
                    .Where(request.Data.Where)
                    .Select(e => new ManifestDTO
                    {
                        Id = e.Id,
                        Name = e.Name,
                        CreatedOn = e.CreatedOn,
                        CreatedBy = e.CreatedBy,
                        ModifiedBy = e.ModifiedBy,
                        ModifiedOn = e.ModifiedOn,
                        AliquotCount = e.Aliquots.Count,
                        IsVisible = e.IsVisible,
                        Comments = e.Comments,
                        DateShipped = e.DateShipped,
                        TrackingNumber = e.TrackingNumber,
                        Status = new StatusDTO() { DisplayName = e.Status.DisplayName, Id = e.StatusId, SysName = e.Status.SysName },
                    }
                    ).OrderBy(request.Data.OrderBy);
                }
                int count = query.Count();
                List<ManifestDTO> entity = new List<ManifestDTO>(query.Skip<ManifestDTO>(skip).Take<ManifestDTO>(take));
                return PagedResponseBase<IList<ManifestDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
            }
        }

        public ResponseBase ManifestInsert(MessageBase<ManifestDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (_lab == null)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Manifests without a lab assigned."));
                }

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var status = session.Repository.FindBy<Status>(s => s.SysName.Equals("INPROGRESS"), null);
                    if (status == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "INPROGRESS")));
                    }

                    var newentity = new Manifest()
                    {
                        Name = dto.Data.Name,
                        IsVisible = dto.Data.IsVisible,
                        StatusId = status.Id,
                        Comments = dto.Data.Comments,
                        DateShipped = dto.Data.DateShipped,
                        TrackingNumber = dto.Data.TrackingNumber,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId(),
                    };
                    session.Repository.Insert<Manifest>(newentity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<ManifestDTO> Manifest(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Manifest>(s => s.Id == id.Data, new List<string>() { "Status" })
                .Where(userFilter, _validIds)
                .Select(e => new ManifestDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    StatusId = e.StatusId,
                    IsVisible = e.IsVisible,
                    Comments = e.Comments,
                    DateShipped = e.DateShipped,
                    TrackingNumber = e.TrackingNumber,
                    Status = new StatusDTO()
                    {
                        Id = e.Status.Id,
                        DisplayName = e.Status.DisplayName,
                        SysName = e.Status.SysName,
                    },
                    AliquotCount = e.Aliquots.Count,
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<ManifestDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                var aliquots = session.Repository.Query<Aliquot>(s => s.ManifestId == id.Data, new List<string>() { "Isolate", "Isolate.Specimen", "Isolate.Specimen.Status" })
                    .Where(userFilter, _validIds)
                    .Select(e => new AliquotDTO
                    {
                        StatusId = e.StatusId,
                        Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                        Id = e.Id,
                        IsolateId = e.IsolateId,
                        CUID = e.CUID,
                        TestCode = e.TestCode,
                        IsStoredLocally = e.IsStoredLocally,
                        TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(m => m.ManifestId == id.Data),
                        Isolate = new IsolateDTO() {
                        StatusId = e.Isolate.StatusId,
                        Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                        DateHarvested = e.Isolate.DateHarvested,
                        Specimen = new SpecimenDTO() {
                        SubType = e.Isolate.Specimen.SubType, CSID = e.Isolate.Specimen.CSID, Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },
                        SpecimenID = e.Isolate.Specimen.SpecimenID,
                        DateCollected = e.Isolate.Specimen.DateCollected,
                        
                    } },
                        ManifestId = e.ManifestId,
                        Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                    }
                    ).OrderBy(s => s.Isolate.Specimen.CSID).ThenBy(a => a.CUID).ToList();
                entity.Aliquots = aliquots;
                var tempL = session.Repository.Query<AspNetUser>(s => s.Id == entity.CreatedBy, new List<string>() { "Lab" }).FirstOrDefault();
                entity.Lab = new LabDTO() { Id = tempL.Lab.Id, Name = tempL.Lab.Name };
                return ResponseBase<ManifestDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase ManifestDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Manifests."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Manifest>().Where(s => s.Id == id.Data)
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Manifest>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase ManifestEdit(MessageBase<ManifestDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Manifests."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Manifest>(s => s.Id == dto.Data.Id, new List<string>() { "Aliquots" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.StatusId = dto.Data.StatusId;
                        entity.IsVisible = dto.Data.IsVisible;
                        entity.Comments = dto.Data.Comments;
                        entity.DateShipped = dto.Data.DateShipped;
                        entity.TrackingNumber = dto.Data.TrackingNumber;
                        entity.ModifiedBy = _user.Identity.GetUserId();
                        entity.ModifiedOn = DateTime.UtcNow;

                        //kindof brute force
                        var toremove = entity.Aliquots.Select(s => s.Id).Except(dto.Data.Aliquots.Select(x => x.Id)).ToList();
                        toremove.AddRange(entity.Aliquots.Where(s => s.IsStoredLocally).Select(s => s.Id).ToList());
                        var toadd = dto.Data.Aliquots.Where(s => !s.IsStoredLocally).Select(x => x.Id).Except(entity.Aliquots.Select(s => s.Id)).ToList();
                        foreach (var item in toremove.Distinct())
                        {
                            var e = session.Repository.Find<Aliquot>(item);
                            if (e != null)
                            {
                                entity.Aliquots.Remove(e);
                            }
                        }
                        foreach (var item in toadd.Distinct())
                        {
                            var e = session.Repository.Find<Aliquot>(item);
                            if (e != null)
                            {
                                entity.Aliquots.Add(e);
                            }
                        }
                        session.Repository.Update<Manifest>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<AliquotDTO> GetAliquotByCUID(MessageBase<string> cuid)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Aliquot>(s => s.CUID == cuid.Data, new List<string>() { "Isolate", "Isolate.Status", "Isolate.Specimen", "Isolate.Specimen.Status" })
                .Where(userFilter, _validIds)
                .Where(s => !s.IsStoredLocally)
                .Select(e => new AliquotDTO
                {
                    StatusId = e.StatusId,
                    Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                    Id = e.Id,
                    CUID = e.CUID,
                    TestCode = e.TestCode,
                    IsolateId = e.IsolateId,
                    Isolate = new IsolateDTO() {
                        StatusId = e.Isolate.StatusId,
                        Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                        Specimen = new SpecimenDTO() { SubType = e.Isolate.Specimen.SubType, CSID = e.Isolate.Specimen.CSID, Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName } }
                    },
                    ManifestId = e.ManifestId,
                    Manifest = e.ManifestId.HasValue ? new ManifestDTO() {Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn} : null,
                    IsStoredLocally = e.IsStoredLocally,
                    TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<AliquotDTO>.CreateErrorResponse(new Exception(string.Format(@"CUID {0} not found, or is stored locally.", cuid.Data)));
                }
                return ResponseBase<AliquotDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase<IList<ManifestDTO>> ManifestList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Manifest>(s => s.Id == s.Id, new List<string>() { "Status" })
                .Where(userFilter, _validIds)
                .Select(e => new ManifestDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    DateShipped = e.DateShipped,
                    TrackingNumber = e.TrackingNumber,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    StatusId = e.StatusId,
                    IsVisible = e.IsVisible,
                    Status = new StatusDTO()
                    {
                        Id = e.Status.Id,
                        DisplayName = e.Status.DisplayName,
                        SysName = e.Status.SysName,
                    },
                    AliquotCount = e.Aliquots.Count,
                }
                ).ToList();
                entity.Insert(0, new ManifestDTO() { Id = null, IsVisible = true });
                return ResponseBase<IList<ManifestDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase IsolateTypeInsert(MessageBase<IsolateTypeDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create Isolate Types."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new IsolateType()
                    {
                        DisplayName = dto.Data.DisplayName,
                        SysName = dto.Data.SysName,
                        IsVisible = dto.Data.IsVisible,
                    };
                    session.Repository.Insert<IsolateType>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IsolateTypeDTO> IsolateType(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<IsolateType>(s => s.Id == id.Data)
                .Select(e => new IsolateTypeDTO
                {
                    DisplayName = e.DisplayName,
                    SysName = e.SysName,
                    IsVisible = e.IsVisible,
                }
                )
                .FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase<IsolateTypeDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<IsolateTypeDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase IsolateTypeDelete(MessageBase<int> id)
        {
            if (!_user.User.IsInRole("Admin"))
            {
                return ResponseBase.CreateErrorResponse(new Exception("Cannot delete Isolate Types."));
            }
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<IsolateType>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<IsolateType>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase IsolateTypeEdit(MessageBase<IsolateTypeDTO> dto)
        {
            if (!_user.User.IsInRole("Admin"))
            {
                return ResponseBase.CreateErrorResponse(new Exception("Cannot edit Isolate Types."));
            }
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<IsolateType>().Where(s => s.Id == dto.Data.Id)
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.DisplayName = dto.Data.DisplayName;
                        entity.SysName = dto.Data.SysName;
                        entity.IsVisible = dto.Data.IsVisible;
                        session.Repository.Update<IsolateType>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase<TemplateDTO> GenerateWeeklyFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {

                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Weekly")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Weekly" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Weekly template not found.")));
                    }
                }

                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<APHLWeeklyDTO>();

                var viewwhere = "@0.Contains(outerIt.Id)";
                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Data.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Aliquot>()
                                        .Where(s => s.TestCode == "RCV")
                                        .Select(e => new APHLWeeklyDTO()
                                        {
                                            CSID = e.Isolate.Specimen.CSID,
                                            CUID = e.CUID,
                                            DateCollected = e.Isolate.Specimen.DateCollected,
                                            DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                            SenderId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                            SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                            SpecimenID = e.Isolate.Specimen.SpecimenID,
                                            State = e.Isolate.Specimen.State.Abbreviation,
                                            SubType = e.Isolate.Specimen.SubType,
                                            Id = e.Isolate.Specimen.Id,
                                            CreatedBy = e.Isolate.Specimen.CreatedBy,
                                            Lab = e.Isolate.Specimen.Lab.Name,
                                        })
                                        .Where(userFilter, _validIds)
                                        //.Where(viewwhere, dto.Data);
                                        .Where(viewwhere, localIdList);  
                    localAliquotList.AddRange(query.ToList());
                }
                // localAliquotList
                //var outputfile = ExcelFileFactory.GenerateFile<APHLWeeklyDTO>(entity, query.ToList(), 2);

                // * CLAW - 53
                if (localAliquotList.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to report"));
                }

                var outputfile = ExcelFileFactory.GenerateFile<APHLWeeklyDTO>(entity, localAliquotList, 2);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }

        public ResponseBase<TemplateDTO> GenerateMonthlyFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<APHLMonthlySpecimenDTO>();

                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Monthly")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "Monthly" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Monthly template not found.")));
                    }
                }

                var status = session.Repository.FindBy<Status>(s => s.SysName.Equals("SUBMITTED"), null);
                if (status == null)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "SUBMITTED")));
                }

                var viewwhere = "@0.Contains(outerIt.Id)";

                for (int iPage = 0; iPage < pages; iPage++) 
                {
                    var localIdList = dto.Data.Skip(iPage * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Specimen>()
                        .Where(s => s.Isolates.Any(i => i.Aliquots.Any(a => a.TestCode == "RCV")))
                                        .Select(e => new APHLMonthlySpecimenDTO()
                                        {
                                            Id = e.Id,
                                            InCulture = e.Status.SysName.Equals("INPROGRESS"),
                                            Inoculated = e.DateInoculated <= DateTime.UtcNow,
                                            Recovered = e.Isolates.Any(s => s.IsolateType.SysName.Equals("GROWN")),
                                            Shipped = e.Isolates.SelectMany(s => s.Aliquots).Where(t => t.Manifest != null && status.Id.Equals(t.Isolate.Specimen.StatusId)).Min(ds => ds.Manifest.DateShipped) <= DateTime.UtcNow,
                                            // must be submitted *
                                            // count is one per specimen
                                            // must be shipped
                                            // * CLAW-81 13 JAN 2017
                                            //SubType = e.SubType,
                                            SubType = e.WHOSubmittedSubtype,
                                            TAT = DbFunctions.DiffDays(e.Package.DateReceived, e.Isolates.SelectMany(s => s.Aliquots).Where(t => t.Manifest != null && status.Id.Equals(t.Isolate.Specimen.StatusId)).Max(ds => ds.Manifest.DateShipped)),
                                            // must be submitted
                                            // count is one per specimen
                                            // must be shipped
                                            // value is MAX(SENT ALIQUOT) - DATE RECIEVED
                                            // VNR == RCV shipped, !VNR == Grown Osolate shipped
                                            TiterLT4 = e.Isolates.Any(s => s.Titer != null && s.Titer.Value < 4),
                                            VNR = e.Isolates.Any(s => s.IsolateType.SysName.Equals("VNR")),
                                            Lab = e.Lab.Name,
                                            CreatedBy = e.CreatedBy,
                                        })
                                        .Where(userFilter, _validIds)
                                        //.Where(viewwhere, dto.Data);
                                        .Where(viewwhere, localIdList);
                    localAliquotList.AddRange(query.ToList());
                };
                




                    //var subtypes = query.GroupBy(s => s.SubType).Select(gbS =>
                var subtypes = localAliquotList.AsQueryable().GroupBy(s => s.SubType).Select(gbS =>
                    new APHLMonthlyDTO()
                    {
                        SubType = gbS.Key,
                        Received = gbS.Count(),
                        Inocolated = gbS.Count(s => s.Inoculated.HasValue && s.Inoculated.Value),
                        Recovered = gbS.Count(s => s.Recovered.HasValue && s.Recovered.Value),
                        InCulture = gbS.Count(s => s.InCulture.HasValue && s.InCulture.Value),
                        HAUnder4 = gbS.Count(s => s.TiterLT4.HasValue && s.TiterLT4.Value),
                        VNR = gbS.Count(s => s.VNR.HasValue && s.VNR.Value),
                        TotalSent = gbS.Count(s => s.Shipped.HasValue && s.Shipped.Value),
                        TAT_Avg = gbS.Average(s => s.TAT),
                        Recovered_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.Recovered.HasValue && s.Recovered.Value) / (double)gbS.Count(), 2),
                        HAUnder4_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.TiterLT4.HasValue && s.TiterLT4.Value) / (double)gbS.Count(), 2),
                        VNR_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.VNR.HasValue && s.VNR.Value) / (double)gbS.Count(), 2),
                        PendingInCulture_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.InCulture.HasValue && s.InCulture.Value) / (double)gbS.Count(), 2),
                    }
                    );
                var output = subtypes.ToList();

                //var total = query.GroupBy(s => s.Id > 0).Select(gbS =>
                var total = localAliquotList.AsQueryable().GroupBy(s => s.Id > 0).Select(gbS =>
                    new APHLMonthlyDTO()
                    {
                        SubType = "Total",
                        Received = gbS.Count(),
                        Inocolated = gbS.Count(s => s.Inoculated.HasValue && s.Inoculated.Value),
                        Recovered = gbS.Count(s => s.Recovered.HasValue && s.Recovered.Value),
                        InCulture = gbS.Count(s => s.InCulture.HasValue && s.InCulture.Value),
                        HAUnder4 = gbS.Count(s => s.TiterLT4.HasValue && s.TiterLT4.Value),
                        VNR = gbS.Count(s => s.VNR.HasValue && s.VNR.Value),
                        TotalSent = gbS.Count(s => s.Shipped.HasValue && s.Shipped.Value),
                        TAT_Avg = gbS.Average(s => s.TAT),
                        Recovered_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.Recovered.HasValue && s.Recovered.Value) / (double)gbS.Count(), 2),
                        HAUnder4_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.TiterLT4.HasValue && s.TiterLT4.Value) / (double)gbS.Count(), 2),
                        VNR_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.VNR.HasValue && s.VNR.Value) / (double)gbS.Count(), 2),
                        PendingInCulture_Pct = Math.Round(100.0 * (double)gbS.Count(s => s.InCulture.HasValue && s.InCulture.Value) / (double)gbS.Count(), 2),
                    }
                    );
                output.AddRange(total.ToList());

                // * CLAW - 53
                if (output.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to report"));
                }

                //var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, query.Union(vnrQuery).ToList());
                var outputfile = ExcelFileFactory.GenerateFile<APHLMonthlyDTO>(entity, output, 6);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }


        public ResponseBase<TemplateDTO> GenerateISRFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {

                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<AliquotDTO>();

                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "ISR")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "ISR" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"ISR template not found.")));
                    }
                }

                var viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                if (view.Data.Equals("Manifests"))
                {
                    viewwhere = "@0.Contains(outerIt.ManifestId.Value)";
                }
                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Data.Skip(i * pagesize).Take(pagesize);

                    var query = session.Repository.Query<Aliquot>()
                                        .Where(s => s.TestCode == "ISR")
                                        .Select(e => new AliquotDTO()
                                        {
                                            StatusId = e.StatusId,
                                            Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                                            Id = e.Id,
                                            IsolateId = e.IsolateId,
                                            CUID = e.CUID,
                                            TestCode = e.TestCode,
                                            CreatedBy = e.CreatedBy,
                                            ManifestId = e.ManifestId,
                                            Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                                            IsStoredLocally = e.IsStoredLocally,
                                            TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                                            Isolate = new IsolateDTO()
                                            {
                                                StatusId = e.Isolate.StatusId,
                                                Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                                                Id = e.Isolate.Id,
                                                PassageLevel = e.Isolate.PassageLevel,
                                                DateHarvested = e.Isolate.DateHarvested,
                                                Titer = e.Isolate.Titer,
                                                RedBloodCellType = e.Isolate.RedBloodCellType,
                                                DateInoculated = e.Isolate.DateInoculated,
                                                CreatedOn = e.Isolate.CreatedOn,
                                                CreatedBy = e.Isolate.CreatedBy,
                                                ModifiedBy = e.Isolate.ModifiedBy,
                                                ModifiedOn = e.Isolate.ModifiedOn,
                                                SpecimenId = e.Isolate.SpecimenId,
                                                Comments = e.Isolate.Comments,
                                                IsolateType = new IsolateTypeDTO()
                                                {
                                                    Id = e.Isolate.IsolateType.Id,
                                                    DisplayName = e.Isolate.IsolateType.DisplayName,
                                                    SysName = e.Isolate.IsolateType.SysName,
                                                    IsVisible = e.Isolate.IsolateType.IsVisible,
                                                },
                                                Specimen = new SpecimenDTO()
                                                {
                                                    FluSeason = e.Isolate.Specimen.FluSeason,
                                                    Id = e.Isolate.Specimen.Id,
                                                    DateInoculated = e.Isolate.Specimen.DateInoculated,
                                                    SampleClass = e.Isolate.Specimen.SampleClass,
                                                    CSID = e.Isolate.Specimen.CSID,
                                                    CUID = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Where(s => s.TestCode.Equals("RCV")).OrderBy(s => s.Id).FirstOrDefault().CUID,
                                                    CreatedOn = e.Isolate.Specimen.CreatedOn,
                                                    CreatedBy = e.Isolate.Specimen.CreatedBy,
                                                    ModifiedBy = e.Isolate.Specimen.ModifiedBy,
                                                    ModifiedOn = e.Isolate.Specimen.ModifiedOn,
                                                    ProjectId = e.Isolate.Specimen.ProjectId,
                                                    Package = new PackageDTO()
                                                    {
                                                        Id = e.Isolate.Specimen.Package.Id,
                                                        Name = e.Isolate.Specimen.Package.Name,
                                                        SenderId = e.Isolate.Specimen.Package.SenderId,
                                                        DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                                        SpecimenCondition = e.Isolate.Specimen.Package.SpecimenCondition,
                                                        Sender = new SenderDTO()
                                                        {
                                                            Id = e.Isolate.Specimen.Package.Sender.Id,
                                                            RasClientId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                                            CompanyName = e.Isolate.Specimen.Package.Sender.CompanyName,
                                                            Address = e.Isolate.Specimen.Package.Sender.Address,
                                                            City = e.Isolate.Specimen.Package.Sender.City,
                                                            State = e.Isolate.Specimen.Package.Sender.State,
                                                        }
                                                    },
                                                    PackageId = e.Isolate.Specimen.PackageId,
                                                    // CLAW-38/40
                                                    Country = new CountryDTO() { Id = e.Isolate.Specimen.Country.Id, Name = e.Isolate.Specimen.Country.Name, ShortCode = e.Isolate.Specimen.Country.ShortCode, LongCode = e.Isolate.Specimen.Country.LongCode },
                                                    CountryId = e.Isolate.Specimen.CountryId,
                                                    // CLAW-38/40
                                                    State = new StateDTO() { Id = e.Isolate.Specimen.State.Id, Name = e.Isolate.Specimen.State.Name, Abbreviation = e.Isolate.Specimen.State.Abbreviation },
                                                    StateId = e.Isolate.Specimen.StateId,
                                                    County = new CountyDTO() { Id = e.Isolate.Specimen.County.Id, Name = e.Isolate.Specimen.County.Name },
                                                    CountyId = e.Isolate.Specimen.CountyId,
                                                    Activity = e.Isolate.Specimen.Activity,
                                                    DateCollected = e.Isolate.Specimen.DateCollected,
                                                    SpecimenID = e.Isolate.Specimen.SpecimenID,
                                                    PatientAge = e.Isolate.Specimen.PatientAge,
                                                    PatientAgeUnits = e.Isolate.Specimen.PatientAgeUnits,
                                                    PatientSex = e.Isolate.Specimen.PatientSex,
                                                    PassageHistory = e.Isolate.Specimen.PassageHistory,
                                                    SubType = e.Isolate.Specimen.SubType,
                                                    SpecimenSource = e.Isolate.Specimen.SpecimenSource,
                                                    InitialStorage = e.Isolate.Specimen.InitialStorage,
                                                    CompCSID1 = e.Isolate.Specimen.CompCSID1,
                                                    CompCSID2 = e.Isolate.Specimen.CompCSID2,
                                                    OtherLocation = e.Isolate.Specimen.OtherLocation,
                                                    DrugResistant = e.Isolate.Specimen.DrugResistant,
                                                    Deceased = e.Isolate.Specimen.Deceased,
                                                    SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                                    NursingHome = e.Isolate.Specimen.NursingHome,
                                                    Vaccinated = e.Isolate.Specimen.Vaccinated,
                                                    Travel = e.Isolate.Specimen.Travel,
                                                    ReasonForSubmission = e.Isolate.Specimen.ReasonForSubmission,
                                                    Comments = e.Isolate.Specimen.Comments,
                                                    StatusId = e.Isolate.Specimen.StatusId,
                                                    ContractLabFrom = e.Isolate.Specimen.Lab != null ? e.Isolate.Specimen.Lab.Name : null,
                                                    Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },

                                                    TravelCountry = e.Isolate.Specimen.TravelCountry,
                                                    TravelState = e.Isolate.Specimen.TravelState,
                                                    TravelCounty = e.Isolate.Specimen.TravelCounty,
                                                    TravelCity = e.Isolate.Specimen.TravelCity,
                                                    TravelReturnDate = e.Isolate.Specimen.TravelReturnDate,
                                                    HostCellLine = e.Isolate.Specimen.HostCellLine,
                                                    Substrate = e.Isolate.Specimen.Substrate,
                                                    PassageNumber = e.Isolate.Specimen.PassageNumber,
                                                    PCRInfA = e.Isolate.Specimen.PCRInfA,
                                                    PCRpdmInfA = e.Isolate.Specimen.PCRpdmInfA,
                                                    PCRpdmH1 = e.Isolate.Specimen.PCRpdmH1,
                                                    PCRH3 = e.Isolate.Specimen.PCRH3,
                                                    PCRInfB = e.Isolate.Specimen.PCRInfB,
                                                    PCRBVic = e.Isolate.Specimen.PCRBVic,
                                                    PCRBYam = e.Isolate.Specimen.PCRBYam,
                                                    City = e.Isolate.Specimen.City,
                                                    WHOSubmittedSubtype = e.Isolate.Specimen.WHOSubmittedSubtype,
                                                }
                                            }
                                        })
                                        .Where(userFilter, _validIds)
                                    //.Where(viewwhere, dto.Data);
                                    .Where(viewwhere, localIdList);  
                                    localAliquotList.AddRange(query.ToList());
                }
                //var results = query.ToList();
                var results = localAliquotList;
                if (_lab != null)
                {
                    foreach (var item in results)
                    {
                        item.VersionCode = _lab.AliquotVersionCode;
                    }
                }

                // * CLAW - 53
                if (results.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to export"));
                }

                var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, results, 6);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }

        public ResponseBase<TemplateDTO> GenerateNAIFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<AliquotDTO>();


                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "NAI")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "NAI" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"NAI template not found.")));
                    }
                }

                var viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                if (view.Data.Equals("Manifests"))
                {
                    viewwhere = "@0.Contains(outerIt.ManifestId.Value)";
                }

                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Data.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Aliquot>()
                                        .Where(s => s.TestCode == "NAI")
                                        .Select(e => new AliquotDTO()
                                        {
                                            StatusId = e.StatusId,
                                            Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                                            Id = e.Id,
                                            IsolateId = e.IsolateId,
                                            CUID = e.CUID,
                                            TestCode = e.TestCode,
                                            CreatedBy = e.CreatedBy,
                                            ManifestId = e.ManifestId,
                                            Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                                            IsStoredLocally = e.IsStoredLocally,
                                            TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                                            Isolate = new IsolateDTO()
                                            {
                                                StatusId = e.Isolate.StatusId,
                                                Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                                                Id = e.Isolate.Id,
                                                PassageLevel = e.Isolate.PassageLevel,
                                                DateHarvested = e.Isolate.DateHarvested,
                                                Titer = e.Isolate.Titer,
                                                RedBloodCellType = e.Isolate.RedBloodCellType,
                                                DateInoculated = e.Isolate.DateInoculated,
                                                CreatedOn = e.Isolate.CreatedOn,
                                                CreatedBy = e.Isolate.CreatedBy,
                                                ModifiedBy = e.Isolate.ModifiedBy,
                                                ModifiedOn = e.Isolate.ModifiedOn,
                                                SpecimenId = e.Isolate.SpecimenId,
                                                Comments = e.Isolate.Comments,
                                                IsolateType = new IsolateTypeDTO()
                                                {
                                                    Id = e.Isolate.IsolateType.Id,
                                                    DisplayName = e.Isolate.IsolateType.DisplayName,
                                                    SysName = e.Isolate.IsolateType.SysName,
                                                    IsVisible = e.Isolate.IsolateType.IsVisible,
                                                },
                                                Specimen = new SpecimenDTO()
                                                {
                                                    FluSeason = e.Isolate.Specimen.FluSeason,
                                                    Id = e.Isolate.Specimen.Id,
                                                    DateInoculated = e.Isolate.Specimen.DateInoculated,
                                                    SampleClass = e.Isolate.Specimen.SampleClass,
                                                    CSID = e.Isolate.Specimen.CSID,
                                                    CUID = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Where(s => s.TestCode.Equals("RCV")).OrderBy(s => s.Id).FirstOrDefault().CUID,
                                                    CreatedOn = e.Isolate.Specimen.CreatedOn,
                                                    CreatedBy = e.Isolate.Specimen.CreatedBy,
                                                    ModifiedBy = e.Isolate.Specimen.ModifiedBy,
                                                    ModifiedOn = e.Isolate.Specimen.ModifiedOn,
                                                    ProjectId = e.Isolate.Specimen.ProjectId,
                                                    Package = new PackageDTO()
                                                    {
                                                        Id = e.Isolate.Specimen.Package.Id,
                                                        Name = e.Isolate.Specimen.Package.Name,
                                                        SenderId = e.Isolate.Specimen.Package.SenderId,
                                                        DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                                        SpecimenCondition = e.Isolate.Specimen.Package.SpecimenCondition,
                                                        Sender = new SenderDTO()
                                                        {
                                                            Id = e.Isolate.Specimen.Package.Sender.Id,
                                                            RasClientId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                                            CompanyName = e.Isolate.Specimen.Package.Sender.CompanyName,
                                                            Address = e.Isolate.Specimen.Package.Sender.Address,
                                                            City = e.Isolate.Specimen.Package.Sender.City,
                                                            State = e.Isolate.Specimen.Package.Sender.State,
                                                        }
                                                    },
                                                    PackageId = e.Isolate.Specimen.PackageId,
                                                    Country = new CountryDTO() { Id = e.Isolate.Specimen.Country.Id, Name = e.Isolate.Specimen.Country.Name, ShortCode = e.Isolate.Specimen.Country.ShortCode, LongCode = e.Isolate.Specimen.Country.LongCode },
                                                    CountryId = e.Isolate.Specimen.CountryId,
                                                    State = new StateDTO() { Id = e.Isolate.Specimen.State.Id, Name = e.Isolate.Specimen.State.Name, Abbreviation = e.Isolate.Specimen.State.Abbreviation },
                                                    StateId = e.Isolate.Specimen.StateId,
                                                    County = new CountyDTO() { Id = e.Isolate.Specimen.County.Id, Name = e.Isolate.Specimen.County.Name },
                                                    CountyId = e.Isolate.Specimen.CountyId,
                                                    Activity = e.Isolate.Specimen.Activity,
                                                    DateCollected = e.Isolate.Specimen.DateCollected,
                                                    SpecimenID = e.Isolate.Specimen.SpecimenID,
                                                    PatientAge = e.Isolate.Specimen.PatientAge,
                                                    PatientAgeUnits = e.Isolate.Specimen.PatientAgeUnits,
                                                    PatientSex = e.Isolate.Specimen.PatientSex,
                                                    PassageHistory = e.Isolate.Specimen.PassageHistory,
                                                    SubType = e.Isolate.Specimen.SubType,
                                                    SpecimenSource = e.Isolate.Specimen.SpecimenSource,
                                                    InitialStorage = e.Isolate.Specimen.InitialStorage,
                                                    CompCSID1 = e.Isolate.Specimen.CompCSID1,
                                                    CompCSID2 = e.Isolate.Specimen.CompCSID2,
                                                    OtherLocation = e.Isolate.Specimen.OtherLocation,
                                                    DrugResistant = e.Isolate.Specimen.DrugResistant,
                                                    Deceased = e.Isolate.Specimen.Deceased,
                                                    SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                                    NursingHome = e.Isolate.Specimen.NursingHome,
                                                    Vaccinated = e.Isolate.Specimen.Vaccinated,
                                                    Travel = e.Isolate.Specimen.Travel,
                                                    ReasonForSubmission = e.Isolate.Specimen.ReasonForSubmission,
                                                    Comments = e.Isolate.Specimen.Comments,
                                                    StatusId = e.Isolate.Specimen.StatusId,
                                                    Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },

                                                    TravelCountry = e.Isolate.Specimen.TravelCountry,
                                                    TravelState = e.Isolate.Specimen.TravelState,
                                                    TravelCounty = e.Isolate.Specimen.TravelCounty,
                                                    TravelCity = e.Isolate.Specimen.TravelCity,
                                                    TravelReturnDate = e.Isolate.Specimen.TravelReturnDate,
                                                    HostCellLine = e.Isolate.Specimen.HostCellLine,
                                                    Substrate = e.Isolate.Specimen.Substrate,
                                                    PassageNumber = e.Isolate.Specimen.PassageNumber,
                                                    PCRInfA = e.Isolate.Specimen.PCRInfA,
                                                    PCRpdmInfA = e.Isolate.Specimen.PCRpdmInfA,
                                                    PCRpdmH1 = e.Isolate.Specimen.PCRpdmH1,
                                                    PCRH3 = e.Isolate.Specimen.PCRH3,
                                                    PCRInfB = e.Isolate.Specimen.PCRInfB,
                                                    PCRBVic = e.Isolate.Specimen.PCRBVic,
                                                    PCRBYam = e.Isolate.Specimen.PCRBYam,
                                                    City = e.Isolate.Specimen.City,
                                                    WHOSubmittedSubtype = e.Isolate.Specimen.WHOSubmittedSubtype,
                                                }
                                            }
                                        })
                                        .Where(userFilter, _validIds)
                                        //.Where(viewwhere, dto.Data);
                                        .Where(viewwhere, localIdList);  
                        localAliquotList.AddRange(query.ToList());
                }

                // * CLAW - 53
                if (localAliquotList.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to export"));
                }

                //var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, query.ToList(), 2);
                var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, localAliquotList, 2);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }

        public ResponseBase<TemplateDTO> GenerateINOCFile(MessageBase<List<int>> dto, MessageBase<string> view)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                // * CLAW 80
                var pagesize = 80;
                var pages = dto.Data.Count / pagesize;
                if (dto.Data.Count % pagesize > 0 || pages == 0)
                {
                    pages++;
                }
                var localAliquotList = new List<AliquotDTO>();


                var labWhere = "Id > 0";
                if (_lab != null)
                {
                    labWhere = String.Format(@"LabId = {0}", _lab.Id);
                }
                // * lab specific first
                var entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "INOC")
                .Where(labWhere)
                .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                ).FirstOrDefault();
                if (entity == null)
                {
                    // * non lab specific second
                    entity = session.Repository.Query<Template>().Where(s => s.TemplateType == "INOC" && s.LabId == null)
                    .Select(e => new TemplateDTO() { Id = e.Id, TemplateType = e.TemplateType, FileName = e.FileName, File = e.File }
                    ).FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception(string.Format(@"INOC template not found.")));
                    }
                }

                var viewwhere = "@0.Contains(outerIt.Isolate.Specimen.Id)";
                if (view.Data.Equals("Manifests"))
                {
                    viewwhere = "@0.Contains(outerIt.ManifestId.Value)";
                }

                for (int i = 0; i < pages; i++)
                {
                    var localIdList = dto.Data.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Aliquot>()
                                        .Where(s => s.TestCode == "RCV")
                                        .Select(e => new AliquotDTO()
                                        {
                                            StatusId = e.StatusId,
                                            Status = e.StatusId.HasValue ? new StatusDTO() { Id = e.Status.Id, DisplayName = e.Status.DisplayName, SysName = e.Status.SysName } : null,
                                            Id = e.Id,
                                            IsolateId = e.IsolateId,
                                            CUID = e.CUID,
                                            TestCode = e.TestCode,
                                            CreatedBy = e.CreatedBy,
                                            ManifestId = e.ManifestId,
                                            Manifest = e.ManifestId.HasValue ? new ManifestDTO() { Id = e.Manifest.Id, Name = e.Manifest.Name, CreatedOn = e.Manifest.CreatedOn } : null,
                                            IsStoredLocally = e.IsStoredLocally,
                                            TubeCount = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Count(),
                                            Isolate = new IsolateDTO()
                                            {
                                                StatusId = e.Isolate.StatusId,
                                                Status = e.Isolate.StatusId.HasValue ? new StatusDTO() { Id = e.Isolate.Status.Id, DisplayName = e.Isolate.Status.DisplayName, SysName = e.Isolate.Status.SysName } : null,
                                                Id = e.Isolate.Id,
                                                PassageLevel = e.Isolate.PassageLevel,
                                                DateHarvested = e.Isolate.DateHarvested,
                                                Titer = e.Isolate.Titer,
                                                RedBloodCellType = e.Isolate.RedBloodCellType,
                                                DateInoculated = e.Isolate.DateInoculated,
                                                CreatedOn = e.Isolate.CreatedOn,
                                                CreatedBy = e.Isolate.CreatedBy,
                                                ModifiedBy = e.Isolate.ModifiedBy,
                                                ModifiedOn = e.Isolate.ModifiedOn,
                                                SpecimenId = e.Isolate.SpecimenId,
                                                Comments = e.Isolate.Comments,
                                                IsolateType = new IsolateTypeDTO()
                                                {
                                                    Id = e.Isolate.IsolateType.Id,
                                                    DisplayName = e.Isolate.IsolateType.DisplayName,
                                                    SysName = e.Isolate.IsolateType.SysName,
                                                    IsVisible = e.Isolate.IsolateType.IsVisible,
                                                },
                                                Specimen = new SpecimenDTO()
                                                {
                                                    FluSeason = e.Isolate.Specimen.FluSeason,
                                                    Id = e.Isolate.Specimen.Id,
                                                    DateInoculated = e.Isolate.Specimen.DateInoculated,
                                                    SampleClass = e.Isolate.Specimen.SampleClass,
                                                    CSID = e.Isolate.Specimen.CSID,
                                                    CUID = e.Isolate.Specimen.Isolates.SelectMany(s => s.Aliquots).Where(s => s.TestCode.Equals("RCV")).OrderBy(s => s.Id).FirstOrDefault().CUID,
                                                    CreatedOn = e.Isolate.Specimen.CreatedOn,
                                                    CreatedBy = e.Isolate.Specimen.CreatedBy,
                                                    ModifiedBy = e.Isolate.Specimen.ModifiedBy,
                                                    ModifiedOn = e.Isolate.Specimen.ModifiedOn,
                                                    ProjectId = e.Isolate.Specimen.ProjectId,
                                                    Package = new PackageDTO()
                                                    {
                                                        Id = e.Isolate.Specimen.Package.Id,
                                                        Name = e.Isolate.Specimen.Package.Name,
                                                        SenderId = e.Isolate.Specimen.Package.SenderId,
                                                        DateReceived = e.Isolate.Specimen.Package.DateReceived,
                                                        SpecimenCondition = e.Isolate.Specimen.Package.SpecimenCondition,
                                                        Sender = new SenderDTO()
                                                        {
                                                            Id = e.Isolate.Specimen.Package.Sender.Id,
                                                            RasClientId = e.Isolate.Specimen.Package.Sender.RasClientId,
                                                            CompanyName = e.Isolate.Specimen.Package.Sender.CompanyName,
                                                            Address = e.Isolate.Specimen.Package.Sender.Address,
                                                            City = e.Isolate.Specimen.Package.Sender.City,
                                                            State = e.Isolate.Specimen.Package.Sender.State,
                                                        }
                                                    },
                                                    PackageId = e.Isolate.Specimen.PackageId,
                                                    Country = new CountryDTO() { Id = e.Isolate.Specimen.Country.Id, Name = e.Isolate.Specimen.Country.Name, ShortCode = e.Isolate.Specimen.Country.ShortCode, LongCode = e.Isolate.Specimen.Country.LongCode },
                                                    CountryId = e.Isolate.Specimen.CountryId,
                                                    State = new StateDTO() { Id = e.Isolate.Specimen.State.Id, Name = e.Isolate.Specimen.State.Name, Abbreviation = e.Isolate.Specimen.State.Abbreviation },
                                                    StateId = e.Isolate.Specimen.StateId,
                                                    County = new CountyDTO() { Id = e.Isolate.Specimen.County.Id, Name = e.Isolate.Specimen.County.Name },
                                                    CountyId = e.Isolate.Specimen.CountyId,
                                                    Activity = e.Isolate.Specimen.Activity,
                                                    DateCollected = e.Isolate.Specimen.DateCollected,
                                                    SpecimenID = e.Isolate.Specimen.SpecimenID,
                                                    PatientAge = e.Isolate.Specimen.PatientAge,
                                                    PatientAgeUnits = e.Isolate.Specimen.PatientAgeUnits,
                                                    PatientSex = e.Isolate.Specimen.PatientSex,
                                                    PassageHistory = e.Isolate.Specimen.PassageHistory,
                                                    SubType = e.Isolate.Specimen.SubType,
                                                    SpecimenSource = e.Isolate.Specimen.SpecimenSource,
                                                    InitialStorage = e.Isolate.Specimen.InitialStorage,
                                                    CompCSID1 = e.Isolate.Specimen.CompCSID1,
                                                    CompCSID2 = e.Isolate.Specimen.CompCSID2,
                                                    OtherLocation = e.Isolate.Specimen.OtherLocation,
                                                    DrugResistant = e.Isolate.Specimen.DrugResistant,
                                                    Deceased = e.Isolate.Specimen.Deceased,
                                                    SpecialStudy = e.Isolate.Specimen.SpecialStudy,
                                                    NursingHome = e.Isolate.Specimen.NursingHome,
                                                    Vaccinated = e.Isolate.Specimen.Vaccinated,
                                                    Travel = e.Isolate.Specimen.Travel,
                                                    ReasonForSubmission = e.Isolate.Specimen.ReasonForSubmission,
                                                    Comments = e.Isolate.Specimen.Comments,
                                                    StatusId = e.Isolate.Specimen.StatusId,
                                                    Status = new StatusDTO() { Id = e.Isolate.Specimen.Status.Id, DisplayName = e.Isolate.Specimen.Status.DisplayName, SysName = e.Isolate.Specimen.Status.SysName },

                                                    TravelCountry = e.Isolate.Specimen.TravelCountry,
                                                    TravelState = e.Isolate.Specimen.TravelState,
                                                    TravelCounty = e.Isolate.Specimen.TravelCounty,
                                                    TravelCity = e.Isolate.Specimen.TravelCity,
                                                    TravelReturnDate = e.Isolate.Specimen.TravelReturnDate,
                                                    HostCellLine = e.Isolate.Specimen.HostCellLine,
                                                    Substrate = e.Isolate.Specimen.Substrate,
                                                    PassageNumber = e.Isolate.Specimen.PassageNumber,
                                                    PCRInfA = e.Isolate.Specimen.PCRInfA,
                                                    PCRpdmInfA = e.Isolate.Specimen.PCRpdmInfA,
                                                    PCRpdmH1 = e.Isolate.Specimen.PCRpdmH1,
                                                    PCRH3 = e.Isolate.Specimen.PCRH3,
                                                    PCRInfB = e.Isolate.Specimen.PCRInfB,
                                                    PCRBVic = e.Isolate.Specimen.PCRBVic,
                                                    PCRBYam = e.Isolate.Specimen.PCRBYam,
                                                    City = e.Isolate.Specimen.City,
                                                    WHOSubmittedSubtype = e.Isolate.Specimen.WHOSubmittedSubtype,
                                                }
                                            }
                                        })
                                        .Where(userFilter, _validIds)
                        //.Where(viewwhere, dto.Data);
                                        .Where(viewwhere, localIdList);
                    localAliquotList.AddRange(query.ToList());
                }

                // * CLAW - 53
                if (localAliquotList.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No samples to export"));
                }

                //var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, query.ToList(), 2);
                var outputfile = ExcelFileFactory.GenerateFile<AliquotDTO>(entity, localAliquotList, 7);
                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            }
        }


        public PagedResponseBase<IList<SenderCompositeDTO>> SenderCompositeList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";

            string fluseasonwhere = "Id > 0";
            if (request.Data.FluSeason.HasValue)
            {
                fluseasonwhere = "FluSeason = " + request.Data.FluSeason.ToString();
            }


            if (!String.IsNullOrEmpty(request.Data.IdFilter))
            {
                idwhere = String.Format(@"CompanyName.Contains(""{0}"")
                OR State.Equals(""{0}"")
                ", request.Data.IdFilter);
            }

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var query = session.Repository.Query<Sender>()
                .Where(idwhere)
                .Where(fluseasonwhere)
                .Select(e => new SenderCompositeDTO
                {
                    Sender = new SenderDTO()
                    {
                        Id = e.Id,
                        VersionColumn = e.VersionColumn,
                        CreatedBy = e.CreatedBy,
                        CreatedOn = e.CreatedOn,
                        ModifiedBy = e.ModifiedBy,
                        ModifiedOn = e.ModifiedOn,
                        Address = e.Address,
                        City = e.City,
                        CompanyName = e.CompanyName,
                        Country = e.Country,
                        RasClientId = e.RasClientId,
                        State = e.State,
                        FluSeason = e.FluSeason,
                    }
                }
                ).OrderBy(request.Data.OrderBy);
                int count = query.Count();
                List<SenderCompositeDTO> entity = new List<SenderCompositeDTO>(query.Skip<SenderCompositeDTO>(skip).Take<SenderCompositeDTO>(take));

                // * brute force
                foreach (var item in entity)
                {
                    var e = session.Repository.Query<Contact>(s => s.RasClientId == item.Sender.RasClientId).OrderBy(s => s.Id).FirstOrDefault();
                    if (e != null)
                    {
                        item.Contact = new ContactDTO()
                        {
                            Id = e.Id,
                            VersionColumn = e.VersionColumn,
                            CreatedBy = e.CreatedBy,
                            CreatedOn = e.CreatedOn,
                            ModifiedBy = e.ModifiedBy,
                            ModifiedOn = e.ModifiedOn,
                            Address = e.Address,
                            City = e.City,
                            Country = e.Country,
                            RasClientId = e.RasClientId,
                            State = e.State,
                            Email = e.Email,
                            Fax = e.Fax,
                            FullName = e.FullName,
                            Phone = e.Phone,
                            RasContactId = e.RasContactId,
                            FluSeason = e.FluSeason,
                        };
                    }
                }
                return PagedResponseBase<IList<SenderCompositeDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
            }
        }

        public ResponseBase SenderCompositeInsert(MessageBase<SenderCompositeDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Sender>().Where(s => s.RasClientId == dto.Data.Sender.RasClientId)
                    .FirstOrDefault();
                    if (entity != null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"RasClientId {0} already exists.", dto.Data.Sender.RasClientId)));
                    }
                    var contact = session.Repository.Query<Contact>().Where(s => s.RasContactId == dto.Data.Contact.RasContactId)
                    .FirstOrDefault();
                    if (contact != null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"RasContactId {0} already exists.", dto.Data.Contact.RasContactId)));
                    }
                    var newentity = new Sender()
                    {
                        Address = dto.Data.Sender.Address,
                        City = dto.Data.Sender.City,
                        CompanyName = dto.Data.Sender.CompanyName,
                        Country = dto.Data.Sender.Country,
                        CreatedBy = _user.UserId,
                        CreatedOn = DateTime.UtcNow,
                        RasClientId = dto.Data.Sender.RasClientId,
                        State = dto.Data.Sender.State,
                        ModifiedOn = DateTime.UtcNow,
                        ModifiedBy = _user.UserId,
                        FluSeason = _currentFluSeason,
                    };
                    session.Repository.Insert<Sender>(newentity);
                    var newcontact = new Contact()
                    {
                        Address = dto.Data.Contact.Address,
                        City = dto.Data.Contact.City,
                        Country = dto.Data.Contact.Country,
                        CreatedBy = _user.UserId,
                        CreatedOn = DateTime.UtcNow,
                        Email = dto.Data.Contact.Email,
                        Fax = dto.Data.Contact.Fax,
                        FullName = dto.Data.Contact.FullName,
                        Phone = dto.Data.Contact.Phone,
                        RasClientId = dto.Data.Contact.RasClientId,
                        RasContactId = dto.Data.Contact.RasContactId,
                        State = dto.Data.Contact.State,
                        ModifiedOn = DateTime.UtcNow,
                        ModifiedBy = _user.UserId,
                        FluSeason = _currentFluSeason,
                    };
                    session.Repository.Insert<Contact>(newcontact);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<SenderCompositeDTO> SenderComposite(MessageBase<string> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Sender>(s => s.RasClientId == id.Data)
                                .Select(e => new SenderCompositeDTO
                                {
                                    Sender = new SenderDTO()
                                    {
                                        Id = e.Id,
                                        VersionColumn = e.VersionColumn,
                                        CreatedBy = e.CreatedBy,
                                        CreatedOn = e.CreatedOn,
                                        ModifiedBy = e.ModifiedBy,
                                        ModifiedOn = e.ModifiedOn,
                                        Address = e.Address,
                                        City = e.City,
                                        CompanyName = e.CompanyName,
                                        Country = e.Country,
                                        RasClientId = e.RasClientId,
                                        State = e.State,
                                        FluSeason = e.FluSeason,
                                    }
                                }
                                )
                                .FirstOrDefault();
                // * brute force
                if (entity != null)
                {
                    var e = session.Repository.Query<Contact>(s => s.RasClientId == id.Data).OrderBy(s => s.Id).FirstOrDefault();
                    if (e != null)
                    {
                        entity.Contact = new ContactDTO()
                        {
                            Id = e.Id,
                            VersionColumn = e.VersionColumn,
                            CreatedBy = e.CreatedBy,
                            CreatedOn = e.CreatedOn,
                            ModifiedBy = e.ModifiedBy,
                            ModifiedOn = e.ModifiedOn,
                            Address = e.Address,
                            City = e.City,
                            Country = e.Country,
                            RasClientId = e.RasClientId,
                            State = e.State,
                            Email = e.Email,
                            Fax = e.Fax,
                            FullName = e.FullName,
                            Phone = e.Phone,
                            RasContactId = e.RasContactId,
                            FluSeason = e.FluSeason,
                        };
                    }
                }

                if (entity == null)
                {
                    return ResponseBase<SenderCompositeDTO>.CreateErrorResponse(new Exception(string.Format(@"RasClientId {0} not found.", id.Data)));
                }
                return ResponseBase<SenderCompositeDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase SenderCompositeDelete(MessageBase<string> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Sender>().Where(s => s.RasClientId == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"RasClientId {0} not found.", id.Data)));
                    }
                    else
                    {
                        var contacts = session.Repository.Query<Contact>(s => s.RasClientId == id.Data).ToArray<Contact>();
                        for (int i = contacts.Length - 1; i >= 0; i--)
                        {
                            session.Repository.Delete<Contact>(contacts[i]);
                        }
                        session.Repository.Delete<Sender>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase SenderCompositeEdit(MessageBase<SenderCompositeDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Sender>().Where(s => s.RasClientId == dto.Data.Sender.RasClientId)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Sender RasClientId {0} not found.", dto.Data.Sender.RasClientId)));
                    }
                    else
                    {
                        entity.ModifiedBy = _user.UserId;
                        entity.ModifiedOn = DateTime.UtcNow;
                        entity.Address = dto.Data.Sender.Address;
                        entity.City = dto.Data.Sender.City;
                        entity.CompanyName = dto.Data.Sender.CompanyName;
                        entity.Country = dto.Data.Sender.Country;
                        entity.State = dto.Data.Sender.State;
                        entity.FluSeason = dto.Data.Sender.FluSeason;
                        session.Repository.Update<Sender>(entity);
                    }
                    var contact = session.Repository.Query<Contact>().Where(s => s.RasClientId == dto.Data.Sender.RasClientId)
                    .FirstOrDefault();
                    if (contact == null)
                    {
                        var newcontact = new Contact()
                        {
                            Address = dto.Data.Contact.Address,
                            City = dto.Data.Contact.City,
                            Country = dto.Data.Contact.Country,
                            CreatedBy = _user.UserId,
                            CreatedOn = DateTime.UtcNow,
                            Email = dto.Data.Contact.Email,
                            Fax = dto.Data.Contact.Fax,
                            FullName = dto.Data.Contact.FullName,
                            Phone = dto.Data.Contact.Phone,
                            RasClientId = dto.Data.Sender.RasClientId,
                            RasContactId = dto.Data.Contact.RasContactId,
                            State = dto.Data.Contact.State,
                            FluSeason = dto.Data.Contact.FluSeason,
                        };
                        session.Repository.Insert<Contact>(newcontact);
                    }
                    else
                    {
                        contact.ModifiedBy = _user.UserId;
                        contact.ModifiedOn = DateTime.UtcNow;
                        contact.Address = dto.Data.Contact.Address;
                        contact.City = dto.Data.Contact.City;
                        contact.Country = dto.Data.Contact.Country;
                        contact.State = dto.Data.Contact.State;
                        contact.RasContactId = dto.Data.Contact.RasContactId;
                        contact.Email = dto.Data.Contact.Email;
                        contact.Fax = dto.Data.Contact.Fax;
                        contact.FullName = dto.Data.Contact.FullName;
                        contact.Phone = dto.Data.Contact.Phone;
                        contact.FluSeason = dto.Data.Contact.FluSeason;
                        session.Repository.Update<Contact>(contact);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase<WHOImportDTO> ValidateWHO(MessageBase<WHOImportDTO> dto)
        {
            var result = new WHOImportDTO();
            int ColumnStart = dto.Data.ColumnStart;
            int? ColumnEnd = dto.Data.ColumnEnd;
            int RowStart = dto.Data.RowStart;
            int? RowEnd = dto.Data.RowEnd;
            result.ColumnStart = ColumnStart;
            result.ColumnEnd = ColumnEnd;
            result.RowStart = RowStart;
            result.RowEnd = RowEnd;
            result.FileName = dto.Data.FileName;
            string[] PropertyOrder = dto.Data.PropertyOrder.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            //result.File = new byte[dto.Data.File.LongCount()];
            //Array.Copy(dto.Data.File, result.File, dto.Data.File.LongCount());
            //using (var stream = new MemoryStream(result.File))
            using (var stream = new MemoryStream(dto.Data.File))
            {
                using (var excel = new ExcelPackage(stream))
                {
                    var sheet = excel.Workbook.Worksheets[1];
                    var datarange = sheet.Cells[sheet.Dimension.Start.Row + RowStart
                        , sheet.Dimension.Start.Column + ColumnStart
                        , RowEnd.HasValue ? sheet.Dimension.Start.Row + RowEnd.Value : sheet.Dimension.End.Row
                        , ColumnEnd.HasValue ? sheet.Dimension.Start.Column +ColumnEnd.Value : sheet.Dimension.End.Column];
                    var transformer = new ExcelRowTransformer(typeof(WHOSpecimenDTO), PropertyOrder, ColumnStart);
                    result.WHOSpecimens = transformer.GetRows(datarange).ToList();
                }
            }
            var whospecimens = result.WHOSpecimens;
            List<SpecimenDTO> specimens;
            if (!MapWHOSpecimenDTO(ref whospecimens, out specimens))
            {
                // updated errors
                result.WHOSpecimens = whospecimens;
                result.IsError = true;
            }
            return ResponseBase<WHOImportDTO>.CreateSuccessResponse(result);
        }

        public ResponseBase ImportWHO(MessageBase<WHOImportDTO> dto)
        {
            {
                return ExecuteValidatedDataAction(() =>
                {
                    if (_lab == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("Cannot insert without a lab assigned."));
                    }

                    if (dto.Data.WHOSpecimens == null || dto.Data.WHOSpecimens.Count == 0)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("No specimens to insert."));
                    }

                    if (dto.Data.WHOSpecimens.Count(s => s.IsError) > 0)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("Not all specimens are valid."));
                    }

                    var whospecimens = dto.Data.WHOSpecimens;
                    List<SpecimenDTO> specimens;
                    if (!MapWHOSpecimenDTO(ref whospecimens, out specimens))
                    {
                        return ResponseBase.CreateErrorResponse(new Exception("Not all specimens are valid (mapping.)"));
                    }

                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {
                        var status = session.Repository.FindBy<Status>(s => s.SysName.Equals("INPROGRESS"), null);
                        if (status == null)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Status {0} not found.", "INPROGRESS")));
                        }

                        var isolateType = session.Repository.FindBy<IsolateType>(s => s.SysName.Equals("ORIGINAL"), null);
                        if (isolateType == null)
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"IsolateType {0} not found.", "ORIGINAL")));
                        }

                        foreach (var item in specimens)
                        {
                            item.CSID = String.Empty;
                            item.CUID = String.Empty;
                            SpecimenInsert_Internal(new MessageBase<SpecimenDTO>() { Data = item }, session, status, isolateType);                            
                        }

                        return ResponseBase.CreateSuccessResponse();
                    }
                });
            }
        }

        private bool MapWHOSpecimenDTO(ref List<WHOSpecimenDTO> whospecimens, out List<SpecimenDTO> specimens)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var countries = this.GetCountries().Data;
                var states = this.GetStates().Data;
                var counties = new List<CountyDTO>();
                //int? countryid = null;
                //if (countries != null)
                //{
                //    var tempC = countries.Where(s => s.Name == "United States").FirstOrDefault();
                //    countryid = tempC != null ? tempC.Id : null;
                //}
                //var usid = countries.Where(s => s.Name == "United States").FirstOrDefault();

                var translateProperties = session.Repository.GetAll<WHOTranslation>().ToList();

                var foo = ExcelRowTransformer.GetPropertyInfoRecursive(typeof(WHOSpecimenDTO));
                var props = (from kvp in foo
                             let attr = kvp.Value.GetCustomAttributes(typeof(HeadingAttribute), true)
                             where attr.Length == 1
                             select new { Type = kvp.Key, Property = kvp.Value, Attribute = attr.First() as HeadingAttribute }).ToList();

                translateProperties.RemoveAll(s => !props.Any(e => e.Attribute.ColumnHeaderText.Equals(s.Heading)));
                specimens = new List<SpecimenDTO>();
                foreach (var item in whospecimens)
                {
                    // * this method is really called twice in the workflow, only do this once:
                    if (string.IsNullOrEmpty(item.WhoSubmittedSubType))
                    {
                        item.WhoSubmittedSubType = item.SubType;
                        item.WhoSubmittedSubType_Error = item.SubType_Error;                        
                    }

                    #region translations
                    foreach (var prop in translateProperties)
                    {
                        var pi = props.Where(s => s.Attribute.ColumnHeaderText.Equals(prop.Heading)).FirstOrDefault();
                        if (pi != null)
	                    {
                            var oldvalue = pi.Property.GetValue(item);
                            if (oldvalue != null)
	                        {
                                var match = Regex.Match(oldvalue.ToString(), prop.MatchExpression);
                                if (match.Success)
                                {
                                    pi.Property.SetValue(item, Convert.ChangeType(prop.ReplacementValue ?? String.Empty, pi.Property.PropertyType));
                                    var piC = props.Where(s => s.Attribute.ColumnHeaderText.Equals(prop.CommentHeading)).FirstOrDefault();
                                    if (piC != null && !string.IsNullOrEmpty(prop.CommentValue))
                                    {
                                        var oldcomment = piC.Property.GetValue(item).ToString();
                                        piC.Property.SetValue(item, Convert.ChangeType(string.Format("{0} {1}", oldcomment, prop.CommentValue), pi.Property.PropertyType));
                                    }
                                }
	                        }
	                    }
                    }
                    #endregion

                    bool iserror = false;
                    String newvalue;
                    var specimen = new SpecimenDTO();

                    specimen.PassageHistory = item.PassageHistory;
                    specimen.City = item.City;
                    specimen.Comments = item.Comments;
                    specimen.DateCollected = item.DateCollected;

                    // * CLAW-131
                    //// * CLAW-116
                    //if (!specimen.DateCollected.HasValue)
                    //{
                    //    iserror = true;
                    //    item.DateCollected_Error = "Date Collected is required";
                    //}

                    #region item.Deceased;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.Deceased, "YNU_BLANK", out newvalue))
                    {
                        specimen.Deceased = newvalue;
                    }
                    else 
                    {
                        iserror = true;
                        item.Deceased_Error = newvalue;
                    }
                    #endregion  

                    #region item.NursingHome;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.NursingHome, "YNU_BLANK", out newvalue))
                    {
                        specimen.NursingHome = newvalue;
                    }
                    else
                    {
                        iserror = true;
                        item.NursingHome_Error = newvalue;
                    }
                    #endregion  

                    specimen.PackageId = item.PackageId;

                    #region patientage

                    if (!String.IsNullOrEmpty(item.PatientAge))
                    {
                        int tempAge = 0;
                        if (int.TryParse(item.PatientAge, out tempAge))
                        {
                            specimen.PatientAge = tempAge.ToString();
                        }
                        else
                        {
                            item.PatientAge_Error = @"Patient Age is not an integer";
                        }                        
                    }


                    if (!String.IsNullOrEmpty(item.PatientAgeUnits))
                    {
                        if (ValidateSelectListItem(item.PatientAgeUnits, "AGE_TYPE", out newvalue, true))
                        {
                            specimen.PatientAgeUnits = newvalue;
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(item.PatientAge))
                        {
                            // CLAW-39
                            newvalue = string.Empty;
                            if (ValidateSelectListItem("Y", "AGE_TYPE", out newvalue, true))
                            {
                                specimen.PatientAgeUnits = newvalue;
                            }
                        }
                    }

                    //if (!String.IsNullOrEmpty(item.PatientAge))
                    //{
                    //    var numAlpha = new Regex("(?<Numeric>[0-9]*)(?<Alpha>[a-zA-Z \0x0020]*)"); 
                    //    var match = numAlpha.Match(item.PatientAge);

                    //    var alpha = match.Groups["Alpha"].Value.Trim();
                    //    var num = match.Groups["Numeric"].Value;
                    //    if (String.IsNullOrEmpty(num))
                    //    {
                    //        iserror = true;
                    //        item.PatientAge_Error = @"Unalbe to determine a numeric value";
                    //    }
                    //    else
                    //    {
                    //        specimen.PatientAge = num;
                    //    }
                    //    if (String.IsNullOrEmpty(alpha))
                    //    {
                    //        newvalue = string.Empty;
                    //        if (!string.IsNullOrEmpty(item.PatientAgeUnits) && ValidateSelectListItem(item.PatientAgeUnits, "AGE_TYPE", out newvalue, true))
                    //        {
                    //                specimen.PatientAgeUnits = newvalue;
                    //        }
                    //        else
                    //        {
                    //            //iserror = true;
                    //            //item.PatientAge_Error = @"Unalbe to determine a age unit";
                    //            // CLAW-39
                    //            newvalue = string.Empty;
                    //            if (ValidateSelectListItem("Y", "AGE_TYPE", out newvalue, true))
                    //            {
                    //                specimen.PatientAgeUnits = newvalue;
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        newvalue = string.Empty;
                    //        if (ValidateSelectListItem(alpha, "AGE_TYPE", out newvalue, false))
                    //        {
                    //            specimen.PatientAgeUnits = newvalue;
                    //        }
                    //        else
                    //        {
                    //            newvalue = string.Empty;
                    //            if (ValidateSelectListItem(alpha[0].ToString(), "AGE_TYPE", out newvalue, true))
                    //            {
                    //                specimen.PatientAgeUnits = newvalue;
                    //            }
                    //            else
                    //            {
                    //                string[] monthtypes = new string[] { "mos", "mnths", "mnth", "mths", "mth" };
                    //                if (monthtypes.Contains(alpha[0].ToString().ToLower()))
                    //                {
                    //                    if (ValidateSelectListItem("Mon", "AGE_TYPE", out newvalue, true))
                    //                    {
                    //                        specimen.PatientAgeUnits = newvalue;
                    //                    }
                    //                    else
                    //                    {
                    //                        iserror = true;
                    //                        item.PatientAge_Error = newvalue;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //};

                    #endregion

                    #region item.PatientSex;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.PatientSex, "GNDR", out newvalue, true))
                    {
                        specimen.PatientSex = newvalue;
                    }
                    else
                    {
                        iserror = true;
                        item.PatientSex_Error = newvalue;
                    }
                    #endregion  

                    #region item.ReasonForSubmission;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.ReasonForSubmission, "SUBM_REAS", out newvalue))
                    {
                        specimen.ReasonForSubmission = newvalue;
                    }
                    else
                    {
                        iserror = true;
                        item.ReasonForSubmission_Error = newvalue;
                    }
                    #endregion

                    specimen.SpecimenID = item.SpecimenID;

                    #region item.SpecimenSource;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.SpecimenSource, "SPEC_SOURCE", out newvalue))
                    {
                        specimen.SpecimenSource = newvalue;
                    }
                    else
                    {
                        iserror = true;
                        item.SpecimenSource_Error = newvalue;
                    }
                    #endregion

                    #region CTChecks
                    // * FLULIMS 767 YCX0 06 JAN 2017
                    var validCt = new Regex(@"^(((?:\d*\.)?\d+)|NT|nt)$");
                    if (!string.IsNullOrEmpty(item.PCRBVic))
                    {
                        item.PCRBVic = item.PCRBVic.Trim();
                        var match = validCt.Match(item.PCRBVic);
                        if (match == null || !match.Success)
                        {
                            item.PCRBVic_Error = string.Format("Supplied PCRBVic is not valid: {0}", item.PCRBVic);
                            iserror = true;
                        } 
                    }
                    if (!string.IsNullOrEmpty(item.PCRBYam))
                    {
                        item.PCRBYam = item.PCRBYam.Trim();
                        var match = validCt.Match(item.PCRBYam);
                        if (match == null || !match.Success)
                        {
                            item.PCRBYam_Error = string.Format("Supplied PCRBYam is not valid: {0}", item.PCRBYam);
                            iserror = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.PCRH3))
                    {
                        item.PCRH3 = item.PCRH3.Trim();
                        var match = validCt.Match(item.PCRH3);
                        if (match == null || !match.Success)
                        {
                            item.PCRH3_Error = string.Format("Supplied PCRH3 is not valid: {0}", item.PCRH3);
                            iserror = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.PCRInfA))
                    {
                        item.PCRInfA = item.PCRInfA.Trim();
                        var match = validCt.Match(item.PCRInfA);
                        if (match == null || !match.Success)
                        {
                            item.PCRInfA_Error = string.Format("Supplied PCRInfA is not valid: {0}", item.PCRInfA);
                            iserror = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.PCRInfB))
                    {
                        item.PCRInfB = item.PCRInfB.Trim();
                        var match = validCt.Match(item.PCRInfB);
                        if (match == null || !match.Success)
                        {
                            item.PCRInfB_Error = string.Format("Supplied PCRInfB is not valid: {0}", item.PCRInfB);
                            iserror = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.PCRpdmH1))
                    {
                        item.PCRpdmH1 = item.PCRpdmH1.Trim();
                        var match = validCt.Match(item.PCRpdmH1);
                        if (match == null || !match.Success)
                        {
                            item.PCRpdmH1_Error = string.Format("Supplied PCRpdmH1 is not valid: {0}", item.PCRpdmH1);
                            iserror = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.PCRpdmInfA))
                    {
                        item.PCRpdmInfA = item.PCRpdmInfA.Trim();
                        var match = validCt.Match(item.PCRpdmInfA);
                        if (match == null || !match.Success)
                        {
                            item.PCRpdmInfA_Error = string.Format("Supplied PCRpdmInfA is not valid: {0}", item.PCRpdmInfA);
                            iserror = true;
                        }
                    }
                    #endregion

                    // handled below
                    //#region state
                    //if (!String.IsNullOrEmpty(item.State))
                    //{
                    //    //using (var session = new SingleDisposableRepositorySession(_contextSource))
                    //    {
                    //        var stateE = session.Repository.Query<State>(s => s.Abbreviation == item.State).FirstOrDefault();
                    //        if (stateE == null)
                    //        {
                    //            stateE = session.Repository.Query<State>(s => s.Name == item.State).FirstOrDefault();
                    //        }
                    //        if (stateE != null)
                    //        {
                    //            specimen.StateId = stateE.Id;
                    //            specimen.State = new StateDTO() { Id = stateE.Id, Abbreviation = stateE.Abbreviation, Name = stateE.Name };
                    //        }
                    //        else
                    //        {
                    //            iserror = true;
                    //            item.State_Error = string.Format(@"Value {0} not found for State.", item.State);;
                    //        }
                    //    }
		 
                    //}
                    //#endregion  

                    specimen.SubType = item.SubType;
                    #region item.Vaccinated;
                    newvalue = string.Empty;
                    if (ValidateSelectListItem(item.Vaccinated, "YNU_BLANK", out newvalue))
                    {
                        specimen.Vaccinated = newvalue;
                    }
                    else
                    {
                        iserror = true;
                        item.Vaccinated_Error = newvalue;
                    }
                    #endregion  


                    int? tempcountryid = null;
                    // * they switched to short code 23 SETP 2016
                    var tempcountry =  countries.FirstOrDefault(s => s.ShortCode == item.Country);
                    if (tempcountry == null)
                    {
                        tempcountry =  countries.FirstOrDefault(s => s.LongCode == item.Country);
                    }
                    tempcountryid = tempcountry == null ? null : tempcountry.Id;

                    int? tempstateid = null;
                    var tempstate = states.FirstOrDefault(s => s.Abbreviation == item.State);
                    tempstateid = tempstate == null ? null : tempstate.Id;
                    specimen.CountryId = tempcountryid;

                    // * CLAW-122
                    if (!specimen.CountryId.HasValue)
                    {
                        iserror = true;
                        item.Country_Error = "Country is required";
                    }

                    specimen.StateId = tempstateid;
                    int? tempcountyid = null;
                    if (specimen.StateId != null)
                    {
                        var tempcounty = counties.FirstOrDefault(s => s.Name == item.County);
                        if (tempcounty == null)
                        {
                            var tempcounties = this.GetCounties(new MessageBase<int>() { Data = specimen.StateId.Value }).Data;
                            if (tempcounties != null)
                            {
                                counties.AddRange(tempcounties);
                                tempcounty = counties.FirstOrDefault(s => s.Name == item.County);
                                tempcountyid = tempcounty == null ? null : tempcounty.Id;
                            }
                        }
                        else
                        {
                            tempcountyid = tempcounty == null ? null : tempcounty.Id;
                        }
                    }
                    specimen.CountyId = tempcountyid;

                    specimen.City = item.City;
                    specimen.PCRInfA = item.PCRInfA;
                    specimen.PCRpdmInfA = item.PCRpdmInfA;
                    specimen.PCRpdmH1 = item.PCRpdmH1;
                    specimen.PCRH3 = item.PCRH3;
                    specimen.PCRInfB = item.PCRInfB;
                    specimen.PCRBVic = item.PCRBVic;
                    specimen.PCRBYam = item.PCRBYam;
                    //handled above
                    //specimen.PatientAgeUnits = item.PatientAgeUnits;

                    specimen.TravelCountry = item.TravelCountry;
                    specimen.TravelState = item.TravelState;
                    specimen.TravelCounty = item.TravelCounty;
                    specimen.TravelCity = item.TravelCity;
                    specimen.TravelReturnDate = item.TravelReturnDate;
                    specimen.HostCellLine = item.HostCellLine;
                    specimen.Substrate = item.Substrate;
                    specimen.PassageNumber = item.PassageNumber;

                    specimen.WHOSubmittedSubtype = item.WhoSubmittedSubType;

                    if (!iserror)
                    {
                        specimens.Add(specimen);
                    }
                    else
                    {
                        item.IsError = iserror;
                    }
                }
                return whospecimens.Count == specimens.Count;
            }
        }

        private bool ValidateSelectListItem(string value, string category, out string returnvalue, bool startswith = false)
        {
            returnvalue = string.Empty;

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var categoryE = session.Repository.Query<Category>().Where(s => s.LitsQuestionId == category).FirstOrDefault();
                if (categoryE == null)
                {
                    returnvalue = string.Format(@"Category {0} not found.", category);
                    return false;
                }

                if (!categoryE.AllowBlank && String.IsNullOrEmpty(value))
                {
                    returnvalue = string.Format(@"Category {0} is required.", category);
                    return false;
                }

                if (categoryE.AllowBlank && String.IsNullOrEmpty(value))
                {
                    returnvalue = String.Empty;
                    return true;
                }

                var entity = session.Repository.Query<Synonym>().Where(s => s.Status.Equals("Active") && s.CategoryId == categoryE.Id).OrderBy(s => s.Condition.Sorter)
                .Select(e => e.Name).ToList();
                foreach (var item in entity)
                {
                    if (string.Equals(item, value.ToUpperInvariant(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        returnvalue = item;
                        return true;
                    }
                    else if (startswith && item.StartsWith(value.ToUpperInvariant(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        returnvalue = item;
                        return true;                        
                    }
                }
                returnvalue = string.Format(@"Value {0} not found for category {1}.", value, category);
                return false;
            } 
        }


        public ResponseBase WHOTranslationInsert(MessageBase<WHOTranslationDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot create WHO Translations."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new WHOTranslation()
                    {
                        CommentHeading = dto.Data.CommentHeading,
                        CommentValue = dto.Data.CommentValue,
                        Heading = dto.Data.Heading,
                        MatchExpression = dto.Data.MatchExpression,
                        ModifiedBy = _user.UserId,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.UserId,
                        CreatedOn = DateTime.UtcNow,
                        ReplacementValue = dto.Data.ReplacementValue,
                    };
                    session.Repository.Insert<WHOTranslation>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<WHOTranslationDTO> WHOTranslation(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<WHOTranslation>(s => s.Id == id.Data)
                .Select(e => new WHOTranslationDTO
                {
                    CommentHeading = e.CommentHeading,
                    CommentValue = e.CommentValue,
                    CreatedBy = e.CreatedBy,
                    CreatedOn = e.CreatedOn,
                    Heading = e.Heading,
                    Id = e.Id,
                    MatchExpression = e.MatchExpression,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    ReplacementValue = e.ReplacementValue,
                }
                )
                .FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase<WHOTranslationDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<WHOTranslationDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase WHOTranslationDelete(MessageBase<int> id)
        {
            if (!_user.User.IsInRole("Admin"))
            {
                return ResponseBase.CreateErrorResponse(new Exception("Cannot delete WHO Translations."));
            }
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<WHOTranslation>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<WHOTranslation>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase WHOTranslationEdit(MessageBase<WHOTranslationDTO> dto)
        {
            if (!_user.User.IsInRole("Admin"))
            {
                return ResponseBase.CreateErrorResponse(new Exception("Cannot edit WHO Translations."));
            }
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<WHOTranslation>().Where(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.CommentHeading = dto.Data.CommentHeading;
                        entity.CommentValue = dto.Data.CommentValue;
                        entity.Heading = dto.Data.Heading;
                        entity.MatchExpression = dto.Data.MatchExpression;
                        entity.ModifiedBy = _user.UserId;
                        entity.ModifiedOn = DateTime.UtcNow;
                        entity.ReplacementValue = dto.Data.ReplacementValue;
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<WHOTranslationDTO>> WHOTranslationList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<WHOTranslation>().OrderBy(s => s.Id)
                .Select(e => new WHOTranslationDTO() 
                    {
                        CommentHeading = e.CommentHeading,
                        CommentValue = e.CommentValue,
                        CreatedBy = e.CreatedBy,
                        CreatedOn = e.CreatedOn,
                        Heading = e.Heading,
                        Id = e.Id,
                        MatchExpression = e.MatchExpression,
                        ModifiedBy = e.ModifiedBy,
                        ModifiedOn = e.ModifiedOn,
                        ReplacementValue = e.ReplacementValue,
                    }
                ).ToList();
                return ResponseBase<IList<WHOTranslationDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase<VersionInfoDTO> VersionInfo()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<VersionInfo>().OrderByDescending(s => s.Version).Select(e => new VersionInfoDTO()
                {
                    AppliedOn = e.AppliedOn,
                    Description = e.Description,
                    Version = e.Version
                }).FirstOrDefault();
                return ResponseBase<VersionInfoDTO>.CreateSuccessResponse(entity);
            }
        }


        public PagedResponseBase<IList<CategoryDTO>> CategoryList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";

            if (!String.IsNullOrEmpty(request.Data.IdFilter))
            {
                idwhere = String.Format(@"LitsQuestionId.Contains(""{0}"")
                 OR Name.Contains(""{0}"")
                 OR Description.Contains(""{0}"")
                ", request.Data.IdFilter);
            }

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                IQueryable<CategoryDTO> query = session.Repository.Query<Category>()
                .Where(userFilter, _validIds)
                .Where(idwhere)
                .Select(e => new CategoryDTO
                {
                    AllowBlank = e.AllowBlank,
                    ConditionsCount = e.Conditions.Count,
                    Description = e.Description,
                    Id = e.Id,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    SynonymsCount = e.Synonyms.Count,
                }
                ).OrderBy(request.Data.OrderBy);
                int count = query.Count();
                List<CategoryDTO> entity = new List<CategoryDTO>(query.Skip<CategoryDTO>(skip).Take<CategoryDTO>(take));
                return PagedResponseBase<IList<CategoryDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
            }
        }

        public ResponseBase CategoryDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot delete Categories."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Category>(s => s.Id == id.Data, new List<string>() { "Conditions", "Conditions.Synonyms" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        var arrayConditions = entity.Conditions.ToArray<Condition>();
                        for (int i = arrayConditions.Length - 1; i >= 0; i--)
                        {
                            var condition = arrayConditions[i];
                            var arraySynonym = condition.Synonyms.ToArray<Synonym>();
                            for (int j = arraySynonym.Length - 1; j >= 0; j--)
                            {
                                var synonym = arraySynonym[j];
                                session.Repository.Delete<Synonym>(synonym);
                            }
                            session.Repository.Delete<Condition>(condition);
                        }
                        session.Repository.Delete<Category>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<ConditionDTO>> ConditionList(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Condition>(s => s.CategoryId == id.Data, new List<string>() { "Category" })
                .Select(e => new ConditionDTO()
                {
                    CategoryId = e.CategoryId,
                    Category = new CategoryDTO() { Id = e.Category.Id, AllowBlank = e.Category.AllowBlank, LitsQuestionId = e.Category.LitsQuestionId, Name = e.Category.Name },
                    Description = e.Description,
                    Id = e.Id,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    Sorter = e.Sorter,
                    Status = e.Status,
                    SynonymsCount = e.Synonyms.Count,
                }
                ).ToList();
                return ResponseBase<IList<ConditionDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase ConditionDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot delete Conditions."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Condition>(s => s.Id == id.Data, new List<string>() { "Synonyms" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        var arraySynonym = entity.Synonyms.ToArray<Synonym>();
                        for (int i = arraySynonym.Length - 1; i >= 0; i--)
                        {
                            var synonym = arraySynonym[i];
                            session.Repository.Delete<Synonym>(synonym);
                        }
                        session.Repository.Delete<Condition>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<SynonymDTO>> SynonymList(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Synonym>(s => s.ConditionId == id.Data)
                .Select(e => new SynonymDTO()
                {
                    Id = e.Id,
                    CategoryId = e.CategoryId,
                    ConditionId = e.ConditionId,
                    ConditionName = e.ConditionName,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    Status = e.Status,
                }
                ).ToList();
                return ResponseBase<IList<SynonymDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase SynonymDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot delete Synonyms."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Synonym>(s => s.Id == id.Data)
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Synonym>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase<CategoryDTO> Category(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Category>(s => s.Id == id.Data)
                .Select(e => new CategoryDTO
                {
                    AllowBlank = e.AllowBlank,
                    ConditionsCount = e.Conditions.Count,
                    Description = e.Description,
                    Id = e.Id,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    SynonymsCount = e.Synonyms.Count,
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<CategoryDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<CategoryDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase<ConditionDTO> Condition(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Condition>(s => s.Id == id.Data, new List<string>() { "Category" })
                .Select(e => new ConditionDTO
                {
                    CategoryId = e.CategoryId,
                    Category = new CategoryDTO() { Id = e.Category.Id, AllowBlank = e.Category.AllowBlank, LitsQuestionId = e.Category.LitsQuestionId, Name = e.Category.Name },
                    Description = e.Description,
                    Id = e.Id,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    Sorter = e.Sorter,
                    Status = e.Status,
                    SynonymsCount = e.Synonyms.Count,
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<ConditionDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<ConditionDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase<SynonymDTO> Synonym(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Synonym>(s => s.Id == id.Data)
                .Select(e => new SynonymDTO
                {
                    Id = e.Id,
                    CategoryId = e.CategoryId,
                    ConditionId = e.ConditionId,
                    ConditionName = e.ConditionName,
                    LitsQuestionId = e.LitsQuestionId,
                    Name = e.Name,
                    Status = e.Status,
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<SynonymDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<SynonymDTO>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase<IList<SenderDTO>> GetSenders(MessageBase<SenderDTO> dto)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var items = new List<SenderDTO>();
                items = session.Repository.Query<Sender>().Where(s => s.State.Equals(dto.Data.State) && (s.FluSeason == _currentFluSeason || s.Id == dto.Data.Id)).Select(e => new SenderDTO()
                {
                    Id = e.Id,
                    Address = e.Address,
                    City = e.City,
                    CompanyName = e.CompanyName,
                    Country = e.Country,
                    RasClientId = e.RasClientId,
                    State = e.State,
                }).ToList();
                return ResponseBase<IList<SenderDTO>>.CreateSuccessResponse(items);
            }    
        }


        public ResponseBase<IList<AspNetUserDTO>> UserList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<AspNetUser>()
                .Select(e => new AspNetUserDTO
                {
                    Id = e.Id,
                    AccessFailedCount = e.AccessFailedCount,
                    Email = e.Email,
                    EmailConfirmed = e.EmailConfirmed,
                    Lab = e.Lab != null ? new LabDTO () {Id = e.Lab.Id, Name = e.Lab.Name} : null,
                    LabId = e.LabId,
                    LockoutEnabled = e.LockoutEnabled,
                    LockoutEndDateUtc = e.LockoutEndDateUtc,
                    PasswordHash = e.PasswordHash,
                    PhoneNumber = e.PhoneNumber,
                    PhoneNumberConfirmed = e.PhoneNumberConfirmed,
                    SecurityStamp = e.SecurityStamp,
                    TwoFactorEnabled = e.TwoFactorEnabled,
                    UserName = e.UserName,
                    DateDisabled = e.DateDisabled,
                }
                )
                .ToList();
                foreach (var dto in entity)
                {
                    var e = session.Repository.Query<AspNetUser>(s => s.Id == dto.Id, new List<string>() { "AspNetRoles" }).FirstOrDefault();
                    if (e.AspNetRoles != null)
	                {
                        dto.AspNetRoles = e.AspNetRoles.ToDTOList();    
	                }
                }
                return ResponseBase<IList<AspNetUserDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase UserInsert(MessageBase<AspNetUserDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new AspNetUser()
                    {
                        AccessFailedCount = dto.Data.AccessFailedCount,
                        Email = dto.Data.Email,
                        EmailConfirmed = dto.Data.EmailConfirmed,
                        LabId = dto.Data.LabId,
                        LockoutEnabled = dto.Data.LockoutEnabled,
                        LockoutEndDateUtc = dto.Data.LockoutEndDateUtc,
                        PhoneNumber = dto.Data.PhoneNumber,
                        PhoneNumberConfirmed = dto.Data.PhoneNumberConfirmed,
                        TwoFactorEnabled = dto.Data.TwoFactorEnabled,
                        UserName = dto.Data.UserName,
                    };
                    session.Repository.Insert<AspNetUser>(newentity);
                    foreach (var item in dto.Data.AspNetRoles)
                    {
                        var role = session.Repository.Find<AspNetRole>(item.Id);
                        if (role != null)
                        {
                            newentity.AspNetRoles.Add(role);
                        }
                    }
                    session.Repository.Update<AspNetUser>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<AspNetUserDTO> User(MessageBase<string> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<AspNetUser>().Where(s => s.Id == id.Data)
                .Select(e => new AspNetUserDTO
                {
                    Id = e.Id,
                    AccessFailedCount = e.AccessFailedCount,
                    Email = e.Email,
                    EmailConfirmed = e.EmailConfirmed,
                    Lab = e.Lab != null ? new LabDTO() { Id = e.Lab.Id, Name = e.Lab.Name } : null,
                    LabId = e.LabId,
                    LockoutEnabled = e.LockoutEnabled,
                    LockoutEndDateUtc = e.LockoutEndDateUtc,
                    PasswordHash = e.PasswordHash,
                    PhoneNumber = e.PhoneNumber,
                    PhoneNumberConfirmed = e.PhoneNumberConfirmed,
                    SecurityStamp = e.SecurityStamp,
                    TwoFactorEnabled = e.TwoFactorEnabled,
                    UserName = e.UserName,
                    DateDisabled = e.DateDisabled,
                }
                )
                .FirstOrDefault();
                if (entity == null)
                {
                    return ResponseBase<AspNetUserDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                var temp = session.Repository.Query<AspNetUser>(s => s.Id == entity.Id, new List<string>() { "AspNetRoles" }).FirstOrDefault();
                if (temp.AspNetRoles != null)
                {
                    entity.AspNetRoles = temp.AspNetRoles.ToDTOList();
                }
                return ResponseBase<AspNetUserDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase UserDelete(MessageBase<string> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<AspNetUser>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<AspNetUser>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase UserEdit(MessageBase<AspNetUserDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<AspNetUser>().Where(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.AccessFailedCount = dto.Data.AccessFailedCount;
                        entity.Email = dto.Data.Email;
                        entity.EmailConfirmed = dto.Data.EmailConfirmed;
                        entity.LabId = dto.Data.LabId;
                        entity.LockoutEnabled = dto.Data.LockoutEnabled;
                        entity.LockoutEndDateUtc = dto.Data.LockoutEndDateUtc;
                        entity.PhoneNumber = dto.Data.PhoneNumber;
                        entity.PhoneNumberConfirmed = dto.Data.PhoneNumberConfirmed;
                        entity.TwoFactorEnabled = dto.Data.TwoFactorEnabled;
                        entity.UserName = dto.Data.UserName;

                        entity.AspNetRoles.Clear();

                        session.Repository.Update<AspNetUser>(entity);
                        
                        foreach (var item in dto.Data.AspNetRoles)
                        {
                            var role = session.Repository.Find<AspNetRole>(item.Id);
                            if (role != null)
                            {
                                entity.AspNetRoles.Add(role);
                            }
                        }

                        session.Repository.Update<AspNetUser>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<AspNetRoleDTO>> RoleList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var idwhere = "Id > 0";
                if (_lab != null)
                {
                    idwhere = String.Format(@"Id == {0}", _lab.Id);
                }
                var entity = session.Repository.Query<AspNetRole>()
                .Select(e => new AspNetRoleDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    AspNetUsers = e.AspNetUsers.ToDTOList(),
                }
                )
                .Where(idwhere)
                .ToList();
                return ResponseBase<IList<AspNetRoleDTO>>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase RoleInsert(MessageBase<AspNetRoleDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new AspNetRole()
                    {
                        Name = dto.Data.Name,
                    };
                    session.Repository.Insert<AspNetRole>(newentity);

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<AspNetRoleDTO> Role(MessageBase<string> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<AspNetRole>().Where(s => s.Id == id.Data)
                .Select(e => new AspNetRoleDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    AspNetUsers = e.AspNetUsers.ToDTOList(),
                }
                )
                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<AspNetRoleDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<AspNetRoleDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase RoleDelete(MessageBase<string> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<AspNetRole>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        var arrayUsers = entity.AspNetUsers.ToArray<AspNetUser>();
                        for (int i = arrayUsers.Length - 1; i >= 0; i--)
                        {
                            entity.AspNetUsers.Remove(arrayUsers[i]);
                        }
                        session.Repository.Update<AspNetRole>(entity);
                        session.Repository.Delete<AspNetRole>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase RoleEdit(MessageBase<AspNetRoleDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {

                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<AspNetRole>().Where(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        session.Repository.Update<AspNetRole>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public PagedResponseBase<IList<TransmissionDTO>> TransmissionList(MessageBase<RequestDetail> request)
        {
            int skip = request.Data.ItemsPerPage * (request.Data.CurrentPage - 1);
            int take = request.Data.ItemsPerPage;
            string idwhere = "Id > 0";

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var query = session.Repository.Query<Transmission>()
                .Where(idwhere)
                .Select(e => new TransmissionDTO
                {
                    Id = e.Id,
                    Payload = e.Payload,
                    Response = e.Response,
                    Signature = e.Signature,
                    CreatedBy = _user.UserId,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = _user.UserId,
                    SpecimenId = e.SpecimenId,
                    Specimen = new SpecimenDTO() { CSID = e.Specimen.CSID },
                    Isolate = e.IsolateId.HasValue ? new IsolateDTO(){ Id = e.Isolate.Id, IsolateType = new IsolateTypeDTO(){ DisplayName = e.Isolate.IsolateType.DisplayName }} : null,
                    Aliquot = e.AliquotId.HasValue ? new AliquotDTO(){ CUID = e.Aliquot.CUID } : null,
                    TransmittedOn = e.TransmittedOn,
                    Attempts = e.Attempts,
                    SupercedeId = e.SupercedeId,
                }
                ).OrderBy(request.Data.OrderBy);
                int count = query.Count();
                List<TransmissionDTO> entity = new List<TransmissionDTO>(query.Skip<TransmissionDTO>(skip).Take<TransmissionDTO>(take));

                return PagedResponseBase<IList<TransmissionDTO>>.CreateSuccessResponse(entity, count, request.Data.CurrentPage, request.Data.ItemsPerPage);
            }
        }

        public ResponseBase TransmissionInsert(MessageBase<TransmissionDTO> dto)
        {
            return ResponseBase.CreateErrorResponse(new Exception("Not implemented"));
            //return ExecuteValidatedDataAction(() =>
            //{
            //    using (var session = new SingleDisposableRepositorySession(_contextSource))
            //    {
            //        var newentity = new Transmission()
            //        {
            //            Payload = dto.Data.Payload,
            //            Signature = dto.Data.Signature,
                        
            //            CreatedBy = _user.UserId,
            //            CreatedOn = DateTime.UtcNow,
            //            ModifiedOn = DateTime.UtcNow,
            //            ModifiedBy = _user.UserId,
            //        };
            //        session.Repository.Insert<Transmission>(newentity);
            //        return ResponseBase.CreateSuccessResponse();
            //    }
            //});
        }

        public ResponseBase<TransmissionDTO> Transmission(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<Transmission>(s => s.Id == id.Data)
                                .Select(e => new TransmissionDTO
                                {
                                    Id = e.Id,
                                    Payload = e.Payload,
                                    Response = e.Response,
                                    Signature = e.Signature,
                                    CreatedBy = _user.UserId,
                                    CreatedOn = DateTime.UtcNow,
                                    ModifiedOn = DateTime.UtcNow,
                                    ModifiedBy = _user.UserId,
                                    SpecimenId = e.SpecimenId,
                                    Specimen = new SpecimenDTO() { CSID = e.Specimen.CSID },
                                    Isolate = e.IsolateId.HasValue ? new IsolateDTO() { Id = e.Isolate.Id, IsolateType = new IsolateTypeDTO() { DisplayName = e.Isolate.IsolateType.DisplayName } } : null,
                                    Aliquot = e.AliquotId.HasValue ? new AliquotDTO() { CUID = e.Aliquot.CUID } : null,
                                    TransmittedOn = e.TransmittedOn,
                                    Attempts = e.Attempts,
                                    SupercedeId = e.SupercedeId,
                                }
                                )
                                .FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<TransmissionDTO>.CreateErrorResponse(new Exception(string.Format(@"TransmissionId {0} not found.", id.Data)));
                }
                return ResponseBase<TransmissionDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase TransmissionDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Transmission>().Where(s => s.Id == id.Data)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"TransmissionId {0} not found.", id.Data)));
                    }
                    else
                    {
                        session.Repository.Delete<Transmission>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase TransmissionEdit(MessageBase<TransmissionDTO> dto)
        {
            return ResponseBase.CreateErrorResponse(new Exception("Not implemented"));
            //return ExecuteValidatedDataAction(() =>
            //{

            //    using (var session = new SingleDisposableRepositorySession(_contextSource))
            //    {
            //        var entity = session.Repository.Query<Transmission>().Where(s => s.Id == dto.Data.Id)
            //        .FirstOrDefault();
            //        if (entity == null)
            //        {
            //            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Transmission Id {0} not found.", dto.Data.Id)));
            //        }
            //        else
            //        {
            //            entity.Payload = dto.Data.Payload;
            //            entity.Response = dto.Data.Response;
            //            entity.Signature = dto.Data.Signature;
            //            entity.ModifiedBy = _user.UserId;
            //            entity.ModifiedOn = DateTime.UtcNow;
            //            session.Repository.Update<Transmission>(entity);
            //        }
            //        return ResponseBase.CreateSuccessResponse();
            //    }
            //});
        }

        private List<Transmission> GenerateTransmissionFile_Internal(List<int> ids)
        {
            var localTransmissionList = new List<Transmission>();

            var outputfile = new TemplateDTO();
            var pagesize = 80;
            var pages = ids.Count / pagesize;
            if (ids.Count % pagesize > 0 || pages == 0)
            {
                pages++;
            }
                

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                for (int i = 0; i < pages; i++)
                {
                    var localIdList = ids.Skip(i * pagesize).Take(pagesize);
                    var query = session.Repository.Query<Transmission>(s => localIdList.Contains(s.Id));
                    localTransmissionList.AddRange(query.ToList());
                }

                localTransmissionList.Sort();
                Transmission superT = new Transmission();
                for (int i = 0; i < localTransmissionList.Count; i++)
                {
                    if (!superT.Equals(localTransmissionList[i]))
                    {
                        localTransmissionList[i].TransmittedOn = DateTime.UtcNow;
                        superT = localTransmissionList[i];
                    }
                    else
                    {
                        localTransmissionList[i].SupercedeId = superT.Id;
                    }
                    localTransmissionList[i].Attempts = localTransmissionList[i].Attempts.HasValue ? localTransmissionList[i].Attempts + 1 : 1;
                    localTransmissionList[i].ModifiedOn = DateTime.UtcNow;
                    localTransmissionList[i].ModifiedBy = _user.UserId;
                }
                foreach (var item in localTransmissionList)
                {
                    session.Repository.Update<Transmission>(item);
                }
            }
            return localTransmissionList;
        }

        public ResponseBase GenerateTransmissionFile_Background(MessageBase<int> size, MessageBase<string> path)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var query = session.Repository.Query<Transmission>(s => !s.TransmittedOn.HasValue && !s.SupercedeId.HasValue).Select(s => s.Id);
                    var localTransmissionList = GenerateTransmissionFile_Internal(query.ToList());

                    var itemsforfile = localTransmissionList.Where(s => !s.SupercedeId.HasValue).ToList();

                    var pagesize = size.Data;
                    var pages = itemsforfile.Count / pagesize;
                    if (itemsforfile.Count % pagesize > 0 || pages == 0)
                    {
                        pages++;
                    }

                    for (int i = 0; i < pages; i++)
                    {
                        var localitemsforfile = itemsforfile.Skip(i * pagesize).Take(pagesize);
                        Thread.Sleep(1);
                        var filepath = Path.Combine(path.Data, DateTime.UtcNow.ToFileTimeUtc().ToString() + ".json");
                        var file = System.Text.Encoding.UTF8.GetBytes("[" + String.Join(",", localTransmissionList.Where(s => !s.SupercedeId.HasValue).Select(s => s.Payload).ToArray()) + "]");
                        System.IO.File.WriteAllBytes(filepath, file);                          
                    }                

                    return ResponseBase<int>.CreateErrorResponse(new NotImplementedException());
                }
            });
        }

        public ResponseBase<TemplateDTO> GenerateTransmissionFile(MessageBase<List<int>> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                var localTransmissionList = GenerateTransmissionFile_Internal(dto.Data);

                // * CLAW - 53
                if (localTransmissionList.Count == 0)
                {
                    return ResponseBase<TemplateDTO>.CreateErrorResponse(new Exception("No records to transmit"));
                }

                var outputfile = new TemplateDTO()
                {
                    TemplateType = string.Empty,
                    FileName = DateTime.UtcNow.ToFileTimeUtc().ToString() + ".json",
                    File = System.Text.Encoding.UTF8.GetBytes("[" + String.Join(",", localTransmissionList.Where(s => !s.SupercedeId.HasValue).Select(s => s.Payload).ToArray()) + "]")
                };

                return ResponseBase<TemplateDTO>.CreateSuccessResponse(outputfile);
            });
        }

        public ResponseBase<IList<NameValueDTO>> GetRemainingCounts(MessageBase<long> csid, MessageBase<string> cuid)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var result = new List<NameValueDTO>();
                var currentcsid = session.Repository.Query<Specimen>().OrderByDescending(s => s.CSID).FirstOrDefault();
                if (currentcsid != null)
                {
                    long temp = 0;
                    long.TryParse(currentcsid.CSID, out temp);
                    result.Add(new NameValueDTO() { Name = "CSID", Value = String.Format(@"{0} ({1})", csid.Data - temp, currentcsid.CSID) });
                }
                var currentcuid = session.Repository.Query<Aliquot>().OrderByDescending(s => s.CUID).FirstOrDefault();
                if (currentcuid != null)
                {
                    long currentlong = currentcuid.CUID.Decode36();
                    long maxlong = cuid.Data.Decode36();
                    result.Add(new NameValueDTO() { Name = "CUID", Value = String.Format(@"{0} ({1})", maxlong - currentlong, currentcuid.CUID) });
                }
                return ResponseBase<IList<NameValueDTO>>.CreateSuccessResponse(result);
            }
        }

        public ResponseBase<IList<SpecimenNoteDTO>> SpecimenNoteList(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<SpecimenNote>(s => s.SpecimenId == id.Data, new List<string>() { "NoteCategory", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new SpecimenNoteDTO
                {

                    Id = e.Id,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenId = e.SpecimenId,
                    FileName = e.FileName,
                    Note = e.Note,
                    Specimen = new SpecimenDTO { Id = e.Specimen.Id, CSID = e.Specimen.CSID, SpecimenID = e.Specimen.SpecimenID, StatusId = e.Specimen.StatusId, Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName } },
                    NoteCategoryId = e.NoteCategoryId,
                    NoteCategory = new NoteCategoryDTO() { Id = e.NoteCategory.Id, DisplayName = e.NoteCategory.DisplayName, SysName = e.NoteCategory.SysName },

                }
                ).ToList();
                return ResponseBase<IList<SpecimenNoteDTO>>.CreateSuccessResponse(entity);
            }
        }


        public ResponseBase SpecimenNoteInsert(MessageBase<SpecimenNoteDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {

                    var newentity = new SpecimenNote()
                    {
                        SpecimenId = dto.Data.SpecimenId,
                        FileName = dto.Data.FileName,
                        File = dto.Data.File,
                        Note = dto.Data.Note,
                        NoteCategoryId = dto.Data.NoteCategoryId,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        CreatedBy = _user.Identity.GetUserId(),
                        ModifiedBy = _user.Identity.GetUserId()
                    };
                    session.Repository.Insert<SpecimenNote>(newentity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase SpecimenNoteDelete(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimen Notes."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<SpecimenNote>(s => s.Id == id.Data, new List<string>() { "NoteCategory", "Specimen.Status" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else
                    {
                        if (!entity.Specimen.Status.SysName.Equals("INPROGRESS") && !_user.User.IsInRole("Lab Editor"))
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot delete Specimen Notes from a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                        }
                        session.Repository.Delete<SpecimenNote>(entity);
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<SpecimenNoteDTO> SpecimenNote(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<SpecimenNote>(s => s.Id == id.Data, new List<string>() { "NoteCategory", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new SpecimenNoteDTO
                {

                    Id = e.Id,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenId = e.SpecimenId,
                    FileName = e.FileName,
                    Note = e.Note,
                    Specimen = new SpecimenDTO { Id = e.Specimen.Id, CSID = e.Specimen.CSID, SpecimenID = e.Specimen.SpecimenID, StatusId = e.Specimen.StatusId, Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName } },
                    NoteCategoryId = e.NoteCategoryId,
                    NoteCategory = new NoteCategoryDTO() { Id = e.NoteCategory.Id, DisplayName = e.NoteCategory.DisplayName, SysName = e.NoteCategory.SysName },

                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<SpecimenNoteDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<SpecimenNoteDTO>.CreateSuccessResponse(entity);
            }
        }

        public ResponseBase<SpecimenNoteDTO> SpecimenNoteFile(MessageBase<int> id)
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<SpecimenNote>(s => s.Id == id.Data, new List<string>() { "NoteCategory", "Specimen", "Specimen.Status" })
                .Where(userFilter, _validIds)
                .Select(e => new SpecimenNoteDTO
                {

                    Id = e.Id,
                    CreatedOn = e.CreatedOn,
                    CreatedBy = e.CreatedBy,
                    ModifiedBy = e.ModifiedBy,
                    ModifiedOn = e.ModifiedOn,
                    SpecimenId = e.SpecimenId,
                    FileName = e.FileName,
                    Note = e.Note,
                    File = e.File,
                    Specimen = new SpecimenDTO { Id = e.Specimen.Id, CSID = e.Specimen.CSID, SpecimenID = e.Specimen.SpecimenID, StatusId = e.Specimen.StatusId, Status = new StatusDTO() { Id = e.Specimen.Status.Id, DisplayName = e.Specimen.Status.DisplayName, SysName = e.Specimen.Status.SysName } },
                    NoteCategoryId = e.NoteCategoryId,
                    NoteCategory = new NoteCategoryDTO() { Id = e.NoteCategory.Id, DisplayName = e.NoteCategory.DisplayName, SysName = e.NoteCategory.SysName },
                }
                ).FirstOrDefault();

                if (entity == null)
                {
                    return ResponseBase<SpecimenNoteDTO>.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                }
                return ResponseBase<SpecimenNoteDTO>.CreateSuccessResponse(entity);
            }
        }
        
        public ResponseBase SpecimenNoteFile_Remove(MessageBase<int> id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimen Notes."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<SpecimenNote>(s => s.Id == id.Data, new List<string>() { "NoteCategory", "Specimen.Status" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", id.Data)));
                    }
                    else 
                    {
                        if (!entity.Specimen.Status.SysName.Equals("INPROGRESS") && !_user.User.IsInRole("Lab Editor"))
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot edit Specimen Notes from a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                        } 
                            else
                        { 
                            entity.File = null;
                            entity.FileName = null;
                            session.Repository.Update<SpecimenNote>(entity);
                        }
                    }

                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase SpecimenNoteEdit(MessageBase<SpecimenNoteDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (IsUserReadOnly)
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit or delete Specimen Notes."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<SpecimenNote>(s => s.Id == dto.Data.Id, new List<string>() { "Specimen", "Specimen.Status" })
                    .Where(userFilter, _validIds)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        if (!entity.Specimen.Status.SysName.Equals("INPROGRESS") && !_user.User.IsInRole("Lab Editor"))
                        {
                            return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Cannot edit Specimen Notes from a specimen with the status {0}", entity.Specimen.Status.DisplayName)));
                        }
                        else
                        {
                            entity.Note = dto.Data.Note;
                            if (dto.Data.File != null)
                            {
                                entity.FileName = dto.Data.FileName;
                                entity.File = dto.Data.File;
                            }
                            entity.NoteCategoryId = dto.Data.NoteCategoryId;
                            entity.ModifiedBy = _user.Identity.GetUserId();
                            entity.ModifiedOn = DateTime.UtcNow;
                            session.Repository.Update<SpecimenNote>(entity);
                        }
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }

        public ResponseBase<IList<NoteCategoryDTO>> NoteCategoryList()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var entity = session.Repository.Query<NoteCategory>().OrderBy(s => s.Id)
                .Select(e => new NoteCategoryDTO() { Id = e.Id, DisplayName = e.DisplayName, SysName = e.SysName }
                ).ToList();
                return ResponseBase<IList<NoteCategoryDTO>>.CreateSuccessResponse(entity);
            }
        }
        public ResponseBase CategoryInsert(MessageBase<CategoryDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot insert Categories."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = new Category();
                    entity.AllowBlank = dto.Data.AllowBlank;
                    entity.LitsQuestionId = dto.Data.LitsQuestionId;
                    entity.Name = dto.Data.Name;
                    entity.Description = dto.Data.Description;
                    entity.CreatedBy = _user.UserId;
                    entity.CreatedOn = DateTime.UtcNow;
                    entity.ModifiedBy = _user.UserId;
                    entity.ModifiedOn = DateTime.UtcNow;
                    session.Repository.Insert<Category>(entity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        public ResponseBase CategoryEdit(MessageBase<CategoryDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit Categories."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Category>(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.AllowBlank = dto.Data.AllowBlank;
                        entity.LitsQuestionId = dto.Data.LitsQuestionId;
                        entity.Name = dto.Data.Name;
                        entity.Description = dto.Data.Description;
                        entity.ModifiedBy = _user.UserId;
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Category>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        public ResponseBase ConditionInsert(MessageBase<ConditionDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot insert Conditions."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = new Condition();
                    entity.LitsQuestionId = dto.Data.LitsQuestionId;
                    entity.Name = dto.Data.Name;
                    entity.Description = dto.Data.Description;
                    entity.CategoryId = dto.Data.CategoryId;
                    entity.Status = dto.Data.Status;
                    entity.Sorter = dto.Data.Sorter;
                    entity.CreatedBy = _user.UserId;
                    entity.CreatedOn = DateTime.UtcNow;
                    entity.ModifiedBy = _user.UserId;
                    entity.ModifiedOn = DateTime.UtcNow;
                    session.Repository.Insert<Condition>(entity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        public ResponseBase ConditionEdit(MessageBase<ConditionDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit Conditions."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Condition>(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.Description = dto.Data.Description;
                        entity.Status = dto.Data.Status;
                        entity.Sorter = dto.Data.Sorter;
                        entity.ModifiedBy = _user.UserId;
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Condition>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        public ResponseBase SynonymInsert(MessageBase<SynonymDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot insert Synonyms."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = new Synonym();
                    entity.CategoryId = dto.Data.CategoryId;
                    entity.ConditionId = dto.Data.ConditionId;
                    entity.LitsQuestionId = dto.Data.LitsQuestionId;
                    entity.Name = dto.Data.Name;
                    entity.Status = dto.Data.Status;
                    entity.ConditionName = dto.Data.ConditionName;
                    entity.CreatedBy = _user.UserId;
                    entity.CreatedOn = DateTime.UtcNow;
                    entity.ModifiedBy = _user.UserId;
                    entity.ModifiedOn = DateTime.UtcNow;
                    session.Repository.Insert<Synonym>(entity);
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
        public ResponseBase SynonymEdit(MessageBase<SynonymDTO> dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (!_user.User.IsInRole("Admin"))
                {
                    return ResponseBase.CreateErrorResponse(new Exception("Cannot edit Synonyms."));
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var entity = session.Repository.Query<Synonym>(s => s.Id == dto.Data.Id)
                    .FirstOrDefault();
                    if (entity == null)
                    {
                        return ResponseBase.CreateErrorResponse(new Exception(string.Format(@"Id {0} not found.", dto.Data.Id)));
                    }
                    else
                    {
                        entity.Name = dto.Data.Name;
                        entity.Status = dto.Data.Status;
                        entity.ModifiedBy = _user.UserId;
                        entity.ModifiedOn = DateTime.UtcNow;
                        session.Repository.Update<Synonym>(entity);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }


        public ResponseBase<IList<LabConsumptionDTO>> GenerateLabConsumptionFile()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {

                var tSQL = @"SELECT 
CSID_Table.CSID, CUID_Table.CUID, CSID_Table.Lab, CSID_Table.Month, CSID_Table.Year  
FROM 
(SELECT COUNT(A.CSID) AS CSID, DATEPART(mm, A.CreatedOn) AS Month, DATEPART(yy, A.CreatedOn) AS Year, C.Name AS Lab FROM Specimen A
JOIN AspNetUsers B ON A.CreatedBy = B.Id
JOIN Lab C ON B.LabId = C.Id
GROUP BY DATEPART(mm, A.CreatedOn), DATEPART(yy, A.CreatedOn), C.Name) AS CSID_Table
JOIN 
(SELECT COUNT(A.CUID) AS CUID, DATEPART(mm, A.CreatedOn) AS Month, DATEPART(yy, A.CreatedOn) AS Year, C.Name AS Lab FROM Aliquot A
JOIN AspNetUsers B ON A.CreatedBy = B.Id
JOIN Lab C ON B.LabId = C.Id
GROUP BY DATEPART(mm, A.CreatedOn), DATEPART(yy, A.CreatedOn), C.Name) AS CUID_Table
ON CSID_Table.Lab = CUID_Table.Lab 
AND CSID_Table.Month = CUID_Table.Month 
AND CSID_Table.Year = CUID_Table.Year 
ORDER BY CSID_Table.Year, CSID_Table.Month, CSID_Table.Lab";
                var entities = session.Repository.ExecuteQuery<LabConsumptionDTO>(tSQL, new object[] { }).ToList();
                return ResponseBase<IList<LabConsumptionDTO>>.CreateSuccessResponse(entities);
            }


        }
    }
}
