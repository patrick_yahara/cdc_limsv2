﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Web.Common;

namespace ContractLab.Utilities
{
    public class UserContextModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserContext>().To<AspNetUserContext>().InRequestScope();
        }
    }
}
