﻿using Ninject;
using Ninject.Modules;
using ContractLab.DataService.Interfaces;
using Yahara.Core.Repository;
using ContractLab.Repository;
using System.Security.Principal;

namespace ContractLab.DataService.Modules
{
    public class DataServiceRuntimeModule : NinjectModule
    {
        public override void Load()
        {
            var settingsProvider = Kernel.Get<ISettingsProvider>();

            Bind<ContractLabDbContextSource>().To<ContractLabDbContextSource>()
                                    .InSingletonScope()
                                    .WithConstructorArgument("databaseSettings", new BasicDatabaseSettings { ConnectionString = settingsProvider.GetConnectionString() });

            Bind<IContractLabDataService>().To<ContractLabDataService>();
            Bind<IContractLabDataService_HttpContextFree>().To<ContractLabDataService_HttpContextFree>();
        }
    }
}
