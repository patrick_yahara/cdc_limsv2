﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DataService
{
    public class RequestDetail
    {
        public string IdFilter { get; set; }
        public string Where { get; set; }
        public string OrderBy { get; set; }
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public DateTime? From  { get; set; }
        public DateTime? To { get; set; }
        public string CUID { get; set; }
        public int? FluSeason { get; set; }

        public RequestDetail()
        {
            this.Where = "Id = Id";
            this.OrderBy = "Id DESC";
            this.CurrentPage = 1;
            this.ItemsPerPage = int.MaxValue;
            this.CUID = string.Empty;
        }

        //public RequestDetail(string where, string orderby)
        //    : base()
        //{
        //    if (!string.IsNullOrEmpty(where))
        //    {
        //        this.Where = where;
        //    }
        //    if (!string.IsNullOrEmpty(orderby))
        //    {
        //        this.OrderBy = orderby;
        //    }
        //}

        //public RequestDetail(int currentpage, int itemsperpage)
        //    : base()
        //{
        //    this.CurrentPage = currentpage;
        //    this.ItemsPerPage = itemsperpage;
        //}

    }
}
