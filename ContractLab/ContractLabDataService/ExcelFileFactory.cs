﻿using ContractLab.DTO;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.DataService
{
    public static class ExcelFileFactory
    {
        private static readonly int _headerRowOffset = 0;
        private static int _dataRowOffset = 6;

        public static TemplateDTO GenerateFile<T>(TemplateDTO template, List<T> items, int dataRowOffsetoffset)
        {
            _dataRowOffset = dataRowOffsetoffset;
            return GenerateFile(template, items);
        }

        public static TemplateDTO GenerateFile<T>(TemplateDTO template, List<T> items)
        {
            ExcelRowTransformer transformer;
            ExcelWorksheet templateSheet;
            ExcelPackage outputEP = null;
            ExcelWorksheet sheetO = null;
            var outputrow = _dataRowOffset;
            var templateSheetName = string.Empty;
            //using(var templateEP = new ExcelPackage(localfileinfo))
            using (var msO = new MemoryStream())
            {
                using (var msTemplate = new MemoryStream(template.File))
                {
                    using (var templateEP = new ExcelPackage(msTemplate))
                    {
                        templateSheet = templateEP.Workbook.Worksheets[1];
                        templateSheetName = templateSheet.Name;
                        var headerrange = templateSheet.Cells[templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.Start.Column, templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.End.Column];
                        transformer = new ExcelRowTransformer(typeof(T), headerrange);
                        outputEP = new ExcelPackage(msO, msTemplate);
                    }
                }
                sheetO = outputEP.Workbook.Worksheets[1];
                foreach (var item in items)
                {
                    transformer.SetValuesRecursive(ref sheetO, outputrow++, item);
                }
                outputEP.SaveAs(msO);
                return new TemplateDTO() { TemplateType = template.TemplateType, FileName = template.FileName, File = msO.ToArray() };                
            }
        }

        public static TemplateDTO GenerateJsonFile<T>(TemplateDTO template, List<T> items)
        {
            using (var msO = new MemoryStream())
            {
                var serializer = new JsonFx.Json.JsonWriter();
                var sheetO = GenerateObjects(template, items);
                var payload = serializer.Write(sheetO);
                return new TemplateDTO() { TemplateType = template.TemplateType, FileName = Path.ChangeExtension(template.FileName, "json"), File = System.Text.Encoding.UTF8.GetBytes(payload) };
            }
        }

        public static List<ExpandoObject> GenerateObjects<T>(TemplateDTO template, List<T> items)
        {
            ExcelRowTransformer transformer;
            ExcelWorksheet templateSheet;
            var sheetO = new List<ExpandoObject>();
            var outputrow = 0;
            var templateSheetName = string.Empty;
            using (var msO = new MemoryStream())
            {
                using (var msTemplate = new MemoryStream(template.File))
                {
                    using (var templateEP = new ExcelPackage(msTemplate))
                    {
                        templateSheet = templateEP.Workbook.Worksheets[1];
                        templateSheetName = templateSheet.Name;
                        var headerrange = templateSheet.Cells[templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.Start.Column, templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.End.Column];
                        transformer = new ExcelRowTransformer(typeof(T), headerrange);
                    }
                }
                foreach (var item in items)
                {
                    transformer.SetValuesRecursiveJson(ref sheetO, outputrow++, item, true);
                }
                return sheetO;
            }
        }

        // * Erxpensive, painted into a corner with the Excel as template
        public static ExpandoObject GenerateObject<T>(TemplateDTO template, T item)
        {
            ExcelRowTransformer transformer;
            ExcelWorksheet templateSheet;
            var sheetO = new List<ExpandoObject>();
            var outputrow = 0;
            var templateSheetName = string.Empty;
            using (var msO = new MemoryStream())
            {
                using (var msTemplate = new MemoryStream(template.File))
                {
                    using (var templateEP = new ExcelPackage(msTemplate))
                    {
                        templateSheet = templateEP.Workbook.Worksheets[1];
                        templateSheetName = templateSheet.Name;
                        var headerrange = templateSheet.Cells[templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.Start.Column, templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.End.Column];
                        transformer = new ExcelRowTransformer(typeof(T), headerrange);
                    }
                }
                transformer.SetValuesRecursiveJson(ref sheetO, outputrow++, item, true);
                return sheetO.FirstOrDefault();
            }
        }


    }
}
