﻿using ContractLab.DTO;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ContractLab.DataService
{
    public class ExcelRowTransformer
    {
        private readonly Type _type;
        public readonly Dictionary<Type, List<KeyValuePair<int, PropertyInfo>>> TypedColumns = new Dictionary<Type, List<KeyValuePair<int, PropertyInfo>>>();

        private readonly bool _nullasempty = true;

        private readonly Dictionary<int, string> _columnmap = new Dictionary<int, string>();
        
        public static IEnumerable<KeyValuePair<Type, PropertyInfo>> GetPropertyInfoRecursive(Type t)
        {
            PropertyInfo[] props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prp in props)
            {
                if (prp.PropertyType.Module.ScopeName != "CommonLanguageRuntimeLibrary")
                {
                    foreach (var info in GetPropertyInfoRecursive(prp.PropertyType))
                        yield return info;
                }
                else
                {
                    yield return new KeyValuePair<Type, PropertyInfo>(t, prp);
                }
            }
        }

        public ExcelRowTransformer(Type type, ExcelRangeBase headerrow, bool nullasempltystring = false)
        {
            _type = type;

            var foo = ExcelRowTransformer.GetPropertyInfoRecursive(type);
            var props = (from kvp in foo
                         let attr = kvp.Value.GetCustomAttributes(typeof(HeadingAttribute), true)
                         where attr.Length == 1
                         select new { Type = kvp.Key, Property = kvp.Value, Attribute = attr.First() as HeadingAttribute }).ToList();

            if (headerrow == null || headerrow.Start.Row != headerrow.End.Row) return;
            for (var col = headerrow.Start.Column; col <= headerrow.End.Column; col++)
            {
                var nameer = headerrow.Worksheet.Cells[headerrow.Start.Row, col];
                if (nameer == null) continue;
                var nameVal = nameer.Value;
                if (nameVal == null) continue;
                var name = nameVal.ToString();
                if (String.IsNullOrEmpty(name)) continue;
                _columnmap.Add(col, name);
                var pi = props.Where(s => s.Attribute.ColumnHeaderText.Equals(name)).FirstOrDefault();
                if (pi == null) continue;
                if (TypedColumns.ContainsKey(pi.Type))
                {
                    TypedColumns[pi.Type].Add(new KeyValuePair<int, PropertyInfo>(col, pi.Property));
                }
                else
                {
                    var tempList = new List<KeyValuePair<int, PropertyInfo>>();
                    var tempKVP = new KeyValuePair<int, PropertyInfo>(col, pi.Property);
                    tempList.Add(tempKVP);
                    TypedColumns.Add(pi.Type, tempList);
                }
            }
        }

        public ExcelRowTransformer(Type type, string[] propertylist, int columnoffset, bool nullasempltystring = false)
        {
            _type = type;

            var foo = ExcelRowTransformer.GetPropertyInfoRecursive(type);
            var props = (from kvp in foo
                         let attr = kvp.Value.GetCustomAttributes(typeof(HeadingAttribute), true)
                         where attr.Length == 1
                         select new { Type = kvp.Key, Property = kvp.Value, Attribute = attr.First() as HeadingAttribute }).ToList();

            if (propertylist == null || propertylist.Length <= 0) return;
            for (var col = columnoffset; col < propertylist.Length + columnoffset; col++)
            {
                var name = propertylist[col - columnoffset];
                if (String.IsNullOrEmpty(name)) continue;
                _columnmap.Add(col, name);
                var pi = props.Where(s => s.Property.Name.Equals(name)).FirstOrDefault();
                if (pi == null) continue;
                if (TypedColumns.ContainsKey(pi.Type))
                {
                    TypedColumns[pi.Type].Add(new KeyValuePair<int, PropertyInfo>(col, pi.Property));
                }
                else
                {
                    var tempList = new List<KeyValuePair<int, PropertyInfo>>();
                    var tempKVP = new KeyValuePair<int, PropertyInfo>(col, pi.Property);
                    tempList.Add(tempKVP);
                    TypedColumns.Add(pi.Type, tempList);
                }
            }
        }

        public void SetValuesRecursive(ref ExcelWorksheet sheet, int index, object row)
        {
            Type t = row.GetType();
            // fill it in
            if (TypedColumns.ContainsKey(t))
            {
                foreach (var item in TypedColumns[t])
                {
                    var value = item.Value.GetValue(row);
                    if (_nullasempty && value == null)
                    {
                        sheet.Cells[index, item.Key].Value = string.Empty;
                    } 
                    else
                    {
                        sheet.Cells[index, item.Key].Value = value.ToString();
                    }
                }
            }
            // recursion
            PropertyInfo[] props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prp in props)
            {
                // not value type or dotnet class
                if (prp.PropertyType.Module.ScopeName != "CommonLanguageRuntimeLibrary")
                {
                    var value = prp.GetValue(row);
                    if (value == null)
                    {
                        continue;
                    }
                    SetValuesRecursive(ref sheet, index, value);
                }
                // do nothing
            }
        }

        public void SetValuesRecursiveJson(ref List<ExpandoObject> sheet, int index, object row, bool isnew = false)
        {
            Type t = row.GetType();
            // fill it in
            if (TypedColumns.ContainsKey(t))
            {
                var tempitem = new ExpandoObject() as IDictionary<string, Object>;
                if (!isnew && sheet.Count > index)
                {
                    tempitem = sheet[index] as IDictionary<string, Object>;
                }
                    
                foreach (var item in TypedColumns[t])
                {
                    var value = item.Value.GetValue(row);
                    var name = string.Empty;
                    if (_columnmap.ContainsKey(item.Key))
                    {
                        name = _columnmap[item.Key];
                    }
                    if (!string.IsNullOrEmpty(name) && !tempitem.ContainsKey(name))
                    {
                        if (_nullasempty && value == null)
                        {
                            //sheet.Cells[index, item.Key].Value = string.Empty;
                            tempitem.Add(name, string.Empty);
                        }
                        else
                        {
                            //sheet.Cells[index, item.Key].Value = value.ToString();
                            tempitem.Add(name, value.ToString());
                        }
                    }
                }
                if (isnew)
                {
                    sheet.Add(tempitem as ExpandoObject);    
                }
            }
            // recursion
            PropertyInfo[] props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prp in props)
            {
                // not value type or dotnet class
                if (prp.PropertyType.Module.ScopeName != "CommonLanguageRuntimeLibrary")
                {
                    var value = prp.GetValue(row);
                    if (value == null)
                    {
                        continue;
                    }
                    SetValuesRecursiveJson(ref sheet, index, value);
                }
                // do nothing
            }
        }

        // * I am going to heavily type these for now
        public WHOSpecimenDTO GetRow(ExcelRangeBase valuerow)
        {
            return GetRow(valuerow, valuerow.Start.Row);
        }

        public WHOSpecimenDTO GetRow(ExcelRangeBase valuerow, int rownumber, string OrdFieldName = "RowNumber")
        {
            var result = (WHOSpecimenDTO)Activator.CreateInstance(_type);
            if (!string.IsNullOrEmpty(OrdFieldName))
            {
                var propOrd = result.GetType().GetProperty(OrdFieldName);
                if (propOrd != null)
                {
                    propOrd.SetValue(result, Convert.ChangeType(rownumber, propOrd.PropertyType));
                }
            }
            List<KeyValuePair<int, PropertyInfo>> _columns = new List<KeyValuePair<int, PropertyInfo>>();
            if (!TypedColumns.TryGetValue(_type, out _columns))
	        {
		        return result;
	        }
            foreach (var kvp in _columns)
            {
                var range = valuerow.Worksheet.Cells[rownumber, kvp.Key];
                if (range == null) continue;

                //var value = range.Value;
                var value = range.Text;

                if (value != null && string.IsNullOrEmpty(value.ToString()) && kvp.Value.PropertyType.IsGenericType && kvp.Value.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    value = null;
                }

                if (_nullasempty && value == null && !(kvp.Value.PropertyType.IsGenericType && kvp.Value.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    try
                    {
                        kvp.Value.SetValue(result, Convert.ChangeType(String.Empty, kvp.Value.PropertyType));
                    }
                    catch (Exception ex)
                    {
                        var error = kvp.Value.ErrorPI();
                        if (error != null)
                        {
                            error.SetValue(result, ex.Message);   
                        }
                    }
                    
                }
                else if (value != null)
                {
                    if (kvp.Value.PropertyType.Name.Equals("DateTime"))
                    {
                        try
                        {
                            //double d = double.Parse(value.ToString());
                            //DateTime conv = DateTime.FromOADate(d);
                            //kvp.Value.SetValue(result, conv, null);
                            if (range.Value != null)
                            {
                                if (range.Value is DateTime)
                                {
                                    kvp.Value.SetValue(result, (DateTime)range.Value, null);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            var error = kvp.Value.ErrorPI();
                            if (error != null)
                            {
                                error.SetValue(result, ex.Message);   
                            }
                        }
                    } else
                    if (kvp.Value.PropertyType.IsGenericType && kvp.Value.PropertyType.GenericTypeArguments[0].Name.Equals("DateTime"))
                    {
                        try 
                        {
                            //double d = double.Parse(value.ToString());
                            //DateTime conv = DateTime.FromOADate(d);
                            //kvp.Value.SetValue(result, conv, null);
                            if (range.Value != null)
                            {
                                if (range.Value is DateTime)
                                {
                                    kvp.Value.SetValue(result, (DateTime)range.Value, null);
                                }
                                else
                                {
                                    // * CLAW - 83 13 JAN 2017
                                    DateTime tempDT;
                                    if (DateTime.TryParse(range.Text, out tempDT))
                                    {
                                        kvp.Value.SetValue(result, tempDT, null);
                                    }
                                    else
                                    {
                                        var error = kvp.Value.ErrorPI();
                                        if (error != null)
                                        {
                                            error.SetValue(result, @"Could not parse DateTime");
                                        }
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            var error = kvp.Value.ErrorPI();
                            if (error != null)
                            {
                                error.SetValue(result, ex.Message);
                            }
                        }
                    }
                    else
                    { 
                        try
                        {
                            // * check for nullable
                            // * CLAW - 83 13 JAN 2017
                            if (kvp.Value.PropertyType.IsGenericType && kvp.Value.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                var t = Nullable.GetUnderlyingType(kvp.Value.PropertyType);
                                object safeValue = (value == null) ? null : Convert.ChangeType(value, t);
                                kvp.Value.SetValue(result, safeValue, null);
                            }
                            else
                            {
                                kvp.Value.SetValue(result, Convert.ChangeType(value, kvp.Value.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = kvp.Value.ErrorPI();
                            if (error != null)
                            {
                                error.SetValue(result, ex.Message);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public IEnumerable<WHOSpecimenDTO> GetRows(ExcelRangeBase valuerange)
        {
            var result = new List<WHOSpecimenDTO>();
            for (var i = valuerange.Start.Row; i <= valuerange.End.Row; i++)
            {
                WHOSpecimenDTO row;
                try
                {
                    row = GetRow(valuerange, i);
                }
                catch (Exception ex)
                {
                    row = new WHOSpecimenDTO();
                    row.RowNumber = i;
                    row.IsError = true;
                    row.RowError = ex.Message;
                }
                if (string.IsNullOrEmpty(row.SpecimenID) && !row.DateCollected.HasValue)
	            {
                    break;
	            }
                result.Add(row);
            }
            return result;
        }

    }
}