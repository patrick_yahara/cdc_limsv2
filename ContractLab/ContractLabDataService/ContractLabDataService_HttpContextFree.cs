﻿using ContractLab.DTO;
using ContractLab.Repository;
using ContractLab.Repository.Model;
using ContractLab.DataService.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Yahara.Core.Repository.EF;
using Yahara.ServiceInfrastructure.EF;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.DataService
{
    class ContractLabDataService_HttpContextFree : EfDataServiceBase, IContractLabDataService_HttpContextFree
    {
        private readonly ContractLabDbContextSource _contextSource;

        public ContractLabDataService_HttpContextFree(ContractLabDbContextSource contextSource)
        {
            var tempcurrent = DateTime.UtcNow;
            _contextSource = contextSource;
        }

        private List<Transmission> GenerateTransmissionFile_Internal(SingleDisposableRepositorySession session, List<int> ids)
        {
            var localTransmissionList = new List<Transmission>();

            var outputfile = new TemplateDTO();
            var pagesize = 80;
            var pages = ids.Count / pagesize;
            if (ids.Count % pagesize > 0 || pages == 0)
            {
                pages++;
            }


            for (int i = 0; i < pages; i++)
            {
                var localIdList = ids.Skip(i * pagesize).Take(pagesize);
                var query = session.Repository.Query<Transmission>(s => localIdList.Contains(s.Id));
                localTransmissionList.AddRange(query.ToList());
            }

            localTransmissionList.Sort();
            Transmission superT = new Transmission();
            for (int i = 0; i < localTransmissionList.Count; i++)
            {
                if (!superT.Equals(localTransmissionList[i]))
                {
                    localTransmissionList[i].TransmittedOn = DateTime.UtcNow;
                    superT = localTransmissionList[i];
                }
                else
                {
                    localTransmissionList[i].SupercedeId = superT.Id;
                }
                localTransmissionList[i].Attempts = localTransmissionList[i].Attempts.HasValue ? localTransmissionList[i].Attempts + 1 : 1;
                localTransmissionList[i].ModifiedOn = DateTime.UtcNow;
                localTransmissionList[i].ModifiedBy = "Hangfire";
            }
            foreach (var item in localTransmissionList)
            {
                session.Repository.Update<Transmission>(item);
            }
            return localTransmissionList;
        }

        public ResponseBase GenerateTransmissionFile_Background(MessageBase<int> size, MessageBase<string> path)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var query = session.Repository.Query<Transmission>(s => !s.TransmittedOn.HasValue && !s.SupercedeId.HasValue).Select(s => s.Id);
                    var localTransmissionList = GenerateTransmissionFile_Internal(session, query.ToList());

                    var itemsforfile = localTransmissionList.Where(s => !s.SupercedeId.HasValue).ToList();

                    if (itemsforfile.Count == 0)
                    {
                        return ResponseBase.CreateSuccessResponse();
                    }

                    var pagesize = size.Data;
                    var pages = itemsforfile.Count / pagesize;
                    if (itemsforfile.Count % pagesize > 0 || pages == 0)
                    {
                        pages++;
                    }

                    for (int i = 0; i < pages; i++)
                    {
                        var localitemsforfile = itemsforfile.Skip(i * pagesize).Take(pagesize);
                        Thread.Sleep(1);
                        var filepath = Path.Combine(path.Data, DateTime.UtcNow.ToFileTimeUtc().ToString() + ".json");
                        var file = System.Text.Encoding.UTF8.GetBytes("[" + String.Join(",", localTransmissionList.Where(s => !s.SupercedeId.HasValue).Select(s => s.Payload).ToArray()) + "]");
                        System.IO.File.WriteAllBytes(filepath, file);
                    }
                    return ResponseBase.CreateSuccessResponse();
                }
            });
        }
    }
}
