﻿
Setting up a service project

1) Build the solution to pull in the  nuget references.
2) Replace ContractLabDbContextSource with the DbContextSource from your .Repository project.
3) Rename ImplementationOfDataService and IImplementationOfDataService to suit your needs.
4) Add any additional DI configuration in DataServiceRuntimeModule.
5) Add any Automapper mappings from your Repository.Model to DataService.Model classes in DataServiceToModelAutoMappings.
6) Delete this file.
