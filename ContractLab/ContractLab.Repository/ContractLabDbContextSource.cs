﻿using Yahara.Core.Repository;
using Yahara.Core.Repository.EF;

namespace ContractLab.Repository
{
    public class ContractLabDbContextSource : EntityFrameworkDbContextSource<ContractLabDbContext, DefaultContextInitializer>
    {
        public ContractLabDbContextSource(BasicDatabaseSettings databaseSettings)
            : base(databaseSettings)
        {
        }

        public override string GetConfigurationsNamespace()
        {
            return "ContractLab.Repository.Models.Mapping";
        }
    }
}
