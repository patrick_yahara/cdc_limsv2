﻿

Setting up a repository project

1) Build the solution to pull in the  nuget references.
2) Replace ProjectName__ with your project name in ProjectName__DbContext and ProjectName__DbContextSource
3) Run Entity Framework -> Reverse Engineer Code First to generate your model.
4) (optional) Move the model folder to another assembly.
5) Verify ProjectName__DbContextSource.GetConfigurationsNamespace() returns the correct namesapce for model objects.
6) Update the connection string in your upstream project.  A template SQL connection string is part of this project.
7) Delete this file.