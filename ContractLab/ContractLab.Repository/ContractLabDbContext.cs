using System.Data.Entity.Infrastructure;
using Yahara.Core.Repository.EF;

namespace ContractLab.Repository
{
    public class ContractLabDbContext : DomainSpecificContext
    {
        public ContractLabDbContext(string nameOrConnectionString, DbCompiledModel compiledModel)
            : base(nameOrConnectionString, compiledModel)
        {
            Configuration.LazyLoadingEnabled = false;
        }
    }
}