using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Package : ConcreteEntityBase<int>
    {
        public Package()
        {
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public DateTime? DateReceived { get; set; }
        public string SpecimenCondition { get; set; }
        public int SenderId { get; set; }
        public int? ContactId { get; set; }
        public string Comments { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Sender Sender { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
