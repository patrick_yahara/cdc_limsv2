using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class State : ConcreteEntityBase<int>
    {
        public State()
        {
            Counties = new List<County>();
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public virtual ICollection<County> Counties { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
