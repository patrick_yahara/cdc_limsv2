using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Lab : ConcreteEntityBase<int>
    {
        public Lab()
        {
            AspNetUsers = new List<AspNetUser>();
            Specimens = new List<Specimen>();
            Templates = new List<Template>();
        }

        public override int Id { get; set; }
        public string Name { get; set; }
        public string PrinterURI { get; set; }
        public string LabelPreamble { get; set; }
        public string LabelPostamble { get; set; }
        public string LabelFormatString { get; set; }
        public string WHOLabelFormatString { get; set; }
        public string StoredLocallyCodes { get; set; }
        public string AliquotVersionCode { get; set; }
        public string LabelTestCodeOrder { get; set; }
        public string ImportPropertyOrder { get; set; }
        public string GrownTestCodeList { get; set; }
        public string IttTestCodeList { get; set; }
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }
        public virtual ICollection<Template> Templates { get; set; }

	}
}
