using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Category : ConcreteEntityBase<int>
    {
        public Category()
        {
            Conditions = new List<Condition>();
            Synonyms = new List<Synonym>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LitsQuestionId { get; set; }
        public bool AllowBlank { get; set; }
        public virtual ICollection<Condition> Conditions { get; set; }
        public virtual ICollection<Synonym> Synonyms { get; set; }

	}
}
