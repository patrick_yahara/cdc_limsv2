using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class NoteCategory : ConcreteEntityBase<int>
    {
        public NoteCategory()
        {
            SpecimenNotes = new List<SpecimenNote>();
        }

        public override int Id { get; set; }
        public string SysName { get; set; }
        public string DisplayName { get; set; }
        public virtual ICollection<SpecimenNote> SpecimenNotes { get; set; }

	}
}
