using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class AspNetRole : ConcreteEntityBase<string>
    {
        public AspNetRole()
        {
            AspNetUsers = new List<AspNetUser>();
        }

        public override string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }

	}
}
