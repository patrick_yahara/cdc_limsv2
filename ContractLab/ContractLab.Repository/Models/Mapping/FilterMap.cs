
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class FilterMap : EntityTypeConfiguration<Filter>
    {
        public FilterMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Display)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.BaseObject)
                .IsRequired()
                .HasMaxLength(512);

            Property(t => t.FilterText)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            ToTable("Filter");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Display).HasColumnName("Display");
            Property(t => t.BaseObject).HasColumnName("BaseObject");
            Property(t => t.FilterText).HasColumnName("FilterText");
            Property(t => t.IsLabAvailable).HasColumnName("IsLabAvailable");
        }
    }
}
