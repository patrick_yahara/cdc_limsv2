
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class IsolateMap : EntityTypeConfiguration<Isolate>
    {
        public IsolateMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.PassageLevel)
                .HasMaxLength(128);

            Property(t => t.RedBloodCellType)
                .HasMaxLength(20);

            Property(t => t.Comments)
                .HasMaxLength(512);

            // Table & Column Mappings
            ToTable("Isolate");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            Property(t => t.PassageLevel).HasColumnName("PassageLevel");
            Property(t => t.DateHarvested).HasColumnName("DateHarvested");
            Property(t => t.Titer).HasColumnName("Titer");
            Property(t => t.RedBloodCellType).HasColumnName("RedBloodCellType");
            Property(t => t.DateInoculated).HasColumnName("DateInoculated");
            Property(t => t.IsolateTypeId).HasColumnName("IsolateTypeId");
            Property(t => t.Comments).HasColumnName("Comments");
            Property(t => t.StatusId).HasColumnName("StatusId");

            // Relationships
            HasRequired(t => t.IsolateType)
                .WithMany(t => t.Isolates)
                .HasForeignKey(d => d.IsolateTypeId);
            HasRequired(t => t.Specimen)
                .WithMany(t => t.Isolates)
                .HasForeignKey(d => d.SpecimenId);
            HasOptional(t => t.Status)
                .WithMany(t => t.Isolates)
                .HasForeignKey(d => d.StatusId);


        }
    }
}
