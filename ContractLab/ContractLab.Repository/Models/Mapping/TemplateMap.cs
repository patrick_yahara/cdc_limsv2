
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class TemplateMap : EntityTypeConfiguration<Template>
    {
        public TemplateMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.TemplateType)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.FileName)
                .IsRequired()
                .HasMaxLength(256);

            Property(t => t.File)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Template");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.TemplateType).HasColumnName("TemplateType");
            Property(t => t.FileName).HasColumnName("FileName");
            Property(t => t.File).HasColumnName("File");
            Property(t => t.LabId).HasColumnName("LabId");

            // Relationships
            HasOptional(t => t.Lab)
                .WithMany(t => t.Templates)
                .HasForeignKey(d => d.LabId);


        }
    }
}
