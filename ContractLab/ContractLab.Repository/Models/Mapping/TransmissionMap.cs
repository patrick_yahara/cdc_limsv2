
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class TransmissionMap : EntityTypeConfiguration<Transmission>
    {
        public TransmissionMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Payload)
                .IsRequired();

            Property(t => t.SpecimenRowVersion)
                .IsRequired()
                .HasMaxLength(8);

            Property(t => t.IsolateRowVersion)
                .HasMaxLength(8);

            Property(t => t.AliquotRowVersion)
                .HasMaxLength(8);

            Property(t => t.Signature)
                .HasMaxLength(2048);

            // Table & Column Mappings
            ToTable("Transmission");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            Property(t => t.IsolateId).HasColumnName("IsolateId");
            Property(t => t.AliquotId).HasColumnName("AliquotId");
            Property(t => t.Payload).HasColumnName("Payload");
            Property(t => t.Response).HasColumnName("Response");
            Property(t => t.Attempts).HasColumnName("Attempts");
            Property(t => t.TransmittedOn).HasColumnName("TransmittedOn");
            Property(t => t.SpecimenRowVersion).HasColumnName("SpecimenRowVersion");
            Property(t => t.IsolateRowVersion).HasColumnName("IsolateRowVersion");
            Property(t => t.AliquotRowVersion).HasColumnName("AliquotRowVersion");
            Property(t => t.Signature).HasColumnName("Signature");
            Property(t => t.SupercedeId).HasColumnName("SupercedeId");

            // Relationships
            HasOptional(t => t.Aliquot)
                .WithMany(t => t.Transmissions)
                .HasForeignKey(d => d.AliquotId);
            HasOptional(t => t.Isolate)
                .WithMany(t => t.Transmissions)
                .HasForeignKey(d => d.IsolateId);
            HasRequired(t => t.Specimen)
                .WithMany(t => t.Transmissions)
                .HasForeignKey(d => d.SpecimenId);


        }
    }
}
