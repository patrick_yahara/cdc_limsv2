
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContractLab.Repository.Models.Mapping
{
    public class VersionInfoMap : EntityTypeConfiguration<VersionInfo>
    {
        public VersionInfoMap()
        {
            // Primary Key
            HasKey(t => t.Version);

            // Properties
            Property(t => t.Version)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Description)
                .HasMaxLength(1024);

            // Table & Column Mappings
            ToTable("VersionInfo");
            Property(t => t.Version).HasColumnName("Version");
            Property(t => t.AppliedOn).HasColumnName("AppliedOn");
            Property(t => t.Description).HasColumnName("Description");
        }
    }
}
