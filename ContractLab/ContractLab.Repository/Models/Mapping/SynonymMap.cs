
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class SynonymMap : EntityTypeConfiguration<Synonym>
    {
        public SynonymMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(255);

            Property(t => t.LitsQuestionId)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.ConditionName)
                .IsRequired()
                .HasMaxLength(255);

            Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            ToTable("Synonym");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.LitsQuestionId).HasColumnName("LitsQuestionId");
            Property(t => t.CategoryId).HasColumnName("CategoryId");
            Property(t => t.ConditionName).HasColumnName("ConditionName");
            Property(t => t.ConditionId).HasColumnName("ConditionId");
            Property(t => t.Status).HasColumnName("Status");

            // Relationships
            HasRequired(t => t.Category)
                .WithMany(t => t.Synonyms)
                .HasForeignKey(d => d.CategoryId);
            HasRequired(t => t.Condition)
                .WithMany(t => t.Synonyms)
                .HasForeignKey(d => d.ConditionId);


        }
    }
}
