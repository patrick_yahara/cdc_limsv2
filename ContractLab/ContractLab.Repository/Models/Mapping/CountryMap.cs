
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class CountryMap : EntityTypeConfiguration<Country>
    {
        public CountryMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ShortCode)
                .HasMaxLength(10);

            Property(t => t.LongCode)
                .HasMaxLength(10);

            // Table & Column Mappings
            ToTable("Country");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.ShortCode).HasColumnName("ShortCode");
            Property(t => t.LongCode).HasColumnName("LongCode");
            Property(t => t.NumberCode).HasColumnName("NumberCode");
        }
    }
}
