
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class SpecimenNoteMap : EntityTypeConfiguration<SpecimenNote>
    {
        public SpecimenNoteMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Note)
                .IsRequired();

            Property(t => t.FileName)
                .HasMaxLength(256);


            // Table & Column Mappings
            ToTable("SpecimenNote");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            Property(t => t.NoteCategoryId).HasColumnName("NoteCategoryId");
            Property(t => t.Note).HasColumnName("Note");
            Property(t => t.FileName).HasColumnName("FileName");
            Property(t => t.File).HasColumnName("File");

            // Relationships
            HasRequired(t => t.NoteCategory)
                .WithMany(t => t.SpecimenNotes)
                .HasForeignKey(d => d.NoteCategoryId);
            HasRequired(t => t.Specimen)
                .WithMany(t => t.SpecimenNotes)
                .HasForeignKey(d => d.SpecimenId);


        }
    }
}
