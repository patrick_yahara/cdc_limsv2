
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class IsolateTypeMap : EntityTypeConfiguration<IsolateType>
    {
        public IsolateTypeMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.DisplayName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.SysName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("IsolateType");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.DisplayName).HasColumnName("DisplayName");
            Property(t => t.SysName).HasColumnName("SysName");
            Property(t => t.IsVisible).HasColumnName("IsVisible");
        }
    }
}
