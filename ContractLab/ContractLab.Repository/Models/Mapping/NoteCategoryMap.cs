
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class NoteCategoryMap : EntityTypeConfiguration<NoteCategory>
    {
        public NoteCategoryMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.SysName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.DisplayName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("NoteCategory");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.SysName).HasColumnName("SysName");
            Property(t => t.DisplayName).HasColumnName("DisplayName");
        }
    }
}
