
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class ManifestMap : EntityTypeConfiguration<Manifest>
    {
        public ManifestMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.Comments)
                .HasMaxLength(2048);

            Property(t => t.TrackingNumber)
                .HasMaxLength(255);

            // Table & Column Mappings
            ToTable("Manifest");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.IsVisible).HasColumnName("IsVisible");
            Property(t => t.Comments).HasColumnName("Comments");
            Property(t => t.DateShipped).HasColumnName("DateShipped");
            Property(t => t.TrackingNumber).HasColumnName("TrackingNumber");

            // Relationships
            HasRequired(t => t.Status)
                .WithMany(t => t.Manifests)
                .HasForeignKey(d => d.StatusId);


        }
    }
}
