
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContractLab.Repository.Models.Mapping
{
    public class SpecimenMap : EntityTypeConfiguration<Specimen>
    {
        public SpecimenMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.CSID)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.CUID)
                .HasMaxLength(128);

            Property(t => t.SpecimenID)
                .HasMaxLength(255);

            Property(t => t.PatientAge)
                .HasMaxLength(50);

            Property(t => t.PatientAgeUnits)
                .HasMaxLength(255);

            Property(t => t.PatientSex)
                .HasMaxLength(128);

            Property(t => t.Activity)
                .HasMaxLength(255);

            Property(t => t.PassageHistory)
                .HasMaxLength(128);

            Property(t => t.SubType)
                .HasMaxLength(128);

            Property(t => t.SpecimenSource)
                .HasMaxLength(255);

            Property(t => t.InitialStorage)
                .HasMaxLength(255);

            Property(t => t.CompCSID1)
                .HasMaxLength(128);

            Property(t => t.CompCSID2)
                .HasMaxLength(128);

            Property(t => t.OtherLocation)
                .HasMaxLength(255);

            Property(t => t.DrugResistant)
                .HasMaxLength(20);

            Property(t => t.Deceased)
                .HasMaxLength(20);

            Property(t => t.SpecialStudy)
                .HasMaxLength(128);

            Property(t => t.ContractLabFrom)
                .HasMaxLength(128);

            Property(t => t.NursingHome)
                .HasMaxLength(20);

            Property(t => t.Vaccinated)
                .HasMaxLength(20);

            Property(t => t.Travel)
                .HasMaxLength(255);

            Property(t => t.ReasonForSubmission)
                .HasMaxLength(255);

            Property(t => t.Comments)
                .HasMaxLength(512);

            Property(t => t.SampleClass)
                .HasMaxLength(255);

            Property(t => t.TravelCity)
                .HasMaxLength(128);

            Property(t => t.HostCellLine)
                .HasMaxLength(128);

            Property(t => t.Substrate)
                .HasMaxLength(128);

            Property(t => t.PassageNumber)
                .HasMaxLength(128);

            Property(t => t.PCRInfA)
                .HasMaxLength(10);

            Property(t => t.PCRpdmH1)
                .HasMaxLength(10);

            Property(t => t.PCRH3)
                .HasMaxLength(10);

            Property(t => t.PCRInfB)
                .HasMaxLength(10);

            Property(t => t.PCRBVic)
                .HasMaxLength(10);

            Property(t => t.PCRBYam)
                .HasMaxLength(10);

            Property(t => t.City)
                .HasMaxLength(128);

            Property(t => t.PCRpdmInfA)
                .HasMaxLength(10);

            Property(t => t.TravelCountry)
                .HasMaxLength(128);

            Property(t => t.TravelState)
                .HasMaxLength(128);

            Property(t => t.TravelCounty)
                .HasMaxLength(128);

            Property(t => t.WHOSubmittedSubtype)
                .HasMaxLength(128);

            // Table & Column Mappings
            ToTable("Specimen");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.CSID).HasColumnName("CSID");
            Property(t => t.CUID).HasColumnName("CUID");
            Property(t => t.ProjectId).HasColumnName("ProjectId");
            Property(t => t.SpecimenID).HasColumnName("SpecimenID");
            Property(t => t.DateCollected).HasColumnName("DateCollected");
            Property(t => t.DateCollectedEstimated).HasColumnName("DateCollectedEstimated");
            Property(t => t.PatientAge).HasColumnName("PatientAge");
            Property(t => t.PatientAgeUnits).HasColumnName("PatientAgeUnits");
            Property(t => t.PatientSex).HasColumnName("PatientSex");
            Property(t => t.Activity).HasColumnName("Activity");
            Property(t => t.PassageHistory).HasColumnName("PassageHistory");
            Property(t => t.SubType).HasColumnName("SubType");
            Property(t => t.SpecimenSource).HasColumnName("SpecimenSource");
            Property(t => t.InitialStorage).HasColumnName("InitialStorage");
            Property(t => t.CompCSID1).HasColumnName("CompCSID1");
            Property(t => t.CompCSID2).HasColumnName("CompCSID2");
            Property(t => t.OtherLocation).HasColumnName("OtherLocation");
            Property(t => t.DrugResistant).HasColumnName("DrugResistant");
            Property(t => t.Deceased).HasColumnName("Deceased");
            Property(t => t.SpecialStudy).HasColumnName("SpecialStudy");
            Property(t => t.ContractLabFrom).HasColumnName("ContractLabFrom");
            Property(t => t.NursingHome).HasColumnName("NursingHome");
            Property(t => t.Vaccinated).HasColumnName("Vaccinated");
            Property(t => t.Travel).HasColumnName("Travel");
            Property(t => t.ReasonForSubmission).HasColumnName("ReasonForSubmission");
            Property(t => t.Comments).HasColumnName("Comments");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.SampleClass).HasColumnName("SampleClass");
            Property(t => t.DateInoculated).HasColumnName("DateInoculated");
            Property(t => t.PackageId).HasColumnName("PackageId");
            Property(t => t.CountryId).HasColumnName("CountryId");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CountyId).HasColumnName("CountyId");
            Property(t => t.LabId).HasColumnName("LabId");
            Property(t => t.TravelCity).HasColumnName("TravelCity");
            Property(t => t.HostCellLine).HasColumnName("HostCellLine");
            Property(t => t.Substrate).HasColumnName("Substrate");
            Property(t => t.PassageNumber).HasColumnName("PassageNumber");
            Property(t => t.PCRInfA).HasColumnName("PCRInfA");
            Property(t => t.PCRpdmH1).HasColumnName("PCRpdmH1");
            Property(t => t.PCRH3).HasColumnName("PCRH3");
            Property(t => t.PCRInfB).HasColumnName("PCRInfB");
            Property(t => t.PCRBVic).HasColumnName("PCRBVic");
            Property(t => t.PCRBYam).HasColumnName("PCRBYam");
            Property(t => t.City).HasColumnName("City");
            Property(t => t.TravelReturnDate).HasColumnName("TravelReturnDate");
            Property(t => t.FluSeason).HasColumnName("FluSeason").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(t => t.PCRpdmInfA).HasColumnName("PCRpdmInfA");
            Property(t => t.TravelCountry).HasColumnName("TravelCountry");
            Property(t => t.TravelState).HasColumnName("TravelState");
            Property(t => t.TravelCounty).HasColumnName("TravelCounty");
            Property(t => t.WHOSubmittedSubtype).HasColumnName("WHOSubmittedSubtype");

            // Relationships
            HasOptional(t => t.Country)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.CountryId);
            HasOptional(t => t.County)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.CountyId);
            HasOptional(t => t.Lab)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.LabId);
            HasRequired(t => t.Package)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.PackageId);
            HasOptional(t => t.Project)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.ProjectId);
            HasOptional(t => t.State)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.StateId);
            HasRequired(t => t.Status)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.StatusId);


        }
    }
}
