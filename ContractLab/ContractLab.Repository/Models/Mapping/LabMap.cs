
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class LabMap : EntityTypeConfiguration<Lab>
    {
        public LabMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Name)
                .HasMaxLength(128);

            Property(t => t.PrinterURI)
                .HasMaxLength(2083);

            Property(t => t.LabelPreamble)
                .HasMaxLength(512);

            Property(t => t.LabelPostamble)
                .HasMaxLength(512);

            Property(t => t.LabelFormatString)
                .HasMaxLength(2048);

            Property(t => t.WHOLabelFormatString)
                .HasMaxLength(2048);

            Property(t => t.StoredLocallyCodes)
                .HasMaxLength(128);

            Property(t => t.AliquotVersionCode)
                .HasMaxLength(50);

            Property(t => t.LabelTestCodeOrder)
                .HasMaxLength(50);

            Property(t => t.GrownTestCodeList)
                .HasMaxLength(255);

            Property(t => t.IttTestCodeList)
                .HasMaxLength(255);

            // Table & Column Mappings
            ToTable("Lab");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.PrinterURI).HasColumnName("PrinterURI");
            Property(t => t.LabelPreamble).HasColumnName("LabelPreamble");
            Property(t => t.LabelPostamble).HasColumnName("LabelPostamble");
            Property(t => t.LabelFormatString).HasColumnName("LabelFormatString");
            Property(t => t.WHOLabelFormatString).HasColumnName("WHOLabelFormatString");
            Property(t => t.StoredLocallyCodes).HasColumnName("StoredLocallyCodes");
            Property(t => t.AliquotVersionCode).HasColumnName("AliquotVersionCode");
            Property(t => t.LabelTestCodeOrder).HasColumnName("LabelTestCodeOrder");
            Property(t => t.ImportPropertyOrder).HasColumnName("ImportPropertyOrder");
            Property(t => t.GrownTestCodeList).HasColumnName("GrownTestCodeList");
            Property(t => t.IttTestCodeList).HasColumnName("IttTestCodeList");
        }
    }
}
