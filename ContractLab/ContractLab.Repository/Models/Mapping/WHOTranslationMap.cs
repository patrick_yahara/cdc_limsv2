
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class WHOTranslationMap : EntityTypeConfiguration<WHOTranslation>
    {
        public WHOTranslationMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Heading)
                .IsRequired()
                .HasMaxLength(256);

            Property(t => t.MatchExpression)
                .HasMaxLength(512);

            Property(t => t.ReplacementValue)
                .HasMaxLength(512);

            Property(t => t.CommentHeading)
                .IsRequired()
                .HasMaxLength(256);

            Property(t => t.CommentValue)
                .HasMaxLength(512);

            // Table & Column Mappings
            ToTable("WHOTranslation");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.Heading).HasColumnName("Heading");
            Property(t => t.MatchExpression).HasColumnName("MatchExpression");
            Property(t => t.ReplacementValue).HasColumnName("ReplacementValue");
            Property(t => t.CommentHeading).HasColumnName("CommentHeading");
            Property(t => t.CommentValue).HasColumnName("CommentValue");
        }
    }
}
