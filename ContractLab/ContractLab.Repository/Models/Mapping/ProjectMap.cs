
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            ToTable("Project");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}
