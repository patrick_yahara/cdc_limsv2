
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class PackageMap : EntityTypeConfiguration<Package>
    {
        public PackageMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.SpecimenCondition)
                .HasMaxLength(255);

            Property(t => t.Comments)
                .HasMaxLength(2048);

            // Table & Column Mappings
            ToTable("Package");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.IsVisible).HasColumnName("IsVisible");
            Property(t => t.DateReceived).HasColumnName("DateReceived");
            Property(t => t.SpecimenCondition).HasColumnName("SpecimenCondition");
            Property(t => t.SenderId).HasColumnName("SenderId");
            Property(t => t.ContactId).HasColumnName("ContactId");
            Property(t => t.Comments).HasColumnName("Comments");

            // Relationships
            HasOptional(t => t.Contact)
                .WithMany(t => t.Packages)
                .HasForeignKey(d => d.ContactId);
            HasRequired(t => t.Sender)
                .WithMany(t => t.Packages)
                .HasForeignKey(d => d.SenderId);


        }
    }
}
