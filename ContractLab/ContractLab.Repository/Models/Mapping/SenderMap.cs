
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class SenderMap : EntityTypeConfiguration<Sender>
    {
        public SenderMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.CompanyName)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.Address)
                .HasMaxLength(128);

            Property(t => t.City)
                .HasMaxLength(128);

            Property(t => t.State)
                .HasMaxLength(50);

            Property(t => t.Country)
                .HasMaxLength(50);

            Property(t => t.RasClientId)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Sender");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.CompanyName).HasColumnName("CompanyName");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.City).HasColumnName("City");
            Property(t => t.State).HasColumnName("State");
            Property(t => t.Country).HasColumnName("Country");
            Property(t => t.RasClientId).HasColumnName("RasClientId");
            Property(t => t.FluSeason).HasColumnName("FluSeason");
        }
    }
}
