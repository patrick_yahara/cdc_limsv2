
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class AliquotMap : EntityTypeConfiguration<Aliquot>
    {
        public AliquotMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.CUID)
                .HasMaxLength(128);

            Property(t => t.TestCode)
                .HasMaxLength(20);

            // Table & Column Mappings
            ToTable("Aliquot");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.IsolateId).HasColumnName("IsolateId");
            Property(t => t.CUID).HasColumnName("CUID");
            Property(t => t.TestCode).HasColumnName("TestCode");
            Property(t => t.ManifestId).HasColumnName("ManifestId");
            Property(t => t.IsStoredLocally).HasColumnName("IsStoredLocally");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.RedshiftTracking).HasColumnName("RedshiftTracking");

            // Relationships
            HasRequired(t => t.Isolate)
                .WithMany(t => t.Aliquots)
                .HasForeignKey(d => d.IsolateId);
            HasOptional(t => t.Manifest)
                .WithMany(t => t.Aliquots)
                .HasForeignKey(d => d.ManifestId);
            HasOptional(t => t.Status)
                .WithMany(t => t.Aliquots)
                .HasForeignKey(d => d.StatusId);


        }
    }
}
