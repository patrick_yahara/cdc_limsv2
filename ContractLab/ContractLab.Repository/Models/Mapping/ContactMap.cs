
using System.Data.Entity.ModelConfiguration;
using ContractLab.Repository.Model;

namespace ContractLab.Repository.Models.Mapping
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.ModifiedBy)
                .IsRequired()
                .HasMaxLength(128);

            Property(t => t.VersionColumn)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            Property(t => t.FullName)
                .IsRequired()
                .HasMaxLength(256);

            Property(t => t.Address)
                .HasMaxLength(512);

            Property(t => t.City)
                .HasMaxLength(128);

            Property(t => t.State)
                .HasMaxLength(50);

            Property(t => t.Country)
                .HasMaxLength(50);

            Property(t => t.Phone)
                .HasMaxLength(128);

            Property(t => t.Fax)
                .HasMaxLength(128);

            Property(t => t.Email)
                .HasMaxLength(128);

            Property(t => t.RasClientId)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.RasContactId)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Contact");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            Property(t => t.VersionColumn).HasColumnName("VersionColumn");
            Property(t => t.FullName).HasColumnName("FullName");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.City).HasColumnName("City");
            Property(t => t.State).HasColumnName("State");
            Property(t => t.Country).HasColumnName("Country");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.RasClientId).HasColumnName("RasClientId");
            Property(t => t.RasContactId).HasColumnName("RasContactId");
            Property(t => t.FluSeason).HasColumnName("FluSeason");
        }
    }
}
