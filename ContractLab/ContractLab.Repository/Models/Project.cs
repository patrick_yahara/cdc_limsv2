using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Project : ConcreteEntityBase<int>
    {
        public Project()
        {
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
