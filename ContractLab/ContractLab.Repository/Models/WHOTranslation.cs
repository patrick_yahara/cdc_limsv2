using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class WHOTranslation : ConcreteEntityBase<int>
    {
        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Heading { get; set; }
        public string MatchExpression { get; set; }
        public string ReplacementValue { get; set; }
        public string CommentHeading { get; set; }
        public string CommentValue { get; set; }

	}
}
