using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Manifest : ConcreteEntityBase<int>
    {
        public Manifest()
        {
            Aliquots = new List<Aliquot>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public int StatusId { get; set; }
        public bool IsVisible { get; set; }
        public string Comments { get; set; }
        public DateTime? DateShipped { get; set; }
        public string TrackingNumber { get; set; }
        public virtual ICollection<Aliquot> Aliquots { get; set; }
        public virtual Status Status { get; set; }

	}
}
