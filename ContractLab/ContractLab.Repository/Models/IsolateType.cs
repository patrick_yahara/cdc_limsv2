using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class IsolateType : ConcreteEntityBase<int>
    {
        public IsolateType()
        {
            Isolates = new List<Isolate>();
        }

        public override int Id { get; set; }
        public string DisplayName { get; set; }
        public string SysName { get; set; }
        public bool IsVisible { get; set; }
        public virtual ICollection<Isolate> Isolates { get; set; }

	}
}
