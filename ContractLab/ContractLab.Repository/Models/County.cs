using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class County : ConcreteEntityBase<int>
    {
        public County()
        {
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
