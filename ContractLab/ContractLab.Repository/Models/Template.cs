using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Template : ConcreteEntityBase<int>
    {
        public override int Id { get; set; }
        public string TemplateType { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public int? LabId { get; set; }
        public virtual Lab Lab { get; set; }

	}
}
