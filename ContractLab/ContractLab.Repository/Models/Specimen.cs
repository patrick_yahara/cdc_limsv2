using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Specimen : ConcreteEntityBase<int>
    {
        public Specimen()
        {
            Isolates = new List<Isolate>();
            SpecimenNotes = new List<SpecimenNote>();
            Transmissions = new List<Transmission>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string CSID { get; set; }
        public string CUID { get; set; }
        public int? ProjectId { get; set; }
        public string SpecimenID { get; set; }
        public DateTime? DateCollected { get; set; }
        public bool DateCollectedEstimated { get; set; }
        public string PatientAge { get; set; }
        public string PatientAgeUnits { get; set; }
        public string PatientSex { get; set; }
        public string Activity { get; set; }
        public string PassageHistory { get; set; }
        public string SubType { get; set; }
        public string SpecimenSource { get; set; }
        public string InitialStorage { get; set; }
        public string CompCSID1 { get; set; }
        public string CompCSID2 { get; set; }
        public string OtherLocation { get; set; }
        public string DrugResistant { get; set; }
        public string Deceased { get; set; }
        public string SpecialStudy { get; set; }
        public string ContractLabFrom { get; set; }
        public string NursingHome { get; set; }
        public string Vaccinated { get; set; }
        public string Travel { get; set; }
        public string ReasonForSubmission { get; set; }
        public string Comments { get; set; }
        public int StatusId { get; set; }
        public string SampleClass { get; set; }
        public DateTime? DateInoculated { get; set; }
        public int PackageId { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CountyId { get; set; }
        public int? LabId { get; set; }
        public string TravelCity { get; set; }
        public string HostCellLine { get; set; }
        public string Substrate { get; set; }
        public string PassageNumber { get; set; }
        public string PCRInfA { get; set; }
        public string PCRpdmH1 { get; set; }
        public string PCRH3 { get; set; }
        public string PCRInfB { get; set; }
        public string PCRBVic { get; set; }
        public string PCRBYam { get; set; }
        public string City { get; set; }
        public DateTime? TravelReturnDate { get; set; }
        public int? FluSeason { get; set; }
        public string PCRpdmInfA { get; set; }
        public string TravelCountry { get; set; }
        public string TravelState { get; set; }
        public string TravelCounty { get; set; }
        public string WHOSubmittedSubtype { get; set; }
        public virtual Country Country { get; set; }
        public virtual County County { get; set; }
        public virtual ICollection<Isolate> Isolates { get; set; }
        public virtual Lab Lab { get; set; }
        public virtual Package Package { get; set; }
        public virtual Project Project { get; set; }
        public virtual State State { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<SpecimenNote> SpecimenNotes { get; set; }
        public virtual ICollection<Transmission> Transmissions { get; set; }

	}
}
