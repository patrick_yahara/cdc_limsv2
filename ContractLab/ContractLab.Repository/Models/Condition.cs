using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Condition : ConcreteEntityBase<int>
    {
        public Condition()
        {
            Synonyms = new List<Synonym>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LitsQuestionId { get; set; }
        public int CategoryId { get; set; }
        public string Status { get; set; }
        public int Sorter { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Synonym> Synonyms { get; set; }

	}
}
