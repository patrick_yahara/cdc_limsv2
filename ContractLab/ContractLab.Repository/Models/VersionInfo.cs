using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class VersionInfo : ConcreteEntityBase<long>
    {
        public long Version { get; set; }
        public DateTime? AppliedOn { get; set; }
        public string Description { get; set; }

		public override long Id
		{
			get
			{
				return Version;
			}
			set
			{
				Version = value;
			}
		}
	}
}
