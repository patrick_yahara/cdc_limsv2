using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class SpecimenNote : ConcreteEntityBase<int>
    {
        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int SpecimenId { get; set; }
        public int NoteCategoryId { get; set; }
        public string Note { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public virtual NoteCategory NoteCategory { get; set; }
        public virtual Specimen Specimen { get; set; }
	}
}
