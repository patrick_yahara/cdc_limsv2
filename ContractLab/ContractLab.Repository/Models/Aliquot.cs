using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Aliquot : ConcreteEntityBase<int>
    {
        public Aliquot()
        {
            Transmissions = new List<Transmission>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int IsolateId { get; set; }
        public string CUID { get; set; }
        public string TestCode { get; set; }
        public int? ManifestId { get; set; }
        public bool IsStoredLocally { get; set; }
        public int? StatusId { get; set; }
        public bool RedshiftTracking { get; set; }
        public virtual Isolate Isolate { get; set; }
        public virtual Manifest Manifest { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Transmission> Transmissions { get; set; }

	}
}
