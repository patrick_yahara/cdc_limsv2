using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Country : ConcreteEntityBase<int>
    {
        public Country()
        {
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
        public string LongCode { get; set; }
        public int? NumberCode { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
