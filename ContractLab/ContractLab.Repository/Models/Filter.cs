using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Filter : ConcreteEntityBase<int>
    {
        public override int Id { get; set; }
        public string Display { get; set; }
        public string BaseObject { get; set; }
        public string FilterText { get; set; }
        public bool IsLabAvailable { get; set; }
	}
}
