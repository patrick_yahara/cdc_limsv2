using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Isolate : ConcreteEntityBase<int>
    {
        public Isolate()
        {
            Aliquots = new List<Aliquot>();
            Transmissions = new List<Transmission>();
        }

        public override int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] VersionColumn { get; set; }
        public int SpecimenId { get; set; }
        public string PassageLevel { get; set; }
        public DateTime? DateHarvested { get; set; }
        public int? Titer { get; set; }
        public string RedBloodCellType { get; set; }
        public DateTime? DateInoculated { get; set; }
        public int IsolateTypeId { get; set; }
        public string Comments { get; set; }
        public int? StatusId { get; set; }
        public virtual ICollection<Aliquot> Aliquots { get; set; }
        public virtual IsolateType IsolateType { get; set; }
        public virtual Specimen Specimen { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Transmission> Transmissions { get; set; }

	}
}
