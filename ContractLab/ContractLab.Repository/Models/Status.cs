using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace ContractLab.Repository.Model
{
    public class Status : ConcreteEntityBase<int>
    {
        public Status()
        {
            Aliquots = new List<Aliquot>();
            Isolates = new List<Isolate>();
            Manifests = new List<Manifest>();
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string SysName { get; set; }
        public string DisplayName { get; set; }
        public virtual ICollection<Aliquot> Aliquots { get; set; }
        public virtual ICollection<Isolate> Isolates { get; set; }
        public virtual ICollection<Manifest> Manifests { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }

	}
}
