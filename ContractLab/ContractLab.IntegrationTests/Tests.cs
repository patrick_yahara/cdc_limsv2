﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContractLab.Repository;
using ContractLab.DataService.Interfaces;
using ContractLab.DataService.Modules;
using NUnit.Framework;
using Shouldly;
using Yahara.Core.Repository;
using Yahara.ServiceInfrastructure.Messages;

namespace ContractLab.IntegrationTests
{
    [TestFixture]
    public class DataServiceValidationTests
    {
        [Test]
        public void CanConstructInfrastructure_ContextSource()
        {
            var connection = ConfigurationManager.ConnectionStrings["ContractLabEFModel"].ConnectionString;
            var contextSource = new ContractLabDbContextSource(new BasicDatabaseSettings
            {
                ConnectionString = connection
            });

            contextSource.ShouldNotBe(null);
            contextSource.GetConfigurationsNamespace().ShouldNotBeNullOrEmpty();
            contextSource.GetConfigurationsNamespace().ShouldBe("ContractLab.Repository.Models.Mapping");

            var context = contextSource.CreateContext();
            context.ShouldNotBe(null);
            context.Database.Exists().ShouldBe(true);
        }

        [Test]
        public void CanGetProjectListFromService()
        {
            var connection = ConfigurationManager.ConnectionStrings["ContractLabEFModel"].ConnectionString;
            var contextSource = new ContractLabDbContextSource(new BasicDatabaseSettings
            {
                ConnectionString = connection
            });

            var service = new ContractLab.DataService.ContractLabDataService(contextSource, null);

            var projects = service.ProjectList();
            projects.Success.ShouldBe(true);
        }
        
        [Test]
        public void CanGetSingleProjectFromService()
        {
            var connection = ConfigurationManager.ConnectionStrings["ContractLabEFModel"].ConnectionString;
            var contextSource = new ContractLabDbContextSource(new BasicDatabaseSettings
            {
                ConnectionString = connection
            });

            var service = new ContractLab.DataService.ContractLabDataService(contextSource, null);

            var project = service.Project(new MessageBase<int>() { Data = 1 });
            project.Success.ShouldBe(true);
        }
    }
}
