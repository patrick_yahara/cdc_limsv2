﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZebraPrinterBridge
{
    public class HttpServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerRequest, BridgeResponse> _responderMethod;

        public HttpServer(string[] prefixes, Func<HttpListenerRequest, BridgeResponse> method)
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException(
                    "Needs Windows XP SP2, Server 2003 or later.");
 
            //// URI prefixes are required, for example 
            //// "http://localhost:8080/index/".
            //if (prefixes == null || prefixes.Length == 0)
            //    throw new ArgumentException("prefixes");
 
            // A responder method is required
            if (method == null)
                throw new ArgumentException("method");
 
            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);
 
            _responderMethod = method;
            _listener.Start();
        }

        public HttpServer(Func<HttpListenerRequest, BridgeResponse> method, params string[] prefixes)
            : this(prefixes, method) { }
 
        public void Run()
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                //Console.WriteLine("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                if (ctx.Request.HttpMethod == "OPTIONS")
                                {
                                    ctx.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
                                    ctx.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                                    ctx.Response.AddHeader("Access-Control-Max-Age", "1728000");
                                }
                                ctx.Response.AppendHeader("Access-Control-Allow-Origin", "*");
                                var bridgeResponse = _responderMethod(ctx.Request);
                                ctx.Response.StatusCode = (int)bridgeResponse.StatusCode;
                                byte[] buf = Encoding.UTF8.GetBytes(bridgeResponse.ResponseText);
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch ( Exception ex ) // suppress any exceptions
                            {
                                Console.WriteLine(ex.Message);
                            }
                            finally
                            {
                                
                                // always close the stream
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch { } // suppress any exceptions
            });
        }
 
        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }
    }
}
