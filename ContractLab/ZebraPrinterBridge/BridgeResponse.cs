﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ZebraPrinterBridge
{
    public class BridgeResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public String ResponseText { get; set; }
    }
}
