﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;


namespace ZebraPrinterBridge
{
	/// <summary>
	/// 
	/// </summary>
	class ContextMenus
	{
		/// <summary>
		/// Creates this instance.
		/// </summary>
		/// <returns>ContextMenuStrip</returns>
		public ContextMenuStrip Create()
		{
            return Create(string.Empty, string.Empty, null);
		}

		/// <summary>
		/// Processes a menu item.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		void Exit_Click(object sender, EventArgs e)
		{
			// Quit without further ado.
			Application.Exit();
		}

        internal ContextMenuStrip Create(string address, string printer, ToolStripMenuItem refresh)
        {
            // Add the default menu options.
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem item;
            ToolStripSeparator sep;

            item = new ToolStripMenuItem();
            item.Text = address;
            menu.Items.Add(item);

            if (!string.IsNullOrEmpty(printer))
            {
                item = new ToolStripMenuItem();
                item.Text = printer;
                menu.Items.Add(item);
            }


            if (refresh != null)
            {
                // Separator.
                sep = new ToolStripSeparator();
                menu.Items.Add(sep);
                menu.Items.Add(refresh);
            }

            // Separator.
            sep = new ToolStripSeparator();
            menu.Items.Add(sep);

            // Exit.
            item = new ToolStripMenuItem();
            item.Text = "Exit";
            item.Click += new System.EventHandler(Exit_Click);
            //item.Image = Resources.Exit;
            menu.Items.Add(item);

            return menu;
        }
    }
}