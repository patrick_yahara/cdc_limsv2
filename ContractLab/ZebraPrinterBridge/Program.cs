﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommandLine;
using CommandLine.Text;

namespace ZebraPrinterBridge
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            var options = new Options();
            var parser = new Parser();
            if (parser.ParseArguments(args, options) && options.tray)
            {
                if (!SingleInstance.Start())
                {
                    SingleInstance.ShowFirstInstance();
                    return;
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                try
                {
                    // Show the system tray icon.
                    using (BridgeService pi = new BridgeService(args))
                    {
                        pi.Display();
                        // Make sure the application runs!
                        Application.Run();
                    }
                }
                catch (Exception ex)
                {
                    Debugger.Log(0, "", ex.Message);
                }

                SingleInstance.Stop();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
            { 
                new BridgeService(args) 
            };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
