﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZebraPrinterBridge
{
    /*
^XA
^XFE:ISOLATE.ZPL^FS
^FN1^FD1234567890^FS
^FN2^FDSSSSS^FS
^FN3^FDXX^FS
^FN4^FD2015^FS
^FN5^FDORG^FS
^FN6^FDS1^FS
^FN7^FDQQQQQQQQ^FS
^XZ
     * 
     */

    public class Labels
    {
        public string url { get; set; }
        public string preamble { get; set; }
        public string format { get; set; }
        public string postamble { get; set; }
        public List<Label> data { get; set; }
    }    

    public class Label
    {
        public Label() { }
        public string HA { get; set; } // 6
        public string FT { get; set; } // 3
        public string PH { get; set; } // 6
        public string DH { get; set; } // 10
        public string ST { get; set; } // 2
        public string CUIDHR { get; set; } // NA
        public string CUIDBC { get; set; } // NA
        public string CSID { get; set; } // 10
        public string SUB { get; set; } // 12
        public string SID { get; set; }// 12
        //public List<byte> GetPageForZebra(Com.SharpZebra.Printing.PrinterSettings ps, List<Point> positions, int bcW, int bcR, int bcH)
        //{
        //    if (bcW == 0)
        //    {
        //        bcW = 2;
        //    }
        //    if (bcR == 0)
        //    {
        //        bcR = 3;
        //    }
        //    if (bcH == 0)
        //    {
        //        bcH = 45;
        //    }
        //    //if (barCode == null)
        //    //{
        //        var barCode = new Point(220, 55);
        //    //}
        //    //if (fontSize == null)
        //    //{
        //        var fontSize = new Point(25, 24);
        //    //}
        //    if (positions == null)
        //    {
        //        positions = new List<Point>();
        //    }
        //    if (positions.Count == 0)
        //    {
        //        positions.Add(new Point(41, 441));
        //        positions.Add(new Point(90, 441));
        //        positions.Add(new Point(139, 441));
        //        positions.Add(new Point(187, 441));
        //        positions.Add(new Point(187, 371));
        //        positions.Add(new Point(187, 238));
        //        positions.Add(new Point(236, 441));
        //        positions.Add(new Point(41, 238));
        //    }
        //    Debug.Assert(positions.Count >= 8, String.Format("Only {0} positions supplied, need 8.", positions.Count)); 
        //    List<byte> page = new List<byte>();
        //    page.AddRange(ZPLCommands.ClearPrinter(ps));
        //    //page.AddRange(ZPLCommands.TextWrite(41, 441, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.CSID));
        //    //page.AddRange(ZPLCommands.TextWrite(90, 441, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.SID));
        //    //page.AddRange(ZPLCommands.TextWrite(139, 441, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.DH));
        //    //page.AddRange(ZPLCommands.TextWrite(187, 441, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.FT));
        //    //page.AddRange(ZPLCommands.TextWrite(187, 371, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.PH));
        //    //page.AddRange(ZPLCommands.TextWrite(187, 238, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.HA));
        //    //page.AddRange(ZPLCommands.TextWrite(236, 441, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_NORMAL, 15, 12, this.SUB));
        //    page.AddRange(ZPLCommands.TextWrite(positions[5].X, positions[5].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.HA));
        //    page.AddRange(ZPLCommands.TextWrite(positions[3].X, positions[3].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.FT));
        //    page.AddRange(ZPLCommands.TextWrite(positions[4].X, positions[4].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.PH));
        //    page.AddRange(ZPLCommands.TextWrite(positions[2].X, positions[2].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.DH));
        //    page.AddRange(ZPLCommands.TextWrite(positions[7].X, positions[7].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.ST));
        //    page.AddRange(ZPLCommands.TextWrite(positions[0].X, positions[0].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.CSID));
        //    page.AddRange(ZPLCommands.TextWrite(positions[6].X, positions[6].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.SUB));
        //    page.AddRange(ZPLCommands.TextWrite(positions[1].X, positions[1].Y, ElementDrawRotation.ROTATE_270_DEGREES, ZebraFont.STANDARD_SMALLEST, fontSize.X, fontSize.Y, this.SID));
        //    page.AddRange(Encoding.GetEncoding(850).GetBytes(String.Format(@"^BY{0},{1},{2}^FT{3},{4}^BCI,,Y,N^FD>;{5}^FS", bcW, bcR, bcH, barCode.X, barCode.Y, this.CUID)));
        //    page.AddRange(ZPLCommands.PrintBuffer(1));
        //    return page;
        //}
//^BY2,3,45^FT220,55^BCI,,Y,N
//^FD>;1234567890^FS
//^FT236,441^A0B,25,24^FH\^FDWWWWSUBWWWWW^FS
//^FT90,441^A0B,25,24^FH\^FDWWWWSIDWWWWW^FS
//^FT41,441^A0B,25,26^FH\^FDWWWCSIDWWW^FS
//^FT139,441^A0B,25,24^FH\^FDWWWWDHWWWW^FS
//^FT187,371^A0B,25,24^FH\^FDWWPHWW^FS
//^FT187,441^A0B,25,24^FH\^FDWFT^FS
//^FT187,238^A0B,25,24^FH\^FDWWHAWW^FS
//^FT50,155^A0B,25,24^FH\^FDST^FS
    }
}
