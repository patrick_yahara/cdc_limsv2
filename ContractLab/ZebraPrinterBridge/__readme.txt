﻿ZebraPrinterBridge.exe 
A windows application which acts as an interface to a connected Zebra label printer.
Can be run as an application, or as a service
It hosts an .NET HTTPListener that allows CORS Post request to it (from https or http depending on confiuration)
It queries the workstation for connected Zebra printers via setupapi.dll
It modifies objects sent via json to ZPL (Zebra Programming Language)
It writes the ZPL to the usb port via kernel32.dll

If run as an application, requires to be run as Administrator
If run as a service, needs to run as Local System, which the installer will set it to

Added 15 OCT 2015
It writes to the Event Log (Windows, Application, Source = "Zebra Printer Bridge") on Start, Stop and any errors

ZebraPrinterBridge.exe has three command line options 
(the second two can be used in the service also)
--tray 
A boolean flag, if present will run ZebraPrinterBridge.exe as a tray app and not a service
Should not be used when service is running

--printer
Not required
String; Used for debugging, when set the application will try to find the named printer, if not found
will use the first Zebra printer present

--port
Not required
Integer; Defaults to 8443 if not set. Used to specify which port the HTTPListener listens to
The HTTPListener is set to listen on all external TCP interfaces

Added 15 OCT 2015
--http
A boolean flag. Service now runs as https by default, to run in http mode, use --http

AIMSClawCA.pfx
A self-signed issuing authority and key, used by AIMSClawSignedByCA

AIMSClawSignedByCA.pfx
A self-signed certificate and key, used by ZebraPrinterBridge.exe to support https traffic

CommandLine.dll
Used to parse the command line

Newtonsoft.Json.dll
Used to deserialize the incoming json objects

To install the self-signed certificates:
See Certificates.doc
The application GUID is {f42006d3-944d-419e-88d0-670873a12a13}

To install the service:
From a command prompt, running as administrator, run _install.bat
If there are no errors, start the service (NET START "Zebra Printer Bridge" or from a UI)

To uninstall the service:
From a command prompt, running as administrator, run _uninstall.bat

To test connectivity with a UI:
Stop the service NET STOP "Zebra Printer Bridge"
From a command prompt, running as administrator, run ZebraPrinterBridge.exe --tray

13 OCT 2015
15 OCT 2015
To test connectivity with the service:
Navigate to https://<ipaddress>:<port> (or http://<ipaddress>:<port>)

To reset the printer connection to the service:
Navigate to https://<ipaddress>:<port>/Reset (or http://<ipaddress>:<port>/Reset)