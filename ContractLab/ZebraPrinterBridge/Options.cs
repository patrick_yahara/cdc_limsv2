﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZebraPrinterBridge
{
    public class Options
    {
        [Option(DefaultValue = false, HelpText = "Run in the system tray")]
        public bool tray { get; set; }

        [Option(DefaultValue = "", HelpText = "Zebra Printer Name (USB)")]
        public string printer { get; set; }

        [Option(DefaultValue = 8443, HelpText = "The port to use for responding to POST messages")]
        public int port { get; set; }

        [Option(DefaultValue = false, HelpText = "Run in http mode, not https")]
        public bool http { get; set; }
    }
}
