﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using ZebraPrinterBridge.Properties;
using System.Runtime.Serialization.Json;
using System.Drawing;
using System.ServiceProcess;
using CommandLine;
using CommandLine.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace ZebraPrinterBridge
{
    public class BridgeService : ServiceBase, IDisposable
    {
        private HttpServer _server;
        private NotifyIcon _ni;
        private ZebraUsbStream _directPrinter;
        private int _port;
        private string _printer;
        private string[] _args;
        private Options _options;
        public BridgeService()
        {
            this.ServiceName = "Zebra Printer Bridge";

            // * event logging
            this.AutoLog = false;
            ((ISupportInitialize)this.EventLog).BeginInit();
            if (!EventLog.SourceExists(this.ServiceName))
            {
                EventLog.CreateEventSource(this.ServiceName, "Application");
            }
            ((ISupportInitialize)this.EventLog).EndInit();
            this.EventLog.Source = this.ServiceName;
            this.EventLog.Log = "Application";

            _ni = new NotifyIcon();

            _printer = "";
            _port = 8443;

            Initialize();
            
        }

        private void Initialize()
        {
            _directPrinter = null;
            _server = null;

            if (!String.IsNullOrEmpty(_printer))
            {
                try
                {
                    _directPrinter = new ZebraUsbStream(_printer);
                }
                catch (Exception)
                {
                    try
                    {
                        _directPrinter = new ZebraUsbStream();
                    }
                    catch (Exception ex2)
                    {
                        //Debugger.Log(0, "", ex2.Message);
                        this.EventLog.WriteEntry("Error initializing printer " + _printer + ": " + ex2.Message);
                    }
                }
            }
            else
            {
                try
                {
                    _directPrinter = new ZebraUsbStream();
                }
                catch (Exception ex2)
                {
                    this.EventLog.WriteEntry("Error initializing printer: " + ex2.Message);
                }
            }

            var endpoint = String.Format(@"https://+:{0}/", _port);
            if (_options != null && _options.http)
            {
                endpoint = String.Format(@"http://+:{0}/", _port);
            }

            //var endpoint = String.Format(@"http://+:{0}/", _port);
            String[] prefixes = { endpoint };
            try
            {
                _server = new HttpServer(SendResponse, prefixes);
                _server.Run();
            }
            catch (Exception ex)
            {
                _server = null;
                this.EventLog.WriteEntry("Error starting HttpListener: " + ex.Message);
            }
        }

        public BridgeService(string[] args)
        {
            this.ServiceName = "Zebra Printer Bridge";

            // * event logging
            this.AutoLog = false;
            ((ISupportInitialize)this.EventLog).BeginInit();
            if (!EventLog.SourceExists(this.ServiceName))
            {
                EventLog.CreateEventSource(this.ServiceName, "Application");
            }
            ((ISupportInitialize)this.EventLog).EndInit();
            this.EventLog.Source = this.ServiceName;
            this.EventLog.Log = "Application";

            _ni = new NotifyIcon();
            
            _args = args;
            _ni = new NotifyIcon();

            var options = new Options();
            var parser = new Parser();
            if (parser.ParseArguments(args, options))
            {
                _options = options;
                _printer = options.printer;
                _port = options.port;
            } else
            { 
                _printer = "";
                _port = 8443;
            }

            Initialize();
        }

        private BridgeResponse SendResponse(HttpListenerRequest request)
        {
            var result = new BridgeResponse() { StatusCode = HttpStatusCode.Accepted, ResponseText = "" };
               
            #region POST
            if (request.HttpMethod == "POST")
            {
                if (_directPrinter != null)
                {
                    var body = request.InputStream;
                    var encoding = request.ContentEncoding;
                    var reader = new System.IO.StreamReader(body, encoding);
                    var s = reader.ReadToEnd();
                    var incoming = JsonConvert.DeserializeObject<Labels>(s);
                    foreach (var item in incoming.data)
                    {
                        var populatedZPL = String.Concat(incoming.preamble
                            , String.Format(incoming.format
                            , item.CUIDBC
                            , item.SUB
                            , item.SID
                            , item.CSID
                            , item.DH
                            , item.PH
                            , item.FT
                            , item.HA
                            , item.ST
                            , item.CUIDHR
                            ), incoming.postamble);
                        var command = Encoding.GetEncoding(850).GetBytes(populatedZPL);
                        try
                        {
                            _directPrinter.Write(command, 0, command.Length);
                        }
                        catch (Exception ex)
                        {
                            result.StatusCode = HttpStatusCode.InternalServerError;
                            result.ResponseText = ex.Message;
                        }
                    }
                    body.Close();
                    reader.Close();
                }
                else
                {
                    result.ResponseText = "Lost Connection To Printer";
                    result.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else
            { 
                //nothing to do
            }
            #endregion

            #region GET
            if (request.HttpMethod == "GET")
            {
                if (request.Url.AbsolutePath.ToUpper().Contains("RESET"))
                {
                    this.RefreshZebra();
                    System.Threading.Thread.Sleep(3000);
                }
                if (_directPrinter != null)
                {
                    try
                    {
                        result.ResponseText = _directPrinter.ConnectedPrinter;
                        var command = Encoding.GetEncoding(850).GetBytes("~HQES");
                        _directPrinter.Write(command, 0, command.Length);
                        byte[] bytes = new byte[512];
                        int n = _directPrinter.Read(bytes, 0, 512);
                        var resultA = new byte[n];
                        Array.Copy(bytes, resultA, n);
                        result.ResponseText = String.Format("<HTML><BODY>{0}<br>{1}</BODY></HTML>", Encoding.UTF8.GetString(resultA), _directPrinter.ConnectedPrinter);
                    }
                    catch (Exception ex)
                    {
                        result.ResponseText = String.Format("<HTML><BODY>{0}<br>Check printer connectivity, and try /RESET/ </BODY></HTML>", ex.Message);
                    }
                    
                } else
                {
                    result.ResponseText = "<HTML><BODY>Lost Connection To Printer</BODY></HTML>";
                }
            }
            #endregion
            return result;
        }

        public void Display()
        {
            string printer = null;
            // Put the icon in the system tray and allow it react to mouse clicks.			
            _ni.MouseClick += new MouseEventHandler(ni_MouseClick);

            var bmp = ZebraPrinterBridge.Properties.Resources.print;
            if (_server == null || !System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                bmp = ZebraPrinterBridge.Properties.Resources.print_red;
            } else
                if (_directPrinter == null)
                {
                    bmp = ZebraPrinterBridge.Properties.Resources.print_yellow;
                }
                else
                {
                    printer = _directPrinter.ConnectedPrinter;
                }
            _ni.Icon = Icon.FromHandle(bmp.GetHicon());
            var address = !String.IsNullOrEmpty(Utility.GetLocalIPAddress()) ? System.Environment.NewLine + Utility.GetLocalIPAddress() + ":" + _port.ToString() : string.Empty;
            _ni.Text = String.Format(@"Zebra Printer Bridge{0}", address);
            _ni.Visible = true;

            var item = new ToolStripMenuItem();
            item.Text = "Refresh";
            item.Click += item_Click;

            _ni.ContextMenuStrip = new ContextMenus().Create(address, printer, item);
        }

        void item_Click(object sender, EventArgs e)
        {
            RefreshZebra();
        }

        public void RefreshZebra()
        {

            _directPrinter = null;
            string printer = "";
            
            try
            {
                _directPrinter = new ZebraUsbStream(printer);
            }
            catch (Exception)
            {
                printer = "";
                try
                {
                    _directPrinter = new ZebraUsbStream();
                    printer = _directPrinter.ConnectedPrinter;
                }
                catch (Exception ex2)
                {
                    printer = "";
                    this.EventLog.WriteEntry("Error initializing printer: " + ex2.Message);
                }
            }
            var bmp = ZebraPrinterBridge.Properties.Resources.print;
            if (_server == null || !System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                bmp = ZebraPrinterBridge.Properties.Resources.print_red;
            }
            else
            if (_directPrinter == null)
            {
                bmp = ZebraPrinterBridge.Properties.Resources.print_yellow;
            }
            else
            {
                printer = _directPrinter.ConnectedPrinter;
            }

            if (_options != null && _options.tray)
            {
                _ni.Icon = Icon.FromHandle(bmp.GetHicon());
                var address = !String.IsNullOrEmpty(Utility.GetLocalIPAddress()) ? System.Environment.NewLine + Utility.GetLocalIPAddress() + ":" + _port.ToString() : string.Empty;
                _ni.Text = String.Format(@"Zebra Printer Bridge{0}", address);

                var item = new ToolStripMenuItem();
                item.Text = "Refresh";
                item.Click += item_Click;
                _ni.ContextMenuStrip = null;
                _ni.ContextMenuStrip = new ContextMenus().Create(address, printer, item);
            }

        }

        void ni_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
            }
        }

        protected override void Dispose(bool disposing)
        {
            _printer = null;
            _server = null;
            _ni.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnStart(string[] args)
        {
            _args = args;
            _ni = new NotifyIcon();
            var options = new Options();
            var parser = new CommandLine.Parser();
            if (parser.ParseArguments(args, options))
            {
                _printer = options.printer;
                _port = options.port;
            }
            else
            {
                _printer = "";
                _port = 8443;
            }
            Initialize();

        }

        protected override void OnStop()
        {
            if (_server != null)
            {
                _server.Stop();
                _server = null;
                this.EventLog.WriteEntry("HttpListener Stopped");
            }
            base.OnStop();
        }

        private void InitializeComponent()
        {
            // 
            // BridgeService
            // 
            this.ServiceName = "Zebra Printer Bridge";

        }
    }
}
