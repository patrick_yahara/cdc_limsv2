﻿namespace ZebraPrinterBridge
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ZebraPrinterBridgeProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ZebraPrinterBridgeServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ZebraPrinterBridgeProcessInstaller
            // 
            this.ZebraPrinterBridgeProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ZebraPrinterBridgeProcessInstaller.Password = null;
            this.ZebraPrinterBridgeProcessInstaller.Username = null;
            // 
            // ZebraPrinterBridgeServiceInstaller
            // 
            this.ZebraPrinterBridgeServiceInstaller.Description = "Used to sent Post requests to USB connected Zebra printer";
            this.ZebraPrinterBridgeServiceInstaller.DisplayName = "Zebra Printer Bridge";
            this.ZebraPrinterBridgeServiceInstaller.ServiceName = "Zebra Printer Bridge";
            this.ZebraPrinterBridgeServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ZebraPrinterBridgeProcessInstaller,
            this.ZebraPrinterBridgeServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ZebraPrinterBridgeProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ZebraPrinterBridgeServiceInstaller;
    }
}