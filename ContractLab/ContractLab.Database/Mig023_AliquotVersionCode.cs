﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201603180000)]
    public class Mig023_AliquotVersionCode : Migration
    {
        public override void Up()
        {
            Create.Column("AliquotVersionCode").OnTable("Lab").AsString(50).Nullable();
        }

        public override void Down()
        {
            Delete.Column("AliquotVersionCode").FromTable("Lab");
        }
    }
}
