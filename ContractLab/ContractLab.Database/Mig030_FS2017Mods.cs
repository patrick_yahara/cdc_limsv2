﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201609200000)]
    public class Mig030_FS2017Mods : Migration
    {
        public override void Up()
        {
            Delete.ForeignKey("FK_Specimen_TravelCountryId_Country_Id").OnTable("Specimen");
            Delete.ForeignKey("FK_Specimen_TravelCountyId_County_Id").OnTable("Specimen");
            Delete.ForeignKey("FK_Specimen_TravelStateId_State_Id").OnTable("Specimen");
            Delete.Column("TravelCountryId").FromTable("Specimen");
            Delete.Column("TravelStateId").FromTable("Specimen");
            Delete.Column("TravelCountyId").FromTable("Specimen");
            Create.Column("TravelCountry").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("TravelState").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("TravelCounty").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("WHOSubmittedSubtype").OnTable("Specimen").AsString(128).Nullable();
        }

        public override void Down()
        {
            Create.Column("TravelCountryId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("Country", "Id");
            Create.Column("TravelStateId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("State", "Id");
            Create.Column("TravelCountyId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("County", "Id");
            Delete.Column("TravelCountry").FromTable("Specimen");
            Delete.Column("TravelState").FromTable("Specimen");
            Delete.Column("TravelCounty").FromTable("Specimen");
            Delete.Column("WHOSubmittedSubtype").FromTable("Specimen");
        }
    }
}
