﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506080001)]
    public class Mig001_SecuritySchema : Migration
    {
        public override void Up()
        {
            Execute.Script(@"Scripts\Mig001_SecuritySchema.sql");
        }

        public override void Down()
        {
        }
    }
}
