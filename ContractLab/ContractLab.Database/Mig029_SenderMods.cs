﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201609160000)]
    public class Mig029_SenderMods : Migration
    {
        public override void Up()
        {
            Create.Column("FluSeason").OnTable("Sender").AsInt32().Nullable();
            Create.Column("FluSeason").OnTable("Contact").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("FluSeason").FromTable("Sender");
            Delete.Column("FluSeason").FromTable("Contact");
        }
    }
}
