﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201509010000)]
    public class Mig014_TemplateTable : Migration
    {
        public override void Up()
        {
            Create.Table("Template")
            .WithIdColumn()
            .WithColumn("TemplateType").AsString(50).NotNullable()
            .WithColumn("FileName").AsString(256).NotNullable()
            .WithColumn("File").AsBinary(int.MaxValue)
            ;
        }

        public override void Down()
        {
            Delete.Table("Template");
        }
    }
}
