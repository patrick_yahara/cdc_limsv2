﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506230000)]
    public class Mig007_PickListTables : Migration
    {
        public override void Up()
        {
            Create.Table("Category")
                        .WithIdColumn()
                        .WithTimeStamps()
                        .WithColumn("Name").AsString(255).NotNullable()
                        .WithColumn("Description").AsString(255).NotNullable()
                        .WithColumn("LitsQuestionId").AsString(50).NotNullable()
                        .WithColumn("AllowBlank").AsBoolean().NotNullable();
                        ;

            Create.Table("Condition")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Name").AsString(255).NotNullable()
            .WithColumn("Description").AsString(255).NotNullable()
            .WithColumn("LitsQuestionId").AsString(50).NotNullable()
            .WithColumn("CategoryId").AsInt32().NotNullable()
            .WithColumn("Status").AsString(20).NotNullable()
            .WithColumn("Sorter").AsInt32().NotNullable()
            ;
            Create.ForeignKey().FromTable("Condition").ForeignColumn("CategoryId").ToTable("Category").PrimaryColumn("Id");

            Create.Table("Synonym")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Name").AsString(255).NotNullable()
            .WithColumn("LitsQuestionId").AsString(50).NotNullable()
            .WithColumn("CategoryId").AsInt32().NotNullable()
            .WithColumn("ConditionName").AsString(255).NotNullable()
            .WithColumn("ConditionId").AsInt32().NotNullable()
            .WithColumn("Status").AsString(20).NotNullable()
            ;
            Create.ForeignKey().FromTable("Synonym").ForeignColumn("ConditionId").ToTable("Condition").PrimaryColumn("Id");
            Create.ForeignKey().FromTable("Synonym").ForeignColumn("CategoryId").ToTable("Category").PrimaryColumn("Id");

        }

        public override void Down()
        {
            Delete.Table("Synonym");
            Delete.Table("Condition");
            Delete.Table("Category");
        }

    }
}
