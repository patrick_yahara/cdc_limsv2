﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506080003)]
    public class Mig003_ProjectTable : Migration
    {
        public override void Up()
        {
            Create.Table("Project")
            .WithIdColumn()
            .WithTimeStamps()
            //.WithColumn("UserId").AsString(128).NotNullable()
            //.WithColumn("StatusId").AsInt32().NotNullable()
            .WithColumn("Name").AsString(128).NotNullable()
            ;
            //Create.ForeignKey().FromTable("Project").ForeignColumn("UserId").ToTable("AspNetUsers").PrimaryColumn("Id");
            //Create.ForeignKey().FromTable("Project").ForeignColumn("StatusId").ToTable("Status").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("Project");
        }
    }
}
