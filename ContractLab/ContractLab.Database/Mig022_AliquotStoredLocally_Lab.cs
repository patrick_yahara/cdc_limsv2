﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201601191300)]
    public class Mig022_AliquotStoredLocally_Lab : Migration
    {
        public override void Up()
        {
            Create.Column("StoredLocallyCodes").OnTable("Lab").AsString(128).Nullable();
        }

        public override void Down()
        {
            Delete.Column("StoredLocallyCodes").FromTable("Lab");
        }
    }
}
