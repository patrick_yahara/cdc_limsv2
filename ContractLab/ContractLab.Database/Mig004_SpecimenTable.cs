﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506170000)]
    public class Mig004_SpecimenTable : Migration
    {
        public override void Up()
        {
            Create.Table("Specimen")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("CSID").AsString(128).NotNullable()
            .WithColumn("CUID").AsString(128).Nullable()
            .WithColumn("ProjectId").AsInt32().Nullable()
            .WithColumn("SpecimenID").AsString(255).Nullable()
            .WithColumn("DateCollected").AsDateTime().Nullable()
            .WithColumn("DateCollectedEstimated").AsBoolean().WithDefaultValue(false)
            .WithColumn("PatientAge").AsString(50).Nullable()
            .WithColumn("PatientAgeUnits").AsString(255).Nullable()
            .WithColumn("PatientSex").AsString(128).Nullable()
            .WithColumn("Activity").AsString(255).Nullable()
            .WithColumn("PassageHistory").AsString(128).Nullable()
            .WithColumn("SubType").AsString(128).Nullable()
            .WithColumn("SpecimenSource").AsString(255).Nullable()
            .WithColumn("InitialStorage").AsString(255).Nullable()
            .WithColumn("CompCSID1").AsString(128).Nullable()
            .WithColumn("CompCSID2").AsString(128).Nullable()
            .WithColumn("OtherLocation").AsString(255).Nullable()
            .WithColumn("DrugResistant").AsString(20).Nullable()
            .WithColumn("Deceased").AsString(20).Nullable()
            .WithColumn("SpecialStudy").AsString(128).Nullable()
            .WithColumn("ContractLabFrom").AsString(128).Nullable()
            .WithColumn("NursingHome").AsString(20).Nullable()
            .WithColumn("Vaccinated").AsString(20).Nullable()
            .WithColumn("Travel").AsString(255).Nullable()
            .WithColumn("ReasonForSubmission").AsString(255).Nullable()
            .WithColumn("Comments").AsString(512).Nullable()
            .WithColumn("StatusId").AsInt32().NotNullable()
            .WithColumn("SampleClass").AsString(255).Nullable()
            .WithColumn("DateInoculated").AsDateTime().Nullable()
            ;
            Create.ForeignKey().FromTable("Specimen").ForeignColumn("ProjectId").ToTable("Project").PrimaryColumn("Id");
            Create.ForeignKey().FromTable("Specimen").ForeignColumn("StatusId").ToTable("Status").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("Specimen");
        }

    }
}
