﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201601190000)]
    public class Mig021_AliquotStoredLocally : Migration
    {
        public override void Up()
        {
            Create.Column("IsStoredLocally").OnTable("Aliquot").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("IsStoredLocally").FromTable("Aliquot");
        }
    }
}
