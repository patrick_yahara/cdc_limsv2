﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201510030000)]
    public class Mig016_ManifestTable : Migration
    {
        public override void Up()
        {
            Create.Table("Manifest")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Name").AsString(128).NotNullable()
            .WithColumn("StatusId").AsInt32().NotNullable()
            .WithColumn("IsVisible").AsBoolean().WithDefaultValue(true)
            .WithColumn("Comments").AsString(2048).Nullable()
            ;
            Create.Column("ManifestId").OnTable("Aliquot").AsInt32().Nullable().ForeignKey("Manifest", "Id");
            Create.ForeignKey().FromTable("Manifest").ForeignColumn("StatusId").ToTable("Status").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Column("ManifestId").FromTable("Aliquot");
            Delete.Table("Manifest");
        }
    }
}
