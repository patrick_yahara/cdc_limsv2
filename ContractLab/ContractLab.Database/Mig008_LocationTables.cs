﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506240000)]
    public class Mig008_LocationTables : Migration
    {
        public override void Up()
        {
            Create.Table("Country")
            .WithIdColumn()
            .WithColumn("Name").AsString(128).NotNullable()
            .WithColumn("ShortCode").AsString(10).Nullable()
            .WithColumn("LongCode").AsString(10).Nullable()
            .WithColumn("NumberCode").AsInt32().Nullable()
            ;
            Create.Column("CountryId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("Country", "Id");

            Create.Table("State")
            .WithIdColumn()
            .WithColumn("Name").AsString(128).NotNullable()
            .WithColumn("Abbreviation").AsString(2).NotNullable()
            ;
            Create.Column("StateId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("State", "Id");

            Create.Table("County")
            .WithIdColumn()
            .WithColumn("Name").AsString(128).NotNullable()
            .WithColumn("StateId").AsInt32().NotNullable()
            ;
            Create.ForeignKey().FromTable("County").ForeignColumn("StateId").ToTable("State").PrimaryColumn("Id");
            Create.Column("CountyId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("County", "Id");

            Execute.Script("LocationsInsert.usql");
        
        }

        public override void Down()
        {
            Delete.Table("County");
            Delete.Table("State");
            Delete.Table("Country");
        }
    }
}
