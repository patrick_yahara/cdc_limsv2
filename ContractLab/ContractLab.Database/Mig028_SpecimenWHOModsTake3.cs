﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201608260000)]
    public class Mig028_SpecimenWHOModsTake3 : Migration
    {
        public override void Up()
        {
            // * specimen fields
            Create.Column("PCRpdmInfA").OnTable("Specimen").AsString(10).Nullable();
            Delete.Column("PCRpdmInf").FromTable("Specimen");
        }

        public override void Down()
        {
            // * specimen fields
            Delete.Column("PCRpdmInfA").FromTable("Specimen");
            Create.Column("PCRpdmInf").OnTable("Specimen").AsString(10).Nullable();
        }
    }
}
