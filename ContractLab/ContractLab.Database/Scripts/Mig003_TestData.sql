﻿DECLARE @CreatedBy varchar(128)
SET @CreatedBy = '8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39'
INSERT INTO ContractLab.dbo.Sender (CompanyName, [Address], City, [State], [Country], RasClientId, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)
VALUES ('Division of Virology & Immunology','Labs Admin.
MD Dept. of Health & Mental Hygiene 201 W. Preston St., Rm. 4A6','Baltimore','MD','US','041', @CreatedBy, GETUTCDATE(), @CreatedBy, GETUTCDATE())
GO
SET IDENTITY_INSERT ContractLab.dbo.Package ON
GO
DECLARE @CreatedBy varchar(128)
SET @CreatedBy = '8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39'
DECLARE @PackageId int
SET @PackageId = 1
DECLARE @SenderId int
SET @SenderId = (SELECT TOP 1 Id FROM Sender WHERE RasClientId = '041')
IF EXISTS(SELECT * FROM Package WHERE Id = @PackageId) DELETE Package WHERE Id = @PackageId
INSERT INTO Package (Id, SenderId, Name, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, DateReceived, SpecimenCondition) 
VALUES (@PackageId, @SenderId, 'Foo', @CreatedBy, GETUTCDATE(), @CreatedBy, GETUTCDATE(), '8/31/2015', 'Frozen')
GO
SET IDENTITY_INSERT ContractLab.dbo.Package OFF
GO
SET IDENTITY_INSERT ContractLab.dbo.Specimen ON
GO
DISABLE TRIGGER dbo.trg_SpecimenINSERT ON dbo.Specimen;
GO
DECLARE @CreatedBy varchar(128)
SET @CreatedBy = 'f2bdc804-bcd7-47a0-8566-52c0df94d03c'
DECLARE @StatusId int
SET @StatusId = (SELECT TOP 1 Id FROM Status WHERE SysName LIKE 'INPROGRESS')
DECLARE @PackageId int
SET @PackageId = 1
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (1
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123456'
           ,'MKYD0001'
           ,NULL
           ,'specimenIDno1'
           ,'8/1/2015'
           ,0
           ,'39'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'C1'
           ,'H1'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (2
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123457'
           ,'MKYD0002'
           ,NULL
           ,'specimenIDno2'
           ,'8/1/2015'
           ,0
           ,'40'
           ,'Years'
           ,'Female'
           ,'Local'
           ,'C2'
           ,'H2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'Y'
           ,''
           ,'Y'
           ,'N'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'CO')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (3
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123458'
           ,'MKYD0003'
           ,NULL
           ,'specimenIDno3'
           ,'8/1/2015'
           ,0
           ,'41'
           ,'Years'
           ,'Female'
           ,'Regional'
           ,'C3'
           ,'H3'


           ,'NW'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'U'
           ,''
           ,'Y'
           ,'U'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'CO')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (4
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123459'
           ,'MKYD0004'
           ,NULL
           ,'specimenIDno4'
           ,'8/1/2015'
           ,0
           ,'42'
           ,'Years'
           ,'Female'
           ,'Sporadic'
           ,'C4'
           ,'H4'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (5
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123460'
           ,'MKYD0005'
           ,NULL
           ,'specimenIDno5'
           ,'8/1/2015'
           ,0
           ,'43'
           ,'Years'
           ,'Female'
           ,'Widespread'
           ,'C5'
           ,'H5'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (6
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123461'
           ,'MKYD0006'
           ,NULL
           ,'specimenIDno6'
           ,'8/1/2015'
           ,0
           ,'44'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1'
           ,'Bvic'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'GA')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (7
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123462'
           ,'MKYD0007'
           ,NULL
           ,'specimenIDno7'
           ,'8/1/2015'
           ,0
           ,'45'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S2'
           ,'Byam'


           ,'BAL'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (8
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123463'
           ,'MKYD0008'
           ,NULL
           ,'specimenIDno8'
           ,'8/1/2015'
           ,0
           ,'46'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S3'
           ,'H1N1'


           ,'NW'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (9
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123464'
           ,'MKYD0009'
           ,NULL
           ,'specimenIDno9'
           ,'8/1/2015'
           ,0
           ,'47'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S4'
           ,'H3N2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (10
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123465'
           ,'MKYD0010'
           ,NULL
           ,'specimenIDno10'
           ,'8/1/2015'
           ,0
           ,'48'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S5'
           ,'B'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Surveillance'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'CO')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (11
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123466'
           ,'MKYD0011'
           ,NULL
           ,'specimenIDno11'
           ,'8/1/2015'
           ,0
           ,'49'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C1'
           ,'H1'


           ,'BP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (12
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123467'
           ,'MKYD0012'
           ,NULL
           ,'specimenIDno12'
           ,'8/1/2015'
           ,0
           ,'50'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C2'
           ,'H2'


           ,'CSF'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (13
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123468'
           ,'MKYD0013'
           ,NULL
           ,'specimenIDno13'
           ,'8/1/2015'
           ,0
           ,'51'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C3'
           ,'H3'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'DC')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (14
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123469'
           ,'MKYD0014'
           ,NULL
           ,'specimenIDno14'
           ,'8/1/2015'
           ,0
           ,'52'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C4'
           ,'H4'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Other'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (15
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123470'
           ,'MKYD0015'
           ,NULL
           ,'specimenIDno15'
           ,'8/1/2015'
           ,0
           ,'53'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C5'
           ,'H5'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (16
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123471'
           ,'MKYD0016'
           ,NULL
           ,'specimenIDno16'
           ,'8/1/2015'
           ,0
           ,'54'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C6'
           ,'Bvic'


           ,'TI'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Study Sample'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (17
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123472'
           ,'MKYD0017'
           ,NULL
           ,'specimenIDno17'
           ,'8/1/2015'
           ,0
           ,'55'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C7'
           ,'Byam'


           ,'TS'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (18
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123473'
           ,'MKYD0018'
           ,NULL
           ,'specimenIDno18'
           ,'8/1/2015'
           ,0
           ,'56'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,'S1/C8'
           ,'H1N1'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,''
           ,''
           ,'Y'
           ,''
           ,'Guam'
           ,'Surveillance'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (19
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123474'
           ,'MKYD0019'
           ,NULL
           ,'specimenIDno19'
           ,'8/1/2015'
           ,0
           ,'57'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H3N2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (20
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123475'
           ,'MKYD0020'
           ,NULL
           ,'specimenIDno20'
           ,'8/1/2015'
           ,0
           ,'58'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'B'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (21
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123476'
           ,'MKYD0021'
           ,NULL
           ,'specimenIDno21'
           ,'8/1/2015'
           ,0
           ,'59'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H1'


           ,'NS'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (22
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123477'
           ,'MKYD0022'
           ,NULL
           ,'specimenIDno22'
           ,'8/1/2015'
           ,0
           ,'60'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (23
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123478'
           ,'MKYD0023'
           ,NULL
           ,'specimenIDno23'
           ,'8/1/2015'
           ,0
           ,'61'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H3'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (24
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123479'
           ,'MKYD0024'
           ,NULL
           ,'specimenIDno24'
           ,'8/1/2015'
           ,0
           ,'62'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H4'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (25
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123480'
           ,'MKYD0025'
           ,NULL
           ,'specimenIDno25'
           ,'8/1/2015'
           ,0
           ,'63'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H5'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (26
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123481'
           ,'MKYD0026'
           ,NULL
           ,'specimenIDno26'
           ,'8/1/2015'
           ,0
           ,'64'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'Bvic'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (27
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123482'
           ,'MKYD0027'
           ,NULL
           ,'specimenIDno27'
           ,'8/1/2015'
           ,0
           ,'65'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'Byam'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (28
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123483'
           ,'MKYD0028'
           ,NULL
           ,'specimenIDno28'
           ,'8/1/2015'
           ,0
           ,'66'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H1N1'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (29
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123484'
           ,'MKYD0029'
           ,NULL
           ,'specimenIDno29'
           ,'8/1/2015'
           ,0
           ,'67'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H3N2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (30
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123485'
           ,'MKYD0030'
           ,NULL
           ,'specimenIDno30'
           ,'8/1/2015'
           ,0
           ,'68'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'B'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (31
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123486'
           ,'MKYD0031'
           ,NULL
           ,'specimenIDno31'
           ,'8/1/2015'
           ,0
           ,'69'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H1'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
INSERT INTO [dbo].[Specimen]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[CSID]
           ,[CUID]
           ,[ProjectId]
           ,[SpecimenID]
           ,[DateCollected]
           ,[DateCollectedEstimated]
           ,[PatientAge]
           ,[PatientAgeUnits]
           ,[PatientSex]
           ,[Activity]
           ,[PassageHistory]
           ,[SubType]


           ,[SpecimenSource]
           ,[InitialStorage]
           ,[CompCSID1]
           ,[CompCSID2]
           ,[OtherLocation]
           ,[DrugResistant]
           ,[Deceased]
           ,[SpecialStudy]
           ,[NursingHome]
           ,[Vaccinated]
           ,[Travel]
           ,[ReasonForSubmission]
           ,[Comments]
           ,[StatusId]
           ,[PackageId]
           ,[CountryId]
           ,[StateId]
           ,[CountyId])
     VALUES
           (32
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,'2000123487'
           ,'MKYD0032'
           ,NULL
           ,'specimenIDno32'
           ,'8/1/2015'
           ,0
           ,'70'
           ,'Years'
           ,'Female'
           ,'No activity'
           ,''
           ,'H2'


           ,'NP'
           ,NULL
           ,'2000123457'
           ,'2000123458'
           ,'grocery store'
           ,'N'
           ,'N'
           ,''
           ,'Y'
           ,'Y'
           ,'Guam'
           ,'Antiviral-Diagnosis'
           ,'comments'
           ,@StatusId
           ,@PackageId
           ,(SELECT TOP 1 Id FROM [Country] WHERE ShortCode LIKE 'US')
           ,(SELECT TOP 1 Id FROM [State] WHERE Abbreviation LIKE 'AZ')
           ,NULL)
GO
ENABLE TRIGGER dbo.trg_SpecimenINSERT ON dbo.Specimen;
GO
SET IDENTITY_INSERT ContractLab.dbo.Specimen OFF
GO
SET IDENTITY_INSERT ContractLab.dbo.Isolate ON
GO
DECLARE @CreatedBy varchar(128)
SET @CreatedBy = '8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39'
DECLARE @IsolateTypeId int
SET @IsolateTypeId = (SELECT TOP 1 Id FROM IsolateType WHERE SysName LIKE 'ORIGINAL')
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (1
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,1
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (2
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,2
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (3
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,3
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (4
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,4
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (5
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,5
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (6
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,6
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (7
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,7
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (8
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,8
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (9
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,9
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (10
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,10
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (11
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,11
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (12
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,12
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (13
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,13
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (14
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,14
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (15
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,15
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (16
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,16
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (17
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,17
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (18
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,18
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (19
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,19
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (20
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,20
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (21
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,21
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (22
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,22
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (23
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,23
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (24
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,24
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (25
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,25
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (26
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,26
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (27
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,27
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (28
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,28
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (29
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,29
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (30
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,30
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (31
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,31
           ,@IsolateTypeId)
INSERT INTO [dbo].[Isolate]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[SpecimenId]
           ,[IsolateTypeId])
     VALUES
           (32
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,32
           ,@IsolateTypeId)
GO
SET IDENTITY_INSERT ContractLab.dbo.Isolate OFF
GO
SET IDENTITY_INSERT ContractLab.dbo.Aliquot ON
GO
DISABLE TRIGGER dbo.trg_AliquotINSERT ON dbo.Aliquot;
GO
DECLARE @CreatedBy varchar(128)
SET @CreatedBy = '8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39'
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           1
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,1
           ,'MKYD0001'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           2
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,2
           ,'MKYD0002'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           3
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,3
           ,'MKYD0003'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           4
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,4
           ,'MKYD0004'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           5
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,5
           ,'MKYD0005'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           6
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,6
           ,'MKYD0006'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           7
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,7
           ,'MKYD0007'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           8
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,8
           ,'MKYD0008'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           9
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,9
           ,'MKYD0009'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           10
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,10
           ,'MKYD0010'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           11
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,11
           ,'MKYD0011'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           12
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,12
           ,'MKYD0012'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           13
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,13
           ,'MKYD0013'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           14
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,14
           ,'MKYD0014'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           15
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,15
           ,'MKYD0015'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           16
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,16
           ,'MKYD0016'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           17
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,17
           ,'MKYD0017'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           18
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,18
           ,'MKYD0018'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           19
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,19
           ,'MKYD0019'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           20
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,20
           ,'MKYD0020'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           21
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,21
           ,'MKYD0021'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           22
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,22
           ,'MKYD0022'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           23
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,23
           ,'MKYD0023'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           24
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,24
           ,'MKYD0024'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           25
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,25
           ,'MKYD0025'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           26
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,26
           ,'MKYD0026'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           27
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,27
           ,'MKYD0027'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           28
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,28
           ,'MKYD0028'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           29
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,29
           ,'MKYD0029'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           30
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,30
           ,'MKYD0030'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           31
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,31
           ,'MKYD0031'
           ,'ORG')
INSERT INTO [dbo].[Aliquot]
           ([Id]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy]
           ,[IsolateId]
           ,[CUID]
           ,[TestCode])
     VALUES (
           32
           ,GETUTCDATE()
           ,@CreatedBy
           ,GETUTCDATE()
           ,@CreatedBy
           ,32
           ,'MKYD0032'
           ,'ORG')
GO
SET IDENTITY_INSERT ContractLab.dbo.Aliquot OFF
GO
DECLARE @CreatedBy varchar(128)
SET @CreatedBy = '8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39'
DECLARE @IsolateTypeId int
SET @IsolateTypeId = (SELECT TOP 1 Id FROM IsolateType WHERE SysName LIKE 'GROWN')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0001','ISA')
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0001'),'C2','8/31/2015','32','TRBC','8/26/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0001'),'C2','8/31/2015','32','TRBC','8/26/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0001'),'C2','8/31/2015','32','TRBC','8/26/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0001'),'C2','8/31/2015','32','TRBC','8/26/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0001'),'C2','8/31/2015','32','TRBC','8/26/2015',@IsolateTypeId)
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0007','ISA')
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0002'),'S2','9/1/2015','8','GPRBC','8/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0002'),'S2','9/1/2015','8','GPRBC','8/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0002'),'S2','9/1/2015','8','GPRBC','8/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0002'),'S2','9/1/2015','8','GPRBC','8/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0002'),'S2','9/1/2015','8','GPRBC','8/28/2015',@IsolateTypeId)
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0013','ISA')
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S1','9/2/2015','64','TRBC','9/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S1','9/2/2015','64','TRBC','9/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S1','9/2/2015','64','TRBC','9/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S1','9/2/2015','64','TRBC','9/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S1','9/2/2015','64','TRBC','9/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S2','9/3/2015','8','GPRBC','9/5/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S2','9/3/2015','8','GPRBC','9/5/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S2','9/3/2015','8','GPRBC','9/5/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S2','9/3/2015','8','GPRBC','9/5/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0003'),'C1/S2','9/3/2015','8','GPRBC','9/5/2015',@IsolateTypeId)
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0004'),'MKYY0024','ISA')
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0004'),'S2','8/1/2015','64','GPRBC','8/3/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0004'),'S2','8/1/2015','64','GPRBC','8/3/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0004'),'S2','8/1/2015','64','GPRBC','8/3/2015',@IsolateTypeId)
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0005'),'MKYY0028','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0006'),'MKYY0029','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0007'),'MKYY0030','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'ORIGINAL' WHERE A.CUID LIKE 'MKYD0008'),'MKYY0031','ISA')
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0008'),'S3','7/25/2015','32','TRBC','8/3/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0008'),'S3','7/25/2015','32','TRBC','8/3/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0008'),'S3','7/25/2015','32','TRBC','8/3/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0009') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0009'),'','','','','',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0010') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0010'),'','','','','',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0011') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0011'),'','','','','',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0012') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0012'),'','','','','',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0012') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0012'),'','8/1/2015','0','TRBC','8/1/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1','6/15/2015','128','GPRBC','7/25/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1','6/15/2015','128','GPRBC','7/25/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1','6/15/2015','128','GPRBC','7/25/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S2','7/1/2015','64','TRBC','7/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S2','7/1/2015','64','TRBC','7/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S2','7/1/2015','64','TRBC','7/28/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1/S1','8/1/2015','32','GPRBC','8/15/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1/S1','8/1/2015','32','GPRBC','8/15/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1/S1','8/1/2015','32','GPRBC','8/15/2015',@IsolateTypeId)
IF NOT EXISTS (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013') INSERT INTO [dbo].[Isolate] ([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[SpecimenId],[PassageLevel],[DateHarvested],[Titer],[RedBloodCellType],[DateInoculated],[IsolateTypeId]) VALUES (GETUTCDATE(),@CreatedBy,GETUTCDATE(),@CreatedBy, (SELECT TOP 1 A.Id FROM Specimen A WHERE A.CUID LIKE 'MKYD0013'),'S1/S1','8/1/2015','32','GPRBC','8/15/2015',@IsolateTypeId)

INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0002','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0003','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0004','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0005','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0001'),'MKYY0006','RHI')

INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0008','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0009','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0010','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0011','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0002'),'MKYY0012','RHI')

INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0014','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0015','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0016','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0017','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0018','RHI')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0019','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0020','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0021','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0022','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0003'),'MKYY0023','RHI')

INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004'),'MKYY0025','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004'),'MKYY0026','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0004'),'MKYY0027','RPS')




INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008'),'MKYY0032','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008'),'MKYY0033','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0008'),'MKYY0034','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0009'),'MKYY0035','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0010'),'MKYY0036','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0011'),'MKYY0037','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0012'),'','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0012'),'','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0040','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0041','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0042','RPS')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0043','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0044','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0045','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0046','REF')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0047','ISA')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0048','MET')
INSERT INTO [dbo].[Aliquot]([CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsolateId],[CUID],[TestCode]) VALUES (GETUTCDATE(), @CreatedBy, GETUTCDATE(), @CreatedBy, (SELECT TOP 1 B.Id FROM Specimen A JOIN Isolate B ON A.Id = B.SpecimenId JOIN IsolateType C ON C.Id = B.IsolateTypeId AND C.SysName = 'GROWN' WHERE A.CUID LIKE 'MKYD0013'),'MKYY0049','RPS')
GO
ENABLE TRIGGER dbo.trg_AliquotINSERT ON dbo.Aliquot;
GO
UPDATE Specimen SET SampleClass = 'Original'
UPDATE Specimen SET CUID = NULL
UPDATE Specimen SET ContractLabFrom = 'WI'