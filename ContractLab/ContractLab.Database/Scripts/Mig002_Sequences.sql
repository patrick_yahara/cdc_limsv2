--PRODUCTION
--CREATE SEQUENCE CSIDSequence AS BIGINT START WITH 3025623331 MINVALUE 3025623331 MAXVALUE 3025632330 INCREMENT BY 1 NO CACHE;
--CREATE SEQUENCE CUIDSequence AS BIGINT START WITH 2821032077127 MINVALUE 2821032077127 MAXVALUE 2821032140126 INCREMENT BY 1 NO CACHE;
--DEVELOPMENT
CREATE SEQUENCE CSIDSequence AS BIGINT START WITH 2016000000 MINVALUE 2016000000 MAXVALUE 2016999999 INCREMENT BY 1 NO CACHE;
CREATE SEQUENCE CUIDSequence AS BIGINT START WITH 806031402130 MINVALUE 806031402130 MAXVALUE 806032601855 INCREMENT BY 1 NO CACHE;

GO
CREATE FUNCTION fn_ToBase36 
(
	@val BIGINT
)
RETURNS NVARCHAR(128)
AS
BEGIN
	DECLARE @alldigits AS VARCHAR(36);
	SET @alldigits='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	DECLARE @Result NVARCHAR(128)
	SET @Result = ''
	WHILE @val>0
	BEGIN
	SET @Result = SUBSTRING(@alldigits,@val % 36 + 1,1) + @Result;
	SET @val = @val / 36;
	END
	RETURN @Result
END
GO
CREATE FUNCTION fn_ToBase10
(
	@val NVARCHAR(128)
)
RETURNS BIGINT
AS
BEGIN
	DECLARE @pos AS INT;
	DECLARE @i AS INT;
	DECLARE @alldigits AS VARCHAR(36);
	SET @alldigits='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	DECLARE @Result BIGINT
	SET @Result = 0
	SET @pos=LEN(@val) - 1;
	SET @i = 1;
	WHILE @i <= LEN(@val)
	BEGIN
		SET @Result = @Result + CAST((CHARINDEX(SUBSTRING(@val, @i, 1), @alldigits)-1)*POWER(CAST(36 AS BIGINT), @pos) AS BIGINT);
		SET @pos = @pos - 1;
		SET @i = @i + 1;
	END
	RETURN @Result
END
GO

CREATE TRIGGER dbo.trg_SpecimenINSERT 
   ON  dbo.Specimen 
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Specimen 
		SET CSID = NEXT VALUE FOR dbo.CSIDSequence
	WHERE Id IN (SELECT Id FROM inserted)
END
GO

CREATE TRIGGER dbo.trg_AliquotINSERT 
   ON  dbo.Aliquot 
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	UPDATE Aliquot 
		SET CUID = dbo.fn_ToBase36(NEXT VALUE FOR dbo.CUIDSequence)
	WHERE Id IN (SELECT Id FROM inserted)
END
GO