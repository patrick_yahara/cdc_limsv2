﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201701250001)]
    public class Mig032_TransmissionChanges : Migration
    {
        public override void Up()
        {
            Create.Column("StatusId").OnTable("Aliquot").AsInt32().Nullable().ForeignKey("Status", "Id");
            Create.Column("StatusId").OnTable("Isolate").AsInt32().Nullable().ForeignKey("Status", "Id");
        }

        public override void Down()
        {
            Delete.Column("StatusId").FromTable("Aliquot");
            Delete.Column("StatusId").FromTable("Isolate");

        }
    }
}
