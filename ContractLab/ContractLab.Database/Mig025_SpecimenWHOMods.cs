﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201606170000)]
    public class Mig025_SpecimenWHOMods : Migration
    {
        public override void Up()
        {
            // * specimen fields
            Create.Column("TravelCountryId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("Country", "Id");
            Create.Column("TravelStateId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("State", "Id");
            Create.Column("TravelCountyId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("County", "Id");
            Create.Column("TravelCity").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("HostCellLine").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("Substrate").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("PassageNumber").OnTable("Specimen").AsString(128).Nullable();
            Create.Column("PCRInfA").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRpdmInf").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRpdmH1").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRH3").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRInfB").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRBVic").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("PCRBYam").OnTable("Specimen").AsString(10).Nullable();
            Create.Column("City").OnTable("Specimen").AsString(128).Nullable();
        }

        public override void Down()
        {
            // * specimen fields
            Delete.Column("TravelCountryId").FromTable("Specimen");
            Delete.Column("TravelStateId").FromTable("Specimen");
            Delete.Column("TravelCountyId").FromTable("Specimen");
            Delete.Column("TravelCity").FromTable("Specimen");
            Delete.Column("HostCellLine").FromTable("Specimen");
            Delete.Column("Substrate").FromTable("Specimen");
            Delete.Column("PassageNumber").FromTable("Specimen");
            Delete.Column("PCRInfA").FromTable("Specimen");
            Delete.Column("PCRpdmInf").FromTable("Specimen");
            Delete.Column("PCRpdmH1").FromTable("Specimen");
            Delete.Column("PCRH3").FromTable("Specimen");
            Delete.Column("PCRInfB").FromTable("Specimen");
            Delete.Column("PCRBVic").FromTable("Specimen");
            Delete.Column("PCRBYam").FromTable("Specimen");
            Delete.Column("City").FromTable("Specimen");
        }
    }
}
