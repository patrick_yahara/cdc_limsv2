﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506180000)]
    public class Mig005_IsolateTable : Migration
    {
        public override void Up()
        {
            Create.Table("Isolate")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("SpecimenId").AsInt32().NotNullable()
            .WithColumn("PassageLevel").AsString(128).Nullable()
            .WithColumn("DateHarvested").AsDateTime().Nullable()
            .WithColumn("Titer").AsInt32().Nullable()
            .WithColumn("RedBloodCellType").AsString(20).Nullable()
            .WithColumn("DateInoculated").AsDateTime().Nullable()
            ;
            Create.ForeignKey().FromTable("Isolate").ForeignColumn("SpecimenId").ToTable("Specimen").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("Isolate");
        }
    }
}
