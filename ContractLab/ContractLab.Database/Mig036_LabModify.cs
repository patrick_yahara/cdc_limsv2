﻿using FluentMigrator;

namespace ContractLab.Database
{
    [Migration(201810280000)]
    public class Mig036_LabModify : Migration
    {
        public override void Up()
        {
            Create.Column("GrownTestCodeList").OnTable("Lab").AsString(255).Nullable();
            Create.Column("IttTestCodeList").OnTable("Lab").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.Column("GrownTestCodeList").FromTable("Lab");
            Delete.Column("IttTestCodeList").FromTable("Lab");
        }
    }
}
