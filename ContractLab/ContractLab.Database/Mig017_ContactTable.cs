﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201510070000)]
    public class Mig017_ContactTable : Migration
    {
        public override void Up()
        {
            Create.Table("Contact")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("FullName").AsString(256).NotNullable()
            .WithColumn("Address").AsString(512).Nullable()
            .WithColumn("City").AsString(128).Nullable()
            .WithColumn("State").AsString(50).Nullable()
            .WithColumn("Country").AsString(50).Nullable()
            .WithColumn("Phone").AsString(128).Nullable()
            .WithColumn("Fax").AsString(128).Nullable()
            .WithColumn("Email").AsString(128).Nullable()
            .WithColumn("RasClientId").AsString(50).NotNullable()
            .WithColumn("RasContactId").AsString(50).NotNullable()
            ;
            Create.Column("ContactId").OnTable("Package").AsInt32().Nullable().ForeignKey("Contact", "Id");

            Create.Column("Comments").OnTable("Package").AsString(2048).Nullable();
        }

        public override void Down()
        {
            Delete.Column("ContactId").FromTable("Package");
            Delete.Table("Contact");
        }
    }
}
