﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506181335)]
    public class Mig006_PackageTable : Migration
    {
        public override void Up()
        {
            Create.Table("Package")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Name").AsString(128).NotNullable()
            .WithColumn("IsVisible").AsBoolean().WithDefaultValue(true)
            .WithColumn("DateReceived").AsDateTime().Nullable()
            .WithColumn("SpecimenCondition").AsString(255).Nullable()
            ;
            Create.Column("PackageId").OnTable("Specimen").AsInt32().NotNullable().ForeignKey("Package", "Id");
        }

        public override void Down()
        {
            Delete.Column("PackageId").FromTable("Specimen");
            Delete.Table("Package");
        }

    }
}
