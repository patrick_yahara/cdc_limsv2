﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201709110000)]
    public class Mig034_UserModify : Migration
    {
        public override void Up()
        {
            Create.Column("DateDisabled").OnTable("AspNetUsers").AsDateTime().Nullable();
        }

        public override void Down()
        {
            Delete.Column("DateDisabled").FromTable("AspNetUsers");
        }
    }
}
