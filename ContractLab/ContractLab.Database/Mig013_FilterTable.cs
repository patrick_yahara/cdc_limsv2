﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201508240000)]
    public class Mig013_FilterTable : Migration
    {
        public override void Up()
        {
            Create.Table("Filter")
            .WithIdColumn()
            .WithColumn("Display").AsString(128).NotNullable()
            .WithColumn("BaseObject").AsString(512).NotNullable()
            .WithColumn("FilterText").AsString(128).NotNullable()
            .WithColumn("IsLabAvailable").AsBoolean().NotNullable().WithDefaultValue(true)
            ;
        }

        public override void Down()
        {
            Delete.Table("Filter");
        }
    }
}
