﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201509040000)]
    public class Mig015_IsolateTypeTable : Migration
    {
        public override void Up()
        {
            Create.Table("IsolateType")
            .WithIdColumn()
            .WithColumn("DisplayName").AsString(50).NotNullable()
            .WithColumn("SysName").AsString(50).NotNullable()
            ;
            Create.Column("IsolateTypeId").OnTable("Isolate").AsInt32().NotNullable().ForeignKey("IsolateType", "Id");
            Insert.IntoTable("IsolateType").Row(new { DisplayName = "Original", SysName = "ORIGINAL" });
            Insert.IntoTable("IsolateType").Row(new { DisplayName = "Not Harvested", SysName = "NOTHARVESTED" });
            Insert.IntoTable("IsolateType").Row(new { DisplayName = "Grown", SysName = "GROWN" });
        }

        public override void Down()
        {
            Delete.Column("IsolateTypeId").FromTable("Isolate");
            Delete.Table("IsolateType");
        }
    }
}
