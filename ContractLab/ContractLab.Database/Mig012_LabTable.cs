﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201507210000)]
    public class Mig012_LabTable : Migration
    {
        public override void Up()
        {
            Create.Table("Lab")
            .WithIdColumn()
            .WithColumn("Name").AsString(128).Nullable()
            .WithColumn("PrinterURI").AsString(2083).Nullable() //max of IE
            .WithColumn("LabelPreamble").AsString(512).Nullable()
            .WithColumn("LabelPostamble").AsString(512).Nullable()
            .WithColumn("LabelFormatString").AsString(2048).Nullable()
            ;
            Create.Column("LabId").OnTable("AspNetUsers").AsInt32().Nullable().ForeignKey("Lab", "Id");
        }

        public override void Down()
        {
            Delete.Column("LabId").FromTable("AspNetUsers");
            Delete.Table("Lab");
        }
    }
}
