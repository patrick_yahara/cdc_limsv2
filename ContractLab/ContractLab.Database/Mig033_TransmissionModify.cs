﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201703040000)]
    public class Mig033_TransmissionModify : Migration
    {
        public override void Up()
        {
            Create.Column("SupercedeId").OnTable("Transmission").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Column("SupercedeId").FromTable("Transmission");
        }
    }
}
