﻿using FluentMigrator;

namespace ContractLab.Database
{
    [Migration(201810300000)]
    public class Mig037_SpecimenModify : Migration
    {
        public override void Up()
        {
            Alter.Column("DateCollected").OnTable("Specimen").AsDateTime().Nullable();
        }

        public override void Down()
        {
            Alter.Column("DateCollected").OnTable("Specimen").AsDateTime().NotNullable();
        }
    }
}
