﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201507020000)]
    public class Mig009_AliquotTable : Migration
    {
        public override void Up()
        {
            Create.Table("Aliquot")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("IsolateId").AsInt32().NotNullable()
            .WithColumn("CUID").AsString(128).Nullable()
            .WithColumn("TestCode").AsString(20).Nullable()
            ;
            Create.ForeignKey().FromTable("Aliquot").ForeignColumn("IsolateId").ToTable("Isolate").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("Aliquot");
        }
    }
}
