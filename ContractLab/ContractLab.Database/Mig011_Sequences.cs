﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201507090000)]
    public class Mig011_Sequences : Migration
    {
        public override void Up()
        {
            Execute.Script(@"Scripts\Mig002_Sequences.sql");
        }

        public override void Down()
        {
        }
    }
}
