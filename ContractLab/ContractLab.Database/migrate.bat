@ECHO OFF
set DIRECTION=%1
set PROJECT_PATH=ContractLab.Database.csproj

if "%DIRECTION%"=="" set DIRECTION="Migrate"
if "%DIRECTION%"=="up" set DIRECTION="Migrate"
if "%DIRECTION%"=="down" set DIRECTION="Rollback"

SET CONFIGURATION=%2
if "%CONFIGURATION%"=="" set CONFIGURATION=Debug

echo "%CONFIGURATION%"

msbuild %PROJECT_PATH% /p:Configuration=%CONFIGURATION% /p:DeployProfile=%CONFIGURATION% /t:Clean,Build,%DIRECTION%