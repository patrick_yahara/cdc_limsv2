﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201608210000)]
    public class Mig026_SpecimenWHOModsTake2 : Migration
    {
        public override void Up()
        {
            // * specimen fields
            Create.Column("TravelReturnDate").OnTable("Specimen").AsDateTime().Nullable();
            Create.Column("ImportPropertyOrder").OnTable("Lab").AsString(Int32.MaxValue).Nullable();
        }

        public override void Down()
        {
            // * specimen fields
            Delete.Column("TravelReturnDate").FromTable("Specimen");
            Delete.Column("ImportPropertyOrder").FromTable("Lab");
        }
    }
}
