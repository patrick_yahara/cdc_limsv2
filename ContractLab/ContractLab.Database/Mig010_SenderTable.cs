﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201507060000)]
    public class Mig010_SenderTable : Migration
    {
        public override void Up()
        {
            Create.Table("Sender")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("CompanyName").AsString(128).NotNullable()
            .WithColumn("Address").AsString(128).Nullable()
            .WithColumn("City").AsString(128).Nullable()
            .WithColumn("State").AsString(50).Nullable()
            .WithColumn("Country").AsString(50).Nullable()
            .WithColumn("RasClientId").AsString(50).NotNullable()
            ;
            Create.Column("SenderId").OnTable("Package").AsInt32().NotNullable().ForeignKey("Sender", "Id");
        }

        public override void Down()
        {
            Delete.Column("SenderId").FromTable("Package");
            Delete.Table("Sender");
        }
    }
}
