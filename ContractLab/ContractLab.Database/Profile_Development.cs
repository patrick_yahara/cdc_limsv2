﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Profile("Debug")]
    public class Profile_Development : Migration
    {

        public override void Down()
        {
            
        }

        public override void Up()
        {

        }

//        public override void Up()
//        {
//            Insert.IntoTable("Filter").Row(new { Display = "2015 Specimens", BaseObject = "Specimen", FilterText = @"CreatedOn.ToString() > ""12/31/2014""", IsLabAvailable = true });

//            Insert.IntoTable("Lab").Row(new
//            {
//                Name = "WI",
//                PrinterURI = @"http://SLHW0673:8080/",
//                LabelPreamble = @"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD27^JUS^LRN^CI0^XZ
//^XA
//^MMT
//^PW260
//^LL0530
//^LS0",
//                LabelFormatString = @"^ARN1,1^BY1,3,60^FT195,90^BCI,,Y,N
//^FD>:{0}^FS
//^FT236,441^ARB,1,1^FH\^FD{4}^FS
//^FT90,441^ARB,1,1^FH\^FD{2}^FS
//^FT41,441^ARB,1,1^FH\^FD{3}^FS
//^FT139,441^ARB,1,1^FH\^FD{1}^FS
//^FT187,320^ARB,1,1^FH\^FD{6}^FS
//^FT187,441^ARB,1,1^FH\^FD{7}^FS
//^FT187,238^ARB,1,1^FH\^FD{9}^FS
//^FT41,170,1^ARB,1,1^FH\^FD{8}^FS",
//                LabelPostamble = @"^PQ1,0,1,Y^XZ",

//            });
//            Insert.IntoTable("Lab").Row(new
//            {
//                Name = "CA",
//                PrinterURI = @"http://127.0.0.1:8080/",
//                LabelPreamble = @"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD27^JUS^LRN^CI0^XZ
//^XA
//^MMT
//^PW260
//^LL0530
//^LS0",
//                LabelFormatString = @"^ARN1,1^BY1,3,60^FT195,90^BCI,,Y,N
//^FD>:{0}^FS
//^FT236,441^ARB,1,1^FH\^FD{4}^FS
//^FT90,441^ARB,1,1^FH\^FD{2}^FS
//^FT41,441^ARB,1,1^FH\^FD{3}^FS
//^FT139,441^ARB,1,1^FH\^FD{1}^FS
//^FT187,320^ARB,1,1^FH\^FD{6}^FS
//^FT187,441^ARB,1,1^FH\^FD{7}^FS
//^FT187,238^ARB,1,1^FH\^FD{9}^FS
//^FT41,170,1^ARB,1,1^FH\^FD{8}^FS",
//                LabelPostamble = @"^PQ1,0,1,Y^XZ",
//            });
//            Insert.IntoTable("Lab").Row(new
//            {
//                Name = "NY",
//                PrinterURI = @"http://127.0.0.1:8080/",
//                LabelPreamble = @"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD27^JUS^LRN^CI0^XZ
//^XA
//^MMT
//^PW260
//^LL0530
//^LS0",
//                LabelFormatString = @"^ARN1,1^BY1,3,60^FT195,90^BCI,,Y,N
//^FD>:{0}^FS
//^FT236,441^ARB,1,1^FH\^FD{4}^FS
//^FT90,441^ARB,1,1^FH\^FD{2}^FS
//^FT41,441^ARB,1,1^FH\^FD{3}^FS
//^FT139,441^ARB,1,1^FH\^FD{1}^FS
//^FT187,320^ARB,1,1^FH\^FD{6}^FS
//^FT187,441^ARB,1,1^FH\^FD{7}^FS
//^FT187,238^ARB,1,1^FH\^FD{9}^FS
//^FT41,170,1^ARB,1,1^FH\^FD{8}^FS",
//                LabelPostamble = @"^PQ1,0,1,Y^XZ",
//            });

//            Insert.IntoTable("AspNetRoles").Row(new { Id = "1463eb29-ec71-4de8-b1b7-a6af674c76f8", Name = "Admin" });
//            Insert.IntoTable("AspNetRoles").Row(new { Id = "fdcb5ca4-a71c-49d2-9a48-fa32bf9348f8", Name = "CDCEPI" });
//            Insert.IntoTable("AspNetRoles").Row(new { Id = "4d25327c-2f06-4b9b-9496-276f2e695b7c", Name = "Lab" });
//            Insert.IntoTable("AspNetRoles").Row(new { Id = "24b12dd6-2519-4bf7-a329-56113c817f0a", Name = "CDCLAB" });
//            Insert.IntoTable("AspNetRoles").Row(new { Id = "b88e8de9-c406-4b1c-bdea-6b6888a184f1", Name = "APHL" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39",
//                Email = "clawadmin@admin.net",
//                PasswordHash = "APPJIaNQrz+41e24MXoxthN1SKc0iDtkEGCFmLsCVQ6X77TnY9x+igSNagKd7K5c4A==",
//                SecurityStamp = "d881a921-c437-4d1f-9c81-1b1e9474edac",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "clawadmin@admin.net",
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "1463eb29-ec71-4de8-b1b7-a6af674c76f8", UserId = "8f003ebc-0a53-4e1c-adf6-2cfcb4e54a39" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "a083e124-4fa1-4ad1-8327-6cb28c4239bd",
//                Email = "CDCEPI@admin.net",
//                PasswordHash = "ALUUGbZAQH90HznhXAcWxhLUFOJdsu14Px7pdkwJRduvMfyL4qrIMaOOCDtr8y0jKw==",
//                SecurityStamp = "95a3d431-3478-4008-a142-54244ecda5fd",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "CDCEPI@admin.net",
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "fdcb5ca4-a71c-49d2-9a48-fa32bf9348f8", UserId = "a083e124-4fa1-4ad1-8327-6cb28c4239bd" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "e86dea4f-b36b-4d7b-8557-690bc87acad2",
//                Email = "CDCLAB@admin.net",
//                PasswordHash = "ALUUGbZAQH90HznhXAcWxhLUFOJdsu14Px7pdkwJRduvMfyL4qrIMaOOCDtr8y0jKw==",
//                SecurityStamp = "95a3d431-3478-4008-a142-54244ecda5fd",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "CDCLAB@admin.net",
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "24b12dd6-2519-4bf7-a329-56113c817f0a", UserId = "e86dea4f-b36b-4d7b-8557-690bc87acad2" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "60f8c417-a49b-4fc6-92d9-e575bca5af6f",
//                Email = "APHL@admin.net",
//                PasswordHash = "ALUUGbZAQH90HznhXAcWxhLUFOJdsu14Px7pdkwJRduvMfyL4qrIMaOOCDtr8y0jKw==",
//                SecurityStamp = "95a3d431-3478-4008-a142-54244ecda5fd",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "APHL@admin.net",
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "b88e8de9-c406-4b1c-bdea-6b6888a184f1", UserId = "60f8c417-a49b-4fc6-92d9-e575bca5af6f" });


//            // * WISCONSIN
//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "60ec3aa3-b4a0-45b0-8a65-310ca7723327",
//                Email = "thomas.whyte@slh.wisc.edu",
//                PasswordHash = "ALjVeaQfEifWkc7Gr06Rw6jXEDQtoJJg+Uqjy+5Q3GzNV9wjJydsWt/BFu807C0P3Q==",
//                SecurityStamp = "2e483f82-78e9-4e7d-b612-4331ac93f278",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "thomas.whyte@slh.wisc.edu",
//                LabId = 1,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "60ec3aa3-b4a0-45b0-8a65-310ca7723327" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "6c0a6b45-8128-465c-9e61-fc573c94f22e",
//                Email = "timothy.davis@slh.wisc.edu",
//                PasswordHash = "AFT1n3YQ6S9exAk+aXvRrMYeLYEyt05GS6YpIeg/vgM0IUxVOU3/jaUWSDXTsGQBHg==",
//                SecurityStamp = "fc481836-da51-4b81-a1b7-8381eaa09e88",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "timothy.davis@slh.wisc.edu",
//                LabId = 1,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "6c0a6b45-8128-465c-9e61-fc573c94f22e" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "b6be64d8-0a62-4049-9ab5-0023b76614f6",
//                Email = "richard.griesser@slh.wisc.edu",
//                PasswordHash = "AFT1n3YQ6S9exAk+aXvRrMYeLYEyt05GS6YpIeg/vgM0IUxVOU3/jaUWSDXTsGQBHg==",
//                SecurityStamp = "fc481836-da51-4b81-a1b7-8381eaa09e88",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "richard.griesser@slh.wisc.edu",
//                LabId = 1,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "b6be64d8-0a62-4049-9ab5-0023b76614f6" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "35091475-8f21-4718-b693-2a3127717d62",
//                Email = "tonya.danz@slh.wisc.edu",
//                PasswordHash = "AFT1n3YQ6S9exAk+aXvRrMYeLYEyt05GS6YpIeg/vgM0IUxVOU3/jaUWSDXTsGQBHg==",
//                SecurityStamp = "fc481836-da51-4b81-a1b7-8381eaa09e88",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "tonya.danz@slh.wisc.edu",
//                LabId = 1,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "35091475-8f21-4718-b693-2a3127717d62" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "ae62b20f-2f31-4b8a-a373-67c30f1d0c31",
//                Email = "mary.wedig@slh.wisc.edu",
//                PasswordHash = "AFT1n3YQ6S9exAk+aXvRrMYeLYEyt05GS6YpIeg/vgM0IUxVOU3/jaUWSDXTsGQBHg==",
//                SecurityStamp = "fc481836-da51-4b81-a1b7-8381eaa09e88",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "mary.wedig@slh.wisc.edu",
//                LabId = 1,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "ae62b20f-2f31-4b8a-a373-67c30f1d0c31" });

//            // * CA
//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "e3f029fb-7023-4626-873f-92694117a5b8",
//                Email = "Alice.Chen@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Alice.Chen@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "e3f029fb-7023-4626-873f-92694117a5b8" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "991c8fdc-b1b7-4ecc-904a-822a4a909e33",
//                Email = "Nohemi.Reyes-Martin@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Nohemi.Reyes-Martin@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "991c8fdc-b1b7-4ecc-904a-822a4a909e33" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "6c6b77c5-8dbf-4c39-b484-47b79e9b8b72",
//                Email = "Ricardo.Berumen@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Ricardo.Berumen@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "6c6b77c5-8dbf-4c39-b484-47b79e9b8b72" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "8705d461-9f4d-4f3b-bee6-4a80be95c185",
//                Email = "Estela.Saguar@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Estela.Saguar@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "8705d461-9f4d-4f3b-bee6-4a80be95c185" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "1110f928-207b-4643-84ff-18e3324e7147",
//                Email = "Cindy.Wong@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Cindy.Wong@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "1110f928-207b-4643-84ff-18e3324e7147" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "c913c9cc-5971-484b-86b0-9f74065cb8ea",
//                Email = "Emilia.Fields@cdph.ca.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Emilia.Fields@cdph.ca.gov",
//                LabId = 2,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "c913c9cc-5971-484b-86b0-9f74065cb8ea" });

//            //* NY
//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "8897a472-2cd5-4325-8fe2-0ca0ea2d04a4",
//                Email = "Kirsten.St.George@health.ny.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Kirsten.St.George@health.ny.gov",
//                LabId = 3,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "8897a472-2cd5-4325-8fe2-0ca0ea2d04a4" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "954f1823-754f-4186-90da-301c90453db6",
//                Email = "Meghan.Fuschino@health.ny.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Meghan.Fuschino@health.ny.gov",
//                LabId = 3,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "954f1823-754f-4186-90da-301c90453db6" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "c7bec540-5e17-4efe-a8e2-216e56a086ae",
//                Email = "Deborah.Rusinko@health.ny.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Deborah.Rusinko@health.ny.gov",
//                LabId = 3,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "c7bec540-5e17-4efe-a8e2-216e56a086ae" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "e50a3012-e7eb-4c13-9a19-826367199b0e",
//                Email = "Jennifer.Laplante@health.ny.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Jennifer.Laplante@health.ny.gov",
//                LabId = 3,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "e50a3012-e7eb-4c13-9a19-826367199b0e" });

//            Insert.IntoTable("AspNetUsers").Row(new
//            {
//                Id = "e4d60730-64e1-49f3-9cae-879260d56095",
//                Email = "Kay.Escuyer@health.ny.gov",
//                PasswordHash = "AOQ7fnxxlnXfZg9mvAJ4CWLmaqCzMDgC5YXZ7nAJIwBBFG2QHlrG/lGf8T/p2/3d2w==",
//                SecurityStamp = "c0890e5e-1362-44f0-91a7-35c3c39992f6",
//                LockoutEnabled = 0,
//                EmailConfirmed = 0,
//                PhoneNumberConfirmed = 0,
//                TwoFactorEnabled = 0,
//                AccessFailedCount = 0,
//                UserName = "Kay.Escuyer@health.ny.gov",
//                LabId = 3,
//            });
//            Insert.IntoTable("AspNetUserRoles").Row(new { RoleId = "4d25327c-2f06-4b9b-9496-276f2e695b7c", UserId = "e4d60730-64e1-49f3-9cae-879260d56095" });

//        }
    }
}
