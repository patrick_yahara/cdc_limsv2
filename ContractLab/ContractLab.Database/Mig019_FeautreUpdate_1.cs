﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201510300000)]
    public class Mig019_FeautreUpdate_1 : Migration
    {
        public override void Up()
        {
            Create.Column("DateShipped").OnTable("Manifest").AsDateTime().Nullable();
            Create.Column("TrackingNumber").OnTable("Manifest").AsString(255).Nullable();
            Create.Column("IsVisible").OnTable("IsolateType").AsBoolean().WithDefaultValue(true);
            Create.Column("Comments").OnTable("Isolate").AsString(512).Nullable();
            Create.Column("LabId").OnTable("Specimen").AsInt32().Nullable().ForeignKey("Lab", "Id");
            Create.Column("LabId").OnTable("Template").AsInt32().Nullable().ForeignKey("Lab", "Id");
        }

        public override void Down()
        {
            Delete.Column("DateShipped").FromTable("Manifest");
            Delete.Column("TrackingNumber").FromTable("Manifest");
            Delete.Column("IsVisible").FromTable("IsoalteType");
            Delete.Column("Comments").FromTable("Isolate");
            Delete.Column("LabId").FromTable("Specimen");
            Delete.Column("LabId").FromTable("Template");
        }
    }
}
