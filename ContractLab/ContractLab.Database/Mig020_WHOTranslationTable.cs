﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201601060000)]
    public class Mig020_WHOTranslationTable : Migration
    {
        public override void Up()
        {
            Create.Table("WHOTranslation")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("Heading").AsString(256).NotNullable()
            .WithColumn("MatchExpression").AsString(512).Nullable()
            .WithColumn("ReplacementValue").AsString(512).Nullable()
            .WithColumn("CommentHeading").AsString(256).NotNullable()
            .WithColumn("CommentValue").AsString(512).Nullable()
            ;
        }

        public override void Down()
        {
            Delete.Table("WHOTranslation");
        }
    }
}
