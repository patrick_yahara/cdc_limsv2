﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201709111300)]
    public class Mig035_SpecimenNote : Migration
    {
        public override void Up()
        {
            Create.Table("NoteCategory")
            .WithIdColumn()
            .WithColumn("SysName").AsString(50)
            .WithColumn("DisplayName").AsString(50)
            ;
            Insert.IntoTable("NoteCategory").Row(new { DisplayName = "Post-Transmission Edit", SysName = "TRANSMISSION_EDIT" });
            Insert.IntoTable("NoteCategory").Row(new { DisplayName = "Submitter Comment", SysName = "SUBMITTER_COMMENT" });
            Insert.IntoTable("NoteCategory").Row(new { DisplayName = "Internal Comment", SysName = "INTERNAL_COMMENT" });
            Insert.IntoTable("NoteCategory").Row(new { DisplayName = "Other", SysName = "OTHER" });

            Create.Table("SpecimenNote")
             .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("SpecimenId").AsInt32().NotNullable()
            .WithColumn("NoteCategoryId").AsInt32().NotNullable()
            .WithColumn("Note").AsString(Int32.MaxValue)
            .WithColumn("FileName").AsString(256).Nullable()
            .WithColumn("File").AsBinary(int.MaxValue).Nullable()
            ;
            Create.ForeignKey().FromTable("SpecimenNote").ForeignColumn("SpecimenId").ToTable("Specimen").PrimaryColumn("Id");
            Create.ForeignKey().FromTable("SpecimenNote").ForeignColumn("NoteCategoryId").ToTable("NoteCategory").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("SpecimenNote");
            Delete.Table("NoteCategory");
        }
    }
}
