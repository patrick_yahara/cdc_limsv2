﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201510190000)]
    public class Mig018_WHO_Label : Migration
    {
        public override void Up()
        {
            Create.Column("WHOLabelFormatString").OnTable("Lab").AsString(2048).Nullable();
        }

        public override void Down()
        {
            Delete.Column("WHOLabelFormatString").FromTable("Lab");
        }
    }
}
