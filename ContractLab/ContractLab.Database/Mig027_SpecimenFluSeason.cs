﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201608230000)]
    public class Mig027_SpecimenFluSeason : Migration
    {
        public override void Up()
        {
            Execute.Sql(@"IF COL_LENGTH('Specimen' ,'FluSeason' ) IS NULL
                        ALTER TABLE dbo.Specimen ADD FluSeason AS CASE WHEN DATEPART(MONTH, DateCollected) > 9 THEN DATEPART(YEAR, DateCollected) + 1 ELSE DATEPART(YEAR, DateCollected) END;  
                        ");
        }

        public override void Down()
        {
            Execute.Sql(@"IF COL_LENGTH('Specimen' ,'FluSeason' ) IS NOT NULL
	                    ALTER TABLE dbo.Specimen
                        DROP COLUMN FluSeason");
        }
    }
}
