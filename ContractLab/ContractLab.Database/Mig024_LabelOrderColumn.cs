﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201604080000)]
    public class Mig024_LabelOrderColumn : Migration
    {
        public override void Up()
        {
            Create.Column("LabelTestCodeOrder").OnTable("Lab").AsString(50).Nullable();
        }

        public override void Down()
        {
            Delete.Column("LabelTestCodeOrder").FromTable("Lab");
        }
    }
}
