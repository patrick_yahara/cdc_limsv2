﻿using FluentMigrator.Builders.Create.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    internal static class Utility
    {
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdColumn(this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("Id")
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithTimeStamps(this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("CreatedOn").AsDateTime().NotNullable()
                .WithColumn("CreatedBy").AsAnsiString(128).NotNullable()
                .WithColumn("ModifiedOn").AsDateTime()
                .WithColumn("ModifiedBy").AsAnsiString(128)
                .WithColumn("VersionColumn").AsCustom("ROWVERSION").NotNullable();
        }


    }
}
