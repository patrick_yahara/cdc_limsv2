﻿using FluentMigrator;

namespace ContractLab.Database
{
    [Migration(201907040000)]
    public class Mig038_AliquotMod : Migration
    {
        public override void Up()
        {
            Create.Column("RedshiftTracking").OnTable("Aliquot").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("RedshiftTracking").FromTable("Aliquot");
        }
    }
}
