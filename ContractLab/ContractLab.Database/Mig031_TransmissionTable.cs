﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201701250000)]
    public class Mig031_TransmissionTable : Migration
    {
        public override void Up()
        {
            Create.Table("Transmission")
            .WithIdColumn()
            .WithTimeStamps()
            .WithColumn("SpecimenId").AsInt32().NotNullable()
            .WithColumn("IsolateId").AsInt32().Nullable()
            .WithColumn("AliquotId").AsInt32().Nullable()
            .WithColumn("Payload").AsString(int.MaxValue).NotNullable()
            .WithColumn("Response").AsString(int.MaxValue).Nullable()
            .WithColumn("Attempts").AsInt32().Nullable()
            .WithColumn("TransmittedOn").AsDateTime().Nullable()
            .WithColumn("SpecimenRowVersion").AsBinary(8).NotNullable()
            .WithColumn("IsolateRowVersion").AsBinary(8).Nullable()
            .WithColumn("AliquotRowVersion").AsBinary(8).Nullable()
            .WithColumn("Signature").AsBinary(2048).Nullable()
            ;
            Create.ForeignKey().FromTable("Transmission").ForeignColumn("SpecimenId").ToTable("Specimen").PrimaryColumn("Id");
            Create.ForeignKey().FromTable("Transmission").ForeignColumn("IsolateId").ToTable("Isolate").PrimaryColumn("Id");
            Create.ForeignKey().FromTable("Transmission").ForeignColumn("AliquotId").ToTable("Aliquot").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table("Transmission");
        }
    }
}
