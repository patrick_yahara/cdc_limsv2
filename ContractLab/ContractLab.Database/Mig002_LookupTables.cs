﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractLab.Database
{
    [Migration(201506080002)]
    public class Mig002_LookupTables: Migration
    {
        public override void Up()
        {
            Create.Table("Status")
            .WithIdColumn()
            .WithColumn("SysName").AsString(50)
            .WithColumn("DisplayName").AsString(50)
            ;
            Insert.IntoTable("Status").Row(new { DisplayName = "In Progress", SysName = "INPROGRESS" });
            Insert.IntoTable("Status").Row(new { DisplayName = "Complete", SysName = "COMPLETE" });
            Insert.IntoTable("Status").Row(new { DisplayName = "Submitted", SysName = "SUBMITTED" });
        }
        
        public override void Down()
        {
            Delete.Table("Status");
        }
    }
}
