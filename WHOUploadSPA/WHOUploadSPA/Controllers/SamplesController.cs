﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;
using WHOUploadSPA.DTO;
using OfficeOpenXml;
using System.Web;

namespace WHOUploadSPA.Controllers
{
    [EnableCors("*", "*", "POST")]
    public class SamplesController : ApiController
    {
        [HttpPost]
        //public IEnumerable<FluAccessionReturnDTO> Post([FromBody]string filepath)
        public IEnumerable<FluAccessionReturnDTO>[] Post(string filepath, bool removefile = true)
        {
            var responses = new List<FluAccessionReturnDTO>[2];
            responses[0] = new List<FluAccessionReturnDTO>();
            responses[1] = new List<FluAccessionReturnDTO>();
            var localfilepath = HttpContext.Current.Server.MapPath(filepath);
            if (!File.Exists(localfilepath)) return responses;
            var localfileinfo = new FileInfo(localfilepath);
            try
            {
                // Get the name of the file to upload.
                var fileName = localfileinfo.Name;
                // Get the extension of the uploaded file.
                var extension = localfileinfo.Extension;

                // Allow only files with .doc or .xls extensions
                // to be uploaded.
                if ((extension != ".xlsx") && (extension != ".xls")) return responses;

                var prxy = new MirthService.DefaultAcceptMessageClient();

                var allerrors = new Dictionary<WHOImportDataDTO, List<String>>();
                using (var package = new ExcelPackage(localfileinfo))
                {
                    var sheet = package.Workbook.Worksheets[1];
                    int zzz;
                    var samples = new List<WHOImportDataDTO>();
                    for (var i = 1; i < sheet.Dimension.End.Row; i++)
                    {
                        if (sheet.Cells[i, 1].Value != null
                            && sheet.Cells[i, 2].Value != null
                            && !String.IsNullOrEmpty(sheet.Cells[i, 1].Value.ToString())
                            && !String.IsNullOrEmpty(sheet.Cells[i, 2].Value.ToString())
                            && int.TryParse(sheet.Cells[i, 1].Value.ToString(), out zzz)
                            )
                        {
                            var sample = new WHOImportDataDTO();
                            //sample.DateCollectedEstimated = @"N";
                            sample.Ordinal = sheet.Cells[i, 1].GetValueNS();
                            sample.IsolateLaboratoryNumber = sheet.Cells[i, 2].GetValueNS();
                            var tempDT = new DateTime();
                            if (DateTime.TryParse(sheet.Cells[i, 3].GetValueNS(), out tempDT))
                            {
                                sample.DateSpecimenCollected = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                            }
                            else
                            {
                                // let validator handle this
                                sample.DateSpecimenCollected = sheet.Cells[i, 3].GetValueNS();
                            }
                            sample.PatientAge = sheet.Cells[i, 4].GetValueNS();
                            sample.PatientSex = sheet.Cells[i, 5].GetValueNS();
                            sample.PatientLocation = sheet.Cells[i, 6].GetValueNS();
                            sample.ActivityExtent = sheet.Cells[i, 7].GetValueNS();
                            sample.PassageHistory = sheet.Cells[i, 8].GetValueNS();
                            sample.TypeSubtype = sheet.Cells[i, 9].GetValueNS();
                            sample.IsPatientDeceased = sheet.Cells[i, 10].GetValueNS();
                            sample.IsPatientNursingHome = sheet.Cells[i, 11].GetValueNS();
                            sample.IsPatientVaccinated = sheet.Cells[i, 12].GetValueNS();
                            sample.IsRecentTravel = sheet.Cells[i, 13].GetValueNS();
                            sample.Remarks = sheet.Cells[i, 14].GetValueNS();
                            sample.CDCID = sheet.Cells[i, 15].GetValueNS();
                            sample.CUID = sheet.Cells[i, 16].GetValueNS();
                            tempDT = new DateTime();
                            if (DateTime.TryParse(sheet.Cells[i, 17].GetValueNS(), out tempDT))
                            {
                                sample.DateReceived = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                            }
                            else
                            {
                                // let validator handle this
                                sample.DateReceived = sheet.Cells[i, 17].GetValueNS();
                            }
                            sample.SpecimenCondition = sheet.Cells[i, 18].GetValueNS();
                            sample.SpecimenSource = sheet.Cells[i, 19].GetValueNS();
                            sample.InitialStorage = sheet.Cells[i, 20].GetValueNS();
                            sample.CompCDCID1 = sheet.Cells[i, 21].GetValueNS();
                            sample.CompCDCID2 = sheet.Cells[i, 22].GetValueNS();
                            sample.StateCountry = sheet.Cells[i, 23].GetValueNS();
                            sample.Nationality = sheet.Cells[i, 24].GetValueNS();
                            sample.SpecialStudy = sheet.Cells[i, 25].GetValueNS();
                            //var tempInt = 0;
                            // * Keep zero padding
                            //if (int.TryParse(sheet.Cells[i, 26].GetValueNS(), out tempInt))
                            //{
                            //    sample.SenderID = tempInt.ToString();
                            //}
                            //else
                            //{
                                // let validator handle this
                                sample.SenderID = sheet.Cells[i, 26].GetValueNS();
                            //}

                            sample.DrugResist = sheet.Cells[i, 27].GetValueNS();
                            tempDT = new DateTime();
                            if (DateTime.TryParse(sheet.Cells[i, 28].GetValueNS(), out tempDT))
                            {
                                sample.LoginDate = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                            }
                            else
                            {
                                // let validator handle this
                                sample.LoginDate = sheet.Cells[i, 28].GetValueNS();
                            }
                            sample.reason_for_submission = sheet.Cells[i, 29].GetValueNS();
                            sample.ISR_CUID = sheet.Cells[i, 30].GetValueNS();
                            var errors = new List<String>();
                            if (!WHOImportDataDTOValidator.IsValid(ref sample, out errors))
                            {
                                allerrors.Add(sample, errors);
                            }
                            samples.Add(sample);
                        }
                    }
                    if (samples.Count <= 0) return responses;

                    if (allerrors.Count > 0)
	                {
                        foreach (var sample in allerrors.Keys)
	                    {
                            responses[1].Add(new FluAccessionReturnDTO(){ Display = String.Format(@"Row: {0} CDCID: {1} Errors: {2}", sample.Ordinal, sample.CDCID, String.Join(@"; ", allerrors[sample].ToArray()))});		 
	                    }
		                return responses;
	                }

                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(WHOImportDataDTO));
                    var settings = new XmlWriterSettings
                        {
                            Encoding = new UnicodeEncoding(false, false),
                            Indent = true,
                            OmitXmlDeclaration = true
                            
                        };
                    foreach (var sample in samples)
                    {
                        using (var textWriter = new StringWriter())
                        {
                            using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                            {
                                serializer.Serialize(xmlWriter, sample);
                                try
                                {
                                    var response = new FluAccessionReturnDTO(prxy.acceptMessage(textWriter.ToString()));
                                    if (response.InterpretedResult.GetValueOrDefault())
                                    {
                                        responses[0].Add(response);
                                    }
                                    else
                                    {
                                        responses[1].Add(response);
                                    }
                                }
                                catch (Exception)
                                {
                                    responses[1].Add(new FluAccessionReturnDTO() { Display = String.Format(@"Row: {0} CDCID: {1} Errors: {2}", sample.Ordinal, sample.CDCID, @"Error communicating to the message server") });
                                    return responses;
                                }
                            }
                        }
                    }
                    return responses;
                }
            }
            finally
            {
                if (removefile)
                {
                    var parentDir = localfileinfo.Directory;
                    localfileinfo.Delete();
                    parentDir.Delete();
                }
            }            
        }
    }
}
