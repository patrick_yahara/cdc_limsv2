﻿var mainApp = angular.module('mainApp', ['ngRoute', 'ngAnimate', 'flow', 'uploadControllers']).config(['flowFactoryProvider', function (flowFactoryProvider) {
    var baseUrl = $("base").first().attr("href");
    flowFactoryProvider.factory = fustyFlowFactory;
    flowFactoryProvider.defaults = {
        target: baseUrl + 'api/Upload',
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1
    };
}]);

mainApp.config([
    '$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var baseUrl = $("base").first().attr("href");
        $routeProvider.
            when('/upload', {
                templateUrl: baseUrl + 'Scripts/app/Views/upload.html',
                controller: 'uploadController'
            }).
            when('/process', {
                templateUrl: baseUrl + 'Scripts/app/Views/process.html',
                controller: 'processController'
            }).
            when('/', {
                redirectTo: '/upload'
            }).
            otherwise({
                redirectTo: '/upload'
            });
    }
]);

mainApp.factory('sampleService', function ($q, $http) {
    var localurl = null;
    var baseUrl = $("base").first().attr("href");
    return {
        seturl : function(val)
        {
            localurl = val;
        },
        geturl: function () {
            return localurl;
        },
        insertSample: function () {
            var urlBase = baseUrl + 'api/Samples';
            var deferred = $q.defer();
            return $http.post(urlBase, {}, { params: { 'filepath': localurl } }).success(function (data) {
                deferred.resolve(data);
            }).error(function () {
                deferred.reject();
            });
            return deferred.promise;
        }
    }
});
