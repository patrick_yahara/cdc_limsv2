﻿var uploadControllers = angular.module('uploadControllers', []);

uploadControllers.controller('uploadController', ['$scope', '$location', 'sampleService', function ($scope, $location, sampleService) {
    $scope.cancelText = 'Cancel';
    $scope.uploader = {};
    $scope.upload = function () {
        $scope.uploader.flow.upload();
    };
    $scope.success = function (message) {
        var file = angular.fromJson(message);
        $scope.cancelText = 'Process';
        $scope.processFile = function () {
            var url = file[0].path;
            sampleService.seturl(url);
            $location.path('/process');
        }
    };

}]);

uploadControllers.controller('processController', ['$scope', '$timeout', 'sampleService', function ($scope, $timeout, sampleService) {
    $scope.status = "processing";
    $scope.toprocess = sampleService.geturl();
    sampleService.insertSample().then(function (data) {
        $scope.status = "success";
        $scope.successcollection = data.data[0];
        $scope.failurecollection = data.data[1];
    });
}]);

