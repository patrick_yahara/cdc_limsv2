﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHOUploadSPA.DTO
{
    public static class WHOImportDataDTOValidator
    {
        public static bool IsValid(ref WHOImportDataDTO value, out List<String> errors)
        {
            errors = new List<String>();
            try
            {
                value.PreSerialize(); // REQUIRED TO PASS
            }
            catch (Exception ex)
            {
                errors.Add(@"Error Preparing Data for Import: " + ex.Message);
            }

            int i;
            DateTime dt;
            if (!String.IsNullOrEmpty(value.PatientAge))
            {
                if (!int.TryParse(value.PatientAge, out i))
                {
                    errors.Add(@"PatientAge is not blank or an integer");
                }
            }

            double tempAge = 0.0;
            if (!String.IsNullOrWhiteSpace(value.PatientAge) && !Double.TryParse(value.PatientAge, out tempAge))
            {
                errors.Add(@"Patient Age is not parsable");
            }
            tempAge = 0.0;
            if (Double.TryParse(value.PatientAge, out tempAge))
            {
                if (tempAge < 0.0)
                {
                    errors.Add(@"Patient Age cannot be negative");       
                }
            }


            if (!String.IsNullOrWhiteSpace(value.PatientSex) && !value.PatientSex.In(String.Empty, "M", "F"))
            {
                errors.Add(@"PatientSex is not F or M");
            }
            if (!value.IsPatientDeceased.In(null, "Y", "N", "U"))
            {
                errors.Add(@"IsPatientDeceased is not Y, N or U");
            }
            if (!value.IsPatientNursingHome.In("Y", "N", "U"))
            {
                errors.Add(@"IsPatientNursingHome is not Y, N or U");
            }
            if (!value.IsPatientVaccinated.In("Y", "N", "U"))
            {
                errors.Add(@"IsPatientVaccinated is not Y, N or U");
            }
            if (String.IsNullOrEmpty(value.CDCID))
            {
                errors.Add(@"CDCID is required");
            }
            if (!String.IsNullOrEmpty(value.DateSpecimenCollected))
            {
                if (!DateTime.TryParse(value.DateSpecimenCollected, out dt))
                {
                    errors.Add(@"DateSpecimenCollected is not parseable");
                }
            }
            if (!String.IsNullOrEmpty(value.DateReceived))
            {
                if (!DateTime.TryParse(value.DateReceived, out dt))
                {
                    errors.Add(@"DateReceived is not parseable");
                }
            }
            if (!String.IsNullOrEmpty(value.LoginDate))
            {
                if (!DateTime.TryParse(value.LoginDate, out dt))
                {
                    errors.Add(@"LoginDate is not parseable");
                }
            }
            if (!String.IsNullOrEmpty(value.SenderID))
            {
                if (!int.TryParse(value.SenderID, out i))
                {
                    errors.Add(@"SenderID is not blank or an integer");
                }
            }
            return errors.Count == 0;
        }
    }
}

