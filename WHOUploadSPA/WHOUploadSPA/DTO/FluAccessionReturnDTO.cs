﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace WHOUploadSPA.DTO
{
    [DataContract]
    public class FluAccessionReturnDTO
    {
        /*
            Flu_Accession.ReturnDTOfalse'cdc_id' value already exists Sample: 2014755654- 'cdc_id' value already exists- Not processed Package: - Not processed SubType @SampleLevel: H1PDM09- Not processed Related Sample: 2014755640- Not processed SubSample: F88238- Not processed Aliquot: - Not processed SubType @SubsampleLevel: H1PDM09- Not processed 
        */
        [DataMember(Order = 0)]
        public bool? ReturnedResult { get; set; }
        [IgnoreDataMember]
        public string FullMessage { get; set; }
        [IgnoreDataMember]
        public string InnerMessage { get; set; }
        [DataMember(Order = 1)]
        public string Summary { get; set; }
        [DataMember(Order = 2)]
        public string Sample { get; set; }
        [DataMember(Order = 3)]
        public string Package { get; set; }
        [DataMember(Order = 4)]
        public string SubTypeAtSampleLevel { get; set; }
        [DataMember(Order = 5)]
        public string RelatedSample { get; set; }
        [DataMember(Order = 6)]
        public string SubSample { get; set; }
        [DataMember(Order = 7)]
        public string Aliquot { get; set; }
        [DataMember(Order = 8)]
        public string SubTypeAtSubsampleLevel { get; set; }
        [DataMember(Order = 9)]
        public bool? InterpretedResult { get; set; }
        [DataMember(Order = 10)]
        public string Display { get; set; }

        public FluAccessionReturnDTO()
        {
        }
        public FluAccessionReturnDTO(string message)
        {
            FullMessage = message;
            // UGLY
            if (message.IndexOf(@"<types:ReturnDTO") <= 0)
            {
                Display = @"Error parsing return value";
                return;
            }
            var tempS = message.Substring(message.IndexOf(@"<types:ReturnDTO"));
            tempS = tempS.Substring(0, tempS.IndexOf(@"</soap:Body>"));
            tempS = tempS.Replace(@"types:ReturnDTO", @"ReturnDTO");

            var xDoc = GetXmlDocumentFromString(tempS);


            var items = xDoc.SelectNodes("ReturnDTO");

            foreach (XmlNode item in items)
            {
                var temp = item.SelectSingleNode("bRet").InnerText;
                bool tempB;
                if (bool.TryParse(temp, out tempB))
                {
                    ReturnedResult = tempB;
                }
                else if (bool.TryParse(temp.Substring(0, 4), out tempB))
                {
                    ReturnedResult = tempB;
                }
                InnerMessage = item.SelectSingleNode("sRet").InnerText;
            }
            var lines = InnerMessage.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (lines.Length > 1) 
                Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
            if (lines.Length > 2)
                Package = lines[2].Substring(lines[2].IndexOf(':') + 1);
            if (lines.Length > 3)
                SubTypeAtSampleLevel = lines[3].Substring(lines[3].IndexOf(':') + 1);
            if (lines.Length > 4) 
                RelatedSample = lines[4].Substring(lines[4].IndexOf(':') + 1);
            if (lines.Length > 5)
                SubSample = lines[5].Substring(lines[5].IndexOf(':') + 1);
            if (lines.Length > 6)
                Aliquot = lines[6].Substring(lines[6].IndexOf(':') + 1);
            if (lines.Length > 7)
                SubTypeAtSubsampleLevel = lines[7].Substring(lines[7].IndexOf(':') + 1);
            UpdateInterpretedResult();
            var tempSText = Sample.Substring(0, Sample.IndexOf('-'));
            if (ReturnedResult.HasValue && ReturnedResult.Value)
            {
                Display = string.Format(@"Sample: {0}", tempSText);
            }
            else
            {
                Display = string.Format(@"Sample: {0} Error: {1}", Sample, Summary);
            }

        }

        private XmlDocument GetXmlDocumentFromString(string xml)
        {
            var doc = new XmlDocument();

            using (var sr = new StringReader(xml))
            using (var xtr = new XmlTextReader(sr) { Namespaces = false })
                doc.Load(xtr);

            return doc;
        }

        private void UpdateInterpretedResult() 
        {
            InterpretedResult = ReturnedResult;
            if (!InterpretedResult.GetValueOrDefault())
            {
                
            }
        }

    }
}