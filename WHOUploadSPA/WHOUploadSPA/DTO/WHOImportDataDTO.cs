﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;
using WHOUploadSPA.Utility;

namespace WHOUploadSPA.DTO
{
    public class WHOImportDataDTO
    {
        public String Ordinal { get; set; }
        public String IsolateLaboratoryNumber { get; set; }
        public String DateSpecimenCollected { get; set; }
        public String PatientAge { get; set; }
        public String PatientSex { get; set; }
        public String PatientLocation { get; set; }
        public String ActivityExtent { get; set; }
        public String PassageHistory { get; set; }
        public String TypeSubtype { get; set; }
        public String Negative { get; set; }
        public String PrintableSubtype { get; set; }
        public String host { get; set; }
        public String host_detail { get; set; }
        public String duplicate { get; set; }
        public String subsample_sampleclass { get; set; }
        public String cold_adapted { get; set; }
        public String reassortant { get; set; }
        public String reagent { get; set; }
        public String recombinant { get; set; }
        public String contaminats { get; set; }
        public String IsPatientDeceased { get; set; }
        public String IsPatientNursingHome { get; set; }
        public String IsPatientVaccinated { get; set; }
        public String IsRecentTravel { get; set; }
        public String TravelLocation { get; set; }
        public String Remarks { get; set; }
        public String CDCID { get; set; }
        public String CUID { get; set; }
        public String ISR_CUID { get; set; }
        public String reason_for_submission { get; set; }
        public String DateReceived { get; set; }
        public String SpecimenCondition { get; set; }
        public String SpecimenSource { get; set; }
        public String InitialStorage { get; set; }
        public String CompCDCID1 { get; set; }
        public String CompCDCID2 { get; set; }
        public String StateCountry { get; set; }
        [IgnoreDataMember]
        public String Nationality { get; set; } //DO NOT SERIALIZE THIS
        public String SpecialStudy { get; set; }
        public String SenderID { get; set; }
        public String DrugResist { get; set; } //DO NOT SERIALIZE THIS
        public String LoginDate { get; set; }

        public String DateCollectedEstimated { get; set; }

        public void PreSerialize()
        {

            double tempAge = 0.0;
            if (Double.TryParse(PatientAge, out tempAge))
            {
                PatientAge = Math.Truncate(tempAge).ToString();
            }
            else
            {
                PatientAge = String.Empty;
            }

            #region IsRecentTravel
            if (String.IsNullOrEmpty(IsRecentTravel))
            {
                IsRecentTravel = "U";
            }
            switch (IsRecentTravel)
            {
                case "U":
                    {
                        TravelLocation = String.Empty;
                        break;
                    }
                case "N":
                    {
                        TravelLocation = String.Empty;
                        break;
                    }
                case "Y":
                    {
                        TravelLocation = String.Empty;
                        break;
                    }
                default:
                    {
                        TravelLocation = IsRecentTravel;
                        IsRecentTravel = "Y";
                        break;
                    }
            }
            #endregion
            #region BlanksToU
            IsPatientDeceased = String.IsNullOrEmpty(IsPatientDeceased) ? "U" : IsPatientDeceased;
            IsPatientNursingHome = String.IsNullOrEmpty(IsPatientNursingHome) ? "U" : IsPatientNursingHome;
            IsPatientVaccinated = String.IsNullOrEmpty(IsPatientVaccinated) ? "U" : IsPatientVaccinated;
            #endregion
            #region BlanksToN
            reagent = String.IsNullOrEmpty(reagent) ? "N" : reagent;
            cold_adapted = String.IsNullOrEmpty(cold_adapted) ? "N" : cold_adapted;
            reassortant = String.IsNullOrEmpty(reassortant) ? "N" : reassortant;
            recombinant = String.IsNullOrEmpty(recombinant) ? "N" : recombinant;
            #endregion
            #region DateSpecCollected
            DateCollectedEstimated = "N";
            if (String.IsNullOrEmpty(DateSpecimenCollected))
            {
                DateCollectedEstimated = "Y";

                if ("US".Equals(Nationality))
                {
                    DateTime tempDt;
                    if (DateTime.TryParse(DateReceived, out tempDt))
                    {
                        tempDt = tempDt.AddDays(-30);
                        DateSpecimenCollected = String.Format(@"{0:yyyy-MM-dd}", tempDt);
                    }
                    else
                    {
                        throw new Exception("DateReceived is not a valid date.");
                    }
                }
                else
                {
                    DateTime tempDt;
                    if (DateTime.TryParse(DateReceived, out tempDt))
                    {
                        tempDt = tempDt.AddDays(-60);
                        DateSpecimenCollected = String.Format(@"{0:yyyy-MM-dd}", tempDt);
                    }
                    else
                    {
                        throw new Exception("DateReceived is not a valid date.");
                    }
                }
            }
            #endregion

            #region TypeSubtype

            //// defaults
            //Negative = "N";
            //host = @"Human";
            //host_detail = @"Human";
            //duplicate = "N";
            //subsample_sampleclass = String.Empty;

            //if (string.IsNullOrEmpty(TypeSubtype))
            //{
            //    return;
            //    // no longer required, 304
            //    // throw new Exception("TypeSubtype is required.");
            //}

            ////composit value TypeSubtype
            //var tempString = String.Empty;
            //var values = TypeSubtype.Split(new string[] { @" MIX" }, StringSplitOptions.RemoveEmptyEntries);
            //if (values.Length > 1)
            //{
            //    tempString = values[0].Replace('/', ',') + values[1];
            //}
            //else
            //{
            //    tempString = values[0];
            //}

            //values = tempString.Split('/');
            ////first part is what we Areas going ToString continue ToString parse
            //var value = values[0];
            //var codes = value.Split('-');
            //var subtype = codes[codes.Length - 1];
            //if (@"NEG".Equals(subtype))
            //{
            //    // this is not a subtype, it means negative
            //    Negative = "Y";
            //}
            //else
            //{
            //    PrintableSubtype = subtype;
            //    Negative = "N";
            //}

            //// set above to N uless overridden
            //// Per skype chat Bug 281
            ////cold_adapted = String.Empty;
            ////reassortant = String.Empty;
            ////reagent = String.Empty;
            //for (int i = 0; i < codes.Length - 2; i++)
            //{
            //    switch (codes[i])
            //    {
            //        case "D":
            //            {
            //                duplicate = "Y";
            //                break;
            //            }
            //        case "S":
            //            {
            //                subsample_sampleclass = "Serum";
            //                break;
            //            }
            //        case "M":
            //            {
            //                subsample_sampleclass = "Monoclonal";
            //                break;
            //            }
            //        case "CA":
            //            {
            //                cold_adapted = "Y";
            //                break;
            //            }
            //        case "R":
            //            {
            //                reassortant = "Y";
            //                break;
            //            }
            //        case "RG":
            //            {
            //                reagent = "Y";
            //                break;
            //            }
            //        case "AV":
            //            {
            //                host = "Avian";
            //                host_detail = String.Empty;
            //                break;
            //            }
            //        case "ENV":
            //            {
            //                host = "Environment";
            //                host_detail = String.Empty;
            //                break;
            //            }
            //        case "CN":
            //            {
            //                host = "Mammal";
            //                host_detail = "Canine";
            //                break;
            //            }
            //        case "EQ":
            //            {
            //                host = "Mammal";
            //                host_detail = "Equine";
            //                break;
            //            }
            //        case "FE":
            //            {
            //                host = "Mammal";
            //                host_detail = "Feline";
            //                break;
            //            }
            //        case "SW":
            //            {
            //                host = "Mammal";
            //                host_detail = "Swine";
            //                break;
            //            }
            //        case "AN":
            //            {
            //                host = "Mammal";
            //                host_detail = "Other";
            //                break;
            //            }
            //        default:
            //            {
            //                throw new Exception(@"Unknown code");
            //            }
            //    }
            //}
            ////second part is contaminants (to be mapped eventually)
            //contaminats = String.Join(@"/", values.ToList().GetRange(1, values.Length - 1).ToArray());
            #endregion
            var st = new SubType();
            st.Parse(TypeSubtype);
            this.cold_adapted = st.cold_adapted;
            this.contaminats = st.contaminants;
            this.duplicate = st.duplicate;
            this.host = st.host;
            this.host_detail = st.host_detail;
            this.Negative = st.Negative;
            this.PrintableSubtype = st.PrintableSubtype;
            this.reagent = st.reagent;
            this.reassortant = st.reassortant;
            this.subsample_sampleclass = st.subsample_sampleclass;
            this.TypeSubtype = st.TypeSubtype;

        }

    }
}