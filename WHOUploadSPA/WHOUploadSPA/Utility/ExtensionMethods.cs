﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Web.Mvc;

namespace WHOUploadSPA
{
    public static class ExtensionMethods
    {
        public static String GetValueNS(this ExcelRangeBase erb, bool trim = true)
        {
            return erb.GetValueNSOld(trim) ?? String.Empty;
        }

        public static String GetValueNSOld(this ExcelRangeBase erb, bool trim = true)
        {
            if (trim)
            {
                return erb.Value != null ? erb.Value.ToString().Trim() : null;
            }
            return erb.Value != null ? erb.Value.ToString() : null;
        }

        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                        return true;
            #else
                  return false;
            #endif
        }

        public static bool In<T>(this T item, params T[] items)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            return items.Contains(item);
        }

    }
}