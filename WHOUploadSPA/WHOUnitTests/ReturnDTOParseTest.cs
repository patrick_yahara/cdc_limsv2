﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WHOUploadSPA.DTO;
using System.Diagnostics;

namespace WHOUnitTests
{
    [TestClass]
    public class ReturnDTOParseTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var message = @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?><soap:Envelope      ><soap:Header/><soap:Body soap:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><tns:ClinicalLogin_sessionlessResponse><ClinicalLogin_sessionlessResult href=""#id1""/></tns:ClinicalLogin_sessionlessResponse><soapenc:Array id=""id1"" soapenc:arrayType=""types:ReturnDTO[1]""><Item href=""#id2""/></soapenc:Array><types:ReturnDTO id=""id2"" xsi:type=""types:ReturnDTO""><XmlType xsi:type=""xsd:string"">Flu_Accession.ReturnDTO</XmlType><bRet xsi:type=""xsd:boolean"">true</bRet><sRet xsi:type=""xsd:string"">
            Sample: 2265000324- Metadata info updated- AutoCommit rejected: Package/Subsample/Aliquot failed
            Package: - Not processed
            SubType @SampleLevel: H3- CDC_FLU_SUBTYPE_LINK created
            Related Sample: 2215000324- CDC_SPECIMEN_LINK created
            SubSample: - Metadata info updated
            Aliquot: MTTF6902- Metadata info updated
            SubType @SubsampleLevel: H3- CDC_FLU_SUBTYPE_LINK created</sRet></types:ReturnDTO></soap:Body></soap:Envelope>";
            var foo = new FluAccessionReturnDTO(message);
            Debugger.Log(0, "Testing", foo.Display);
        }
    }
}
