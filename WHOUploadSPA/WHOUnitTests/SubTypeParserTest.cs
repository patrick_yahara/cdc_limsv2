﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WHOUploadSPA.Utility;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace WHOUnitTests
{
    [TestClass]
    public class SubTypeParserTest
    {
        private static string _testInput;

        private TestContext _context;
        public TestContext TestContext
        { get { return _context; } set { _context = value; } }

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            _testInput =
                @"{
                    '': {
                        'PrintableSubtype': '',
                        'TypeSubtype': '',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'N',
                        'host': 'Human',
                        'host_detail': 'Human',
                        'reagent': 'N',
                        'reassortant': 'N',
                        'subsample_sampleclass': ''
		                , 'Negative':'N'
                    },
                    'R-CA-H1PDM09': {
                        'PrintableSubtype': 'H1PDM09',
                        'TypeSubtype': 'R-CA-H1PDM09',
                        'cold_adapted': 'Y',
                        'contaminants': '',
                        'duplicate': 'N',
                        'host': 'Human',
                        'host_detail': 'Human',
                        'reagent': 'N',
                        'reassortant': 'Y',
                        'subsample_sampleclass': ''
		                , 'Negative':'N'
                    },
                    'D-H3/B MIX': {
                        'PrintableSubtype': 'H3,B',
                        'TypeSubtype': 'D-H3/B MIX',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'Y',
                        'host': 'Human',
                        'host_detail': 'Human',
                        'reagent': 'N',
                        'reassortant': 'N',
                        'subsample_sampleclass': ''
		                , 'Negative':'N'
                    },
                    'D-M-SW-H1': {
                        'PrintableSubtype': 'H1',
                        'TypeSubtype': 'D-M-SW-H1',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'Y',
                        'host': 'Mammal',
                        'host_detail': 'Swine',
                        'reagent': 'N',
                        'reassortant': 'N',
                        'subsample_sampleclass': 'Monoclonal'
		                , 'Negative':'N'
                    },
                    'R-AV-H5N6': {
                        'PrintableSubtype': 'H5N6',
                        'TypeSubtype': 'R-AV-H5N6',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'N',
                        'host': 'Avian',
                        'host_detail': '',
                        'reagent': 'N',
                        'reassortant': 'Y',
                        'subsample_sampleclass': ''
		                , 'Negative':'N'
                    },
                    'RG-H3': {
                        'PrintableSubtype': 'H3',
                        'TypeSubtype': 'RG-H3',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'N',
                        'host': 'Human',
                        'host_detail': 'Human',
                        'reagent': 'Y',
                        'reassortant': 'N',
                        'subsample_sampleclass': ''
		                , 'Negative':'N'
                    },
                    'S-B': {
                        'PrintableSubtype': 'B',
                        'TypeSubtype': 'S-B',
                        'cold_adapted': 'N',
                        'contaminants': '',
                        'duplicate': 'N',
                        'host': 'Human',
                        'host_detail': 'Human',
                        'reagent': 'N',
                        'reassortant': 'N',
                        'subsample_sampleclass': 'Serum'
		                , 'Negative':'N'
                    }
                }";
        }

        [TestMethod]
        public void TestMethod1()
        {
            var source = @"";
            var expected = @"{
                'PrintableSubtype': '',
                'TypeSubtype': '',
                'cold_adapted': 'N',
                'contaminants': '',
                'duplicate': 'N',
                'host': 'Human',
                'host_detail': 'Human',
                'reagent': 'N',
                'reassortant': 'N',
                'subsample_sampleclass': '',
                'Negative':'N'
            }";
            var st1 = new SubType();
            var st2 = new SubType(expected);
            st1.Parse(source);
            var isequal = st1.Equals(st2);
            Assert.IsTrue(isequal, "{0} Not Equal to Expected", source);
        }

        [TestMethod]
        public void TestMethod2()
        {
            var results = new Dictionary<string, bool>();

            var jobj = JObject.Parse(_testInput);
            foreach (var item in (jobj as IDictionary<string, JToken>))
	        {
                var source = item.Key;
                var expected = item.Value.ToString();
                var st1 = new SubType();
                var st2 = new SubType(expected);
                st1.Parse(source);
                var isequal = st1.Equals(st2);
                results.Add(source, isequal);	        
	        }
            Assert.IsFalse(results.ContainsValue(false));	
        }
    }
}
