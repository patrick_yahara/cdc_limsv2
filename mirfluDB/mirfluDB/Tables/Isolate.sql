﻿CREATE TABLE [dbo].[Isolate]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    SpecimenId INT NOT NULL,
    Passage nvarchar(50) NOT NULL,
    DateHarvested datetime,
    Vaccine BIT NOT NULL DEFAULT 0, 
    IsolateReceived BIT NOT NULL DEFAULT 0, 
    SequencingStorage nvarchar(20) NOT NULL,
    AAResAdam nvarchar(30) NOT NULL,
    AAResNai nvarchar(100) NOT NULL,
    DownToVaccine nvarchar(20) NOT NULL,
    ContractLabId nvarchar(4),
    Lot nchar(1) NOT NULL,
    CalculatedSubtype nvarchar(30),
    DateContractLabTransfer datetime,
    InitialStorage nvarchar(20) NOT NULL 
)
