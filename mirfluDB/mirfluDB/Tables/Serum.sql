﻿CREATE TABLE [dbo].[Serum]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    Lot nvarchar(12) NOT NULL,
    IsolateId INT,
    Ferret nvarchar(50) NOT NULL,
    InfectingAgent nvarchar(50) NOT NULL,
    BoostingAgent nvarchar(50) NOT NULL,
    DateInoculated datetime,
    DateBoosted datetime,
    DateHarvested datetime,
    Justification nvarchar(50) NOT NULL,
    Comment nvarchar(255) NOT NULL,
    ShortDescription nvarchar(50) NOT NULL,
    ExtraDescription nvarchar(50) NOT NULL
)
