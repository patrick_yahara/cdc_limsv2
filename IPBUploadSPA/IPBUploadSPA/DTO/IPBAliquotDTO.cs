﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "aliquot")]
    public class IPBAliquotDTO
    {
        [XmlElement(ElementName = "cuid")]
        public String CUID { get; set; }
        [XmlElement(ElementName = "fraction_type")]
        public String FractionType { get; set; }
        [XmlElement(ElementName = "parent_cuid")]
        public String ParentAliquot { get; set; }
        [XmlElement(ElementName = "quantity")]
        public String SampleVolumne { get; set; }
        [XmlElement(ElementName = "units")]
        public String VolumnUnitOfMeasure { get; set; }
        [XmlElement(ElementName = "sample_loc_bldg")]
        public String SampleLocBldg { get; set; }
        [XmlElement(ElementName = "sample_loc_room")]
        public String SampleLocRoom { get; set; }
        [XmlElement(ElementName = "sample_loc_frzr")]
        public String SampleLocFrzr { get; set; }
        [XmlElement(ElementName = "sample_loc_box")]
        public String SampleLocBox { get; set; }
        [XmlElement(ElementName = "sample_loc_abs_position")]
        public String SampleLocAbsPosition { get; set; }
        [XmlElement(ElementName = "sample_loc_reason")]
        public String SampleLocReason { get; set; }
        [XmlElement(ElementName = "sample_loc_locationid")]
        public String SampleLocLocationId { get; set; }
    }
}