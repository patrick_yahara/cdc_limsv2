﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IPBUploadSPA.DTO
{
    [DataContract]
    public class UploadRequestDTO
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember(Order = 0)]
        public string Filename { get; set; }
        [DataMember(Order = 1)]
        public string Validated { get; set; }
        [DataMember(Order = 2)]
        public long Progress1 { get; set; }
        [DataMember(Order = 3)]
        public long Progress2 { get; set; }
        [DataMember(Order = 4)]
        public string Error { get; set; }
        [DataMember(Order = 5)]
        public string Site { get; set; }
    }
}