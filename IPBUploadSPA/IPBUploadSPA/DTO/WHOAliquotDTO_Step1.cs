﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "aliquot")]
    public class WHOAliquotDTO_Step1
    {
        public WHOAliquotDTO_Step1()
        { 
        }

        [XmlIgnore]
        public String Id { get; set; }
        [Heading("isolate_id")]
        public String IsolateId { get; set; }
        [Heading("CUID")]
        [XmlElement(ElementName = "cuid")]
        public String CUID { get; set; }
        [Heading("Fraction Type")]
        [XmlElement(ElementName = "fraction_type")]
        public String TestCode { get; set; }
        [XmlElement(ElementName = "initial_storage")]
        public String InitialStorage { get; set; }
        [XmlElement(ElementName = "stored_locally")]
        public String storedlocally { get; set; }

        [Heading("PCRInfA")]
        [XmlElement(ElementName = "pcrinfa")]
        public string PCRInfA { get; set; }
        [Heading("PCRpdmInfA")]
        [XmlElement(ElementName = "pcrpdminfa")]
        public string PCRpdmInfA { get; set; }
        [Heading("PCRpdmH1")]
        [XmlElement(ElementName = "pcrpdmh1")]
        public string PCRpdmH1 { get; set; }
        [Heading("PCRH3")]
        [XmlElement(ElementName = "pcrh3")]
        public string PCRH3 { get; set; }
        [Heading("PCRInfB")]
        [XmlElement(ElementName = "pcrinfb")]
        public string PCRInfB { get; set; }
        [Heading("PCRBVic")]
        [XmlElement(ElementName = "pcrbvic")]
        public string PCRBVic { get; set; }
        [Heading("PCRBYam")]
        [XmlElement(ElementName = "pcrbyam")]
        public string PCRBYam { get; set; }
    }
}