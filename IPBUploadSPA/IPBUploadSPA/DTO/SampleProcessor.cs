﻿using IPBUploadSPA.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPBUploadSPA.DTO
{
    public static class SampleProcessor
    {
        public static readonly string[] _grouping = { @"Ordinal", @"CSID", @"CUID", @"LocalTravel"
                                                        , @"FractionType", @"ParentAliquot", @"SampleVolumne", @"VolumnUnitOfMeasure"
                                                        , @"RecentDomesticTravelDate", @"SampleLocBldg", @"SampleLocRoom", @"SampleLocFrzr"
                                                        , @"SampleLocBox", @"SampleLocAbsPosition", @"SampleLocReason"};

        public static readonly string[] _groupingWHOAliquot = { @"ParentCSID", @"ParentCUID", @"IsolateId"};
        public static readonly string[] _groupingWHO = { @"CSID" };

        public static readonly DynamicEqualityComparer<IPBUploadDTO> _comparer = new DynamicEqualityComparer<IPBUploadDTO>(_grouping);
        public static List<IPBUploadDTO> ProcessSamples(List<IPBUploadDTO> source, out bool IsValid)
        {
            var result = new List<IPBUploadDTO>();
            var lastid = String.Empty;
            IPBUploadDTO lastsample = null;
            foreach (var aliquot in source.OrderBy(s => s.CSID).ThenBy(s => s.CUID))
            {
                if (!lastid.Equals(aliquot.CSID))
                {
                    lastsample = aliquot;
                    lastid = aliquot.CSID;
                    var newaliquot = new IPBAliquotDTO() { 
                        CUID = aliquot.CUID,
                        FractionType = aliquot.FractionType,
                        ParentAliquot = aliquot.ParentAliquot,
                        SampleLocAbsPosition = aliquot.SampleLocAbsPosition,
                        SampleLocBldg = aliquot.SampleLocBldg,
                        SampleLocBox = aliquot.SampleLocBox,
                        SampleLocFrzr = aliquot.SampleLocFrzr,
                        SampleLocReason = aliquot.SampleLocReason,
                        SampleLocRoom = aliquot.SampleLocRoom,
                        SampleVolumne = aliquot.SampleVolume,
                        VolumnUnitOfMeasure = aliquot.VolumnUnitOfMeasure,
                        SampleLocLocationId = aliquot.SampleLocLocationId,
                    };
                    var alist = new List<IPBAliquotDTO>();
                    alist.Add(newaliquot);
                    var newsubsample = new IPBSubsampleDTO() { SampleClass = aliquot.SampleClass, Aliquots = alist };
                    var slist = new List<IPBSubsampleDTO>();
                    slist.Add(newsubsample);
                    aliquot.Subsamples = slist;
                    result.Add(lastsample);
                }
                else
                {
                    if (_comparer.Equals(aliquot, lastsample))
                    {
                        var newaliquot = new IPBAliquotDTO()
                        {
                            CUID = aliquot.CUID,
                            FractionType = aliquot.FractionType,
                            ParentAliquot = aliquot.ParentAliquot,
                            SampleLocAbsPosition = aliquot.SampleLocAbsPosition,
                            SampleLocBldg = aliquot.SampleLocBldg,
                            SampleLocBox = aliquot.SampleLocBox,
                            SampleLocFrzr = aliquot.SampleLocFrzr,
                            SampleLocReason = aliquot.SampleLocReason,
                            SampleLocRoom = aliquot.SampleLocRoom,
                            SampleVolumne = aliquot.SampleVolume,
                            VolumnUnitOfMeasure = aliquot.VolumnUnitOfMeasure,
                            SampleLocLocationId = aliquot.SampleLocLocationId,
                        };
                        lastsample.Subsamples.FirstOrDefault().Aliquots.Add(newaliquot);
                    } else
                    {
                        IsValid = false;
                        return result;
                    }

                }
            }
            IsValid = true;
            return result;
        }

        public static readonly DynamicEqualityComparer<WHOAliquotDTO> _comparerWHOAliquot = new DynamicEqualityComparer<WHOAliquotDTO>(_groupingWHOAliquot);
        public static List<WHOSpecimenDTO_Step2> ProcessSamples(List<WHOAliquotDTO> source, out bool IsValid)
        {
            var result = new List<WHOSpecimenDTO_Step2>();
            var lastid = String.Empty;
            WHOAliquotDTO lastaliquot = null;
            foreach (var aliquot in source.OrderBy(s => s.ParentCSID).ThenBy(s => s.ParentCUID).ThenBy(s => s.IsolateId))
            {
                if (!lastid.Equals(aliquot.ParentCSID))
                {
                    lastaliquot = aliquot;
                    lastid = aliquot.ParentCSID;
                    var newspecimen = new WHOSpecimenDTO_Step2()
                    {
                        CSID = aliquot.ParentCSID,
                        CUID = aliquot.ParentCUID,
                        Ordinal = aliquot.Ordinal,
                        Validated = true,
                        ValidationErrors = String.Empty,
                    };
                    newspecimen.Aliquots.Add(aliquot);
                    result.Add(newspecimen);
                }
                else
                {
                    if (_comparerWHOAliquot.Equals(aliquot, lastaliquot))
                    {
                        result.LastOrDefault().Aliquots.Add(aliquot);
                    }
                    else
                    {
                        IsValid = false;
                        return result;
                    }

                }
            }
            IsValid = true;
            return result;
        }

        public static readonly DynamicEqualityComparer<WHOSpecimenDTO> _comparerWHO = new DynamicEqualityComparer<WHOSpecimenDTO>(_groupingWHO);
        public static List<WHOSpecimenDTO> ProcessSamples(List<WHOSpecimenDTO> source, out bool IsValid)
        {
            var result = new List<WHOSpecimenDTO>();
            var lastid = String.Empty;
            WHOSpecimenDTO lastSpecimen = null;
            foreach (var specimen in source.OrderBy(s => s.CSID).ThenBy(s => s.IsolateId).ThenBy(s => s.CUID))
            {
                if (!lastid.Equals(specimen.CSID))
                {
                    lastSpecimen = specimen;
                    lastid = specimen.CSID;
                    // craete aliquot
                    var newaliquot = new WHOAliquotDTO_Step1()
                    {
                        CUID = specimen.CUID,
                        IsolateId = specimen.IsolateId,
                        TestCode = specimen.TestCode,
                        storedlocally = specimen.storedlocally,
                        // * 402
                        InitialStorage = specimen.InitialStorage,
                        PCRInfA = specimen.PCRInfA,
                        PCRpdmInfA = specimen.PCRpdmInfA,
                        PCRpdmH1 = specimen.PCRpdmH1,
                        PCRH3 = specimen.PCRH3,
                        PCRInfB = specimen.PCRInfB,
                        PCRBVic = specimen.PCRBVic,
                        PCRBYam = specimen.PCRBYam,
                    };
                    // create isolate
                    var newisolate = new WHOIsolateDTO()
                    {
                        PassageLevel = specimen.PassageHistory,
                        SubType = specimen.SubType,
                        PrintableSubType = specimen.PrintableSubtype,

                        SampleClass = specimen.SampleClass,
                        Id = specimen.IsolateId,
                        DateInoculated = specimen.DateInoculated,
                        DateContractLabReceived = specimen.DateContractLabReceived,
                        HostCellLine = specimen.HostCellLine,
                        Substrate = specimen.Substrate,
                        PassageNumber = specimen.PassageNumber,
                    };
                    newisolate.Aliquots.Add(newaliquot);
                    specimen.Isolates.Add(newisolate);
                    // create isolate
                    result.Add(specimen);
                }
                else
                {
                    if (_comparerWHO.Equals(specimen, lastSpecimen))
                    {
                        if (!specimen.IsolateId.Equals(lastSpecimen.IsolateId))
                        {
                            var newisolate = new WHOIsolateDTO()
                            {
                                PassageLevel = specimen.PassageHistory,
                                SubType = specimen.SubType,
                                PrintableSubType = specimen.PrintableSubtype,
                                SampleClass = specimen.SampleClass,
                                Id = specimen.IsolateId,
                                DateInoculated = specimen.DateInoculated,
                                DateContractLabReceived = specimen.DateContractLabReceived,
                                HostCellLine = specimen.HostCellLine,
                                Substrate = specimen.Substrate,
                                PassageNumber = specimen.PassageNumber,
                            };
                            specimen.Isolates.Add(newisolate);
                        }
                        if (!specimen.IsolateId.Equals(lastSpecimen.IsolateId))
                        {
                            var newaliquot = new WHOAliquotDTO_Step1()
                            {
                                CUID = specimen.CUID,
                                IsolateId = specimen.IsolateId,
                                TestCode = specimen.TestCode,
                                InitialStorage = specimen.InitialStorage,
                                storedlocally = specimen.storedlocally,
                                PCRInfA = specimen.PCRInfA,
                                PCRpdmInfA = specimen.PCRpdmInfA,
                                PCRpdmH1 = specimen.PCRpdmH1,
                                PCRH3 = specimen.PCRH3,
                                PCRInfB = specimen.PCRInfB,
                                PCRBVic = specimen.PCRBVic,
                                PCRBYam = specimen.PCRBYam,
                            };
                            specimen.Isolates.LastOrDefault().Aliquots.Add(newaliquot);
                        }
                    }
                    else
                    {
                        IsValid = false;
                        return result;
                    }
                }
            }
            IsValid = true;
            return result;
        }
    
    }
}