﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPBUploadSPA.DTO
{
    public class SubType : IEquatable<SubType>
    {
        public string PrintableSubtype { get; set; }
        public string TypeSubtype { get; set; }
        public string cold_adapted { get; set; }
        public string contaminants { get; set; }
        public string duplicate { get; set; }
        public string host { get; set; }
        public string host_detail { get; set; }
        public string reagent { get; set; }
        public string reassortant { get; set; }
        public string subsample_sampleclass { get; set; }
        public string Negative { get; set; }

        public SubType()
        {
            PrintableSubtype = string.Empty;
            TypeSubtype = string.Empty;
            cold_adapted = "N";
            contaminants = string.Empty;
            duplicate = "N";
            host = "Human";
            host_detail = "Human";
            reagent = "N";
            reassortant = "N";
            subsample_sampleclass = string.Empty;
            Negative = "N";
        }

        public void Parse(string parsevalue)
        {
            TypeSubtype = parsevalue;
            //composit value TypeSubtype
            var tempString = String.Empty;
            var values = TypeSubtype.Split(new string[] { @" MIX" }, StringSplitOptions.RemoveEmptyEntries);
            if (values.Length > 1)
            {
                tempString = values[0].Replace('/', ',') + values[1];
            }
            else if (values.Length == 1)
            {
                tempString = values[0].Replace('/', ',');
            }
            else
            {
                return;
            }

            values = tempString.Split('/');
            //first part is what we Areas going ToString continue ToString parse
            var value = values[0];
            var codes = value.Split('-');
            var subtype = codes[codes.Length - 1];
            if (@"NEG".Equals(subtype))
            {
                // this is not a subtype, it means negative
                Negative = "Y";
            }
            else
            {
                PrintableSubtype = subtype;
                Negative = "N";
            }

            // set above to N uless overridden
            // Per skype chat Bug 281
            //cold_adapted = String.Empty;
            //reassortant = String.Empty;
            //reagent = String.Empty;
            for (int i = 0; i < codes.Length - 1; i++)
            {
                switch (codes[i])
                {
                    case "D":
                        {
                            duplicate = "Y";
                            break;
                        }
                    case "S":
                        {
                            subsample_sampleclass = "Serum";
                            break;
                        }
                    case "M":
                        {
                            subsample_sampleclass = "Monoclonal";
                            break;
                        }
                    case "CA":
                        {
                            cold_adapted = "Y";
                            break;
                        }
                    case "R":
                        {
                            reassortant = "Y";
                            break;
                        }
                    case "RG":
                        {
                            reagent = "Y";
                            break;
                        }
                    case "AV":
                        {
                            host = "Avian";
                            host_detail = String.Empty;
                            break;
                        }
                    case "ENV":
                        {
                            host = "Environment";
                            host_detail = String.Empty;
                            break;
                        }
                    case "CN":
                        {
                            host = "Mammal";
                            host_detail = "Canine";
                            break;
                        }
                    case "EQ":
                        {
                            host = "Mammal";
                            host_detail = "Equine";
                            break;
                        }
                    case "FE":
                        {
                            host = "Mammal";
                            host_detail = "Feline";
                            break;
                        }
                    case "SW":
                        {
                            host = "Mammal";
                            host_detail = "Swine";
                            break;
                        }
                    case "AN":
                        {
                            host = "Mammal";
                            host_detail = "Other";
                            break;
                        }
                    default:
                        {
                            throw new Exception(@"Unknown code");
                        }
                }
            }
            //second part is contaminants (to be mapped eventually)
            contaminants = String.Join(@"/", values.ToList().GetRange(1, values.Length - 1).ToArray());
        }

        public bool Equals(SubType other)
        {
            return
            this.PrintableSubtype.Equals(other.PrintableSubtype) &&
            this.TypeSubtype.Equals(other.TypeSubtype) &&
            this.cold_adapted.Equals(other.cold_adapted) &&
            this.contaminants.Equals(other.contaminants) &&
            this.duplicate.Equals(other.duplicate) &&
            this.host.Equals(other.host) &&
            this.host_detail.Equals(other.host_detail) &&
            this.reagent.Equals(other.reagent) &&
            this.reassortant.Equals(other.reassortant) &&
            this.subsample_sampleclass.Equals(other.subsample_sampleclass) &&
            this.Negative.Equals(other.Negative);
        }
    }

}