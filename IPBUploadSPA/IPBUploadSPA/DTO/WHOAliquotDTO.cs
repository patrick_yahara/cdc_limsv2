﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "aliquot")]
    public class WHOAliquotDTO
    {
        [XmlIgnore]
        public String Id { get; set; }
        [Heading("Isolate ID")]
        [XmlElement(ElementName = "isolate_id")]
        public String IsolateId { get; set; }

        [Heading("CDC_Lab_Received")]
        [XmlElement(ElementName = "cdc_lab_received")]
        public String DateContractLabReceived { get; set; }

        [Heading("CUID")]
        [XmlElement(ElementName = "cuid")]
        public String CUID { get; set; }
        [Heading("Fraction Type")]
        [XmlElement(ElementName = "fraction_type")]
        public String TestCode { get; set; }
        [XmlIgnore]
        public virtual WHOIsolateDTO Isolate { get; set; }
        [Heading("CSID")]
        [XmlElement(ElementName = "csid")]
        public String ParentCSID { get; set; }
        [Heading("PARENT_CUID")]
        [XmlElement(ElementName = "parent_cuid")]
        public String ParentCUID { get; set; }
        [Heading("Passage")]
        [XmlElement(ElementName = "passage")]
        public String PassageLevel { get; set; }
        [Heading("Date harvested")]
        [XmlElement(ElementName = "date_harvested")]
        public String DateHarvested { get; set; }
        [Heading("titer")]
        [XmlElement(ElementName = "ha_titer")]
        public String Titer { get; set; }
        [Heading("RBC type")]
        [XmlElement(ElementName = "rbc_type")]
        public string RedBloodCellType { get; set; }

        [Heading("Sample_Class")]
        [XmlElement(ElementName = "sample_class")]
        public String SampleClass { get; set; }

        [Heading("(Sub) type")]
        [XmlElement(ElementName = "subtype")]
        public string SubType { get; set; }

        // * PDC YCX0 01 DEC 2015
        [Heading("Subsample Type")]
        [XmlElement(ElementName = "isolate_type")]
        public string IsolateType { get; set; }

        [XmlIgnore]
        public String Ordinal { get; set; }
        [XmlIgnore]
        public bool Validated { get; set; }
        [XmlIgnore]
        public String ValidationErrors { get; set; }

        [XmlElement(ElementName = "negative")]
        public String Negative { get; set; }
        [XmlElement(ElementName = "printable_subtype")]
        public String PrintableSubtype { get; set; }
        public String host { get; set; }
        public String host_detail { get; set; }
        public String duplicate { get; set; }
        [XmlIgnore]
        public String subsample_sampleclass { get; set; }
        public String cold_adapted { get; set; }
        public String reassortant { get; set; }
        public String reagent { get; set; }
        public String recombinant { get; set; }
        public String contaminants { get; set; }

        [Heading("Stored_Locally")]
        [XmlElement(ElementName = "stored_locally")]
        public String storedlocally { get; set; }

        [Heading("SAMP_LOC_BLDG")]
        [XmlElement(ElementName = "initial_storage")]
        public String InitialStorage { get; set; }

        public WHOAliquotDTO()
        {
            PrintableSubtype = string.Empty;
        }

        public void PreSerialize()
        {
            if (string.IsNullOrEmpty(SubType))
            {
                return;
            }
            var st = new SubType();
            st.Parse(SubType);
            this.cold_adapted = st.cold_adapted;
            this.contaminants = st.contaminants;
            this.duplicate = st.duplicate;
            this.host = st.host;
            this.host_detail = st.host_detail;
            this.Negative = st.Negative;
            this.PrintableSubtype = st.PrintableSubtype;
            this.reagent = st.reagent;
            this.reassortant = st.reassortant;
            this.subsample_sampleclass = st.subsample_sampleclass;
            // * do not overwrite 
            //this.SampleClass = subsample_sampleclass;
            this.SubType = st.TypeSubtype;
        }

    }
}