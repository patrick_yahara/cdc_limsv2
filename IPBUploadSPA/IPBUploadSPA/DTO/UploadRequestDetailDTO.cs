﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IPBUploadSPA.DTO
{
    [DataContract]
    public class UploadRequestDetailDTO
    {
        [DataMember]
        public long UploadRequestId { get; set; }
        [DataMember(Order = 0)]
        public string Errored { get; set; }
        [DataMember(Order = 1)]
        public string Display { get; set; }
        [DataMember(Order = 2)]
        public long Attempts { get; set; }
        [DataMember(Order = 3)]
        public long? Duration { get; set; }
        [DataMember(Order = 4)]
        public string Response { get; set; }
        [DataMember(Order = 5)]
        public string CSID { get; set; }
        [DataMember(Order = 6)]
        public string CUID { get; set; }
    }
}