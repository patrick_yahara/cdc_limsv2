﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using IPBUploadSPA.StarLIMSService;
using IPBUploadSPA.StarLIMSServiceWHO;

namespace IPBUploadSPA.DTO
{
    [DataContract]
    public class FluAccessionIPBReturnDTO
    {
        private readonly int _step;
        [DataMember(Order = 0)]
        public bool? ReturnedResult { get; set; }
        [IgnoreDataMember]
        public string FullMessage { get; set; }
        [IgnoreDataMember]
        public string InnerMessage { get; set; }
        [DataMember(Order = 1)]
        public string Summary { get; set; }
        [DataMember(Order = 2)]
        public string Sample { get; set; }
        [DataMember(Order = 6)]
        public string SubSample { get; set; }
        [DataMember(Order = 7)]
        public string Aliquot { get; set; }
        [DataMember(Order = 9)]
        public bool? InterpretedResult { get; set; }
        [DataMember(Order = 10)]
        public string Display { get; set; }
        [IgnoreDataMember]
        public string Ordinal { get; set; }
        public FluAccessionIPBReturnDTO()
        {
            _step = 1;
        }

        public FluAccessionIPBReturnDTO(int step, StarLIMSService.ReturnDTO[] messages, String ordinal)
        {
            var temp = messages.GetStringForDB();
            var tempR = temp.Split('|');
            this.FullMessage = temp;

            bool tempB;
            if (bool.TryParse(tempR[0], out tempB))
            {
                ReturnedResult = tempB;
            }
            else if (bool.TryParse(tempR[0].Substring(0, 4), out tempB))
            {
                ReturnedResult = tempB;
            }
            
            // * short term fix
            this.ReturnedResult = tempB;
            var lines = messages[0].sRet.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (_step == 2)
            {
                var sb = new StringBuilder();
                for (var i = 1; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }
            }
            else
            {
                if (lines.Length > 1)
                    Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
                if (lines.Length > 2)
                    SubSample = lines[2].Substring(lines[2].IndexOf(':') + 1);
                var sb = new StringBuilder();
                for (var i = 3; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }

            }
            UpdateInterpretedResult();
            var sbDisplay = new StringBuilder();
            if (_step == 1)
            {
                if (ReturnedResult.HasValue && ReturnedResult.Value)
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0}", Sample.Substring(0, Sample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("SubSample: {0}", SubSample.Substring(0, SubSample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                    sbDisplay.AppendLine(string.Format("{0}", Aliquot));
                }
                else
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0} Error: {1}", Sample, Summary));
                    sbDisplay.AppendLine(SubSample);
                    sbDisplay.AppendLine(Aliquot);
                }

            }
            else if (_step == 2)
            {
                sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                sbDisplay.AppendLine(string.Format("{0}", Aliquot));
            }
            Display = sbDisplay.ToString();
            Display = Regex.Replace(Display, @"\r\n?|\n", "<br />");
        }

        public FluAccessionIPBReturnDTO(int step, StarLIMSServiceWHO.ReturnDTO[] messages, String ordinal)
        {
            var temp = messages.GetStringForDB();
            var tempR = temp.Split('|');
            this.FullMessage = temp;

            bool tempB;
            if (bool.TryParse(tempR[0], out tempB))
            {
                ReturnedResult = tempB;
            }
            else if (bool.TryParse(tempR[0].Substring(0, 4), out tempB))
            {
                ReturnedResult = tempB;
            }

            // * short term fix
            this.ReturnedResult = tempB;
            var lines = messages[0].sRet.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (_step == 2)
            {
                var sb = new StringBuilder();
                for (var i = 1; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }
            }
            else
            {
                if (lines.Length > 1)
                    Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
                if (lines.Length > 2)
                    SubSample = lines[2].Substring(lines[2].IndexOf(':') + 1);
                var sb = new StringBuilder();
                for (var i = 3; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }

            }
            UpdateInterpretedResult();
            var sbDisplay = new StringBuilder();
            if (_step == 1)
            {
                if (ReturnedResult.HasValue && ReturnedResult.Value)
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0}", Sample.Substring(0, Sample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("SubSample: {0}", SubSample.Substring(0, SubSample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                    sbDisplay.AppendLine(string.Format("{0}", Aliquot));
                }
                else
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0} Error: {1}", Sample, Summary));
                    sbDisplay.AppendLine(SubSample);
                    sbDisplay.AppendLine(Aliquot);
                }

            }
            else if (_step == 2)
            {
                sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                sbDisplay.AppendLine(string.Format("{0}", Aliquot));
            }
            Display = sbDisplay.ToString();
            Display = Regex.Replace(Display, @"\r\n?|\n", "<br />");
        }

        public FluAccessionIPBReturnDTO(int step, StarLIMSServiceZVT.ReturnDTO[] messages, String ordinal)
        {
            var temp = messages.GetStringForDB();
            var tempR = temp.Split('|');
            this.FullMessage = temp;

            bool tempB;
            if (bool.TryParse(tempR[0], out tempB))
            {
                ReturnedResult = tempB;
            }
            else if (bool.TryParse(tempR[0].Substring(0, 4), out tempB))
            {
                ReturnedResult = tempB;
            }

            // * short term fix
            this.ReturnedResult = tempB;
            var lines = messages[0].sRet.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (_step == 2)
            {
                var sb = new StringBuilder();
                for (var i = 1; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }
            }
            else
            {
                if (lines.Length > 1)
                    Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
                if (lines.Length > 2)
                    SubSample = lines[2].Substring(lines[2].IndexOf(':') + 1);
                var sb = new StringBuilder();
                for (var i = 3; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }

            }
            UpdateInterpretedResult();
            var sbDisplay = new StringBuilder();
            if (_step == 1)
            {
                if (ReturnedResult.HasValue && ReturnedResult.Value)
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0}", Sample.Substring(0, Sample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("SubSample: {0}", SubSample.Substring(0, SubSample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                    sbDisplay.AppendLine(string.Format("{0}", Aliquot));
                }
                else
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0} Error: {1}", Sample, Summary));
                    sbDisplay.AppendLine(SubSample);
                    sbDisplay.AppendLine(Aliquot);
                }

            }
            else if (_step == 2)
            {
                sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                sbDisplay.AppendLine(string.Format("{0}", Aliquot));
            }
            Display = sbDisplay.ToString();
            Display = Regex.Replace(Display, @"\r\n?|\n", "<br />");
        }

        public FluAccessionIPBReturnDTO(int step, StarLIMSServiceFLAPWHO.ReturnDTO messages, String ordinal)
        {
            var temp = messages.GetStringForDB();
            var tempR = temp.Split('|');
            this.FullMessage = temp;

            bool tempB;
            if (bool.TryParse(tempR[0], out tempB))
            {
                ReturnedResult = tempB;
            }
            else if (bool.TryParse(tempR[0].Substring(0, 4), out tempB))
            {
                ReturnedResult = tempB;
            }

            // * short term fix
            this.ReturnedResult = tempB;
            var lines = messages.sRet.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (_step == 2)
            {
                var sb = new StringBuilder();
                for (var i = 1; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }
            }
            else
            {
                if (lines.Length > 1)
                    Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
                if (lines.Length > 2)
                    SubSample = lines[2].Substring(lines[2].IndexOf(':') + 1);
                var sb = new StringBuilder();
                for (var i = 3; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }

            }
            UpdateInterpretedResult();
            var sbDisplay = new StringBuilder();
            if (_step == 1)
            {
                if (ReturnedResult.HasValue && ReturnedResult.Value)
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0}", Sample.Substring(0, Sample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("SubSample: {0}", SubSample.Substring(0, SubSample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                    sbDisplay.AppendLine(string.Format("{0}", Aliquot));
                }
                else
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0} Error: {1}", Sample, Summary));
                    sbDisplay.AppendLine(SubSample);
                    sbDisplay.AppendLine(Aliquot);
                }

            }
            else if (_step == 2)
            {
                sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                sbDisplay.AppendLine(string.Format("{0}", Aliquot));
            }
            Display = sbDisplay.ToString();
            Display = Regex.Replace(Display, @"\r\n?|\n", "<br />");
        }


        public FluAccessionIPBReturnDTO(int step, string message, String ordinal)
        {
            _step = step;
            FullMessage = message;
            Ordinal = ordinal;
            // UGLY
            if (String.IsNullOrEmpty(message))
            {
                Display = String.Format(@"Row: {0}; Error parsing return value", Ordinal);
                return;
            }
            if (message.IndexOf(@"types:ReturnDTO") <= 0)
            {
                Display = String.Format(@"Row: {0}; Error parsing return value", Ordinal);
                return;
            }
            var tempS = message.Substring(message.IndexOf(@"types:ReturnDTO"));
            tempS = tempS.Substring(0, tempS.IndexOf(@"</soap:Body>"));
            tempS = tempS.Replace(@"types:ReturnDTO", @"ReturnDTO");

            var xDoc = GetXmlDocumentFromString(tempS);


            var items = xDoc.SelectNodes("ReturnDTO");

            foreach (XmlNode item in items)
            {
                var temp = item.SelectSingleNode("bRet").InnerText;
                bool tempB;
                if (bool.TryParse(temp, out tempB))
                {
                    ReturnedResult = tempB;
                }
                else if (bool.TryParse(temp.Substring(0, 4), out tempB))
                {
                    ReturnedResult = tempB;
                }
                InnerMessage = item.SelectSingleNode("sRet").InnerText;
            }
            var lines = InnerMessage.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (lines.Length > 0)
                Summary = lines[0];
            if (_step == 2)
            {
                var sb = new StringBuilder();
                for (var i = 1; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }
            }
            else
            {
                if (lines.Length > 1)
                    Sample = lines[1].Substring(lines[1].IndexOf(':') + 1);
                if (lines.Length > 2)
                    SubSample = lines[2].Substring(lines[2].IndexOf(':') + 1);
                var sb = new StringBuilder();
                for (var i = 3; i < lines.Length; i++)
                {
                    sb.AppendLine("\t" + lines[i].Substring(lines[i].IndexOf(':') + 1));
                }
                if (!String.IsNullOrEmpty(sb.ToString()))
                {
                    Aliquot = sb.ToString();
                }

            }
            UpdateInterpretedResult();
            var sbDisplay = new StringBuilder();
            if (_step == 1)
            {
                if (ReturnedResult.HasValue && ReturnedResult.Value)
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0}", Sample.Substring(0, Sample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("SubSample: {0}", SubSample.Substring(0, SubSample.IndexOf('-'))));
                    sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                    sbDisplay.AppendLine(string.Format("{0}", Aliquot));
                }
                else
                {
                    sbDisplay.AppendLine(string.Format(@"Sample: {0} Error: {1}", Sample, Summary));
                    sbDisplay.AppendLine(SubSample);
                    sbDisplay.AppendLine(Aliquot);
                }

            }
            else if (_step == 2)
            {
                sbDisplay.AppendLine(string.Format("Aliquots: {0}", ""));
                sbDisplay.AppendLine(string.Format("{0}", Aliquot));
            }
            Display = sbDisplay.ToString();
            Display = Regex.Replace(Display, @"\r\n?|\n", "<br />");
        }

        private XmlDocument GetXmlDocumentFromString(string xml)
        {
            var doc = new XmlDocument();

            using (var sr = new StringReader(xml))
            using (var xtr = new XmlTextReader(sr) { Namespaces = false })
                doc.Load(xtr);

            return doc;
        }

        private void UpdateInterpretedResult()
        {
            InterpretedResult = ReturnedResult;
            if (!InterpretedResult.GetValueOrDefault())
            {

            }
        }

    }  
}