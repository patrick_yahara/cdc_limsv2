﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "subsample")]
    public class IPBSubsampleDTO
    {
        public IPBSubsampleDTO()
        {
            this.SampleClass = String.Empty;
        }
        [XmlElement(ElementName = "sample_class")]
        public String SampleClass { get; set; }
        [XmlArray("aliquots")]
        public List<IPBAliquotDTO> Aliquots { get; set; }
    }
}