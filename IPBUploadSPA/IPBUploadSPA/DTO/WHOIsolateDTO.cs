﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "subsample")]
    public class WHOIsolateDTO
    {
        public WHOIsolateDTO()
        {
            this.Aliquots = new List<WHOAliquotDTO_Step1>();
        }
        [Heading("Isolate ID")]
        [XmlElement(ElementName = "isolate_id")]
        public String Id { get; set; }
        [Heading("Passage")]
        [XmlElement(ElementName = "passage")]
        public String PassageLevel { get; set; }
        [Heading("Date harvested")]
        public String DateHarvested { get; set; }
        [Heading("titer")]
        public String Titer { get; set; }
        [Heading("RBC type")]
        public String RedBloodCellType { get; set; }
        [XmlElement(ElementName = "date_inoculated")]
        public String DateInoculated { get; set; }
        [XmlElement(ElementName = "sample_class")]
        public String SampleClass { get; set; }
        [XmlElement(ElementName = "subtype")]
        public String SubType { get; set; }

        [XmlElement(ElementName = "cdc_lab_received")]
        public String DateContractLabReceived { get; set; }

        [XmlElement(ElementName = "printable_subtype")]
        public String PrintableSubType { get; set; }

        [Heading("Host_Cell_Line")]
        [XmlElement(ElementName = "host_cell_line")]
        public string HostCellLine { get; set; }
        [Heading("Substrate")]
        [XmlElement(ElementName = "substrate")]
        public string Substrate { get; set; }
        [Heading("Passage_Number")]
        [XmlElement(ElementName = "passage_number")]
        public string PassageNumber { get; set; }

        [XmlArray("aliquots")]
        public List<WHOAliquotDTO_Step1> Aliquots { get; set; }
        [XmlIgnore]
        public virtual WHOSpecimenDTO Specimen { get; set; }

    }
}