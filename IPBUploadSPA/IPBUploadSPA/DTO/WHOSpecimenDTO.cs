﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "sample")]
    public class WHOSpecimenDTO
    {
        public WHOSpecimenDTO()
        {
            Isolates = new List<WHOIsolateDTO>();
        }
        [XmlIgnore]
        public String Id { get; set; }
        [Heading("CSID")]
        public String CSID { get; set; }
        [Heading("CUID")]
        public String CUID { get; set; }
        [Heading("PACKAGE_ID")]
        [XmlElement(ElementName = "package_name")]
        public String PackageId { get; set; }
        [Heading("Sender ID")]
        [XmlElement(ElementName = "sender_id")]
        public String RasClientId { get; set; }
        [Heading("Specimen ID")]
        [XmlElement(ElementName = "specimen_id")]
        public String SpecimenID { get; set; }
        [Heading("Date_Collected")]
        [XmlElement(ElementName = "date_collected")]
        public String DateCollected { get; set; }
        //* 836
        [Heading("Date_Collected_Estimated")]
        [XmlElement(ElementName = "date_collected_estimated")]
        public String DateCollectedEstimated { get; set; }

        [Heading("Patient_Age")]
        [XmlElement(ElementName = "patient_age")]
        public String PatientAge { get; set; }
        [Heading("Patient_Age_Type")]
        [XmlElement(ElementName = "patient_age_units")]
        public String PatientAgeUnits { get; set; }
        [Heading("Patient_Sex")]
        [XmlElement(ElementName = "patient_gender")]
        public String PatientSex { get; set; }
        [Heading("Extent_of_Activity")]
        public String Activity { get; set; }
        [Heading("Passage History")]
        public String PassageHistory { get; set; }
        [Heading("(Sub) type")]
        public String SubType { get; set; }
        [Heading("WHO_Submitted_Subtype")]
        [XmlElement(ElementName = "who_submitted_subtype")]
        public String SubmittedSubType { get; set; }
        [Heading("Date_Received")]
        [XmlElement(ElementName = "date_received")]
        public String DateReceived { get; set; }
        [Heading("Serum_Condition")]
        [XmlElement(ElementName = "condition")]
        public String SpecimenCondition { get; set; }
        [Heading("Specimen_Type")]
        [XmlElement(ElementName = "specimen_type")]
        public String SpecimenSource { get; set; }
        [Heading("Comp CDC ID #1")]
        [XmlElement(ElementName = "comp_cdc_id_1")]
        public String CompCSID1 { get; set; }
        [Heading("Comp CDC ID #2")]
        [XmlElement(ElementName = "comp_cdc_id_2")]
        public String CompCSID2 { get; set; }
        [Heading("Specimen_Collection_Other")]
        [XmlElement(ElementName = "other_location")]
        public String OtherLocation { get; set; }
        [Heading("Drug Resistant")]
        [XmlElement(ElementName = "drug_resistant")]
        public String DrugResistant { get; set; }
        [Heading("FATAL")]
        [XmlElement(ElementName = "fatal")]
        public String Deceased { get; set; }
        [Heading("Study_Name")]
        [XmlElement(ElementName = "study")]
        public String SpecialStudy { get; set; }
        [Heading("Nursing Home")]
        [XmlElement(ElementName = "nursing_home")]
        public String NursingHome { get; set; }
        [Heading("Flu_Vaccination")]
        [XmlElement(ElementName = "vaccinated")]
        public String Vaccinated { get; set; }
        [Heading("Travel_Place")]
        [XmlElement(ElementName = "travel_location")]
        public String Travel { get; set; }
        [Heading("Reason for submission")]
        [XmlElement(ElementName = "reason_for_submission")]
        public String ReasonForSubmission { get; set; }
        [Heading("Comments")]
        public String Comments { get; set; }

        [Heading("Contract_Lab_From")]
        [XmlElement(ElementName = "contract_lab_from")]
        public String ContractLabFrom { get; set; }


        [Heading("Strain_designation")]
        [XmlElement(ElementName = "strain_designation")]
        public String StrainDesignation { get; set; }

        [Heading("Containment level")]
        [XmlElement(ElementName = "containment_level")]
        public String ContainmentLevel { get; set; }

        [Heading("MTA")]
        [XmlElement(ElementName = "mta")]
        public String mta { get; set; }

        [Heading("Specimen_Collection_Country")]
        public String Country { get; set; }
        [Heading("Specimen_Collection_State")]
        public String State { get; set; }


        [Heading("Travel_City")]
        [XmlElement(ElementName = "travel_city")]
        public string TravelCity { get; set; }
        [Heading("Specimen_Collection_County")]
        public string County { get; set; }
        [Heading("Specimen_Collection_City")]
        public string City { get; set; }
        [Heading("Travel_State")]
        [XmlElement(ElementName = "travel_state")]
        public string TravelState { get; set; }
        [Heading("Travel_Country")]
        [XmlElement(ElementName = "travel_country")]
        public string TravelCountry { get; set; }
        [Heading("Travel_County")]
        [XmlElement(ElementName = "travel_county")]
        public string TravelCounty { get; set; }
        [Heading("Travel_Return_Date")]
        [XmlElement(ElementName = "travel_return_date")]
        public String TravelReturnDate { get; set; }

        [XmlArray("subsamples")]
        public List<WHOIsolateDTO> Isolates { get; set; }

        [Heading("Login_Date")]
        [XmlElement(ElementName = "date_login")]
        public String LoginDate { get; set; }

        [Heading("Date_Logged")]
        [XmlElement(ElementName = "contractlab_logged_date")]
        public String DateLogged { get; set; }

        [Heading("Collection_Comment")]
        [XmlElement(ElementName = "collection_comment")]
        public String CollectionComment { get; set; }
        
        /*
         * To be normalized before serialization
         */
        //Isolate
        [Heading("Sample_Class")]
        [XmlIgnore]
        public String SampleClass { get; set; }
        [Heading("Isolate ID")]
        [XmlIgnore]
        public string IsolateId { get; set; }

        [Heading("CDC_Lab_Received")]
        [XmlIgnore]
        public String DateContractLabReceived { get; set; }

        [Heading("Host_Cell_Line")]
        [XmlIgnore]
        public string HostCellLine { get; set; }
        [Heading("Substrate")]
        [XmlIgnore]
        public string Substrate { get; set; }
        [Heading("Passage_Number")]
        [XmlIgnore]
        public string PassageNumber { get; set; }


        //Aliquot
        [Heading("Fraction Type")]
        [XmlIgnore]
        public String TestCode { get; set; }

        [Heading("Date inoculated")]
        [XmlIgnore]
        public String DateInoculated { get; set; }

        [Heading("SAMP_LOC_BLDG")]
        [XmlIgnore]
        public String InitialStorage { get; set; }

        [Heading("Stored_Locally")]
        [XmlIgnore]
        public String storedlocally { get; set; }

        [Heading("PCRInfA")]
        [XmlIgnore]
        public string PCRInfA { get; set; }
        [Heading("PCRpdmInfA")]
        [XmlIgnore]
        public string PCRpdmInfA { get; set; }
        [Heading("PCRpdmH1")]
        [XmlIgnore]
        public string PCRpdmH1 { get; set; }
        [Heading("PCRH3")]
        [XmlIgnore]
        public string PCRH3 { get; set; }
        [Heading("PCRInfB")]
        [XmlIgnore]
        public string PCRInfB { get; set; }
        [Heading("PCRBVic")]
        [XmlIgnore]
        public string PCRBVic { get; set; }
        [Heading("PCRBYam")]
        [XmlIgnore]
        public string PCRBYam { get; set; }

        [XmlIgnore]
        public String Ordinal { get; set; }
        [XmlIgnore]
        public bool Validated { get; set; }
        [XmlIgnore]
        public String ValidationErrors { get; set; }

        [XmlElement(ElementName = "negative")]
        public String Negative { get; set; }
        [XmlElement(ElementName = "printable_subtype")]
        public String PrintableSubtype { get; set; }
        [Heading("Host")]
        public String host { get; set; }
        [Heading("Host Details")]
        public String host_detail { get; set; }
        [Heading("isr_cuid")]
        public String isr_cuid { get; set; }
        public String duplicate { get; set; }
        public String subsample_sampleclass { get; set; }
        public String cold_adapted { get; set; }
        public String reassortant { get; set; }
        public String reagent { get; set; }
        public String recombinant { get; set; }
        public String contaminants { get; set; }

        public void PreSerialize()
        {
            // * 23 SEPT 2016 PDC Data tweaking
            // * Convert ' - ' ToString '-'
            this.ReasonForSubmission = this.ReasonForSubmission.Replace(" - ", "-");

            if (string.IsNullOrEmpty(SubmittedSubType))
            {
                SubmittedSubType = SubType;    
            }
            
            var st = new SubType();
            
            st.Parse(SubType);
            this.cold_adapted = st.cold_adapted;
            this.contaminants = st.contaminants;
            this.duplicate = st.duplicate;
            // * 402
            if (!string.IsNullOrEmpty(this.host))
            {
                if (!st.host.Equals(this.host) && !st.host.Equals("Human"))
                {
                    throw new Exception(string.Format("supplied host does not match parsed host", this.host, st.host));
                }
            } else
            {
                this.host = st.host;
            }

            if (!string.IsNullOrEmpty(this.host_detail))
            {
                if (!st.host_detail.Equals(this.host_detail) && !st.host_detail.Equals("Human"))
                {
                    throw new Exception(string.Format("supplied host detail does not match parsed host detail", this.host_detail, st.host_detail));
                }
            }
            else
            {
                this.host_detail = st.host_detail;
            }

            this.Negative = st.Negative;
            this.PrintableSubtype = st.PrintableSubtype;
            this.reagent = st.reagent;
            this.reassortant = st.reassortant;
            this.subsample_sampleclass = st.subsample_sampleclass;
            //this.SampleClass = subsample_sampleclass;
            this.SubType = st.TypeSubtype;

            // * FLULIMS 767 YCX0 06 JAN 2017
            var validCt = new Regex(@"^(((?:\d*\.)?\d+)|NT|nt)$");
            if (!string.IsNullOrEmpty(this.PCRBVic))
            {
                this.PCRBVic = this.PCRBVic.Trim();
                var match = validCt.Match(this.PCRBVic);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRBVic is not valid: {0}", this.PCRBVic));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRBYam))
            {
                this.PCRBYam = this.PCRBYam.Trim();
                var match = validCt.Match(this.PCRBYam);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRBYam is not valid: {0}", this.PCRBYam));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRH3))
            {
                this.PCRH3 = this.PCRH3.Trim();
                var match = validCt.Match(this.PCRH3);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRH3 is not valid: {0}", this.PCRH3));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRInfA))
            {
                this.PCRInfA = this.PCRInfA.Trim();
                var match = validCt.Match(this.PCRInfA);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRInfA is not valid: {0}", this.PCRInfA));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRInfB))
            {
                this.PCRInfB = this.PCRInfB.Trim();
                var match = validCt.Match(this.PCRInfB);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRInfB is not valid: {0}", this.PCRInfB));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRpdmH1))
            {
                this.PCRpdmH1 = this.PCRpdmH1.Trim();
                var match = validCt.Match(this.PCRpdmH1);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRpdmH1 is not valid: {0}", this.PCRpdmH1));
                }
            }
            if (!string.IsNullOrEmpty(this.PCRpdmInfA))
            {
                this.PCRpdmInfA = this.PCRpdmInfA.Trim();
                var match = validCt.Match(this.PCRpdmInfA);
                if (match == null || !match.Success)
                {
                    throw new Exception(string.Format("Supplied PCRpdmInfA is not valid: {0}", this.PCRpdmInfA));
                }
            }
        }

    }
}