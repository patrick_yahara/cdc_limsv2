﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPBUploadSPA.DTO
{
    public static class IPBUploadDTOValidator
    {
        public static bool IsValid(ref IPBUploadDTO value, out bool finished, out List<String> errors)
        {
            errors = new List<String>();
            if (String.IsNullOrEmpty(value.CUID) && String.IsNullOrEmpty(value.CSID))
            {
                finished = true;
                return false;
            }
            try
            {
                value.PreSerialize(); // REQUIRED TO PASS
            }
            catch (Exception ex)
            {
                errors.Add(@"Error Preparing Data for Import: " + ex.Message);
            }
            if (String.IsNullOrEmpty(value.CUID))
            {
                errors.Add(@"CUID is required");
            }
            if (String.IsNullOrEmpty(value.CSID))
            {
                errors.Add(@"CSID is required");
            }

            if (String.IsNullOrEmpty(value.PackageID) && String.IsNullOrEmpty(value.SphlSubName))
            {
                errors.Add(@"Either PackageID or SPHLSubName is required");
            }
            finished = false;
            return errors.Count == 0;
        }
        public static bool IsValidAliquotOnly(ref IPBUploadDTO value, out bool finished, out List<String> errors)
        {
            errors = new List<String>();
            if (String.IsNullOrEmpty(value.CUID) && String.IsNullOrEmpty(value.CSID))
            {
                finished = true;
                return false;
            }
            try
            {
                value.PreSerialize(); // REQUIRED TO PASS
            }
            catch (Exception ex)
            {
                errors.Add(@"Error Preparing Data for Import: " + ex.Message);
            }
            if (String.IsNullOrEmpty(value.ParentAliquot))
            {
                errors.Add(@"ParentAliquot is required");
            }
            finished = false;
            return errors.Count == 0;
        }
        public static bool IsValidAliquotOnly(ref WHOAliquotDTO value, out bool finished, out List<String> errors)
        {
            errors = new List<String>();
            try
            {
                value.PreSerialize();
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }
            if (String.IsNullOrEmpty(value.CUID) && String.IsNullOrEmpty(value.ParentCSID))
            {
                finished = true;
                return false;
            }
            if (String.IsNullOrEmpty(value.ParentCUID))
            {
                errors.Add(@"PARENT_CUID is required");
            }
            finished = false;
            return errors.Count == 0;
        }

        public static bool IsValid(ref WHOSpecimenDTO value, out bool finished, out List<String> errors)
        {
            errors = new List<String>();
            try
            {
                value.PreSerialize();
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }
            if (String.IsNullOrEmpty(value.CUID) || String.IsNullOrEmpty(value.CSID))
            {
                finished = true;
                return false;
            }
            finished = false;
            return errors.Count == 0;
        }
        
    }
}