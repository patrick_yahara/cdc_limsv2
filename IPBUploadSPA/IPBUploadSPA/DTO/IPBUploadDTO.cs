﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{
    [XmlType(TypeName = "sample")]
    public class IPBUploadDTO
    {
        public IPBUploadDTO()
        {
            // * can remove when Excel has these columns
            SpecimenCollectionSiteCountry = String.Empty;
            SpecimenCollectionSiteState = String.Empty;
            SpecimenCollectionSiteCounty = String.Empty;
            VolumnUnitOfMeasure = String.Empty;

            this.SphlSubName = String.Empty;
            this.SphlSubName2 = String.Empty;
            this.SphlSubAddr1 = String.Empty;
            this.SphlSubAddr2 = String.Empty;
            this.SphlSubCity = String.Empty;
            this.SphlSubState = String.Empty;
            this.SphlSubZip = String.Empty;
            this.SphlSubCountry = String.Empty;
            this.SphlSubCategory = String.Empty;
            this.SphlSubFullname = String.Empty;
            this.SphlSubFullname1 = String.Empty;
            this.SphlSubPhone = String.Empty;
            this.SphlSubFax = String.Empty;
            this.SphlSubEmail = String.Empty;

            this.Exposure = String.Empty;
            this.ExposureStartDate = String.Empty;
            this.ExposureEndDate = String.Empty;
            this.VaccinesReceived = String.Empty;
            this.Adjuvant = String.Empty;
            this.VaccineConcentration = String.Empty;
            this.DosesReceived = String.Empty;

        }
        public void PreSerialize()
        {
            if (String.IsNullOrEmpty(SphlSubName))
            {
                SphlSubName = SphlSubName2;
            }
            if (String.IsNullOrEmpty(SphlSubFullname))
            {
                SphlSubFullname = SphlSubFullname1;
            }
            if (String.IsNullOrEmpty(SampleVolume))
            {
                throw new Exception(@"Sample Volume is required");
            };
            var tempD = 0.0;
            if (double.TryParse(SampleVolume, out tempD))
            {
                SampleVolume = tempD.ToString();
            }
            else
            {
                var tempsv = SampleVolume;
                try
                {
                    var numAlpha = new Regex("(?<Numeric>[0-9,.]*)(?<Alpha>[a-zA-Z ]*)");
                    var match = numAlpha.Match(tempsv);
                    if (match != null && match.Groups["Numeric"].Length > 0)
                    {
                        if (double.TryParse(match.Groups["Numeric"].Value, out tempD))
                        {
                            SampleVolume = tempD.ToString();
                        }
                        else
                        {
                            string newString = Regex.Replace(tempsv, "[^.0-9]", "");
                            tempD = 0.0;
                            if (double.TryParse(newString, out tempD))
                            {
                                SampleVolume = tempD.ToString();
                            }
                            else
                            {
                                throw new Exception(@"Unable to parse Sample Volume.");
                            }
                        }

                    }
                    else 
                    {
                        string newString = Regex.Replace(tempsv, "[^.0-9]", "");
                        tempD = 0.0;
                        if (double.TryParse(newString, out tempD))
                        {
                            SampleVolume = tempD.ToString();
                        }
                        else
                        {
                            throw new Exception(@"Unable to parse Sample Volume.");
                        }
                    }
                    if (match != null && match.Groups["Alpha"].Length > 0)
                    {
                        VolumnUnitOfMeasure = match.Groups["Alpha"].Value;
                    }
                }
                catch (Exception ex)
                {

                    string newString = Regex.Replace(tempsv, "[^.0-9]", "");
                    tempD = 0.0;
                    if (double.TryParse(newString, out tempD))
                    {
                        SampleVolume = tempD.ToString();
                    }
                    else
                    {
                        throw new Exception(@"Unable to parse Sample Volume.");
                    }
                }

            }
        }
        [XmlIgnore]
        public String Ordinal { get; set; }
        [XmlIgnore]
        public bool Validated { get; set; }
        [XmlIgnore]
        public String ValidationErrors { get; set; }

        [Heading("Exposure")]
        [XmlElement(ElementName = "exposure")]
        public String Exposure { get; set; }
        [Heading("Exposure_Start_Date")]
        [XmlElement(ElementName = "exposure_start_date")]
        public String ExposureStartDate { get; set; }
        [Heading("Exposure_End_Date")]
        [XmlElement(ElementName = "exposure_end_date")]
        public String ExposureEndDate { get; set; }
        [Heading("Vaccines_Received")]
        [XmlElement(ElementName = "vaccines_received")]
        public String VaccinesReceived { get; set; }
        [Heading("Adjuvant")]
        [XmlElement(ElementName = "adjuvant")]
        public String Adjuvant { get; set; }
        [Heading("Vaccine_Concentration")]
        [XmlElement(ElementName = "vaccine_concentration")]
        public String VaccineConcentration { get; set; }
        [Heading("Doses_Received")]
        [XmlElement(ElementName = "doses_received")]
        public String DosesReceived { get; set; }

        [Heading("study_question_1_caption")]
        [XmlElement(ElementName = "study_question_1_caption")]
        public String StudyQuestion1Caption { get; set; }
        [Heading("study_question_1_response")]
        [XmlElement(ElementName = "study_question_1_response")]
        public String StudyQuestion1Response { get; set; }
        [Heading("study_question_2_caption")]
        [XmlElement(ElementName = "study_question_2_caption")]
        public String StudyQuestion2Caption { get; set; }
        [Heading("study_question_2_response")]
        [XmlElement(ElementName = "study_question_2_response")]
        public String StudyQuestion2Response { get; set; }

        [Heading("SPHL_SUB_NAME")]
        [XmlElement(ElementName = "sphl_sub_name")]
        public String SphlSubName { get; set; }

        [Heading("SPHL_SUB_NAME2")]
        [XmlIgnore]
        public String SphlSubName2 { get; set; }

        [Heading("SPHL_SUB_ADDR1")]
        [XmlElement(ElementName = "sphl_sub_addr1")]
        public String SphlSubAddr1 { get; set; }

        [Heading("SPHL_SUB_ADDR2")]
        [XmlElement(ElementName = "sphl_sub_addr2")]
        public String SphlSubAddr2 { get; set; }

        [Heading("SPHL_SUB_CITY")]
        [XmlElement(ElementName = "sphl_sub_city")]
        public String SphlSubCity { get; set; }

        [Heading("SPHL_SUB_STATE")]
        [XmlElement(ElementName = "sphl_sub_state")]
        public String SphlSubState { get; set; }

        [Heading("SPHL_SUB_ZIP")]
        [XmlElement(ElementName = "sphl_sub_zip")]
        public String SphlSubZip { get; set; }

        [Heading("SPHL_SUB_COUNTRY")]
        [XmlElement(ElementName = "sphl_sub_country")]
        public String SphlSubCountry { get; set; }

        [Heading("SPHL_SUB_CATEGORY")]
        [XmlElement(ElementName = "sphl_sub_category")]
        public String SphlSubCategory { get; set; }

        [Heading("SPHL_SUB_FULLNAME")]
        [XmlElement(ElementName = "sphl_sub_fullname")]
        public String SphlSubFullname { get; set; }

        [Heading("SPHL_SUB_FULLNAME1")]
        [XmlIgnore]
        public String SphlSubFullname1 { get; set; }

        [Heading("SPHL_SUB_PHONE")]
        [XmlElement(ElementName = "sphl_sub_phone")]
        public String SphlSubPhone { get; set; }

        [Heading("SPHL_SUB_FAX")]
        [XmlElement(ElementName = "sphl_sub_fax")]
        public String SphlSubFax { get; set; }

        [Heading("SPHL_SUB_EMAIL")]
        [XmlElement(ElementName = "sphl_sub_email")]
        public String SphlSubEmail { get; set; }



        [Heading("Sample_Class")]
        [XmlIgnore]
        public String SampleClass { get; set; }

        [Heading("PACKAGE_ID")]
        [XmlElement(ElementName = "package_id")]
        public String PackageID { get; set; }
        [Heading("CSID")]
        [XmlElement(ElementName = "csid")]
        public String CSID{ get; set; }
        [Heading("CUID")]
        [XmlIgnore]
        public String CUID { get; set; }
        [Heading("ORIGIN")]
        [XmlElement(ElementName = "host")]
        public String Origin { get; set; }
        [Heading("PATIENT_ID1")]
        [XmlElement(ElementName = "patient_id_1")]
        public String PatientID1 { get; set; }
        [Heading("PatID1_Assigned_By")]
        [XmlElement(ElementName = "patient_id_1_assigned_by")]
        public String PatID1AssignedBy { get; set; }
        [Heading("PATIENT_ID_2")]
        [XmlElement(ElementName = "patient_id_2")]
        public String PatientID2 { get; set; }
        [Heading("PatID2_Assigned_By")]
        [XmlElement(ElementName = "patient_id_2_assigned_by")]
        public String PatID2AssignedBy { get; set; }
        [Heading("USA_Residence")]
        [XmlElement(ElementName = "usa_residence")]
        public String USAResidence { get; set; }
        [Heading("Specimen_Type")]
        [XmlElement(ElementName = "specimen_type")]
        public String SpecimenType { get; set; }
        [Heading("Serum_Condition")]
        [XmlElement(ElementName = "condition")]
        public String SerumCondition { get; set; }
        [Heading("Serology_Type")]
        [XmlElement(ElementName = "serology_type")]
        public String SerologyType { get; set; }
        [Heading("Date_Collected")]
        [XmlElement(ElementName = "date_collected")]
        public String DateCollected { get; set; }
        [Heading("Date_Received")]
        [XmlElement(ElementName = "date_received")]
        public String DateReceived { get; set; }
        [Heading("SUSPAGENTCAT")]
        [XmlElement(ElementName = "suspected_agent_category")]
        public String SuspectedAgentCategory { get; set; }
        [Heading("Clinical_Diagnosis")]
        [XmlElement(ElementName = "clinical_diagnosis")]
        public String ClinicalDiagnosis { get; set; }
        [Heading("Associated_illness")]
        [XmlElement(ElementName = "associated_illness")]
        public String AssociatedIllness { get; set; }
        [Heading("Family_illness")]
        [XmlElement(ElementName = "family_illness")]
        public String FamilyIllness { get; set; }
        [Heading("Family_illness_Comment")]
        [XmlElement(ElementName = "family_illness_comment")]
        public String FamilyIllnessComment { get; set; }
        [Heading("Community_illness")]
        [XmlElement(ElementName = "community_illness")]
        public String CommunityIllness { get; set; }
        [Heading("Community_Illness_Comment")]
        [XmlElement(ElementName = "community_illness_comment")]
        public String CommunityIllnessComment { get; set; }
        [Heading("Extent_of_Activity")]
        [XmlElement(ElementName = "activity")]
        public String ExtentOfActivity { get; set; }
        [Heading("Animal_Contacts")]
        [XmlElement(ElementName = "animal_contacts")]
        public String AnimalContacts { get; set; }
        [Heading("Comments")]
        [XmlElement(ElementName = "comments")]
        public String Comments { get; set; }
        [Heading("Lab_Study_ID")]
        [XmlElement(ElementName = "local_id_1")]
        public String LabStudyID { get; set; }
        [Heading("Lab_Study_ID_Type")]
        [XmlElement(ElementName = "local_id_1_type")]
        public String LabStudyIDType { get; set; }
        [Heading("Local_Lab_ID")]
        [XmlElement(ElementName = "local_id_2")]
        public String LocalLabID { get; set; }
        [Heading("Local_ID_Type")]
        [XmlElement(ElementName = "local_id_2_type")]
        public String LocalIDType { get; set; }
        [Heading("Local_ID_3")]
        [XmlElement(ElementName = "local_id_3")]
        public String LocalID3 { get; set; }
        [Heading("Local_ID_3_Type")]
        [XmlElement(ElementName = "local_id_3_type")]
        public String LocalID3Type { get; set; }
        [Heading("Specimen_Collection_Other")]
        [XmlElement(ElementName = "other_location")]
        public String SpecimenCollectionSite { get; set; }
        [Heading("Specimen_Collection_Country")]
        [XmlElement(ElementName = "country")]
        public String SpecimenCollectionSiteCountry { get; set; }
        [Heading("Specimen_Collection_State")]
        [XmlElement(ElementName = "state")]
        public String SpecimenCollectionSiteState { get; set; }
        [Heading("Specimen_Collection_County")]
        [XmlElement(ElementName = "county")]
        public String SpecimenCollectionSiteCounty { get; set; }
        [Heading("Patient_Age")]
        [XmlElement(ElementName = "patient_age")]
        public String PatientAge { get; set; }
        [Heading("Patient_Age_Type")]
        [XmlElement(ElementName = "patient_age_units")]
        public String PatientAgeType { get; set; }
        [Heading("Patient_Sex")]
        [XmlElement(ElementName = "patient_gender")]
        public String PatientSex { get; set; }
        [Heading("Patient_Date_of_Birth")]
        [XmlElement(ElementName = "patient_birth_date")]
        public String PatientDateOfBirth { get; set; }
        [Heading("ILI_Symptoms_Date_of_Onset")]
        [XmlElement(ElementName = "date_onset")]
        public String ILISymptionsDateOfOnset { get; set; }
        [Heading("Days_Since_Onset")]
        [XmlElement(ElementName = "days_since_onset")]
        public String DaysSinceOnset { get; set; }
        [Heading("SPECIES")]
        [XmlElement(ElementName = "species")]
        public String Species { get; set; }
        [Heading("Animal Type")]
        [XmlElement(ElementName = "animal_type")]
        public String AnimalType { get; set; }
        [Heading("Category")]
        [XmlElement(ElementName = "animal_category")]
        public String AnimalCategory { get; set; }
        [Heading("FATAL")]
        [XmlElement(ElementName = "fatal")]
        public String Fatal { get; set; }
        [Heading("Travel_Place")]
        [XmlElement(ElementName = "travel_location")]
        public String ForeignTravel { get; set; }
        [Heading("MAIN_CR_EPI.LOCAL_TRAVEL")]
        [XmlIgnore]
        public String LocalTravel { get; set; }
        [Heading("Antiviral_Drugs")]
        [XmlElement(ElementName = "antiviral_drug")]
        public String AntiviralDrugs { get; set; }
        [Heading("Antiviral_Drugs_Date_Initiated")]
        [XmlElement(ElementName = "antiviral_drug_date_start")]
        public String AntiviralDrugsDateInitiated { get; set; }
        [Heading("Antiviral_Drugs_Date_Stopped")]
        [XmlElement(ElementName = "antiviral_drug_date_stop")]
        public String AntiviralDrugsDateStopped { get; set; }
        [Heading("Flu_Vaccination")]
        [XmlElement(ElementName = "immunization_1")]
        public String FluVaccination { get; set; }
        [Heading("Immunization_Date")]
        [XmlElement(ElementName = "immunization_1_date")]
        public String ImmunizationDate { get; set; }
        [Heading("Flu_Vaccination_2")]
        [XmlElement(ElementName = "immunization_2")]
        public String FluVaccination2 { get; set; }
        [Heading("Immunization_Date_2")]
        [XmlElement(ElementName = "immunization_2_date")]
        public String ImmunizationDate2 { get; set; }
        [Heading("FRACTION_TYPE")]
        [XmlIgnore]
        public String FractionType { get; set; }
        [Heading("PARENT_CUID")]
        [XmlIgnore]
        public String ParentAliquot { get; set; }
        [Heading("SAMPLE_VOLUME")]
        [XmlIgnore]
        public String SampleVolume { get; set; }
        [Heading("UNIT_OF_VOLUME")]
        [XmlIgnore]
        public String VolumnUnitOfMeasure { get; set; }
        [Heading("ILI_Symptoms")]
        [XmlElement(ElementName = "ili_symptoms")]
        public String ILISymptions { get; set; }
        [Heading("Travel_Date")]
        [XmlElement(ElementName = "travel_date")]
        public String RecentForeignTravelDate { get; set; }
        [Heading("CR_Question:Tab_14554:Domestic Travel Date")]
        [XmlIgnore]
        public String RecentDomesticTravelDate { get; set; }
        [Heading("Flu_Substudy")]
        [XmlElement(ElementName = "substudy")]
        public String FluSubstudy { get; set; }
        [Heading("Confirmed_Case")]
        [XmlElement(ElementName = "confirmed_case")]
        public String ConfirmedCase { get; set; }
        [Heading("Case_Contact")]
        [XmlElement(ElementName = "case_contact")]
        public String CaseContact { get; set; }
        [Heading("Case_Contact_Type")]
        [XmlElement(ElementName = "case_contact_type")]
        public String CaseContactType { get; set; }
        [Heading("Household_ID")]
        [XmlElement(ElementName = "household_id")]
        public String HouseholdID { get; set; }
        [Heading("Study Specific #1")]
        [XmlElement(ElementName = "flight_number")]
        public String FlightNumber { get; set; }
        [Heading("Study Specific #2")]
        [XmlElement(ElementName = "cabin_number")]
        public String CabinNumber { get; set; }
        [Heading("Study Specific #3")]
        [XmlElement(ElementName = "seat_number")]
        public String SeatNumber { get; set; }
        [Heading("Age Cateogry")]
        [XmlElement(ElementName = "age_category")]
        public String AgeCategory { get; set; }
        [Heading("Age Category Type")]
        [XmlElement(ElementName = "age_category_type")]
        public String AgeCategoryType { get; set; }
        [Heading("SAMP_LOC_BLDG")]
        [XmlIgnore]
        public String SampleLocBldg { get; set; }
        [Heading("SAMP_LOC_ROOM")]
        [XmlIgnore]
        public String SampleLocRoom { get; set; }
        [Heading("SAMP_LOC_FRZR")]
        [XmlIgnore]
        public String SampleLocFrzr { get; set; }
        [Heading("SAMP_LOC_BOX")]
        [XmlIgnore]
        public String SampleLocBox { get; set; }
        [Heading("SAMP_LOC_ABS_POSITION")]
        [XmlIgnore]
        public String SampleLocAbsPosition { get; set; }
        [Heading("SAMP_LOC_REASON")]
        [XmlIgnore]
        public String SampleLocReason { get; set; }
        [Heading("SAMP_LOC_LOCATIONID")]
        [XmlIgnore]
        public String SampleLocLocationId { get; set; }
        [Heading("Study_Name")]
        [XmlElement(ElementName = "study")]
        public String StudyName { get; set; }
        [XmlArray("subsamples")]
        public List<IPBSubsampleDTO> Subsamples { get; set; }
   }
        

}