﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace IPBUploadSPA.DTO
{

    public class WHOSpecimenDTO_Step2
    {
        public WHOSpecimenDTO_Step2()
        {
            Aliquots = new List<WHOAliquotDTO>();
        }
        [XmlIgnore]
        public String CSID { get; set; }
        [XmlIgnore]
        public String CUID { get; set; }
        public virtual ICollection<WHOAliquotDTO> Aliquots { get; set; }
        [XmlIgnore]
        public String Ordinal { get; set; }
        [XmlIgnore]
        public bool Validated { get; set; }
        [XmlIgnore]
        public String ValidationErrors { get; set; }
    }
}