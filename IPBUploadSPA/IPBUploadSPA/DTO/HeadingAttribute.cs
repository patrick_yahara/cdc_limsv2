﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPBUploadSPA.DTO
{
    [AttributeUsage(AttributeTargets.Property)]
    public class HeadingAttribute : System.Attribute
    {
        private string _columnHeaderText;
        public string ColumnHeaderText
        {
            get { return _columnHeaderText; }
            set { _columnHeaderText = value; }
        }

        public HeadingAttribute(string value)
        {
            ColumnHeaderText = value;
        }
    }
}