﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace IPBUploadSPA.Utility
{
    public class DynamicEqualityComparer<T> : IEqualityComparer<T>
    {
        private readonly Type _type;
        private string[] _groupby;
        private List<String> _groupbylist;
        public DynamicEqualityComparer(string[] groupbycolumns)
        {
            _type = _type = typeof(T);;
            _groupby = groupbycolumns;
            _groupbylist = _groupby.ToList<String>();
        }

        public bool Equals(T x, T y)
        {
 	        var result = true;
            var props = from p in _type.GetProperties().Where(s => !_groupbylist.Contains(s.Name) && s.GetType().IsValueType) select p;
            foreach (var p in props)
            {
                if (p.GetValue(x).Equals(p.GetValue(y)))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        public int GetHashCode(T obj)
        {
            var sb = new StringBuilder();
 	        var props = from p in _type.GetProperties().Where(s => !_groupbylist.Contains(s.Name) && s.GetType().IsValueType) select p;
            foreach (var p in props)
            {
                sb.Append(p.GetValue(obj).ToString());
            }       
            return sb.ToString().GetHashCode();
        }
    }
}