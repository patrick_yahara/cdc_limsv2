﻿using IPBUploadSPA.DTO;
using IPBUploadSPA.MirthService;
using IPBUploadSPA.Models;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using OfficeOpenXml;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Serialization;
using IPBUploadSPA.StarLIMSService;
using IPBUploadSPA.StarLIMSServiceWHO;

namespace IPBUploadSPA.Utility
{
    public class QueueProcessor
    {
        private class FileItem
        {
            public int Site { get; set; }
            public string File { get; set; }
        }

        private readonly ConcurrentQueue<FileItem> _filestoadd = new ConcurrentQueue<FileItem>();
        private ConcurrentQueue<long> _requeststoprocess = new ConcurrentQueue<long>();
        private readonly int _headerRowOffset;
        private readonly int _dataRowOffset;
        private readonly int _dataRowOffset_IPB;
        private volatile bool _cancelled = false;
        private volatile bool _cancelledFile = false;
        private volatile bool _badconnection = false;
        public volatile bool IsStarted = false;
        public volatile bool IsStartedFile = false;
        private volatile int _currentProcessingId;

        private readonly int _timeoutCall = 20;
        private readonly int _timeoutCallAliquot = 20;
        private readonly int _callRetries = 2;
        private readonly int _callRetryDelay = 5;
        private readonly int _maxConnectionRetries = 2;
        private readonly int _dataStoreCleanupHour = 2;
        private readonly int _dataStoreRequestAge = 4;

        private readonly string _user = "SYSADM";
        private readonly string _password = "LIMS";

        //private DefaultAcceptMessageClient prxy;
        private StarLIMSService.WS_FastAccession_wsSoapClient prxy;
        private StarLIMSServiceWHO.WS_FastAccession_wsSoapClient prxyWHO;
        private StarLIMSServiceZVT.WS_FastAccession_wsSoapClient prxyZVT;
        private StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoapClient prxyFLAPWHO;

        private FixedInterval retryStrategy;
        private RetryPolicy<CustomTransientErrorDetectionStrategy> retryPolicy;

        private readonly Stopwatch _stopwatch = new Stopwatch();
        private int _attempts;

        public static bool HasInstance()
        {
            return _instance != null;
        }

        public static void Invalidate()
        {
            _instance = null;
        }

        public static bool HasCancelledInstance()
        {
            var result = false;
            if (_instance != null)
            {
                if (_instance._cancelled)
                {
                    result = true;
                }
                if (_instance._cancelledFile)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool HasBadConnection()
        {
            var result = false;
            if (_instance != null)
            {
                if (_instance._badconnection)
                {
                    result = true;
                }
            }
            return result;
        }


        private static QueueProcessor _instance;
        private QueueProcessor()
        {
            int temp = 0;
            var htemp = WebConfigurationManager.AppSettings["templateHeaderRowOffset"];
            if (int.TryParse(htemp, out temp))
            {
                _headerRowOffset = temp;
            }
            else
            {
                _headerRowOffset = 0;
            }
            temp = 0;
            var dsctemp = WebConfigurationManager.AppSettings["dataStoreCleanupHour"];
            if (int.TryParse(dsctemp, out temp))
            {
                _dataStoreCleanupHour = temp;
            }
            else
            {
                _dataStoreCleanupHour = 2;
            }
            temp = 0;
            var dsratemp = WebConfigurationManager.AppSettings["dataStoreRequestAge"];
            if (int.TryParse(dsratemp, out temp))
            {
                _dataStoreCleanupHour = temp;
            }
            else
            {
                _dataStoreCleanupHour = 2;
            }
            temp = 0;
            var dtemp = WebConfigurationManager.AppSettings["templateDataRowOffset"];
            if (int.TryParse(dtemp, out temp))
            {
                _dataRowOffset = temp;
            }
            else
            {
                _dataRowOffset = 5;
            }
            temp = 0;
            var dtemp_IPB = WebConfigurationManager.AppSettings["templateDataRowOffsetIPB"];
            if (int.TryParse(dtemp_IPB, out temp))
            {
                _dataRowOffset_IPB= temp;
            }
            else
            {
                _dataRowOffset_IPB = 2;
            }

            // * the amount of time the call will wait for a response before timing out, when sending a sample to import
            var stemp = WebConfigurationManager.AppSettings["sampleOperationTimeout"];
            if (int.TryParse(stemp, out temp))
            {
                _timeoutCall = temp;
            }
            else
            {
                _timeoutCall = 20;
            }
            temp = 0;
            // * the amount of time the call will wait for a response before timing out, when sending a set of aliquots to import
            stemp = WebConfigurationManager.AppSettings["aliquotOperationTimeout"];
            if (int.TryParse(stemp, out temp))
            {
                _timeoutCallAliquot = temp;
            }
            else
            {
                _timeoutCallAliquot = 20;
            }
            temp = 0;
            // * the number of times the system will make a call (without a response) before creating a new connection
            stemp = WebConfigurationManager.AppSettings["callRetries"];
            if (int.TryParse(stemp, out temp))
            {
                _callRetries = temp;
            }
            else
            {
                _callRetries = 2;
            }
            temp = 0;
            // * the number of seconds the system waits before re-trying a call on the same connection
            stemp = WebConfigurationManager.AppSettings["callRetryDelay"];
            if (int.TryParse(stemp, out temp))
            {
                _callRetryDelay = temp;
            }
            else
            {
                _callRetryDelay = 5;
            }
            temp = 0;
            // * total number of connections (including initial) system will try to create if there are communication issues
            stemp = WebConfigurationManager.AppSettings["maxConnectionRetries"];
            if (int.TryParse(stemp, out temp))
            {
                _maxConnectionRetries = temp;
            }
            else
            {
                _maxConnectionRetries = 5;
            }
            temp = 0;

            retryStrategy = new FixedInterval(_callRetries, TimeSpan.FromSeconds(_callRetryDelay));
            retryPolicy =
              new RetryPolicy<CustomTransientErrorDetectionStrategy>(retryStrategy);
            retryPolicy.Retrying += retryPolicy_Retrying;


        }

        void retryPolicy_Retrying(object sender, RetryingEventArgs e)
        {
            _attempts = e.CurrentRetryCount + 1;
        }

        public static QueueProcessor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QueueProcessor();
                }
                return _instance;
            }
        }

        public void AddFileToQueue(string filepath, int site)
        {
            var item = new FileItem() { File = filepath, Site = site};
            _filestoadd.Enqueue(item);
        }

        public void AddProcessRequestToQueue(long id)
        {
            if (_currentProcessingId != (int)id && !_requeststoprocess.Contains(id))
            {
                _requeststoprocess.Enqueue(id);
            }
        }

        public void RemoveProcessRequestFromQueue(long id)
        {
            if (_currentProcessingId != (int)id && _requeststoprocess.Contains(id))
            {
                var temp = _requeststoprocess.ToList();
                temp.Remove(id);
                _requeststoprocess = new ConcurrentQueue<long>(temp);
            }
        }

        private bool AddFileToDataStore(IPBUploadEntities context, FileItem localfilepath, bool removefile = true)
        {
            const string delimiter = @"|";
            var result = true;
            ExcelPackage package = null;
            XmlWriterSettings settings = null;
            settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };
            if (!File.Exists(localfilepath.File))
            {
                result = false;
                return result;
            }
            var localfileinfo = new FileInfo(localfilepath.File);
            try
            {
                    // Get the name of the file to upload.
                    var fileName = localfileinfo.Name;
                    // Get the extension of the uploaded file.
                    var extension = localfileinfo.Extension;
                    var upload = new UploadRequest { filename = fileName, daterequested = DateTime.UtcNow.ToString(), site = localfilepath.Site };
                    if (extension != ".xlsx")
                    {
                        // currently only support opendoc type documents
                        // this is xlsx, will through an exception if format does not match
                        // check the file extension first
                        upload.errored = 1;
                        upload.error = @"Only xlsx files are supported.";
                        result = false;
                    }
                    else
                    {
                        try
                        {
                            // see if this file is supported by our Excel lib
                            package = new ExcelPackage(localfileinfo);
                        }
                        catch (Exception)
                        {
                            upload.errored = 1;
                            upload.error = @"Unable to interpret Excel file.";
                            result = false;
                        }
                    }
                    context.UploadRequests.Add(upload);
                    context.SaveChanges();

                    var newid = upload.id;
                    #region Influenza Site
                    
                    var sheet = package.Workbook.Worksheets[1];
                    var headerrange = sheet.Cells[sheet.Dimension.Start.Row + _headerRowOffset, sheet.Dimension.Start.Column, sheet.Dimension.Start.Row + _headerRowOffset, sheet.Dimension.End.Column];

                    if (result && (localfilepath.Site == 1 || localfilepath.Site == 2 || localfilepath.Site == 3))
                    {
                        List<WHOSpecimenDTO> samples = new List<WHOSpecimenDTO>();
                        List<WHOAliquotDTO> aliquots = new List<WHOAliquotDTO>();
                        List<WHOSpecimenDTO_Step2> aliquotsToSerialize = new List<WHOSpecimenDTO_Step2>();
                        ExcelRowTransformer<WHOSpecimenDTO> factoryS = null;
                        ExcelRowTransformer<WHOAliquotDTO> factoryA = null;
                        factoryS = new ExcelRowTransformer<WHOSpecimenDTO>(headerrange, true);
                        if (factoryS.HasIPBField())
                        {
                            upload.errored = 1;
                            upload.error = @"IPB template field present";
                            upload.validated = 0;
                            upload.preprogress = 0;
                            result = false;
                            context.SaveChanges();
                            return result;
                        }
                        if (factoryS.HasStep2Field())
                        {
                            upload.step = 2;
                            context.SaveChanges();
                            factoryA = new ExcelRowTransformer<WHOAliquotDTO>(headerrange, true);
                            aliquots = factoryA.GetRows(sheet.Cells[
                                sheet.Dimension.Start.Row + _dataRowOffset, sheet.Dimension.Start.Column,
                                sheet.Dimension.End.Row, sheet.Dimension.End.Column]).ToList();
                            if (result && aliquots.Count <= 0)
                            {
                                upload.errored = 1;
                                upload.error = @"No records provided in Excel file.";
                                upload.validated = 0;
                                upload.preprogress = 0;
                                result = false;
                                context.SaveChanges();
                                return result;
                            }
                        }
                        else
                        {
                            upload.step = 1;
                            context.SaveChanges();
                            samples = factoryS.GetRows(sheet.Cells[
                                sheet.Dimension.Start.Row + _dataRowOffset, sheet.Dimension.Start.Column,
                                sheet.Dimension.End.Row, sheet.Dimension.End.Column]).ToList();

                            if (samples.All(s => String.IsNullOrEmpty(s.CSID)))
                            {
                                upload.errored = 1;
                                upload.error = @"No CSIDs provided in Excel file.";
                                upload.validated = 0;
                                upload.preprogress = 0;
                                result = false;
                                context.SaveChanges();
                                return result;
                            }
                            if (result && samples.Count <= 0)
                            {
                                upload.errored = 1;
                                upload.error = @"No records provided in Excel file.";
                                upload.validated = 0;
                                upload.preprogress = 0;
                                result = false;
                                context.SaveChanges();
                                return result;
                            }
                        }

                        if (upload.step.Equals(1))
                        {
                            samples.RemoveAll(s => string.IsNullOrEmpty(s.CSID));
                            for (int i = 0; i < samples.Count; i++)
                            {
                                var sample = samples[i];
                                var errors = new List<String>();
                                var finished = false;
                                if (!IPBUploadDTOValidator.IsValid(ref sample, out finished, out errors))
                                {
                                    if (finished)
                                    {
                                        // is this needed with sqlite version?
                                        samples.RemoveRange(i, samples.Count - i);
                                        break;
                                    }
                                    sample.ValidationErrors = String.Join(delimiter, errors);
                                    upload.errored = 1;
                                    upload.validated = 0;
                                    // * 767
                                    var detail = new UploadRequestDetail()
                                    {
                                        uploadrequestid = newid,
                                        ordinal = sample.Ordinal,
                                        csid = sample.CSID,
                                        payload = string.Empty,
                                        errored = 1,
                                        display = sample.ValidationErrors,
                                    };
                                    context.UploadRequestDetails.Add(detail);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    sample.Validated = true;
                                }
                            }
                            // if all valid, rollup
                            if (!samples.Any(s => !s.Validated))
                            {
                                var isvalid = true;
                                samples = SampleProcessor.ProcessSamples(samples, out isvalid);
                                if (!isvalid)
                                {
                                    upload.validated = 0;
                                    upload.errored = 1;
                                    upload.error = @"Error performing sample/aliquot rollup, data is not uniform";
                                    context.SaveChanges();
                                    return false;
                                }
                                else
                                {
                                    upload.validated = 1;
                                    context.SaveChanges();
                                };
                            }
                            else
                            {
                                upload.preprogress = 100;
                                upload.errored = 1;
                                upload.error = @"All rows are not valid";
                                context.SaveChanges();
                                return false;
                            };
                            // serialize and save
                            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<WHOSpecimenDTO>), new XmlRootAttribute("samples"));
                            var itemcount = samples.Count();
                            var current = 0;
                            var currentPct = upload.preprogress;
                            foreach (var sample in samples)
                            {
                                using (var textWriter = new StringWriter())
                                {
                                    using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                                    {
                                        try
                                        {
                                            serializer.Serialize(xmlWriter, new List<WHOSpecimenDTO>() { sample });
                                            var sampleText = textWriter.ToString();
                                            var tempcuid = sample.CUID;
                                            if (upload.validated.Equals(1))
                                            {
                                                tempcuid = sample.Isolates.FirstOrDefault().Aliquots.Count > 1 ? @"Multiple"
                                                   : sample.Isolates.FirstOrDefault().Aliquots.FirstOrDefault() != null
                                                   ? sample.Isolates.FirstOrDefault().Aliquots.FirstOrDefault().CUID : null;
                                            }
                                            var detail = new UploadRequestDetail()
                                            {
                                                uploadrequestid = newid,
                                                ordinal = sample.Ordinal,
                                                payload = sampleText,
                                                csid = sample.CSID,
                                                cuid = tempcuid,
                                                errored = sample.Validated ? 0 : 1,
                                                display = sample.Validated ? String.Empty : sample.ValidationErrors,
                                                response = sample.Validated ? String.Empty : sample.ValidationErrors,
                                            };
                                            context.UploadRequestDetails.Add(detail);
                                        }
                                        catch (Exception ex)
                                        {
                                            upload.errored = 1;
                                            upload.error = @"Error with serializing object.";
                                            result = false;
                                            upload.preprogress = 100;
                                            context.SaveChanges();
                                            break;
                                        }
                                        current++;
                                        currentPct = (long)Math.Round((decimal)((current * 1.0 / itemcount * 1.0) * 100.0));
                                        if (currentPct != upload.preprogress)
                                        {
                                            upload.preprogress = currentPct;
                                        }
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }

                        if (upload.step.Equals(2))
                        {
                            aliquots.RemoveAll(s => string.IsNullOrEmpty(s.ParentCUID));
                            for (int i = 0; i < aliquots.Count; i++)
                            {
                                var aliquot = aliquots[i];
                                var errors = new List<String>();
                                var finished = false;
                                if (!IPBUploadDTOValidator.IsValidAliquotOnly(ref aliquot, out finished, out errors))
                                {
                                    if (finished)
                                    {
                                        // is this needed with sqlite version?
                                        aliquots.RemoveRange(i, aliquots.Count - i);
                                        break;
                                    }
                                    aliquot.ValidationErrors = String.Join(delimiter, errors);
                                    upload.errored = 1;
                                    upload.validated = 0;
                                }
                                else
                                {
                                    aliquot.Validated = true;
                                }
                            }
                            // if all valid, rollup
                            if (!aliquots.Any(s => !s.Validated))
                            {
                                var isvalid = true;
                                aliquotsToSerialize = SampleProcessor.ProcessSamples(aliquots, out isvalid);
                                if (!isvalid)
                                {
                                    upload.validated = 0;
                                    upload.errored = 1;
                                    upload.error = @"Error performing sample/aliquot rollup, data is not uniform";
                                    context.SaveChanges();
                                    return false;
                                }
                                else
                                {
                                    upload.validated = 1;
                                    context.SaveChanges();
                                };
                            }
                            else
                            {
                                upload.errored = 1;
                                upload.error = @"All rows are not valid";
                                context.SaveChanges();
                                return false;
                            };
                            // serialize and save
                            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<WHOAliquotDTO>), new XmlRootAttribute("aliquots"));
                            var itemcount = aliquotsToSerialize.Count();
                            var current = 0;
                            var currentPct = upload.preprogress;
                            foreach (var specimen_Step2 in aliquotsToSerialize)
                            {
                                using (var textWriter = new StringWriter())
                                {
                                    using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                                    {
                                        try
                                        {
                                            serializer.Serialize(xmlWriter, specimen_Step2.Aliquots.ToList());
                                            var sampleText = textWriter.ToString();
                                            var tempcuid = specimen_Step2.CUID;
                                            if (upload.validated.Equals(1))
                                            {
                                                tempcuid = specimen_Step2.Aliquots.Count > 1 ? @"Multiple"
                                                   : specimen_Step2.Aliquots.FirstOrDefault() != null
                                                   ? specimen_Step2.Aliquots.FirstOrDefault().CUID : null;
                                            }
                                            var detail = new UploadRequestDetail()
                                            {
                                                uploadrequestid = newid,
                                                ordinal = specimen_Step2.Ordinal,
                                                payload = sampleText,
                                                csid = specimen_Step2.CSID,
                                                cuid = tempcuid,
                                                errored = specimen_Step2.Validated ? 0 : 1,
                                                display = specimen_Step2.Validated ? String.Empty : specimen_Step2.ValidationErrors,
                                                response = specimen_Step2.Validated ? String.Empty : specimen_Step2.ValidationErrors,
                                            };
                                            context.UploadRequestDetails.Add(detail);
                                        }
                                        catch (Exception ex)
                                        {
                                            upload.errored = 1;
                                            upload.error = @"Error with serializing object.";
                                            result = false;
                                            upload.preprogress = 100;
                                            context.SaveChanges();
                                            break;
                                        }
                                        current++;
                                        currentPct = (long)Math.Round((decimal)((current * 1.0 / itemcount * 1.0) * 100.0));
                                        if (currentPct != upload.preprogress)
                                        {
                                            upload.preprogress = currentPct;
                                        }
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }
                        if (upload.errored == 0)
                        {
                            upload.serialized = 1;
                            upload.validated = 1;
                        }
                        upload.preprogress = 100;
                        context.SaveChanges();
                    }
                    #endregion
                    #region IPB Site
                    if (result && localfilepath.Site == 0)
                    {
                        List<IPBUploadDTO> samples = new List<IPBUploadDTO>();
                        try
                        {
                            var factory = new ExcelRowTransformer<IPBUploadDTO>(headerrange, true);

                            if (!factory.HasIPBField())
                            {
                                upload.errored = 1;
                                upload.error = @"Required IPB template field not present";
                                upload.validated = 0;
                                upload.preprogress = 0;
                                result = false;
                                context.SaveChanges();
                            } else
                            { 
                                samples = factory.GetRows(sheet.Cells[
                                    sheet.Dimension.Start.Row + _dataRowOffset_IPB, sheet.Dimension.Start.Column,
                                    sheet.Dimension.End.Row, sheet.Dimension.End.Column]).ToList();
                            }
                        }
                        catch (Exception)
                        {
                            upload.errored = 1;
                            upload.error = @"Failure parsing Excel file.";
                            result = false;
                        }

                        if (result && samples.All(s => String.IsNullOrEmpty(s.CSID)))
                        {
                            upload.errored = 1;
                            upload.error = @"No CSIDs provided in Excel file.";
                            result = false;
                        }

                        if (result && samples.Count <= 0)
                        {
                            upload.errored = 1;
                            upload.error = @"No records provided in Excel file.";
                            result = false;
                        }

                        // 1 Step 1 or 2
                        // * first or second import?
                        if (result)
                        {
                            if (samples.Any(s => !string.IsNullOrEmpty(s.ParentAliquot)))
                            {
                                upload.step = 2;
                            }
                            else
                            {
                                upload.step = 1;
                            }                            
                        }

                        context.SaveChanges();

                        // 2 validate
                        if (result)
                        {
                            if (upload.step == 2)
                            {
                                samples.RemoveAll(s => string.IsNullOrEmpty(s.ParentAliquot));
                                for (int i = 0; i < samples.Count; i++)
                                {
                                    var sample = samples[i];
                                    var errors = new List<String>();
                                    var finished = false;
                                    if (!IPBUploadDTOValidator.IsValidAliquotOnly(ref sample, out finished, out errors))
                                    {
                                        if (finished)
                                        {
                                            // is this needed with sqlite version?
                                            samples.RemoveRange(i, samples.Count - i);
                                            break;
                                        }
                                        sample.ValidationErrors = String.Join(delimiter, errors);
                                        upload.errored = 1;
                                        upload.validated = 0;
                                    }
                                    else
                                    {
                                        sample.Validated = true;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < samples.Count; i++)
                                {
                                    var sample = samples[i];
                                    var errors = new List<String>();
                                    var finished = false;
                                    if (!IPBUploadDTOValidator.IsValid(ref sample, out finished, out errors))
                                    {
                                        if (finished)
                                        {
                                            samples.RemoveRange(i, samples.Count - i);
                                            break;
                                        }
                                        sample.ValidationErrors = String.Join(delimiter, errors);
                                        upload.errored = 1;
                                        upload.validated = 0;
                                    }
                                    else
                                    {
                                        sample.Validated = true;
                                    }
                                }

                            }
                        }
                        if (result)
                        {
                            // if all valid, rollup
                            if (!samples.Any(s => !s.Validated))
                            {
                                var isvalid = true;
                                samples = SampleProcessor.ProcessSamples(samples, out isvalid);
                                if (!isvalid)
                                {
                                    upload.validated = 0;
                                    upload.errored = 1;
                                    upload.error = @"Error performing sample/aliquot rollup, data is not uniform";
                                    context.SaveChanges();
                                }
                                else
                                {
                                    upload.validated = 1;
                                    context.SaveChanges();
                                };
                            }
                            else
                            {
                                upload.errored = 1;
                                upload.error = @"All rows are not valid";
                                context.SaveChanges();
                            };
                        }
                        if (result)
                        {
                            // 4 try to serialize
                            if (upload.step == 2)
                            {
                                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<IPBAliquotDTO>), new XmlRootAttribute("aliquots"));
                                var itemcount = samples.Count();
                                var current = 0;
                                var currentPct = upload.preprogress;
                                foreach (var sample in samples)
                                {
                                    using (var textWriter = new StringWriter())
                                    {
                                        using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                                        {
                                            try
                                            {
                                                serializer.Serialize(xmlWriter, sample.Subsamples.FirstOrDefault().Aliquots.ToList());
                                                var sampleText = textWriter.ToString();
                                                var tempcuid = sample.CUID;
                                                if (upload.validated.Equals(1))
                                                {
                                                    tempcuid = sample.Subsamples.FirstOrDefault().Aliquots.Count > 1 ? @"Multiple"
                                                       : sample.Subsamples.FirstOrDefault().Aliquots.FirstOrDefault() != null
                                                       ? sample.Subsamples.FirstOrDefault().Aliquots.FirstOrDefault().CUID : null;
                                                }
                                                var detail = new UploadRequestDetail()
                                                {
                                                    uploadrequestid = newid,
                                                    ordinal = sample.Ordinal,
                                                    payload = sampleText,
                                                    csid = sample.CSID,
                                                    cuid = tempcuid,
                                                    errored = sample.Validated ? 0 : 1,
                                                    display = sample.Validated ? String.Empty : sample.ValidationErrors,
                                                    response = sample.Validated ? String.Empty : sample.ValidationErrors,
                                                };
                                                context.UploadRequestDetails.Add(detail);
                                            }
                                            catch (Exception ex)
                                            {
                                                upload.errored = 1;
                                                upload.error = @"Error with serializing object.";
                                                result = false;
                                                upload.preprogress = 100;
                                                context.SaveChanges();
                                                break;
                                            }
                                            current++;
                                            currentPct = (long)Math.Round((decimal)((current * 1.0 / itemcount * 1.0) * 100.0));
                                            if (currentPct != upload.preprogress)
                                            {
                                                upload.preprogress = currentPct;
                                            }
                                            context.SaveChanges();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //*var serializer = new System.Xml.Serialization.XmlSerializer(typeof(IPBUploadDTO));
                                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<IPBUploadDTO>), new XmlRootAttribute("samples"));
                                var itemcount = samples.Count();
                                var current = 0;
                                var currentPct = upload.preprogress;
                                foreach (var sample in samples)
                                {
                                    using (var textWriter = new StringWriter())
                                    {
                                        using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                                        {
                                            try
                                            {
                                                serializer.Serialize(xmlWriter, new List<IPBUploadDTO>() { sample });
                                                var sampleText = textWriter.ToString();
                                                var tempcuid = sample.CUID;
                                                if (upload.validated.Equals(1))
                                                {
                                                    tempcuid = sample.Subsamples.FirstOrDefault().Aliquots.Count > 1 ? @"Multiple"
                                                       : sample.Subsamples.FirstOrDefault().Aliquots.FirstOrDefault() != null
                                                       ? sample.Subsamples.FirstOrDefault().Aliquots.FirstOrDefault().CUID : null;
                                                }
                                                var detail = new UploadRequestDetail()
                                                {
                                                    uploadrequestid = newid,
                                                    ordinal = sample.Ordinal,
                                                    payload = sampleText,
                                                    csid = sample.CSID,
                                                    cuid = tempcuid,
                                                    errored = sample.Validated ? 0 : 1,
                                                    display = sample.Validated ? String.Empty : sample.ValidationErrors,
                                                    response = sample.Validated ? String.Empty : sample.ValidationErrors,
                                                };
                                                context.UploadRequestDetails.Add(detail);
                                            }
                                            catch (Exception ex)
                                            {
                                                upload.errored = 1;
                                                upload.error = @"Error with serializing object.";
                                                result = false;
                                                upload.preprogress = 100;
                                                context.SaveChanges();
                                                break;
                                            }
                                            current++;
                                            currentPct = (long)Math.Round((decimal)((current * 1.0 / itemcount * 1.0) * 100.0));
                                            if (currentPct != upload.preprogress)
                                            {
                                                upload.preprogress = currentPct;
                                            }
                                            context.SaveChanges();

                                        }
                                    }
                                }
                            }
                        }
                        if (upload.errored == 0)
                        {
                            upload.serialized = 1;
                            upload.validated = 1;
                        }
                        upload.preprogress = 100;
                        context.SaveChanges();
                    }
                    #endregion
            }
            catch (Exception ex)
            {
                result = (ex == null);
            }
            finally
            {
                // xml before text, as xml references text
                //if (xmlWriter != null) { xmlWriter.Dispose(); };
                //if (textWriter != null) { textWriter.Dispose(); };
                if (package != null) { package.Dispose(); };
                if (removefile)
                {
                    var parentDir = localfileinfo.Directory;
                    localfileinfo.Delete();
                    parentDir.Delete();
                }
            }
            return result;
        }

        private void ProcessSamples(IPBUploadEntities context, long id)
        {
           {
                var request = context.UploadRequests.Where(
                    s => s.id.Equals(id) && s.completed.Equals(0) && s.errored.Equals(0) && s.validated.Equals(1) && s.serialized.Equals(1)
                    ).OrderBy(s => s.id).FirstOrDefault();
                if (request != null && request.id > 0)
                {
                    _badconnection = false;
                    // * prxy = new MirthService.DefaultAcceptMessageClient();
                    prxy = new StarLIMSService.WS_FastAccession_wsSoapClient();
                    prxyWHO = new StarLIMSServiceWHO.WS_FastAccession_wsSoapClient();
                    prxyZVT = new StarLIMSServiceZVT.WS_FastAccession_wsSoapClient();
                    prxyFLAPWHO = new StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoapClient();
                    if (request.step == 1)
                    {
                        ((IContextChannel)prxy.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                        ((IContextChannel)prxyWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                        ((IContextChannel)prxyZVT.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                        ((IContextChannel)prxyFLAPWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                    }
                    else
                    {
                        ((IContextChannel)prxy.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                        ((IContextChannel)prxyWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                        ((IContextChannel)prxyZVT.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                        ((IContextChannel)prxyFLAPWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                    }

                    var connections = 1;
                    var items = context.UploadRequestDetails.Where(s => s.uploadrequestid.Equals(request.id) && s.attempts.Equals(0)).OrderBy(s => s.ordinal).ToList();
                    var current = 0;
                    var itemcount = items.Count;
                    var currentPct = request.progress;
                    var itemerror = false;
                    foreach (var item in items)
                    {
                        try
                        {
                            StarLIMSService.ReturnDTO[] responseDTOArray = null;
                            StarLIMSServiceWHO.ReturnDTO[] responseDTOArrayWHO = null;
                            StarLIMSServiceZVT.ReturnDTO[] responseDTOArrayZVT = null;
                            StarLIMSServiceFLAPWHO.ReturnDTO responseDTOFLAPWHO = null;
                            _stopwatch.Restart();
                            _attempts = 1;
                            // * var responseText = retryPolicy.ExecuteAction<String>(() => prxy.acceptMessage(item.payload));
                            if (request.site.Equals(0))
                            {
                                responseDTOArray = retryPolicy.ExecuteAction<StarLIMSService.ReturnDTO[]>(() => prxy.FastLogin_sessionless(item.payload, false, _user, _password));
                            }
                            else if (request.site.Equals(1))
                            {
                                responseDTOArrayWHO = retryPolicy.ExecuteAction<StarLIMSServiceWHO.ReturnDTO[]>(() => prxyWHO.FastLogin_sessionless(item.payload, false, _user, _password));
                            }
                            else if (request.site.Equals(2)) 
                            { 
                                responseDTOArrayZVT = retryPolicy.ExecuteAction<StarLIMSServiceZVT.ReturnDTO[]>(() => prxyZVT.FastLogin_sessionless(item.payload, false, _user, _password));
                            }
                            else
                            {
                                responseDTOFLAPWHO = retryPolicy.ExecuteAction<StarLIMSServiceFLAPWHO.ReturnDTO>(() => prxyFLAPWHO.FastLoginEx_sessionless(item.payload, false, _user, _password));
                            }
                            current++;
                            currentPct = (long)Math.Round((decimal)(current * 100.0 / itemcount));
                            if (currentPct != request.progress)
                            {
                                request.progress = currentPct;
                            }
                            if (request.site.Equals(0))
                            {
                                item.response = responseDTOArray.GetStringForDB();
                            }
                            else if (request.site.Equals(1))
                            {
                                item.response = responseDTOArrayWHO.GetStringForDB();
                            }
                            else if (request.site.Equals(2))
                            {
                                item.response = responseDTOArrayZVT.GetStringForDB();
                            }
                            else
                            {
                                item.response = responseDTOFLAPWHO.GetStringForDB();
                            }

                            try
                            {
                                FluAccessionIPBReturnDTO parsederror = null;
                                if (request.site.Equals(0))
                                {
                                    parsederror = new FluAccessionIPBReturnDTO((int)request.step, responseDTOArray, item.ordinal);
                                }
                                else if (request.site.Equals(1))
                                {
                                    parsederror = new FluAccessionIPBReturnDTO((int)request.step, responseDTOArrayWHO, item.ordinal);
                                }
                                else if (request.site.Equals(2))
                                {
                                    parsederror = new FluAccessionIPBReturnDTO((int)request.step, responseDTOArrayZVT, item.ordinal);
                                } else
                                {
                                    parsederror = new FluAccessionIPBReturnDTO((int)request.step, responseDTOFLAPWHO, item.ordinal);
                                }
                                if (parsederror.InterpretedResult.HasValue && !parsederror.InterpretedResult.Value)
	                            {
                                    item.errored = 1;
                                    itemerror = true;
	                            }
                                item.display = parsederror.Display;
                            }
                            catch (Exception)
                            {
                                item.errored = 1;
                                item.display = @"Error interpreting returned value/error";
                                itemerror = true;
                            }
                            item.attempts = _attempts;
                            item.durationms = _stopwatch.ElapsedMilliseconds;
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            request.errored = 1;
                            request.error = @"Error communicating to the message server";
                            context.SaveChanges();
                            if (_maxConnectionRetries <= connections++)
                            {
                                // * prxy = new MirthService.DefaultAcceptMessageClient();
                                prxy = new StarLIMSService.WS_FastAccession_wsSoapClient();
                                prxyWHO = new StarLIMSServiceWHO.WS_FastAccession_wsSoapClient();
                                prxyZVT = new StarLIMSServiceZVT.WS_FastAccession_wsSoapClient();
                                prxyFLAPWHO = new StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoapClient();
                                if (request.step == 1)
                                {
                                    ((IContextChannel)prxy.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                                    ((IContextChannel)prxyWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                                    ((IContextChannel)prxyZVT.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                                    ((IContextChannel)prxyFLAPWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCall);
                                }
                                else
                                {
                                    ((IContextChannel)prxy.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                                    ((IContextChannel)prxyWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                                    ((IContextChannel)prxyZVT.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                                    ((IContextChannel)prxyFLAPWHO.InnerChannel).OperationTimeout = new TimeSpan(0, 0, _timeoutCallAliquot);
                                }
                                continue;
                            }
                            else
                            {
                                request.errored = 1;
                                request.error = String.Format(@"Maximum connection retires reached ({0})", _maxConnectionRetries);
                                context.SaveChanges();
                                _badconnection = true;
                                break;
                            }
                        }
                    }
                    if (itemerror)
                    {
                        request.errored = 1;
                        request.error = @"Error with a detail item";
                        context.SaveChanges();
                    }
                    else
                    {
                        request.completed = 1;
                        context.SaveChanges();
                    }
                }
            }
            
        }

        public void StartAddToStore(CancellationToken cancellationToken = default(CancellationToken))
        {
            IsStartedFile = true;
            _cancelledFile = false;
            var context = new IPBUploadEntities();
            //String filepath = null; 
            FileItem filepath = null;
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                // 05/05/2015 458 PDC
                //if (_filestoadd.TryDequeue(out filepath))
                while (_filestoadd.TryDequeue(out filepath))
                {
                    AddFileToDataStore(context, filepath);

                }
                // 05/05/2015 458 PDC
                // do not want long running process; only import if directed by UI
                //else
                //{
                //    Thread.Sleep(5000);
                //}
            }
            catch (DbUpdateConcurrencyException)
            {
                // this will throw if our rowcount is 0, which I just want to ignore   
            }
            catch (Exception ex)
            {
                ProcessFileCancellation(context, filepath.File);
            }
            finally
            {
                IsStartedFile = false;
                context.Dispose();
                context = null;
            }

            // 05/05/2015 458 PDC
            // do not want long running process; only import if directed by UI
            //HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartAddToStore(ct));

        }

        private void ProcessFileCancellation(IPBUploadEntities context, string filepath)
        {
            context.SaveChanges();
            _cancelledFile = true;
            if (!String.IsNullOrEmpty(filepath) && File.Exists(filepath))
            {
                try
                {
                    File.Delete(filepath);
                }
                catch (Exception)
                {
                }
            }
        }
        
        public void StartProcessing(CancellationToken cancellationToken = default(CancellationToken))
        {
            IsStarted = true;
            _cancelled = false;
            var context = new IPBUploadEntities();
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                // 05/05/2015 458 PDC
                // Seperating actions
                //String filepath;
                //if (_filestoadd.TryDequeue(out filepath))
                //{
                //    AddFileToDataStore(context, filepath);
                //}
                // 05/05/2015 458 PDC
                long tempId;
                while (_requeststoprocess.TryDequeue(out tempId))
                {
                    _currentProcessingId = (int)tempId;
                    ProcessSamples(context, tempId);
                }
                
            }
            catch (DbUpdateConcurrencyException)
            {
                // this will throw if our rowcount is 0, which I just want to ignore   
            }
            catch (Exception ex)
            {
                ProcessCancellation(context);
            } 
            finally
            {
                IsStarted = false;
                context.Dispose();
                context = null;
                _currentProcessingId = 0;
            }

            // 05/05/2015 458 PDC
            //String fn = null;
            //if (!_filestoadd.TryPeek(out fn) && DateTime.Now.Hour.Equals(_dataStoreCleanupHour))
            //{
            //    HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.CleanupDataStore(ct));
            //} else if (!_badconnection)
            //{
            //    HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartProcessing(ct));
            //}
            
        }

        public void CleanupDataStore(CancellationToken cancellationToken)
        {
            var context = new IPBUploadEntities();
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                var tempsql = "DELETE FROM UploadRequest WHERE daterequested <= date('now','-@offset day')";
                context.Database.ExecuteSqlCommand(tempsql, new SQLiteParameter("offset", _dataStoreRequestAge));
                tempsql = @"DELETE FROM UploadRequestDetail WHERE uploadrequestid IN (SELECT uploadrequestid FROM UploadRequestDetail 
                                                                                        LEFT OUTER JOIN UploadRequest ON UploadRequest.id = UploadRequestDetail.uploadrequestid
                                                                                        WHERE UploadRequest.id IS NULL)";
                context.Database.ExecuteSqlCommand(tempsql);

                // this will kill any transactions
                context.Database.Connection.Close();
                context.Database.Connection.Open();
                using (var command = context.Database.Connection.CreateCommand())
                {
                    command.CommandText = "VACUUM;";
                    command.ExecuteNonQuery();
                }
                context.Database.Connection.Close();
            }
            catch (DbUpdateConcurrencyException)
            {
                // this does not mean the service should be shut down
            }
            catch (Exception ex)
            {
            }
            finally
            {
                context.Dispose();
                context = null;
            }
        }

        private void ProcessCancellation(IPBUploadEntities context)
        {
            context.SaveChanges();
            _cancelled = true;
        }


    }
}