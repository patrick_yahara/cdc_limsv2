﻿using IPBUploadSPA.DTO;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IPBUploadSPA.Utility
{
    public class ExcelRowTransformer<T>
    {
        private readonly Type _type;
        private readonly Dictionary<int, PropertyInfo> _columns = new Dictionary<int, PropertyInfo>();
        private readonly bool _nullasempty;
        private readonly HashSet<string> _headings = new HashSet<string>();

        public bool HasIPBField()
        {
            if (_columns.Count > 0)
            {
                return _columns.Values.Count(s => s.Name.Equals("SampleLocAbsPosition")) > 0;
            }
            return false;
        }

        public bool HasStep2Field()
        {
            if (_headings.Count > 0)
            {
                return _headings.Contains("PARENT_CUID");
            }
            return false;
        }

        public ExcelRowTransformer(ExcelRangeBase headerrow, bool nullasempltystring = false)
        {
            _type = typeof(T);
            _nullasempty = nullasempltystring;
            var props = (from p in _type.GetProperties()
                         let attr = p.GetCustomAttributes(typeof(HeadingAttribute), true)
                         where attr.Length == 1
                         select new { Property = p, Attribute = attr.First() as HeadingAttribute }).ToList();

            //foreach (var prop in props)
            //{
            //    Console.WriteLine(string.Format(@"{0} : {1}", prop.Property.Name, prop.Attribute == null ? "null" : prop.Attribute.ColumnHeaderText));
            //}

            //Assert headerrow.Start.Row == headerrow.End.Row
            if (headerrow == null || headerrow.Start.Row != headerrow.End.Row) return;
            for (var col = headerrow.Start.Column; col <= headerrow.End.Column; col++)
            {
                var nameer = headerrow.Worksheet.Cells[headerrow.Start.Row, col];
                if (nameer == null) continue;
                var nameVal = nameer.Value;
                if (nameVal == null) continue;
                var name = nameVal.ToString();
                if (String.IsNullOrEmpty(name)) continue;
                _headings.Add(name);
                var pi = props.Where(s => s.Attribute.ColumnHeaderText.Equals(name)).Select(s => s.Property).FirstOrDefault();
                if (pi == null) continue;
                _columns.Add(col, pi);

            }
        }

        public T GetRow(ExcelRangeBase valuerow)
        {
            return GetRow(valuerow, valuerow.Start.Row);
        }

        public T GetRow(ExcelRangeBase valuerow, int rownumber, string OrdFieldName = "Ordinal")
        {
            var result = (T)Activator.CreateInstance(_type);
            if (!string.IsNullOrEmpty(OrdFieldName))
            {
                var propOrd = result.GetType().GetProperty(OrdFieldName);
                if (propOrd != null)
                {
                    propOrd.SetValue(result, Convert.ChangeType(rownumber, propOrd.PropertyType));
                }
            }
            foreach (var key in _columns.Keys)
            {
                var range = valuerow.Worksheet.Cells[rownumber, key];
                if (range == null) continue;

                var value = range.Value;

                if (value != null && string.IsNullOrEmpty(value.ToString()) && _columns[key].PropertyType.IsGenericType && _columns[key].PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
	            {
                    value = null;
	            }

                if (_nullasempty && value == null && !(_columns[key].PropertyType.IsGenericType && _columns[key].PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    _columns[key].SetValue(result, Convert.ChangeType(String.Empty, _columns[key].PropertyType));
                }
                else if (value != null)
                {

                    // * FLULIMS-711;
                    if (value is DateTime)
                    {
                        value = ((DateTime)value).ToShortDateString();
                    }
                    // * FLULIMS-732 No built in parsing;
                    // http://stackoverflow.com/questions/4655565/reading-dates-from-openxml-excel-files
                    else if (range.Style.Numberformat.NumFmtID > 164 && range.Style.Numberformat.NumFmtID < 181)
                    {
                        DateTime tempdate;
                        if (DateTime.TryParse(range.Text, out tempdate))
                        {
                            value = tempdate.ToShortDateString();
                        }
                        else // * let openxml format, might get time from this
                        {
                            value = range.Text;
                        }
                    }
                    _columns[key].SetValue(result, Convert.ChangeType(value, _columns[key].PropertyType), null);
                }
            }
            return result;
        }

        public IEnumerable<T> GetRows(ExcelRangeBase valuerange)
        {
            var result = new List<T>();
            for (var i = valuerange.Start.Row; i <= valuerange.End.Row; i++)
            {
                //if (i < 13) continue;

                result.Add(GetRow(valuerange, i));
            }
            return result;
        }
    }
}