﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Web.Mvc;
using IPBUploadSPA.StarLIMSService;
using IPBUploadSPA.StarLIMSServiceWHO;

namespace IPBUploadSPA
{
    public static class ExtensionMethods
    {
        public static String GetValueNS(this ExcelRangeBase erb)
        {
            return erb.Value != null ? erb.Value.ToString() : null;
        }

        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                        return true;
            #else
                  return false;
            #endif
        }

        public static String GetStringForDB(this StarLIMSService.ReturnDTO[] values)
        {
            var allvalid = true;
            var result = String.Empty;
            for (int i = 0; i < values.Length; i++)
            {
                var item = values[i];
                if (allvalid && !item.bRet)
                {
                    allvalid = false;
                }
                result = result + item.sRet + System.Environment.NewLine;
            }
            return string.Concat(allvalid, "|", result);
        }

        public static String GetStringForDB(this StarLIMSServiceWHO.ReturnDTO[] values)
        {
            var allvalid = true;
            var result = String.Empty;
            for (int i = 0; i < values.Length; i++)
            {
                var item = values[i];
                if (allvalid && !item.bRet)
                {
                    allvalid = false;
                }
                result = result + item.sRet + System.Environment.NewLine;
            }
            return string.Concat(allvalid, "|", result);
        }

        public static String GetStringForDB(this StarLIMSServiceZVT.ReturnDTO[] values)
        {
            var allvalid = true;
            var result = String.Empty;
            for (int i = 0; i < values.Length; i++)
            {
                var item = values[i];
                if (allvalid && !item.bRet)
                {
                    allvalid = false;
                }
                result = result + item.sRet + System.Environment.NewLine;
            }
            return string.Concat(allvalid, "|", result);
        }

        public static String GetStringForDB(this StarLIMSServiceFLAPWHO.ReturnDTO value)
        {
            var allvalid = true;
            var result = String.Empty;
            //for (int i = 0; i < values.Length; i++)
            //{
                var item = value;
                if (allvalid && !item.bRet)
                {
                    allvalid = false;
                }
                result = result + item.sRet + System.Environment.NewLine;
            //}
            return string.Concat(allvalid, "|", result);
        }
    }
}