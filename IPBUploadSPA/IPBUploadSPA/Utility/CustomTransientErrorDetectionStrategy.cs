﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace IPBUploadSPA.Utility
{
    public class CustomTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception ex)
        {
            var result = false;
            var timeoutEx = ex as TimeoutException;
            if (timeoutEx != null)
            {
                result = true;
            }
            var communicationEx = ex as CommunicationException;
            if (communicationEx != null)
            {
                result = true;
            }
            return result;
        }
    }
}