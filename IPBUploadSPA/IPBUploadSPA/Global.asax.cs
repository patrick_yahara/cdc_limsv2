﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace IPBUploadSPA
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private readonly string BASEFOLDERNAME = "uploads";

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var PATH = HttpContext.Current.Server.MapPath(string.Format(@"~/{0}/", BASEFOLDERNAME));
            AppDomain.CurrentDomain.SetData("DataDirectory", PATH);

                
        }
    }
}
