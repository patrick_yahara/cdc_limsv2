﻿var mainApp = angular.module('mainApp', ['ngRoute', 'ngAnimate', 'flow', 'uploadControllers']).config(['flowFactoryProvider', function (flowFactoryProvider) {
    var baseUrl = $("base").first().attr("href");
    flowFactoryProvider.factory = fustyFlowFactory;
    flowFactoryProvider.defaults = {
        target: baseUrl + 'api/Upload',
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1
    };
}]).filter('unsafe', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
});

mainApp.config([
    '$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var baseUrl = $("base").first().attr("href");
        $routeProvider.
            when('/upload', {
                templateUrl: baseUrl + 'Scripts/app/Views/upload.html',
                controller: 'uploadController'
            }).
            when('/process/:uploadrequestid', {
                templateUrl: baseUrl + 'Scripts/app/Views/process.html',
                controller: 'processController'
            }).
            when('/process/:uploadrequestid/:skip/:take', {
                templateUrl: baseUrl + 'Scripts/app/Views/process.html',
                controller: 'processControllerST'
            }).
            when('/', {
                redirectTo: '/upload'
            }).
            otherwise({
                redirectTo: '/upload'
            });
    }
]);

mainApp.directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function(val) {
                return '' + val;
            });
        }
    };
});

mainApp.factory('sampleService', function ($q, $http) {
    var self = this;
    self.queuedata = [];
    var localurl = null;
    var baseUrl = $("base").first().attr("href");
    return {
        seturl : function(val)
        {
            localurl = val;
        },
        geturl: function () {
            return localurl;
        },
        insertSample: function (limsSite) {
            console.log(limsSite);
            var urlBase = baseUrl + 'api/Samples';
            var deferred = $q.defer();
            return $http.post(urlBase, {}, { params: { 'filepath': localurl, 'limssite': limsSite } }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        getQueue: function () {
            var urlBase = baseUrl + 'api/Samples/GetUploadRequests';
            var deferred = $q.defer();
            return $http.get(urlBase).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        deleteRow: function (id) {
            var urlBase = baseUrl + 'odata/UploadRequests(' + id + ')';
            var deferred = $q.defer();
            return $http.delete(urlBase).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        processRow: function (id) {
            var urlBase = baseUrl + 'api/Samples/Process';
            var deferred = $q.defer();
            return $http.get(urlBase, {
                params: {
                    uploadrequestid: id
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        cleanupRequests: function () {
            var urlBase = baseUrl + 'api/Samples/CleanupRequests';
            var deferred = $q.defer();
            return $http.get(urlBase).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        getDetails: function (id) {
            var urlBase = baseUrl + 'api/Samples/GetUploadRequestDetails';
            var deferred = $q.defer();
            return $http.get(urlBase, {
                params: {
                    uploadrequestid: id
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
        getDetailsST: function (id, skip, take) {
            var urlBase = baseUrl + 'api/Samples/GetUploadRequestDetailsST';
            var deferred = $q.defer();
            return $http.get(urlBase, {
                params: {
                    uploadrequestid: id,
                    skip: skip,
                    take: take
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        },
    }
});
