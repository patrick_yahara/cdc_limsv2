﻿var uploadControllers = angular.module('uploadControllers', []);

uploadControllers.controller('uploadController', ['$scope', '$location', '$interval', '$filter', 'sampleService', function ($scope, $location, $interval, $filter, sampleService) {



    $scope.sort = {
        sortingOrder: 'id',
        reverse: false
    };
 
    $scope.items = [];


    this.refreshData = function () {
        sampleService.getQueue().then(function (data) {
            $scope.status = "success";
            $scope.items = data.data;
        }, function (status) { $scope.status = "failure"; });
    }
    this.refreshData();
    
    $interval(function () {
        this.refreshData();
    }.bind(this), 10000);

    $scope.deleteRow = function (id) {
        sampleService.deleteRow(id);
        single_object = $filter('filter')($scope.items, function (d) { return d.Id === id; })[0];
        if (!!single_object) {
            var index = $scope.items.indexOf(single_object);
            if (index > -1) {
                $scope.items.splice(index, 1);
            }
        }
    };

    $scope.processRow = function (id) {
        sampleService.processRow(id);
    };

    $scope.cleanupRequests = function () {
        sampleService.cleanupRequests();
    };

    $scope.removeDisabled = function (item) {
        result = (!!item.Error);
        if (result) {
            result = (item.Error.length > 0);
        }
        if (result) {
            return false;
        }
        result = (!!item.Progress2);
        if (result) {
            result = (item.Progress2 >= 100);
        }
        if (result) {
            return false;
        }
        return true;
    };


    $scope.model = { limsSite: 0 };

    $scope.cancelText = 'Cancel';
    $scope.uploader = {};
    $scope.upload = function () {
        $scope.uploader.flow.upload();
    };
    $scope.success = function (flow, fileparam, message) {
        var file = angular.fromJson(message);
        $scope.cancelText = 'Add to Queue';
        $scope.processFile = function () {
            var url = file[0].path;
            sampleService.seturl(url);
            sampleService.insertSample($scope.model.limsSite).then(
                function (data) {
                    $scope.status = "success";
                }
                , function (status) {
                    $scope.status = "failure";
                });
            while ($scope.uploader.flow.files.length > 0) {
                $scope.uploader.flow.files.pop();
            }
        };
    };

    $scope.showDetails = function (id)
    {
        $location.path('/process/' + id);
    }
}]);

uploadControllers.controller('processController', ['$scope', '$location', '$interval', '$routeParams', '$timeout', 'sampleService', function ($scope, $location, $interval, $routeParams, $timeout, sampleService) {
    $scope.items = [];


    this.refreshData = function () {
        sampleService.getDetails($routeParams.uploadrequestid).then(function (data) {
            $scope.status = "success";
            $scope.items = data.data;
        }, function (status) { $scope.status = "failure"; });
    }
    this.refreshData();

    $interval(function () {
        this.refreshData();
    }.bind(this), 10000);

    $scope.desiredResponse = null;
    $scope.status = "processing";
    $scope.toprocess = sampleService.geturl();
    
    $scope.hasResponse = function (item) {
        if (!!item.Response) {
            
            if (item.Response.length > 0) {
                return false;
            }
        }
        return true;
    };
    $scope.showResponse = function (item) {
        $scope.responseText = item.Response;
        $scope.desiredResponse = item;
        console.log(item.Response);
    };

    $scope.hideResponse = function () {
        $scope.responseText = null;
        $scope.desiredResponse = null;
    };

    $scope.returnUpload = function ()
    { $location.path('/upload');};
}]);

uploadControllers.controller('processControllerST', ['$scope', '$location', '$interval', '$routeParams', '$timeout', 'sampleService', function ($scope, $location, $interval, $routeParams, $timeout, sampleService) {
    $scope.items = [];


    this.refreshData = function () {
        sampleService.getDetailsST($routeParams.uploadrequestid, $routeParams.skip, $routeParams.take).then(function (data) {
            $scope.status = "success";
            $scope.items = data.data;
        }, function (status) { $scope.status = "failure"; });
    }
    this.refreshData();

    $interval(function () {
        this.refreshData();
    }.bind(this), 10000);

    $scope.desiredResponse = null;
    $scope.status = "processing";
    $scope.toprocess = sampleService.geturl();

    $scope.hasResponse = function (item) {
        if (!!item.Response) {

            if (item.Response.length > 0) {
                return false;
            }
        }
        return true;
    };
    $scope.showResponse = function (item) {
        $scope.responseText = item.Response;
        $scope.desiredResponse = item;
        console.log(item.Response);
    };

    $scope.hideResponse = function () {
        $scope.responseText = null;
        $scope.desiredResponse = null;
    };

    $scope.returnUpload = function ()
    { $location.path('/upload'); };
}]);