﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPBUploadSPA.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? site)
        {
            ViewBag.Title = "Home Page";

            if (site.HasValue)
            {
                var cookie = HttpContext.Request.Cookies["SiteCookie"];
                if (cookie != null && !cookie.Value.Equals(site.Value.ToString()))
                {
                    CreateNewCookieMethod(1);
                }            
                CreateNewCookieMethod(site.Value);
            }
            else 
            {
                var cookie = HttpContext.Request.Cookies["SiteCookie"];
                if (cookie == null)
                {
                    CreateNewCookieMethod(1);
                }            
            }


            return View();
        }

        private void CreateNewCookieMethod(int site)
        {
            HttpCookie cookie = new HttpCookie("SiteCookie");
            cookie.Value = site.ToString();
        }
    }
}
