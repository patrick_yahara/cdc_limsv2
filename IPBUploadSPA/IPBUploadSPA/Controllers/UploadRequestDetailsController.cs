﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using IPBUploadSPA.Models;
using System.Web.Http.Cors;

namespace IPBUploadSPA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using IPBUploadSPA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<UploadRequestDetail>("UploadRequestDetails");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [EnableCors("*", "*", "*")]
    public class UploadRequestDetailsController : ODataController
    {
        private IPBUploadEntities db = new IPBUploadEntities();

        // GET: odata/UploadRequestDetails
        [Queryable]
        public IQueryable<UploadRequestDetail> GetUploadRequestDetails()
        {
            return db.UploadRequestDetails;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UploadRequestDetailExists(long key)
        {
            return db.UploadRequestDetails.Count(e => e.id == key) > 0;
        }
    }
}
