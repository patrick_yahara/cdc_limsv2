﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using IPBUploadSPA.Models;
using System.Web.Http.Cors;
using System.Threading.Tasks;
using IPBUploadSPA.Utility;
using System.Web.Hosting;

namespace IPBUploadSPA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using IPBUploadSPA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<UploadRequest>("UploadRequests");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [EnableCors("*", "*", "*")]
    public class UploadRequestsController : ODataController
    {
        private IPBUploadEntities db = new IPBUploadEntities();

        // GET: odata/UploadRequests
        [Queryable]
        public IQueryable<UploadRequest> GetUploadRequests()
        {
            return db.UploadRequests;
        }

        // DELETE: odata/UploadRequests(5)
        public IHttpActionResult Delete([FromODataUri] long key)
        {
            UploadRequest uploadRequest = db.UploadRequests.Find(key);
            if (uploadRequest == null)
            {
                return NotFound();
            }
            db.UploadRequestDetails.RemoveRange(db.UploadRequestDetails.Where(s => s.uploadrequestid.Equals(uploadRequest.id)));
            db.UploadRequests.Remove(uploadRequest);
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                // this will throw if our rowcount is 0, which I just want to ignore               
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UploadRequestExists(long key)
        {
            return db.UploadRequests.Count(e => e.id == key) > 0;
        }
    }
}
