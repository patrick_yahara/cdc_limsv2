﻿using IPBUploadSPA.DTO;
using IPBUploadSPA.Utility;
using IPBUploadSPA.Models;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Hosting;

namespace IPBUploadSPA.Controllers
{
    [EnableCors("*", "*", "*")]
    public class SamplesController : ApiController
    {

        public SamplesController()
        {
          
        }

        [HttpPost]
        public void Post(string filepath, int limssite, bool removefile = true)
        {
            var responses = new List<UploadRequestDTO>();

            QueueProcessor.Instance.AddFileToQueue(HttpContext.Current.Server.MapPath(filepath), limssite);
            if (!QueueProcessor.Instance.IsStartedFile)
            {
                HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartAddToStore(ct));
            }

            // 05/05/2015 458 PDC
            //if (!QueueProcessor.Instance.IsStarted)
            //{
            //    HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartProcessing(ct));
            //}

        }

        [HttpGet]
        public void Process(long uploadrequestid)
        {
            QueueProcessor.Instance.AddProcessRequestToQueue(uploadrequestid);
            if (!QueueProcessor.Instance.IsStarted)
            {
                HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartProcessing(ct));
            }
        }

        [HttpGet]
        public void CleanupRequests()
        {
            HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.CleanupDataStore(ct));
        }

        [HttpGet]
        public IEnumerable<UploadRequestDTO> GetUploadRequests()
        {
            using (var context = new IPBUploadEntities())
            {
                var query = from s in context.UploadRequests
                            orderby s.daterequested descending
                            select new UploadRequestDTO
                            {
                                Id = s.id,
                                Filename = s.filename,
                                Progress1 = s.preprogress,
                                Progress2 = s.progress,
                                Validated = s.validated.Equals(1) ? "Y" : "N",
                                Error = s.error,
                                Site = s.site.Equals(0) ? "IPB" : s.site.Equals(1) ? "Influenza CLAW" : s.site.Equals(2) ? "ZVT" : "Influenza WHO",
                            };
                return query.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<UploadRequestDetailDTO> GetUploadRequestDetails(long uploadrequestid)
        {
            using (var context = new IPBUploadEntities())
            {
                var query = from s in context.UploadRequestDetails
                            where s.uploadrequestid.Equals(uploadrequestid)
                            orderby s.errored descending, s.ordinal ascending
                            select new UploadRequestDetailDTO
                            {
                                UploadRequestId = s.uploadrequestid,
                                Attempts = s.attempts,
                                Display = s.errored.Equals(1) ? s.display : null,
                                Duration = s.durationms,
                                Response = s.response,
                                Errored = s.errored.Equals(1) ? "Y" : "N",
                                CSID = s.csid,
                                CUID = s.cuid,
                            };
                return query.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<UploadRequestDetailDTO> GetUploadRequestDetailsST(long uploadrequestid, int skip, int take)
        {
            using (var context = new IPBUploadEntities())
            {
                var query = from s in context.UploadRequestDetails
                            where s.uploadrequestid.Equals(uploadrequestid)
                            orderby s.errored descending, s.ordinal ascending
                            select new UploadRequestDetailDTO
                            {
                                UploadRequestId = s.uploadrequestid,
                                Attempts = s.attempts,
                                Display = s.errored.Equals(1) ? s.display : null,
                                Duration = s.durationms,
                                Response = s.response,
                                Errored = s.errored.Equals(1) ? "Y" : "N",
                                CSID = s.csid,
                                CUID = s.cuid,
                            };
                return query.Skip(skip).Take(take).ToList();
            }
        }
    }
}
