﻿using IPBUploadSPA.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPBUploadSPA.Controllers
{
    [EnableCors("*", "*", "*")]
    public class UPMController : ApiController
    {
        [HttpGet]
        public string ResetInstance()
        {
            QueueProcessor.Invalidate();
            return GetState();
        }

        [HttpGet]
        public string StartProcessor()
        {
            if (!QueueProcessor.Instance.IsStarted)
            {
                HostingEnvironment.QueueBackgroundWorkItem(ct => QueueProcessor.Instance.StartProcessing(ct));
            }
            return GetState();
        }

        [HttpGet]
        public string GetState()
        {
            var result = new StringBuilder();
            result.AppendLine(String.Format("Has Instance: {0}", QueueProcessor.HasInstance()));
            result.AppendLine(String.Format("Has Cancelled Instance: {0}", QueueProcessor.HasCancelledInstance()));
            result.AppendLine(String.Format("Has Bad Connection: {0}", QueueProcessor.HasBadConnection()));
            return result.ToString();
        }
    }
}
