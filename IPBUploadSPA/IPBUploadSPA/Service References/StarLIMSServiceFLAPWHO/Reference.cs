﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPBUploadSPA.StarLIMSServiceFLAPWHO {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.starlims.com/webservices/", ConfigurationName="StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoap")]
    public interface WS_FastAccession_wsSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLogin", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[] FastLogin(string xml, bool debug);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLogin", ReplyAction="*")]
        System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[]> FastLoginAsync(string xml, bool debug);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLogin_sessionless", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[] FastLogin_sessionless(string xml, bool debug, string user, string pwd);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLogin_sessionless", ReplyAction="*")]
        System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[]> FastLogin_sessionlessAsync(string xml, bool debug, string user, string pwd);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLoginEx", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO FastLoginEx(string xmlEx, bool debugEx);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLoginEx", ReplyAction="*")]
        System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO> FastLoginExAsync(string xmlEx, bool debugEx);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLoginEx_sessionless", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO FastLoginEx_sessionless(string xmlEx, bool debugEx, string user, string pwd);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/FastLoginEx_sessionless", ReplyAction="*")]
        System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO> FastLoginEx_sessionlessAsync(string xmlEx, bool debugEx, string user, string pwd);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogin", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        bool UserLogin(string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogin", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> UserLoginAsync(string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogout", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        void UserLogout();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogout", ReplyAction="*")]
        System.Threading.Tasks.Task UserLogoutAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogged", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        bool UserLogged();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/UserLogged", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> UserLoggedAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunAction", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        object RunAction(string actionID, object[] parameters);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunAction", ReplyAction="*")]
        System.Threading.Tasks.Task<object> RunActionAsync(string actionID, object[] parameters);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunActionDirect", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        object RunActionDirect(string actionID, object[] parameters, string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunActionDirect", ReplyAction="*")]
        System.Threading.Tasks.Task<object> RunActionDirectAsync(string actionID, object[] parameters, string UserName, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunRESTActions", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(Style=System.ServiceModel.OperationFormatStyle.Rpc, SupportFaults=true, Use=System.ServiceModel.OperationFormatUse.Encoded)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(DtoBase))]
        string[] RunRESTActions(string[] actionUrls);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.starlims.com/webservices/RunRESTActions", ReplyAction="*")]
        System.Threading.Tasks.Task<string[]> RunRESTActionsAsync(string[] actionUrls);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(Namespace="http://www.starlims.com/webservices/encodedTypes")]
    public partial class ReturnDTO : DtoBase {
        
        private bool bRetField;
        
        private string sRetField;
        
        /// <remarks/>
        public bool bRet {
            get {
                return this.bRetField;
            }
            set {
                this.bRetField = value;
                this.RaisePropertyChanged("bRet");
            }
        }
        
        /// <remarks/>
        public string sRet {
            get {
                return this.sRetField;
            }
            set {
                this.sRetField = value;
                this.RaisePropertyChanged("sRet");
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(ReturnDTO))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(Namespace="http://www.starlims.com/webservices/encodedTypes")]
    public partial class DtoBase : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string xmlTypeField;
        
        /// <remarks/>
        public string XmlType {
            get {
                return this.xmlTypeField;
            }
            set {
                this.xmlTypeField = value;
                this.RaisePropertyChanged("XmlType");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WS_FastAccession_wsSoapChannel : IPBUploadSPA.StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WS_FastAccession_wsSoapClient : System.ServiceModel.ClientBase<IPBUploadSPA.StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoap>, IPBUploadSPA.StarLIMSServiceFLAPWHO.WS_FastAccession_wsSoap {
        
        public WS_FastAccession_wsSoapClient() {
        }
        
        public WS_FastAccession_wsSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WS_FastAccession_wsSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WS_FastAccession_wsSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WS_FastAccession_wsSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[] FastLogin(string xml, bool debug) {
            return base.Channel.FastLogin(xml, debug);
        }
        
        public System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[]> FastLoginAsync(string xml, bool debug) {
            return base.Channel.FastLoginAsync(xml, debug);
        }
        
        public IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[] FastLogin_sessionless(string xml, bool debug, string user, string pwd) {
            return base.Channel.FastLogin_sessionless(xml, debug, user, pwd);
        }
        
        public System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO[]> FastLogin_sessionlessAsync(string xml, bool debug, string user, string pwd) {
            return base.Channel.FastLogin_sessionlessAsync(xml, debug, user, pwd);
        }
        
        public IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO FastLoginEx(string xmlEx, bool debugEx) {
            return base.Channel.FastLoginEx(xmlEx, debugEx);
        }
        
        public System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO> FastLoginExAsync(string xmlEx, bool debugEx) {
            return base.Channel.FastLoginExAsync(xmlEx, debugEx);
        }
        
        public IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO FastLoginEx_sessionless(string xmlEx, bool debugEx, string user, string pwd) {
            return base.Channel.FastLoginEx_sessionless(xmlEx, debugEx, user, pwd);
        }
        
        public System.Threading.Tasks.Task<IPBUploadSPA.StarLIMSServiceFLAPWHO.ReturnDTO> FastLoginEx_sessionlessAsync(string xmlEx, bool debugEx, string user, string pwd) {
            return base.Channel.FastLoginEx_sessionlessAsync(xmlEx, debugEx, user, pwd);
        }
        
        public bool UserLogin(string UserName, string Password) {
            return base.Channel.UserLogin(UserName, Password);
        }
        
        public System.Threading.Tasks.Task<bool> UserLoginAsync(string UserName, string Password) {
            return base.Channel.UserLoginAsync(UserName, Password);
        }
        
        public void UserLogout() {
            base.Channel.UserLogout();
        }
        
        public System.Threading.Tasks.Task UserLogoutAsync() {
            return base.Channel.UserLogoutAsync();
        }
        
        public bool UserLogged() {
            return base.Channel.UserLogged();
        }
        
        public System.Threading.Tasks.Task<bool> UserLoggedAsync() {
            return base.Channel.UserLoggedAsync();
        }
        
        public object RunAction(string actionID, object[] parameters) {
            return base.Channel.RunAction(actionID, parameters);
        }
        
        public System.Threading.Tasks.Task<object> RunActionAsync(string actionID, object[] parameters) {
            return base.Channel.RunActionAsync(actionID, parameters);
        }
        
        public object RunActionDirect(string actionID, object[] parameters, string UserName, string Password) {
            return base.Channel.RunActionDirect(actionID, parameters, UserName, Password);
        }
        
        public System.Threading.Tasks.Task<object> RunActionDirectAsync(string actionID, object[] parameters, string UserName, string Password) {
            return base.Channel.RunActionDirectAsync(actionID, parameters, UserName, Password);
        }
        
        public string[] RunRESTActions(string[] actionUrls) {
            return base.Channel.RunRESTActions(actionUrls);
        }
        
        public System.Threading.Tasks.Task<string[]> RunRESTActionsAsync(string[] actionUrls) {
            return base.Channel.RunRESTActionsAsync(actionUrls);
        }
    }
}
