﻿using CSVUtility;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSVFileSorter
{
    class Program
    {
        public  List<String> rooms = new List<string>();
        public  List<String> freezers = new List<string>();
        public  List<String> boxes = new List<string>();
        public List<String> locations = new List<string>();

        public static string _header { get; set; }
        static void Main(string[] args)
        {
            var programclass = new Program();
            if (args.Length < 2)
            {
                Console.WriteLine("Pass in filename");
                Console.WriteLine("Pass in optional file containing strings to filter by based upon first sort column");
                Console.WriteLine("Pass in comparison columns");
                Console.WriteLine(@"CSVFileSorter.exe <filename> [-f<filterFile>] <sort column 1> <sortcolumn 2> … <sortcolumn n>");
                Console.WriteLine("Press Enter to exit"); 
                Console.ReadLine();
                return;
            }
            String inputFileName = args[0];
            string[] headers = null;
            string[] filterList = null;

            //if filter file specified
            if (args[1].StartsWith("-f"))
            {
                string fn = args[1].Substring(2);
                filterList = File.ReadAllLines(fn);
                Console.WriteLine("filtering based on files in {0}:", fn );
                foreach(String study in filterList)
                {
                  Console.WriteLine(study);
                }
                headers = new string[args.Length - 1];
                Array.Copy(args, 1, headers, 0, args.Length - 1);
            }
            else  //if no filter file specified
            {
                headers = new string[args.Length - 1];
                Array.Copy(args, 1, headers, 0, args.Length - 1);
            }

            var t = CreateRowClass(inputFileName, headers);
            if (t != null)
            {
               Console.WriteLine(t.Name);
               foreach (var item in t.GetInterfaces())
               {
                   Console.WriteLine(item.Name);
               }

                Split(inputFileName, filterList, t);

                SortTheChunks(t, ref programclass);

                MergeTheChunks(t);
                Console.WriteLine("Writing Rooms/Freezers/Boxes/Location files");
            }
            else
            {
                Console.WriteLine("Error creating row class!");
            }
            var outpath = Path.Combine(Environment.CurrentDirectory, "process");
            File.WriteAllLines(string.Format("{0}\\rooms.txt", outpath), programclass.rooms);
            File.WriteAllLines(string.Format("{0}\\freezers.txt", outpath), programclass.freezers);
            File.WriteAllLines(string.Format("{0}\\boxes.txt", outpath), programclass.boxes);
            File.WriteAllLines(string.Format("{0}\\locations.txt", outpath), programclass.locations);
            Console.WriteLine("Press Enter to exit");
            //Console.WriteLine("Remember to renable the full functionality of this program!!!");
            Console.ReadLine();
        }

        static Type CreateRowClass(string file, string[] headers)
        {
            Type result = null;
            //Microsoft.CSharp.CSharpCodeProvider 
            using (StreamReader sr = new StreamReader(file))
            {
                if (sr.Peek() >= 0)
                {
                    var headerString = sr.ReadLine();
                    _header = headerString;
                    var headerArray = headerString.Split(',');
                    if (headerArray.Length > 0)
                    {
                        var sb = new StringBuilder();
                        sb.AppendLine(@"public class DynamicRowObject : System.IComparable");
                        sb.AppendLine(@"{");
                        foreach (var item in headerArray)
                        {
                            sb.AppendLine(String.Format("public string {0} {{ get; set; }}", item));
                        }
                        sb.AppendLine(@"public int CompareTo(object obj)");
                        sb.AppendLine(@"{");
                        sb.AppendLine(@"var result = 0;");
                        sb.AppendLine(@"if (obj == null) return 1;");
                        sb.AppendLine(@"var other = obj as DynamicRowObject;");
                        sb.AppendLine(@"if (other == null) return 1;");
                        foreach(String column in headers)
                        {
                            if (headerArray.Contains(column))
                            {
                                sb.AppendLine(String.Format(@"result = this.{0}.CompareTo(other.{0});", column));
                                sb.AppendLine(@"if (result != 0) return result;");
                            }
                        }
                        sb.AppendLine(@"return result;");
                        sb.AppendLine(@"}");

                        sb.AppendLine(@"public void Hydrate(string values)");
                        sb.AppendLine(@"{");
                        //sb.AppendLine(@"string[] valuearray = values.Split(',');");
                        //parse the string honoring the rules of double quoting strings in CSV files
                        sb.AppendLine("string[] valuearray = csvSplitter(values);");
                        sb.AppendLine(@"if (valuearray.Length == 0) return;");
                        sb.AppendLine(String.Format(@"if (valuearray.Length != {0})",headerArray.Length));
                        sb.AppendLine(@"{System.Console.WriteLine(""Input string has wrong number of columns:"");");
                        sb.AppendLine(@"System.Console.WriteLine(""{0}"",values);return;}");
                        for (int i = 0; i < headerArray.Length; i++)
                        {
                            sb.AppendLine(String.Format("{0} = valuearray[{1}];", headerArray[i], i));
                        }
                        sb.AppendLine(@"}");


                        sb.AppendLine(@"private string [] csvSplitter(string values){");
                        //I got this pattern from Stack Overflow, don't ask me to explain it though.
                        //sb.AppendLine(@"string pattern = @",(?=(?:[^""]*""[^""]*"")*[^""]*$)"; ");
                        sb.AppendLine(@"string pattern = @"",(?=(?:[^""""]*""""[^""""]*"""")*[^""""]*$)"";");
                        //sb.AppendLine(@"return System.Text.RegularExpressions.Regex.Split(values.Substring(0, values.Length - 2), pattern);");
                        sb.AppendLine(@"return System.Text.RegularExpressions.Regex.Split(values, pattern);");
                        sb.AppendLine(@"}");


                        sb.AppendLine(@"public override string ToString()");
                        sb.AppendLine(@"{");
                        sb.AppendLine(@"try");
                        sb.AppendLine(@"{");
                        sb.AppendLine(@"if (ORIG_CUID.Equals(cdcuniqueid))");
                        sb.AppendLine(@"{");
                        sb.AppendLine(@"ORIG_CUID = System.String.Empty;");
                        sb.AppendLine(@"}");
                        sb.AppendLine(@"}");
                        sb.AppendLine(@"catch (System.Exception)");
                        sb.AppendLine(@"{");
                        sb.AppendLine(@"}");
                        sb.AppendLine(@"var result = new System.Text.StringBuilder();");
                        for (int i = 0; i < headerArray.Length; i++)
                        {
                            sb.AppendLine(String.Format("result.Append({0});", headerArray[i]));
                            if (i < headerArray.Length - 1)
                            {
                                sb.AppendLine(@"result.Append(',');");
                            }
                        }

                        sb.AppendLine(@"return result.ToString();");
                        sb.AppendLine(@"}");

                        sb.AppendLine(@"}");

                        System.CodeDom.Compiler.CompilerParameters compilerParameters = new CompilerParameters();
                        compilerParameters.GenerateInMemory = true;
                        compilerParameters.TreatWarningsAsErrors = false;
                        compilerParameters.WarningLevel = 4;
                        compilerParameters.ReferencedAssemblies.Add("System.dll");


                        var results = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(compilerParameters, sb.ToString());

                        // throw a copy out to use in the next step
                        compilerParameters = new CompilerParameters();
                        compilerParameters.GenerateInMemory = false;
                        compilerParameters.TreatWarningsAsErrors = false;
                        compilerParameters.WarningLevel = 4;
                        compilerParameters.ReferencedAssemblies.Add("System.dll");
                        compilerParameters.GenerateExecutable = false;
                        compilerParameters.OutputAssembly = "DynamicRowAssembly.dll";

                        CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(compilerParameters, sb.ToString());

                        if (results.Errors.Count == 0)
                        {
                            var assembly = results.CompiledAssembly;
                            result = assembly.GetType("DynamicRowObject");
                        }
                        else
                        {
                            foreach (var item in results.Errors)
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine("Code:");
                            Console.Write(sb.ToString());
                        }

                        
                    }
                }
            }
            return result;
        }

        static void SortEmit(string file, Type type)
        {
            Type listType = typeof(List<>).MakeGenericType(new Type[] { type });
            var listInstance = Activator.CreateInstance(listType);
            //var arrtype = type.MakeArrayType();
            // Arrays need to know then length in the constuctor...boo!
            //var arrobj = Activator.CreateInstance(arrtype);
            using (StreamReader sr = new StreamReader(file))
            {
                if (sr.Peek() >= 0)
                {
                    sr.ReadLine();
                }
                while (sr.Peek() >= 0)
                {
                    var valueString = sr.ReadLine();
                    var obj = Activator.CreateInstance(type);
                    var info = type.GetMethod("Hydrate");
                    if (info != null)
                    {
                        info.Invoke(obj, new object[] { valueString });
                        var addinfo = listType.GetMethod("Add");
                        if (addinfo != null)
                        {
                            addinfo.Invoke(listInstance, new object[] { obj });
                        }
                    }
                }
            }
            
            var sortinfo = listType.GetMethod("Sort", new Type[0]);
            if (sortinfo != null)
            {
                sortinfo.Invoke(listInstance, null);
            }
            var leninfo = listType.GetProperty("Count");
            if (leninfo != null)
            {
                var len = (int)leninfo.GetValue(listInstance);
                for (int i = 0; i < len; i++)
                {
                    var iteminfo = listType.GetMethod("get_Item");
                    if (iteminfo != null)
                    {
                        var value = iteminfo.Invoke(listInstance, new object[] { i });
                        //Console.WriteLine(value.ToString());
                        //Console.WriteLine("");
                    }
                }
            }

        }

        static void Split(string file, string [] filterList, Type t)
        {
            int split_num = 1;
            var outpath = Path.Combine(Environment.CurrentDirectory, "process");
            if (!Directory.Exists(outpath))
            {
                Directory.CreateDirectory(outpath);
            }
            else
            {
                var files = Directory.EnumerateFiles(outpath);
                foreach (String fn in files)
                {
                    Console.WriteLine("Deleting {0}", fn);
                    File.Delete(fn);
                }
            }
            Console.WriteLine("Splitting File into chunks...");
            StreamWriter sw = new StreamWriter(
              string.Format("{1}\\split{0:d5}.csv", split_num, outpath), false);
            Console.WriteLine("Writing {0}", ((FileStream)sw.BaseStream).Name);
            long read_line = 0;
            using (StreamReader sr = new StreamReader(file))
            {
                if (sr.Peek() >= 0)
                {
                    sr.ReadLine();
                }
                while (sr.Peek() >= 0)
                {
                    // Progress reporting
                    if (++read_line % 500 == 0)
                        Console.Write("{0:f2}%   \r",
                          100.0 * sr.BaseStream.Position / sr.BaseStream.Length);

                    //The Excel standard for CSV import is that you need to ignore CR/LF and comma's from within a double quoted string.  The following
                    //logic will ensure that the double quoted strings are appended as one line...
                    String outputString = sr.ReadLine();
                    while ((outputString.Split('"').Length - 1) % 2 == 1) 
                    {
                      outputString += "\n";
                      outputString += sr.ReadLine();
                    } 
                    // Copy a line
                    //sw.WriteLine(sr.ReadLine());

                    if (filterList != null)
                    {
                        if (CompareToList(outputString,"study_name", filterList, t))
                        {
                            sw.WriteLine(outputString);
                        }
                    }
                    else
                    {
                        sw.WriteLine(outputString);
                    }

                    // If the file is big, then make a new split,
                    // however if this was the last line then don't bother
                    if (sw.BaseStream.Length > 50000000 && sr.Peek() >= 0)
                    {
                        sw.Close();
                        split_num++;
                        sw = new StreamWriter(
                          string.Format("{1}\\split{0:d5}.csv", split_num, outpath), false);
                        Console.WriteLine("Writing {0}", ((FileStream)sw.BaseStream).Name);
                    }
                }
            }
            sw.Close();
        }

        static void SortTheChunks(Type type, ref Program prog)
        {
            Console.WriteLine("Sorting the chunks...");
            //var errorList = new List<string>();
            var errorList = new List<SortError>();
            var outpath = Path.Combine(Environment.CurrentDirectory, "process");
            foreach (string path in Directory.GetFiles(outpath, "split*.csv"))
            {
                Console.WriteLine("Reading {0}", path);
                long read_line = 0;
                // Read all lines into an array
                //string[] contents = File.ReadAllLines(path);
                Type listType = typeof(List<>).MakeGenericType(new Type[] { type });
                var listInstance = Activator.CreateInstance(listType);
                //foreach (var item in File.ReadLines(path))
                using (var sr = new StreamReader(path))
                {
                    while (sr.Peek() >= 0)
                    {
                        //The Excel standard for CSV import is that you need to ignore CR/LF and comma's from within a double quoted string.  The following
                        //logic will ensure that the double quoted strings are appended as one line...
                        String outputString = sr.ReadLine();
                        while ((outputString.Split('"').Length - 1) % 2 == 1)
                        {
                            outputString += "\n";
                            outputString += sr.ReadLine();
                        } 

                        //var item = sr.ReadLine();
                        var item = outputString;

                        // Progress reporting
                        if (++read_line % 500 == 0)
                            Console.Write("{0:f2}%   \r",
                              100.0 * sr.BaseStream.Position / sr.BaseStream.Length);

                        if (String.IsNullOrEmpty(item))
                        {
                            continue;
                        }
                        var obj = Activator.CreateInstance(type);
                        var info = type.GetMethod("Hydrate");
                        if (info != null)
                        {
                            try
                            {
                                info.Invoke(obj, new object[] { item });
                                var addinfo = listType.GetMethod("Add");
                                if (addinfo != null)
                                {
                                    addinfo.Invoke(listInstance, new object[] { obj });
                                }
                            }
                            catch (Exception e)
                            {
                          
                                //errorList.Add(item);
                                errorList.Add(new SortError(item, e.ToString()));
                            }

                        }

                    }
                }

                // Sort the in-memory array
                //Array.Sort(contents);
                var sortinfo = listType.GetMethod("Sort", new Type[0]);
                if (sortinfo != null)
                {
                    sortinfo.Invoke(listInstance, null);
                    Console.WriteLine("Ending Sort");
                }
                else
                {
                    Console.WriteLine("Warning: No sort method found!!!!");
                }

                // Create the 'sorted' filename
                string newpath = path.Replace("split", "sorted");
                // Write it
                //File.WriteAllLines(newpath, contents);
                Console.WriteLine("Writing {0}", newpath);
                using (var sw = new StreamWriter(newpath, false))
                {
                    var leninfo = listType.GetProperty("Count");
                    if (leninfo != null)
                    {
                        var len = (int)leninfo.GetValue(listInstance);
                        for (int i = 0; i < len; i++)
                        {
                            var iteminfo = listType.GetMethod("get_Item");
                            if (iteminfo != null)
                            {
                                var value = iteminfo.Invoke(listInstance, new object[] { i });
                                CSVUtility.DROTweaker.Tweak(ref value, ref prog.rooms, ref prog.freezers, ref prog.boxes, ref prog.locations, type);
                                sw.WriteLine(value.ToString());
                            }
                        }
                    }
                }
                var errorpath = path.Replace("split", "error");
                //File.WriteAllLines(errorpath, errorList);
                System.IO.StreamWriter file = new System.IO.StreamWriter(errorpath);
                foreach (SortError so in errorList)
                {
                    file.WriteLine(so.ToString());
                }
                file.Close();
                
                // Delete the unsorted chunk
                // File.Delete(path);
                // Free the in-memory sorted array
                //contents = null;
                var clearinfo = listType.GetMethod("Clear", new Type[0]);
                if (clearinfo != null)
                {
                    clearinfo.Invoke(listInstance, null);
                }
                GC.Collect();
            }
        }

        static void MergeTheChunks(Type type)
        {
            Console.WriteLine("Merging the chunks...");
            var outpath = Path.Combine(Environment.CurrentDirectory, "process");
            string[] paths = Directory.GetFiles(outpath, "sorted*.csv");
            if (paths.Length == 0)
            {
                return;
            }
            int chunks = paths.Length; // Number of chunks
            int recordsize = 1024; // estimated record size
            int records = 500000; // estimated total # records
            int maxusage = 500000000; // max memory usage
            int buffersize = maxusage / chunks; // bytes of each queue
            double recordoverhead = 7.5; // The overhead of using Queue<>
            int bufferlen = (int)(buffersize / recordsize /
              recordoverhead); // number of records in each queue

            // Open the files
            StreamReader[] readers = new StreamReader[chunks];
            for (int i = 0; i < chunks; i++)
                readers[i] = new StreamReader(paths[i]);

            // Make the queues
            Queue<string>[] queues = new Queue<string>[chunks];
            for (int i = 0; i < chunks; i++)
                queues[i] = new Queue<string>(bufferlen);

            // Load the queues
            for (int i = 0; i < chunks; i++)
                LoadQueue(queues[i], readers[i], bufferlen);

            // Merge!
            var sw = new StreamWriter(
                          string.Format("{0}\\output.csv", outpath), false);
            bool done = false;
            int lowest_index, j, progress = 0;
            string lowest_value;
            Console.WriteLine("Starting Merge");
            while (!done)
            {
                // Report the progress
                if (++progress % 500 == 0)
                    Console.Write("{0:f2}%   \r",
                      100.0 * progress / records);

                // Find the chunk with the lowest value
                lowest_index = -1;
                lowest_value = "";
                for (j = 0; j < chunks; j++)
                {
                    if (queues[j] != null)
                    {
                        if (lowest_index < 0 ||

                            CompareOrdLocal(queues[j].Peek(), lowest_value, type) < 0
                            //String.CompareOrdinal(
                            //queues[j].Peek(), lowest_value) < 0
                            
                            )
                        {
                            lowest_index = j;
                            lowest_value = queues[j].Peek();
                        }
                    }
                }

                // Was nothing found in any queue? We must be done then.
                if (lowest_index == -1) { done = true; break; }

                // Output it
                sw.WriteLine(lowest_value);

                // Remove from queue
                queues[lowest_index].Dequeue();
                // Have we emptied the queue? Top it up
                if (queues[lowest_index].Count == 0)
                {
                    LoadQueue(queues[lowest_index],
                      readers[lowest_index], bufferlen);
                    // Was there nothing left to read?
                    if (queues[lowest_index].Count == 0)
                    {
                        queues[lowest_index] = null;
                    }
                }
            }
            sw.Close();
            Console.WriteLine("Ending Merge");
            // Close the files
            for (int i = 0; i < chunks; i++)
            {
                readers[i].Close();
                //delete the files
                //File.Delete(paths[i]);
            }
        }

        static int CompareOrdLocal(string value1, string value2, Type type)
        { 
                
                var obj1 = Activator.CreateInstance(type);
                var obj2 = Activator.CreateInstance(type);
                var argtypes = new Type[1];
                argtypes[0] = new object().GetType();
                var info = type.GetMethod("Hydrate");
                if (info != null)
                {
                    try
                    {
                        info.Invoke(obj1, new object[] { value1 });
                        info.Invoke(obj2, new object[] { value2 });
                        var ctinfo = type.GetMethod("CompareTo", argtypes);
                        if (ctinfo != null)
                        {
                            return (int)ctinfo.Invoke(obj1, new object[] { obj2 });
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error invoking compare to {0}", e.ToString());
                        Console.WriteLine("Comparing:\n {0} \nwith\n {1}", value1, value2);
                        Console.WriteLine("Press Enter to continue");
                        //Console.WriteLine("Remember to renable the full functionality of this program!!!");
                        Console.ReadLine();
                       
                    }
                }
                return 0;
        }

        static bool CompareToList(string line, string keyColumn, string [] list, Type type)
        {
            var obj = Activator.CreateInstance(type);
            var info = type.GetMethod("Hydrate");
            if (info != null)
            {
                info.Invoke(obj, new object[] { line });
            }
            var studyPI = type.GetProperty(keyColumn);
            if (studyPI != null)
            {
                var keyColumnVal = studyPI.GetValue(obj).ToString();
                if (!list.Contains(keyColumnVal))
                {
                    return false;
                }
            }
            return true;
        }

        static void LoadQueue(Queue<string> queue,
          StreamReader file, int records)
        {
            for (int i = 0; i < records; i++)
            {
                if (file.Peek() < 0) break;
                //The Excel standard for CSV import is that you need to ignore CR/LF and comma's from within a double quoted string.  The following
                //logic will ensure that the double quoted strings are appended as one line...
                String outputString = file.ReadLine();
                while ((outputString.Split('"').Length - 1) % 2 == 1)
                {
                    outputString += "\n";
                    outputString += file.ReadLine();
                } 
                queue.Enqueue(outputString);
            }
        }
    }
}
