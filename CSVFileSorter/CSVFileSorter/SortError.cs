﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVFileSorter
{
    public class SortError
    {
        public String Item { get; set; }
        public String Error { get; set; } 

        public SortError(string item, string error)
        {
            Item = item;
            Error = error;
        }

        public override string ToString()
        {
            return String.Format("\"{0}\",{1}", Error.Replace("\r\n", "<HardReturn>").Replace("\"", "<DoubleQuote>"), Item.Replace("\r\n", "<HardReturn>").Replace("\"", "<DoubleQuote>"));
        }
    }
}
