﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVFileSorter
{
    public class Class1 : IComparable
    {
        public string MyProperty { get; set; }
        public int CompareTo(object obj)
        {
            var result = 0;
            if (obj == null) return 1;
            var other = obj as Class1;
            if (other == null) return 1;
            //result = obj.{0}.CompareTo(other.{0});
            if (result != 0) return result;
            return result;
        }

        public void Hydrate(string values)
        {
            //string[] valuearray = values.Split(',');
            //parse the string honoring the rules of double quoting strings in CSV files
            string[] valuearray = csvSplitter(values);
            if (valuearray.Length == 0) return;
            MyProperty = valuearray[0];

        }

        private string [] csvSplitter(string values)
        {
            //string pattern = @"""\s*,\s*""";
            //I got this pattern from Stack Overflow, don't ask me to explain it though.
            string pattern = @",(?=(?:[^""]*""[^""]*"")*[^""]*$)"; 

            // input.Substring(1, input.Length - 2) removes the first and last " from the string
            string[] tokens = System.Text.RegularExpressions.Regex.Split(
                input.Substring(1, input.Length - 2), pattern);
        }

        public override string ToString() 
        {
            var textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            try
            {
                if (ORIG_CUID.Equals(cdcuniqueid))
                {
                    ORIG_CUID = System.String.Empty;
                }
            }
            catch (System.Exception)
            {
            }
            var result = new System.Text.StringBuilder();
            if (true)
            {
                //result.Append(textInfo.ToTitleCase(MyProperty.ToLower()));
            }
            else
            {
                //result.Append(MyProperty);
            }
            //result.Append(',');
            return result.ToString;
        }
        public string ynuConvert(string ynu)
        {
            switch (ynu)
            {   
                case ynu.ToLower().Equals("yes"):
                case ynu.ToLower().Equals("y"):
                    return "Y";
                    break;
                case ynu.ToLower().Equals("no"):
                case ynu.ToLower().Equals("n"):
                    return "N";
                    break;
                case ynu.ToLower().Equals("u"):
                case ynu.ToLower().Equals("unk"):
                case ynu.ToLower().Equals("unknown"):
                    return "U";
                    break;
                default:
                    return ynu;
                    break;
            }
        }
        public string stateAbbreviationExpand(string abbr)
        {
            var states = new System.Collections.Generic.Dictionary<string, string>();

            states.Add("AL", "Alabama");
            states.Add("AK", "Alaska");
            states.Add("AZ", "Arizona");
            states.Add("AR", "Arkansas");
            states.Add("CA", "California");
            states.Add("CO", "Colorado");
            states.Add("CT", "Connecticut");
            states.Add("DE", "Delaware");
            states.Add("DC", "District of Columbia");
            states.Add("FL", "Florida");
            states.Add("GA", "Georgia");
            states.Add("HI", "Hawaii");
            states.Add("ID", "Idaho");
            states.Add("IL", "Illinois");
            states.Add("IN", "Indiana");
            states.Add("IA", "Iowa");
            states.Add("KS", "Kansas");
            states.Add("KY", "Kentucky");
            states.Add("LA", "Louisiana");
            states.Add("ME", "Maine");
            states.Add("MD", "Maryland");
            states.Add("MA", "Massachusetts");
            states.Add("MI", "Michigan");
            states.Add("MN", "Minnesota");
            states.Add("MS", "Mississippi");
            states.Add("MO", "Missouri");
            states.Add("MT", "Montana");
            states.Add("NE", "Nebraska");
            states.Add("NV", "Nevada");
            states.Add("NH", "New Hampshire");
            states.Add("NJ", "New Jersey");
            states.Add("NM", "New Mexico");
            states.Add("NY", "New York");
            states.Add("NC", "North Carolina");
            states.Add("ND", "North Dakota");
            states.Add("OH", "Ohio");
            states.Add("OK", "Oklahoma");
            states.Add("OR", "Oregon");
            states.Add("PA", "Pennsylvania");
            states.Add("RI", "Rhode Island");
            states.Add("SC", "South Carolina");
            states.Add("SD", "South Dakota");
            states.Add("TN", "Tennessee");
            states.Add("TX", "Texas");
            states.Add("UT", "Utah");
            states.Add("VT", "Vermont");
            states.Add("VA", "Virginia");
            states.Add("WA", "Washington");
            states.Add("WV", "West Virginia");
            states.Add("WI", "Wisconsin");
            states.Add("WY", "Wyoming");
            if (states.ContainsKey(abbr))
                return (states[abbr]);
            /* error handler is to return an empty string rather than throwing an exception */
            return "";
        }
        public string stateFullAbbreviate(string full)
        {
            var states = new System.Collections.Generic.Dictionary<string, string>();
            states.Add("Alabama", "AL");
            states.Add("Alaska", "AK");
            states.Add("Arizona", "AZ");
            states.Add("Arkansas", "AR");
            states.Add("California", "CA");
            states.Add("Colorado", "CO");
            states.Add("Connecticut", "CT");
            states.Add("Delaware", "DE");
            states.Add("District of Columbia", "DC");
            states.Add("Florida", "FL");
            states.Add("Georgia", "GA");
            states.Add("Hawaii", "HI");
            states.Add("Idaho", "ID");
            states.Add("Illinois", "IL");
            states.Add("Indiana", "IN");
            states.Add("Iowa", "IA");
            states.Add("Kansas", "KS");
            states.Add("Kentucky", "KY");
            states.Add("Louisiana", "LA");
            states.Add("Maine", "ME");
            states.Add("Maryland", "MD");
            states.Add("Massachusetts", "MA");
            states.Add("Michigan", "MI");
            states.Add("Minnesota", "MN");
            states.Add("Mississippi", "MS");
            states.Add("Missouri", "MO");
            states.Add("Montana", "MT");
            states.Add("Nebraska", "NE");
            states.Add("Nevada", "NV");
            states.Add("New Hampshire", "NH");
            states.Add("New Jersey", "NJ");
            states.Add("New Mexico", "NM");
            states.Add("New York", "NY");
            states.Add("North Carolina", "NC");
            states.Add("North Dakota", "ND");
            states.Add("Ohio", "OH");
            states.Add("Oklahoma", "OK");
            states.Add("Oregon", "OR");
            states.Add("Pennsylvania", "PA");
            states.Add("Rhode Island", "RI");
            states.Add("South Carolina", "SC");
            states.Add("South Dakota", "SD");
            states.Add("Tennessee", "TN");
            states.Add("Texas", "TX");
            states.Add("Utah", "UT");
            states.Add("Vermont", "VT");
            states.Add("Virginia", "VA");
            states.Add("Washington", "WA");
            states.Add("West Virginia", "WV");
            states.Add("Wisconsin", "WI");
            states.Add("Wyoming", "WY");
            if (states.ContainsKey(full))
                return (states[full]);
            /* error handler is to return an empty string rather than throwing an exception */
            return "";
        }

    }
}
