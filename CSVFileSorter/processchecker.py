#!/usr/bin/python

import sys
import csv
import re
from datetime import datetime
from os import listdir
from os.path import isfile, join
from os import walk

#
#
# Sort calculations
#
#

def sortkey(i):
    return str(sample_list[i]["last_accessioned"])+str(sample_list[i]["original_count"]).zfill(7)
    
# create a collection of the studies
def collectStudyList(inputFileName):
    sample_list = {}
    rowsRead = 0
    print "Reading %s " % inputFileName
    rowsRead = 0

    with open(inputFileName,'rb') as csvfile:
        
        #read the headers...
        reader = csv.reader(csvfile, delimiter = ',', quotechar='"')
        headers = reader.next()

    #    print "data headers"
    #    print headers

        #now process the data...
        for row in reader:
            rowsRead += 1
            rowVals = dict(zip(headers,row))
            study_id = rowVals["study_id"]
            full_name = rowVals["study_name"].replace("/","")
            fraction_id = rowVals["FRACTION_ID"]

            if study_id == '':
                study_id = "notSpecified"

            date_accessioned = datetime.strptime( rowVals["DATE_ACCESSIONED"], '%m/%d/%Y').year

            #do sample counts
            if study_id in sample_list:
                sample_list[study_id]["sample_count"] += 1
                if sample_list[study_id]["full_name"] != full_name:
                    print "name mismatch for study %s  %s < old ---- new > %s" & (study_id, sample_list[study_id]["full_name"], full_name)
                if date_accessioned > sample_list[study_id]["last_accessioned"]:
                    sample_list[study_id]["last_accessioned"] = date_accessioned
            else:
                sample_list[study_id] = {"sample_count": 1, "aliquot_count": 0, "original_count":0, "full_name": full_name,"last_accessioned" : date_accessioned}

            #do original and aliquot counts
            if fraction_id == "001":
                sample_list[study_id]["original_count"] += 1
            else:
                sample_list[study_id]["aliquot_count"] += 1


    print
    print
    print "%d rows read with %d unique studies" % (rowsRead, len(sample_list))
    print
    return sample_list

# gather files
def getfilelist(directoryName):
    onlyfiles = [f for f in listdir(directoryName) if isfile(join(directoryName, f))]
 
    #this regex will pull out the (study)("Sample" or "Aliquot")(count) values from the file name
    reString = '([\d\D]*)_(Sample|Aliquot)_(\d+).xlsx$'
    
    fileList = {}
    fileCount = 0
    for fn in onlyfiles:
        studAliCount = re.match(reString,fn)

        if (studAliCount):
            fileList["%s-%s" % (studAliCount.group(1).replace("/",""),studAliCount.group(2))]=studAliCount.groups()[1:]
    return fileList

# Main program
if (len(sys.argv) != 3):
    print "USAGE: processChecker.py <samplelistfile.csv> <OutputDirectory>"
    exit()

rawFileName = sys.argv[1]
directoryName = sys.argv[2]
file_list= getfilelist(directoryName)
sample_list = collectStudyList(rawFileName)


with open('studylist.csv', 'wb') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = ["study_id","study_name", "rowcount",'original_count',"aliquot_count","lastAccessionedYear","fileOriginalCount","fileAliquotCount", "match"], delimiter = ',')
    writer.writeheader()

    for study_id in sorted(sample_list.keys(), key= sortkey):
        #set outputfile created
        #set outputfile count match
        study_name = sample_list[study_id]["full_name"]
        if "%s-Sample"%study_name in file_list:
            fileSampleCount = file_list["%s-Sample"%study_name][1]
        else: 
            fileSampleCount = -1

        if "%s-Aliquot"%study_name in file_list:
            fileAliquotCount = file_list["%s-Aliquot"%study_name][1]
        else:
            fileAliquotCount = -1

        match = "false"
        if (int(fileSampleCount) == int(sample_list[study_id]["original_count"]) and int(fileAliquotCount) ==  int(sample_list[study_id]["aliquot_count"])):
            match = "True"
       
        writer.writerow ( {'study_id' : study_id ,'study_name' : study_name , 'rowcount': sample_list[study_id]["sample_count"], 'original_count': sample_list[study_id]["original_count"], "aliquot_count": sample_list[study_id]["aliquot_count"], 'lastAccessionedYear' : sample_list[study_id]["last_accessioned"], 'fileOriginalCount':fileSampleCount,'fileAliquotCount':fileAliquotCount,'match':match})
