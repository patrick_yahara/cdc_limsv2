#!/usr/bin/python

import sys
import csv
from datetime import datetime


sample_list = {}
rowsRead = 0

# Sort calculations
def sortkey(i):
    return str(sample_list[i]["last_accessioned"])+str(sample_list[i]["original_count"]).zfill(7)

# Main program
if (len(sys.argv) != 2):
    print "USAGE: studyProfiler.py <samplelistfile.txt>"
    exit()

print "Reading %s " % sys.argv[1]

with open(sys.argv[1],'rb') as csvfile:
    
    #read the headers...
    reader = csv.reader(csvfile, delimiter = ',', quotechar='"')
    headers = reader.next()

#    print "data headers"
#    print headers

    #now process the data...
    for row in reader:
        rowsRead += 1
        rowVals = dict(zip(headers,row))
        study_id = rowVals["study_id"]
        full_name = rowVals["study_name"]
        fraction_id = rowVals["FRACTION_ID"]

        if study_id == '':
            study_id = "notSpecified"

        date_accessioned = datetime.strptime( rowVals["DATE_ACCESSIONED"], '%m/%d/%Y').year

        #do sample counts
        if study_id in sample_list:
            sample_list[study_id]["sample_count"] += 1
            if sample_list[study_id]["full_name"] != full_name:
                print "name mismatch for study %s  %s < old ---- new > %s" & (study_id, sample_list[study_id]["full_name"], full_name)
            if date_accessioned > sample_list[study_id]["last_accessioned"]:
                sample_list[study_id]["last_accessioned"] = date_accessioned
        else:
            sample_list[study_id] = {"sample_count": 1, "aliquot_count": 0, "original_count":0, "full_name": full_name,"last_accessioned" : date_accessioned}

        #do original and aliquot counts
        if fraction_id == "001":
            sample_list[study_id]["original_count"] += 1
        else:
            sample_list[study_id]["aliquot_count"] += 1


print
print
print "%d rows read with %d unique studies" % (rowsRead, len(sample_list))
print
print "writing to studylist.csv..."
print


with open('studylist.csv', 'wb') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = ["study_id","study_name", "rowcount",'original_count',"aliquot_count","lastAccessionedYear"], delimiter = ',')
    writer.writeheader()
    
    for study_id in sorted(sample_list.keys(), key= sortkey):
        writer.writerow ( {'study_id' : study_id ,'study_name' : sample_list[study_id]["full_name"] , 'rowcount': sample_list[study_id]["sample_count"], 'original_count': sample_list[study_id]["original_count"], "aliquot_count": sample_list[study_id]["aliquot_count"], 'lastAccessionedYear' : sample_list[study_id]["last_accessioned"]})




