﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVUtility
{
    public class DROTweaker
    {
        public static readonly string[] _ynuColumns = new string[] { 
            "FAMILYILLNESS"
            , "COMMUNITYILLNESS"
            , "ANIMALCONTACTS" 
            , "FATAL" 
            , "Confirmed_Case" 
        };
        public static readonly string[] _upperColumns = new string[] {
            "PARENT_CUID"
            ,"ORIG_CUID"
            ,"cdcuniqueid"
        };
        public static readonly string[] _titleColumns = new string[] { 
            "ORIGIN"
            , "PID_1_ASSIGNED_BY"
            , "PID_2_ASSIGNED_BY" 
            , "CONDITION" 
            , "SPECIMEN_TAKEN_AT" 
            , "DIAGNOSIS" 
            , "ILLNESS" 
            , "EPI_TYPE" 
            , "FRACTION_TYPE"
            //, "LOCAL_TYPE_1" 
            //, "LOCAL_TYPE_2" 
            //, "LOCAL_TYPE_3" 
            , "AGE_TYPE" 
            , "SEX" 
            , "TREATMENT_1_D_B" 
            , "Age_Category_type" 
        };
        public static readonly string[] _stateColumns = new string[] { 
            "SPHL_SUB_STATE"
        };
        public static void Tweak(ref Object obj, ref List<string> rooms, ref List<string> freezers, ref List<string> boxes, ref List<string> locations, Type type)
        {
            var location = string.Empty;
            var validlocation = true;

            //V10
            //Travel_Date Foreign_Travel_Date 
            //Travel_Place FOREIGN_TRAVEL
            //Comments LAB_HISTORY_NARRATIVE

            //Age
            var piAGE = type.GetProperty("AGE");
            if (piAGE != null)
            {
                var valAGE = piAGE.GetValue(obj).ToString();
                if (valAGE.Contains("."))
                {
                    double tempage;
                    if (double.TryParse(valAGE, out tempage))
                    {
                        valAGE = ((int)Math.Truncate(tempage)).ToString();
                        piAGE.SetValue(obj, valAGE);
                    }
                }
            }

            var piDOS = type.GetProperty("DAYS_POST_ONSET");
            if (piDOS != null)
            {
                var valDOS = piDOS.GetValue(obj).ToString();
                if (valDOS.Contains("."))
                {
                    double tempdos;
                    if (double.TryParse(valDOS, out tempdos))
                    {
                        valDOS = ((int)Math.Truncate(tempdos)).ToString();
                        piDOS.SetValue(obj, valDOS);
                    }
                }
            }

            // Travel Properties
            var piFT = type.GetProperty("FOREIGN_TRAVEL");
            var piLT = type.GetProperty("LOCAL_TRAVEL");
            var piFTD = type.GetProperty("Foreign_Travel_Date");
            var piLTD = type.GetProperty("Domestic_Travel_Date");
            var piLHN = type.GetProperty("LAB_HISTORY_NARRATIVE");
            if (piFT != null && piLT != null && piFTD != null && piLTD != null && piLHN != null)
            {
                var valFT = piFT.GetValue(obj).ToString();
                var valLT = piLT.GetValue(obj).ToString();
                var valFTD = piFTD.GetValue(obj).ToString();
                var valLTD = piLTD.GetValue(obj).ToString();
                if (String.IsNullOrEmpty(valFT) && !String.IsNullOrEmpty(valLT))
                {
                    piFT.SetValue(obj, valLT);
                }
                if (String.IsNullOrEmpty(valFTD) && !String.IsNullOrEmpty(valLTD))
                {
                    piFTD.SetValue(obj, valLTD);
                }
                if (!String.IsNullOrEmpty(valFT) && !String.IsNullOrEmpty(valLT))
                {
                    var valLHN = piLHN.GetValue(obj).ToString();
                    String.Concat(valLHN, "|LOCAL_TRAVEL:", valLT);
                    piLHN.SetValue(obj, valLHN);
                }
                if (!String.IsNullOrEmpty(valFTD) && !String.IsNullOrEmpty(valLTD))
                {
                    var valLHN = piLHN.GetValue(obj).ToString();
                    String.Concat(valLHN, "|Domestic_Travel_Date:", valLTD);
                    piLHN.SetValue(obj, valLHN);
                }
            }

            //Disposed of samples set to 0 volume
            var piTransferReason = type.GetProperty("transfer_reason");
            var piVolume = type.GetProperty("QTY");
            var piSamplename = type.GetProperty("external_id");
            if (piTransferReason != null)
            {
                {
                    if (piVolume != null)
                    {
                        var valVolume = piVolume.GetValue(obj).ToString();
                        var valTransferReason = piTransferReason.GetValue(obj).ToString();
                        if (!string.IsNullOrEmpty(valVolume) || valVolume.Trim().Length == 0)
                        {
                            if (!string.IsNullOrEmpty(valTransferReason))
                            {
                                if (valTransferReason.ToUpper().Contains("DISPOSED"))
                                {
                                    String extID = piSamplename.GetValue(obj).ToString();
                                    //Console.WriteLine("Setting volume to 0 for sample {0}", extID);
                                    piVolume.SetValue(obj, "0 mL");
                                }
                            }
                        }
                    }
                }

            }
            //PROPERTY_NAME1
            var piBuilding = type.GetProperty("PROPERTY_NAME1");
            if (piBuilding != null)
            {
                var valBuilding = piBuilding.GetValue(obj).ToString();
                if (!string.IsNullOrEmpty(valBuilding))
                {
                    location = valBuilding;
                }
                else
                {
                    validlocation = false;
                }
            }
            else
            {
                validlocation = false;
            }
            // PROPERTY_NAME2 (Room)
            var piRoom = type.GetProperty("PROPERTY_NAME2");
            if (piRoom != null)
            {
                var valRoom = piRoom.GetValue(obj).ToString();
                valRoom = valRoom.Replace("(room)", String.Empty);
                valRoom = valRoom.Replace("(ROOM)", String.Empty);
                valRoom = valRoom.Replace("(Room)", String.Empty);
                valRoom = valRoom.Trim();
                if (valRoom.StartsWith("room-"))
                {
                    valRoom = valRoom.Replace("room-", "Room ");
                } else 
                if (valRoom.StartsWith("ROOM-"))
                {
                    valRoom = valRoom.Replace("ROOM-", "Room ");
                } else
                if (valRoom.StartsWith("Room-"))
                {
                    valRoom = valRoom.Replace("Room-", "Room ");
                } else

                valRoom = valRoom.Replace("room", "Room");
                valRoom = valRoom.Replace("ROOM", "Room");
                if (!String.IsNullOrEmpty(valRoom) && !valRoom.StartsWith("Room"))
                {
                    valRoom = String.Format("Room {0}", valRoom);    
                }
                piRoom.SetValue(obj, valRoom);
                if (validlocation)
                {
                    location = location + "|" + valRoom;
                }
                if (!rooms.Contains(valRoom))
                {
                    rooms.Add(valRoom);
                }
              
            }
            else
            {
                validlocation = false;
            }
            // PROPERTY_NAME3 (Freezer)
            var piFreezer = type.GetProperty("PROPERTY_NAME3");
            if (piFreezer != null)
            {
                var valFreezer = piFreezer.GetValue(obj).ToString();
                valFreezer = valFreezer.Replace("(freezer)", String.Empty);
                valFreezer = valFreezer.Replace("(Freezer)", String.Empty);
                valFreezer = valFreezer.Replace("(FREEZER)", String.Empty);
                valFreezer = valFreezer.Trim();
                if (valFreezer.StartsWith("FREEZER-"))
                {
                    valFreezer = valFreezer.Replace("FREEZER-", "Freezer ");
                } else 
                if (valFreezer.StartsWith("freezer-"))
                {
                    valFreezer = valFreezer.Replace("freezer-", "Freezer ");
                } else 
                if (valFreezer.StartsWith("Freezer-"))
                {
                    valFreezer = valFreezer.Replace("Freezer-", "Freezer ");
                }
                valFreezer = valFreezer.Replace("freezer", "Freezer");
                valFreezer = valFreezer.Replace("FREEZER", "Freezer");
                if (!String.IsNullOrEmpty(valFreezer) && !valFreezer.StartsWith("Freezer"))
                {
                    valFreezer = String.Format("Freezer {0}", valFreezer);
                }
                piFreezer.SetValue(obj, valFreezer);
                if (validlocation)
                {
                    location = location + "|" + valFreezer;
                }
                if (!freezers.Contains(valFreezer))
                {
                    freezers.Add(valFreezer);
                } 

            }
            else
            {
                validlocation = false;
            }
            // PROPERTY_NAME4 (Box)
            var piBox = type.GetProperty("PROPERTY_NAME4");
            if (piBox != null)
            {
                var valBox = piBox.GetValue(obj).ToString();
                valBox = valBox.Replace("(box)", String.Empty);
                valBox = valBox.Replace("(box 10X10)", String.Empty);
                valBox = valBox.Replace("(BOX)", String.Empty);
                valBox = valBox.Replace("(BOX 10X10)", String.Empty);
                valBox = valBox.Replace("(Box)", String.Empty);
                valBox = valBox.Replace("(Box 10X10)", String.Empty);
                valBox = valBox.Trim();
                valBox = valBox.Replace("box", "Box");
                valBox = valBox.Replace("BOX", "Box");
                piBox.SetValue(obj, valBox);
                if (validlocation)
                {
                    location = location + "|" + valBox;
                }
                if (!boxes.Contains(valBox))
                {
                    boxes.Add(valBox);
                }
            }
            else
            {
                validlocation = false;
            }

            if (validlocation && !locations.Contains(location))
            {
                locations.Add(location);
            }

            foreach (var item in _ynuColumns)
            {
                var pi = type.GetProperty(item);
                if (pi != null)
                {
                    var tempval = pi.GetValue(obj).ToString();
                    pi.SetValue(obj, tempval.ToYNU());
                }
            }
            foreach (var item in _titleColumns)
            {
                var pi = type.GetProperty(item);
                if (pi != null)
                {
                    var tempval = pi.GetValue(obj).ToString();
                    pi.SetValue(obj, tempval.ToTitleCase());
                }
            }
            foreach (var item in _stateColumns)
            {
                var pi = type.GetProperty(item);
                if (pi != null)
                {
                    var tempval = pi.GetValue(obj).ToString();
                    pi.SetValue(obj, tempval.ToStateUSPS());
                }
            }
            foreach (var item in _upperColumns)
            {
                var pi = type.GetProperty(item);
                if (pi != null)
                {
                    var tempval = pi.GetValue(obj).ToString();
                    pi.SetValue(obj, tempval.ToUpper());
                }
            }
        }
    }
}
