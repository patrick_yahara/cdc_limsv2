﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ExcelFileFactory
{
    public class ExcelRowTransformer
    {
        private readonly Type _type;
        public readonly Dictionary<int, PropertyInfo> Columns = new Dictionary<int, PropertyInfo>();
        private readonly bool _nullasempty;
        public ExcelRowTransformer(Type type, ExcelRangeBase headerrow, bool nullasempltystring = false)
        {
            _type = type;
            _nullasempty = nullasempltystring;
            var props = (from p in _type.GetProperties()
                         select new { Property = p, CustomName = p.Name.ToUpper() }).ToList();

            //Assert headerrow.Start.Row == headerrow.End.Row
            if (headerrow == null || headerrow.Start.Row != headerrow.End.Row) return;
            for (var col = headerrow.Start.Column; col <= headerrow.End.Column; col++)
            {
                var nameer = headerrow.Worksheet.Cells[headerrow.Start.Row, col];
                if (nameer == null) continue;
                var nameVal = nameer.Value;
                if (nameVal == null) continue;
                var name = nameVal.ToString();
                if (String.IsNullOrEmpty(name)) continue;
                var pi = props.Where(s => s.CustomName.Equals(name.ToUpper())).Select(s => s.Property).FirstOrDefault();
                if (pi == null) continue;
                Columns.Add(col, pi);
            }
        }

        public void SetValues(ref ExcelWorksheet sheet, int index, object row)
        {
            foreach (var item in Columns)
            {
                sheet.Cells[index, item.Key].Value = item.Value.GetValue(row).ToString();
            }
        }

    }
}