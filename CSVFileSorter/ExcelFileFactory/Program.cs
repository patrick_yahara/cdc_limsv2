﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExcelFileFactory
{
    class Program
    {
        private static readonly int _headerRowOffset = 2;
        private static readonly int _dataRowOffset = 3;
        private static readonly int _segmentLength = 250;
        private static readonly string _studyPrime = "-1QQQ";
        public static HashSet<String> studies = new HashSet<string>();
        static void Main(string[] args)
        {

            if (args.Length < 2)
            {
                Console.WriteLine("Pass in filename");
                Console.WriteLine("Pass in template");
                Console.WriteLine(@"ExcelFileFactory.exe <filename> <template>");
                Console.WriteLine("Press Enter to exit");
                Console.ReadLine();
                return;
            }
            var assemblyPath = Environment.CurrentDirectory;
            var assemblyFile = String.Format("{0}\\DynamicRowAssembly.dll", assemblyPath);
            if (!File.Exists(assemblyFile))
            {
                Console.WriteLine(@"Could not find assebly file");
                Console.WriteLine("Press Enter to exit");
                Console.ReadLine();
                return;
            }
            var dynamicAssembly = Assembly.LoadFile(assemblyFile);
            var type = dynamicAssembly.GetType("DynamicRowObject");

            if (!File.Exists(args[1]))
            {
                Console.WriteLine(@"Could not find template file");
                Console.WriteLine(@"ExcelFileFactory.exe <filename> <template>");
                Console.WriteLine("Press Enter to exit");
                Console.ReadLine();		        
                return;
            }
            var templateFile = String.Format("{0}\\{1}", assemblyPath, args[1]);
            var _templateStream = new FileStream(templateFile, FileMode.Open); 

            //var localfileinfo = new FileInfo(args[1]);
            ExcelRowTransformer transformer;


            ExcelWorksheet templateSheet;
            var templateSheetName = string.Empty;
            //using(var templateEP = new ExcelPackage(localfileinfo))
            using (var templateEP = new ExcelPackage(_templateStream))
            {
                templateSheet = templateEP.Workbook.Worksheets[1];
                templateSheetName = templateSheet.Name;
                var headerrange = templateSheet.Cells[templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.Start.Column, templateSheet.Dimension.Start.Row + _headerRowOffset, templateSheet.Dimension.End.Column];
                transformer = new ExcelRowTransformer(type, headerrange);
            }
            //foreach (var item in transformer.Columns)
            //{
            //    Console.WriteLine(String.Format("{0}", item.Key));
            //}
            var outpath = Path.Combine(Environment.CurrentDirectory, "output");
            if (!Directory.Exists(outpath))
            {
                Directory.CreateDirectory(outpath);
            }
            else 
            {
                var files = Directory.EnumerateFiles(outpath);
                foreach(string file in files)
                {
                    Console.WriteLine("Deleting {0}", file);
                    File.Delete(file);
                }
            }
            var csidPI = type.GetProperty("external_id");
            if (csidPI == null)
            {
                Console.WriteLine("Included type does not have external_id");
                Console.ReadLine();
                return;
            }

            var sl_Building_PI = type.GetProperty("PROPERTY_NAME1");
            if (sl_Building_PI == null)
            {
                Console.WriteLine("Included type does not have building");
                Console.ReadLine();
                return;
            }
            
            var sl_Room_PI = type.GetProperty("PROPERTY_NAME2");
            if (sl_Room_PI == null)
            {
                Console.WriteLine("Included type does not have room");
                Console.ReadLine();
                return;
            }

            var sl_Freezer_PI = type.GetProperty("PROPERTY_NAME3");
            if (sl_Freezer_PI == null)
            {
                Console.WriteLine("Included type does not have freezer");
                Console.ReadLine();
                return;
            }


            var sl_Box_PI = type.GetProperty("PROPERTY_NAME4");
            if (sl_Box_PI == null)
            {
                Console.WriteLine("Included type does not have box");
                Console.ReadLine();
                return;
            }

            var sl_position_PI = type.GetProperty("CUID_POSITION");
            if (sl_position_PI == null)
            {
                Console.WriteLine("Included type does not have CUID_POSITION");
                Console.ReadLine();
                return;
            }

            var sl_transferReason_PI = type.GetProperty("transfer_reason");
            if (sl_transferReason_PI == null)
            {
                Console.WriteLine("Included type does not have transfer reason");
                Console.ReadLine();
                return;
            }

            var sl_qty_PI = type.GetProperty("QTY");
            if (sl_qty_PI == null)
            {
                Console.WriteLine("Included type does not have QTY");
                Console.ReadLine();
                return;
            }

            var cuidPI = type.GetProperty("cdcuniqueid");
            if (cuidPI == null)
            {
                Console.WriteLine("Included type does not have cdcuniqueid");
                Console.ReadLine();
                return;
            }

            var parentPI = type.GetProperty("ORIG_CUID");
            if (parentPI == null)
            {
                Console.WriteLine("Included type does not have ORIG_CUID");
                Console.ReadLine();
                return;
            }

            var studyPI = type.GetProperty("study_name");
            if (studyPI == null)
            {
                Console.WriteLine("Included type does not have study_name");
                Console.ReadLine();
                return;
            }

            ExcelPackage sampleEP = null;
            ExcelPackage aliquotEP = null;
            ExcelWorksheet sheetS = null;
            ExcelWorksheet sheetA = null;
            MemoryStream msS = null;
            MemoryStream msA = null;
            var fileindex = 0;
            var currentrow = 0;
            var nextfilesplit = _segmentLength;

            var samplerow = _dataRowOffset;
            var aliquotrow = _dataRowOffset;

            var lastCSID = "-1";
            var lastCUID = "-1";
            var lastParentCUID = "-1";
            var lastStudy = _studyPrime;
            try {

            string SL_fn = String.Format(@"{0}\output\StorageLocations.csv", Environment.CurrentDirectory);
            StreamWriter StorageLocationsStream = new StreamWriter(SL_fn);
            StorageLocationsStream.WriteLine("currentStudy,currentCSID, currentCUID, currentParentCUID, storageLocation_BLDG, storageLocation_Room, storageLocation_Freezer, storageLocation_Box, storageLocation_Position,transferReason,qty ");
            using (StreamReader sr = new StreamReader(args[0]))
            {
                string valueString;
                while ((valueString =sr.ReadLine()) != null)
                {
                    //Compress quoted strings into the current line
                    while ((valueString.Split('"').Length - 1) % 2 == 1)
                    {
                        valueString += "\n";
                        valueString += sr.ReadLine();
                    }

                    var obj = Activator.CreateInstance(type);
                    var info = type.GetMethod("Hydrate");
                    
                    if (info != null)
                    {
                        info.Invoke(obj, new object[] { valueString });
                    }

                    var currentStudy = studyPI.GetValue(obj).ToString();

                    var storageLocation_BLDG = sl_Building_PI.GetValue(obj).ToString();
                    var storageLocation_Room = sl_Room_PI.GetValue(obj).ToString(); 
                    var storageLocation_Freezer = sl_Freezer_PI.GetValue(obj).ToString();
                    var storageLocation_Box = sl_Box_PI.GetValue(obj).ToString();
                    var storageLocation_Position = sl_position_PI.GetValue(obj).ToString();
                    var transferReason = sl_transferReason_PI.GetValue(obj).ToString();
                    var qty = sl_qty_PI.GetValue(obj).ToString();
                    
                    
                    if ( !currentStudy.Equals(lastStudy))
                    {
                        //write the last study out
                        if (!_studyPrime.Equals(lastStudy))
                        {
                            if (sampleEP != null)
                            {
                                WriteExcelFile(fileindex, lastStudy, samplerow - _dataRowOffset, "Sample", sampleEP);
                                sampleEP = null;
                                sheetS = null;
                                msS = null;
                            }

                            if (aliquotEP != null)
                            {
                                WriteExcelFile(fileindex, lastStudy, aliquotrow - _dataRowOffset, "Aliquot", aliquotEP);
                                aliquotEP = null;
                                sheetA = null;
                                msA = null;
                            }
                            Console.WriteLine(String.Format("Created file number {0}", fileindex));
                        }

                        //create new memory streams
                        msS = new MemoryStream();
                        sampleEP = new ExcelPackage(msS, _templateStream);
                        //sampleEP.Workbook.Worksheets.Add(templateSheetName);
                        sheetS = sampleEP.Workbook.Worksheets[1];

                        msA = new MemoryStream();
                        aliquotEP = new ExcelPackage(msA, _templateStream);
                        //aliquotEP.Workbook.Worksheets.Add(templateSheetName);
                        sheetA = aliquotEP.Workbook.Worksheets[1];
                        samplerow = _dataRowOffset;
                        aliquotrow = _dataRowOffset;

                        fileindex++;
                    }
                    studies.Add(currentStudy);

                    var currentCSID = csidPI.GetValue(obj).ToString();

                    var currentCUID = cuidPI.GetValue(obj).ToString();
                    var currentParentCUID = parentPI.GetValue(obj).ToString();

                    //either
                    //emit original sample
                    if (!currentCSID.Equals(lastCSID) && String.IsNullOrEmpty(currentParentCUID))
                    {
                        transformer.SetValues(ref sheetS, samplerow, obj);
                        currentrow++;
                        samplerow++;
                    } else
                        //emit original sample
                        if (currentCSID.Equals(lastCSID) && !currentCUID.Equals(lastCUID) && String.IsNullOrEmpty(currentParentCUID))
                        {
                            transformer.SetValues(ref sheetS, samplerow, obj);
                            currentrow++;
                            samplerow++;
                        }
                        else
                            //emit aliquot
                            if ((!currentCUID.Equals(lastCUID)) && !currentCUID.Equals(currentParentCUID) && !String.IsNullOrEmpty(currentParentCUID))
                            {
                                transformer.SetValues(ref sheetA, aliquotrow, obj);
                                aliquotrow++;
                            }
                            else
                                {
                                    Console.WriteLine("Warning: Row not a qualified sample or aliquot: %s", aliquotrow.ToString());
                                    Console.WriteLine("Press Enter to exit");
                                    Console.ReadLine();
                                }
                        

                    lastCSID = currentCSID;
                    lastCUID = currentCUID;
                    lastParentCUID = currentParentCUID;
                    lastStudy = currentStudy;

                    StorageLocationsStream.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",currentStudy, currentCSID, currentCUID, currentParentCUID, storageLocation_BLDG, storageLocation_Room, storageLocation_Freezer, storageLocation_Box, storageLocation_Position,transferReason,qty);
                    StorageLocationsStream.Flush();
                    storageLocation_BLDG = null; 
                    storageLocation_Room = null; 
                    storageLocation_Freezer = null; 
                    storageLocation_Box = null; 
                    storageLocation_Position = null;
                    obj = null;
                    GC.Collect(1,GCCollectionMode.Forced);
                }

                if (sampleEP != null)
                {
                    WriteExcelFile(fileindex, lastStudy, samplerow - _dataRowOffset, "Sample", sampleEP);
                    sampleEP = null;

                }
                if (aliquotEP != null)
                {
                    WriteExcelFile(fileindex, lastStudy, aliquotrow - _dataRowOffset, "Aliquot", aliquotEP);
                    aliquotEP = null;
                }

            }
            //Write the remaining files

            if (sampleEP != null)
            {
                WriteExcelFile(fileindex, lastStudy, samplerow - _dataRowOffset, "Sample", sampleEP);
                sampleEP = null;

            }
            if (aliquotEP != null)
            {
                WriteExcelFile(fileindex, lastStudy, aliquotrow - _dataRowOffset, "Aliquot", aliquotEP);
                aliquotEP = null;
            }

            File.WriteAllLines(string.Format(@"{0}\output\studies.txt", assemblyPath), studies);
            Console.WriteLine("Writing Storage locations file");
            StorageLocationsStream.Close();

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
                }
            catch (Exception e)
            {
                Console.WriteLine("Error found: {0}", e.ToString());
                Console.WriteLine("Press Enter to exit");
                Console.ReadLine();
            }
        }
        static public void WriteExcelFile(int fileindex, String fileName, int itemCount,string fileContents, ExcelPackage EP)
        {
            string fn = String.Format(@"{0}\output\{3}_{1}_{4}.xlsx", Environment.CurrentDirectory, fileContents, fileindex, ValidFN(fileName), itemCount);
            EP.SaveAs(new FileInfo(fn));
            Console.WriteLine("Creating {0}", fn);
        }

        static public string ValidFN(string fn)
        {
            string fileSafeStudy = fn;
            var invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            foreach (char c in invalid)
                            {
                                fileSafeStudy = fileSafeStudy.Replace(c.ToString(), String.Empty);
                            }
            return fileSafeStudy;
        }
    }
}
