USE [STARLIMS_DATA_DEV]
GO

/****** Object:  View [dbo].[vw_SASReport]    Script Date: 07/07/2015 18:28:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_SASReport]
AS
/***********************************************************************************************
	DESCRIPTION
		This view is used by the STARLIMS query engine.
             
    PARAMETERS       		
		N/A
		                 
	RETURN VALUE 
		N/A
		         
    PROGRAMMING NOTES
		
    DEPENDENCIES
		Tables used in view:
			SAMPLECONTAINERS
			ORDERS
			FOLDERS
			CENTRALRECEIVING
			PACKAGES
			ETADATA_REPOSITORY
			RASCLIENTCONTACTSEXT
			RASCLIENTS
			ORDTASK
			RESULTS
			INVENTORY
			LOCATIONS_LAYOUT
			CHAINOFCUSTODY
			LOCATIONS
			
    EXAMPLE CALL(5)
		SELECT *
		  FROM [dbo].vw_SASReport
         
    CHANGE HISTORY 
		2015-07-03 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov). Converted to a view
								  from a USP by the same name which was created by 
								  Patrick Cullen (PatrickC@yaharasoftware.com)
								  Happy Birthday Mom!
***********************************************************************************************/
WITH cteREADY
	( external_id			-- CENTRALRECEIVING		EXTERNAL_ID ++
	 ,local_package_id		-- CENTRALRECEIVING		PACKAGEID ++
	 ,SPHL_SUB_NAME			-- RASCLIENTS			COMPNAME --from packageid
	 ,SPHL_SUB_NAME2		-- RASCLIENTCONTACTSEXT UNIT --from packageid
	 ,SPHL_SUB_ADDR1		-- RASCLIENTCONTACTSEXT ADRESS --from packageid
	 ,SPHL_SUB_ADDR2		-- RASCLIENTCONTACTSEXT ADRESS_A --from packageid
	 ,SPHL_SUB_CITY			-- RASCLIENTCONTACTSEXT CITY --from packageid
	 ,SPHL_SUB_STATE		-- RASCLIENTCONTACTSEXT STATE --from packageid
	 ,SPHL_SUB_ZIP			-- RASCLIENTCONTACTSEXT ZIP --from packageid
	 ,SPHL_SUB_COUNTRY		-- RASCLIENTCONTACTSEXT COUNTRY --from packageid
	 ,SPHL_SUB_CATEGORY		-- RASCLIENTS			CATEGORY --from packageid
	 ,SPHL_SUB_FULLNAME		-- RASCLIENTCONTACTSEXT	FULLNAME (one of these will change) --from packageid
	 ,SPHL_SUB_FULLNAME1	-- RASCLIENTCONTACTSEXT	FULLNAME (one of these will change) --from packageid
	 ,SPHL_SUB_PHONE		-- RASCLIENTCONTACTSEXT	CONTACTPHONE --from packageid 
	 ,SPHL_SUB_FAX			-- RASCLIENTCONTACTSEXT	FAX --from packageid 
	 ,SPHL_SUB_EMAIL		-- RASCLIENTCONTACTSEXT	EMAIL --from packageid 
	 ,DATE_RECEIVED			-- METADATA_REPOSITORY	FIELD_DATE_02	CENTRALRECEIVING ++
	 ,DATE_ACCESSIONED		-- CENTRALRECEIVING		DATEENTERED QQQ AUDIT
	 ,DATE_COLLECTED		-- CENTRALRECEIVING		DATE_COLLECTED ++
	 ,ANIMAL_NAME			-- METADATA_REPOSITORY	FIELD_VARCHAR_144	CENTRALRECEIVING (animal type) ++
	 ,CLASS_NAME			-- METADATA_REPOSITORY	FIELD_VARCHAR_39	ORDERS (Sample Class) ++
	 ,patient_id1			-- CENTRALRECEIVING		EXTERNAL_PID1 ++ 
	 ,LOCAL_ID_1			-- METADATA_REPOSITORY	FIELD_VARCHAR_15	CENTRALRECEIVING ++ 
	 ,LOCAL_TYPE_1			-- METADATA_REPOSITORY	FIELD_VARCHAR_214	CENTRALRECEIVING (double check this) ++
	 ,PID_1_assigned_by		-- METADATA_REPOSITORY	FIELD_VARCHAR_232 (CENTRALRECEIVING?) ++
	 ,study_id				-- METADATA_REPOSITORY	FIELD_VARCHAR_15	CENTRALRECEIVING ++
	 ,study_name			-- METADATA_REPOSITORY	FIELD_VARCHAR_183	CENTRALRECEIVING (from cpl code) ++
	 ,origin				-- METADATA_REPOSITORY	FIELD_VARCHAR_01	CENTRALRECEIVING ++
	 --delta
	 -- not in data.csv
	 -- not mapped in V10
	 ,originatorid
	 -- end not in data.csv
	 -- end not mapped in V10
	 --enddelta
	 ,PATIENT_ID_2			-- CENTRALRECEIVING		EXTERNAL_PID3	CENTRALRECEIVING ++ ADDED 3 NOV
	 ,PID_2_assigned_by		-- METADATA_REPOSITORY	FIELD_VARCHAR_233 (CENTRALRECEIVING?) ++
	 -- not in data.csv
	 -- not mapped in V10
	 ,first_name
	 ,last_name
	 -- end not in data.csv
	 -- end not mapped in V10
	 ,condition				-- METADATA_REPOSITORY	FIELD_VARCHAR_16	CENTRALRECEIVING ++
	 ,SPECIMEN_TAKEN_AT		-- METADATA_REPOSITORY	FIELD_VARCHAR_03	CENTRALRECEIVING ++
	 ,SPECIMEN_TYPE			-- METADATA_REPOSITORY	FIELD_VARCHAR_17	CENTRALRECEIVING ++
	 ,SUSPAGENTCAT			-- METADATA_REPOSITORY	FIELD_VARCHAR_04	CENTRALRECEIVING ++
	 ,DIAGNOSIS				-- METADATA_REPOSITORY	FIELD_VARCHAR_08	CENTRALRECEIVING ++
	 ,ILLNESS				-- METADATA_REPOSITORY	FIELD_VARCHAR_140	CENTRALRECEIVING ++ 
	 ,FAMILYILLNESS			-- METADATA_REPOSITORY	FIELD_VARCHAR_103	CENTRALRECEIVING ++ 
	 ,FAMILY_ILLNES_DESCR	-- METADATA_REPOSITORY	FIELD_VARCHAR_22	CENTRALRECEIVING ++
	 ,COMMUNITYILLNESS		-- METADATA_REPOSITORY	FIELD_VARCHAR_104	CENTRALRECEIVING ++
	 ,COMNTY_ILLNES_DESCR	-- METADATA_REPOSITORY	FIELD_VARCHAR_23	CENTRALRECEIVING ++
	 ,EPI_TYPE				-- METADATA_REPOSITORY	FIELD_VARCHAR_213	CENTRALRECEIVING ++
	 ,LOCAL_RESIDENCE		-- METADATA_REPOSITORY	FIELD_VARCHAR_123	CENTRALRECEIVING ++
	 ,LAB_HISTORY_NARRATIVE	-- METADATA_REPOSITORY	FIELD_VARCHAR_L_42	CENTRALRECEIVING ++
	 ,LOCAL_ID_2			-- METADATA_REPOSITORY	FIELD_VARCHAR_215	CENTRALRECEIVING ++
	 ,LOCAL_TYPE_2			-- METADATA_REPOSITORY	FIELD_VARCHAR_216	CENTRALRECEIVING ++
	 ,LOCAL_ID_3			-- METADATA_REPOSITORY	FIELD_VARCHAR_217	CENTRALRECEIVING ++
	 ,LOCAL_TYPE_3			-- METADATA_REPOSITORY	FIELD_VARCHAR_218	CENTRALRECEIVING ++
	 ,SAMPLE_COLLECTED_AT	-- computated
	 /*
	 METADATA_REPOSITORY FIELD_VARCHAR_191 CENTRALRECEIVING Country
	 METADATA_REPOSITORY FIELD_VARCHAR_192 CENTRALRECEIVING State
	 METADATA_REPOSITORY FIELD_VARCHAR_193 CENTRALRECEIVING County
	 METADATA_REPOSITORY FIELD_VARCHAR_198 CENTRALRECEIVING OtherLocation
	 */
	 ,AGE					-- METADATA_REPOSITORY	FIELD_INTEGER_01	CENTRALRECEIVING ++
	 ,AGE_TYPE				-- METADATA_REPOSITORY	FIELD_VARCHAR_147	CENTRALRECEIVING ++
	 ,SEX					-- CENTRALRECEIVING		SEX ++
	 ,BIRTH_DATE			-- CENTRALRECEIVING		BIRTH_DATE ++ ADDED 3 NOV
	 ,DATE_OF_ONSET			-- CENTRALRECEIVING		ONSET_DATE ++ ADDED 3 NOV
	 ,DAYS_ON_SET			-- METADATA_REPOSITORY	FIELD_INTEGER_02	CENTRALRECEIVING ++
	 ,FATAL					-- METADATA_REPOSITORY	FIELD_VARCHAR_136	CENTRALRECEIVING ++ 
	 ,FOREIGN_TRAVEL		-- METADATA_REPOSITORY	FIELD_VARCHAR_211	CENTRALRECEIVING ++
	 -- removed from import
	 ,LOCAL_TRAVEL
	 -- end removed from import
	 ,TREATMENT_1_T			-- METADATA_REPOSITORY	FIELD_VARCHAR_125	CENTRALRECEIVING ++
	 ,TREATMENT_1_D_B		-- METADATA_REPOSITORY	FIELD_DATE_09		CENTRALRECEIVING ++
	 ,TREATMENT_1_D_C		-- METADATA_REPOSITORY	FIELD_DATE_10		CENTRALRECEIVING ++
	 ,IMMUNIZATION_1_T		-- METADATA_REPOSITORY	FIELD_VARCHAR_173	CENTRALRECEIVING ++

	 --delta
	 ,IMMUNIZATION_1_D		-- METADATA_REPOSITORY	FIELD_DATE_14		CENTRALRECEIVING ++
	 ,IMMUNIZATION_2_D		-- METADATA_REPOSITORY	FIELD_DATE_06		CENTRALRECEIVING ++
	 ,IMMUNIZATION_2_T		-- METADATA_REPOSITORY	FIELD_VARCHAR_231	CENTRALRECEIVING ++
	 --enddelta

	 -- not in data.csv
	 -- not mapped in V10
	 ,ORIG_SUB_ID
	 ,ORIG_SUB_NAME
	 ,ORIG_SUB_NAME2
	 ,ORIG_SUB_CATEGORY
	 ,ORIG_SUB_ADDR1
	 ,ORIG_SUB_ADDR2
	 ,ORIG_SUB_CITY
	 ,ORIG_SUB_STATE
	 ,ORIG_SUB_PROVINCE
	 ,ORIG_SUB_ZIP
	 -- end not in data.csv
	 -- end not mapped in V10
	
	 -- not mapped in V10
	 ,ORIG_SUB_COUNTRY
	 -- end not mapped in V10

	 -- not in data.csv
	 -- not mapped in V10
	 ,ORIG_SUB_CONTACTID
	 ,ORIG_SUB_FULLNAME1
	 ,ORIG_SUB_EMAIL
	 ,ORIG_SUB_FAX
	 ,ORIG_SUB_PHONE
	 ,SPHL_SPECIMENID
	 ,SPHL_PatientID
	 ,ORIG_SPECIMENID
	 ,ORIG_PatientID
	 -- end not in data.csv
	 -- end not mapped in V10

	 -- not in data.csv
	 -- not mapped inV10
	 ,date_lab_received
	 -- end not mapped in V10
	 -- end not in data.csv

	 -- not in data.csv
	 -- not mapped in V10
	 ,COP_Subject
	 ,Date_Scanned
	 -- end not mapped in V10
	 -- end not in data.csv

	 -- not in data.csv
	 -- not mapped in V10
	 ,Date_of_Exposure
	 ,Total_Sample_Volume
	 -- end not mapped in V10
	 -- end not in data.csv

	 ,Cabin_no				-- METADATA_REPOSITORY	FIELD_VARCHAR_226	CENTRALRECEIVING ++
	 ,Case_Contact			-- METADATA_REPOSITORY	FIELD_VARCHAR_222	CENTRALRECEIVING ++
	 ,Case_Contact_Type		-- METADATA_REPOSITORY	FIELD_VARCHAR_223	CENTRALRECEIVING ++
	 ,Confirmed_Case		-- METADATA_REPOSITORY	FIELD_VARCHAR_221	CENTRALRECEIVING ++

	 --removed from import
	 ,Domestic_Travel_Date
	 -- end removed from import

	 ,Flight_NO				-- METADATA_REPOSITORY	FIELD_VARCHAR_225	CENTRALRECEIVING ++
	 ,Flu_Substudy			-- METADATA_REPOSITORY	FIELD_VARCHAR_220	CENTRALRECEIVING ++
	 ,Foreign_Travel_Date	-- METADATA_REPOSITORY	FIELD_DATE_03		CENTRALRECEIVING ++
	 ,Household_ID			-- METADATA_REPOSITORY	FIELD_VARCHAR_224	CENTRALRECEIVING ++
	 ,ILI_Symptoms			-- METADATA_REPOSITORY	FIELD_VARCHAR_230	CENTRALRECEIVING ++
	 ,Seat_no				-- METADATA_REPOSITORY	FIELD_VARCHAR_227	CENTRALRECEIVING ++
	 ,Age_Category			-- METADATA_REPOSITORY	FIELD_VARCHAR_228	CENTRALRECEIVING ++
	 ,Age_Category_Type		-- METADATA_REPOSITORY	FIELD_VARCHAR_229	CENTRALRECEIVING ++

	 -- not in data.csv
	 -- not mapped in V10
	 ,fraction_id
	 -- end not mapped in V10
	 -- end not in data.csv

	 ,cdcuniqueid			-- SAMPLECONTAINERS		CONTAINERID
	 ,qty					-- METADATA_REPOSITORY	FIELD_VARCHAR_03	SAMPLECONTAINERS --	++ 3 NOV 2014 Change to SAMPLECONTAINERS AMOUNT
	 -- not in data.csv
	 ,CDCLOCALID			-- SAMPLECONTAINERS		CDCLOCALID
	 -- end not in data.csv
	 ,property_name1		-- SAMPLECONTAINERS		LOCATION_CODE LOCATIONS LOCATION_TYPE  'Building' ++
	 ,property_name2		-- SAMPLECONTAINERS		LOCATION_CODE LOCATIONS LOCATION_TYPE  'Room' ++
	 ,property_name3		-- SAMPLECONTAINERS		LOCATION_CODE LOCATIONS LOCATION_TYPE  'Freezer' ++
	 ,property_name4		-- SAMPLECONTAINERS		LOCATION_CODE LOCATIONS LOCATION_TYPE  'Box' ++
	 ,FRACTION_TYPE			-- METADATA_REPOSITORY	FIELD_VARCHAR_04	SAMPLECONTAINERS ++
	 ,cuid_position			-- DERIVED FROM SAMPLECONTAINERS LOCATION_CODE LOCATIONS LOCATION_TYPE  'Box' LOCATIONS_LAYOUT ++
	 ,transfer_reason		-- CHAINOFCUSTODY		REASONFORMOVER ++
	 ,location_path			-- BUILD FROM SAMPLECONTAINERS LOCATION_CODE  ++

	 -- not in data.csv
	 -- not mapped in V10
	 ,STATUS
	 ,operation_code
	 -- end not mapped in V10
	 -- end not in data.csv

	 -- not in data.csv
	 -- not mapped in V10
	 ,storage_location
	 ,Type_of_Sample
	 ,Date_Sent_to_CDC
	 -- end not mapped in V10
	 -- end not in data.csv

	 -- not in data.csv
	 -- not mapped in V10
	 ,Submitter
	 -- end not in data.csv
	 -- end not mapped in V10

	 ,Sub_Study				-- METADATA_REPOSITORY	FIELD_VARCHAR_220	CENTRALRECEIVING (FluSubstudy_
	
	 ,testcode				-- RESULTS				TESTCODE
	 ,testno				-- RESULTS				TESTNO
	 ,analyte				-- RESULTS				ANALYTE
	 ,numres				-- RESULTS				NUMRES
	 ,rn1					-- RESULTS				RN1
	 ,rn2					-- RESULTS				RN2
	 ,rn3					-- RESULTS				RN3
	 ,rn4					-- RESULTS				RN4
	 ,rn5					-- RESULTS				RN5
	 ,rn6					-- RESULTS				RN6
	 ,rn7					-- RESULTS				RN7
	 ,rn8					-- RESULTS				RN8
	 ,rn9					-- RESULTS				RN9
	 ,rn10					-- RESULTS				RN10
	 ,rep					-- RESULTS 				REP
	 ,ai					-- RESULTS				AI
	 -- not in data.csv
	 -- not mapped in V10
	 ,special_condition
	 -- end not in data.csv
	 -- end not mapped in V10

	 ,DATEENTER				-- RESULTS				DATEENTER
	 ,FOLDERNO				-- SAMPLECONTAINERS		FOLDERNO
	 ,ORDNO					-- SAMPLECONTAINERS		ORDNO
	 ,RUNNO					-- RESULTS				RUNNO
	 ,VIRUS_NAME			-- RESULTS				RN10
	 ,VIRUS_LOT				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_07
	 ,VIRUS_DIL				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_08
	 ,VC_MEDIAN				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_11
	 ,CC_MEDIAN				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_12
	 ,CUTOFF				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_13
	 ,OD1					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_14
	 ,OD2					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_15
	 ,OD3					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_16
	 ,OD4					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_17
	 ,OD5					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_18
	 ,OD6					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_19
	 ,OD7					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_20
	 ,OD8					-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_21
	 ,COMMENT				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_22

	 ,ABSORB_TYPE			-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_06
	 ,ADSORB_ANTIGEN		-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_05
	 ,BEST_RSQ				-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_10
	 ,ERROR_CODE			-- METADATA_REPOSITORY	RESULTS	FIELD_VARCHAR_09
	 
	 ,SAMPLECONTAINERS_ORIGREC	-- SAMPLECONTAINERS ORIGREC
     )
    AS 
	( SELECT D.EXTERNAL_ID
	        ,D.PACKAGEID
	        ,H.COMPNAME
	        ,G.UNIT
			,G.ADRESS
	  		,G.ADRESS_A
	  		,G.CITY
	  		,G.STATE
	  		,G.ZIP
			,G.COUNTRY
	  		,H.CATEGORY
	  		,G.FULLNAME 
			,G.FULLNAME 
			,G.CONTACTPHONE 
			,G.FAX 
			,G.EMAIL 
			,I.FIELD_DATE_02	
			,D.DATEENTERED
			,D.DATE_COLLECTED
			,I.FIELD_VARCHAR_144	
			,J.FIELD_VARCHAR_39	
			,D.EXTERNAL_PID1
			,I.FIELD_VARCHAR_15	
			,I.FIELD_VARCHAR_214	
			,I.FIELD_VARCHAR_232 
			,I.FIELD_VARCHAR_15
			,I.FIELD_VARCHAR_183
			,I.FIELD_VARCHAR_01
			,NULL						-- ORIGINATORID
			,D.EXTERNAL_PID3	
			,I.FIELD_VARCHAR_233 
			,NULL						-- FIRST_NAME
			,NULL						-- LAST_NAME
			,I.FIELD_VARCHAR_16	
			,I.FIELD_VARCHAR_03	
			,I.FIELD_VARCHAR_17	
			,I.FIELD_VARCHAR_04	
			,I.FIELD_VARCHAR_08	
			,I.FIELD_VARCHAR_140	
			,I.FIELD_VARCHAR_103	
			,I.FIELD_VARCHAR_22	
			,I.FIELD_VARCHAR_104	
			,I.FIELD_VARCHAR_23	
			,I.FIELD_VARCHAR_213	
			,I.FIELD_VARCHAR_123	
			,I.FIELD_VARCHAR_L_42	
			,I.FIELD_VARCHAR_215	
			,I.FIELD_VARCHAR_216	
			,I.FIELD_VARCHAR_217	
			,I.FIELD_VARCHAR_218	
			,CASE 
				WHEN COALESCE(I.FIELD_VARCHAR_191,'') + COALESCE(I.FIELD_VARCHAR_192,'') + COALESCE(I.FIELD_VARCHAR_193,'') + COALESCE(I.FIELD_VARCHAR_198,'') = '' THEN NULL
				ELSE COALESCE(I.FIELD_VARCHAR_191, '') + ' | ' + COALESCE(I.FIELD_VARCHAR_192, '') + ' | ' + COALESCE(I.FIELD_VARCHAR_193, '') + ' | ' + COALESCE(I.FIELD_VARCHAR_198, '')
			 END
			,I.FIELD_INTEGER_01	
			,I.FIELD_VARCHAR_147	
			,D.SEX
			,D.BIRTH_DATE
			,D.ONSET_DATE
			,I.FIELD_INTEGER_02	
			,I.FIELD_VARCHAR_136	
			,I.FIELD_VARCHAR_211
			,NULL						-- LOCAL_TRAVEL
			,I.FIELD_VARCHAR_125	
			,I.FIELD_DATE_9	
			,I.FIELD_DATE_10	
			,I.FIELD_VARCHAR_173	
			,I.FIELD_DATE_14	
			,I.FIELD_DATE_6	
			,I.FIELD_VARCHAR_231
			,NULL						-- ORIG_SUB_ID
			,NULL						-- ORIG_SUB_NAME
			,NULL						-- ORIG_SUB_NAME2
			,NULL						-- ORIG_SUB_CATEGORY
			,NULL						-- ORIG_SUB_ADDR1
			,NULL						-- ORIG_SUB_ADDR2
			,NULL						-- ORIG_SUB_CITY
			,NULL						-- ORIG_SUB_STATE
			,NULL						-- ORIG_SUB_PROVINCE
			,NULL						-- ORIG_SUB_ZIP
			,NULL						-- ORIG_SUB_COUNTRY
			,NULL						-- ORIG_SUB_CONTACTID
			,NULL						-- ORIG_SUB_FULLNAME1
			,NULL						-- ORIG_SUB_EMAIL
			,NULL						-- ORIG_SUB_FAX
			,NULL						-- ORIG_SUB_PHONE
			,NULL						-- SPHL_SPECIMENID
			,NULL						-- SPHL_PatientID
			,NULL						-- ORIG_SPECIMENID
			,NULL						-- ORIG_PatientID
			,NULL						-- date_lab_received
			,NULL						-- COP_Subject
			,NULL						-- Date_Scanned
			,NULL						-- Date_of_Exposure
			,NULL						-- Total_Sample_Volume
			,I.FIELD_VARCHAR_226	
			,I.FIELD_VARCHAR_222	
			,I.FIELD_VARCHAR_223	
			,I.FIELD_VARCHAR_221	
			,NULL						-- Domestic_Travel_Date
			,I.FIELD_VARCHAR_225	
			,I.FIELD_VARCHAR_220	
			,I.FIELD_DATE_03	
			,I.FIELD_VARCHAR_224
			,I.FIELD_VARCHAR_230
			,I.FIELD_VARCHAR_227	
			,I.FIELD_VARCHAR_228	
			,I.FIELD_VARCHAR_229	
			,NULL						-- fraction_id
			,A.CONTAINERID
			,A.AMOUNT					--	++ 3 NOV 2014 Change to SAMPLECONTAINERS AMOUNT
			,A.CDCLOCALID			
			,NULL						-- property_name1
			,NULL						-- property_name2
			,NULL						-- property_name3
			,NULL						-- property_name4
			,K.FIELD_VARCHAR_04	
			,LL.CELLTEXT
			,COC.REASONFORMOVER
			,NULL						-- location_path
			,NULL						-- STATUS
			,NULL						-- operation_code
			,NULL						-- storage_location
			,NULL						-- Type_of_Sample
			,NULL						-- Date_Sent_to_CDC
			,NULL						-- Submitter
			,I.FIELD_VARCHAR_220	
			,M.TESTCODE
			,M.TESTNO
			,M.ANALYTE
			,M.NUMRES
			,M.RN1
			,M.RN2
			,M.RN3
			,M.RN4
			,M.RN5
			,M.RN6
			,M.RN7
			,M.RN8
			,M.RN9
			,M.RN10
			,M.REP
			,M.AI
			,NULL						-- special_condition
			,M.DATEENTER
			,A.FOLDERNO
			,A.ORDNO
			,M.RUNNO
			,M.RN10
			,N.FIELD_VARCHAR_07
			,N.FIELD_VARCHAR_08
			,N.FIELD_VARCHAR_11
			,N.FIELD_VARCHAR_12
			,N.FIELD_VARCHAR_13
			,N.FIELD_VARCHAR_14
			,N.FIELD_VARCHAR_15
			,N.FIELD_VARCHAR_16
			,N.FIELD_VARCHAR_17
			,N.FIELD_VARCHAR_18
			,N.FIELD_VARCHAR_19
			,N.FIELD_VARCHAR_20
			,N.FIELD_VARCHAR_21
			,N.FIELD_VARCHAR_22
			,N.FIELD_VARCHAR_06
			,N.FIELD_VARCHAR_05
			,N.FIELD_VARCHAR_10
			,N.FIELD_VARCHAR_09
			,A.ORIGREC
		FROM SAMPLECONTAINERS A WITH (NOLOCK)
			 JOIN ORDERS B WITH (NOLOCK)
			   ON A.ORDNO = B.ORDNO
			 JOIN FOLDERS C WITH (NOLOCK)
			   ON B.FOLDERNO = C.FOLDERNO
			 JOIN CENTRALRECEIVING D WITH (NOLOCK)
			   ON C.EXTERNAL_ID = D.EXTERNAL_ID
				  AND D.DEPT = 'IPB'
			 JOIN PACKAGES E WITH (NOLOCK)
			   ON D.PACKAGEID = E.PACKAGEID
			 JOIN METADATA_REPOSITORY F WITH (NOLOCK)
			   ON F.ORIGINAL_ORIGREC = E.ORIGREC
				  AND F.TABLEREFERENCE = 'PACKAGES'
			 JOIN RASCLIENTCONTACTSEXT G WITH (NOLOCK)
			   ON G.CONTACTID = F.FIELD_VARCHAR_02
			 JOIN RASCLIENTS H WITH (NOLOCK)
			   ON H.RASCLIENTID = G.RASCLIENTID
			 JOIN METADATA_REPOSITORY I WITH (NOLOCK)
			   ON I.ORIGINAL_ORIGREC = D.ORIGREC
				  AND I.TABLEREFERENCE = 'CENTRALRECEIVING'
			 JOIN METADATA_REPOSITORY J WITH (NOLOCK)
			   ON J.ORIGINAL_ORIGREC = B.ORIGREC
				  AND J.TABLEREFERENCE = 'ORDERS'
			 JOIN METADATA_REPOSITORY K WITH (NOLOCK)
			   ON K.ORIGINAL_ORIGREC = A.ORIGREC
				  AND K.TABLEREFERENCE = 'SAMPLECONTAINERS'
			 LEFT OUTER JOIN ORDTASK L WITH (NOLOCK)
			   ON L.CONTAINERID = A.CONTAINERID
				  AND L.ORDNO = A.ORDNO
				  AND L.FOLDERNO = A.FOLDERNO
			 LEFT OUTER JOIN RESULTS M WITH (NOLOCK)
			   ON M.ORDNO = L.ORDNO
				  AND M.FOLDERNO = L.FOLDERNO
				  AND M.CONTAINERID = L.CONTAINERID
			 LEFT JOIN METADATA_REPOSITORY N WITH (NOLOCK)
			   ON N.ORIGINAL_ORIGREC = M.ORIGREC
				  AND N.TABLEREFERENCE = 'RESULTS'
			 JOIN INVENTORY AS INV WITH (NOLOCK)
				  ON INV.INVENTORYID = A.INVENTORYID
			 JOIN LOCATIONS_LAYOUT AS LL WITH (NOLOCK)
				  ON LL.INVENTORYID = INV.INVENTORYID
					 AND LL.LOCATION_ID = A.LOCATION_CODE
			 JOIN ( SELECT MAX(ORIGREC) AS MAX_ORIGREC
						  ,CONTAINERID
						  ,LOCATION_CODE
					  FROM CHAINOFCUSTODY WITH (NOLOCK)
					 GROUP BY CONTAINERID
							 ,LOCATION_CODE
				  ) AS dCOC
			   ON dCOC.CONTAINERID = A.CONTAINERID 
				  AND dCOC.LOCATION_CODE = A.LOCATION_CODE
			 JOIN CHAINOFCUSTODY AS COC WITH (NOLOCK)
			   ON COC.ORIGREC = dCOC.MAX_ORIGREC
	),
	LOC_CTE 
	( SC_ORIGREC
	  ,ORIGREC
	  ,DEPT
	  ,LOCATION
	  ,LOCATIONCODE
	  ,ORIGSTS
	  ,SERVGRP
	  ,CONDITION
	  ,STORAGEFLAG
	  ,LOCATION_DESCRIPTION
	  ,LOCATION_ID
	  ,PARENT_LOCATION_ID
	  ,LOCATION_TYPE
	  ,TOTALCAPACITY
	  ,AVAILABLECAPACITY
	  ,TRACK_FREEZE_THAW
	  ,EQUID
	  ,COPY_ORIGRE
	  ,FULLPATH
	  ,LEVEL
	) 
     AS 
	( SELECT B.ORIGREC AS SC_ORIGREC
			,C.ORIGREC
			,C.DEPT
		    ,C.LOCATION
			,C.LOCATIONCODE
			,C.ORIGSTS
			,C.SERVGRP
			,C.CONDITION
			,C.STORAGEFLAG
			,C.LOCATION_DESCRIPTION
			,C.LOCATION_ID
			,C.PARENT_LOCATION_ID
			,C.LOCATION_TYPE
			,C.TOTALCAPACITY
			,C.AVAILABLECAPACITY
			,C.TRACK_FREEZE_THAW
			,C.EQID
			,C.COPY_ORIGREC
			,C.FULLPATH
			,0 AS LEVEL 
	    FROM cteREADY A
			 JOIN SAMPLECONTAINERS B WITH (NOLOCK)
			   ON A.SAMPLECONTAINERS_ORIGREC = B.ORIGREC
			 JOIN LOCATIONS C WITH (NOLOCK)
			   ON B.LOCATION_CODE = C.LOCATIONCODE
	   UNION ALL
	  SELECT LOC_CTE.SC_ORIGREC
			,A.ORIGREC
			,A.DEPT
		    ,A.LOCATION
			,A.LOCATIONCODE
			,A.ORIGSTS
			,A.SERVGRP
			,A.CONDITION
			,A.STORAGEFLAG
			,A.LOCATION_DESCRIPTION
			,A.LOCATION_ID
			,A.PARENT_LOCATION_ID
			,A.LOCATION_TYPE
			,A.TOTALCAPACITY
			,A.AVAILABLECAPACITY
			,A.TRACK_FREEZE_THAW
			,A.EQID
			,A.COPY_ORIGREC
			,A.FULLPATH
			,LEVEL + 1 AS LEVEL
	    FROM LOC_CTE
			 JOIN LOCATIONS A WITH (NOLOCK)
			   ON A.LOCATION_ID = LOC_CTE.PARENT_LOCATION_ID
				  AND LOC_CTE.LEVEL < 10
	),
	cteLOCATION 
	( SAMPLECONTAINERS_ORIGREC
	 ,LOCATION_ID
	 ,LOCATION
	 ,LOCATION_TYPE
	 ,LEVEL
    )
    AS
    ( SELECT SC_ORIGREC
			,LOCATION_ID
			,LOCATION
			,LOCATION_TYPE
			,LEVEL
		FROM LOC_CTE
     ),
     cteGO
     AS
	( SELECT cteR.external_id
			,cteR.local_package_id
			,cteR.SPHL_SUB_NAME
			,cteR.SPHL_SUB_NAME2
			,cteR.SPHL_SUB_ADDR1
			,cteR.SPHL_SUB_ADDR2
			,cteR.SPHL_SUB_CITY
			,cteR.SPHL_SUB_STATE
			,cteR.SPHL_SUB_ZIP
			,cteR.SPHL_SUB_COUNTRY
			,cteR.SPHL_SUB_CATEGORY
			,cteR.SPHL_SUB_FULLNAME
			,cteR.SPHL_SUB_FULLNAME1
			,cteR.SPHL_SUB_PHONE
			,cteR.SPHL_SUB_FAX
			,cteR.SPHL_SUB_EMAIL
			,cteR.DATE_RECEIVED
			,cteR.DATE_ACCESSIONED
			,cteR.DATE_COLLECTED
			,cteR.ANIMAL_NAME
			,cteR.CLASS_NAME
			,cteR.patient_id1
			,cteR.LOCAL_ID_1
			,cteR.LOCAL_TYPE_1
			,cteR.PID_1_assigned_by
			,cteR.study_id
			,cteR.study_name
			,cteR.origin
			,cteR.originatorid
			,cteR.PATIENT_ID_2
			,cteR.PID_2_assigned_by
			,cteR.first_name
			,cteR.last_name
			,cteR.condition
			,cteR.SPECIMEN_TAKEN_AT
			,cteR.SPECIMEN_TYPE
			,cteR.SUSPAGENTCAT
			,cteR.DIAGNOSIS
			,cteR.ILLNESS
			,cteR.FAMILYILLNESS
			,cteR.FAMILY_ILLNES_DESCR
			,cteR.COMMUNITYILLNESS
			,cteR.COMNTY_ILLNES_DESCR
			,cteR.EPI_TYPE
			,cteR.LOCAL_RESIDENCE
			,cteR.LAB_HISTORY_NARRATIVE
			,cteR.LOCAL_ID_2
			,cteR.LOCAL_TYPE_2
			,cteR.LOCAL_ID_3
			,cteR.LOCAL_TYPE_3
			,cteR.SAMPLE_COLLECTED_AT
			,cteR.AGE
			,cteR.AGE_TYPE
			,cteR.SEX
			,cteR.BIRTH_DATE
			,cteR.DATE_OF_ONSET
			,cteR.DAYS_ON_SET
			,cteR.FATAL
			,cteR.FOREIGN_TRAVEL
			,cteR.LOCAL_TRAVEL
			,cteR.TREATMENT_1_T
			,cteR.TREATMENT_1_D_B
			,cteR.TREATMENT_1_D_C
			,cteR.IMMUNIZATION_1_T
			,cteR.IMMUNIZATION_1_D
			,cteR.IMMUNIZATION_2_D
			,cteR.IMMUNIZATION_2_T
			,cteR.ORIG_SUB_ID
			,cteR.ORIG_SUB_NAME
			,cteR.ORIG_SUB_NAME2
			,cteR.ORIG_SUB_CATEGORY
			,cteR.ORIG_SUB_ADDR1
			,cteR.ORIG_SUB_ADDR2
			,cteR.ORIG_SUB_CITY
			,cteR.ORIG_SUB_STATE
			,cteR.ORIG_SUB_PROVINCE
			,cteR.ORIG_SUB_ZIP
			,cteR.ORIG_SUB_COUNTRY
			,cteR.ORIG_SUB_CONTACTID
			,cteR.ORIG_SUB_FULLNAME1
			,cteR.ORIG_SUB_EMAIL
			,cteR.ORIG_SUB_FAX
			,cteR.ORIG_SUB_PHONE
			,cteR.SPHL_SPECIMENID
			,cteR.SPHL_PatientID
			,cteR.ORIG_SPECIMENID
			,cteR.ORIG_PatientID
			,cteR.date_lab_received
			,cteR.COP_Subject
			,cteR.Date_Scanned
			,cteR.Date_of_Exposure
			,cteR.Total_Sample_Volume
			,cteR.Cabin_no
			,cteR.Case_Contact
			,cteR.Case_Contact_Type
			,cteR.Confirmed_Case
			,cteR.Domestic_Travel_Date
			,cteR.Flight_NO
			,cteR.Flu_Substudy
			,cteR.Foreign_Travel_Date
			,cteR.Household_ID
			,cteR.ILI_Symptoms
			,cteR.Seat_no
			,cteR.Age_Category
			,cteR.Age_Category_Type
			,cteR.fraction_id
			,cteR.cdcuniqueid
			,cteR.qty
			,cteR.CDCLOCALID
			,dLocation.Building AS property_name1
			,dLocation.Room AS property_name2
			,dLocation.Freezer AS property_name3
			,dLocation.Box AS property_name4
			,cteR.FRACTION_TYPE
			,cteR.cuid_position
			,cteR.transfer_reason
			,CASE 
				WHEN dLocation.Building IS NOT NULL AND dLocation.Building <> '' THEN dLocation.Building + '-->'
				ELSE ''
			 END + 
			 CASE 
				WHEN dLocation.Room IS NOT NULL AND dLocation.Room <> '' THEN dLocation.Room + '-->' 
				ELSE ''
			 END +
			 CASE 
				WHEN dLocation.Freezer IS NOT NULL AND dLocation.Freezer <> '' THEN + dLocation.Freezer + '-->' 
				ELSE ''
			 END +
			 CASE
				WHEN dLocation.Box IS NOT NULL AND dLocation.Box <> '' THEN dLocation.Box + '-->'
				ELSE ''
			 END + 
			 CASE
				WHEN cteR.cuid_position IS NOT NULL AND cteR.cuid_position <> '' THEN cteR.cuid_position 
				ELSE ''
			 END AS location_path
			,cteR.STATUS
			,cteR.operation_code
			,cteR.storage_location
			,cteR.Type_of_Sample
			,cteR.Date_Sent_to_CDC
			,cteR.Submitter
			,cteR.Sub_Study
			,cteR.testcode
			,cteR.testno
			,cteR.analyte
			,cteR.numres
			,cteR.rn1
			,cteR.rn2
			,cteR.rn3
			,cteR.rn4
			,cteR.rn5
			,cteR.rn6
			,cteR.rn7
			,cteR.rn8
			,cteR.rn9
			,cteR.rn10
			,cteR.rep
			,cteR.ai
			,cteR.special_condition
			,cteR.DATEENTER
			,cteR.FOLDERNO
			,cteR.ORDNO
			,cteR.RUNNO
			,cteR.VIRUS_NAME
			,cteR.VIRUS_LOT
			,cteR.VIRUS_DIL
			,cteR.VC_MEDIAN
			,cteR.CC_MEDIAN
			,cteR.CUTOFF
			,cteR.OD1
			,cteR.OD2
			,cteR.OD3
			,cteR.OD4
			,cteR.OD5
			,cteR.OD6
			,cteR.OD7
			,cteR.OD8
			,cteR.COMMENT
			,cteR.ABSORB_TYPE
			,cteR.ADSORB_ANTIGEN
			,cteR.BEST_RSQ
			,cteR.ERROR_CODE
			,cteR.SAMPLECONTAINERS_ORIGREC
		FROM cteREADY AS cteR
			 JOIN ( SELECT pvtLocation.SAMPLECONTAINERS_ORIGREC
						  ,pvtLocation.[Box (9X9) - AN] AS Box
						  ,pvtLocation.[Building] AS Building
						  ,pvtLocation.[Freezer] AS Freezer
						  ,pvtLocation.[Room] AS Room
					  FROM ( SELECT SAMPLECONTAINERS_ORIGREC
								   ,LOCATION
								   ,LOCATION_TYPE
							  FROM cteLOCATION
						   ) AS Driver
					 PIVOT ( MAX([LOCATION]) 
	 				   FOR [LOCATION_TYPE] 
						   IN ( [Box (9X9) - AN]
							   ,[Building]
							   ,[Freezer]
							   ,[Room]
							  )
						   ) AS pvtLocation
				  ) AS dLocation
			   ON cteR.SAMPLECONTAINERS_ORIGREC = dLocation.SAMPLECONTAINERS_ORIGREC
	)				  

SELECT * 
  FROM cteGO

GO


