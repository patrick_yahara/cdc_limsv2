USE STARLIMS_DATA_DEV
GO

CREATE LOGIN IPB_SAS 
    WITH PASSWORD = 'password1!';
GO

-- Creates a database user for the login created above.
CREATE USER IPB_SAS FOR LOGIN IPB_SAS;
GO