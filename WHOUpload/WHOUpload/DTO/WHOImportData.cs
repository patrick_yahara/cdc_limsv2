﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHOUploadWeb.DTO
{
    public class WHOImportData
    {
        public String Ordinal { get; set; }
        public String IsolateLaboratoryNumber { get; set; }
        public String DateSpecimenCollected { get; set; }
        public String PatientAge { get; set; }
        public String PatientSex { get; set; }
        public String PatientLocation { get; set; }
        public String ActivityExtent { get; set; }
        public String PassageHistory { get; set; }
        public String TypeSubtype { get; set; }
        public String IsPatientDeceased { get; set; }
        public String IsPatientNursingHome { get; set; }
        public String IsPatientVaccinated { get; set; }
        public String IsRecentTravel { get; set; }
        public String Remarks { get; set; }
        public String CDCID { get; set; }
        public String DateReceived { get; set; }
        public String SpecimenCondition { get; set; }
        public String SpecimenSource { get; set; }
        public String InitialStorage { get; set; }
        public String CompCDCID1 { get; set; }
        public String CompCDCID2 { get; set; }
        public String StateCountry { get; set; }
        public String Nationality { get; set; }
        public String SpecialStudy { get; set; }
        public String SenderID { get; set; }
        public String DrugResist { get; set; }
        public String LoginDate { get; set; }
        public String DateCollectedEstimated { get; set; }
    }
}