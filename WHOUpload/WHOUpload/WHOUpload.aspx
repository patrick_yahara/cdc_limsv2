﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WHOUpload.aspx.cs" Inherits="WHOUploadWeb.WHOUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WHO File Upload</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Literal id="ltrlOutput" runat="server">RAB OOF</asp:Literal>
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" />
    </div>
        <asp:Button id="btnUpload" runat="server" OnClick="btnUpload_OnClick" Text="Upload"></asp:Button>
    </form>
</body>
</html>
