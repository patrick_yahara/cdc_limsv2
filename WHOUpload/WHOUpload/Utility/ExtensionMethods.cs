﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;

namespace WHOUploadWeb.Utility
{
    public static class ExtensionMethods
    {
        public static String GetValueNS(this ExcelRangeBase erb)
        {
            return erb.Value != null ? erb.Value.ToString() : null;
        }
    }
}