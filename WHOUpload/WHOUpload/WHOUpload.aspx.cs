﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Web.UI;
using System.Xml;
using OfficeOpenXml;
using WHOUploadWeb.DTO;
using WHOUploadWeb.Utility;
using System.Runtime.Serialization.Json;

namespace WHOUploadWeb
{
    public partial class WHOUpload : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            var responses = new List<FluAccessionReturnDTO>();
            if (!FileUpload1.HasFile) return;
            // Get the name of the file to upload.
            var fileName = Server.HtmlEncode(FileUpload1.FileName);
            // Get the extension of the uploaded file.
            var extension = Path.GetExtension(fileName);

            // Allow only files with .doc or .xls extensions
            // to be uploaded.
            if ((extension != ".xlsx") && (extension != ".xls")) return;

            //var binding = new BasicHttpBinding();
            //var endpoint = new EndpointAddress(@"http://localhost:8081/services/MirthPDCTest");
            var prxy = new MirthService.DefaultAcceptMessageClient();
                    
            using (var package = new ExcelPackage(FileUpload1.PostedFile.InputStream))
            {
                var sheet = package.Workbook.Worksheets[1];
                int zzz;
                var samples = new List<WHOImportData>();
                for (var i = 1; i < sheet.Dimension.End.Row; i++)
                {
                    if (sheet.Cells[i, 1].Value != null
                        && sheet.Cells[i, 2].Value != null 
                        && !String.IsNullOrEmpty(sheet.Cells[i, 1].Value.ToString())
                        && !String.IsNullOrEmpty(sheet.Cells[i, 2].Value.ToString())
                        && int.TryParse(sheet.Cells[i, 1].Value.ToString(), out zzz)
                        )
                    {
                        var sample = new WHOImportData();
                        sample.DateCollectedEstimated = @"N";
                        sample.Ordinal = sheet.Cells[i, 1].GetValueNS();
                        sample.IsolateLaboratoryNumber = sheet.Cells[i, 2].GetValueNS();
                        var tempDT = new DateTime();
                        if (DateTime.TryParse(sheet.Cells[i, 3].GetValueNS(), out tempDT))
                        {
                            sample.DateSpecimenCollected = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                        }
                        sample.PatientAge = sheet.Cells[i, 4].GetValueNS();
                        sample.PatientSex = sheet.Cells[i, 5].GetValueNS();
                        sample.PatientLocation = sheet.Cells[i, 6].GetValueNS();
                        sample.ActivityExtent = sheet.Cells[i, 7].GetValueNS();
                        sample.PassageHistory = sheet.Cells[i, 8].GetValueNS();
                        sample.TypeSubtype = sheet.Cells[i, 9].GetValueNS();
                        sample.IsPatientDeceased = sheet.Cells[i, 10].GetValueNS();
                        sample.IsPatientNursingHome = sheet.Cells[i, 11].GetValueNS();
                        sample.IsPatientVaccinated = sheet.Cells[i, 12].GetValueNS();
                        sample.IsRecentTravel = sheet.Cells[i, 13].GetValueNS();
                        sample.Remarks = sheet.Cells[i, 14].GetValueNS();
                        sample.CDCID = sheet.Cells[i, 15].GetValueNS();
                        tempDT = new DateTime();
                        if (DateTime.TryParse(sheet.Cells[i, 16].GetValueNS(), out tempDT))
                        {
                            sample.DateReceived = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                        }
                        sample.SpecimenCondition = sheet.Cells[i, 17].GetValueNS();
                        sample.SpecimenSource = sheet.Cells[i, 18].GetValueNS();
                        sample.InitialStorage = sheet.Cells[i, 19].GetValueNS();
                        sample.CompCDCID1 = sheet.Cells[i, 20].GetValueNS();
                        sample.CompCDCID2 = sheet.Cells[i, 21].GetValueNS();
                        sample.StateCountry = sheet.Cells[i, 22].GetValueNS();
                        sample.Nationality = sheet.Cells[i, 23].GetValueNS();
                        sample.SpecialStudy = sheet.Cells[i, 24].GetValueNS();
                        var tempInt = 0;
                        if (int.TryParse(sheet.Cells[i, 25].GetValueNS(), out tempInt))
                        {
                            sample.SenderID = tempInt.ToString();
                        }
                        sample.DrugResist = sheet.Cells[i, 26].GetValueNS();
                        tempDT = new DateTime();
                        if (DateTime.TryParse(sheet.Cells[i, 27].GetValueNS(), out tempDT))
                        {
                            sample.LoginDate = String.Format(@"{0:yyyy-MM-dd}", tempDT);
                        }
                        samples.Add(sample);
                        //Debugger.Log(0, @"", String.Format(@"{0}{1}", sheet.Cells[i, 1].Value, System.Environment.NewLine));
                    }
                }
                if (samples.Count <= 0) return;
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(WHOImportData));
                var settings = new XmlWriterSettings
                    {
                        Encoding = new UnicodeEncoding(false, false),
                        Indent = true,
                        OmitXmlDeclaration = true
                    };
                foreach (var sample in samples)
                {
                    using (var textWriter = new StringWriter())
                    {
                        using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                        {
                            serializer.Serialize(xmlWriter, sample);
                            responses.Add(new FluAccessionReturnDTO(prxy.acceptMessage(textWriter.ToString())));
                        }
                    }
                }
                var outputSerializer = new DataContractJsonSerializer(typeof(FluAccessionReturnDTO));
                var output = new StringBuilder();
                foreach (var item in responses)
                {
                    using (var ms = new MemoryStream())
                    {
                        outputSerializer.WriteObject(ms, item);
                        output.AppendLine(Encoding.Default.GetString(ms.ToArray()));
                    }
                    
                }
                ltrlOutput.Text = output.ToString();
            }
        }
    }
}
