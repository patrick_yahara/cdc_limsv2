using System.Data.Entity.Infrastructure;
using Yahara.Core.Repository.EF;

namespace PanOpTESModel.Models
{
    public class PanOpTESDbContext : DomainSpecificContext
    {
        public PanOpTESDbContext(string nameOrConnectionString, DbCompiledModel compiledModel)
            : base(nameOrConnectionString, compiledModel)
        {
            Configuration.LazyLoadingEnabled = false;

        }
    } 
}
