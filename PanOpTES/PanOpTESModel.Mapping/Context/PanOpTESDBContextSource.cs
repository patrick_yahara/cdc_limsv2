using System.Data.Entity.Infrastructure;
using Yahara.Core.Repository;
using Yahara.Core.Repository.EF;

namespace PanOpTESModel.Models
{
    public class PanOpTESDbContextSource : EntityFrameworkDbContextSource<PanOpTESDbContext, DefaultContextInitializer>
    {
        public PanOpTESDbContextSource(BasicDatabaseSettings databaseSettings)
            : base(databaseSettings)
        {
        }

        public override string GetConfigurationsNamespace()
        {
            return "PanOpTESModel.Mapping.Configurations";
        }
    }
}
