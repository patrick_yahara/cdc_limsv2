using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class CountyMap : EntityTypeConfiguration<County>
    {
        public CountyMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StateCode)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.CountyCode)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CountyClass)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("County");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.StateCode).HasColumnName("StateCode");
            this.Property(t => t.CountyCode).HasColumnName("CountyCode");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CountryId).HasColumnName("CountryId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CountyClass).HasColumnName("CountyClass");

            // Relationships
            this.HasRequired(t => t.Country)
                .WithMany(t => t.Counties)
                .HasForeignKey(d => d.CountryId);
            this.HasOptional(t => t.State)
                .WithMany(t => t.Counties)
                .HasForeignKey(d => d.StateId);

        }
    }
}
