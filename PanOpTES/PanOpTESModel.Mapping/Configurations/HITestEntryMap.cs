using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class HITestEntryMap : EntityTypeConfiguration<HITestEntry>
    {
        public HITestEntryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.EntryDescription)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.EntryError)
                .HasMaxLength(250);

            this.Property(t => t.Clade)
                .HasMaxLength(90);

            // Table & Column Mappings
            this.ToTable("HITestEntry");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.EntryTypeId).HasColumnName("EntryTypeId");
            this.Property(t => t.HITestId).HasColumnName("HITestId");
            this.Property(t => t.SubsampleId).HasColumnName("SubsampleId");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.EntryDescription).HasColumnName("EntryDescription");
            this.Property(t => t.EntryError).HasColumnName("EntryError");
            this.Property(t => t.Clade).HasColumnName("Clade");
            this.Property(t => t.Titer).HasColumnName("Titer");
            this.Property(t => t.BackTitre).HasColumnName("BackTitre");
            this.Property(t => t.DoNotReport).HasColumnName("DoNotReport");

            // Relationships
            this.HasOptional(t => t.EntryType)
                .WithMany(t => t.HITestEntries)
                .HasForeignKey(d => d.EntryTypeId);
            this.HasRequired(t => t.HITest)
                .WithMany(t => t.HITestEntries)
                .HasForeignKey(d => d.HITestId);
            this.HasOptional(t => t.Subsample)
                .WithMany(t => t.HITestEntries)
                .HasForeignKey(d => d.SubsampleId);

        }
    }
}
