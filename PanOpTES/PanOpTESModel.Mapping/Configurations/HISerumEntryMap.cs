using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class HISerumEntryMap : EntityTypeConfiguration<HISerumEntry>
    {
        public HISerumEntryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Position)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SerumEntryDescription)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Clade)
                .HasMaxLength(110);

            this.Property(t => t.PassageComment)
                .HasMaxLength(70);

            this.Property(t => t.ShortDescription)
                .HasMaxLength(150);

            this.Property(t => t.ExtraDescription)
                .HasMaxLength(150);

            this.Property(t => t.Supplier)
                .HasMaxLength(150);

            this.Property(t => t.SeraName)
                .HasMaxLength(250);

            this.Property(t => t.PassageHistory)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("HISerumEntry");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.EntryTypeId).HasColumnName("EntryTypeId");
            this.Property(t => t.HITestId).HasColumnName("HITestId");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.IsNewSerum).HasColumnName("IsNewSerum");
            this.Property(t => t.IsBoosted).HasColumnName("IsBoosted");
            this.Property(t => t.IsConcentrated).HasColumnName("IsConcentrated");
            this.Property(t => t.IsPool).HasColumnName("IsPool");
            this.Property(t => t.ControlTiter).HasColumnName("ControlTiter");
            this.Property(t => t.SerumEntryDescription).HasColumnName("SerumEntryDescription");
            this.Property(t => t.Clade).HasColumnName("Clade");
            this.Property(t => t.PassageComment).HasColumnName("PassageComment");
            this.Property(t => t.ShortDescription).HasColumnName("ShortDescription");
            this.Property(t => t.ExtraDescription).HasColumnName("ExtraDescription");
            this.Property(t => t.Supplier).HasColumnName("Supplier");
            this.Property(t => t.SeraName).HasColumnName("SeraName");
            this.Property(t => t.PassageHistory).HasColumnName("PassageHistory");

            // Relationships
            this.HasMany(t => t.Serums)
                .WithMany(t => t.HISerumEntries)
                .Map(m =>
                    {
                        m.ToTable("HISerumEntrySerum");
                        m.MapLeftKey("HISerumEntryId");
                        m.MapRightKey("SerumId");
                    });

            this.HasOptional(t => t.EntryType)
                .WithMany(t => t.HISerumEntries)
                .HasForeignKey(d => d.EntryTypeId);
            this.HasRequired(t => t.HITest)
                .WithMany(t => t.HISerumEntries)
                .HasForeignKey(d => d.HITestId);

        }
    }
}
