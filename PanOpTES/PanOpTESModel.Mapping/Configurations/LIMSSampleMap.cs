using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class LIMSSampleMap : EntityTypeConfiguration<LIMSSample>
    {
        public LIMSSampleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.CDCId)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SpecimenId)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Country)
                .HasMaxLength(250);

            this.Property(t => t.State)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("LIMSSample");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.IsAvailable).HasColumnName("IsAvailable");
            this.Property(t => t.CDCId).HasColumnName("CDCId");
            this.Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            this.Property(t => t.DateCollected).HasColumnName("DateCollected");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.State).HasColumnName("State");
        }
    }
}
