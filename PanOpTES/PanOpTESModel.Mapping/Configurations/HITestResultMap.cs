using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class HITestResultMap : EntityTypeConfiguration<HITestResult>
    {
        public HITestResultMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("HITestResult");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.HITestEntryId).HasColumnName("HITestEntryId");
            this.Property(t => t.HISerumEntryId).HasColumnName("HISerumEntryId");
            this.Property(t => t.Titer).HasColumnName("Titer");
            this.Property(t => t.IsError).HasColumnName("IsError");
            this.Property(t => t.IsHomologous).HasColumnName("IsHomologous");
            this.Property(t => t.LogFold).HasColumnName("LogFold");

            // Relationships
            this.HasRequired(t => t.HISerumEntry)
                .WithMany(t => t.HITestResults)
                .HasForeignKey(d => d.HISerumEntryId);
            this.HasRequired(t => t.HITestEntry)
                .WithMany(t => t.HITestResults)
                .HasForeignKey(d => d.HITestEntryId);

        }
    }
}
