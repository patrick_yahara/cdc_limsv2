using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class SubsampleMap : EntityTypeConfiguration<Subsample>
    {
        public SubsampleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Passage)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Lot)
                .IsRequired()
                .HasMaxLength(90);

            this.Property(t => t.ContractLab)
                .HasMaxLength(20);

            this.Property(t => t.CalculatedSubtype)
                .IsRequired()
                .HasMaxLength(110);

            this.Property(t => t.SequencingStorage)
                .IsRequired()
                .HasMaxLength(90);

            this.Property(t => t.InitialStorage)
                .IsRequired()
                .HasMaxLength(90);

            this.Property(t => t.AAResAdam)
                .IsRequired()
                .HasMaxLength(110);

            this.Property(t => t.AAResNai)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.DownToVaccine)
                .IsRequired()
                .HasMaxLength(90);

            this.Property(t => t.SampleClass)
                .HasMaxLength(50);

            this.Property(t => t.ExternalCUId)
                .HasMaxLength(50);

            this.Property(t => t.ExternalSubsampleId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Subsample");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            this.Property(t => t.IsFromSubmitter).HasColumnName("IsFromSubmitter");
            this.Property(t => t.Passage).HasColumnName("Passage");
            this.Property(t => t.DateHarvested).HasColumnName("DateHarvested");
            this.Property(t => t.Lot).HasColumnName("Lot");
            this.Property(t => t.ContractLab).HasColumnName("ContractLab");
            this.Property(t => t.DateContractLabTransfer).HasColumnName("DateContractLabTransfer");
            this.Property(t => t.IsVaccine).HasColumnName("IsVaccine");
            this.Property(t => t.CalculatedSubtype).HasColumnName("CalculatedSubtype");
            this.Property(t => t.SequencingStorage).HasColumnName("SequencingStorage");
            this.Property(t => t.InitialStorage).HasColumnName("InitialStorage");
            this.Property(t => t.AAResAdam).HasColumnName("AAResAdam");
            this.Property(t => t.AAResNai).HasColumnName("AAResNai");
            this.Property(t => t.DownToVaccine).HasColumnName("DownToVaccine");
            this.Property(t => t.SampleClass).HasColumnName("SampleClass");
            this.Property(t => t.AlternateId).HasColumnName("AlternateId");
            this.Property(t => t.ExternalCUId).HasColumnName("ExternalCUId");
            this.Property(t => t.ExternalSubsampleId).HasColumnName("ExternalSubsampleId");

            // Relationships
            this.HasOptional(t => t.Specimen)
                .WithMany(t => t.Subsamples)
                .HasForeignKey(d => d.SpecimenId);

        }
    }
}
