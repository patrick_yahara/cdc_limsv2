using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class SerumMap : EntityTypeConfiguration<Serum>
    {
        public SerumMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Lot)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Ferret)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.InfectingAgent)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.BoostingAgent)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Justification)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Comment)
                .IsRequired()
                .HasMaxLength(550);

            this.Property(t => t.ShortDescription)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.ExtraDescription)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("Serum");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.Lot).HasColumnName("Lot");
            this.Property(t => t.SubsampleId).HasColumnName("SubsampleId");
            this.Property(t => t.Ferret).HasColumnName("Ferret");
            this.Property(t => t.InfectingAgent).HasColumnName("InfectingAgent");
            this.Property(t => t.BoostingAgent).HasColumnName("BoostingAgent");
            this.Property(t => t.DateInoculated).HasColumnName("DateInoculated");
            this.Property(t => t.DateBoosted).HasColumnName("DateBoosted");
            this.Property(t => t.DateHarvested).HasColumnName("DateHarvested");
            this.Property(t => t.Justification).HasColumnName("Justification");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.ShortDescription).HasColumnName("ShortDescription");
            this.Property(t => t.ExtraDescription).HasColumnName("ExtraDescription");

            // Relationships
            this.HasOptional(t => t.Subsample)
                .WithMany(t => t.Serums)
                .HasForeignKey(d => d.SubsampleId);

        }
    }
}
