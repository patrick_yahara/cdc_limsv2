using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class SpecimenMap : EntityTypeConfiguration<Specimen>
    {
        public SpecimenMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.CDCId)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SpecimenId)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.StrainName)
                .IsRequired()
                .HasMaxLength(170);

            this.Property(t => t.YearCollected)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Activity)
                .HasMaxLength(50);

            this.Property(t => t.CollectionMethod)
                .HasMaxLength(70);

            this.Property(t => t.PassageReceived)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.CollectionComment)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.ContractLab)
                .HasMaxLength(20);

            this.Property(t => t.PatientGender)
                .HasMaxLength(50);

            this.Property(t => t.PatientLocation)
                .IsRequired()
                .HasMaxLength(350);

            this.Property(t => t.SpecialStudy)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.GeneticScreen)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.LabComment)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.ReasonForSubmission)
                .IsRequired()
                .HasMaxLength(110);

            this.Property(t => t.ShippingCondition)
                .HasMaxLength(90);

            this.Property(t => t.TravelLocation)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.ReagentClass)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Specimen");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.CDCId).HasColumnName("CDCId");
            this.Property(t => t.SpecimenId).HasColumnName("SpecimenId");
            this.Property(t => t.StrainName).HasColumnName("StrainName");
            this.Property(t => t.IsDuplicate).HasColumnName("IsDuplicate");
            this.Property(t => t.IsReassortant).HasColumnName("IsReassortant");
            this.Property(t => t.IsRecombinant).HasColumnName("IsRecombinant");
            this.Property(t => t.IsNegative).HasColumnName("IsNegative");
            this.Property(t => t.IsColdAdapted).HasColumnName("IsColdAdapted");
            this.Property(t => t.IsRNAOnly).HasColumnName("IsRNAOnly");
            this.Property(t => t.IsCDNAOnly).HasColumnName("IsCDNAOnly");
            this.Property(t => t.HostId).HasColumnName("HostId");
            this.Property(t => t.DateCollected).HasColumnName("DateCollected");
            this.Property(t => t.YearCollected).HasColumnName("YearCollected");
            this.Property(t => t.DateReceived).HasColumnName("DateReceived");
            this.Property(t => t.DateContractLabTransfer).HasColumnName("DateContractLabTransfer");
            this.Property(t => t.DateLogin).HasColumnName("DateLogin");
            this.Property(t => t.IsCollectionDateEstimated).HasColumnName("IsCollectionDateEstimated");
            this.Property(t => t.Activity).HasColumnName("Activity");
            this.Property(t => t.CollectionMethod).HasColumnName("CollectionMethod");
            this.Property(t => t.CountryId).HasColumnName("CountryId");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CountyId).HasColumnName("CountyId");
            this.Property(t => t.SenderCode).HasColumnName("SenderCode");
            this.Property(t => t.PassageReceived).HasColumnName("PassageReceived");
            this.Property(t => t.CollectionComment).HasColumnName("CollectionComment");
            this.Property(t => t.IsOriginal).HasColumnName("IsOriginal");
            this.Property(t => t.IsVaccine).HasColumnName("IsVaccine");
            this.Property(t => t.ContractLab).HasColumnName("ContractLab");
            this.Property(t => t.PackageId).HasColumnName("PackageId");
            this.Property(t => t.PatientAge).HasColumnName("PatientAge");
            this.Property(t => t.PatientGender).HasColumnName("PatientGender");
            this.Property(t => t.PatientLocation).HasColumnName("PatientLocation");
            this.Property(t => t.IsPatientDeceased).HasColumnName("IsPatientDeceased");
            this.Property(t => t.IsPatientVaccinated).HasColumnName("IsPatientVaccinated");
            this.Property(t => t.IsNursingHome).HasColumnName("IsNursingHome");
            this.Property(t => t.SpecialStudy).HasColumnName("SpecialStudy");
            this.Property(t => t.GeneticScreen).HasColumnName("GeneticScreen");
            this.Property(t => t.DatePCR).HasColumnName("DatePCR");
            this.Property(t => t.LabComment).HasColumnName("LabComment");
            this.Property(t => t.ReasonForSubmission).HasColumnName("ReasonForSubmission");
            this.Property(t => t.ShippingCondition).HasColumnName("ShippingCondition");
            this.Property(t => t.IsTravel).HasColumnName("IsTravel");
            this.Property(t => t.TravelLocation).HasColumnName("TravelLocation");
            this.Property(t => t.IsReagent).HasColumnName("IsReagent");
            this.Property(t => t.ReagentClass).HasColumnName("ReagentClass");
            this.Property(t => t.SenderLabId).HasColumnName("SenderLabId");

            // Relationships
            this.HasOptional(t => t.Country)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.CountryId);
            this.HasOptional(t => t.County)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.CountyId);
            this.HasOptional(t => t.State)
                .WithMany(t => t.Specimens)
                .HasForeignKey(d => d.StateId);

        }
    }
}
