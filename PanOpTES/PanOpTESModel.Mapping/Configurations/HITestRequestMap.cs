using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class HITestRequestMap : EntityTypeConfiguration<HITestRequest>
    {
        public HITestRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.HITestEntryName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.HISerumEntryName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.ErrorMessage)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("HITestRequest");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.HITestEntryName).HasColumnName("HITestEntryName");
            this.Property(t => t.HISerumEntryName).HasColumnName("HISerumEntryName");
            this.Property(t => t.Difference).HasColumnName("Difference");
            this.Property(t => t.IsError).HasColumnName("IsError");
            this.Property(t => t.ErrorMessage).HasColumnName("ErrorMessage");
            this.Property(t => t.HITestId).HasColumnName("HITestId");
            this.Property(t => t.EntryTypeId).HasColumnName("EntryTypeId");
            this.Property(t => t.LIMSSampleId).HasColumnName("LIMSSampleId");

            // Relationships
            this.HasRequired(t => t.EntryType)
                .WithMany(t => t.HITestRequests)
                .HasForeignKey(d => d.EntryTypeId);
            this.HasRequired(t => t.HITest)
                .WithMany(t => t.HITestRequests)
                .HasForeignKey(d => d.HITestId);
            this.HasOptional(t => t.LIMSSample)
                .WithMany(t => t.HITestRequests)
                .HasForeignKey(d => d.LIMSSampleId);

        }
    }
}
