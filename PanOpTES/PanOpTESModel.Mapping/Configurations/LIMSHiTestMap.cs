using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class LIMSHiTestMap : EntityTypeConfiguration<LIMSHiTest>
    {
        public LIMSHiTestMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("LIMSHiTest");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.DateRequested).HasColumnName("DateRequested");
        }
    }
}
