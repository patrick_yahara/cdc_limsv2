using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class CountryMap : EntityTypeConfiguration<Country>
    {
        public CountryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.ISO)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.PrintableName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.ISO3)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ANSICode)
                .HasMaxLength(50);

            this.Property(t => t.HistoricalCode)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.GISAidName)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Country");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.ISO).HasColumnName("ISO");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.PrintableName).HasColumnName("PrintableName");
            this.Property(t => t.ISO3).HasColumnName("ISO3");
            this.Property(t => t.NumCode).HasColumnName("NumCode");
            this.Property(t => t.ANSICode).HasColumnName("ANSICode");
            this.Property(t => t.HistoricalCode).HasColumnName("HistoricalCode");
            this.Property(t => t.RegionId).HasColumnName("RegionId");
            this.Property(t => t.WHORegionId).HasColumnName("WHORegionId");
            this.Property(t => t.HHSRegionId).HasColumnName("HHSRegionId");
            this.Property(t => t.GISAidCode).HasColumnName("GISAidCode");
            this.Property(t => t.GISAidName).HasColumnName("GISAidName");

            // Relationships
            this.HasOptional(t => t.HHSRegion)
                .WithMany(t => t.Countries)
                .HasForeignKey(d => d.HHSRegionId);
            this.HasOptional(t => t.Region)
                .WithMany(t => t.Countries)
                .HasForeignKey(d => d.RegionId);
            this.HasOptional(t => t.WHORegion)
                .WithMany(t => t.Countries)
                .HasForeignKey(d => d.WHORegionId);

        }
    }
}
