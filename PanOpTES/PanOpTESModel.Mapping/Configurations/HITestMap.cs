using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class HITestMap : EntityTypeConfiguration<HITest>
    {
        public HITestMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.TestFileName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.TestDescription)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SubType)
                .HasMaxLength(90);

            // Table & Column Mappings
            this.ToTable("HITest");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.DateTested).HasColumnName("DateTested");
            this.Property(t => t.TestFileName).HasColumnName("TestFileName");
            this.Property(t => t.TestDescription).HasColumnName("TestDescription");
            this.Property(t => t.SubType).HasColumnName("SubType");
        }
    }
}
