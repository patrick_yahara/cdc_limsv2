using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PanOpTESModel.Mapping.Configurations
{
    public class LIMSSubsampleMap : EntityTypeConfiguration<LIMSSubsample>
    {
        public LIMSSubsampleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(64);

            this.Property(t => t.VersionStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.FlulimsSSId)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.FlulimsCCUId)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ContractLab)
                .HasMaxLength(255);

            this.Property(t => t.HaResults)
                .HasMaxLength(255);

            this.Property(t => t.Lot)
                .HasMaxLength(255);

            this.Property(t => t.Passage)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("LIMSSubsample");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.VersionStamp).HasColumnName("VersionStamp");
            this.Property(t => t.FlulimsSSId).HasColumnName("FlulimsSSId");
            this.Property(t => t.FlulimsCCUId).HasColumnName("FlulimsCCUId");
            this.Property(t => t.ContractLab).HasColumnName("ContractLab");
            this.Property(t => t.DateHarvested).HasColumnName("DateHarvested");
            this.Property(t => t.HaResults).HasColumnName("HaResults");
            this.Property(t => t.Lot).HasColumnName("Lot");
            this.Property(t => t.Passage).HasColumnName("Passage");
            this.Property(t => t.LIMSHATiterId).HasColumnName("LIMSHATiterId");
            this.Property(t => t.LIMSHiTestId).HasColumnName("LIMSHiTestId");
            this.Property(t => t.LIMSSampleId).HasColumnName("LIMSSampleId");

            // Relationships
            this.HasOptional(t => t.LIMSHATiter)
                .WithMany(t => t.LIMSSubsamples)
                .HasForeignKey(d => d.LIMSHATiterId);
            this.HasOptional(t => t.LIMSHiTest)
                .WithMany(t => t.LIMSSubsamples)
                .HasForeignKey(d => d.LIMSHiTestId);
            this.HasOptional(t => t.LIMSSample)
                .WithMany(t => t.LIMSSubsamples)
                .HasForeignKey(d => d.LIMSSampleId);

        }
    }
}
