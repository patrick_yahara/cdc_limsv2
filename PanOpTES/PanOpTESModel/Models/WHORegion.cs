using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class WHORegion : ConcreteEntityBase<int>, IAggregateRoot
    {
        public WHORegion()
        {
            Countries = new List<Country>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string Abbrev { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Country> Countries { get; set; }
    }
}
