using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HITestEntry : ConcreteEntityBase<int>, IAggregateRoot
    {
        public HITestEntry()
        {
            HITestFoldChanges = new List<HITestFoldChange>();
            HITestResults = new List<HITestResult>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public int? EntryTypeId { get; set; }
        public int HITestId { get; set; }
        public int? SubsampleId { get; set; }
        public short Position { get; set; }
        public string EntryDescription { get; set; }
        public string EntryError { get; set; }
        public string Clade { get; set; }
        public int? Titer { get; set; }
        public Nullable<short> BackTitre { get; set; }
        public bool DoNotReport { get; set; }
        public virtual EntryType EntryType { get; set; }
        public virtual HITest HITest { get; set; }
        public virtual Subsample Subsample { get; set; }
        public virtual ICollection<HITestFoldChange> HITestFoldChanges { get; set; }
        public virtual ICollection<HITestResult> HITestResults { get; set; }
    }
}
