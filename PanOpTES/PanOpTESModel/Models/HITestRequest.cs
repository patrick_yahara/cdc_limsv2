using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HITestRequest : ConcreteEntityBase<int>, IAggregateRoot
    {
        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string HITestEntryName { get; set; }
        public string HISerumEntryName { get; set; }
        public int? Difference { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public int HITestId { get; set; }
        public int EntryTypeId { get; set; }
        public int? LIMSSampleId { get; set; }
        public virtual EntryType EntryType { get; set; }
        public virtual HITest HITest { get; set; }
        public virtual LIMSSample LIMSSample { get; set; }
    }
}
