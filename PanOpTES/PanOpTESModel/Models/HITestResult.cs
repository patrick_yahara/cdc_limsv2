using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HITestResult : ConcreteEntityBase<int>, IAggregateRoot
    {
        public HITestResult()
        {
            HITestFoldChanges = new List<HITestFoldChange>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public int HITestEntryId { get; set; }
        public int HISerumEntryId { get; set; }
        public short Titer { get; set; }
        public bool IsError { get; set; }
        public bool IsHomologous { get; set; }
        public short LogFold { get; set; }
        public virtual HISerumEntry HISerumEntry { get; set; }
        public virtual HITestEntry HITestEntry { get; set; }
        public virtual ICollection<HITestFoldChange> HITestFoldChanges { get; set; }
    }
}
