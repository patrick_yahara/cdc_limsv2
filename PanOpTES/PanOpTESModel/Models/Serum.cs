using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class Serum : ConcreteEntityBase<int>, IAggregateRoot
    {
        public Serum()
        {
            HISerumEntries = new List<HISerumEntry>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string Lot { get; set; }
        public int? SubsampleId { get; set; }
        public string Ferret { get; set; }
        public string InfectingAgent { get; set; }
        public string BoostingAgent { get; set; }
        public DateTime? DateInoculated { get; set; }
        public DateTime? DateBoosted { get; set; }
        public DateTime? DateHarvested { get; set; }
        public string Justification { get; set; }
        public string Comment { get; set; }
        public string ShortDescription { get; set; }
        public string ExtraDescription { get; set; }
        public virtual Subsample Subsample { get; set; }
        public virtual ICollection<HISerumEntry> HISerumEntries { get; set; }
    }
}
