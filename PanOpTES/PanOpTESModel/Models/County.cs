using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class County : ConcreteEntityBase<int>, IAggregateRoot
    {
        public County()
        {
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string Code { get; set; }
        public string StateCode { get; set; }
        public string CountyCode { get; set; }
        public int? StateId { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string CountyClass { get; set; }
        public virtual Country Country { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }
    }
}
