using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class LIMSHATiter : ConcreteEntityBase<int>, IAggregateRoot
    {
        public LIMSHATiter()
        {
            LIMSSubsamples = new List<LIMSSubsample>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string RBCType { get; set; }
        public int? Titer { get; set; }
        public virtual ICollection<LIMSSubsample> LIMSSubsamples { get; set; }
    }
}
