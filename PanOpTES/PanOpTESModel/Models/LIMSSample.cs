using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class LIMSSample : ConcreteEntityBase<int>, IAggregateRoot
    {
        public LIMSSample()
        {
            HITestRequests = new List<HITestRequest>();
            LIMSSubsamples = new List<LIMSSubsample>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public bool IsAvailable { get; set; }
        public string CDCId { get; set; }
        public string SpecimenId { get; set; }
        public DateTime? DateCollected { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public virtual ICollection<HITestRequest> HITestRequests { get; set; }
        public virtual ICollection<LIMSSubsample> LIMSSubsamples { get; set; }
    }
}
