using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HHSRegion : ConcreteEntityBase<int>, IAggregateRoot
    {
        public HHSRegion()
        {
            Countries = new List<Country>();
            States = new List<State>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public short Code { get; set; }
        public string Name { get; set; }
        public string HHSRegionDescription { get; set; }
        public virtual ICollection<Country> Countries { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
