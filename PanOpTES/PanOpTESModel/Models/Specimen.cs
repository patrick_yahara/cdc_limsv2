using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class Specimen : ConcreteEntityBase<int>, IAggregateRoot
    {
        public Specimen()
        {
            Subsamples = new List<Subsample>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string CDCId { get; set; }
        public string SpecimenId { get; set; }
        public string StrainName { get; set; }
        public bool IsDuplicate { get; set; }
        public bool IsReassortant { get; set; }
        public bool IsRecombinant { get; set; }
        public bool IsNegative { get; set; }
        public bool IsColdAdapted { get; set; }
        public bool IsRNAOnly { get; set; }
        public bool IsCDNAOnly { get; set; }
        public int? HostId { get; set; }
        public DateTime? DateCollected { get; set; }
        public string YearCollected { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime? DateContractLabTransfer { get; set; }
        public DateTime? DateLogin { get; set; }
        public bool IsCollectionDateEstimated { get; set; }
        public string Activity { get; set; }
        public string CollectionMethod { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CountyId { get; set; }
        public int? SenderCode { get; set; }
        public string PassageReceived { get; set; }
        public string CollectionComment { get; set; }
        public bool IsOriginal { get; set; }
        public bool IsVaccine { get; set; }
        public string ContractLab { get; set; }
        public int? PackageId { get; set; }
        public int? PatientAge { get; set; }
        public string PatientGender { get; set; }
        public string PatientLocation { get; set; }
        public Nullable<bool> IsPatientDeceased { get; set; }
        public Nullable<bool> IsPatientVaccinated { get; set; }
        public Nullable<bool> IsNursingHome { get; set; }
        public string SpecialStudy { get; set; }
        public string GeneticScreen { get; set; }
        public DateTime? DatePCR { get; set; }
        public string LabComment { get; set; }
        public string ReasonForSubmission { get; set; }
        public string ShippingCondition { get; set; }
        public Nullable<bool> IsTravel { get; set; }
        public string TravelLocation { get; set; }
        public bool IsReagent { get; set; }
        public string ReagentClass { get; set; }
        public int? SenderLabId { get; set; }
        public virtual Country Country { get; set; }
        public virtual County County { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Subsample> Subsamples { get; set; }
    }
}
