using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HISerumEntry : ConcreteEntityBase<int>, IAggregateRoot
    {
        public HISerumEntry()
        {
            HITestResults = new List<HITestResult>();
            Serums = new List<Serum>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public int? EntryTypeId { get; set; }
        public int HITestId { get; set; }
        public string Position { get; set; }
        public bool IsNewSerum { get; set; }
        public bool IsBoosted { get; set; }
        public bool IsConcentrated { get; set; }
        public bool IsPool { get; set; }
        public Nullable<short> ControlTiter { get; set; }
        public string SerumEntryDescription { get; set; }
        public string Clade { get; set; }
        public string PassageComment { get; set; }
        public string ShortDescription { get; set; }
        public string ExtraDescription { get; set; }
        public string Supplier { get; set; }
        public string SeraName { get; set; }
        public string PassageHistory { get; set; }
        public virtual EntryType EntryType { get; set; }
        public virtual HITest HITest { get; set; }
        public virtual ICollection<HITestResult> HITestResults { get; set; }
        public virtual ICollection<Serum> Serums { get; set; }
    }
}
