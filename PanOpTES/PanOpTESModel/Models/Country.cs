using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class Country : ConcreteEntityBase<int>, IAggregateRoot
    {
        public Country()
        {
            Counties = new List<County>();
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string ISO { get; set; }
        public string Name { get; set; }
        public string PrintableName { get; set; }
        public string ISO3 { get; set; }
        public short NumCode { get; set; }
        public string ANSICode { get; set; }
        public string HistoricalCode { get; set; }
        public int? RegionId { get; set; }
        public int? WHORegionId { get; set; }
        public int? HHSRegionId { get; set; }
        public Nullable<short> GISAidCode { get; set; }
        public string GISAidName { get; set; }
        public virtual HHSRegion HHSRegion { get; set; }
        public virtual Region Region { get; set; }
        public virtual WHORegion WHORegion { get; set; }
        public virtual ICollection<County> Counties { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }
    }
}
