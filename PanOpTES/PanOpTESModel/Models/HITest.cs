using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HITest : ConcreteEntityBase<int>, IAggregateRoot
    {
        public HITest()
        {
            HISerumEntries = new List<HISerumEntry>();
            HITestEntries = new List<HITestEntry>();
            HITestRequests = new List<HITestRequest>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public DateTime DateTested { get; set; }
        public string TestFileName { get; set; }
        public string TestDescription { get; set; }
        public string SubType { get; set; }
        public virtual ICollection<HISerumEntry> HISerumEntries { get; set; }
        public virtual ICollection<HITestEntry> HITestEntries { get; set; }
        public virtual ICollection<HITestRequest> HITestRequests { get; set; }
    }
}
