using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class State : ConcreteEntityBase<int>, IAggregateRoot
    {
        public State()
        {
            Counties = new List<County>();
            Specimens = new List<Specimen>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string Abbrev { get; set; }
        public string Name { get; set; }
        public string ANSICode { get; set; }
        public int HHSRegionId { get; set; }
        public Nullable<short> GISAidCode { get; set; }
        public virtual ICollection<County> Counties { get; set; }
        public virtual HHSRegion HHSRegion { get; set; }
        public virtual ICollection<Specimen> Specimens { get; set; }
    }
}
