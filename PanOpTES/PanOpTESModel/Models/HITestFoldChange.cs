using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class HITestFoldChange : ConcreteEntityBase<int>, IAggregateRoot
    {
        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public int? HITestEntryId { get; set; }
        public int? HITestResultId { get; set; }
        public Nullable<short> Fold { get; set; }
        public virtual HITestEntry HITestEntry { get; set; }
        public virtual HITestResult HITestResult { get; set; }
    }
}
