using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class Subsample : ConcreteEntityBase<int>, IAggregateRoot
    {
        public Subsample()
        {
            HITestEntries = new List<HITestEntry>();
            Serums = new List<Serum>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public int? SpecimenId { get; set; }
        public bool IsFromSubmitter { get; set; }
        public string Passage { get; set; }
        public DateTime? DateHarvested { get; set; }
        public string Lot { get; set; }
        public string ContractLab { get; set; }
        public DateTime? DateContractLabTransfer { get; set; }
        public bool IsVaccine { get; set; }
        public string CalculatedSubtype { get; set; }
        public string SequencingStorage { get; set; }
        public string InitialStorage { get; set; }
        public string AAResAdam { get; set; }
        public string AAResNai { get; set; }
        public string DownToVaccine { get; set; }
        public string SampleClass { get; set; }
        public int? AlternateId { get; set; }
        public string ExternalCUId { get; set; }
        public string ExternalSubsampleId { get; set; }
        public virtual ICollection<HITestEntry> HITestEntries { get; set; }
        public virtual ICollection<Serum> Serums { get; set; }
        public virtual Specimen Specimen { get; set; }
    }
}
