using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class LIMSSubsample : ConcreteEntityBase<int>, IAggregateRoot
    {
        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string FlulimsSSId { get; set; }
        public string FlulimsCCUId { get; set; }
        public string ContractLab { get; set; }
        public DateTime? DateHarvested { get; set; }
        public string HaResults { get; set; }
        public string Lot { get; set; }
        public string Passage { get; set; }
        public int? LIMSHATiterId { get; set; }
        public int? LIMSHiTestId { get; set; }
        public int? LIMSSampleId { get; set; }
        public virtual LIMSHATiter LIMSHATiter { get; set; }
        public virtual LIMSHiTest LIMSHiTest { get; set; }
        public virtual LIMSSample LIMSSample { get; set; }
    }
}
