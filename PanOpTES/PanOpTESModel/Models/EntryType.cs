using System;
using System.Collections.Generic;
using Yahara.Core.Repository;

namespace PanOpTESModel
{
    public class EntryType : ConcreteEntityBase<int>, IAggregateRoot
    {
        public EntryType()
        {
            HISerumEntries = new List<HISerumEntry>();
            HITestEntries = new List<HITestEntry>();
            HITestRequests = new List<HITestRequest>();
        }

        public override int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte[] VersionStamp { get; set; }
        public string DisplayName { get; set; }
        public string SystemName { get; set; }
        public virtual ICollection<HISerumEntry> HISerumEntries { get; set; }
        public virtual ICollection<HITestEntry> HITestEntries { get; set; }
        public virtual ICollection<HITestRequest> HITestRequests { get; set; }
    }
}
