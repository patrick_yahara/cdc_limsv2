﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.Common.Utility
{
    public static class EFAuditFieldUtility
    {
        public static void UpdateAuditFields(dynamic efObject, bool isUpdate = false)
        {
            try
            {
                if (isUpdate)
                {
                    efObject.ModifiedOn = DateTime.UtcNow;
                    efObject.ModifiedBy = ClaimsPrincipal.Current.Identity.Name;
                }
                else
                {
                    efObject.CreatedOn = DateTime.UtcNow;
                    efObject.CreatedBy = ClaimsPrincipal.Current.Identity.Name;
                }

            }
            catch (Exception e)
            {
                throw new InvalidOperationException(
                    "Attempt to set audit fields on an object that does not contain audit fields", e);
            }
        }
    }
}
