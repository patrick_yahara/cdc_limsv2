﻿using System;

namespace PanOpTES.Common
{
    public class PanOpTESConstants
    {
        public struct EntryTypes
        {
            public const String reference = "REFERENCE";
            public const String test = "TEST";
            public const String st_ctrl = "STCTRL";
            public const String serum_ctrl = "SERUMCTRL";
            public const String project = "PROJECT";
            public const String reference2 = "REFERENCE2";
        }

        public struct ColumnHeaders
        {
            public const String cdcid = "CDCID";
            public const String ssid = "SSID";
            public const String datecollected = "Date Collected";
            public const String passage = "PASSAGE";
            public const String dateharvested = "Date Harvested";
            public const String titre = "TITRE";
            public const String backtitre = "BackTitre";
            public const String importresult = "Import Result";
        }
    }
}
