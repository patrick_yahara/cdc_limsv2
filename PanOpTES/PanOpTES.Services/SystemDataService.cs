﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using PanOpTESModel;
using PanOpTESModel.Models;
using Yahara.Core.Logging;
using Yahara.Core.Repository.EF;
using Yahara.ServiceInfrastructure.EF;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services
{
    public class SystemDataService : EfDataServiceBase, ISystemDataService
    {
        private readonly PanOpTESDbContextSource _contextSource;
        public SystemDataService(PanOpTESDbContextSource contextSource)
        {
            try
            {
                _contextSource = contextSource;
            }
            catch (Exception ex)
            {
                LoggingFactory.GetLogger().Log(typeof(HITestDataService), LoggingLevel.Error, ex);
                throw;
            }
        }

        public ResponseBase<ICollection<EntryTypeDto>> GetAllEntryTypes()
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var test = session.Repository.GetAll<EntryType>()
                                      .Select(e => new EntryTypeDto()
                                      {
                                          Id = e.Id,
                                          SystemName = e.SystemName,
                                      }).ToList();
                    return ResponseBase<ICollection<EntryTypeDto>>.CreateSuccessResponse(test);
                }
            });
        }

        public ResponseBase<EntryTypeDto> GetEntryType(string systemName)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var test = session.Repository.Query<EntryType>(s => systemName.Equals(s.SystemName))
                                      .Select(e => new EntryTypeDto()
                                      {
                                          Id = e.Id,
                                          SystemName = e.SystemName,
                                      }).FirstOrDefault();
                    return ResponseBase<EntryTypeDto>.CreateSuccessResponse(test);
                }
            });
        }
    }
}
