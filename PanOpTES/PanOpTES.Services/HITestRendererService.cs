﻿using System.Drawing;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PanOpTES.Common;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using Yahara.ServiceInfrastructure.Messages;
using System.Linq;

namespace PanOpTES.Services
{
    public class HITestRendererService : IHITestRenderService
    {
        public ResponseBase<byte[]> GetODocTemplate(HITestTemplateDto template)
        {
            using (var stream = new MemoryStream())
            {
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets.Add("HITest");
                    var i = 5;
                    var j = 6;
                    var rotate1 = j;
                    var offset_x = 4;
                    const int offset_y = 2;
                    const int offset_y2 = 4;
                    // REFERENCE
                    worksheet.Cells[offset_x - 3, 1].Value = "Template used for importing data only";
                    worksheet.Cells[offset_x-2, 1].Value = PanOpTESConstants.EntryTypes.reference;

                    worksheet.Row(offset_x - 1).Hidden = true;

                    worksheet.Cells[offset_x, j - 3].Value = PanOpTESConstants.ColumnHeaders.cdcid;
                    worksheet.Cells[offset_x, j - 2].Value = PanOpTESConstants.ColumnHeaders.ssid;
                    worksheet.Cells[offset_x, j - 1].Value = PanOpTESConstants.ColumnHeaders.datecollected;
                    var colcount = template.HIReferenceEntries.Count(s => s.IsColumn);
                    worksheet.Names.Add("Sera", worksheet.Cells[string.Format(@"{0}:{1}", ExcelCellBase.GetAddress(offset_x - 1, j), ExcelCellBase.GetAddress(offset_x - 1, j + colcount - 1))]);
                    foreach (var entry in from t in template.HIReferenceEntries where t.IsColumn orderby t.Position select t)
                    {
                        worksheet.Cells[offset_x - 1, j].Value = entry.AlternativeId;
                        worksheet.Cells[offset_x - 1, j].Style.WrapText = true;
                        var celltemp = worksheet.Cells[offset_x, j];
                        celltemp.Style.TextRotation = 90;
                        celltemp.Value = entry.EntryDescription;
                        j++;
                    }
                    worksheet.Cells[offset_x, j].Value = PanOpTESConstants.ColumnHeaders.passage;
                    j++;
                    worksheet.Cells[offset_x, j].Value = "Date Harvested";
                    j++;
                    worksheet.Cells[offset_x, j].Value = PanOpTESConstants.ColumnHeaders.titre;
                    j++;
                    worksheet.Cells[offset_x, j].Value = PanOpTESConstants.ColumnHeaders.backtitre;
                    j++;
                    worksheet.Cells[offset_x, j].Value = PanOpTESConstants.ColumnHeaders.importresult;
                    var dropdownColumn = j;
                    j++;

                    foreach (var entry in from t in template.HIReferenceEntries where t.IsRow orderby t.Position select t)
                    {
                        //if (entry.IsRow)
                        //{
                            worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                            worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                            worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                            worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                            if (entry.Subsample != null)
                            {
                                worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                                worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                            }
                            worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                            var valuation = worksheet.DataValidations.AddListValidation(ExcelCellBase.GetAddress(i, dropdownColumn));
                            valuation.Formula.ExcelFormula = "=Sera";       
                            i++;
                        //}
                    }
                    foreach (var entry in from t in template.HIReferenceEntries where t.EntryType.SystemName.Equals(PanOpTESConstants.EntryTypes.st_ctrl) orderby t.Position select t)
                    {
                        //if (entry.IsRow)
                        //{
                        worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                        worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                        worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                        worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                        if (entry.Subsample != null)
                        {
                            worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                            worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                        }
                        worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                        i++;
                        //}
                    }
                    foreach (var entry in from t in template.HIReferenceEntries where t.IsRow && t.IsReference2 orderby t.Position select t)
                    {
                        //if (entry.IsRow && entry.IsReference2)
                        //{
                        worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                        worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                        worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                        worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                        if (entry.Subsample != null)
                        {
                            worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                            worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                        }
                        worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                        i++;
                        //}
                    }

                    // TEST
                    worksheet.Cells[i, 1].Value = PanOpTESConstants.EntryTypes.test;
                    offset_x = i + 1;
                    i += 1;
                    j = 5;
                    //worksheet.Cells[offset_x, j - 2].Value = PanOpTESConstants.ColumnHeaders.cdcid;
                    //worksheet.Cells[offset_x, j - 1].Value = PanOpTESConstants.ColumnHeaders.datecollected;

                    //foreach (var entry in from t in template.HIReferenceEntries where t.IsColumn orderby t.Position select t)
                    //{
                    //    worksheet.Cells[offset_x, j].Value = entry.EntryDescription;
                    //    j++;
                    //}
                    foreach (var entry in from t in template.HITestEntries where t.IsRow orderby t.Position select t)
                    {
                            worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                            worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                            worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                            worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                            if (entry.Subsample != null)
                            {
                                worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                                worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                            }
                            worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                            var valuation = worksheet.DataValidations.AddListValidation(ExcelCellBase.GetAddress(i, dropdownColumn));
                            valuation.Formula.ExcelFormula = "=Sera";                            
                            i++;
                    }
                    //worksheet.Cells[offset_x, j].Value = "Passage";
                    //j++;
                    //worksheet.Cells[offset_x, j].Value = "Date Harvested";
                    //j++;
                    //worksheet.Cells[offset_x, j].Value = "Titer";
                    //j++;
                    //worksheet.Cells[offset_x, j].Value = "BackTitre";
                    //j++;

                    // REFERENCE2
                    if (template.HIReferenceEntries.Count(s => s.IsReference2) > 0)
                    {
                    worksheet.Cells[i, 1].Value = PanOpTESConstants.EntryTypes.reference2;
                    offset_x = i + 1;
                    i += 1;
                    j = 5;
                    //worksheet.Cells[offset_x, j - 2].Value = PanOpTESConstants.ColumnHeaders.cdcid;
                        //worksheet.Cells[offset_x, j - 1].Value = PanOpTESConstants.ColumnHeaders.datecollected;
                        //foreach (var entry in from t in template.HIReferenceEntries where t.IsColumn orderby t.Position select t)
                        //{
                        //    worksheet.Cells[offset_x, j].Value = entry.EntryDescription;
                        //    j++;
                        //}
                        foreach (var entry in from t in template.HIReferenceEntries where t.IsRow && t.IsReference2 orderby t.Position select t)
                        {
                                worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                                worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                                worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                                worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                                if (entry.Subsample != null)
                                {
                                    worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                                    worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                                }
                                worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                                i++;
                        }
                        //worksheet.Cells[offset_x, j].Value = "Passage";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "Date Harvested";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "Titer";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "BackTitre";
                        //j++;
                    }

                    // STUDY / PROJECT
                    if (template.HIProjectEntries.Count(s => s.IsRow) > 0)
                    {
                        worksheet.Cells[i, 1].Value = PanOpTESConstants.EntryTypes.project;
                        offset_x = i + 1;
                        i += 1;
                        j = 5;
                        //worksheet.Cells[offset_x, j - 2].Value = PanOpTESConstants.ColumnHeaders.cdcid;
                        //worksheet.Cells[offset_x, j - 1].Value = PanOpTESConstants.ColumnHeaders.datecollected;
                        //foreach (var entry in from t in template.HIReferenceEntries where t.IsColumn orderby t.Position select t)
                        //{
                        //    worksheet.Cells[offset_x, j].Value = entry.EntryDescription;
                        //    j++;
                        //}
                        foreach (var entry in from t in template.HIProjectEntries where t.IsRow orderby t.Position select t)
                        {
                                worksheet.Cells[i, offset_y].Value = entry.EntryDescription;
                                worksheet.Cells[i, offset_y + 1].Value = entry.CDCId;
                                worksheet.Cells[i, offset_y + 2].Value = entry.AlternativeId;
                                worksheet.Cells[i, offset_y + 3].Value = entry.DateCollected.HasValue ? entry.DateCollected.Value.ToString("d") : string.Empty;
                                if (entry.Subsample != null)
                                {
                                    worksheet.Cells[i, offset_y + offset_y2 + colcount].Value = entry.Subsample.Passage;
                                    worksheet.Cells[i, offset_y + offset_y2 + colcount + 1].Value = entry.Subsample.DateHarvestedDisplay;
                                }
                                worksheet.Cells[i, offset_y + offset_y2 + colcount + 2].Value = entry.Titer;
                                i++;
                        }
                        //worksheet.Cells[offset_x, j].Value = "Passage";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "Date Harvested";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "Titer";
                        //j++;
                        //worksheet.Cells[offset_x, j].Value = "BackTitre";
                        //j++;
                    }

                    // SERUM CONTROL
                    worksheet.Cells[i, 1].Value = PanOpTESConstants.EntryTypes.serum_ctrl;
                    offset_x = i + 1;
                    i += 1;
                    j = 5;
                    worksheet.Cells[i, offset_y].Value = PanOpTESConstants.EntryTypes.serum_ctrl;
                    //foreach (var entry in from t in template.HIReferenceEntries where t.IsColumn orderby t.Position select t)
                    //{
                    //    worksheet.Cells[offset_x, j].Value = entry.EntryDescription;
                    //    j++;
                    //}

                    worksheet.Cells.AutoFitColumns(0);
                    package.Workbook.Properties.SetCustomPropertyValue("BaseHITestId", template.BaseHITestId.ToString());
                    //package.Save(); <--Does this dispose?
                    return new ResponseBase<byte[]>(){Data = package.GetAsByteArray()};
                }
            }
        }
    }
}
