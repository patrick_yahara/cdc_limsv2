﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using PanOpTES.Common;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using Yahara.Core.Logging;
using Yahara.ServiceInfrastructure.Messages;
using System.Drawing;

namespace PanOpTES.Services
{
    public class HITestIntakeService : IHITestIntakeService
    {
        public class LocalCellRange
        {
            public int StartRow;
            public int StartCol;
            public int EndRow;
            public int EndCol;

            public LocalCellRange()
            {
            }

            public LocalCellRange(int startrow, int startcol, int endrow, int endcol)
            {
                StartRow = startrow;
                StartCol = startcol;
                EndRow = endrow;
                EndCol = endcol;
            }

            public bool IsEmpty {
                get { return (StartRow == 0 && StartCol == 0 && EndRow == 0 && EndCol == 0); }
            }
        }

        private readonly SystemDataService _lookup;
        public HITestIntakeService(SystemDataService systemData)
        {
            try
            {
                _lookup = systemData;
            }
            catch (Exception ex)
            {
                LoggingFactory.GetLogger().Log(typeof(HITestDataService), LoggingLevel.Error, ex);
                throw;
            }
        }

        private class ParsingDetails
        {
            public ParsingDetails(string sectionName, int row, int col)
            {
                sectionOffset.Add(sectionName, new Point(col, row)); 
            }

            private Dictionary<string, LocalCellRange> _referenceRangeTest = new Dictionary<string, LocalCellRange>();
            private Dictionary<string, Point> _sectionOffset = new Dictionary<string, Point>();
            private Dictionary<string, ExcelCellAddress> _startingPos = new Dictionary<string, ExcelCellAddress>();

            public Dictionary<string, Point> sectionOffset
            {
                get { return _sectionOffset; }
                set { _sectionOffset = value; }
            }
            public Dictionary<string, ExcelCellAddress> startingPos
            {
                get { return _startingPos; }
                set { _startingPos = value; }
            }

            public LocalCellRange referenceRangeSera { get; set; }
            public Dictionary<string, LocalCellRange> referenceRangeTest
            {
                get { return _referenceRangeTest; }
                set { _referenceRangeTest = value; }
            }
        }

        public ResponseBase<HITestDto> ImportTemplate(string filename, string description, byte[] test)
        {
            using (var stream = new MemoryStream(test))
            {
                using (var package = new ExcelPackage(stream))
                {
                    //validation steps
                    var sheet = package.Workbook.Worksheets[1];
                    var details = new ParsingDetails(PanOpTESConstants.EntryTypes.reference, 3, 3)
                        {
                            referenceRangeSera = new LocalCellRange()
                        };

                    //create test
                    var testDto = new HITestDto
                    {
                        TestFileName = filename,
                        TestDescription = description,
                        DateTested = DateTime.UtcNow
                    };

                    var fixedColumnPositions = new Dictionary<string, int>();
                    var iserror = mapSection(PanOpTESConstants.EntryTypes.reference, sheet, ref fixedColumnPositions, ref details);
                    if (iserror != null)
                    {
                        return iserror;
                    }
                    details.sectionOffset.Add(PanOpTESConstants.EntryTypes.test, new Point(1, 1));
                    iserror = mapSection(PanOpTESConstants.EntryTypes.test, sheet, ref fixedColumnPositions, ref details);
                    if (iserror != null)
                    {
                        return iserror;
                    }
                    details.sectionOffset.Add(PanOpTESConstants.EntryTypes.reference2, new Point(1, 1));
                    iserror = mapSection(PanOpTESConstants.EntryTypes.reference2, sheet, ref fixedColumnPositions, ref details);
                    if (iserror != null)
                    {
                        return iserror;
                    }
                    details.sectionOffset.Add(PanOpTESConstants.EntryTypes.serum_ctrl, new Point(1, 1));
                    iserror = mapSection(PanOpTESConstants.EntryTypes.serum_ctrl, sheet, ref fixedColumnPositions, ref details);
                    if (iserror != null)
                    {
                        return iserror;
                    }

                    foreach (var section in details.referenceRangeTest.Keys)
                    {
                        var isothererror = processSection(section, details, sheet, fixedColumnPositions, ref testDto);
                        if (!isothererror.Success) return isothererror;
                    }

                    //return test
                    return ResponseBase<HITestDto>.CreateSuccessResponse(testDto);
                }
            }
        }

        private static ResponseBase<HITestDto> mapSection(string sectionType, ExcelWorksheet sheet, ref Dictionary<string, int> fixedColumnPositions, ref ParsingDetails details)
        {
            for (var i = 1; i < sheet.Dimension.End.Row; i++)
            {
                if (sectionType.Equals(sheet.Cells[i, 1].Value))
                {
                    details.startingPos.Add(sectionType, new ExcelCellAddress(i, 1));
                    break;
                }
            }
            if (!details.startingPos.ContainsKey(sectionType))
            {
                // do nothing
                return null;
                //if (!sectionType.Equals(PanOpTESConstants.EntryTypes.project))
                //{
                //    return ResponseBase<HITestDto>.CreateErrorResponse(new Exception("Invalid Format"));    
                //}
            }
            details.referenceRangeTest.Add(sectionType, new LocalCellRange(details.startingPos[sectionType].Row + details.sectionOffset[sectionType].Y, details.startingPos[sectionType].Column + 1, sheet.Dimension.End.Row, details.startingPos[sectionType].Column + 1));
            for (var i = details.referenceRangeTest[sectionType].StartRow; i <= details.referenceRangeTest[sectionType].EndRow; i++)
            {
                // look for whitespace indicating the matrix ends row wise
                if (String.IsNullOrEmpty(sheet.Cells[i, details.referenceRangeTest[sectionType].StartCol].Text))
                {
                    details.referenceRangeTest[sectionType].EndRow = i - 1;
                    break;
                }
            }

            if (sectionType.Equals(PanOpTESConstants.EntryTypes.reference))
            {
                details.referenceRangeSera = new LocalCellRange(details.startingPos[sectionType].Row + 2, details.startingPos[sectionType].Column + details.sectionOffset[sectionType].X, details.startingPos[sectionType].Row + 1, sheet.Dimension.End.Column);
                var endfound = false;
                var descfound = false;
                var fia = typeof(PanOpTESConstants.ColumnHeaders).GetFields();
                var tempS = new PanOpTESConstants.ColumnHeaders();
                var fixedC = fia.Select(fieldInfo => fieldInfo.GetValue(tempS).ToString()).ToList();

                var tempEndCol = details.referenceRangeSera.EndCol;
                // assert StartCol > 1
                for (var i = details.referenceRangeSera.StartCol - 1; i <= details.referenceRangeSera.EndCol; i++)
                {
                    // look for coded column markers
                    if (fixedC.Contains(sheet.Cells[details.referenceRangeSera.StartRow, i].Text))
                    {
                        fixedColumnPositions.Add(sheet.Cells[details.referenceRangeSera.StartRow, i].Text, i);
                        // this messes up logic on when to stop; fixed columns are at end, or handled by offset in start
                        if (!endfound && descfound)
                        {
                            tempEndCol = i - 1;
                            endfound = true;
                        }
                    }
                    else
                        // look for whitespace indicating the matrix ends column wise
                        if (String.IsNullOrEmpty(sheet.Cells[details.referenceRangeSera.StartRow, i].Text))
                        {
                            if (!endfound)
                            {
                                tempEndCol = i;
                                endfound = true;
                            }
                        }
                        else
                        {
                            // found a description
                            descfound = true;
                        }
                }
                details.referenceRangeSera.EndCol = tempEndCol;                
            }

            return null;
        }

        private ResponseBase<HITestDto> processSection(string sectionType, ParsingDetails details, ExcelWorksheet sheet, Dictionary<string, int> fixedColumnPositions, ref HITestDto testDto)
        {
            //get reference entry type
            var entryType = _lookup.GetEntryType(sectionType).Data;
            if (entryType == null)
            {
                {
                    return ResponseBase<HITestDto>.CreateErrorResponse(new Exception("Error looking up Entry Type"));
                }
            }
            //create test entries
            for (var i = details.referenceRangeTest[sectionType].StartRow; i <= details.referenceRangeTest[sectionType].EndRow; i++)
            {
                var testEntry = new HITestEntryDto();
                testEntry.EntryType = entryType;
                testEntry.EntryDescription = sheet.Cells[i, details.referenceRangeTest[sectionType].StartCol].Text;

                testEntry.Position = (short) i;
                var tempI = 0;
                if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.titre)
                    && int.TryParse(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.titre]].Text, out tempI))
                {
                    testEntry.Titer = tempI;
                }
                short tempS = 0;
                if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.backtitre)
                    &&
                    short.TryParse(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.backtitre]].Text,
                                   out tempS))
                {
                    testEntry.BackTiter = tempS;
                }
                if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.passage)
                    && !String.IsNullOrEmpty(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.passage]].Text))
                {
                    var subSample = new SubsampleDto() {};
                    subSample.Passage =
                        sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.passage]].Text;
                    subSample.ExternalSubsampleId =
                        sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.ssid]].Text;
                    testEntry.Subsample = subSample;
                }
                // have to to this after subsample is set, temp issue
                if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.cdcid)
                    && !String.IsNullOrEmpty(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.cdcid]].Text))
                {
                    testEntry.CDCId =
                        sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.cdcid]].Text;
                }

                // new HITestRequest
                if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.importresult)
                    && !String.IsNullOrEmpty(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.importresult]].Text))
                {
                    var request = new HITestRequestDto()
                    {
                        HISerumEntryName =
                            sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.importresult]].Text,
                        EntryType = entryType,
                    };
                    //PanOpTESConstants.ColumnHeaders.ssid
                    if (fixedColumnPositions.ContainsKey(PanOpTESConstants.ColumnHeaders.ssid)
                        && !String.IsNullOrEmpty(sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.ssid]].Text))
                    {
                        request.HITestEntryName =
                            sheet.Cells[i, fixedColumnPositions[PanOpTESConstants.ColumnHeaders.ssid]].Text;
                    }
                    else
                    {
                        request.HITestEntryName = testEntry.EntryDescription;
                    };
                    (testDto.HITestRequests as List<HITestRequestDto>).Add(request);
                }                  

                (testDto.HITestEntries as List<HITestEntryDto>).Add(testEntry);
            }

            //create serum entries
            for (var i = details.referenceRangeSera.StartCol; i <= details.referenceRangeSera.EndCol; i++)
            {
                var seraEntry = new HISerumEntryDto();
                seraEntry.EntryType = entryType;
                seraEntry.SerumEntryDescription = sheet.Cells[details.referenceRangeSera.StartRow, i].Text;
                seraEntry.ExternalSubsampleId = sheet.Cells[details.referenceRangeSera.StartRow - 1, i].Text;
                seraEntry.Position = i;
                (testDto.HISerumEntries as List<HISerumEntryDto>).Add(seraEntry);
            }

            //create testresult entries
            foreach (var testE in testDto.HITestEntries)
            {
                foreach (var serumE in testDto.HISerumEntries)
                {
                    var testResult = new HITestResultDto();
                    try
                    {
                        var value = 0;
                        var obj = sheet.GetValue(testE.Position, serumE.Position);
                        Debugger.Log(1, "",string.Format("{0}{2}", obj.ToString(), serumE.Position, System.Environment.NewLine));
                        value = Convert.ToInt32(obj);
                        testResult.Titer = value;
                    }
                    catch (Exception Ex)
                    {
                        testResult.Titer = 0;
                        testResult.IsError = true;
                        Debugger.Log(1, "",
                                     string.Format("{0}, {1}{2}", testE.Position, serumE.Position, System.Environment.NewLine));
                        Debugger.Log(1, "",
                                     string.Format("{0}{2}", Ex.Message, serumE.Position, System.Environment.NewLine));
                    }
                    testResult.TestEntry = testE;
                    testResult.SerumEntry = serumE;
                    (testE.HITestResults as List<HITestResultDto>).Add(testResult);
                    (serumE.HITestResults as List<HITestResultDto>).Add(testResult);
                }
            }
            return ResponseBase<HITestDto>.CreateSuccessResponse(testDto);
        }
    }
}
