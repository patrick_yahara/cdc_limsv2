﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services
{
    public class HITestCalculationService : IHITestCalculationService
    {
        public ResponseBase<HITestDto> CalculateHITestResults(HITestDto test)
        {
            // calculate logs for all test 
            var results =
                test.HITestEntries.SelectMany(s => s.HITestResults)
                    .Union(test.HISerumEntries.SelectMany(s => s.HITestResults));
            foreach (var result in results)
            {
                try
                {
                    if (result.Titer != 0)
                    {
                        result.LogFold = Convert.ToInt32(Math.Floor(Math.Log(result.Titer / 5.0) / Math.Log(2)));
                    }
                    else
                    {
                        result.IsError = true;
                    }
                    
                }
                catch (Exception ex)
                {
                    result.IsError = true;
                }
            }
            // Find selected reference entry in set
            var requestReference =
                test.HITestRequests.FirstOrDefault(
                    s => s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.reference));
            if (requestReference != null)
            {
                // find the log fold and store it
                var testE =
                    test.HITestEntries.FirstOrDefault(
                        s => s.Subsample.ExternalSubsampleId.Equals(requestReference.HITestEntryName) && s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.reference));
                var sreumE = test.HISerumEntries.FirstOrDefault(
                        s => s.ExternalSubsampleId.Equals(requestReference.HISerumEntryName) && s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.reference));
                var result = results.FirstOrDefault(s => s.TestEntry.Equals(testE) && s.SerumEntry.Equals(sreumE));
                if (result != null)
                {
                    // calculate differences for selected exam(s)
                    var requestTest =
                        test.HITestRequests.Where(
                            s => s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.test));
                    foreach (var request in requestTest)
                    {
                        var testE_R = test.HITestEntries.FirstOrDefault(
                                                s => s.Subsample.ExternalSubsampleId.Equals(request.HITestEntryName) && s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.test));
                        var sreumE_R = test.HISerumEntries.FirstOrDefault(
                                s => s.ExternalSubsampleId.Equals(request.HISerumEntryName) && s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.test));
                        if (testE_R != null && sreumE_R != null)
                        {
                            var result_R = results.FirstOrDefault(s => s.TestEntry.Equals(testE_R) && s.SerumEntry.Equals(sreumE_R));
                            if (result_R != null)
                            {
                                request.Difference = result.LogFold = result_R.LogFold;
                            }
                            else
                            {
                                request.IsError = true;
                                request.ErrorMessage = "Result not found for request";
                            }                            
                        }
                        else
                        {
                            request.IsError = true;
                            request.ErrorMessage = "Result not found for request";
                        }                            
                    }
                }
                else
                {
                    var requestTest =
                        test.HITestRequests.Where(
                            s => s.EntryType.SystemName.Equals(PanOpTES.Common.PanOpTESConstants.EntryTypes.test));
                    foreach (var request in requestTest)
                    {
                        request.IsError = true;
                        request.ErrorMessage = "Reference not found for request";
                    }
                }

            }
            return ResponseBase<HITestDto>.CreateSuccessResponse(test);
        }
    }
}
