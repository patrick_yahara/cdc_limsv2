﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;
using PanOpTES.Services.Interfaces;
using PanOpTESModel.Models;
using Yahara.Core.Repository;

namespace PanOpTES.Services.Ninject
{
    public class PanOpTESServiceModule : NinjectModule
    {
        public override void Load()
        {
            var settingsProvider = Kernel.Get<ISettingsProvider>();
            //We want one and only one PanOpTESDbContextSource to be constructed, so this is setup as a Singleton binding

            Bind<PanOpTESDbContextSource>().To<PanOpTESDbContextSource>()
                                    .InSingletonScope()
                                    .WithConstructorArgument("databaseSettings", new BasicDatabaseSettings { ConnectionString = settingsProvider.GetConnectionString() });
            Bind<ISystemDataService>().To<SystemDataService>();
            Bind<IHITestDataService>().To<HITestDataService>();
            Bind<IHITestIntakeService>().To<HITestIntakeService>();
            Bind<IHITestRenderService>().To<HITestRendererService>();
            Bind<IHITestCalculationService>().To<HITestCalculationService>();
        }
    }
}
