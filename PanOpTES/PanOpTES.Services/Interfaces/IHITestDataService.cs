﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.DataTransferObjects;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services.Interfaces
{
    public interface IHITestDataService
    {
        ResponseBase<ICollection<HITestDto>> GetAllHITests();
        ResponseBase<HITestTemplateDto> GetHITestTemplate(int id);
        ResponseBase<int> AddHITest(HITestDto dto);
        ResponseBase<ICollection<HITestEntryDto>> GetEntriesByCDCId(string[] ids);
        ResponseBase<ICollection<SpecimenDto>> GetSpecimensByCDCId(string[] ids);
        ResponseBase AddMirthSample(MirthSample dto);
        ResponseBase<ICollection<StarLIMSDCUDto>> GetDCUByTestId(int id);
    }
}
