﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.Services.Interfaces
{
    public interface ISettingsProvider
    {
        string GetConnectionString();
    }
}
