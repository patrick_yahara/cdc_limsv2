﻿using PanOpTES.DataTransferObjects;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services.Interfaces
{
    public interface IHITestIntakeService
    {
        ResponseBase<HITestDto> ImportTemplate(string filename, string description, byte[] test);
    }
}
