﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.DataTransferObjects;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services.Interfaces
{
    public interface IHITestRenderService
    {
        ResponseBase<Byte[]> GetODocTemplate(HITestTemplateDto template);
    }
}
