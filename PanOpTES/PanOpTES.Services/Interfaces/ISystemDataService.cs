﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.DataTransferObjects;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services.Interfaces
{
    public interface ISystemDataService
    {
        ResponseBase<ICollection<EntryTypeDto>> GetAllEntryTypes();
        ResponseBase<EntryTypeDto> GetEntryType(string systemName);
    }
}
