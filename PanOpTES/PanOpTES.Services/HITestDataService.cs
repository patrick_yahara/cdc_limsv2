﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Linq;
using PanOpTES.Common;
using PanOpTES.Common.Utility;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using PanOpTESModel;
using PanOpTESModel.Models;
using Yahara.Core.Logging;
using Yahara.Core.Repository.EF;
using Yahara.ServiceInfrastructure.EF;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.Services
{
    public class HITestDataService : EfDataServiceBase, IHITestDataService
    {
        private readonly PanOpTESDbContextSource _contextSource;
        private List<EntryType> _entryTypes;

        public HITestDataService(PanOpTESDbContextSource contextSource)
        {
            try
            {
                _contextSource = contextSource;
            }
            catch (Exception ex)
            {
                LoggingFactory.GetLogger().Log(typeof(HITestDataService), LoggingLevel.Error, ex);
                throw;
            }
        }

        public ResponseBase<ICollection<HITestDto>> GetAllHITests()
        {
            return ExecuteValidatedDataAction(() =>
                {
                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {
                        var test = session.Repository.GetAll<HITest>()
                                          .Select(e => new HITestDto()
                                              {
                                                  Id = e.Id,
                                                  DateTested = e.DateTested,
                                                  TestDescription = e.TestDescription,
                                                  TestFileName = e.TestFileName,
                                              }).OrderByDescending(s => s.DateTested).ToList();
                        return ResponseBase<ICollection<HITestDto>>.CreateSuccessResponse(test);
                    }
                });
        }

        public ResponseBase<HITestTemplateDto> GetHITestTemplate(int id)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var includes = new List<string>
                        {
                            "HITestEntries",
                            "HITestEntries.Subsample",
                            "HITestEntries.EntryType",
                            "HITestEntries.Subsample.Specimen",
                        };
                    var testDto = session.Repository.Query<HITest>(s => s.Id.Equals(id), includes).Select(test =>
                                new HITestDto()
                                    {
                                        Id = test.Id,
                                        DateTested = test.DateTested,
                                        TestDescription = test.TestDescription,
                                        TestFileName = test.TestFileName,
                                        HITestEntries =
                                        test.HITestEntries.Where(s => s.Subsample != null).Select(s => new HITestEntryDto()
                                            {
                                                EntryDescription = s.EntryDescription,
                                                Id = s.Id,
                                                Position = s.Position,
                                                Titer = s.Titer,
                                                BackTiter = s.BackTitre,
                                                EntryType = new EntryTypeDto() { Id = s.EntryTypeId.Value, SystemName = s.EntryType.SystemName },
                                                SubsampleId = s.SubsampleId,
                                                Subsample = new SubsampleDto()
                                                {
                                                    Id = s.Subsample.Id,
                                                    Passage = s.Subsample.Passage,
                                                    DateHarvested = s.Subsample.DateHarvested.Value,
                                                    SpecimenId = s.Subsample.SpecimenId.Value,
                                                    Specimen = new SpecimenDto()
                                                      {
                                                          Id = s.Subsample.SpecimenId.Value,
                                                          CDCId = s.Subsample.Specimen.CDCId,
                                                          DateCollected = s.Subsample.Specimen.DateCollected
                                                      }
                                                }
                                            })
                                    }).FirstOrDefault();
                    if (testDto == null && !testDto.HITestEntries.Any())
                    {
                        return
                            ResponseBase<HITestTemplateDto>.CreateErrorResponse(new Exception(@"Test Entries not found"));
                    }
                    else
                    {
                        var entries = testDto.HITestEntries.ToList();
                        entries.RemoveAll(s => s.EntryType.SystemName.Equals(PanOpTESConstants.EntryTypes.test));
                        var testET = session.Repository.Query<EntryType>(s => PanOpTESConstants.EntryTypes.test.Equals(s.SystemName))
                                      .Select(e => new EntryTypeDto()
                                      {
                                          Id = e.Id,
                                          SystemName = e.SystemName,
                                      }).FirstOrDefault();
                        var limsEntries =
                            session.Repository.Query<LIMSSubsample>(s => s.LIMSSample.IsAvailable)
                                   .Select(sub => new HITestEntryDto()
                                       {
                                       }).ToList();
                        limsEntries =
                            session.Repository.Query<LIMSSubsample>(s => s.LIMSSample.IsAvailable).Select(sub => new HITestEntryDto()
                                    {
                                                EntryType = new EntryTypeDto(){Id = testET.Id, SystemName = testET.SystemName}, 
                                                EntryDescription = sub.LIMSSample.SpecimenId,
                                                Titer = sub.LIMSHATiter.Titer,
                                                Subsample = new SubsampleDto()
                                                {
                                                    Passage = sub.Passage,
                                                    DateHarvested = sub.DateHarvested,
                                                    ExternalCUId = sub.FlulimsCCUId,
                                                    ExternalSubsampleId = sub.FlulimsSSId,
                                                    Specimen = new SpecimenDto()
                                                    {
                                                        CDCId = sub.LIMSSample.CDCId,
                                                        DateCollected = sub.LIMSSample.DateCollected
                                                    }                                           
                                                }
                                            }).ToList();
                        if (limsEntries != null)
                        {
                            short i = 0;
                            limsEntries.ForEach(s => s.Position = i++);
                            entries.AddRange(limsEntries);
                        }
                        testDto.HITestEntries = entries;
                        var template = new HITestTemplateDto {BaseHITestId = testDto.Id, HITestEntries = testDto.HITestEntries};
                        return ResponseBase<HITestTemplateDto>.CreateSuccessResponse(template);
                    }
                }
            });
        }

        public ResponseBase<int> AddHITest(HITestDto dto)
        {
            return ExecuteValidatedDataAction(() =>
            {
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var test = new HITest();
                    test.DateTested = dto.DateTested;
                    test.TestDescription = dto.TestDescription;
                    test.TestFileName = dto.TestFileName;
                    EFAuditFieldUtility.UpdateAuditFields(test);
                    test.CreatedBy = !String.IsNullOrEmpty(test.CreatedBy) ? test.CreatedBy : "System";

                    try
                    {
                        session.Repository.Insert(test);
                    }
                    catch (Exception ex)
                    {
                        return ResponseBase<int>.CreateErrorResponse(ex);
                    }

                    foreach (var requestDto in dto.HITestRequests)
                    {
                        var request = new HITestRequest();
                        request.HISerumEntryName = requestDto.HISerumEntryName;
                        request.HITestEntryName = requestDto.HITestEntryName;
                        request.EntryTypeId = requestDto.EntryType.Id;
                        request.HITestId = test.Id;
                        request.Difference = requestDto.Difference;
                        request.IsError = requestDto.IsError;
                        request.ErrorMessage = requestDto.ErrorMessage;
                        EFAuditFieldUtility.UpdateAuditFields(request);
                        request.CreatedBy = !String.IsNullOrEmpty(request.CreatedBy)
                                                  ? request.CreatedBy
                                                  : "System";
                        try
                        {
                            session.Repository.Insert(request);
                        }
                        catch (DbEntityValidationException valex)
                        {
                            var error = ResponseBase<int>.CreateErrorResponse(valex);
                            return error;
                        }
                        catch (Exception ex)
                        {
                            var error = ResponseBase<int>.CreateErrorResponse(ex);
                            return error;
                        }
                        var testET = session.Repository.Query<EntryType>(s => PanOpTESConstants.EntryTypes.test.Equals(s.SystemName))
                                                              .Select(e => new EntryTypeDto()
                                                              {
                                                                  Id = e.Id,
                                                                  SystemName = e.SystemName,
                                                              }).FirstOrDefault();
                        if (request.EntryTypeId.Equals(testET.Id))
                        {
                                //var limssample =
                                //    session.Repository.Query<LIMSSample>(
                                //        s => s.IsAvailable && s.LIMSSubsamples.Equals(testE.EntryDescription))
                                //           .OrderBy(s => s.Id)
                                //           .FirstOrDefault();
                            var limssample =
                                session.Repository.Query<LIMSSubsample>(
                                    s => s.FlulimsSSId.Equals(request.HITestEntryName))
                                    .Select(s => s.LIMSSample).Where(s => s.IsAvailable)
                                    .Distinct().OrderBy(s => s.CreatedOn).FirstOrDefault();

                            if (limssample != null && limssample.Id > 0)
                            {
                                limssample.IsAvailable = false;
                                EFAuditFieldUtility.UpdateAuditFields(limssample);
                                try
                                {
                                    session.Repository.Update(limssample);
                                    try
                                    {
                                        request.LIMSSampleId = limssample.Id;
                                        session.Repository.Update(request);
                                    }
                                    catch (DbEntityValidationException valex)
                                    {
                                        var error = ResponseBase<int>.CreateErrorResponse(valex);
                                        return error;
                                    }
                                    catch (Exception ex)
                                    {
                                        var error = ResponseBase<int>.CreateErrorResponse(ex);
                                        return error;
                                    }
                                }
                                catch (DbEntityValidationException valex)
                                {
                                    var error = ResponseBase<int>.CreateErrorResponse(valex);
                                    return error;
                                }
                                catch (Exception ex)
                                {
                                    var error = ResponseBase<int>.CreateErrorResponse(ex);
                                    return error;
                                }
                            }

                        }
                    }

                    foreach (var testE in dto.HITestEntries)
                    {
                        if (testE.Subsample != null)
                        {
                            var subsample = new Subsample() { };
                            if (testE.Subsample.SpecimenId != 0)
                            {
                                subsample.SpecimenId = testE.Subsample.SpecimenId;
                            }
                            subsample.Passage = testE.Subsample.Passage;
                            subsample.IsFromSubmitter = true;
                            subsample.Lot = "System";
                            subsample.ContractLab = "System";
                            subsample.SequencingStorage = "System";
                            subsample.InitialStorage = "System";
                            subsample.AAResAdam = "System";
                            subsample.AAResNai = "System";
                            subsample.DownToVaccine = "System";
                            subsample.IsVaccine = true;
                            subsample.CalculatedSubtype = "System";
                            EFAuditFieldUtility.UpdateAuditFields(subsample);
                            subsample.CreatedBy = !String.IsNullOrEmpty(subsample.CreatedBy)
                                                      ? subsample.CreatedBy
                                                      : "System";
                            try
                            {
                                session.Repository.Insert(subsample);
                                testE.SubsampleId = subsample.Id;
                                testE.Subsample.Id = subsample.Id;
                            }
                            catch (DbEntityValidationException valex)
                            {
                                var error = ResponseBase<int>.CreateErrorResponse(valex);
                                return error;
                            }
                            catch (Exception ex)
                            {
                                var error = ResponseBase<int>.CreateErrorResponse(ex);
                                return error;
                            }
                        }
                        var testEntry = new HITestEntry();
                        testEntry.HITestId = test.Id;
                        testEntry.EntryDescription = testE.EntryDescription;
                        testEntry.EntryTypeId = testE.EntryType.Id;
                        testEntry.Position = testE.Position;
                        testEntry.DoNotReport = false;
                        testEntry.Titer = testE.Titer;
                        testEntry.BackTitre = testE.BackTiter;
                        testEntry.SubsampleId = testE.SubsampleId;
                        EFAuditFieldUtility.UpdateAuditFields(testEntry);
                        testEntry.CreatedBy = !String.IsNullOrEmpty(testEntry.CreatedBy)
                                                  ? testEntry.CreatedBy
                                                  : "System";
                        try
                        {
                            session.Repository.Insert(testEntry);
                            testE.Id = testEntry.Id;
                            foreach (var hiTestResultDto in testE.HITestResults)
                            {
                                hiTestResultDto.TestEntryId = testE.Id;
                            }
                        }
                        catch (Exception ex)
                        {
                            return ResponseBase<int>.CreateErrorResponse(ex);
                        }
                    }
                    foreach (var serumE in dto.HISerumEntries)
                        {
                            var serumEntry = new HISerumEntry();
                            serumEntry.HITestId = test.Id;
                            serumEntry.EntryTypeId = serumE.EntryType.Id;
                            serumEntry.SerumEntryDescription = serumE.SerumEntryDescription;
                            serumEntry.Position = serumE.Position.ToString();
                            EFAuditFieldUtility.UpdateAuditFields(serumEntry);
                            serumEntry.CreatedBy = !String.IsNullOrEmpty(serumEntry.CreatedBy) ? serumEntry.CreatedBy : "System";
                            try
                            {
                                session.Repository.Insert(serumEntry);
                                serumE.Id = serumEntry.Id;
                                foreach (var hiTestResultDto in serumE.HITestResults)
                                {
                                    hiTestResultDto.SerumEntryId = serumE.Id;
                                }
                            }
                            catch (Exception ex)
                            {
                                return ResponseBase<int>.CreateErrorResponse(ex);
                            }
                        }

                    var entries =
                        dto.HITestEntries.SelectMany(s => s.HITestResults)
                           .Union(dto.HISerumEntries.SelectMany(s => s.HITestResults))
                           .Distinct();
                    foreach (var hiTestResultDto in entries)
                    {
                        var testResult = new HITestResult();
                        testResult.Titer = (short)hiTestResultDto.Titer;
                        testResult.HISerumEntryId = hiTestResultDto.SerumEntryId;
                        testResult.HITestEntryId = hiTestResultDto.TestEntryId;
                        testResult.IsError = hiTestResultDto.IsError;
                        testResult.LogFold = (short)hiTestResultDto.LogFold;
                        EFAuditFieldUtility.UpdateAuditFields(testResult);
                        testResult.CreatedBy = !String.IsNullOrEmpty(testResult.CreatedBy) ? testResult.CreatedBy : "System";
                        try
                        {
                            session.Repository.Insert(testResult);
                        }
                        catch (Exception ex)
                        {
                            return ResponseBase<int>.CreateErrorResponse(ex);
                        }
                    }

                    return ResponseBase<int>.CreateSuccessResponse(test.Id);
                }
            });
        }

        public ResponseBase<ICollection<HITestEntryDto>> GetEntriesByCDCId(string[] ids)
        {
            return ExecuteValidatedDataAction(() =>
                {
                    if (ids == null)
                    {
                        return ResponseBase<ICollection<HITestEntryDto>>.CreateErrorResponse(new ArgumentNullException());
                    }
                    if (ids.Length == 0)
                    {
                        return
                            ResponseBase<ICollection<HITestEntryDto>>.CreateErrorResponse(
                                new ArgumentOutOfRangeException());
                    }
                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {
                        var results = session.Repository.Query<HITestEntry>(s => ids.Contains(s.Subsample.Specimen.CDCId)).Select(s => new HITestEntryDto()
                                        {
                                            EntryDescription = s.EntryDescription,
                                            Id = s.Id,
                                            Position = s.Position,
                                            Titer = s.Titer,
                                            BackTiter = s.BackTitre,
                                            EntryType = new EntryTypeDto() { Id = s.EntryTypeId.Value, SystemName = s.EntryType.SystemName },
                                            SubsampleId = s.SubsampleId,
                                            Subsample = new SubsampleDto()
                                            {
                                                Id = s.Subsample.Id
                                                ,
                                                Passage = s.Subsample.Passage
                                                ,
                                                SpecimenId = s.Subsample.SpecimenId.Value
                                                ,
                                                Specimen = new SpecimenDto()
                                                {
                                                    Id = s.Subsample.SpecimenId.Value
                                                    ,
                                                    CDCId = s.Subsample.Specimen.CDCId
                                                }
                                            }
                                        }).ToList();
                        return ResponseBase<ICollection<HITestEntryDto>>.CreateSuccessResponse(results);
                    }
                });
        }
        public ResponseBase<ICollection<SpecimenDto>> GetSpecimensByCDCId(string[] ids)
        {
            return ExecuteValidatedDataAction(() =>
            {
                if (ids == null)
                {
                    return ResponseBase<ICollection<SpecimenDto>>.CreateErrorResponse(new ArgumentNullException());
                }
                if (ids.Length == 0)
                {
                    return
                        ResponseBase<ICollection<SpecimenDto>>.CreateErrorResponse(
                            new ArgumentOutOfRangeException());
                }
                using (var session = new SingleDisposableRepositorySession(_contextSource))
                {
                    var results = session.Repository.Query<Specimen>(s => ids.Contains(s.CDCId)).Select(s => new SpecimenDto()
                    {
                        Id = s.Id,
                        CDCId = s.CDCId
                    }).ToList();

                    return ResponseBase<ICollection<SpecimenDto>>.CreateSuccessResponse(results);
                }
            });
        }

        public ResponseBase AddMirthSample(MirthSample dto)
        {
            return ExecuteValidatedDataAction(() =>
                {
                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {
                        var sample = new LIMSSample
                            {
                                CDCId = dto.cdc_id,
                                Country = dto.country,
                                SpecimenId = dto.specimen_id,
                                IsAvailable = true,
                            };
                        DateTime temp;
                        if (DateTime.TryParse(dto.date_collected, out temp))
                        {
                            sample.DateCollected = temp;
                        }
                        sample.State = dto.state;
                        EFAuditFieldUtility.UpdateAuditFields(sample);
                        sample.CreatedBy = !String.IsNullOrEmpty(sample.CreatedBy) ? sample.CreatedBy : "System";
                        try
                        {
                            session.Repository.Insert(sample);
                        }
                        catch (DbEntityValidationException ex)
                        {
                            return ResponseBase.CreateErrorResponse(ex);
                        }
                        catch (Exception ex)
                        {
                            return ResponseBase.CreateErrorResponse(ex);
                        }
                        foreach (var subDto in dto.subsamples)
                        {
                            int tempInt;
                            var hatiter = new LIMSHATiter();
                            var hitest = new LIMSHiTest();
                            if (subDto.ha_titer != null)
                            {
                                hatiter.RBCType = subDto.ha_titer.rbc_type;
                                if (int.TryParse(subDto.ha_titer.titer, out tempInt))
                                {
                                    hatiter.Titer = tempInt;
                                }
                                EFAuditFieldUtility.UpdateAuditFields(hatiter);
                                hatiter.CreatedBy = !String.IsNullOrEmpty(hatiter.CreatedBy) ? hatiter.CreatedBy : "System";
                                try
                                {
                                    session.Repository.Insert(hatiter);
                                }
                                catch (DbEntityValidationException ex)
                                {
                                    return ResponseBase.CreateErrorResponse(ex);
                                }
                                catch (Exception ex)
                                {
                                    return ResponseBase.CreateErrorResponse(ex);
                                }
                            }
                            if (subDto.hi_test != null)
                            {
                                if (DateTime.TryParse(subDto.hi_test.date_requested, out temp))
                                {
                                    hitest.DateRequested = temp;
                                }
                                EFAuditFieldUtility.UpdateAuditFields(hitest);
                                hitest.CreatedBy = !String.IsNullOrEmpty(hitest.CreatedBy) ? hitest.CreatedBy : "System";
                                try
                                {
                                    session.Repository.Insert(hitest);
                                }
                                catch (DbEntityValidationException ex)
                                {
                                    return ResponseBase.CreateErrorResponse(ex);
                                }
                                catch (Exception ex)
                                {
                                    return ResponseBase.CreateErrorResponse(ex);
                                }

                            }
                            var subsample = new LIMSSubsample();
                            if (hatiter.Id > 0)
                            {
                                subsample.LIMSHATiterId = hatiter.Id;
                            }
                            if (hitest.Id > 0)
                            {
                                subsample.LIMSHiTestId = hitest.Id;
                            }
                            subsample.LIMSSampleId = sample.Id;
                            subsample.ContractLab = subDto.contract_lab;
                            if (DateTime.TryParse(subDto.date_harvested, out temp))
                            {
                                subsample.DateHarvested = temp;    
                            }
                            subsample.FlulimsSSId = subDto.flulims_ssid;    
                            subsample.Lot = subDto.lot;
                            subsample.Passage = subDto.passage;
                            subsample.FlulimsCCUId = subDto.cuid;
                            EFAuditFieldUtility.UpdateAuditFields(subsample);
                            subsample.CreatedBy = !String.IsNullOrEmpty(subsample.CreatedBy) ? subsample.CreatedBy : "System";
                            try
                            {
                                session.Repository.Insert(subsample);
                            }
                            catch (DbEntityValidationException ex)
                            {
                                return ResponseBase.CreateErrorResponse(ex);
                            }
                            catch (Exception ex)
                            {
                                return ResponseBase.CreateErrorResponse(ex);
                            }
                        }
                        return ResponseBase.CreateSuccessResponse();
                    }
                });
        }

        public ResponseBase<ICollection<StarLIMSDCUDto>> GetDCUByTestId(int id)
        {
            return ExecuteValidatedDataAction(() =>
                {
                    var result = new List<StarLIMSDCUDto>();
                    using (var session = new SingleDisposableRepositorySession(_contextSource))
                    {

                        var refET = session.Repository.Query<EntryType>(s => PanOpTESConstants.EntryTypes.reference.Equals(s.SystemName))
                                                              .Select(e => new EntryTypeDto()
                                                              {
                                                                  Id = e.Id,
                                                                  SystemName = e.SystemName,
                                                              }).FirstOrDefault();

                        var includes = new List<string>
                        {
	                        "HITestRequests",
	                        "HITestRequests.LIMSSample",
	                        "HITestRequests.LIMSSample.LIMSSubsamples",
	                        "HITestRequests.LIMSSample.LIMSSubsamples.LIMSHATiter",
                        };
                        var test = session.Repository.Query<HITest>(s => s.Id.Equals(id), includes).FirstOrDefault();
                        if (test != null)
                        {
                            var refRequest = test.HITestRequests.FirstOrDefault(s => s.EntryTypeId == refET.Id);
                            if (refRequest != null)
                            {
                                result = test.HITestRequests.Where(s => s.EntryTypeId != refET.Id).Select(r => new StarLIMSDCUDto()
                                    {
                                        CUID = r.LIMSSample.LIMSSubsamples.FirstOrDefault().FlulimsCCUId,
                                        TestID = test.Id.ToString(),
                                        TestDate = test.CreatedOn.ToShortDateString(),
                                        TestName = test.TestDescription,
                                        RBCType = r.LIMSSample.LIMSSubsamples.FirstOrDefault().LIMSHATiter.RBCType,
                                        Interpretation = refRequest.HISerumEntryName,
                                        LogFoldChange = r.Difference.ToString(),
                                    }
                                    ).ToList();
                            }
                        }
                        return ResponseBase<ICollection<StarLIMSDCUDto>>.CreateSuccessResponse(result);
                    }
                });
        }
    }
}
