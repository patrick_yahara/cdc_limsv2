ALTER PROCEDURE [dbo].[usp_LoadEntryType_HITestEntry]
WITH EXECUTE AS CALLER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure loads/updates the EntryType lookup table and then loads/updates the 
		HITestEntry table with values from ##tmpEntryType and ##tmpFluHITestEnry.

    PARAMETERS
		None

	RETURN VALUE 
		0 - No Error. 

    PROGRAMMING NOTES 
		This procedure is called from SSIS package HITestEntry.

    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpEntryType
  			##tmpFluHITestEntry
  			EntryType
  			HITestEntry

    EXAMPLE CALL(5)
		EXEC [dbo].usp_LoadEntryType_HITestEntry

    CHANGE HISTORY 
		2014-03-11 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
		2014-04-01 - Cesar Sala - Added 'REFERENCE2' as an EntryType.
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON

IF OBJECT_ID('TempDB..##tmpEntryType') IS NOT NULL
BEGIN
	-- These values are used by PanOpTES and do not exist in the host system.
	INSERT INTO ##tmpEntryType
		   ( EntryType )
	VALUES ( 'REFERENCE2' )
	
	MERGE [dbo].EntryType AS EntryType
	USING [dbo].##tmpEntryType AS tmpEntryType
	   ON EntryType.DisplayName = tmpEntryType.EntryType
	 WHEN MATCHED THEN
		  UPDATE 
			 SET DisplayName = EntryType
	 WHEN NOT MATCHED THEN
		  INSERT ( DisplayName,
				   SystemName )
		  VALUES ( tmpEntryType.EntryType, 
				   UPPER( REPLACE (tmpEntryType.EntryType, '_', '' ) ) );
END;

IF OBJECT_ID('TempDB..##tmpFluHITestEntry') IS NOT NULL
BEGIN
	SET IDENTITY_INSERT [dbo].HITestEntry ON;

	WITH cteFluHITestEntry
		 ( Id, 
		   HITestId,
		   Position, 
		   IsolateId, 
		   EntryTypeId,
		   EntryDescription, 
		   EntryError, 
		   Clade, 
		   Titer, 
		   BackTiter,
		   DoNotReport ) 
	  AS	 
		 ( SELECT tmpFHTE.id, 
				  tmpFHTE.hi_test_id,
				  tmpFHTE.position, 
				  tmpFHTE.isolate_id, 
				  EntryType.Id,
				  tmpFHTE.[description], 
				  tmpFHTE.error, 
				  tmpFHTE.clade, 
				  tmpFHTE.titer, 
				  tmpFHTE.back_titer,
				  tmpFHTE.do_not_report
			 FROM [dbo].##tmpFluHITestEntry AS tmpFHTE
				  INNER JOIN [dbo].EntryType AS EntryType
					 ON tmpFHTE.entry_type = EntryType.DisplayName )
				     
	MERGE [dbo].HITestEntry AS HITestEntry
	USING cteFluHITestEntry AS cteFHTE
	   ON HITestEntry.Id = cteFHTE.Id
	 WHEN MATCHED THEN
		  UPDATE
			 SET HITestEntry.EntryTypeId = cteFHTE.EntryTypeId, 
				 HITestEntry.HITestId = cteFHTE.HITestId, 
				 HITestEntry.SubsampleId = cteFHTE.IsolateId, 
				 HITestEntry.Position= cteFHTE.Position,  
				 HITestEntry.EntryDescription = cteFHTE.EntryDescription, 
				 HITestEntry.EntryError = cteFHTE.EntryError, 
				 HITestEntry.Clade = cteFHTE.Clade, 
				 HITestEntry.Titer = cteFHTE.Titer, 
				 HITestEntry.BackTitre = cteFHTE.BackTiter, 
				 HITestEntry.DoNotReport = cteFHTE.DoNotReport
	 WHEN NOT MATCHED THEN
		  INSERT ( Id, 
				   EntryTypeId, 
				   HITestId, 
				   SubsampleId, 
				   Position, 
				   EntryDescription, 
				   EntryError, 
				   Clade, 
				   Titer, 
				   BackTitre, 
				   DoNotReport )
		  VALUES ( cteFHTE.Id, 
				   cteFHTE.EntryTypeId, 
				   cteFHTE.HITestId, 
				   cteFHTE.IsolateId, 
				   cteFHTE.Position, 
				   cteFHTE.EntryDescription, 
				   cteFHTE.EntryError, 
				   cteFHTE.Clade, 
				   cteFHTE.Titer, 
				   cteFHTE.BackTiter,
				   cteFHTE.DoNotReport );

	SET IDENTITY_INSERT [dbo].HITestEntry OFF;
END;

RETURN(0)
