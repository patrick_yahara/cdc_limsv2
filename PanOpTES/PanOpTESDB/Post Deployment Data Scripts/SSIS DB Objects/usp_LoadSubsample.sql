ALTER PROCEDURE [dbo].[usp_LoadSubsample]
WITH EXECUTE AS OWNER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure loads/updates the Subsample table with values from ##tmpFluIsolate. 
		##tmpFluIsolate was loaded in the Subsample SSIS package.   
             
    PARAMETERS
		None
		            
	RETURN VALUE 
		 0 - No Error. 
         
    PROGRAMMING NOTES 
		This procedure is called from the Subsample SSIS package.
		
    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpFluIsolate
			Subsample

    EXAMPLE CALL(5)
		EXEC [dbo].usp_LoadSubsample
         
    CHANGE HISTORY 
		2014-03-25 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
		2014-03-26 - Cesar Sala - Added transforms for Passage, Vaccine, and SampleClass.
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON

IF OBJECT_ID('TempDB..##tmpFluIsolate') IS NOT NULL
BEGIN
	SET IDENTITY_INSERT [dbo].Subsample ON;

	WITH cteFluIsolate
		 ( Id,
		   SpecimenId,
		   Passage,
		   DateHarvested,
		   Vaccine,
		   IsolateReceived,
		   SequencingStorage,
		   AAResAdam,
		   AAResNai,
		   DownToVaccine,
		   ContractLabId, 
		   Lot,
		   CalculatedSubtype,
		   DateContractLabTransfer,
		   InitialStorage,
		   SampleClass ) 
	  AS	 
		 ( SELECT tmpFluIsolate.[Id],
				  tmpFluIsolate.[specimen_id], 
				  CASE 
					 WHEN Specimen.[PassageReceived] = ''
						THEN tmpFluIsolate.[passage]
					 ELSE
						CASE 
						   WHEN tmpFluIsolate.[isolate_received] = 1
							  THEN Specimen.[PassageReceived] 
						   ELSE
							  tmpFluIsolate.[passage]
						 END
				  END AS [passage], 
				  tmpFluIsolate.[date_harvested], 
				  CASE 
					 WHEN Specimen.[PassageReceived] = ''
						THEN tmpFluIsolate.[vaccine]
					 ELSE
						CASE 
						   WHEN tmpFluIsolate.[isolate_received] = 1
							  THEN Specimen.[IsVaccine]  
						   ELSE
							  tmpFluIsolate.[vaccine]
						 END
				  END AS [vaccine], 
				  tmpFluIsolate.[isolate_received], 
				  CASE
					 WHEN tmpFluIsolate.[sequencing_storage] = ''
						THEN tmpFluIsolate.[initial_storage]
					 ELSE
						tmpFluIsolate.[sequencing_storage]
				  END AS [sequencing_storage],
				  tmpFluIsolate.[aa_res_adam], 
				  tmpFluIsolate.[aa_res_nai], 
				  tmpFluIsolate.[down_to_vaccine], 
				  tmpFluIsolate.[contract_lab_id], 
				  tmpFluIsolate.[lot], 
				  tmpFluIsolate.[calculated_subtype], 
				  tmpFluIsolate.[date_contract_lab_transfer], 
				  tmpFluIsolate.[initial_storage], 
				  CASE
					 WHEN tmpFluIsolate.[isolate_received] = 1 And Specimen.[IsOriginal] = 1
						THEN 'Original' 
					 ELSE
						CASE
						   WHEN Specimen.[ReagentClass] = 'rna' And Specimen.[IsRNAOnly] = 1
							  THEN 'RNA'
						   WHEN Specimen.[ReagentClass] = 'cdna' And Specimen.[IsCDNAOnly] = 1
							  THEN 'cDNA'
						   WHEN Specimen.[ReagentClass] = 'serum'
							  THEN 'Serum'
						   WHEN Specimen.[ReagentClass] = 'monoclonal'
							  THEN 'Monoclonal'
						   WHEN Specimen.[ReagentClass] = 'monoclonal'
							  THEN 'Monoclonal'
						   ELSE
							  CASE
								 WHEN tmpFluIsolate.[passage] = ''
									THEN NULL
								 ELSE
									'Isolate'
							  END
						END
				  END AS [sampleclass]
			 FROM [dbo].##tmpFluIsolate AS tmpFluIsolate
				  INNER JOIN Specimen AS Specimen
					 ON tmpFluIsolate.specimen_id = Specimen.Id )
				     
	MERGE [dbo].Subsample AS Subsample
	USING cteFluIsolate AS cteFluIsolate
	   ON Subsample.Id = cteFluIsolate.Id
	 WHEN MATCHED THEN
		  UPDATE
			 SET Subsample.[SpecimenId] = cteFluIsolate.[SpecimenId], 
 				 Subsample.[IsFromSubmitter] = cteFluIsolate.[IsolateReceived], 
 				 Subsample.[Passage] = cteFluIsolate.[Passage], 
 				 Subsample.[DateHarvested] = cteFluIsolate.[DateHarvested], 
 				 Subsample.[Lot] = cteFluIsolate.[Lot], 
 				 Subsample.[ContractLab] = cteFluIsolate.[ContractLabId], 
 				 Subsample.[DateContractLabTransfer] = cteFluIsolate.[DateContractLabTransfer], 
 				 Subsample.[IsVaccine] = cteFluIsolate.[Vaccine], 
 				 Subsample.[CalculatedSubtype] = cteFluIsolate.[CalculatedSubtype], 
 				 Subsample.[SequencingStorage] = cteFluIsolate.[SequencingStorage], 
 				 Subsample.[InitialStorage] = cteFluIsolate.[InitialStorage], 
 				 Subsample.[AAResAdam] = cteFluIsolate.[AAResAdam], 
 				 Subsample.[AAResNai] = cteFluIsolate.[AAResNai], 
 				 Subsample.[DownToVaccine] = cteFluIsolate.[DownToVaccine], 
 				 Subsample.[SampleClass] = cteFluIsolate.[SampleClass]
	 WHEN NOT MATCHED THEN
		  INSERT ( [Id], 
				   [SpecimenId], 
				   [IsFromSubmitter], 
				   [Passage], 
				   [DateHarvested], 
				   [Lot], 
				   [ContractLab], 
				   [DateContractLabTransfer], 
				   [IsVaccine], 
				   [CalculatedSubtype], 
				   [SequencingStorage], 
				   [InitialStorage], 
				   [AAResAdam], 
				   [AAResNai], 
				   [DownToVaccine], 
				   [SampleClass] )
		  VALUES ( cteFluIsolate.[Id], 
				   cteFluIsolate.[SpecimenId], 
				   cteFluIsolate.[IsolateReceived], 
				   cteFluIsolate.[Passage], 
				   cteFluIsolate.[DateHarvested], 
				   cteFluIsolate.[Lot], 
				   cteFluIsolate.[ContractLabId], 
				   cteFluIsolate.[DateContractLabTransfer], 
				   cteFluIsolate.[Vaccine], 
				   cteFluIsolate.[CalculatedSubtype], 
				   cteFluIsolate.[SequencingStorage], 
				   cteFluIsolate.[InitialStorage], 
				   cteFluIsolate.[AAResAdam], 
				   cteFluIsolate.[AAResNai], 
				   cteFluIsolate.[DownToVaccine], 
				   cteFluIsolate.[SampleClass] );
				   
	SET IDENTITY_INSERT [dbo].Subsample OFF;
END;

RETURN(0)
