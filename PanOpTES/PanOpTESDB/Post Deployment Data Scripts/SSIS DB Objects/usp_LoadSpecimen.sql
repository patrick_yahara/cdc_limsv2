ALTER PROCEDURE [dbo].[usp_LoadSpecimen]
WITH EXECUTE AS OWNER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure loads/updates the Specimen table with values from ##tmpFluSpecimen. 
		##tmpFluSpecimen was loaded in the Specimen SSIS package.   
             
    PARAMETERS
		None
		            
	RETURN VALUE 
		 0 - No Error. 
         
    PROGRAMMING NOTES 
		This procedure is called from the Specimen SSIS package.
		
    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpFluSpecimen
			Country
			County
			Specimen
			State

    EXAMPLE CALL(5)
		EXEC [dbo].usp_LoadSpecimen
         
    CHANGE HISTORY 
		2014-03-21 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON

IF OBJECT_ID('TempDB..##tmpFluSpecimen') IS NOT NULL
BEGIN
	SET IDENTITY_INSERT [dbo].Specimen ON;

	WITH cteFluSpecimen
		 ( Id,
		   CDCId,
		   SpecimenId,
		   StrainName,
		   IsReassortant,
		   IsNegative,
		   IsColdAdapted,
		   IsRNAOnly,
		   IsCDNAOnly,
		   HostId,
		   DateCollected,
		   IsCollectionDateEstimated, 
		   DateReceived,
		   CountryId,
		   StateId,
		   IsReagent,
		   ReagentClass,
		   PassageReceived,
		   CollectionComment,
		   IsOriginal,
		   IsVaccine,
		   SenderCode,
		   PatientAge,
		   PatientGender,
		   PatientLocation,
		   IsPatientDeceased,
		   SpecialStudy,
		   GeneticScreen,
		   Activity,
		   IsRecombinant,
		   LabComment,
		   ContractLab,
		   DatePCR,
		   SenderLabId,
		   YearCollected,
		   IsDuplicate,
		   ReasonForSubmission,
		   DateLogin,
		   CountyId,
		   DateContractLabTransfer,
		   IsNursingHome,
		   IsPatientVaccinated,
		   ShippingCondition,
		   IsTravel,
		   TravelLocation,
		   PackageId,
		   CollectionMethod ) 
	  AS	 
		 ( SELECT tmpFluSpecimen.[Id],
				  tmpFluSpecimen.[cdc_id], 
				  tmpFluSpecimen.[specimen_id], 
				  tmpFluSpecimen.[strain_name], 
				  tmpFluSpecimen.[reassortant], 
				  tmpFluSpecimen.[negative], 
				  tmpFluSpecimen.[cold_adapted], 
				  tmpFluSpecimen.[rna_only], 
				  tmpFluSpecimen.[cdna_only], 
				  tmpFluSpecimen.[host_id], 
				  tmpFluSpecimen.[date_collected], 
				  CASE 
					 WHEN tmpFluSpecimen.[date_collected] IS NULL
						THEN 
						   CASE 
							  WHEN tmpFluSpecimen.[estimated_collection_date] IS NULL
								 THEN 0
							  ELSE
								 1
						   END
					 ELSE
						0
				  END AS [IsCollectionDateEstimated], 
				  tmpFluSpecimen.[date_received], 
				  Country.[Id], 
				  State.[Id], 
				  tmpFluSpecimen.[reagent], 
				  tmpFluSpecimen.[reagent_class], 
				  tmpFluSpecimen.[passage_received], 
				  tmpFluSpecimen.[collection_comment], 
				  tmpFluSpecimen.[original], 
				  tmpFluSpecimen.[vaccine], 
				  tmpFluSpecimen.[sender_code],
				  tmpFluSpecimen.[patient_age],
				  CASE tmpFluSpecimen.[patient_gender]
					 WHEN 'M'
						THEN 'Male'
					 WHEN 'F'
						THEN 'Female'
					 WHEN ''
						THEN NULL
				  END AS [patient_gender], 
				  tmpFluSpecimen.[patient_location], 
				  tmpFluSpecimen.[patient_deceased],
				  tmpFluSpecimen.[special_study], 
				  tmpFluSpecimen.[genetic_screen], 
				  CASE tmpFluSpecimen.[activity]
					 WHEN ''
						THEN NULL
					 ELSE UPPER( LEFT( tmpFluSpecimen.[activity], 1 ) ) +
						   SUBSTRING( tmpFluSpecimen.[activity], 2, LEN ( tmpFluSpecimen.[activity] ) )
				  END AS [activity], 
				  tmpFluSpecimen.[recombinant], 
				  tmpFluSpecimen.[lab_comment] , 
				  tmpFluSpecimen.[contract_lab_id],
				  tmpFluSpecimen.[date_pcr],
				  tmpFluSpecimen.[sender_lab_id],
				  tmpFluSpecimen.[year_collected], 
				  tmpFluSpecimen.[duplicate], 
				  tmpFluSpecimen.[reason_for_submission], 
				  tmpFluSpecimen.[date_login],
				  County.[Id],
				  tmpFluSpecimen.[date_contract_lab_transfer],
				  tmpFluSpecimen.[nursing_home],
				  tmpFluSpecimen.[patient_vaccinated],
				  CASE tmpFluSpecimen.[shipping_condition]
					 WHEN ''
						THEN
						   NULL
						ELSE UPPER( LEFT( tmpFluSpecimen.[shipping_condition], 1 ) ) +
							  SUBSTRING( tmpFluSpecimen.[shipping_condition], 2, LEN( tmpFluSpecimen.[shipping_condition] ) )
				  END AS [shipping_condition], 
				  tmpFluSpecimen.[travel],
				  tmpFluSpecimen.[travel_location], 
				  tmpFluSpecimen.[package_id],
				  CASE tmpFluSpecimen.[collection_method]
					 WHEN ''
						THEN
						   NULL
					  ELSE UPPER( tmpFluSpecimen.[collection_method] )
				  END AS [collection_method]
			 FROM [dbo].##tmpFluSpecimen AS tmpFluSpecimen
				   LEFT OUTER JOIN [dbo].Country AS Country
					 ON tmpFluSpecimen.country_id = Country.ISO
				   LEFT OUTER JOIN [dbo].County AS County
					 ON tmpFluSpecimen.county_id = County.Code
				   LEFT OUTER JOIN [dbo].State AS State
					 ON tmpFluSpecimen.state_id = State.Abbrev )
				     
	MERGE [dbo].Specimen AS Specimen
	USING cteFluSpecimen AS cteFluSpecimen
	   ON Specimen.Id = cteFluSpecimen.Id
	 WHEN MATCHED THEN
		  UPDATE
			 SET Specimen.[CDCId] = cteFluSpecimen.[CDCId], 
				 Specimen.[SpecimenId] = cteFluSpecimen.[SpecimenId], 
				 Specimen.[StrainName] = cteFluSpecimen.[StrainName], 
				 Specimen.[IsDuplicate] = cteFluSpecimen.[IsDuplicate], 
				 Specimen.[IsReassortant] = cteFluSpecimen.[IsReassortant], 
				 Specimen.[IsRecombinant] = cteFluSpecimen.[IsRecombinant], 
				 Specimen.[IsNegative] = cteFluSpecimen.[IsNegative], 
				 Specimen.[IsColdAdapted] = cteFluSpecimen.[IsColdAdapted], 
				 Specimen.[IsRNAOnly] = cteFluSpecimen.[IsRNAOnly], 
				 Specimen.[IsCDNAOnly] = cteFluSpecimen.[IsCDNAOnly], 
				 Specimen.[HostId] = cteFluSpecimen.[HostId], 
				 Specimen.[DateCollected] = cteFluSpecimen.[DateCollected], 
				 Specimen.[YearCollected] = cteFluSpecimen.[YearCollected], 
				 Specimen.[DateReceived] = cteFluSpecimen.[DateReceived], 
				 Specimen.[DateContractLabTransfer] = cteFluSpecimen.[DateContractLabTransfer], 
				 Specimen.[DateLogin] = cteFluSpecimen.[DateLogin], 
				 Specimen.[IsCollectionDateEstimated] = cteFluSpecimen.[IsCollectionDateEstimated], 
				 Specimen.[Activity] = cteFluSpecimen.[Activity], 
				 Specimen.[CollectionMethod] = cteFluSpecimen.[CollectionMethod], 
				 Specimen.[CountryId] = cteFluSpecimen.[CountryId], 
				 Specimen.[StateId] = cteFluSpecimen.[StateId], 
				 Specimen.[CountyId] = cteFluSpecimen.[CountyId], 
				 Specimen.[SenderCode] = cteFluSpecimen.[SenderCode], 
				 Specimen.[PassageReceived] = cteFluSpecimen.[PassageReceived], 
				 Specimen.[CollectionComment] = cteFluSpecimen.[CollectionComment], 
				 Specimen.[IsOriginal] = cteFluSpecimen.[IsOriginal], 
				 Specimen.[IsVaccine] = cteFluSpecimen.[IsVaccine], 
				 Specimen.[ContractLab] = cteFluSpecimen.[ContractLab], 
				 Specimen.[PackageId] = cteFluSpecimen.[PackageId], 
				 Specimen.[PatientAge] = cteFluSpecimen.[PatientAge], 
				 Specimen.[PatientGender] = cteFluSpecimen.[PatientGender], 
				 Specimen.[PatientLocation] = cteFluSpecimen.[PatientLocation], 
				 Specimen.[IsPatientDeceased] = cteFluSpecimen.[IsPatientDeceased], 
				 Specimen.[IsPatientVaccinated] = cteFluSpecimen.[IsPatientVaccinated], 
				 Specimen.[IsNursingHome] = cteFluSpecimen.[IsNursingHome], 
				 Specimen.[SpecialStudy] = cteFluSpecimen.[SpecialStudy], 
				 Specimen.[GeneticScreen] = cteFluSpecimen.[GeneticScreen], 
				 Specimen.[DatePCR] = cteFluSpecimen.[DatePCR], 
				 Specimen.[LabComment] = cteFluSpecimen.[LabComment], 
				 Specimen.[ReasonForSubmission] = cteFluSpecimen.[ReasonForSubmission], 
				 Specimen.[ShippingCondition] = cteFluSpecimen.[ShippingCondition], 
				 Specimen.[IsTravel] = cteFluSpecimen.[IsTravel], 
				 Specimen.[TravelLocation] = cteFluSpecimen.[TravelLocation], 
				 Specimen.[IsReagent] = cteFluSpecimen.[IsReagent], 
				 Specimen.[ReagentClass] = cteFluSpecimen.[ReagentClass], 
				 Specimen.[SenderLabId] = cteFluSpecimen.[SenderLabId] 
	 WHEN NOT MATCHED THEN
		  INSERT ( [Id], 
				   [CDCId], 
				   [SpecimenId], 
				   [StrainName], 
				   [IsDuplicate], 
				   [IsReassortant], 
				   [IsRecombinant], 
				   [IsNegative], 
				   [IsColdAdapted], 
				   [IsRNAOnly], 
				   [IsCDNAOnly], 
				   [HostId], 
				   [DateCollected], 
				   [YearCollected], 
				   [DateReceived], 
				   [DateContractLabTransfer], 
				   [DateLogin], 
				   [IsCollectionDateEstimated], 
				   [Activity], 
				   [CollectionMethod], 
				   [CountryId], 
				   [StateId], 
				   [CountyId], 
				   [SenderCode], 
				   [PassageReceived], 
				   [CollectionComment], 
				   [IsOriginal], 
				   [IsVaccine], 
				   [ContractLab], 
				   [PackageId], 
				   [PatientAge], 
				   [PatientGender], 
				   [PatientLocation], 
				   [IsPatientDeceased], 
				   [IsPatientVaccinated], 
				   [IsNursingHome], 
				   [SpecialStudy], 
				   [GeneticScreen], 
				   [DatePCR], 
				   [LabComment], 
				   [ReasonForSubmission], 
				   [ShippingCondition], 
				   [IsTravel], 
				   [TravelLocation], 
				   [IsReagent], 
				   [ReagentClass], 
				   [SenderLabId] )
		  VALUES ( cteFluSpecimen.[Id], 
				   cteFluSpecimen.[CDCId], 
				   cteFluSpecimen.[SpecimenId], 
				   cteFluSpecimen.[StrainName], 
				   cteFluSpecimen.[IsDuplicate], 
				   cteFluSpecimen.[IsReassortant], 
				   cteFluSpecimen.[IsRecombinant], 
				   cteFluSpecimen.[IsNegative], 
				   cteFluSpecimen.[IsColdAdapted], 
				   cteFluSpecimen.[IsRNAOnly], 
				   cteFluSpecimen.[IsCDNAOnly], 
				   cteFluSpecimen.[HostId], 
				   cteFluSpecimen.[DateCollected], 
				   cteFluSpecimen.[YearCollected], 
				   cteFluSpecimen.[DateReceived], 
				   cteFluSpecimen.[DateContractLabTransfer], 
				   cteFluSpecimen.[DateLogin], 
				   cteFluSpecimen.[IsCollectionDateEstimated], 
				   cteFluSpecimen.[Activity], 
				   cteFluSpecimen.[CollectionMethod], 
				   cteFluSpecimen.[CountryId], 
				   cteFluSpecimen.[StateId], 
				   cteFluSpecimen.[CountyId], 
				   cteFluSpecimen.[SenderCode], 
				   cteFluSpecimen.[PassageReceived], 
				   cteFluSpecimen.[CollectionComment], 
				   cteFluSpecimen.[IsOriginal], 
				   cteFluSpecimen.[IsVaccine], 
				   cteFluSpecimen.[ContractLab], 
				   cteFluSpecimen.[PackageId], 
				   cteFluSpecimen.[PatientAge], 
				   cteFluSpecimen.[PatientGender], 
				   cteFluSpecimen.[PatientLocation], 
				   cteFluSpecimen.[IsPatientDeceased], 
				   cteFluSpecimen.[IsPatientVaccinated], 
				   cteFluSpecimen.[IsNursingHome], 
				   cteFluSpecimen.[SpecialStudy], 
				   cteFluSpecimen.[GeneticScreen], 
				   cteFluSpecimen.[DatePCR], 
				   cteFluSpecimen.[LabComment], 
				   cteFluSpecimen.[ReasonForSubmission], 
				   cteFluSpecimen.[ShippingCondition], 
				   cteFluSpecimen.[IsTravel], 
				   cteFluSpecimen.[TravelLocation], 
				   cteFluSpecimen.[IsReagent], 
				   cteFluSpecimen.[ReagentClass], 
				   cteFluSpecimen.[SenderLabId] );

	SET IDENTITY_INSERT [dbo].Specimen OFF;
END;

RETURN(0)
