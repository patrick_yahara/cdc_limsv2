CREATE PROCEDURE [dbo].[usp_CreatetmpFluHITestEntry_tmpEntryType]
WITH EXECUTE AS CALLER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure creates global temporary tables, ##tmpEntryType and 
		##tmpFluHITestEntry. Both tables hold data from DISC.Flu_HI_Test_Entry. Both tables 
		are used to facilitate MERGE statements.
             
    PARAMETERS
		None
		            
	RETURN VALUE 
		 0 - No Error. 
         
    PROGRAMMING NOTES 
		This procedure is called from SSIS package HITestEntry. The DISC database is PostgreSQL.
		
    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpEntryType
  			##tmpFluHITestEntry

    EXAMPLE CALL(5)
		EXEC [dbo].usp_CreateHITestEntryTmpTables
         
    CHANGE HISTORY 
		2014-03-11 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
		2014-04-01 - Cesar Sala - Added identity column to tmpEntryType to help ensure that 
					 constant Entry Types that are inserted for use by PanOpTES always 
					 maintain the proper order.
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON


IF OBJECT_ID('TempDB..##tmpEntryType') IS NOT NULL
	DROP TABLE ##tmpEntryType;

CREATE TABLE ##tmpEntryType
	( [Id] INT NOT NULL IDENTITY( 1,1 ), 
	  [EntryType] NVARCHAR(70) NOT NULL

	  PRIMARY KEY CLUSTERED 
	  (
		[Id] ASC
	  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

IF OBJECT_ID('TempDB..##tmpFluHITestEntry') IS NOT NULL
	DROP TABLE ##tmpFluHITestEntry;

CREATE TABLE ##tmpFluHITestEntry
	( [id] INT NOT NULL,
	  [hi_test_id] INT NOT NULL,
	  [position] INT NOT NULL,
	  [isolate_id] INT,
	  [entry_type] NVARCHAR(70) NOT NULL,
	  [description] NVARCHAR(250) NOT NULL,
	  [error] NVARCHAR(250) NOT NULL,
	  [clade] NVARCHAR(90) NOT NULL,
	  [titer] INT,
	  [back_titer] SMALLINT,
	  [do_not_report] BIT NOT NULL
	  
	  PRIMARY KEY CLUSTERED 
	  (
		[Id] ASC
	  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

RETURN(0)
