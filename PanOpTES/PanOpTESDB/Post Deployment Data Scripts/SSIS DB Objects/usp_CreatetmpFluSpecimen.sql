CREATE PROCEDURE [dbo].[usp_CreatetmpFluSpecimen]
WITH EXECUTE AS OWNER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure creates the global temporary table, ##tmpFluSpecimen. The table holds data 
		from DISC.Flu_Specimen and is used to facilitate a MERGE.

    PARAMETERS
		None

	RETURN VALUE 
		0 - No Error. 

    PROGRAMMING NOTES 
		This procedure is called from the Specimen SSIS package.

    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpFluSpecimen

    EXAMPLE CALL(5)
		EXEC [dbo].usp_CreatetmpFluSpecimen

    CHANGE HISTORY 
		2014-03-21 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON


IF OBJECT_ID('TempDB..##tmpFluSpecimen') IS NOT NULL
	DROP TABLE ##tmpFluSpecimen;

CREATE TABLE ##tmpFluSpecimen
	( [id] INT, 
	  [cdc_id] NVARCHAR(50), 
	  [specimen_id] NVARCHAR(150), 
	  [strain_name] NVARCHAR(170), 
	  [reassortant] BIT, 
	  [negative] BIT, 
	  [cold_adapted] BIT, 
	  [rna_only] BIT, 
	  [cdna_only] BIT, 
	  [host_id] INT, 
	  [date_collected] DATE,
	  [date_received] DATE,
	  [country_id] NVARCHAR(10),
	  [state_id] NVARCHAR(10),
	  [reagent] BIT, 
	  [reagent_class] NVARCHAR(50), 
	  [passage_received] NVARCHAR(150), 
	  [collection_comment] NVARCHAR(150), 
	  [original] BIT, 
	  [vaccine] BIT, 
	  [sender_code] INT,
	  [patient_age] INT,
	  [patient_gender] NVARCHAR(10), 
	  [patient_location] NVARCHAR(350), 
	  [patient_deceased] BIT,
	  [special_study] NVARCHAR(150), 
	  [genetic_screen] NVARCHAR(250), 
	  [activity] NVARCHAR(50), 
	  [recombinant] BIT, 
	  [lab_comment] NVARCHAR(250), 
	  [contract_lab_id] NVARCHAR(20),
	  [date_pcr] DATE,
	  [sender_lab_id] INT,
	  [year_collected] NVARCHAR(20), 
	  [duplicate] BIT, 
	  [estimated_collection_date] DATE,
	  [reason_for_submission] NVARCHAR(110), 
	  [date_login] DATE,
	  [county_id] NVARCHAR(20),
	  [date_contract_lab_transfer] DATE,
	  [nursing_home] BIT,
	  [patient_vaccinated] BIT,
	  [shipping_condition] NVARCHAR(90), 
	  [travel] BIT,
	  [travel_location] NVARCHAR(250), 
	  [package_id] INT,
	  [collection_method] NVARCHAR(70) 

	  PRIMARY KEY CLUSTERED 
	  (
		[Id] ASC
	  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

RETURN(0)
