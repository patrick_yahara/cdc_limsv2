CREATE PROCEDURE [dbo].[usp_CreatetmpFluIsolate]
WITH EXECUTE AS CALLER
AS
/***********************************************************************************************
	DESCRIPTION
		This procedure creates the global temporary table, ##tmpFluIsolate. The table holds data 
		from DISC.Flu_Isolate and is used to facilitate a MERGE.

    PARAMETERS
		None

	RETURN VALUE 
		0 - No Error. 

    PROGRAMMING NOTES 
		This procedure is called from the Specimen SSIS package.

    DEPENDENCIES
		Tables/Views used in procedure:
			##tmpFluIsolate

    EXAMPLE CALL(5)
		EXEC [dbo].usp_CreatetmpFluIsolate

    CHANGE HISTORY 
		2014-03-25 - Cesar Sala - Created by Cesar Sala (CSala@cdc.gov).
		2014-03-26 - Cesar Sala - Added sampleclass column.
***********************************************************************************************/
SET NOCOUNT ON
SET XACT_ABORT ON


IF OBJECT_ID('TempDB..##tmpFluIsolate') IS NOT NULL
	DROP TABLE ##tmpFluIsolate;

CREATE TABLE ##tmpFluIsolate
	( [id] INT, 
	  [specimen_id] INT, 
	  [passage] NVARCHAR(150), 
	  [date_harvested] DATE, 
	  [vaccine] BIT, 
	  [isolate_received] BIT, 
	  [sequencing_storage] NVARCHAR(90), 
	  [aa_res_adam] NVARCHAR(110), 
	  [aa_res_nai] NVARCHAR(250), 
	  [down_to_vaccine] NVARCHAR(90),
	  [contract_lab_id] NVARCHAR(20),
	  [lot] NVARCHAR(90),
	  [calculated_subtype] NVARCHAR(110),
	  [date_contract_lab_transfer] DATE,
	  [initial_storage] NVARCHAR(90), 
	  [sampleclass] NVARCHAR(50)

	  PRIMARY KEY CLUSTERED 
	  (
		[id] ASC
	  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

RETURN(0)
