﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*
	CHANGE HISTORY:
		03.07.2014 - Cesar Sala (CSala@cdc.gov) - Commented out EntryType data MERGE as the data load is now performed via SSIS.
		03.13.2014 - Cesar Sala - Reinserted MERGE with a server name check to allow for insertion of development data on servers 
					 that are not named 'SLDB-DEV\STARLIMS', i.e. other local servers.
*/
DECLARE	@ServerName NVARCHAR(50);

SET @ServerName = CONVERT (sysname, SERVERPROPERTY ('servername') );

IF @ServerName <> 'SLDB-DEV\STARLIMS'
BEGIN
	MERGE [dbo].[EntryType] as targ
	USING ( VALUES 
	('reference', 'REFERENCE')
	, ('test', 'TEST')
	, ('st_ctrl', 'STCTRL')
	, ('serum_ctrl', 'SERUMCTRL')
	, ('project', 'PROJECT')
	, ('reference2', 'REFERENCE2')
	) AS src ([DisplayName], [SystemName])
	ON targ.[SystemName] = src.[SystemName]
	WHEN MATCHED AND (targ.[DisplayName] <> src.[DisplayName] OR targ.[SystemName] <> src.[SystemName]) THEN
		UPDATE SET targ.[DisplayName] = src.[DisplayName], targ.[SystemName] = src.[SystemName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [DisplayName], [SystemName])
		VALUES ( src.[DisplayName], src.[SystemName]);
END;
GO