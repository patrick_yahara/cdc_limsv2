﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*
	CHANGE HISTORY:
		02.27.2014 - Cesar Sala (CSala@cdc.gov) - Commented out Specimen data MERGE as the data load is now performed via SSIS.
		02.27.2014 - Cesar Sala (CSala@cdc.gov) - Commented out Subsample data MERGE as the data load is now performed via SSIS.
		03.07.2014 - Cesar Sala (CSala@cdc.gov) - Commented out HITest data MERGE as the data load is now performed via SSIS.
		03.12.2014 - Cesar Sala (CSala@cdc.gov) - Commented out HITestEntry data MERGE as the data load is now performed via SSIS.
		03.13.2014 - Cesar Sala - Reinserted MERGE with a server name check to allow for insertion of development data on servers 
					 that are not named 'SLDB-DEV\STARLIMS', i.e. other local servers.
*/
DECLARE @ServerName NVARCHAR(50);

SET @ServerName = CONVERT (sysname, SERVERPROPERTY ('servername') );

IF @ServerName <> 'SLDB-DEV\STARLIMS'
BEGIN
	--[flu_hi_test]
	SET IDENTITY_INSERT [dbo].[HITest] ON
	MERGE [dbo].[HITest] as targ
	USING ( VALUES 
	(1486,'07 NOV 2013','hi-tests/2014/h3/11-7-13.xls','HEMAGGLUTINATION INHIBITION REACTIONS OF INFLUENZA H3 VIRUSES','')
	) AS src ([Id], [DateTested], [TestFileName], [TestDescription], [SubType])
	ON targ.[Id] = src.[Id]
	WHEN MATCHED AND (targ.[DateTested] <> src.[DateTested] OR targ.[TestFileName] <> src.[TestFileName] OR targ.[TestDescription] <> src.[TestDescription] OR targ.[SubType] <> src.[SubType]) THEN
		UPDATE SET targ.[DateTested] = src.[DateTested], targ.[TestFileName] = src.[TestFileName], targ.[TestDescription] = src.[TestDescription], targ.[SubType] = src.[SubType]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Id], [DateTested], [TestFileName], [TestDescription], [SubType])
		VALUES ( src.[Id], src.[DateTested], src.[TestFileName], src.[TestDescription], src.[SubType])
	;
	SET IDENTITY_INSERT [dbo].[HITest] OFF
	-- end [flu_hi_test]

	-- flu_specimen
	SET IDENTITY_INSERT [dbo].[Specimen] ON
	MERGE [dbo].[Specimen] as targ
	USING ( VALUES 
	(149103,'2012784336','A/VICTORIA/361/2011','A/Victoria/361/2011','true','false','false','false','false','false','false',10,'24 OCT 2011','','12 MAR 2012',NULL,'12 MAR 2012','false','','', NULL, NULL, NULL,'508','C2','TC','false','false',NULL,'10368',NULL,'','Victoria Australia',NULL,NULL,NULL,'','Influenza A/H3',NULL,'VW# 62294','other','frozen',NULL,'','false','','163')
	,(167614,'2014700072','485162','','false','false','false','false','false','false','false',10,'06 JUL 2013','','11 OCT 2013',NULL,'11 OCT 2013','false','','', NULL, NULL, NULL,'963','C3','TC','false','false',NULL,'11683',0,'M','Dhaka Bangladesh',NULL,NULL,NULL,'','',NULL,'6 MONTH OLD','surveillance','frozen',NULL,'','false','','181')
	,(167667,'2014700148','1312723','A/India/2723/2013','false','false','false','false','false','false','false',10,'05 JUL 2013','','16 OCT 2013',NULL,'16 OCT 2013','false','','', NULL, NULL, NULL,'536','C2','TC','false','false',NULL,'11684',3,'M','Nagpur, India',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','60')
	,(167879,'2014700187','A/HONG KONG/4985/2013','','false','false','false','false','false','false','false',10,'17 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',7,'F','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(50595,'2003714420','2003100054/ORIGINAL','A/Wyoming/03/2003','true','false','false','false','false','false','false',10,'13 FEB 2003','','27 FEB 2003',NULL,'27 FEB 2003','false','sporadic','', NULL, NULL, NULL,'32','','ORIGINAL','true','false',NULL,'4301',20,'','WY',NULL,NULL,NULL,'','Influenza A/H3',NULL,'','','frozen',NULL,'','false','','114')
	,(166505,'2013761058','A/NIAKHAR/6939/2012 ORIGINAL','A/Niakhar/6939/2012','false','false','false','false','false','false','false',10,'24 OCT 2012','','05 AUG 2013',NULL,'06 AUG 2013','false','epidemic','', NULL, NULL, NULL,'123','','ORIGINAL','true','false',NULL,'11495',2,'M','Niakhar Senegal',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','',NULL)
	,(152485,'2012704894','AVI1200405','A/Texas/50/2012','false','false','false','false','false','false','false',10,'15 APR 2012','','16 MAY 2012','30 MAY 2012','16 MAY 2012','false','','', NULL, NULL, NULL,'63','M1','TC','false','false','CA','10187',0,'M','Fort Worth, TX',NULL,NULL,NULL,'','Influenza A/H3',NULL,'11 MONTHS OLD','surveillance','frozen',NULL,'','false','','143')
	,(167668,'2014700149','1311318','','false','false','false','false','false','false','false',10,'17 JUN 2013','','16 OCT 2013',NULL,'16 OCT 2013','false','','', NULL, NULL, NULL,'536','C1','TC','false','false',NULL,'11684',20,'M','Kerala, India',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','60')
	,(154208,'2012787893','30832 ORIGINAL','A/Puerto Rico/36/2012','false','false','false','false','false','false','false',10,'27 JUN 2012','','18 JUL 2012',NULL,'18 JUL 2012','false','sporadic','nw', NULL, NULL, NULL,'209','','NASAL WASH','true','false',NULL,'10589',2,'M','San Juan Puerto Rico',NULL,NULL,NULL,'','Influenza A/H3',NULL,'','surveillance','frozen',NULL,'','false','','203')
	,(167681,'2014703523','IDR13-30596','','false','false','false','false','false','false','false',10,'01 OCT 2013','','16 OCT 2013','25 OCT 2013','16 OCT 2013','false','','', NULL, NULL, NULL,'27','C2','TC','false','false','WI','11687',84,'M','New York Co., NY','false',NULL,'false','','',NULL,'','surveillance','frozen',NULL,'','false','','31')
	,(167863,'2014704579','100413-0989 ORIGINAL','A/North Carolina/11/2013','false','false','false','false','false','false','false',10,'03 OCT 2013','','18 OCT 2013','05 NOV 2013','18 OCT 2013','false','','np', NULL, NULL, NULL,'261','','NP & TS','true','false','UT','11725',21,'F','Forsyth Co., NC',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','51')
	,(166860,'2013762535','A/VICTORIA/505/2013','A/Victoria/505/2013','false','false','false','false','false','false','false',10,'17 JUN 2013','','16 AUG 2013',NULL,'19 AUG 2013','false','','', NULL, NULL, NULL,'508','E4','EG','false','true',NULL,'11618',NULL,'','Victoria Australia',NULL,NULL,NULL,'','Influenza A/H3',NULL,'SEROLOGY ANTIGEN SH 2013','other','frozen',NULL,'','false','','163')
	,(157968,'2013701044','A/WISCONSIN/67/2005','','false','false','false','false','false','false','false',NULL,NULL,'','06 DEC 2012',NULL,'07 DEC 2012','false','','ot', NULL, NULL, NULL,'508','REAGENT','','false','false',NULL,'10904',NULL,'','',NULL,NULL,NULL,'','',NULL,'INACTIVATED VIRUS FOR H3 CONTROL IN HI TEST','other','cold',NULL,'','true','','163')
	,(167835,'2014700173','WA0192620','A/Washington/10/2013','false','false','false','false','false','false','false',10,'07 OCT 2013','','24 OCT 2013',NULL,'24 OCT 2013','false','sporadic','', NULL, NULL, NULL,'80','C1','','false','false',NULL,'11716',78,'F','Burien, King Co., WA','false',NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','84')
	,(167874,'2014700182','A/HONG KONG/4936/2013','','false','false','false','false','false','false','false',10,'15 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',8,'M','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(167849,'2014703534','M13017590 ORIGINAL','A/Pennsylvania/08/2013','false','false','false','false','false','false','false',10,'06 OCT 2013','','18 OCT 2013','01 NOV 2013','18 OCT 2013','false','sporadic','np', NULL, NULL, NULL,'257','','NP','true','false','WI','11721',58,'F','Dauphin Co., PA','false',NULL,NULL,'','',NULL,'FLUA21.6 H3-21.7','surveillance','frozen',NULL,'','false','','145')
	,(167877,'2014700185','A/HONG KONG/4946/2013','','false','false','false','false','false','false','false',10,'17 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',79,'M','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(167887,'2014700195','A/HONG KONG/5648/2013','A/Hong Kong/5648/2013','false','false','false','false','false','false','false',10,'14 OCT 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',4,'F','',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(152484,'2012704893','AVI1200405 ORIGINAL','A/Texas/50/2012','true','false','false','false','false','false','false',10,'15 APR 2012','','16 MAY 2012','22 MAY 2012','16 MAY 2012','false','','np', NULL, NULL, NULL,'63','','NP WASH','true','false','CA','10187',0,'M','Fort Worth, TX',NULL,NULL,NULL,'','Influenza A/H3',NULL,'11 MONTHS OLD','surveillance','frozen',NULL,'','false','','143')
	,(167876,'2014700184','A/HONG KONG/4943/2013','','false','false','false','false','false','false','false',10,'16 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',5,'F','New Territories Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(167844,'2014703530','13VR009184 ORIGINAL','A/Wisconsin/14/2013','false','false','false','false','false','false','false',10,'12 OCT 2013','','17 OCT 2013','25 OCT 2013','17 OCT 2013','false','','ns', NULL, NULL, NULL,'25','','NASAL SWAB','true','false','WI','11719',0,'F','Medford, WI',NULL,NULL,NULL,'','',NULL,'FLUA34 H3-35.1 QUICK VIEW-A .2YR','surveillance','cold',NULL,'','false','','103')
	,(167815,'2014705508','176813 ORIGINAL','','false','false','false','false','false','false','false',10,'20 JUL 2013','','08 OCT 2013','23 OCT 2013','09 OCT 2013','false','outbreak','np', NULL, NULL, NULL,'83','','NP SWAB','true','false','CA','11715',30,'M','Juneau, AK',NULL,NULL,NULL,'','',NULL,'CDC CRUISE RESP SURV PILOT PROG.','surveillance','cold',NULL,'','false','','83')
	,(162862,'2013758692','130004786 ORIGINAL','A/American Samoa/4786/2013','false','false','false','false','false','false','false',10,'22 FEB 2013','','22 MAR 2013',NULL,'22 MAR 2013','false','','np', NULL, NULL, NULL,'84','','NP','true','false',NULL,'11403',0,'','','false','false','false','','Influenza A/H3',NULL,'ONSET DATE 2/22/13; 9 MONTH OLD','surveillance','frozen',NULL,'','false','','88')
	,(167660,'2014700151','1317305','','false','false','false','false','false','false','false',10,'30 MAY 2013','','16 OCT 2013',NULL,'16 OCT 2013','false','','', NULL, NULL, NULL,'536','C1','','false','false',NULL,'11684',30,'M','Kolkata, India',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','60')
	,(167873,'2014700181','A/HONG KONG/4928/2013','A/Hong Kong/4928/2013','false','false','false','false','false','false','false',10,'17 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',5,'F','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(167661,'2014700142','1313543','A/India/3543/2013','false','false','false','false','false','false','false',10,'12 AUG 2013','','16 OCT 2013',NULL,'16 OCT 2013','false','','', NULL, NULL, NULL,'536','C2','TC','false','false',NULL,'11684',6,'F','Pune, India',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','60')
	,(166513,'2013761066','A/NIAKHAR/6990/2012 ORIGINAL','A/Niakhar/6990/2012','false','false','false','false','false','false','false',10,'30 OCT 2012','','05 AUG 2013',NULL,'06 AUG 2013','false','epidemic','', NULL, NULL, NULL,'123','','ORIGINAL','true','false',NULL,'11495',3,'F','Niakhar Senegal',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','',NULL)
	,(167697,'2014700159','35468 ORIGINAL','A/Puerto Rico/19/2013','false','false','false','false','false','false','false',10,'15 SEP 2013','','17 OCT 2013',NULL,'17 OCT 2013','false','sporadic','ts', NULL, NULL, NULL,'209','','THROAT SWAB','true','false',NULL,'11695',5,'M','Dorado Puerto Rico',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','203')
	,(167833,'2014705515','N1300574201 ORIGINAL','','false','false','false','false','false','false','false',10,'30 SEP 2013','','09 OCT 2013','30 OCT 2013','09 OCT 2013','false','','ns', NULL, NULL, NULL,'61','','NASAL SWAB','true','false','CA','11730',20,'F','Baton Rouge, LA',NULL,NULL,'false','','',NULL,'','surveillance','frozen',NULL,'','false','','141')
	,(167829,'2014705516','CL2013-00021188 ORIGINAL','A/Nevada/07/2013','false','false','false','false','false','false','false',10,'08 OCT 2013','','15 OCT 2013','30 OCT 2013','15 OCT 2013','false','sporadic','np', NULL, NULL, NULL,'78','','NP SWAB','true','false','CA','11712',69,'F','Washoe Co., NV',NULL,NULL,NULL,'','',NULL,'FIRST POSITIVE. ROOM TEMP.','surveillance','warm',NULL,'','false','','216')
	,(167843,'2014703529','13VR009143 ORIGINAL','','false','false','false','false','false','false','false',10,'09 OCT 2013','','17 OCT 2013','25 OCT 2013','17 OCT 2013','false','','ns', NULL, NULL, NULL,'25','','NASAL SWAB','true','false','WI','11719',23,'M','Medford, WI',NULL,NULL,NULL,'','',NULL,'FLUA29.5 H3-22 FLUA QUICK VIEW','surveillance','cold',NULL,'','false','','103')
	,(157966,'2013701042','A/VICTORIA/210/2009','','false','false','false','false','false','false','false',NULL,NULL,'','06 DEC 2012',NULL,'07 DEC 2012','false','','ot', NULL, NULL, NULL,'508','REAGENT','','false','false',NULL,'10904',NULL,'','',NULL,NULL,NULL,'','',NULL,'INACTIVATED VIRUS FOR H3 CONTROL IN HI TEST','other','cold',NULL,'','true','','163')
	,(157967,'2013701043','A/VICTORIA/361/2011','','false','false','false','false','false','false','false',NULL,NULL,'','06 DEC 2012',NULL,'07 DEC 2012','false','','ot', NULL, NULL, NULL,'508','REAGENT','','false','false',NULL,'10904',NULL,'','',NULL,NULL,NULL,'','',NULL,'INACTIVATED VIRUS FOR H3 CONTROL IN HI TEST','other','cold',NULL,'','true','','163')
	,(167821,'2014705519','13V-19065 ORIGINAL','','false','false','false','false','false','false','false',10,'09 OCT 2013','','16 OCT 2013','30 OCT 2013','17 OCT 2013','false','','np', NULL, NULL, NULL,'88','','NP SWAB','true','false','CA','11731',95,'F','Seattle, WA',NULL,NULL,'true','','',NULL,'','surveillance','frozen',NULL,'','false','','78')
	,(166511,'2013761064','A/NIAKHAR/6986/2012 ORIGINAL','A/Niakhar/6986/2012','false','false','false','false','false','false','false',10,'30 OCT 2012','','05 AUG 2013',NULL,'06 AUG 2013','false','epidemic','', NULL, NULL, NULL,'123','','ORIGINAL','true','false',NULL,'11495',4,'F','Niakhar Senegal',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','',NULL)
	,(167832,'2014705514','179603 ORIGINAL','','false','false','false','false','false','false','false',10,'07 SEP 2013','','08 OCT 2013','30 OCT 2013','09 OCT 2013','false','outbreak','np', NULL, NULL, NULL,'83','','NP SWAB','true','false','CA','11715',65,'M','Whittier, AK',NULL,NULL,NULL,'','',NULL,'CDC CRUISE RESP SURV PILOT PROG.','surveillance','cold',NULL,'','false','','83')
	,(167823,'2014705522','N1300586901 ORIGINAL','A/Louisiana/09/2013','false','false','false','false','false','false','false',10,'07 OCT 2013','','17 OCT 2013','30 OCT 2013','17 OCT 2013','false','','np', NULL, NULL, NULL,'61','','NP SWAB','true','false','CA','11710',22,'M','Shreveport, LA',NULL,NULL,'false','','',NULL,'A 26.6, H3 28.1','surveillance','frozen',NULL,'','false','','141')
	,(147234,'2012702646','A/VICTORIA/361/2011','A/Victoria/361/2011','false','false','false','false','false','false','false',10,'24 OCT 2011','','23 JAN 2012',NULL,'23 JAN 2012','false','','', NULL, NULL, NULL,'508','E3','EG','false','true',NULL,'9905',NULL,'','',NULL,NULL,NULL,'','Influenza A/H3',NULL,'SEEDLOT#1110498','surveillance','frozen',NULL,'','false','','163')
	,(167878,'2014700186','A/HONG KONG/4950/2013','','false','false','false','false','false','false','false',10,'17 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',79,'F','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	,(166360,'2013760946','14C0030002 ORIGINAL','A/Georgia/03/2013','false','false','false','false','false','false','false',10,'14 JUL 2013','','31 JUL 2013',NULL,'01 AUG 2013','false','','np', NULL, NULL, NULL,'48','','NP','true','false',NULL,'11489',19,'','Marrietta, GA','false',NULL,NULL,'','Influenza A/H3',NULL,'','surveillance','cold',NULL,'','false','','192')
	,(167617,'2014700075','334089','','false','false','false','false','false','false','false',10,'16 JUL 2013','','11 OCT 2013',NULL,'11 OCT 2013','false','','', NULL, NULL, NULL,'963','C3','TC','false','false',NULL,'11683',6,'F','Dhaka Bangladesh',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','181')
	,(167678,'2014703525','IDR13-30597','','false','false','false','false','false','false','false',10,'28 SEP 2013','','16 OCT 2013','25 OCT 2013','16 OCT 2013','false','','', NULL, NULL, NULL,'27','C1','TC','false','false','WI','11687',2,'M','New York Co., NY','false',NULL,'false','','',NULL,'','surveillance','frozen',NULL,'','false','','31')
	,(157315,'2013700547','IDR12-34376 ORIGINAL','A/New York/39/2012','false','false','false','false','false','false','false',10,'20 OCT 2012','','08 NOV 2012',NULL,'08 NOV 2012','false','sporadic','ns', NULL, NULL, NULL,'27','','NASAL SWAB','true','false',NULL,'10845',5,'M','Jefferson Co., NY',NULL,NULL,NULL,'','Influenza A/H3',NULL,'','surveillance','frozen',NULL,'','false','','31')
	,(167875,'2014700183','A/HONG KONG/4939/2013','','false','false','false','false','false','false','false',10,'16 SEP 2013','','30 OCT 2013',NULL,'30 OCT 2013','false','outbreak','', NULL, NULL, NULL,'531','CX','','false','false',NULL,'11733',8,'F','Kowloon Hong Kong',NULL,NULL,NULL,'','',NULL,'','surveillance','frozen',NULL,'','false','','139')
	) AS src ([Id], [CDCId], [SpecimenId], [StrainName], [IsDuplicate], [IsReassortant], [IsRecombinant], [IsNegative], [IsColdAdapted], [IsRNAOnly], [IsCDNAOnly], [HostId], [DateCollected], [YearCollected], [DateReceived], [DateContractLabTransfer], [DateLogin], [IsCollectionDateEstimated], [Activity], [CollectionMethod], [CountryId], [StateId], [CountyId], [SenderCode], [PassageReceived], [CollectionComment], [IsOriginal], [IsVaccine], [ContractLab], [PackageId], [PatientAge], [PatientGender], [PatientLocation], [IsPatientDeceased], [IsPatientVaccinated], [IsNursingHome], [SpecialStudy], [GeneticScreen], [DatePCR], [LabComment], [ReasonForSubmission], [ShippingCondition], [IsTravel], [TravelLocation], [IsReagent], [ReagentClass], [SenderLabId])
	ON targ.[Id] = src.[Id]
	WHEN MATCHED AND (targ.[CDCId] <> src.[CDCId] OR targ.[SpecimenId] <> src.[SpecimenId] OR targ.[StrainName] <> src.[StrainName] OR targ.[IsDuplicate] <> src.[IsDuplicate] OR targ.[IsReassortant] <> src.[IsReassortant] OR targ.[IsRecombinant] <> src.[IsRecombinant] OR targ.[IsNegative] <> src.[IsNegative] OR targ.[IsColdAdapted] <> src.[IsColdAdapted] OR targ.[IsRNAOnly] <> src.[IsRNAOnly] OR targ.[IsCDNAOnly] <> src.[IsCDNAOnly] OR targ.[HostId] <> src.[HostId] OR targ.[DateCollected] <> src.[DateCollected] OR targ.[YearCollected] <> src.[YearCollected] OR targ.[DateReceived] <> src.[DateReceived] OR targ.[DateContractLabTransfer] <> src.[DateContractLabTransfer] OR targ.[DateLogin] <> src.[DateLogin] OR targ.[IsCollectionDateEstimated] <> src.[IsCollectionDateEstimated] OR targ.[Activity] <> src.[Activity] OR targ.[CollectionMethod] <> src.[CollectionMethod] OR targ.[CountryId] <> src.[CountryId] OR targ.[StateId] <> src.[StateId] OR targ.[CountyId] <> src.[CountyId] OR targ.[SenderCode] <> src.[SenderCode] OR targ.[PassageReceived] <> src.[PassageReceived] OR targ.[CollectionComment] <> src.[CollectionComment] OR targ.[IsOriginal] <> src.[IsOriginal] OR targ.[IsVaccine] <> src.[IsVaccine] OR targ.[ContractLab] <> src.[ContractLab] OR targ.[PackageId] <> src.[PackageId] OR targ.[PatientAge] <> src.[PatientAge] OR targ.[PatientGender] <> src.[PatientGender] OR targ.[PatientLocation] <> src.[PatientLocation] OR targ.[IsPatientDeceased] <> src.[IsPatientDeceased] OR targ.[IsPatientVaccinated] <> src.[IsPatientVaccinated] OR targ.[IsNursingHome] <> src.[IsNursingHome] OR targ.[SpecialStudy] <> src.[SpecialStudy] OR targ.[GeneticScreen] <> src.[GeneticScreen] OR targ.[DatePCR] <> src.[DatePCR] OR targ.[LabComment] <> src.[LabComment] OR targ.[ReasonForSubmission] <> src.[ReasonForSubmission] OR targ.[ShippingCondition] <> src.[ShippingCondition] OR targ.[IsTravel] <> src.[IsTravel] OR targ.[TravelLocation] <> src.[TravelLocation] OR targ.[IsReagent] <> src.[IsReagent] OR targ.[ReagentClass] <> src.[ReagentClass] OR targ.[SenderLabId] <> src.[SenderLabId]) THEN
		UPDATE SET targ.[CDCId] = src.[CDCId], targ.[SpecimenId] = src.[SpecimenId], targ.[StrainName] = src.[StrainName], targ.[IsDuplicate] = src.[IsDuplicate], targ.[IsReassortant] = src.[IsReassortant], targ.[IsRecombinant] = src.[IsRecombinant], targ.[IsNegative] = src.[IsNegative], targ.[IsColdAdapted] = src.[IsColdAdapted], targ.[IsRNAOnly] = src.[IsRNAOnly], targ.[IsCDNAOnly] = src.[IsCDNAOnly], targ.[HostId] = src.[HostId], targ.[DateCollected] = src.[DateCollected], targ.[YearCollected] = src.[YearCollected], targ.[DateReceived] = src.[DateReceived], targ.[DateContractLabTransfer] = src.[DateContractLabTransfer], targ.[DateLogin] = src.[DateLogin], targ.[IsCollectionDateEstimated] = src.[IsCollectionDateEstimated], targ.[Activity] = src.[Activity], targ.[CollectionMethod] = src.[CollectionMethod], targ.[CountryId] = src.[CountryId], targ.[StateId] = src.[StateId], targ.[CountyId] = src.[CountyId], targ.[SenderCode] = src.[SenderCode], targ.[PassageReceived] = src.[PassageReceived], targ.[CollectionComment] = src.[CollectionComment], targ.[IsOriginal] = src.[IsOriginal], targ.[IsVaccine] = src.[IsVaccine], targ.[ContractLab] = src.[ContractLab], targ.[PackageId] = src.[PackageId], targ.[PatientAge] = src.[PatientAge], targ.[PatientGender] = src.[PatientGender], targ.[PatientLocation] = src.[PatientLocation], targ.[IsPatientDeceased] = src.[IsPatientDeceased], targ.[IsPatientVaccinated] = src.[IsPatientVaccinated], targ.[IsNursingHome] = src.[IsNursingHome], targ.[SpecialStudy] = src.[SpecialStudy], targ.[GeneticScreen] = src.[GeneticScreen], targ.[DatePCR] = src.[DatePCR], targ.[LabComment] = src.[LabComment], targ.[ReasonForSubmission] = src.[ReasonForSubmission], targ.[ShippingCondition] = src.[ShippingCondition], targ.[IsTravel] = src.[IsTravel], targ.[TravelLocation] = src.[TravelLocation], targ.[IsReagent] = src.[IsReagent], targ.[ReagentClass] = src.[ReagentClass], targ.[SenderLabId] = src.[SenderLabId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Id], [CDCId], [SpecimenId], [StrainName], [IsDuplicate], [IsReassortant], [IsRecombinant], [IsNegative], [IsColdAdapted], [IsRNAOnly], [IsCDNAOnly], [HostId], [DateCollected], [YearCollected], [DateReceived], [DateContractLabTransfer], [DateLogin], [IsCollectionDateEstimated], [Activity], [CollectionMethod], [CountryId], [StateId], [CountyId], [SenderCode], [PassageReceived], [CollectionComment], [IsOriginal], [IsVaccine], [ContractLab], [PackageId], [PatientAge], [PatientGender], [PatientLocation], [IsPatientDeceased], [IsPatientVaccinated], [IsNursingHome], [SpecialStudy], [GeneticScreen], [DatePCR], [LabComment], [ReasonForSubmission], [ShippingCondition], [IsTravel], [TravelLocation], [IsReagent], [ReagentClass], [SenderLabId])
		VALUES ( src.[Id], src.[CDCId], src.[SpecimenId], src.[StrainName], src.[IsDuplicate], src.[IsReassortant], src.[IsRecombinant], src.[IsNegative], src.[IsColdAdapted], src.[IsRNAOnly], src.[IsCDNAOnly], src.[HostId], src.[DateCollected], src.[YearCollected], src.[DateReceived], src.[DateContractLabTransfer], src.[DateLogin], src.[IsCollectionDateEstimated], src.[Activity], src.[CollectionMethod], src.[CountryId], src.[StateId], src.[CountyId], src.[SenderCode], src.[PassageReceived], src.[CollectionComment], src.[IsOriginal], src.[IsVaccine], src.[ContractLab], src.[PackageId], src.[PatientAge], src.[PatientGender], src.[PatientLocation], src.[IsPatientDeceased], src.[IsPatientVaccinated], src.[IsNursingHome], src.[SpecialStudy], src.[GeneticScreen], src.[DatePCR], src.[LabComment], src.[ReasonForSubmission], src.[ShippingCondition], src.[IsTravel], src.[TravelLocation], src.[IsReagent], src.[ReagentClass], src.[SenderLabId])
	;
	SET IDENTITY_INSERT [dbo].[Specimen] OFF
	-- end flu_specimen

	-- flu_isolate
	SET IDENTITY_INSERT [dbo].[Subsample] ON
	MERGE [dbo].[Subsample] as targ
	USING ( VALUES 
	(242290,167668,'false','C1/C2','31 OCT 2013','','',NULL,'false','','432-G5','','','','')
	,(241248,166360,'false','E4','16 SEP 2013','','',NULL,'true','','','','','','')
	,(231465,152485,'false','M1/C2','01 FEB 2013','','',NULL,'false','','','','','','')
	,(242112,167614,'false','C3/C2','25 OCT 2013','','',NULL,'false','','431-H7','','','','')
	,(242063,166511,'false','C2','24 OCT 2013','','',NULL,'false','','431-G3','','','','')
	,(242096,167833,'false','C2','22 OCT 2013','','CA','30 OCT 2013','false','','432-H2','','','','')
	,(240819,166860,'false','E4/E3','30 AUG 2013','','',NULL,'true','','','','','','')
	,(242111,167617,'false','C3/C2','25 OCT 2013','','',NULL,'false','','431-H8','','','','')
	,(226455,157968,'true','REAGENT',NULL,'','',NULL,'false','','','R2-S2','','','')
	,(242403,167874,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-B9','','','','')
	,(242118,167667,'false','C2/C2','28 OCT 2013','','',NULL,'false','','431-I1','','','','')
	,(226453,157966,'true','REAGENT',NULL,'','',NULL,'false','','','R2-S2','','','')
	,(242093,167815,'false','C1','17 OCT 2013','','CA','23 OCT 2013','false','','431-H3','','','','')
	,(241586,157315,'false','E5','27 SEP 2013','','',NULL,'true','','','','','','')
	,(209585,147234,'false','E3/E3','06 FEB 2012','','',NULL,'true','','','','','','')
	,(242405,167873,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-B8','','','','')
	,(242059,166505,'false','C2','24 OCT 2013','','',NULL,'false','','431-G2','','','','')
	,(241839,166360,'false','C3','03 OCT 2013','','',NULL,'false','','','','','','')
	,(242229,167823,'false','C1','24 OCT 2013','','CA','30 OCT 2013','false','','432-H7','','','','')
	,(242226,167832,'false','C2','23 OCT 2013','','CA','30 OCT 2013','false','','432-H1','','','','')
	,(224499,152484,'false','E5','18 OCT 2012','','',NULL,'true','','','','','','')
	,(242404,167878,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-C3','','','','')
	,(242228,167821,'false','C1','23 OCT 2013','','CA','30 OCT 2013','false','','432-H5','','','','')
	,(242119,167660,'false','C1/C2','28 OCT 2013','','',NULL,'false','','431-I2','','','','')
	,(242109,166513,'false','C2','25 OCT 2013','','',NULL,'false','','431-H6','','','','')
	,(242402,167877,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-C2','','','','')
	,(242122,162862,'false','C3','28 OCT 2013','','',NULL,'false','','','','','','')
	,(225589,154208,'false','E4','23 NOV 2012','','',NULL,'true','','','','','','')
	,(242144,167843,'false','C1','22 OCT 2013','','WI','25 OCT 2013','false','','432-F6','','','','')
	,(242140,167678,'false','C1/C1','21 OCT 2013','','WI','25 OCT 2013','false','','432-F3','','','','')
	,(242139,167681,'false','C2/C1','21 OCT 2013','','WI','25 OCT 2013','false','','432-F2','','','','')
	,(242145,167844,'false','C1','22 OCT 2013','','WI','25 OCT 2013','false','','432-F7','','','','')
	,(242127,167835,'false','C1/C1','29 OCT 2013','','',NULL,'false','','431-I8','','','','')
	,(242356,167879,'false','CX/C1','04 NOV 2013','','',NULL,'false','','433-A3','','','','')
	,(236494,50595,'false','SpfCK2E4','22 APR 2013','','',NULL,'true','','','','','','')
	,(241241,149103,'false','C2/C3','16 SEP 2013','','',NULL,'false','','','','','','')
	,(240221,162862,'false','E5','15 AUG 2013','','',NULL,'true','','','','','','')
	,(242349,167849,'false','C2','28 OCT 2013','','WI','01 NOV 2013','false','','432-I6','','','','')
	,(237063,157315,'false','C3','05 MAY 2013','','',NULL,'false','','','','','','')
	,(240968,162862,'false','C3','29 AUG 2013','','',NULL,'false','','','','','','')
	,(242355,167876,'false','CX/C1','04 NOV 2013','','',NULL,'false','','433-A2','','','','')
	,(242227,167829,'false','C1','24 OCT 2013','','CA','30 OCT 2013','false','','432-H3','','','','')
	,(242406,167875,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-C1','','','','')
	,(242354,167697,'false','C2','04 NOV 2013','','',NULL,'false','','433-A1','','','','4TX')
	,(242357,167887,'false','CX/C1','05 NOV 2013','','',NULL,'false','','433-A4','','','','')
	,(242126,167661,'false','C2/C2','29 OCT 2013','','',NULL,'false','','431-I7','','','','')
	,(226454,157967,'true','REAGENT',NULL,'','',NULL,'false','','','R2-S2','','','')
	,(231775,157315,'false','E4','11 FEB 2013','','',NULL,'true','','','','','','')
	,(242393,167863,'false','C2','31 OCT 2013','','UT','05 NOV 2013','false','','433-B2','','','','')
	) AS src ([Id], [SpecimenId], [IsFromSubmitter], [Passage], [DateHarvested], [Lot], [ContractLab], [DateContractLabTransfer], [IsVaccine], [CalculatedSubType], [SequencingStorage], [InitialStorage], [AAResAdam], [AAResNai], [DownToVaccine])
	ON targ.[Id] = src.[Id]
	WHEN MATCHED AND (targ.[SpecimenId] <> src.[SpecimenId] OR targ.[IsFromSubmitter] <> src.[IsFromSubmitter] OR targ.[Passage] <> src.[Passage] OR targ.[DateHarvested] <> src.[DateHarvested] OR targ.[Lot] <> src.[Lot] OR targ.[ContractLab] <> src.[ContractLab] OR targ.[DateContractLabTransfer] <> src.[DateContractLabTransfer] OR targ.[IsVaccine] <> src.[IsVaccine] OR targ.[CalculatedSubType] <> src.[CalculatedSubType] OR targ.[SequencingStorage] <> src.[SequencingStorage] OR targ.[InitialStorage] <> src.[InitialStorage] OR targ.[AAResAdam] <> src.[AAResAdam] OR targ.[AAResNai] <> src.[AAResNai] OR targ.[DownToVaccine] <> src.[DownToVaccine]) THEN
		UPDATE SET targ.[SpecimenId] = src.[SpecimenId], targ.[IsFromSubmitter] = src.[IsFromSubmitter], targ.[Passage] = src.[Passage], targ.[DateHarvested] = src.[DateHarvested], targ.[Lot] = src.[Lot], targ.[ContractLab] = src.[ContractLab], targ.[DateContractLabTransfer] = src.[DateContractLabTransfer], targ.[IsVaccine] = src.[IsVaccine], targ.[CalculatedSubType] = src.[CalculatedSubType], targ.[SequencingStorage] = src.[SequencingStorage], targ.[InitialStorage] = src.[InitialStorage], targ.[AAResAdam] = src.[AAResAdam], targ.[AAResNai] = src.[AAResNai], targ.[DownToVaccine] = src.[DownToVaccine]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Id], [SpecimenId], [IsFromSubmitter], [Passage], [DateHarvested], [Lot], [ContractLab], [DateContractLabTransfer], [IsVaccine], [CalculatedSubType], [SequencingStorage], [InitialStorage], [AAResAdam], [AAResNai], [DownToVaccine])
		VALUES ( src.[Id], src.[SpecimenId], src.[IsFromSubmitter], src.[Passage], src.[DateHarvested], src.[Lot], src.[ContractLab], src.[DateContractLabTransfer], src.[IsVaccine], src.[CalculatedSubType], src.[SequencingStorage], src.[InitialStorage], src.[AAResAdam], src.[AAResNai], src.[DownToVaccine])
	;
	SET IDENTITY_INSERT [dbo].[Subsample] OFF
	--end flu_isolate

	--[flu_hi_test_entry]
	SET IDENTITY_INSERT [dbo].[HITestEntry] ON
	MERGE [dbo].[HITestEntry] as targ
	USING ( VALUES 
	(81196,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,231775,6,'A/New York/39/2012','','',48,4,'false')
	,(81197,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,241586,7,'A/New York/39/2012 NEW','','',40,4,'false')
	,(81199,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,240221,9,'A/American Samoa/4786/2013','','',72,4,'false')
	,(81201,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,242122,11,'A/American Samoa/4786/2013 NEW','','',16,4,'false')
	,(81202,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,240819,12,'A/Victoria/505/2013','','',128,4,'false')
	,(81204,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,241839,14,'A/Georgia/3/2013 NEW','','',40,4,'false')
	,(81206,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,226453,16,'A/Victoria/210/2009 (FROM AUSTRALIA)','','',32,4,'false')
	,(81208,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,226455,18,'A/Wisconsin/67/2005 (FROM AUSTRALIA)','','',40,4,'false')
	,(81209,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,231465,22,'A/Texas/50/2012','','',40,4,'false')
	,(81211,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242063,24,'A/Niakhar/6986/2012 ORIGINAL','','',64,4,'false')
	,(81213,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242112,26,'485162','','',16,4,'false')
	,(81214,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242111,27,'334089','','',32,4,'false')
	,(81215,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242126,28,'1313543','','',32,4,'false')
	,(81216,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242118,29,'1312723','','',32,4,'false')
	,(81217,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242290,30,'1311318','','',8,4,'false')
	,(81218,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242119,31,'1317305','','',8,4,'false')
	,(81220,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242127,33,'WA0192620','','',32,4,'false')
	,(81221,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242405,34,'A/Hong Kong/4928/2013','','',32,4,'false')
	,(81223,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242406,36,'A/Hong Kong/4939/2013','','',32,4,'false')
	,(81224,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242355,37,'A/Hong Kong/4943/2013','','',64,4,'false')
	,(81226,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242404,39,'A/Hong Kong/4950/2013','','',32,4,'false')
	,(81227,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242356,40,'A/Hong Kong/4985/2013','','',64,4,'false')
	,(81229,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242139,42,'IDR13-30596','','',64,4,'false')
	,(81230,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242140,43,'IDR13-30597','','',64,4,'false')
	,(81231,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242144,44,'13VR009143 ORIGINAL','','',16,4,'false')
	,(81194,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,224499,4,'A/Texas/50/2012','','',48,4,'false')
	,(81191,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,236494,1,'A/Wyoming/03/2003','','',104,4,'false')
	,(81192,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,209585,2,'A/Victoria/361/2011','','',56,4,'false')
	,(81193,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,241241,3,'A/Victoria/361/2011','','',16,4,'false')
	,(81195,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,231465,5,'A/Texas/50/2012','','',40,4,'false')
	,(81198,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,237063,8,'A/New York/39/2012','','',32,4,'false')
	,(81200,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,240968,10,'A/American Samoa/4786/2013','','',16,4,'false')
	,(81203,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,241248,13,'A/Georgia/3/2013','','',64,4,'false')
	,(81205,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,225589,15,'A/Puerto Rico/36/2012','','',56,4,'false')
	,(81207,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,226454,17,'A/Victoria/361/2011 (FROM AUSTRALIA)','','',40,4,'false')
	,(81271,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'STCTRL'),1486,242228,19,'FR-1188 WHO KIT B (VIC) CONTROL LOT#1314BVAG','','',192,4,'false')
	,(81272,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'STCTRL'),1486,242403,20,'FR-1186 WHO KIT B (YAM) CONTROL LOT#1314BYAG','','',360,4,'false')
	,(81273,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'STCTRL'),1486,242145,21,'FR-1184 WHO KIT H1pdm CONTROL LOT#1314H1AG','','',192,4,'false')
	,(81210,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242059,23,'A/Niakhar/6939/2012 ORIGINAL','','',64,4,'false')
	,(81212,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242109,25,'A/Niakhar/6990/2012 ORIGINAL','','',16,4,'false')
	,(81219,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242354,32,'35468 ORIGINAL','','',48,4,'false')
	,(81222,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242403,35,'A/Hong Kong/4936/2013','','',64,4,'false')
	,(81225,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242402,38,'A/Hong Kong/4946/2013','','',64,4,'false')
	,(81228,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242357,41,'A/Hong Kong/5648/2013','','',40,4,'false')
	,(81232,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242145,45,'13VR009184 ORIGINAL','','',32,4,'false')
	,(81233,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242349,46,'M13017590 ORIGINAL','','',64,4,'false')
	,(81234,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242393,47,'100413-0989 ORIGINAL','','',16,4,'false')
	,(81235,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242093,48,'176813 ORIGINAL','','',24,4,'false')
	,(81236,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242226,49,'179603 ORIGINAL','','',40,4,'false')
	,(81237,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242096,50,'N1300574201 ORIGINAL','','',48,4,'false')
	,(81238,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242227,51,'CL2013-00021188 ORIGINAL','','',24,4,'false')
	,(81239,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242228,52,'13V-19065 ORIGINAL','','',40,4,'false')
	,(81240,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'TEST'),1486,242229,53,'N1300586901 ORIGINAL','','',64,4,'false')
	,(81241,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'REFERENCE'),1486,231465,54,'A/Texas/50/2012','','',40,4,'false')
	,(81274,(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE 'SERUMCTRL'),1486,NULL,55,'SERUM CONTROL','','',NULL,NULL,'false')
	) AS src ([Id], [EntryTypeId], [HITestId], [SubsampleId], [Position], [EntryDescription], [EntryError], [Clade], [Titer], [BackTitre], [DoNotReport])
	ON targ.[Id] = src.[Id]
	WHEN MATCHED AND (targ.[EntryTypeId] <> src.[EntryTypeId] OR targ.[HITestId] <> src.[HITestId] OR targ.[SubsampleId] <> src.[SubsampleId] OR targ.[Position] <> src.[Position] OR targ.[EntryDescription] <> src.[EntryDescription] OR targ.[EntryError] <> src.[EntryError] OR targ.[Clade] <> src.[Clade] OR targ.[Titer] <> src.[Titer] OR targ.[BackTitre] <> src.[BackTitre] OR targ.[DoNotReport] <> src.[DoNotReport]) THEN
		UPDATE SET targ.[EntryTypeId] = src.[EntryTypeId], targ.[HITestId] = src.[HITestId], targ.[SubsampleId] = src.[SubsampleId], targ.[Position] = src.[Position], targ.[EntryDescription] = src.[EntryDescription], targ.[EntryError] = src.[EntryError], targ.[Clade] = src.[Clade], targ.[Titer] = src.[Titer], targ.[BackTitre] = src.[BackTitre], targ.[DoNotReport] = src.[DoNotReport]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ( [Id], [EntryTypeId], [HITestId], [SubsampleId], [Position], [EntryDescription], [EntryError], [Clade], [Titer], [BackTitre], [DoNotReport])
		VALUES ( src.[Id], src.[EntryTypeId], src.[HITestId], src.[SubsampleId], src.[Position], src.[EntryDescription], src.[EntryError], src.[Clade], src.[Titer], src.[BackTitre], src.[DoNotReport])
	;
	SET IDENTITY_INSERT [dbo].[HITestEntry] OFF
	--end [flu_hi_test_entry
END;
GO
