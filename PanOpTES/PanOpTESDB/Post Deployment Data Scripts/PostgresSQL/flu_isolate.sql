﻿/*
([Id]
, [SpecimenId]
, [IsFromSubmitter]
, [Passage]
, [DateHarvested]
, [Lot]
, [ContractLab]
, [DateContractLabTransfer]
, [Vaccine]
, [CalculatedSubType]
, [SequencingStorage]
, [InitialStorage]
, [AA_Res_Adam]
, [AA_Res_Nai]
, [DownToVaccine]
)
*/
/*
select DISTINCT '(' || A.id 
|| ',' || A.specimen_id
|| ',''' || A.isolate_received || ''''
|| ',''' || A.passage || ''''
|| ',' || CASE WHEN char_length(to_char(A.date_harvested, 'DD MON YYYY')) > 0 THEN  '''' || to_char(A.date_harvested, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',''' || A.lot || ''''
|| ',''' || COALESCE(A.contract_lab_id, '') || ''''
|| ',' || CASE WHEN char_length(to_char(A.date_contract_lab_transfer, 'DD MON YYYY')) > 0 THEN  '''' || to_char(A.date_contract_lab_transfer, 'DD MON YYYY') || ''''  ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(A.vaccine::text) > 0 THEN  '''' || A.vaccine::text  || '''' ELSE 'NULL' END || ''
|| ','''''
|| ',''' || A.sequencing_storage || ''''
|| ',''' || A.initial_storage || ''''
|| ',''' || A.aa_res_adam || ''''
|| ',''' || A.aa_res_nai || ''''
|| ',''' || A.down_to_vaccine || ''''
|| ')'
AS MergeLine
from flu_isolate A
JOIN flu_hi_test_entry B ON A.id = B.isolate_id
where B.hi_test_id IN (1486) 
*/
