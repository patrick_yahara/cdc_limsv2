﻿/*
/*
([Id]
, [CDCId]
, [SpecimenId]
, [StrainName]
, [Duplicate]
, [Reassortant]
, [Recombinant]
, [Negativet]
, [ColdAdapted]
, [MAOnly]
, [CDNAOnly]
, [HostId]
, [DateCollected]
, [YearCollected]
, [DateReceived]
, [DateContractLabTransfer]
, [DateLogin]
, [EstimatedCollectionDate]
, [Activity]
, [CollectionMethod]
, [CountryId]
, [StateId]
, [CountyId]
, [SenderCode]
, [PassageReceived]
, [CollectionComment]
, [Original]
, [Vaccine]
, [ContractLabId]
, [PackageId]
, [PatientAge]
, [PatientGender]
, [PatientLocation]
, [PatientDeceased]
, [PatientVaccinated]
, [NursingHome]
, [SpecialStudy]
, [GeneticScreen]
, [DatePCR]
, [LabComment]
, [ReasonForSubmission]
, [ShippingCondition]
, [Travel]
, [TravelLocation]
, [Reagent]
, [ReagentClass]
, [SenderLabId])
*/

select DISTINCT '(' || A.id 
|| ',''' || A.cdc_id || ''''
|| ',''' || A.specimen_id || ''''
|| ',''' || A.strain_name || ''''
|| ',''' || A.duplicate || ''''
|| ',''' || A.reassortant || ''''
|| ',''' || A.recombinant || ''''
|| ',''' || A.negative || ''''
|| ',''' || A.cold_adapted || ''''
|| ',''' || A.rna_only || ''''
|| ',''' || A.cdna_only || ''''
|| ',' || CASE WHEN char_length(A.host_id::text) > 0 THEN A.host_id::text ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(to_char(A.date_collected, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.date_collected, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',''' || A.year_collected || ''''
|| ',' || CASE WHEN char_length(to_char(A.date_received, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.date_received, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(to_char(A.date_contract_lab_transfer, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.date_contract_lab_transfer, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(to_char(A.date_login, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.date_login, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(to_char(A.estimated_collection_date, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.estimated_collection_date, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',''' || A.activity || ''''
|| ',''' || A.collection_method || ''''
|| ', NULL' --|| ',' || CASE WHEN char_length(A.country_id) > 0 THEN '''' || A.country_id || '''' ELSE 'NULL' END || ''
|| ', NULL' --|| ',' || CASE WHEN char_length(A.state_id) > 0 THEN '''' || A.state_id || '''' ELSE 'NULL' END || ''
|| ', NULL' --|| ',' || CASE WHEN char_length(A.county_id) > 0 THEN '''' || A.county_id || '''' ELSE 'NULL' END || ''
|| ',''' || CASE WHEN char_length(A.sender_code::text) > 0 THEN A.sender_code::text ELSE 'NULL' END || ''''
|| ',''' || A.passage_received || ''''
|| ',''' || A.collection_comment || ''''
|| ',''' || A.original || ''''
|| ',''' || A.vaccine || ''''
|| ',' || CASE WHEN char_length(A.contract_lab_id) > 0 THEN '''' || A.contract_lab_id || '''' ELSE 'NULL' END || ''
|| ',''' || CASE WHEN char_length(A.package_id::text) > 0 THEN A.package_id::text ELSE 'NULL' END || ''''
|| ',' || CASE WHEN char_length(A.patient_age::text) > 0 THEN A.patient_age::text ELSE 'NULL' END || ''
|| ',''' || A.patient_gender || ''''
|| ',''' || A.patient_location || ''''
|| ',' || CASE WHEN char_length(A.patient_deceased::text) > 0 THEN '''' || A.patient_deceased::text || '''' ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(A.patient_vaccinated::text) > 0 THEN '''' || A.patient_vaccinated::text || '''' ELSE 'NULL' END || ''
|| ',' || CASE WHEN char_length(A.nursing_home::text) > 0 THEN '''' || A.nursing_home::text || '''' ELSE 'NULL' END || ''
|| ',''' || A.special_study || ''''
|| ',''' || A.genetic_screen || ''''
|| ',' || CASE WHEN char_length(to_char(A.date_pcr, 'DD MON YYYY')) > 0 THEN '''' || to_char(A.date_pcr, 'DD MON YYYY') || '''' ELSE 'NULL' END || ''
|| ',''' || A.lab_comment || ''''
|| ',''' || A.reason_for_submission || ''''
|| ',''' || A.shipping_condition || ''''
|| ',' || CASE WHEN char_length(A.travel::text) > 0 THEN '''' || A.travel::text || '''' ELSE 'NULL' END || ''
|| ',''' || A.travel_location || ''''
|| ',''' || A.reagent || ''''
|| ',''' || A.reagent_class || ''''
|| ',' || CASE WHEN char_length(A.sender_lab_id::text) > 0 THEN '''' || A.sender_lab_id::text || '''' ELSE 'NULL' END || ''
|| ')'
AS MergeLine
from flu_isolate C
JOIN flu_hi_test_entry B ON C.id = B.isolate_id
JOIN flu_specimen A ON C.specimen_id = A.id
where B.hi_test_id IN (1486) 
*/