﻿/*([Id]
, [EntryTypeId]
, [HITestId]
, [SubsampleId]
, [Position]
, [EntryDescription]
, [EntryError]
, [Clade]
, [Titer]
, [BackTitre]
, [DoNotReport])
*/
/*
select '(' || id 
|| ',' || '(SELECT TOP 1 Id FROM [dbo].[EntryType] WHERE [SystemName] LIKE ''' || upper(replace(replace(entry_type, '_', ''), ' ', '')) || ''')'
|| ',' || hi_test_id
|| ',' || CASE WHEN COALESCE(isolate_id, -1) > -1 THEN isolate_id::text ELSE 'NULL' END
|| ',' || position
|| ',''' || description || ''''
|| ',''' || error || ''''
|| ',''' || clade || ''''
|| ',' || CASE WHEN char_length(titer::text) > 0 THEN titer::text ELSE 'NULL' END
|| ',' || CASE WHEN char_length(back_titer::text) > 0 THEN back_titer::text ELSE 'NULL' END
|| ',' || do_not_report
|| ')'
AS MergeLine from flu_hi_test_entry 
where hi_test_id IN (1486) 
*/