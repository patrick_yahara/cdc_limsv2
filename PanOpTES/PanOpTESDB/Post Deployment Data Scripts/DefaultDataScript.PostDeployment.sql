﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r "..\Post Deployment Data Scripts\Script.AddStaticData.sql"
:r "..\Post Deployment Data Scripts\Script.AddDevData.sql"
:r "..\Post Deployment Data Scripts\Script.SetPermissions.sql"
--:r "..\Post Deployment Data Scripts\SSIS DB Objects\usp_CreateFluSpecimenTmpTable.sql"
--:r "..\Post Deployment Data Scripts\SSIS DB Objects\usp_LoadSpecimen.sql"
--:r "..\Post Deployment Data Scripts\SSIS DB Objects\usp_CreateFluIsolateTmpTable.sql"
--:r "..\Post Deployment Data Scripts\SSIS DB Objects\usp_LoadSubsample.sql"
