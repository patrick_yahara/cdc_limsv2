CREATE TABLE [dbo].[State]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION, 
	[Abbrev] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ANSICode] [nvarchar](10) NULL,
	[HHSRegionId] INT NOT NULL,
	[GISAidCode] [smallint] NULL, 
    CONSTRAINT [PK_State] PRIMARY KEY ([Id]), 
    CONSTRAINT [CK_State_GISAidCode] CHECK (GISAidCode >= 0), 
    CONSTRAINT [FK_State_HHSRegion] FOREIGN KEY ([HHSRegionId]) REFERENCES [HHSRegion]([Id]),
)