CREATE TABLE [dbo].[WHORegion]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION, 
	[Abbrev] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NOT NULL, 
    CONSTRAINT [PK_WHORegion] PRIMARY KEY ([Id]),
)