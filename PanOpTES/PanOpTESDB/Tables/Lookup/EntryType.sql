﻿CREATE TABLE [dbo].[EntryType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
    [ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	[DisplayName] NVARCHAR(64) NOT NULL,
	[SystemName] NVARCHAR(64) NOT NULL,
)
