﻿CREATE TABLE [dbo].[HITest]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
    [ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	DateTested DATETIME NOT NULL,
	TestFileName NVARCHAR (250) NOT NULL,
	TestDescription NVARCHAR (250) NOT NULL,
	SubType NVARCHAR (90) NULL,
)
--CREATE TABLE flu_hi_test (
--    id integer DEFAULT nextval('flu_hi_test_id_seq'::regclass) NOT NULL,
--    date_tested date NOT NULL,
--    test_file character varying(100) NOT NULL,
--    description character varying(100) NOT NULL,
--    calculated_subtype character varying(30) NOT NULL
--);
