﻿CREATE TABLE [dbo].[LIMSHiTest]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
    [ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	DateRequested DATETIME NOT NULL,
)
