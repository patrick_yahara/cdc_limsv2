﻿CREATE TABLE [dbo].[LIMSSample]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
    [ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	[IsAvailable] BIT DEFAULT ((1)) NOT NULL, 
	[CDCId] [nvarchar](50) NOT NULL,
	[SpecimenId] [nvarchar](150) NOT NULL,
	[DateCollected] [datetime] NULL,
	[Country] [nvarchar](250) NULL,
	[State] [nvarchar](100) NULL,
)
