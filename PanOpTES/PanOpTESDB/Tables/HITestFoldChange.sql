﻿CREATE TABLE [dbo].[HITestFoldChange]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
	[ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	[HITestEntryId] INT NULL,
	[HITestResultId] INT NULL,
	Fold SMALLINT NULL,
	CONSTRAINT [FK_HITestFoldChange_HITestEntry] FOREIGN KEY([HITestEntryId]) REFERENCES [HITestEntry] ([Id]),
	CONSTRAINT [FK_HITestFoldChange_HITestResult] FOREIGN KEY([HITestResultId]) REFERENCES [HITestResult] ([Id]),
)
--CREATE TABLE flu_hi_test_foldchange (
--    id integer NOT NULL,
--    hi_test_entry_id integer NOT NULL,
--    hi_test_result_id integer NOT NULL,
--    fold smallint
--);