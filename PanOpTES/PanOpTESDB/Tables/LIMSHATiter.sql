﻿CREATE TABLE [dbo].[LIMSHATiter]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy]             NVARCHAR (64)    NULL,
    [ModifiedOn]             DATETIME         NULL, 
	[VersionStamp] ROWVERSION,
	RBCType NVARCHAR (255),
	Titer INT,
)
