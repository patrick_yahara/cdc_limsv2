﻿CREATE TABLE [dbo].[HITestRequest]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY
	, [CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System'
	, [CreatedOn] DATETIME NOT NULL DEFAULT getutcdate()
	, [ModifiedBy]             NVARCHAR (64)    NULL
	, [ModifiedOn]             DATETIME         NULL
	, [VersionStamp] ROWVERSION
	, [HITestEntryName] NVARCHAR(250) NOT NULL
	, [HISerumEntryName] NVARCHAR(250) NOT NULL
    , [Difference] INT NULL
    , [IsError] BIT NOT NULL DEFAULT 0
	, [ErrorMessage] NVARCHAR(250) NULL
	, [HITestId] INT NOT NULL
	, [EntryTypeId] INT NOT NULL
	, [LIMSSampleId] INT NULL
    , CONSTRAINT [FK_HITestRequest_HITest] FOREIGN KEY([HITestId]) REFERENCES [HITest] ([Id])
	, CONSTRAINT [FK_HITestRequest_EntryType] FOREIGN KEY([EntryTypeId]) REFERENCES [EntryType] ([Id])
    , CONSTRAINT [FK_HITestRequest_LIMSSample] FOREIGN KEY([LIMSSampleId]) REFERENCES [LIMSSample] ([Id])
)
