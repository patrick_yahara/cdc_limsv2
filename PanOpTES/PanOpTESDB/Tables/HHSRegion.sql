CREATE TABLE [dbo].[HHSRegion]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION, 
	[Code] [smallint] NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[HHSRegionDescription] [nvarchar](500) NOT NULL, 
    CONSTRAINT [PK_HHSRegion] PRIMARY KEY ([Id]), 
    CONSTRAINT [CK_HHSRegion_Code] CHECK (Code >= 0),
)