CREATE TABLE [dbo].[Serum]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION,
	[Lot] [nvarchar](50) NOT NULL,
	[SubsampleId] [int] NULL,
	[Ferret] [nvarchar](150) NOT NULL,
	[InfectingAgent] [nvarchar](150) NOT NULL,
	[BoostingAgent] [nvarchar](150) NOT NULL,
	[DateInoculated] [datetime] NULL,
	[DateBoosted] [datetime] NULL,
	[DateHarvested] [datetime] NULL,
	[Justification] [nvarchar](150) NOT NULL,
	[Comment] [nvarchar](550) NOT NULL,
	[ShortDescription] [nvarchar](150) NOT NULL,
	[ExtraDescription] [nvarchar](150) NOT NULL, 
    CONSTRAINT [PK_Serum] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Serum_Subsample] FOREIGN KEY ([SubsampleId]) REFERENCES [Subsample]([Id])
)

