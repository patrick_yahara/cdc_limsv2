﻿CREATE TABLE [dbo].[HISerumEntrySerum]
(
	[HISerumEntryId] INT NOT NULL,
	[SerumId] INT NOT NULL,
	CONSTRAINT [PK_HISerumEntry_Serum] PRIMARY KEY ([HISerumEntryId], [SerumId]),
	CONSTRAINT [FK_HISerumEntry_HISerumEntry] FOREIGN KEY([HISerumEntryId]) REFERENCES [HISerumEntry] ([Id]),
	CONSTRAINT [FK_HISerumEntry_Serum] FOREIGN KEY([SerumId]) REFERENCES [Serum] ([Id])
)
--CREATE TABLE flu_hi_serum_entry_sera (
--    id integer NOT NULL,
--    hiserumentry_id integer NOT NULL,
--    serum_id integer NOT NULL
--);
