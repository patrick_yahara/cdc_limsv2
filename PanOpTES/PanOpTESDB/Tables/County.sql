CREATE TABLE [dbo].[County]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION, 
	[Code] [nvarchar](50) NOT NULL,
	[StateCode] [nvarchar](10) NOT NULL,
	[CountyCode] [nvarchar](10) NOT NULL,
	[StateId] INT NULL,
	[CountryId] INT NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CountyClass] [nvarchar](10) NOT NULL, 
    CONSTRAINT [PK_County] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_County_Country] FOREIGN KEY ([CountryId]) REFERENCES [Country]([Id]), 
    CONSTRAINT [FK_County_State] FOREIGN KEY ([StateId]) REFERENCES [State]([Id]),
)