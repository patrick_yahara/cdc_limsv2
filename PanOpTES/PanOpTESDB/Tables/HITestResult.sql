﻿CREATE TABLE [dbo].[HITestResult]
(
	[Id] INT NOT NULL IDENTITY, 
	[CreatedBy] NVARCHAR(64) NOT NULL DEFAULT 'System', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedBy] NVARCHAR (64) NULL,
	[ModifiedOn] DATETIME NULL, 
	[VersionStamp] ROWVERSION,
	[HITestEntryId] INT NOT NULL,
	[HISerumEntryId] INT NOT NULL,
	[Titer] SMALLINT NOT NULL,
	[IsError] BIT NOT NULL DEFAULT 0,
	[IsHomologous] BIT NOT NULL DEFAULT 0,
	[LogFold] SMALLINT NOT NULL, 
    CONSTRAINT [PK_HITestResult] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_HITestResult_HITestEntry] FOREIGN KEY([HITestEntryId]) REFERENCES [HITestEntry] ([Id]),
	CONSTRAINT [FK_HITestResult_HISerumEntry] FOREIGN KEY([HISerumEntryId]) REFERENCES [HISerumEntry] ([Id]), 
)
--CREATE TABLE flu_hi_test_result (
--    id integer NOT NULL,
--    hi_test_entry_id integer NOT NULL,
--    hi_serum_entry_id integer NOT NULL,
--    titer smallint,
--    error boolean NOT NULL,
--    homologous boolean NOT NULL,
--    logfold smallint,
--    CONSTRAINT flu_hi_test_result_titer_check CHECK ((titer >= 0))
--);