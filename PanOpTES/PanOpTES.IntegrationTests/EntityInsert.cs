﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PanOpTES.Common.Utility;
using PanOpTESModel;
using PanOpTESModel.Models;
using Yahara.Core.Repository;
using Yahara.Core.Repository.EF;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTES.IntegrationTests
{
    [TestClass]
    public class EntityInsert
    {
        private static PanOpTESDbContextSource _contextSource;
        
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            _contextSource = new PanOpTESDbContextSource(new BasicDatabaseSettings() { ConnectionString = @"Data Source=.;Initial Catalog=PanOpTESDB;Integrated Security=True;MultipleActiveResultSets=True" });
        }

        [TestMethod]
        public void TestInsertHITest_TestEntry()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var test = new HITest();
                test.DateTested = DateTime.UtcNow;
                test.TestDescription = "FOO";
                test.TestFileName = "FOO.xls";
                EFAuditFieldUtility.UpdateAuditFields(test);
                test.CreatedBy = !String.IsNullOrEmpty(test.CreatedBy) ? test.CreatedBy : "System";

                    var testEntry = new HITestEntry();
                    testEntry.EntryDescription = "BAR";
                    testEntry.EntryTypeId = 1;
                    testEntry.Position = 1;
                    testEntry.DoNotReport = false;
                    EFAuditFieldUtility.UpdateAuditFields(testEntry);
                    testEntry.CreatedBy = !String.IsNullOrEmpty(testEntry.CreatedBy) ? testEntry.CreatedBy : "System";
                    test.HITestEntries.Add(testEntry);

                    //var serumEntry = new HISerumEntry();
                    //serumEntry.SerumEntryDescription = "BAR";
                    //serumEntry.Position = "1";
                    //EFAuditFieldUtility.UpdateAuditFields(serumEntry);
                    //serumEntry.CreatedBy = !String.IsNullOrEmpty(serumEntry.CreatedBy) ? serumEntry.CreatedBy : "System";
                    //test.HIReferenceEntries.Add(serumEntry);    
             
                try
                {
                    session.Repository.Insert(test);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(false);
                }
            } 
        }

        [TestMethod]
        public void TestInsertHITest_SerumEntry()
        {
            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var test = new HITest();
                test.DateTested = DateTime.UtcNow;
                test.TestDescription = "FOO";
                test.TestFileName = "FOO.xls";
                EFAuditFieldUtility.UpdateAuditFields(test);
                test.CreatedBy = !String.IsNullOrEmpty(test.CreatedBy) ? test.CreatedBy : "System";

                //var testEntry = new HITestEntry();
                //testEntry.EntryDescription = "BAR";
                //testEntry.EntryTypeId = 1;
                //testEntry.Position = 1;
                //testEntry.DoNotReport = false;
                //EFAuditFieldUtility.UpdateAuditFields(testEntry);
                //testEntry.CreatedBy = !String.IsNullOrEmpty(testEntry.CreatedBy) ? testEntry.CreatedBy : "System";
                //test.HITestEntries.Add(testEntry);

                var serumEntry = new HISerumEntry();
                serumEntry.SerumEntryDescription = "BAR";
                serumEntry.Position = "1";
                EFAuditFieldUtility.UpdateAuditFields(serumEntry);
                serumEntry.CreatedBy = !String.IsNullOrEmpty(serumEntry.CreatedBy) ? serumEntry.CreatedBy : "System";
                test.HISerumEntries.Add(serumEntry);       
 
                try
                {
                    session.Repository.Insert(test);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(false);
                }
            }
        }

        //[TestMethod]
        //public void TestInsertHITest_TestEntry_SerumEntry()
        //{
        //    using (var session = new SingleDisposableRepositorySession(_contextSource))
        //    {
        //        var test = new HITest();
        //        test.DateTested = DateTime.UtcNow;
        //        test.TestDescription = "FOO";
        //        test.TestFileName = "FOO.xls";
        //        EFAuditFieldUtility.UpdateAuditFields(test);
        //        test.CreatedBy = !String.IsNullOrEmpty(test.CreatedBy) ? test.CreatedBy : "System";

        //        var testEntry = new HITestEntry();
        //        testEntry.EntryDescription = "BAR";
        //        testEntry.EntryTypeId = 1;
        //        testEntry.Position = 1;
        //        testEntry.DoNotReport = false;
        //        EFAuditFieldUtility.UpdateAuditFields(testEntry);
        //        testEntry.CreatedBy = !String.IsNullOrEmpty(testEntry.CreatedBy) ? testEntry.CreatedBy : "System";
        //        test.HITestEntries.Add(testEntry);

        //        var serumEntry = new HISerumEntry();
        //        serumEntry.SerumEntryDescription = "BAR";
        //        serumEntry.Position = "1";
        //        EFAuditFieldUtility.UpdateAuditFields(serumEntry);
        //        serumEntry.CreatedBy = !String.IsNullOrEmpty(serumEntry.CreatedBy) ? serumEntry.CreatedBy : "System";
        //        test.HIReferenceEntries.Add(serumEntry);    

        //        try
        //        {
        //            session.Repository.Insert(test);
        //        }
        //        catch (Exception ex)
        //        {
        //            Assert.IsTrue(false);
        //        }
        //    }
        //}

        [TestMethod]
        public void TestInsertHITest_TestEntry_SerumEntry()
        {

            using (var session = new SingleDisposableRepositorySession(_contextSource))
            {
                var test = new HITest();
                test.DateTested = DateTime.UtcNow;
                test.TestDescription = "FOO";
                test.TestFileName = "FOO.xls";
                EFAuditFieldUtility.UpdateAuditFields(test);
                test.CreatedBy = !String.IsNullOrEmpty(test.CreatedBy) ? test.CreatedBy : "System";
                session.Repository.Insert(test);

                var testEntry = new HITestEntry();
                testEntry.HITestId = test.Id;
                testEntry.EntryDescription = "BAR";
                testEntry.EntryTypeId = 1;
                testEntry.Position = 1;
                testEntry.DoNotReport = false;
                EFAuditFieldUtility.UpdateAuditFields(testEntry);
                testEntry.CreatedBy = !String.IsNullOrEmpty(testEntry.CreatedBy) ? testEntry.CreatedBy : "System";
                session.Repository.Insert(testEntry);

                var serumEntry = new HISerumEntry();
                serumEntry.HITestId = test.Id;
                serumEntry.SerumEntryDescription = "BAR";
                serumEntry.Position = "1";
                EFAuditFieldUtility.UpdateAuditFields(serumEntry);
                serumEntry.CreatedBy = !String.IsNullOrEmpty(serumEntry.CreatedBy) ? serumEntry.CreatedBy : "System";
                session.Repository.Insert(serumEntry);
            }
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _contextSource = null;
        }
    }
}
