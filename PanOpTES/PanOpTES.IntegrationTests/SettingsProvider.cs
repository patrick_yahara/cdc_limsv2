﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PanOpTES.Services.Interfaces;

namespace PanOpTES.IntegrationTests
{
    public class SettingsProvider : ISettingsProvider
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["PanOpTESSqlConnection"].ConnectionString;
        }
    }
}
