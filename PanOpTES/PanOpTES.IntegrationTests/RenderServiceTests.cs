﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using PanOpTES.Services.Ninject;

namespace PanOpTES.IntegrationTests
{
    [TestClass]
    public class RenderServiceTests
    {
        [TestClass]
        public class GetODocTemplateMethod : RenderServiceTests
        {
            [TestMethod]
            public void ReturnsByteArray()
            {
                //ARRANGE
                var kernel = new StandardKernel();
                kernel.Bind<ISettingsProvider>().To<SettingsProvider>();
                kernel.Load(new PanOpTESServiceModule());
                var ds = kernel.Get<IHITestDataService>();
                var rs = kernel.Get<IHITestRenderService>();
                var hasBytes = false;
                //ACT
                if (ds != null)
                {
                    var results = ds.GetAllHITests();
                    if (results != null && results.Success)
                    {
                        if ((results.Data != null && results.Data.Count > 0))
                        {
                            var id = new List<HITestDto>(results.Data)[0].Id;
                            var template = ds.GetHITestTemplate(id);
                            if (template != null && template.Success && template.Data.HITestEntries != null)
                            {
                                var xlfile = rs.GetODocTemplate(template.Data);
                                if (xlfile != null && xlfile.Success && xlfile.Data.Length > 0)
                                {
                                    hasBytes = true;
                                }
                            }
                        }
                    }
                }
                //ASSERT
                Assert.IsTrue(hasBytes);
            }
        }
    }
}
