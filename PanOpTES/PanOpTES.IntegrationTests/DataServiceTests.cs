﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services;
using PanOpTES.Services.Interfaces;
using PanOpTES.Services.Ninject;
using PanOpTESModel.Models;
using Yahara.Core.Repository;

namespace PanOpTES.IntegrationTests
{
    [TestClass]
    public class DataServiceTests
    {
        [TestClass]
        public class GetHITestsMethod : DataServiceTests
        {
            [TestMethod]
            public void ReturnsRecords()
            {
                //ARRANGE
                var kernel = new StandardKernel();
                kernel.Bind<ISettingsProvider>().To<SettingsProvider>();
                kernel.Load(new PanOpTESServiceModule());
                var ds = kernel.Get<IHITestDataService>();
                var hasRecords = false;
                //ACT
                if (ds != null)
                {
                    var results = ds.GetAllHITests();
                    if (results != null && results.Success)
                    {
                        hasRecords = (results.Data != null && results.Data.Count > 0);
                        if (hasRecords)
                        {
                            foreach (var test in results.Data)
                            {
                                Console.WriteLine(String.Format(@"{1:MM/dd/yyyy}: {0}{2}", test.TestDescription, test.DateTested, System.Environment.NewLine));
                            }
                            
                        }
                    }                  
                }
                //ASSERT
                Assert.IsTrue(hasRecords);
            }
        }

        [TestClass]
        public class GetHITestTemplateMethod : DataServiceTests
        {
            [TestMethod]
            public void ReturnsRecords()
            {
                //ARRANGE
                var kernel = new StandardKernel();
                kernel.Bind<ISettingsProvider>().To<SettingsProvider>();
                kernel.Load(new PanOpTESServiceModule());
                var ds = kernel.Get<IHITestDataService>();
                var hasRecords = false;
                //ACT
                if (ds != null)
                {
                    var results = ds.GetAllHITests();
                    if (results != null && results.Success)
                    {
                        //hasRecords = (results.Data != null && results.Data.Count > 0);
                        //if (hasRecords)
                        //{
                        //    foreach (var test in results.Data)
                        //    {
                        //        Console.WriteLine(String.Format(@"{1:MM/dd/yyyy}: {0}{2}", test.TestDescription, test.DateTested, System.Environment.NewLine));
                        //    }

                        //}
                        if ((results.Data != null && results.Data.Count > 0))
                        {
                            var id = new List<HITestDto>(results.Data)[0].Id;
                            var template = ds.GetHITestTemplate(id);
                            if (template != null && template.Success && template.Data.HITestEntries != null)
                            {
                                hasRecords = true;
                            }
                        }
                    }
                }
                //ASSERT
                Assert.IsTrue(hasRecords);
            }
        }

        [TestClass]
        public class AddHITestMethod : DataServiceTests
        {
            [TestMethod]
            public void InsertsRecord()
            {
                //ARRANGE
                var kernel = new StandardKernel();
                kernel.Bind<ISettingsProvider>().To<SettingsProvider>();
                kernel.Load(new PanOpTESServiceModule());
                var ds = kernel.Get<IHITestDataService>();
                var insertedRecord = false;
                //ACT
                if (ds != null)
                {
                    var test = new HITestDto();
                    test.DateTested = DateTime.UtcNow;
                    test.TestDescription = "FOO";
                    test.TestFileName = "test.xls";
                    var testEntryDto = new HITestEntryDto();
                    testEntryDto.EntryDescription = "BAR";
                    testEntryDto.Position = 1;
                    testEntryDto.EntryType = new EntryTypeDto(){Id = 1};
                    (test.HITestEntries as List<HITestEntryDto>).Add(testEntryDto);
                    var serumEntryDto = new HISerumEntryDto();
                    serumEntryDto.SerumEntryDescription = "BAR THE SECOND";
                    serumEntryDto.Position = 1;
                    (test.HISerumEntries as List<HISerumEntryDto>).Add(serumEntryDto);
                    var results = ds.AddHITest(test);
                    if (results != null && results.Success)
                    {
                        insertedRecord = true;
                    }
                }
                //ASSERT
                Assert.IsTrue(insertedRecord);
            }
        }
    }
}
