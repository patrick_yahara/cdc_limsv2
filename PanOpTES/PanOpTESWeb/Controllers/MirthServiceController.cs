﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;

namespace PanOpTESWeb.Controllers
{
    public class MirthServiceController : ApiController
    {
        public IHITestDataService HITestDataService { get; set; }
        public MirthServiceController(IHITestDataService service)
        {
            this.HITestDataService = service;
        }

        [HttpPost]
        public void Upsert([FromBody]string value)
        {
            MirthSample sample;
            try
            {
                var extraTypes = new Type[4];
                extraTypes[0] = typeof (MirthSubsample);
                extraTypes[1] = typeof(List<MirthSubsample>);
                extraTypes[2] = typeof(MirthHATiter);
                extraTypes[3] = typeof(MirthHiTest);
                var serializer = new XmlSerializer(typeof(MirthSample), extraTypes);
                using (TextReader reader = new StringReader(value))
                {
                    sample = (MirthSample)serializer.Deserialize(reader);
                }
                var added = HITestDataService.AddMirthSample(sample);
            }
            catch (Exception ex)
            {
                //log?
            }

        }
    }
}
