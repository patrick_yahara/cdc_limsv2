﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTESWeb.Controllers
{
    public class HITestRenderServiceController : ApiController
    {
        public IHITestRenderService HITestRenderService { get; set; }
        public HITestRenderServiceController(IHITestRenderService service)
        {
            this.HITestRenderService = service;
        }

        public ResponseBase<byte[]> GetODocTemplate(HITestTemplateDto template)
        {
            try
            {
                var response = HITestRenderService.GetODocTemplate(template);
                return response;
            }
            catch (Exception ex)
            {
                return ResponseBase<byte[]>.CreateErrorResponse(ex);
            }
        }
    }
}
