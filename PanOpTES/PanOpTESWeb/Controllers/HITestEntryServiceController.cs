﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PanOpTES.Common;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;

namespace PanOpTESWeb.Controllers
{
    public class HITestEntryServiceController : ApiController
    {
        public IHITestDataService HITestDataService { get; set; }
        public HITestEntryServiceController(IHITestDataService service)
        {
            this.HITestDataService = service;
        }

        // GET api/hitestentryservice/5
        public HITestTemplateDto GetTemplate(int id, string entryType)
        {
            var response = HITestDataService.GetHITestTemplate(id);
            // short term filter out 
            response.Data.HITestEntries =
                response.Data.HITestEntries.Where(s => entryType.Equals(s.EntryType.SystemName))
                        .ToList();
            return response.Data;
        }

        public List<HITestEntryDto> GetTestEntries(string[] cdcids)
        {
            var response = HITestDataService.GetEntriesByCDCId(cdcids);
            // short term filter out 
            return response.Data.ToList();
        }
    }
}
