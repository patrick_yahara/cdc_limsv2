﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PanOpTES.Common;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using PanOpTESWeb.Common;
using PanOpTESWeb.ViewModels.HiTestEntry;

namespace PanOpTESWeb.Controllers
{
    public class HITestEntryController : Controller
    {

        protected override void OnResultExecuting(ResultExecutingContext context)
        {
            CheckAndHandleFileResult(context);

            base.OnResultExecuting(context);
        }

        private const string FILE_DOWNLOAD_COOKIE_NAME = "fileDownload";

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="context"></param>
        private void CheckAndHandleFileResult(ResultExecutingContext context)
        {
            if (context.Result is FileResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                Response.SetCookie(new HttpCookie(FILE_DOWNLOAD_COOKIE_NAME, "true") { Path = "/" });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (Request.Cookies[FILE_DOWNLOAD_COOKIE_NAME] != null)
                    Response.Cookies[FILE_DOWNLOAD_COOKIE_NAME].Expires = DateTime.Now.AddYears(-1);
        }

        //
        // GET: /HITestEntry/

        public IHITestRenderService HITestRenderService { get; set; }
        public IHITestDataService HITestDataService { get; set; }
        public HITestEntryController(IHITestRenderService service, IHITestDataService dataservice)
        {
            this.HITestRenderService = service;
            this.HITestDataService = dataservice;
        }


        public ActionResult Index(int id)
        {
            var template = HITestDataService.GetHITestTemplate(id);
            return View(new IndexVM { id = id, TemplateDto = template.Data });
        }

        [FileDownload]
        public ActionResult RenderDefault(int id)
        {
            var template = HITestDataService.GetHITestTemplate(id);
            return Render(template.Data);
        }

        [HttpPost, FileDownload]
        public ActionResult Render(HITestTemplateDto template)
        {
            HITestTemplateDto localtemplate = null;
            List<HITestEntryDto> testEntries = null;
            List<HITestEntryDto> referenceEntries = null;
            List<HITestEntryDto> projectEntries = null;
            List<HITestEntryDto> subtypecontrolEntries = new List<HITestEntryDto>();
            if (template == null)
            {
                var templateData = Request["template"];
                var referenceData = Request["reference"];
                var testData = Request["test"];
                var projectData = Request["project"];
                var subtypecontrolData = Request["subtypecontrol"];
                if (string.IsNullOrEmpty(testData))
                {
                    return View();
                }
                if (string.IsNullOrEmpty(referenceData))
                {
                    return View();
                }
                if (string.IsNullOrEmpty(templateData))
                {
                    return View();    
                }
                if (string.IsNullOrEmpty(projectData))
                {
                    return View();
                }
                try
                {
                    testEntries = new JavaScriptSerializer().Deserialize<List<HITestEntryDto>>(testData);
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }

                try
                {
                    referenceEntries = new JavaScriptSerializer().Deserialize<List<HITestEntryDto>>(referenceData);
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                try
                {
                    projectEntries = new JavaScriptSerializer().Deserialize<List<HITestEntryDto>>(projectData);
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                try
                {
                    if (!string.IsNullOrEmpty(subtypecontrolData))
                    {
                        subtypecontrolEntries = new JavaScriptSerializer().Deserialize<List<HITestEntryDto>>(subtypecontrolData);
                        referenceEntries.AddRange(subtypecontrolEntries);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                try
                {
                    // \/Date(1330848000000-0800)\/
                    localtemplate = new JavaScriptSerializer().Deserialize<HITestTemplateDto>(templateData);
                    localtemplate.HITestEntries = testEntries.Where(s => s.IsRow);
                    localtemplate.HIReferenceEntries = referenceEntries.Where(s => s.IsRow || s.IsColumn || s.IsReference2 || s.EntryType.Equals(PanOpTESConstants.EntryTypes.st_ctrl));
                    localtemplate.HIProjectEntries = projectEntries.Where(s => s.IsRow);
                }
                catch (Exception ex)
                {

                    throw ex;
                }                
            }
            else
            {
                localtemplate = template;
            };
            if (localtemplate == null)
            {
                return View();
            };
            var document = HITestRenderService.GetODocTemplate(localtemplate);
            if (document.Success)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = localtemplate.Filename,
                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(document.Data, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            else
            {
                return View();
            }
        }

    }
}
