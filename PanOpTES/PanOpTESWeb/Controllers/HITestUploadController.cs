﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;

namespace PanOpTESWeb.Controllers
{
    public class HITestUploadController : Controller
    {
        public IHITestIntakeService HITestIntakeService { get; set; }
        public IHITestDataService HITestDataService { get; set; }
        public IHITestCalculationService HITesCalculationService { get; set; }
        public HITestUploadController(IHITestIntakeService intakeService, IHITestDataService dataService, IHITestCalculationService calcService)
        {
            this.HITestIntakeService = intakeService;
            this.HITestDataService = dataService;
            this.HITesCalculationService = calcService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file, string description)
        {
            var uploaded = false;
            if (!string.IsNullOrEmpty(description) && file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                try
                {
                    byte[] data;
                    using (var inputStream = file.InputStream)
                    {
                        var memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        data = memoryStream.ToArray();
                    }
                    var testDto = HITestIntakeService.ImportTemplate(fileName, description, data);
                    if (testDto.Success && testDto.Data != null)
                    {
                        // PDC Temp Lookup of Speciments, testing CDCId lookup
                        var ids = testDto.Data.HITestEntries.Select(e => e.CDCId).Distinct().ToArray();
                        var specimens = HITestDataService.GetSpecimensByCDCId(ids);
                        if (specimens.Data != null)
                        {
                            testDto.Data.HITestEntries.ToList().ForEach(
                                delegate(HITestEntryDto testEntry)
                                {
                                    if (testEntry != null && !String.IsNullOrEmpty(testEntry.CDCId))
                                    {
                                        var spec = specimens.Data.FirstOrDefault(s => s.CDCId.Equals(testEntry.CDCId));
                                        if (spec != null && testEntry.Subsample != null)
                                        {
                                            testEntry.Subsample.Specimen = spec;
                                            testEntry.Subsample.SpecimenId = spec.Id;
                                        }
                                    }
                                }
                                );
                        }
                        var calculated = HITesCalculationService.CalculateHITestResults(testDto.Data);
                        if (calculated.Success && calculated.Data != null)
                        {
                            var added = HITestDataService.AddHITest(calculated.Data);
                            if (added.Success)
                            {
                                uploaded = true;
                                var dcus = HITestDataService.GetDCUByTestId(added.Data);
                                if (dcus.Success && dcus.Data.Count > 0)
                                {
                                    var pathDCU = @"C:\Temp\DCUDrop";

                                    var rootWebConfig1 =
                                        System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(@"/PanOpTES");
                                    if (rootWebConfig1.AppSettings.Settings.Count > 0)
                                    {
                                        var customSetting =
                                            rootWebConfig1.AppSettings.Settings["dcuDropLocation"];
                                        if (customSetting != null)
                                        {
                                            pathDCU = customSetting.Value;
                                        }
                                    }
                                
                                    if (Directory.Exists(pathDCU))
                                    {
                                        var di = new DirectoryInfo(pathDCU);
                                        var filename = Path.Combine(
                                            di.FullName,
                                            String.Format("{0}.dcu", DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss")));
                                        using (var outfile = new StreamWriter(filename, true))
                                        {
                                            outfile.WriteLine(StarLIMSDCUDto.GetHeader());
                                            foreach (var starLimsdcuDto in dcus.Data)
                                            {
                                                outfile.WriteLine(starLimsdcuDto.ToString());
                                            }
                                        }                                                
                                    }
                                }
                            }                            
                        }

                    }
                }
                catch (Exception ex)
                {
                }
            //    // store the file inside ~/App_Data/uploads folder
            //    var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
            //    file.SaveAs(path);
            }
            // redirect back to the index action to show the form once again
            return uploaded ? RedirectToAction("Index", "HITest") : RedirectToAction("Error");
            
        }
    }
}

