﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PanOpTES.DataTransferObjects;
using PanOpTES.Services.Interfaces;
using Yahara.ServiceInfrastructure.Messages;

namespace PanOpTESWeb.Controllers
{
    public class HITestServiceController : ApiController
    {
        public IHITestDataService HITestDataService { get; set; }
        public HITestServiceController(IHITestDataService service)
        {
            this.HITestDataService = service;
        }

        public List<HITestDto> Get()
        {
            var response = HITestDataService.GetAllHITests();
            return response.Data.ToList();
        }
    }
}
