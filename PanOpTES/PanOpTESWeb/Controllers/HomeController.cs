﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PanOpTESWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "PanOpTES helps make decisions--besides 'What's for lunch?'";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Feel free to give us a jingle.";

            return View();
        }
    }
}
