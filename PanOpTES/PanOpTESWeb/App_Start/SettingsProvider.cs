﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using PanOpTES.Services.Interfaces;

namespace PanOpTESWeb.App_Start
{
    public class SettingsProvider : ISettingsProvider
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["PanOpTESSqlConnection"].ConnectionString;
        }
    }
}