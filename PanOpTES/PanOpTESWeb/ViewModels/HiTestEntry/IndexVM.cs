﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanOpTES.DataTransferObjects;

namespace PanOpTESWeb.ViewModels.HiTestEntry
{
    public class IndexVM
    {
        public int id { get; set; }
        public HITestTemplateDto TemplateDto { get; set; }
    }
}