﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class HITestEntryDto
    {
        public HITestEntryDto()
        {
            HITestResults = new List<HITestResultDto>();
        }

        public int Id { get; set; }
        public String EntryDescription { get; set; }
        public short Position { get; set; }
        public int? Titer { get; set; }
        public short? BackTiter { get; set; }
        public EntryTypeDto EntryType { get; set; }
        public bool IsRow
        {
            get { return _isRow; }
            set { _isRow = value; }
        }
        public bool IsColumn
        {
            get { return _isColumn; }
            set { _isColumn = value; }
        }

        public bool IsReference2 { get; set; }

        private bool _isRow = true;
        private bool _isColumn = true;
        public int? SubsampleId { get; set; }
        public SubsampleDto Subsample
        {
            get { return _subsample; }
            set
            {
                _cdcId = null;
                _subsample = value;
            }
        }

        private string _alternativeId;
        public String AlternativeId
        {
            get
            {
                if (string.IsNullOrEmpty(_alternativeId) && Subsample != null)
                {
                    _alternativeId = String.IsNullOrEmpty(Subsample.ExternalSubsampleId) ? CDCId : Subsample.ExternalSubsampleId;
                }
                return _alternativeId;
            }
            set
            {
                _alternativeId = value;
            }
        }

        private string _cdcId;
        private SubsampleDto _subsample;

        public String CDCId
        {
            get
            {
                if (string.IsNullOrEmpty(_cdcId) && Subsample != null && Subsample.Specimen != null)
                {
                    _cdcId = Subsample.Specimen.CDCId;
                }
                return _cdcId;
            }
            set
            {
                _cdcId = value;
            }
        }

        private DateTime? _dateCollected;
        public DateTime? DateCollected
        {
            get
            {
                if (!_dateCollected.HasValue && Subsample != null && Subsample.Specimen != null)
                {
                    _dateCollected = Subsample.Specimen.DateCollected;
                }
                return _dateCollected;
            }
            set
            {
                _dateCollected = value;
            }
        }

        private String _dateHarvested;
        public String DateHarvested
        {
            get
            {
                if (String.IsNullOrEmpty(_dateHarvested) && Subsample != null)
                {
                    _dateHarvested = Subsample.DateHarvestedDisplay;
                }
                return _dateHarvested;
            }
            set
            {
                _dateHarvested = value;
            }
        }

        private String _passage;
        public String Passage
        {
            get
            {
                if (String.IsNullOrEmpty(_passage) && Subsample != null)
                {
                    _passage = Subsample.Passage;
                }
                return _passage;
            }
            set
            {
                _passage = value;
            }
        }

        public IEnumerable<HITestResultDto> HITestResults { get; set; }
    }
}
