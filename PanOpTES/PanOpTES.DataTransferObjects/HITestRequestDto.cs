﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class HITestRequestDto
    {
        public int Id { get; set; }
        public String HITestEntryName { get; set; }
        public String HISerumEntryName { get; set; }
        public EntryTypeDto EntryType { get; set; }
        public int Difference { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}

