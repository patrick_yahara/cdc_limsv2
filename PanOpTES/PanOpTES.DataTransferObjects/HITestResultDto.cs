﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class HITestResultDto
    {
        public int Id { get; set; }
        public int Titer { get; set; }
        public bool IsError { get; set; }
        public int TestEntryId { get; set; }
        public int SerumEntryId { get; set; }
        public int LogFold { get; set; }
        public HITestEntryDto TestEntry { get; set; }
        public HISerumEntryDto SerumEntry { get; set; }
    }
}
