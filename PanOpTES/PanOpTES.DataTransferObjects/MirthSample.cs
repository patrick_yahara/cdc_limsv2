﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PanOpTES.DataTransferObjects
{
    [Serializable]
    [XmlRoot(ElementName = "sample")]
    public class MirthSample
    {
        public MirthSample()
        {
            subsamples = new List<MirthSubsample>();
        }

        public string cdc_id { get; set; }
        public string country { get; set; }
        public string date_collected { get; set; }
        public string specimen_id { get; set; }
        public string strain_name { get; set; }
        public string state { get; set; }
        [XmlArrayItem("subsample", typeof(MirthSubsample))]
        [XmlArray("subsamples")]
        public List<MirthSubsample> subsamples { get; set; }
    }
}
