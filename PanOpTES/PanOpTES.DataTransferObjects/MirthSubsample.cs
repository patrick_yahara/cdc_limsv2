﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PanOpTES.DataTransferObjects
{
    [Serializable]
    public class MirthSubsample
    {
        public MirthSubsample()
        {
        }

        public string flulims_ssid { get; set; }
        public string cuid { get; set; }
        public string contract_lab { get; set; }
        public string date_harvested { get; set; }
        public string ha_results { get; set; }
        public string lot { get; set; }
        public string passage { get; set; }
        public MirthHATiter ha_titer { get; set; }
        public MirthHiTest hi_test { get; set; }
    }
}
