﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class HITestDto
    {
        public HITestDto()
        {
            HITestEntries = new List<HITestEntryDto>();
            HISerumEntries = new List<HISerumEntryDto>();
            HITestRequests = new List<HITestRequestDto>();
        }

        public int Id { get; set; }
        public string TestFileName { get; set; }
        public string TestDescription { get; set; }
        public DateTime DateTested { get; set; }
        public IEnumerable<HITestEntryDto> HITestEntries { get; set; }
        public IEnumerable<HISerumEntryDto> HISerumEntries { get; set; }
        public IEnumerable<HITestRequestDto> HITestRequests { get; set; }
        public String DateTestedDisplay
        {
            get { return DateTested.ToShortDateString(); }
        }
    }
}
