﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class SpecimenDto
    {
        public int Id { get; set; }
        public string CDCId { get; set; }
        public DateTime? DateCollected { get; set; }
    }
}
