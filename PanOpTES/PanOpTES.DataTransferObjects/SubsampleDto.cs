﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class SubsampleDto
    {
        public int Id { get; set; }
        public int SpecimenId { get; set; }
        public string Passage { get; set; }
        public string ExternalCUId { get; set; }
        public string ExternalSubsampleId { get; set; }
        public DateTime? DateHarvested { get; set; }
        public SpecimenDto Specimen { get; set; }
        public String DateHarvestedDisplay {
            get { return DateHarvested.HasValue ? DateHarvested.Value.ToShortDateString() : String.Empty; }
        }
    }
}
