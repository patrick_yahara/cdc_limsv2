﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanOpTES.DataTransferObjects
{
    public class HISerumEntryDto
    {
        public HISerumEntryDto()
        {
            HITestResults = new List<HITestResultDto>();
        }
        public int Id { get; set; }
        public EntryTypeDto EntryType { get; set; }
        public String SerumEntryDescription { get; set; }
        public string ExternalSubsampleId { get; set; }
        public int Position { get; set; }
        public IEnumerable<HITestResultDto> HITestResults { get; set; }
    }
}
