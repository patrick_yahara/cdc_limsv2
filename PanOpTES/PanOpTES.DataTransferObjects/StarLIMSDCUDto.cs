﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;

namespace PanOpTES.DataTransferObjects
{
    public class StarLIMSDCUDto
    {
        /* CUID	Test ID	Test Name	Test Date	RBC Type	Log Fold Change	Interpretation */
        public String CUID { get; set; }
        public String TestID { get; set; }
        public String TestName { get; set; }
        public String TestDate { get; set; }
        public String RBCType { get; set; }
        public String LogFoldChange { get; set; }
        public String Interpretation { get; set; }

        public static String GetHeader()
        {
            return @"CUID,Test ID,Test Name,Test Date,RBC Type,Log Fold Change,Interpretation";
        }

        public override string ToString()
        {
            return String.Format(@"{0},{1},{2},{3},{4},{5},{6}", CUID, TestID, TestName, TestDate, RBCType, LogFoldChange, Interpretation);
        }
    }
}
