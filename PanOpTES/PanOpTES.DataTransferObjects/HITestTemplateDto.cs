﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace PanOpTES.DataTransferObjects
{
    public class HITestTemplateDto
    {
        [ScriptIgnore]
        public DateTime CreatedDate { get; set; }
        public IEnumerable<HITestEntryDto> HITestEntries { get; set; }
        public IEnumerable<HITestEntryDto> HIReferenceEntries { get; set; }
        public IEnumerable<HITestEntryDto> HIProjectEntries { get; set; }
        public int BaseHITestId { get; set; }

        public HITestTemplateDto()
        {
            CreatedDate = DateTime.UtcNow;
        }

        public string Filename
        {
            get { return string.Format(@"{0}_{1}.xlsx", BaseHITestId, CreatedDate.ToString("dMMMyyyy")); }
        }
    }
}
